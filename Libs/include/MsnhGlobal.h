﻿#ifndef MSNHGLOBAL_H
#define MSNHGLOBAL_H

#include <algorithm>
#include <math.h>

#ifndef MSNH_LIBS_VERSION
#define MSNH_LIBS_VERSION 2
#endif

#if defined(__linux__) || defined(__APPLE__)
    #define EXTERNC         extern "C"
    #define MSNH_SHARED_EXPORT     __attribute__((visibility("default")))
    #define MSNH_SHARED_IMPORT     __attribute__((visibility("default")))
    #define MSNH_SHARED_HIDDEN     __attribute__((visibility("hidden")))
#endif

#if _WIN32
    #if defined(MSNH_LIBRARY)
    #  define MSNH_SHARED_EXPORT __declspec(dllexport)
    #else
    #  define MSNH_SHARED_EXPORT __declspec(dllimport)
    #endif

    #pragma warning(disable:4251) // class 'type' needs to have dll-interface to be used by clients of class 'type2'
    #pragma warning(disable:4100) // unreferenced formal parameter
    #pragma warning(disable:4996) // strtok unsafe
    #pragma warning(disable:4244) // possible loss of data
    #define _CRT_SECURE_NO_WARNINGS // this function or variable may be unsave
#endif


enum MsnhProcessWidgetType
{
    MSNH_WIDGET_DEFAULT = 0,
    MSNH_WIDGET_HIDDEN,

    //Bitfield or Booleans
    MSNH_WIDGET_CHECKBOXES,

    //Enumerated integer
    MSNH_WIDGET_RADIOBUTTONS,
    MSNH_WIDGET_COMBOBOX,

    //Numeric
    MSNH_WIDGET_SLIDER,
    MSNH_WIDGET_SPINNER,

    //String
    MSNH_WIDGET_TEXTFIELD,
    MSNH_WIDGET_LABEL,
    MSNH_WIDGET_TITLE,
    MSNH_WIDGET_FILE_OPEN,
    MSNH_WIDGET_FILE_SAVE,
    MSNH_WIDGET_FOLDER,

    //Integer: A button that increments the property after each click
    //Bool:    A button that returns true once after it has been pressed
    MSNH_WIDGET_BUTTON,

    MSNH_WIDGET_GROUP,

    //Add additional widget types here
    //...
    MSNH_NUM_WIDGETS
};

MSNH_SHARED_EXPORT const char *widgetName(MsnhProcessWidgetType type);

#endif // MSNHGLOBAL_H
