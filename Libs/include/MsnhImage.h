﻿#ifndef MSNHIMAGE_H
#define MSNHIMAGE_H

#include <opencv2/opencv.hpp>
#include <MsnhProto/Protocal/cpp/Base.h>

namespace MsnhProtocal
{
class MsnhImage : public MsnhProtocal::Base
{
public:
    MsnhImage():MsnhProtocal::Base(999999999, MsnhProtoType::IS_DATA)
    {
        _specialType = true;
        _specialName = "MsnhImage";
    }

    static uint32_t getID(){return 999999999;}
    static std::string getName(){return "MsnhImage";}
    cv::Mat mat;
};
}
#endif // MSNHIMAGE_H
