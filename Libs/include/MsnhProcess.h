﻿#ifndef MSNH_PROCESS_H
#define MSNH_PROCESS_H

#include <MsnhProcessProperty.h>
#include <MsnhGlobal.h>
#include <pugg/Driver.h>
#include <MsnhProto/Protocal/cpp/Base.h>


//typedef std::map<std::string, std::shared_ptr<MsnhProcessProperty>> MsnhProcessPropertyMap;

class MSNH_SHARED_EXPORT MsnhProcess
{
public:
    enum MsnhProcessCategory
    {
        CATEGORY_UNDEFINED,
        CATEGORY_2D_CAM_IO,
        CATEGORY_3D_CAM_IO,
        CATEGORY_DATA_IO,
        CATEGORY_IMAGE_PROC,
        CATEGORY_DATA_PROC,
    };

    MsnhProcess();
    virtual ~MsnhProcess();

    void setCategory(MsnhProcessCategory category){_category = category;}
    MsnhProcessCategory getCategory() const {return _category;}

    void setIsSource(bool isSource){_isSource = isSource;}
    bool getIsSource(){return _isSource;}

    virtual MsnhProcess* clone () const = 0;
    virtual bool init () = 0;
    virtual void refresh(){}
    virtual void destroy () = 0;
    virtual bool resetParams() = 0;
    virtual bool processInputData(MsnhProtocal::Base *) = 0;
    virtual MsnhProtocal::Base* getResultData() = 0;


    void addProcessPropertyInt (const char* name, const char* title, const char* description, int value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT, int min = 0, int max = 0);
    void addProcessPropertyUnsignedInt (const char* name, const char* title, const char* description, unsigned int value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT, unsigned int min = 0, unsigned int max = 0);
    void addProcessPropertyDouble (const char* name, const char* title, const char* description, double value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT, double min = 0.0, double max = 0.0);
    void addProcessPropertyFloat (const char* name, const char* title, const char* description, float value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT, float min = 0.0f, float max = 0.0f);
    void addProcessPropertyBool (const char* name, const char* title, const char* description, bool value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);
    void addProcessPropertyBoolOneShot (const char* name, const char* title, const char* description, bool value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);
    void addProcessPropertyString (const char* name, const char* title, const char* description, const std::string &value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);
    void addProcessPropertyVectorInt (const char* name, const char* title, const char* description, const std::vector<int> &value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);
    void addProcessPropertyVectorDouble (const char* name, const char* title, const char* description, const std::vector<double> &value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);
    int  getProcessPropertyInt (const char* name);
    unsigned int getProcessPropertyUnsignedInt (const char* name);
    double getProcessPropertyDouble (const char* name);
    float getProcessPropertyFloat (const char* name);
    bool getProcessPropertyBool (const char* name);
    bool getProcessPropertyBoolOneShot (const char* name);
    std::string getProcessPropertyString (const char* name);
    std::vector<int> getProcessPropertyVectorInt (const char* name);
    std::vector<double> getProcessPropertyVectorDouble (const char* name);

    static const int version = MSNH_LIBS_VERSION;
    static const std::string serverName();

    MsnhProcessPropertyMap *getProperties();

protected:
    void checkPropertyKey(const char* name);
    MsnhProcessCategory _category;
    bool _isSource;
    MsnhProcessPropertyMap _properties;
};

template <class Derived>
class MsnhClonableProcess : public MsnhProcess
{
public:
    virtual MsnhProcess* clone() const
    {
        return new Derived(static_cast<const Derived&>(*this)); // call the copy ctor.
    }
};

class MsnhProcessDriver : public pugg::Driver
{
public:
    MsnhProcessDriver(std::string name, int version) : pugg::Driver(MsnhProcess::serverName(),name,version) {}
    virtual MsnhProcess* create() = 0;
    virtual std::string className() = 0;
    virtual std::string author() = 0;
    virtual int         version() = 0;

};
#endif // MSNH_PROCESS_H
