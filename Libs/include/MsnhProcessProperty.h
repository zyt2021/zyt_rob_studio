#ifndef MSNHPROCESSPROPERTY_H
#define MSNHPROCESSPROPERTY_H

#include <MsnhGlobal.h>
#include <cstring>
#include <string>
#include <map>
#include <sstream>
#include <vector>
#include <memory>

class MsnhProcess;

// we need a base class in order to use the template in std::map
class MSNH_SHARED_EXPORT MsnhProcessProperty
{
public:
    struct SerializedData
    {
        std::string type;
        std::string widget;
        std::string widgetName;
        std::string value;
    };

    struct DeserialationFailed : public std::runtime_error
    { DeserialationFailed(): std::runtime_error("") {} };

    int position() const                  { return _position; }
    const char* name() const              { return _name; }
    const char* title() const             { return _title; }
    const char* description() const       { return _description; }
    MsnhProcessWidgetType widget() const   { return _widget; }
    virtual const char* type() const = 0;
    virtual SerializedData serialize() const = 0;
    virtual void deserialize(const SerializedData &data) = 0;
    virtual MsnhProcessProperty *clone() const = 0;
    virtual void resetValue() = 0;

protected:
    MsnhProcessProperty(int position,
                       const char *name,
                       const char *title,
                       const char *description,
                       MsnhProcess *process,
                       MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);

    int _position;                  //!< Position in GUI
    const char* _name;              //!< ID for GUI
    const char* _title;             //!< Short title for GUI
    const char* _description;       //!< Short help for GUI
    MsnhProcess*  _process;
    MsnhProcessWidgetType _widget;
};


// templates are not possible because of serialization issues

// INT
class MSNH_SHARED_EXPORT MsnhProcessPropertyInt : public MsnhProcessProperty
{
public:
    MsnhProcessPropertyInt(MsnhProcess* process, int position, const char* name, const char* title, const char* description, int value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT, int min=0, int max=0);

    int min() const                         { return _min; }
    int max() const                         { return _max; }
    int value() const                       { return _value; }
    void setValue(int value);
    void resetValue()                       { setValue(_default); }
    virtual const char *type() const        { return "int"; }
    virtual SerializedData serialize() const;
    virtual void deserialize(const SerializedData &data);
    MsnhProcessProperty *clone() const;

private:
    int _min;                         //!< min value, may be used in GUI
    int _max;                         //!< max value, may be used in GUI
    int _value;                       //!< current value
    int _default;                     //!< default value
};

// UNSIGNED INT
class MSNH_SHARED_EXPORT MsnhProcessPropertyUnsignedInt: public MsnhProcessProperty
{
public:
    MsnhProcessPropertyUnsignedInt(MsnhProcess* process, int position, const char* name, const char* title, const char* description, unsigned int value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT, unsigned int min=0, unsigned int max=0);

    unsigned int min() const                         { return _min; }
    unsigned int max() const                         { return _max; }
    unsigned int value() const                       { return _value; }
    void setValue(unsigned int value);
    void resetValue()                                { setValue(_default); }
    virtual const char *type() const                 { return "uint"; }
    virtual SerializedData serialize() const;
    virtual void deserialize(const SerializedData &data);
    MsnhProcessProperty *clone() const;

private:
    unsigned int _min;                         //!< min value, may be used in GUI
    unsigned int _max;                         //!< max value, may be used in GUI
    unsigned int _value;                       //!< current value
    unsigned int _default;                     //!< default value
};

// DOUBLE
class MSNH_SHARED_EXPORT MsnhProcessPropertyDouble : public MsnhProcessProperty
{
public:
    MsnhProcessPropertyDouble(MsnhProcess* process, int position, const char* name, const char* title, const char* description, double value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT, double min=0.0, double max=0.0);

    double min() const                         { return _min; }
    double max() const                         { return _max; }
    double value() const                       { return _value; }
    void setValue(double value);
    void resetValue()                          { setValue(_default); }
    virtual const char *type() const           { return "double"; }
    virtual SerializedData serialize() const;
    virtual void deserialize(const SerializedData &data);
    MsnhProcessProperty *clone() const;

private:
    double _min;                         //!< min value, may be used in GUI
    double _max;                         //!< max value, may be used in GUI
    double _value;                       //!< current value
    double _default;                     //!< default value
};

// DOUBLE
class MSNH_SHARED_EXPORT MsnhProcessPropertyFloat : public MsnhProcessProperty
{
public:
    MsnhProcessPropertyFloat(MsnhProcess* process, int position, const char* name, const char* title, const char* description, float value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT,  float min=0.0f, float max=0.0f);

    float min() const                         { return _min; }
    float max() const                         { return _max; }
    float value() const                       { return _value; }
    void setValue(float value);
    void resetValue()                         { setValue(_default); }
    virtual const char *type() const          { return "float"; }
    virtual SerializedData serialize() const;
    virtual void deserialize(const SerializedData &data);
    MsnhProcessProperty *clone() const;

private:
    float _min;                         //!< min value, may be used in GUI
    float _max;                         //!< max value, may be used in GUI
    float _value;                       //!< current value
    float _default;                     //!< default value
};



// BOOL
class MSNH_SHARED_EXPORT MsnhProcessPropertyBool : public MsnhProcessProperty
{
public:
    MsnhProcessPropertyBool(MsnhProcess* process, int position, const char* name, const char* title, const char* description, bool value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);

    bool value() const                       { return _value; }
    void setValue(bool value);
    void resetValue()                        { setValue(_default); }
    virtual const char *type() const         { return "bool"; }
    virtual SerializedData serialize() const;
    virtual void deserialize(const SerializedData &data);
    MsnhProcessProperty *clone() const;

protected:
    bool _value;                       //!< current value
    bool _default;                     //!< default value
};

// BOOL ONE SHOT
class MSNH_SHARED_EXPORT MsnhProcessPropertyBoolOneShot : public MsnhProcessPropertyBool
{
public:
    MsnhProcessPropertyBoolOneShot(MsnhProcess* process, int position, const char* name, const char* title, const char* description, bool value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);
    bool value()                             { bool ret = _value; _value = false; return ret; }
};

// STRING
class MSNH_SHARED_EXPORT MsnhProcessPropertyString : public MsnhProcessProperty
{
public:
    MsnhProcessPropertyString(MsnhProcess* process, int position, const char* name, const char* title, const char* description, const std::string &value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);

    std::string value() const                       { return _value; }
    void setValue(const std::string &value);
    void setValue(std::string &&value);
    void resetValue()                               { setValue(_default); }
    virtual const char *type() const                { return "string"; }
    virtual SerializedData serialize() const;
    virtual void deserialize(const SerializedData &data);
    MsnhProcessProperty *clone() const;

private:
    std::string _value;                       //!< current value
    std::string _default;                     //!< default value
};


// VECTOR<INT>
class MSNH_SHARED_EXPORT MsnhProcessPropertyVectorInt : public MsnhProcessProperty
{
public:
    MsnhProcessPropertyVectorInt(MsnhProcess* process, int position, const char* name, const char* title, const char* description, const std::vector<int> &value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);

    const std::vector<int> &value() const                       { return _value; }
    void setValue(const std::vector<int> &value);
    void setValue(std::vector<int> &&value);
    void resetValue()                                           { setValue(_default); }
    virtual const char *type() const                            { return "vector<int>"; }
    virtual SerializedData serialize() const;
    virtual void deserialize(const SerializedData &data);
    MsnhProcessProperty *clone() const;

private:
    std::vector<int> _value;                       //!< current value
    std::vector<int> _default;                     //!< default value
};

// VECTOR<DOUBLE>
class MSNH_SHARED_EXPORT MsnhProcessPropertyVectorDouble : public MsnhProcessProperty
{
public:
    MsnhProcessPropertyVectorDouble(MsnhProcess* process, int position, const char* name, const char* title, const char* description, const std::vector<double> &value, MsnhProcessWidgetType widget = MSNH_WIDGET_DEFAULT);

    const std::vector<double> &value() const                    { return _value; }
    void setValue(const std::vector<double> &value);
    void setValue(std::vector<double> &&value);
    void resetValue()                                           { setValue(_default); }
    virtual const char *type() const                            { return "vector<double>"; }
    virtual SerializedData serialize() const;
    virtual void deserialize(const SerializedData &data);
    MsnhProcessProperty *clone() const;

private:
    std::vector<double> _value;                       //!< current value
    std::vector<double> _default;                     //!< default value
};

//! MsnhProcessPropertyMap
typedef std::map<std::string, std::shared_ptr<MsnhProcessProperty>> MsnhProcessPropertyMap;


#endif // MSNHPROCESSPROPERTY_H
