﻿#ifndef MSNHOPENCVCAMERA_H
#define MSNHOPENCVCAMERA_H

#include <MsnhProcess.h>
#include <MsnhImage.h>
#include <Msnhnet/cv/MsnhCVVideo.h>
#include <iostream>

class MSNH_SHARED_EXPORT MsnhOpenCVCamera : public MsnhClonableProcess<MsnhOpenCVCamera>
{
public:
    MsnhOpenCVCamera(): MsnhClonableProcess(){_category = CATEGORY_2D_CAM_IO;}
    bool init();
    bool resetParams();
    void refresh();
    void listCameras();
    void destroy();
    bool processInputData(MsnhProtocal::Base* data);
    MsnhProtocal::Base *getResultData();

private:
    MsnhProtocal::MsnhImage* _res;
    cv::VideoCapture _cap;
    cv::Mat _mat;
    Msnhnet::VideoCapture cap;
};


#endif // MSNHOPENCVCAMERA_H
