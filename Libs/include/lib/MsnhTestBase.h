﻿#ifndef MSNHTESTBASE_H
#define MSNHTESTBASE_H
#include <MsnhProcess.h>
#include <MsnhProto/Protocal/cpp/Ping.h>
#include <iostream>

class MSNH_SHARED_EXPORT MsnhTestBase : public MsnhClonableProcess<MsnhTestBase>
{
public:
    MsnhTestBase(): MsnhClonableProcess(){_category = CATEGORY_DATA_PROC;}
    ~MsnhTestBase() {}
    bool init();
    bool resetParams();
    void destroy();
    bool processInputData(MsnhProtocal::Base* data);
    MsnhProtocal::Base* getResultData();

};

#endif // MSNHTESTBASE_H
