﻿#ifndef MSNHTESTIMAGE_H
#define MSNHTESTIMAGE_H

#include <MsnhProcess.h>
#include <MsnhImage.h>
#include <iostream>

class MSNH_SHARED_EXPORT MsnhTestImage : public MsnhClonableProcess<MsnhTestImage>
{
public:
    MsnhTestImage(): MsnhClonableProcess(){_category = CATEGORY_IMAGE_PROC;}
    ~MsnhTestImage() {}
    bool init();
    bool resetParams();
    void destroy();
    bool processInputData(MsnhProtocal::Base* data);
    MsnhProtocal::Base *getResultData();

private:
    MsnhProtocal::MsnhImage* _res;
};
#endif // MSNHTESTIMAGE_H
