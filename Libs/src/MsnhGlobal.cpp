#include <MsnhGlobal.h>

const char* const widgetNames[MSNH_NUM_WIDGETS] =
{
    "MSNH_WIDGET_DEFAULT",
    "MSNH_WIDGET_HIDDEN",
    "MSNH_WIDGET_CHECKBOXES",
    "MSNH_WIDGET_RADIOBUTTONS",
    "MSNH_WIDGET_COMBOBOX",
    "MSNH_WIDGET_SLIDER",
    "MSNH_WIDGET_SPINNER",
    "MSNH_WIDGET_TEXTFIELD",
    "MSNH_WIDGET_LABEL",
    "MSNH_WIDGET_TITLE",
    "MSNH_WIDGET_FILE_OPEN",
    "MSNH_WIDGET_FILE_SAVE",
    "MSNH_WIDGET_FOLDER",
    "MSNH_WIDGET_BUTTON",
    "MSNH_WIDGET_GROUP",
};

const char *widgetName(MsnhProcessWidgetType type)
{
    if (type < 0 || type >= MSNH_NUM_WIDGETS)
        return "UNKNOWN";
    return widgetNames[type];
}
