﻿#include <MsnhProcess.h>


MsnhProcess::MsnhProcess()
{
    _category = CATEGORY_UNDEFINED;
    _isSource = false;
}

MsnhProcess::~MsnhProcess()
{
}

void MsnhProcess::addProcessPropertyInt(const char *name, const char *title, const char *description, int value, MsnhProcessWidgetType widget, int min, int max)
{
    _properties[name].reset(new MsnhProcessPropertyInt(this, (int)_properties.size(), name, title, description, value, widget, min, max));
}

void MsnhProcess::addProcessPropertyUnsignedInt(const char *name, const char *title, const char *description, unsigned int value, MsnhProcessWidgetType widget, unsigned int min, unsigned int max)
{
    _properties[name].reset(new MsnhProcessPropertyUnsignedInt(this, (int)_properties.size(), name, title, description, value, widget, min, max));
}

void MsnhProcess::addProcessPropertyDouble(const char *name, const char *title, const char *description, double value, MsnhProcessWidgetType widget, double min, double max)
{
    _properties[name].reset(new MsnhProcessPropertyDouble(this, (int)_properties.size(), name, title, description, value, widget, min, max));
}

void MsnhProcess::addProcessPropertyFloat(const char *name, const char *title, const char *description, float value, MsnhProcessWidgetType widget, float min, float max)
{
    _properties[name].reset(new MsnhProcessPropertyFloat(this, (int)_properties.size(), name, title, description, value, widget, min, max));
}

void MsnhProcess::addProcessPropertyBool(const char *name, const char *title, const char *description, bool value, MsnhProcessWidgetType widget)
{
    _properties[name].reset(new MsnhProcessPropertyBool(this, (int)_properties.size(), name, title, description, value, widget));
}

void MsnhProcess::addProcessPropertyBoolOneShot(const char *name, const char *title, const char *description, bool value, MsnhProcessWidgetType widget)
{
    _properties[name].reset(new MsnhProcessPropertyBoolOneShot(this, (int)_properties.size(), name, title, description, value, widget));
}

void MsnhProcess::addProcessPropertyString(const char *name, const char *title, const char *description, const std::string &value, MsnhProcessWidgetType widget)
{
    _properties[name].reset(new MsnhProcessPropertyString(this, (int)_properties.size(), name, title, description, value, widget));
}

void MsnhProcess::addProcessPropertyVectorInt(const char *name, const char *title, const char *description, const std::vector<int> &value, MsnhProcessWidgetType widget)
{
    _properties[name].reset(new MsnhProcessPropertyVectorInt(this, (int)_properties.size(), name, title, description, value, widget));
}

void MsnhProcess::addProcessPropertyVectorDouble(const char *name, const char *title, const char *description, const std::vector<double> &value, MsnhProcessWidgetType widget)
{
    _properties[name].reset(new MsnhProcessPropertyVectorDouble(this, (int)_properties.size(), name, title, description, value, widget));
}

int MsnhProcess::getProcessPropertyInt(const char *name)
{
    checkPropertyKey(name);
    return ((MsnhProcessPropertyInt*) _properties[name].get())->value();
}

unsigned int MsnhProcess::getProcessPropertyUnsignedInt(const char *name)
{
    checkPropertyKey(name);
    return ((MsnhProcessPropertyUnsignedInt*) _properties[name].get())->value();
}

double MsnhProcess::getProcessPropertyDouble(const char *name)
{
    checkPropertyKey(name);
    return ((MsnhProcessPropertyDouble*) _properties[name].get())->value();
}

float MsnhProcess::getProcessPropertyFloat(const char *name)
{
    checkPropertyKey(name);
    return ((MsnhProcessPropertyFloat*) _properties[name].get())->value();
}

bool MsnhProcess::getProcessPropertyBool(const char *name)
{
    checkPropertyKey(name);
    return ((MsnhProcessPropertyBool*) _properties[name].get())->value();
}

bool MsnhProcess::getProcessPropertyBoolOneShot(const char *name)
{
    checkPropertyKey(name);
    return ((MsnhProcessPropertyBoolOneShot*) _properties[name].get())->value();
}

std::string MsnhProcess::getProcessPropertyString(const char *name)
{
    checkPropertyKey(name);
    return ((MsnhProcessPropertyString*) _properties[name].get())->value();
}

std::vector<int> MsnhProcess::getProcessPropertyVectorInt(const char *name)
{
    checkPropertyKey(name);
    return ((MsnhProcessPropertyVectorInt*) _properties[name].get())->value();
}

std::vector<double> MsnhProcess::getProcessPropertyVectorDouble(const char *name)
{
    checkPropertyKey(name);
    return ((MsnhProcessPropertyVectorDouble*) _properties[name].get())->value();
}

const std::string MsnhProcess::serverName()
{
    return "MsnhProcessServer";
}

void MsnhProcess::checkPropertyKey(const char *name)
{
    if (_properties.find(name) == _properties.end())
    {
        std::string error("Invalid property key: ");
        error.append(name);
        throw std::runtime_error(error.c_str());
    }
}

MsnhProcessPropertyMap* MsnhProcess::getProperties()
{
    return &_properties;
}
