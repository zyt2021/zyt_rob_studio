#include "MsnhProcessProperty.h"
#include <MsnhProcess.h>
#include <array>
#include <cctype>
#include <regex>

template<class T>
inline std::string serializeValue(const T &value)
{
    std::ostringstream buffer;
    buffer << value;
    return buffer.str();
}

template<>
inline std::string serializeValue<bool>(const bool &value)
{
    std::ostringstream buffer;
    buffer <<  (value ? "true" : "false");
    return buffer.str();
}

template<>
inline std::string serializeValue<std::vector<int>>(const std::vector<int> &value)
{
    std::ostringstream buffer;
    buffer << "[";
    if (value.size() > 0) buffer << value[0];
    for (int i = 1; i < (int) value.size(); ++i) buffer << "," << value[i];
    buffer << "]";
    return buffer.str();
}

template<>
inline std::string serializeValue<std::vector<double>>(const std::vector<double> &value)
{
    std::ostringstream buffer;
    buffer << "[";
    if (value.size() > 0) buffer << value[0];
    for (int i = 1; i < (int) value.size(); ++i) buffer << "," << value[i];
    buffer << "]";
    return buffer.str();
}

template<class T>
inline MsnhProcessProperty::SerializedData serializeProperty(const char *type, MsnhProcessWidgetType widget, const T &value)
{
    MsnhProcessProperty::SerializedData result;
    result.type = serializeValue(type);
    result.widget = serializeValue(widget);
    result.widgetName = serializeValue(widgetName(widget));
    result.value = serializeValue(value);
    return result;
}

inline void deserializeValue(const std::string &data, bool &value)
{
    /*if (data.length() != 4)
    {
        value = false;
        return;
    }*/

    //static const unsigned char nonAsciiMap = 1<<7;
    std::string lowercase(data);
    //tolower is undefined (i.e. might crash) for non-ASCII characters. Since we don't want to parse those, just xor them out
    //std::transform(lowercase.begin(), lowercase.end(), lowercase.begin(), [](char c) { return c ^ nonAsciiMap; });
    std::transform(lowercase.begin(), lowercase.end(), lowercase.begin(), tolower); //Transform to lower case
    value = (lowercase.compare("true") == 0 || lowercase.compare("1") == 0);
}

inline void deserializeValue(const std::string &data, int &value)
{
    int result = 0;
    if (sscanf(data.c_str(),"%d",&result) < 1)
        throw MsnhProcessProperty::DeserialationFailed();
    value = result;
}

inline void deserializeValue(const std::string &data, MsnhProcessWidgetType &value)
{
    int tmp;
    deserializeValue(data,tmp);
    value = (MsnhProcessWidgetType) tmp;
}

inline void deserializeValue(const std::string &data, unsigned int &value)
{
    unsigned int result = 0;
    if (sscanf(data.c_str(),"%u",&result) < 1)
        throw MsnhProcessProperty::DeserialationFailed();
    value = result;
}

inline void deserializeValue(const std::string &data, float &value)
{
    float result = 0;
    if (sscanf(data.c_str(),"%f",&result) < 1)
        throw MsnhProcessProperty::DeserialationFailed();
    value = result;
}

inline void deserializeValue(const std::string &data, double &value)
{
    double result = 0;
    if (sscanf(data.c_str(),"%lf",&result) < 1)
        throw MsnhProcessProperty::DeserialationFailed();
    value = result;
}

inline void deserializeValue(const std::string &data, std::vector<int> &value)
{
    std::vector<int> result;
    std::smatch match;
    auto pos = data.begin();
    while(std::regex_search(pos,data.end(),match,std::regex("[0-9]|-")))
    {
        pos += match.position();

        int charsParsed = 0;
        int element = 0;
        if (sscanf(&(*pos),"%d%n",&element,&charsParsed) > 0)
            result.push_back(element);

        pos += charsParsed;
    }
    value.swap(result);
}

inline void deserializeValue(const std::string &data, std::vector<double> &value)
{
    std::vector<double> result;
    std::smatch match;
    auto pos = data.begin();
    while(std::regex_search(pos,data.end(),match,std::regex("[0-9]|-")))
    {
        pos += match.position();

        int charsParsed = 0;
        double element = 0;
        if (sscanf(&(*pos),"%f%n",&element,&charsParsed) > 0)
            result.push_back(element);

        pos += charsParsed;
    }
    value.swap(result);
}


inline void deserializeValue(const std::string &data, std::string &value)
{
    value = data;
}

template<class T>
inline void deserializeProperty(MsnhProcessProperty::SerializedData data, MsnhProcessWidgetType &widget, T &value)
{
    deserializeValue(data.widget,widget);
    deserializeValue(data.value,value);
}

MsnhProcessProperty::MsnhProcessProperty(int position, const char* name, const char *title, const char *description, MsnhProcess *process, MsnhProcessWidgetType widget):
    _position(position),
    _name(name),
    _title(title),
    _description(description),
    _process(process),
    _widget(widget)
{}

MsnhProcessPropertyInt::MsnhProcessPropertyInt(MsnhProcess *process, int position, const char* name, const char* title, const char *description, int value, MsnhProcessWidgetType widget, int min, int max):
    MsnhProcessProperty(position,name,title,description,process,widget),
    _value(value),
    _default(value),
    _min(min),
    _max(max)
{}

void MsnhProcessPropertyInt::setValue(int value)
{
    _value = value;
    //_process->requestUpdate();
}

MsnhProcessProperty::SerializedData MsnhProcessPropertyInt::serialize() const
{
    return serializeProperty(type(),_widget,_value);
}

void MsnhProcessPropertyInt::deserialize(const SerializedData &data)
{
    deserializeProperty(data,_widget,_value);
}

MsnhProcessProperty *MsnhProcessPropertyInt::clone() const
{
    return new MsnhProcessPropertyInt(*this);
}

MsnhProcessPropertyUnsignedInt::MsnhProcessPropertyUnsignedInt(MsnhProcess *process, int position, const char* name, const char* title, const char *description, unsigned int value, MsnhProcessWidgetType widget, unsigned int min, unsigned int max):
    MsnhProcessProperty(position,name,title,description,process,widget),
    _value(value),
    _default(value),
    _min(min),
    _max(max)
{}

void MsnhProcessPropertyUnsignedInt::setValue(unsigned int value)
{
    _value = value;
    //_process->requestUpdate();
}

MsnhProcessProperty::SerializedData MsnhProcessPropertyUnsignedInt::serialize() const
{
    return serializeProperty(type(),_widget,_value);
}

void MsnhProcessPropertyUnsignedInt::deserialize(const SerializedData &data)
{
    deserializeProperty(data,_widget,_value);
}

MsnhProcessProperty *MsnhProcessPropertyUnsignedInt::clone() const
{
    return new MsnhProcessPropertyUnsignedInt(*this);
}


MsnhProcessPropertyDouble::MsnhProcessPropertyDouble(MsnhProcess *process, int position, const char* name, const char* title, const char *description, double value, MsnhProcessWidgetType widget, double min, double max):
    MsnhProcessProperty(position,name,title,description,process, widget),
    _value(value),
    _default(value),
    _min(min),
    _max(max)
{}

void MsnhProcessPropertyDouble::setValue(double value)
{
    _value = value;
    //_process->requestUpdate();
}

MsnhProcessProperty::SerializedData MsnhProcessPropertyDouble::serialize() const
{
    return serializeProperty(type(),_widget,_value);
}

void MsnhProcessPropertyDouble::deserialize(const SerializedData &data)
{
    deserializeProperty(data,_widget,_value);
}

MsnhProcessProperty *MsnhProcessPropertyDouble::clone() const
{
    return new MsnhProcessPropertyDouble(*this);
}


MsnhProcessPropertyFloat::MsnhProcessPropertyFloat(MsnhProcess *process, int position, const char* name, const char* title, const char *description, float value, MsnhProcessWidgetType widget, float min, float max):
    MsnhProcessProperty(position,name,title,description,process,widget),
    _value(value),
    _default(value),
    _min(min),
    _max(max)
{}

void MsnhProcessPropertyFloat::setValue(float value)
{
    _value = value;
    //_process->requestUpdate();
}

MsnhProcessProperty::SerializedData MsnhProcessPropertyFloat::serialize() const
{
    return serializeProperty(type(),_widget,_value);
}

void MsnhProcessPropertyFloat::deserialize(const SerializedData &data)
{
    deserializeProperty(data,_widget,_value);
}

MsnhProcessProperty *MsnhProcessPropertyFloat::clone() const
{
    return new MsnhProcessPropertyFloat(*this);
}


MsnhProcessPropertyBool::MsnhProcessPropertyBool(MsnhProcess *process, int position, const char* name, const char* title, const char *description, bool value, MsnhProcessWidgetType widget):
    MsnhProcessProperty(position,name,title,description,process,widget),
    _value(value),
    _default(value)
{}

void MsnhProcessPropertyBool::setValue(bool value)
{
    _value = value;
    //_process->requestUpdate();
}

MsnhProcessProperty::SerializedData MsnhProcessPropertyBool::serialize() const
{
    return serializeProperty(type(),_widget,_value);
}

void MsnhProcessPropertyBool::deserialize(const SerializedData &data)
{
    deserializeProperty(data,_widget,_value);
}

MsnhProcessProperty *MsnhProcessPropertyBool::clone() const
{
    return new MsnhProcessPropertyBool(*this);
}

MsnhProcessPropertyBoolOneShot::MsnhProcessPropertyBoolOneShot(MsnhProcess *process, int position, const char* name, const char* title, const char *description, bool value, MsnhProcessWidgetType widget):
    MsnhProcessPropertyBool(process, position,name,title,description,process,widget)
{}


MsnhProcessPropertyString::MsnhProcessPropertyString(MsnhProcess *process, int position, const char* name, const char* title, const char *description, const std::string &value, MsnhProcessWidgetType widget):
    MsnhProcessProperty(position,name,title,description,process,widget),
    _value(value),
    _default(value)
{}

void MsnhProcessPropertyString::setValue(const std::string &value)
{
    _value = value;
    //_process->requestUpdate();
}

void MsnhProcessPropertyString::setValue(std::string &&value)
{
    _value = std::move(value);
    //_process->requestUpdate();
}

MsnhProcessProperty::SerializedData MsnhProcessPropertyString::serialize() const
{
    return serializeProperty(type(),_widget,_value);
}

void MsnhProcessPropertyString::deserialize(const SerializedData &data)
{
    deserializeProperty(data,_widget,_value);
}

MsnhProcessProperty *MsnhProcessPropertyString::clone() const
{
    return new MsnhProcessPropertyString(*this);
}


MsnhProcessPropertyVectorInt::MsnhProcessPropertyVectorInt(MsnhProcess *process, int position, const char* name, const char* title, const char *description, const std::vector<int> &value, MsnhProcessWidgetType widget):
    MsnhProcessProperty(position,name,title,description,process,widget),
    _value(value)
{
    _default = value;
}

void MsnhProcessPropertyVectorInt::setValue(const std::vector<int> &value)
{
    _value = value;
    //_process->requestUpdate();
}

void MsnhProcessPropertyVectorInt::setValue(std::vector<int> &&value)
{
    _value = std::move(value);
    //_process->requestUpdate();
}

MsnhProcessProperty::SerializedData MsnhProcessPropertyVectorInt::serialize() const
{
    return serializeProperty(type(),_widget,_value);
}

void MsnhProcessPropertyVectorInt::deserialize(const SerializedData &data)
{
    deserializeProperty(data,_widget,_value);
}

MsnhProcessProperty *MsnhProcessPropertyVectorInt::clone() const
{
    return new MsnhProcessPropertyVectorInt(*this);
}



MsnhProcessPropertyVectorDouble::MsnhProcessPropertyVectorDouble(MsnhProcess *process, int position, const char* name, const char* title, const char *description, const std::vector<double> &value, MsnhProcessWidgetType widget):
    MsnhProcessProperty(position,name,title,description,process,widget),
    _value(value),
    _default(value)
{}

void MsnhProcessPropertyVectorDouble::setValue(const std::vector<double> &value)
{
    _value = value;
    //_process->requestUpdate();
}

void MsnhProcessPropertyVectorDouble::setValue(std::vector<double> &&value)
{
    _value = std::move(value);
    //_process->requestUpdate();
}

MsnhProcessProperty::SerializedData MsnhProcessPropertyVectorDouble::serialize() const
{
    return serializeProperty(type(),_widget,_value);
}

void MsnhProcessPropertyVectorDouble::deserialize(const SerializedData &data)
{
    deserializeProperty(data,_widget,_value);
}

MsnhProcessProperty *MsnhProcessPropertyVectorDouble::clone() const
{
    return new MsnhProcessPropertyVectorDouble(*this);
}
