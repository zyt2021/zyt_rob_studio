﻿#include <lib/MsnhOpenCVCamera.h>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

bool MsnhOpenCVCamera::init()
{
    _res = nullptr;

    addProcessPropertyBool("mjpeg","使用MJPEG","",false);
    addProcessPropertyInt("width", "宽度", "", 640, MSNH_WIDGET_SLIDER, 640, 4096);
    addProcessPropertyInt("height", "高度", "", 480, MSNH_WIDGET_SLIDER, 480, 4096);
    listCameras();
    _mat = cv::Mat::zeros(getProcessPropertyInt("height"),getProcessPropertyInt("width"),CV_8SC3);
    return true;
}

bool MsnhOpenCVCamera::resetParams()
{
    _cap.release();

    if(_cap.open(getProcessPropertyInt("name")))
    {
        _cap.set(cv::CAP_PROP_FRAME_WIDTH,getProcessPropertyInt("width"));
        _cap.set(cv::CAP_PROP_FRAME_HEIGHT,getProcessPropertyInt("height"));
//        _cap.set(cv::CAP_PROP_FPS,30);
//        int fourcc = cv::VideoWriter::fourcc('M','J','P','G');
//        _cap.set(cv::CAP_PROP_FOURCC,fourcc);
        _mat = cv::Mat::zeros(getProcessPropertyInt("height"),getProcessPropertyInt("width"),CV_8SC3);
        return true;
    }
    else
    {
        return false;
    }
}

void MsnhOpenCVCamera::refresh()
{
    listCameras();
}

void MsnhOpenCVCamera::listCameras()
{
    int num = Msnhnet::VideoCapture::listCameras();

    std::string camStrings = "相机:";

    for (int i = 0; i < num; ++i)
    {
        camStrings += Msnhnet::VideoCapture::getCameraName(i)+"|";
    }
    addProcessPropertyInt("name", strdup(camStrings.c_str()), "", 0, MSNH_WIDGET_COMBOBOX);
}

void MsnhOpenCVCamera::destroy()
{
    _cap.release();

    if(_res)
    {
        delete _res;
        _res = nullptr;
    }

    delete this;
}

bool MsnhOpenCVCamera::processInputData(MsnhProtocal::Base *data)
{
    (void)data;
    if(_cap.isOpened())
    {
        if(!_res)
        {
            _res = new MsnhProtocal::MsnhImage();
        }

        try
        {
            if(_cap.read(_mat)) //解决读取失败问题
            {
                _res->mat = _mat; //如果读取失败,直接返回历史结果
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (cv::Exception& ex)
        {
            std::cout<<ex.what()<<std::endl; //TODO: 是否返回给界面LOG
            return false;
        }
    }
    else
    {
        return false;
    }
}

MsnhProtocal::Base *MsnhOpenCVCamera::getResultData()
{
    return _res;
}
