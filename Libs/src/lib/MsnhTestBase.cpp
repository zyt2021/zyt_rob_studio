﻿#include <lib/MsnhTestBase.h>
bool MsnhTestBase::init()
{
    return true;
}

bool MsnhTestBase::resetParams()
{
    return true;
}

void MsnhTestBase::destroy()
{
    delete this;
}

bool MsnhTestBase::processInputData(MsnhProtocal::Base *data)
{
    if(data->getTypeID()==MsnhProtocal::Ping::getID())
    {
        std::cout<<"ping test: "<<(int)data->toPing()->getDataError()<<std::endl;
        return true;
    }
    else
    {
        return false;
    }
}

MsnhProtocal::Base *MsnhTestBase::getResultData()
{
    return nullptr;
}
