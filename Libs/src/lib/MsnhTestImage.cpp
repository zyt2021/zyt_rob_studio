﻿#include <lib/MsnhTestImage.h>
bool MsnhTestImage::init()
{
    _res = nullptr;
    return true;
}

bool MsnhTestImage::resetParams()
{
    return true;
}

void MsnhTestImage::destroy()
{
    if(_res)
    {
        delete _res;
        _res = nullptr;
    }
    delete this;
}

bool MsnhTestImage::processInputData(MsnhProtocal::Base *data)
{
    if(data->getSpecialType() && data->getSpecialName() == MsnhProtocal::MsnhImage::getName())
    {
        cv::Mat mat = ((MsnhProtocal::MsnhImage*)data)->mat.clone();
        cv::cvtColor(mat,mat,cv::COLOR_RGB2BGR);

        if(_res == nullptr)
        {
            _res = new MsnhProtocal::MsnhImage();
        }

        _res->mat = std::move(mat);

        return true;
    }
    else
    {
        return false;
    }
}

MsnhProtocal::Base *MsnhTestImage::getResultData()
{
    return _res;
}
