project(MsnhRGB2Gray)

include_directories("../../MsnhLibs/include")
include_directories("D:/libs/MsnhProto/include")

find_package(OpenCV)


set(MsnhRGB2Gray_Headers
    MsnhRGB2Gray.h
    )

set(MsnhRGB2Gray_SOURCES
    MsnhRGB2Gray.cpp
    plugin.cpp
    )

set(MsnhLibs_LIB G:/Msnh/MsnhProject/MsnhRobotStudio/build-MsnhRobotStudio210930-Desktop_Qt_5_12_9_MSVC2017_64bit-Release/bin/Release/x64/MsnhLibs.lib)
set(MsnhProto_LIB D:/libs/MsnhProto/lib/MsnhProto.lib)

add_library(MsnhRGB2Gray SHARED
            ${MsnhRGB2Gray_Headers}
            ${MsnhRGB2Gray_SOURCES}
            ${OpenCV_INCLUDE_DIRS}
            )
target_link_libraries(MsnhRGB2Gray ${OpenCV_LIBS} ${MsnhLibs_LIB} ${MsnhProto_LIB})
