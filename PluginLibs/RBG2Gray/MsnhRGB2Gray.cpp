#include "MsnhRGB2Gray.h"

bool MsnhRGB2Gray::init()
{
    _res = nullptr;
    return true;
}

void MsnhRGB2Gray::destroy()
{
    if(_res)
    {
        delete _res;
        _res = nullptr;
    }
}

bool MsnhRGB2Gray::processInputData(MsnhProtocal::Base *data)
{
    if(data->getSpecialType() && data->getSpecialName() == MsnhProtocal::MsnhImage::getName())
    {
        cv::Mat mat = ((MsnhProtocal::MsnhImage*)data)->mat.clone();
        cv::cvtColor(mat,mat,cv::COLOR_RGB2GRAY);

        if(_res == nullptr)
        {
            _res = new MsnhProtocal::MsnhImage();
        }

        _res->mat = std::move(mat);

        return true;
    }
    else
    {
        return false;
    }
}

MsnhProtocal::Base *MsnhRGB2Gray::getResultData()
{
    return _res;
}
