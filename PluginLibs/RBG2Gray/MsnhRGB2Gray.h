#ifndef MSNHRGB2GRAY_H
#define MSNHRGB2GRAY_H

#include <MsnhProcess.h>
#include <MsnhImage.h>
#include <iostream>

class MsnhRGB2Gray : public MsnhClonableProcess<MsnhRGB2Gray>
{
public:
    MsnhRGB2Gray(): MsnhClonableProcess(){_category = CATEGORY_IMAGE_PROC;}
    ~MsnhRGB2Gray() {destroy();}
    bool init();
    void destroy();
    bool processInputData(MsnhProtocal::Base* data);
    MsnhProtocal::Base *getResultData();

private:
    MsnhProtocal::MsnhImage* _res;
};

class MsnhRGB2GrayDriver : public MsnhProcessDriver
{
public:
    MsnhRGB2GrayDriver() : MsnhProcessDriver("MsnhRGB2GrayDriver", MsnhRGB2Gray::version) {}
    MsnhProcess* create()    { return new MsnhRGB2Gray(); }
    std::string className()  { return "MsnhRGB2Gray"; }
    std::string author()     { return "msnh"; }
    int         version()    { return 2; }
};
#endif // MSNHRGB2GRAY_H
