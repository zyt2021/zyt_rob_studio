#include "MsnhProtoDef.h"
//============================ -ping-
uint32_t ping::Info::typeID = 1;
std::string ping::Info::typeName = "ping";
std::string ping::Data::errorU8 = "error";
std::string ping::Cmd::fuckU8 = "fuck";
//============================

//============================ -robotJoints-
uint32_t robotJoints::Info::typeID = 2;
std::string robotJoints::Info::typeName = "robotJoints";
std::string robotJoints::Data::jointsF32Vec = "joints";
//============================

//============================ -pointsCloud-
uint32_t pointsCloud::Info::typeID = 3;
std::string pointsCloud::Info::typeName = "pointsCloud";
std::string pointsCloud::Data::xF32Vec = "x";
std::string pointsCloud::Data::yF32Vec = "y";
std::string pointsCloud::Data::zF32Vec = "z";
std::string pointsCloud::Data::rF32Vec = "r";
std::string pointsCloud::Data::gF32Vec = "g";
std::string pointsCloud::Data::bF32Vec = "b";
//============================

//============================ -testProto-
uint32_t testProto::Info::typeID = 999999999;
std::string testProto::Info::typeName = "testProto";
const float testProto::Const::piF32 = 3.1415926f;
const double testProto::Const::_piF64 = 3.141592611231321312;
std::string testProto::Data::uu8U8 = "uu8";
std::string testProto::Data::ss8S8 = "ss8";
std::string testProto::Data::uu16U16 = "uu16";
std::string testProto::Data::ss16S16 = "ss16";
std::string testProto::Data::uu32U32 = "uu32";
std::string testProto::Data::ss32S32 = "ss32";
std::string testProto::Data::uu64U64 = "uu64";
std::string testProto::Data::ss64S64 = "ss64";
std::string testProto::Data::ff32F32 = "ff32";
std::string testProto::Data::ff64F64 = "ff64";
std::string testProto::Data::sstrStr = "sstr";
std::string testProto::Data::u8VecU8Vec = "u8Vec";
std::string testProto::Data::s8VecS8Vec = "s8Vec";
std::string testProto::Data::u16VecU16Vec = "u16Vec";
std::string testProto::Data::s16VecS16Vec = "s16Vec";
std::string testProto::Data::u32VecU32Vec = "u32Vec";
std::string testProto::Data::s32VecS32Vec = "s32Vec";
std::string testProto::Data::u64VecU64Vec = "u64Vec";
std::string testProto::Data::s64VecS64Vec = "s64Vec";
std::string testProto::Data::f32VecF32Vec = "f32Vec";
std::string testProto::Data::f64VecF64Vec = "f64Vec";
//============================

