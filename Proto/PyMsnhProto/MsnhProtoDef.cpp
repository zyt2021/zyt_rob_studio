#include "MsnhProtoDef.h"
//============================ -TestProto-
uint32_t TestProto::Info::typeID = 0;
std::string TestProto::Info::typeName = "TestProto";
const float TestProto::Const::piF32 = 3.1415926f;
const double TestProto::Const::_piF64 = 3.141592611231321312;
std::string TestProto::Data::uu8U8 = "uu8";
std::string TestProto::Data::ss8S8 = "ss8";
std::string TestProto::Data::uu16U16 = "uu16";
std::string TestProto::Data::ss16S16 = "ss16";
std::string TestProto::Data::uu32U32 = "uu32";
std::string TestProto::Data::ss32S32 = "ss32";
std::string TestProto::Data::uu64U64 = "uu64";
std::string TestProto::Data::ss64S64 = "ss64";
std::string TestProto::Data::ff32F32 = "ff32";
std::string TestProto::Data::ff64F64 = "ff64";
std::string TestProto::Data::sstrStr = "sstr";
std::string TestProto::Data::u8VecU8Vec = "u8Vec";
std::string TestProto::Data::s8VecS8Vec = "s8Vec";
std::string TestProto::Data::u16VecU16Vec = "u16Vec";
std::string TestProto::Data::s16VecS16Vec = "s16Vec";
std::string TestProto::Data::u32VecU32Vec = "u32Vec";
std::string TestProto::Data::s32VecS32Vec = "s32Vec";
std::string TestProto::Data::u64VecU64Vec = "u64Vec";
std::string TestProto::Data::s64VecS64Vec = "s64Vec";
std::string TestProto::Data::f32VecF32Vec = "f32Vec";
std::string TestProto::Data::f64VecF64Vec = "f64Vec";
//============================

//============================ -Ping-
uint32_t Ping::Info::typeID = 1;
std::string Ping::Info::typeName = "Ping";
std::string Ping::Data::errorU8 = "error";
//============================

//============================ -RobotJoints-
uint32_t RobotJoints::Info::typeID = 2;
std::string RobotJoints::Info::typeName = "RobotJoints";
std::string RobotJoints::Data::jointsF32Vec = "joints";
//============================

//============================ -PointsCloud-
uint32_t PointsCloud::Info::typeID = 3;
std::string PointsCloud::Info::typeName = "PointsCloud";
std::string PointsCloud::Data::xF32Vec = "x";
std::string PointsCloud::Data::yF32Vec = "y";
std::string PointsCloud::Data::zF32Vec = "z";
std::string PointsCloud::Data::rF32Vec = "r";
std::string PointsCloud::Data::gF32Vec = "g";
std::string PointsCloud::Data::bF32Vec = "b";
//============================

//============================ -Special-
uint32_t Special::Info::typeID = 999999999;
std::string Special::Info::typeName = "Special";
//============================

