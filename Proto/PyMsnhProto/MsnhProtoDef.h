#ifndef MSNH_PROTO_H
#define MSNH_PROTO_H

#include <string>
#include <stdint.h>

//============================ -TestProto-
namespace TestProto
{
class Info
{
public:
  static uint32_t typeID;
  static std::string typeName;
};
class Const
{
public:
  const static float piF32;
  const static double _piF64;
  enum axis
   {
       TestProto_x,
       TestProto_y,
       TestProto_z,
   };
};
class Data
{
public:
  static std::string uu8U8;
  static std::string ss8S8;
  static std::string uu16U16;
  static std::string ss16S16;
  static std::string uu32U32;
  static std::string ss32S32;
  static std::string uu64U64;
  static std::string ss64S64;
  static std::string ff32F32;
  static std::string ff64F64;
  static std::string sstrStr;
  static std::string u8VecU8Vec;
  static std::string s8VecS8Vec;
  static std::string u16VecU16Vec;
  static std::string s16VecS16Vec;
  static std::string u32VecU32Vec;
  static std::string s32VecS32Vec;
  static std::string u64VecU64Vec;
  static std::string s64VecS64Vec;
  static std::string f32VecF32Vec;
  static std::string f64VecF64Vec;
};
class Cmd
{
public:
};
}
//============================

//============================ -Ping-
namespace Ping
{
class Info
{
public:
  static uint32_t typeID;
  static std::string typeName;
};
class Const
{
public:
};
class Data
{
public:
  static std::string errorU8;
};
class Cmd
{
public:
};
}
//============================

//============================ -RobotJoints-
namespace RobotJoints
{
class Info
{
public:
  static uint32_t typeID;
  static std::string typeName;
};
class Const
{
public:
};
class Data
{
public:
  static std::string jointsF32Vec;
};
class Cmd
{
public:
};
}
//============================

//============================ -PointsCloud-
namespace PointsCloud
{
class Info
{
public:
  static uint32_t typeID;
  static std::string typeName;
};
class Const
{
public:
};
class Data
{
public:
  static std::string xF32Vec;
  static std::string yF32Vec;
  static std::string zF32Vec;
  static std::string rF32Vec;
  static std::string gF32Vec;
  static std::string bF32Vec;
};
class Cmd
{
public:
};
}
//============================

//============================ -Special-
namespace Special
{
class Info
{
public:
  static uint32_t typeID;
  static std::string typeName;
};
class Const
{
public:
};
class Data
{
public:
};
class Cmd
{
public:
};
}
//============================



#endif