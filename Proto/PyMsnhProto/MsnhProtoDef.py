# ============================ -TestProto-
class TestProto:
   class Info:
      typeID = 0
      typeName = "TestProto"
   class Const:
      piF32 = 3.1415926
      _piF64 = 3.141592611231321312
      class axis:
          TestProto_x=0
          TestProto_y=1
          TestProto_z=2
      pass
   class Data:
      uu8U8 = "uu8"
      ss8S8 = "ss8"
      uu16U16 = "uu16"
      ss16S16 = "ss16"
      uu32U32 = "uu32"
      ss32S32 = "ss32"
      uu64U64 = "uu64"
      ss64S64 = "ss64"
      ff32F32 = "ff32"
      ff64F64 = "ff64"
      sstrStr = "sstr"
      u8VecU8Vec = "u8Vec"
      s8VecS8Vec = "s8Vec"
      u16VecU16Vec = "u16Vec"
      s16VecS16Vec = "s16Vec"
      u32VecU32Vec = "u32Vec"
      s32VecS32Vec = "s32Vec"
      u64VecU64Vec = "u64Vec"
      s64VecS64Vec = "s64Vec"
      f32VecF32Vec = "f32Vec"
      f64VecF64Vec = "f64Vec"
      pass
   class Cmd:
      pass
# ============================

# ============================ -Ping-
class Ping:
   class Info:
      typeID = 1
      typeName = "Ping"
   class Const:
      pass
   class Data:
      errorU8 = "error"
      pass
   class Cmd:
      pass
# ============================

# ============================ -RobotJoints-
class RobotJoints:
   class Info:
      typeID = 2
      typeName = "RobotJoints"
   class Const:
      pass
   class Data:
      jointsF32Vec = "joints"
      pass
   class Cmd:
      pass
# ============================

# ============================ -PointsCloud-
class PointsCloud:
   class Info:
      typeID = 3
      typeName = "PointsCloud"
   class Const:
      pass
   class Data:
      xF32Vec = "x"
      yF32Vec = "y"
      zF32Vec = "z"
      rF32Vec = "r"
      gF32Vec = "g"
      bF32Vec = "b"
      pass
   class Cmd:
      pass
# ============================

# ============================ -Special-
class Special:
   class Info:
      typeID = 999999999
      typeName = "Special"
   class Const:
      pass
   class Data:
      pass
   class Cmd:
      pass
# ============================

