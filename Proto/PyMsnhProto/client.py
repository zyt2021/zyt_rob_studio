# -*- coding: utf-8 -*- 
import socket
from MsnhProto import *
from MsnhProtoDef import *

PyMsnhProtoInit.initLib() #初始化MsnhProto库
getTimeCMD = PyMsnhProto(name="getTime",type=PyMsnhProto.CMD) #申明一个CMD模式的getTime协议

getTimeCMD.setU8(getTime.Cmd.typeU8,12) #12小时制

frame = PyMsnhProto() #申明一个空协议

s = socket.socket()
s.connect(('127.0.0.1',9999))  #连接服务端

while True:#
	datas = getTimeCMD.serialize()
	s.send(bytes(datas)) #发送数据
	recv = s.recv(1024)
	frame.fromBytes(list(recv))

	ampm = frame.getStr(getTime.Data.AMPMStr)
	hor  = frame.getU8(getTime.Data.hourU8)
	min  = frame.getU8(getTime.Data.minuteU8)
	sec  = frame.getU8(getTime.Data.secondU8)

	print(ampm.decode(),hor,":",min,":",sec)
s.close()