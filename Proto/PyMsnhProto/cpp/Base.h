#ifndef MSNH_PROTO_BASE_H
#define MSNH_PROTO_BASE_H

#include <string>
#include <stdint.h>
#include <MsnhProto/MsnhProtoManager.h>


namespace MsnhProtocal
{
class TestProto;
class Ping;
class RobotJoints;
class PointsCloud;
class Special;
class Base
{
public:
    Base():_manager(MsnhProtoManager(0,MsnhProtoType::DEFUALT)){}
    Base(uint32_t typeID,MsnhProtoType type):_manager(MsnhProtoManager(typeID,type)){}
    uint32_t getTypeID() {return _manager.getTypeID();}
    std::string getTypeName() {return _manager.getTypeName();}
    virtual void fromBytes(const std::vector<uint8_t> &bytes){_manager.fromBytes(bytes);}
    std::vector<uint8_t> encode(){return _manager.serialize();}
    bool isData(){return _manager.getFrameType() == MsnhProtoType::IS_DATA;}
    bool isCMD(){return _manager.getFrameType() == MsnhProtoType::IS_CMD;}
    bool getSpecialType() const{return _specialType;}
    std::string getSpecialName() const{return _specialName;}

    TestProto* toTestProto(){if(getTypeID()==0){return (TestProto*)(this);}else{return nullptr;}}
    Ping* toPing(){if(getTypeID()==1){return (Ping*)(this);}else{return nullptr;}}
    RobotJoints* toRobotJoints(){if(getTypeID()==2){return (RobotJoints*)(this);}else{return nullptr;}}
    PointsCloud* toPointsCloud(){if(getTypeID()==3){return (PointsCloud*)(this);}else{return nullptr;}}
    Special* toSpecial(){if(getTypeID()==999999999){return (Special*)(this);}else{return nullptr;}}
protected:
    bool _specialType = false;
    std::string _specialName = "NULL";
    MsnhProtoManager _manager;
};

}
#endif
