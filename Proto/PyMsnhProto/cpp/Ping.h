#ifndef MSNH_PROTO_PING_H
#define MSNH_PROTO_PING_H

#include <string>
#include <stdint.h>
#include <MsnhProto/MsnhProtoManager.h>
#include "Base.h"

namespace MsnhProtocal
{
class Ping: public Base
{
public:
    Ping(MsnhProtoType type):Base(1,type){}
    static uint32_t getID(){return 1;}
    static std::string getName(){return "Ping";}
    ///Do not use
    virtual void fromBytes(const std::vector<uint8_t> &bytes) override{(void)bytes;}
    //error
    uint8_t getDataError() {return  _manager.getU8("error");}
    void setDataError(const uint8_t& value) { _manager.setU8("error",value);}

};
}


#endif