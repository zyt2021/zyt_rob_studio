#ifndef MSNH_PROTO_POINTSCLOUD_H
#define MSNH_PROTO_POINTSCLOUD_H

#include <string>
#include <stdint.h>
#include <MsnhProto/MsnhProtoManager.h>
#include "Base.h"

namespace MsnhProtocal
{
class PointsCloud: public Base
{
public:
    PointsCloud(MsnhProtoType type):Base(3,type){}
    static uint32_t getID(){return 3;}
    static std::string getName(){return "PointsCloud";}
    ///Do not use
    virtual void fromBytes(const std::vector<uint8_t> &bytes) override{(void)bytes;}
    //x
    std::vector<float> getDataX() {return  _manager.getF32Vec("x");}
    void setDataX(const std::vector<float>& value) { _manager.setF32Vec("x",value);}
    //y
    std::vector<float> getDataY() {return  _manager.getF32Vec("y");}
    void setDataY(const std::vector<float>& value) { _manager.setF32Vec("y",value);}
    //z
    std::vector<float> getDataZ() {return  _manager.getF32Vec("z");}
    void setDataZ(const std::vector<float>& value) { _manager.setF32Vec("z",value);}
    //r
    std::vector<float> getDataR() {return  _manager.getF32Vec("r");}
    void setDataR(const std::vector<float>& value) { _manager.setF32Vec("r",value);}
    //g
    std::vector<float> getDataG() {return  _manager.getF32Vec("g");}
    void setDataG(const std::vector<float>& value) { _manager.setF32Vec("g",value);}
    //b
    std::vector<float> getDataB() {return  _manager.getF32Vec("b");}
    void setDataB(const std::vector<float>& value) { _manager.setF32Vec("b",value);}

};
}


#endif