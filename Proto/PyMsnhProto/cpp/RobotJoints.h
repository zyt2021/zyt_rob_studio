#ifndef MSNH_PROTO_ROBOTJOINTS_H
#define MSNH_PROTO_ROBOTJOINTS_H

#include <string>
#include <stdint.h>
#include <MsnhProto/MsnhProtoManager.h>
#include "Base.h"

namespace MsnhProtocal
{
class RobotJoints: public Base
{
public:
    RobotJoints(MsnhProtoType type):Base(2,type){}
    static uint32_t getID(){return 2;}
    static std::string getName(){return "RobotJoints";}
    ///Do not use
    virtual void fromBytes(const std::vector<uint8_t> &bytes) override{(void)bytes;}
    //joints
    std::vector<float> getDataJoints() {return  _manager.getF32Vec("joints");}
    void setDataJoints(const std::vector<float>& value) { _manager.setF32Vec("joints",value);}

};
}


#endif