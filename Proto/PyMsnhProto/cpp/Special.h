#ifndef MSNH_PROTO_SPECIAL_H
#define MSNH_PROTO_SPECIAL_H

#include <string>
#include <stdint.h>
#include <MsnhProto/MsnhProtoManager.h>
#include "Base.h"

namespace MsnhProtocal
{
class Special: public Base
{
public:
    Special(MsnhProtoType type):Base(999999999,type){}
    static uint32_t getID(){return 999999999;}
    static std::string getName(){return "Special";}
    ///Do not use
    virtual void fromBytes(const std::vector<uint8_t> &bytes) override{(void)bytes;}

};
}


#endif