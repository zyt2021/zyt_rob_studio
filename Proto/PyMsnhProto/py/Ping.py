import os
import sys
current_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(current_dir)
from Base import Base
class Ping(Base):
    def __init__(self,type=1):
        super().__init__(1,type)
    @classmethod
    def getID(self):
        return 1 
    @classmethod
    def getName(self):
        return 'Ping' 
    def fromBytes(self,data):
        b = Base()
        b.fromBytes(data)
        if b.getTypeID() == 1:
            return self._manager.fromBytes(data)
        else:
            raise Exception("Not a Ping ")

    #error
    def getDataError(self):
        return  self._manager.getU8("error")
    def setDataError(self,value):
        self._manager.setU8("error",value)

