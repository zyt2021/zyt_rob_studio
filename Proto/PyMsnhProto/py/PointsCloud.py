import os
import sys
current_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(current_dir)
from Base import Base
class PointsCloud(Base):
    def __init__(self,type=1):
        super().__init__(1,type)
    @classmethod
    def getID(self):
        return 3 
    @classmethod
    def getName(self):
        return 'PointsCloud' 
    def fromBytes(self,data):
        b = Base()
        b.fromBytes(data)
        if b.getTypeID() == 3:
            return self._manager.fromBytes(data)
        else:
            raise Exception("Not a PointsCloud ")

    #x
    def getDataX(self):
        return  self._manager.getF32Vec("x")
    def setDataX(self,value):
        self._manager.setF32Vec("x",value)
    #y
    def getDataY(self):
        return  self._manager.getF32Vec("y")
    def setDataY(self,value):
        self._manager.setF32Vec("y",value)
    #z
    def getDataZ(self):
        return  self._manager.getF32Vec("z")
    def setDataZ(self,value):
        self._manager.setF32Vec("z",value)
    #r
    def getDataR(self):
        return  self._manager.getF32Vec("r")
    def setDataR(self,value):
        self._manager.setF32Vec("r",value)
    #g
    def getDataG(self):
        return  self._manager.getF32Vec("g")
    def setDataG(self,value):
        self._manager.setF32Vec("g",value)
    #b
    def getDataB(self):
        return  self._manager.getF32Vec("b")
    def setDataB(self,value):
        self._manager.setF32Vec("b",value)

