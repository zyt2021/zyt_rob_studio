import os
import sys
current_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(current_dir)
from Base import Base
class RobotJoints(Base):
    def __init__(self,type=1):
        super().__init__(1,type)
    @classmethod
    def getID(self):
        return 2 
    @classmethod
    def getName(self):
        return 'RobotJoints' 
    def fromBytes(self,data):
        b = Base()
        b.fromBytes(data)
        if b.getTypeID() == 2:
            return self._manager.fromBytes(data)
        else:
            raise Exception("Not a RobotJoints ")

    #joints
    def getDataJoints(self):
        return  self._manager.getF32Vec("joints")
    def setDataJoints(self,value):
        self._manager.setF32Vec("joints",value)

