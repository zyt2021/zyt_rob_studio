import os
import sys
current_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(current_dir)
from Base import Base
class Special(Base):
    def __init__(self,type=1):
        super().__init__(1,type)
    @classmethod
    def getID(self):
        return 999999999 
    @classmethod
    def getName(self):
        return 'Special' 
    def fromBytes(self,data):
        b = Base()
        b.fromBytes(data)
        if b.getTypeID() == 999999999:
            return self._manager.fromBytes(data)
        else:
            raise Exception("Not a Special ")


