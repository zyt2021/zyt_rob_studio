# -*- coding: utf-8 -*- 
import socket
from MsnhProto import *
from MsnhProtoDef import *
import datetime

PyMsnhProtoInit.initLib()  #初始化MsnhProto库
frame = PyMsnhProto()  #申明一个空协议 
timeFrame = PyMsnhProto(name="getTime",type=PyMsnhProto.DATA) #申明一个DATA模式的getTime协议


sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM) #建立一个tcp/ip scoket
sock.bind(('127.0.0.1',9999)) #绑定端口号
sock.listen(128)#监听,同时能连多少个客户端
while True:
	print('开始等待下一个客户端过来。。。')
	client,addr = sock.accept() #接收到客户端的socket，和地址
	print('接收到 client数据',addr)
	while True:
		#
		data = client.recv(1024)    #获取到客户端的数据
		frame.fromBytes(list(data)) 
		timeType = frame.getU8(getTime.Cmd.typeU8)  #时间格式: 12/24

		h = datetime.datetime.now().hour
		m = datetime.datetime.now().minute
		s = datetime.datetime.now().second

		if h > 12:
			timeFrame.setStr(getTime.Data.AMPMStr,"下午")
		else:
			timeFrame.setStr(getTime.Data.AMPMStr,"上午")

		if timeType == 12:
			if h > 12:
				h = h - 12

		timeFrame.setU8(getTime.Data.hourU8, h)
		timeFrame.setU8(getTime.Data.minuteU8, m)
		timeFrame.setU8(getTime.Data.secondU8, s)

		datas = timeFrame.serialize()
		client.send(bytes(datas))  # 数据
sock.close()
