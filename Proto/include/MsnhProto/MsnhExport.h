﻿#ifndef MSNH_EXPORT_H
#define MSNH_EXPORT_H
#include "MsnhProtoMarco.h"

#ifdef EXPORT_MSNHNET_STATIC
    #define MSNH_PROTO_EXPORT
    #define MSNH_PROTO_IMPORT
#else
    #ifdef WIN32
        #define MSNH_PROTO_EXPORT __declspec(dllexport)
        #define MSNH_PROTO_IMPORT __declspec(dllimport)
    #elif __GNUC__ >= 4 || __clang__
        #define MSNH_PROTO_EXPORT __attribute__((visibility("default")))
        #define MSNH_PROTO_IMPORT __attribute__((visibility("default")))
    #else
        #define MSNH_PROTO_EXPORT
        #define MSNH_PROTO_IMPORT
    #endif
#endif

#ifdef EXPORT_MSNHNET
    #define MSNH_PROTO_API MSNH_PROTO_EXPORT
#else
    #ifdef USE_SHARED_MSNHNET
        #define MSNH_PROTO_API MSNH_PROTO_IMPORT
    #else
        #define MSNH_PROTO_API
    #endif
#endif
#endif
