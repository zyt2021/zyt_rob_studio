﻿#ifndef MSNHPROTO_H
#define MSNHPROTO_H
#include <exception>
#include "MsnhProtoFile.h"
#include "MsnhProtoMd5.h"
#include "MsnhExport.h"

using namespace std;
class MsnhProtoException:public exception
{
public:

    MsnhProtoException (string str)
    {
        err=str;
    }

    MsnhProtoException()
    {
        err="error";
    }


    virtual const char* what () const noexcept
    {
        return err.data();
    }

private:

    string err;
};

class MSNH_PROTO_API MsnhProto
{
public:
    MsnhProto();
    static string getTimeFromFrame(const vector<uint8_t>& data);
    static uint32_t getMilliseconds(const vector<uint8_t>& data);
    static vector<uint8_t> serialize(MsnhProtoFrame& frame);
    static MsnhProtoFrame deserialize(const vector<uint8_t>& data);
    static string typeNamefromTypeID(uint32_t typeID);
};

#endif // MSNHPROTO_H
