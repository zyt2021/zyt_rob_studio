#ifndef MSNHPROTODATATYPE_H
#define MSNHPROTODATATYPE_H
#include <stdint.h>
union Float32
{
    uint8_t bytes[4];
    float data;
};
union Float64
{
    uint8_t bytes[8];
    double data;
};
union UInt16
{
    uint8_t bytes[2];
    uint16_t data;
};

union Int16
{
    uint8_t bytes[2];
    int16_t data;
};

union UInt32
{
    uint8_t bytes[4];
    uint32_t data;
};

union Int32
{
    uint8_t bytes[4];
    int32_t data;
};

union UInt64
{
    uint8_t bytes[8];
    uint64_t data;
};

union Int64
{
    uint8_t bytes[8];
    int64_t data;
};
#endif // MSNHPROTODATATYPE_H
