﻿#ifndef MSNHPROTOFILE_H
#define MSNHPROTOFILE_H

#include <exception>
#include <string>
#include <vector>
#include <map>
#include <streambuf>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "MsnhProtoUtil.h"
#include "MsnhProtoFrame.h"
#include "MsnhExport.h"

#ifdef WIN32
#pragma warning( disable: 4251 )
#endif // WIN32


using namespace std;

class MsnhProtoFileException:public exception
{
public:

    MsnhProtoFileException (string str)
    {
        err=str;
    }

    MsnhProtoFileException()
    {
        err="error";
    }


    virtual const char* what () const noexcept
    {
        return err.data();
    }

private:

    string err;
};

class MSNH_PROTO_API MsnhProtoFile
{
public:
    MsnhProtoFile();
    ///
    /// \brief getProtocolFiles  获取协议文件
    ///
    static vector<string> getProtocolFiles();
    ///
    /// \brief getProtocolFiles  获取协议文件
    ///
    static vector<string> getProtocolFiles(const string& path);
    ///
    /// \brief refreshFrameTypeLib  初始化帧库
    ///
    static void initFrameTypeLib();
    ///
    /// \brief refreshFrameTypeLib  初始化帧库
    ///
    static void initFrameTypeLib(const string& path);
    ///
    /// \brief refreshFrameTypeLib  初始化帧库
    ///
    static void initFrameTypeLib(const std::vector<string>& pathVec);
    ///
    /// \brief refreshFrameTypeLib  初始化帧库
    ///
    static void initFrameTypeLibByContent(const std::vector<string>& contentVec, bool replace = false);
    ///
    /// \brief refreshFrameTypeLib  更新帧库
    /// \param path
    /// \param replace   替换之前的类型
    ///
    static void refreshFrameTypeLib(const string& path, bool replace);

    static bool frameLibInited;

    static vector<string> types;

    static vector<string> typesSingle;

    static map<uint32_t,string> typeID2TypeNameMap;

    static map<string,MsnhProtoFrame> dataFrameLib;

    static map<string,MsnhProtoFrame> cmdFrameLib;

    static bool isPosIntType(const std::string& str);

    static bool isIntType(const std::string& str);

    static bool isfloatType(const std::string& str);

    static bool isStrType(const std::string& str);

private:
    static vector<string> getFiles(const string& cate_dir);
    static int8_t contain(const string &s);
    static int8_t containSingle(const string &s);
    static void getFrameBase(const vector<string>& files, bool replace=false);
};


#endif // MSNHPROTOFILE_H
