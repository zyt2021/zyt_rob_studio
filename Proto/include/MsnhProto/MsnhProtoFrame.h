﻿#ifndef MSNHPROTOFRAME_H
#define MSNHPROTOFRAME_H
#include <string>
#include <map>
#include <vector>
using namespace std;
struct MsnhProtoFrame
{
    uint8_t funcID=0;
    uint32_t typeID = 0;
    string typeName = "";

    map<string,uint8_t>  u8Map;
    map<string,int8_t>   s8Map;
    map<string,uint16_t> u16Map;
    map<string,int16_t>  s16Map;
    map<string,uint32_t> u32Map;
    map<string,int32_t>  s32Map;
    map<string,uint64_t> u64Map;
    map<string,int64_t>  s64Map;
    map<string,float>    f32Map;
    map<string,double>   f64Map;

    map<string,vector<uint8_t>>  u8ListMap;
    map<string,vector<int8_t>>   s8ListMap;
    map<string,vector<uint16_t>> u16ListMap;
    map<string,vector<int16_t>>  s16ListMap;
    map<string,vector<uint32_t>> u32ListMap;
    map<string,vector<int32_t>>  s32ListMap;
    map<string,vector<uint64_t>> u64ListMap;
    map<string,vector<int64_t>>  s64ListMap;
    map<string,vector<float>>    f32ListMap;
    map<string,vector<double>>   f64ListMap;

    map<string,string>   strMap;
    map<string,vector<string>>   strListMap;
};

#endif // MSNHPROTOFRAME_H
