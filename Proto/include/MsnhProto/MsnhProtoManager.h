﻿#ifndef MSNHPROTOMANAGER_H
#define MSNHPROTOMANAGER_H

#include <MsnhProto/MsnhProto.h>

enum MsnhProtoType
{
    DEFUALT=0,  //Create manual
    IS_DATA,    //Is data frame
    IS_CMD      //Is cmd frame
};

class MSNH_PROTO_API MsnhProtoManager
{
public:
    MsnhProtoManager(uint32_t typeID,MsnhProtoType _frameType);
    MsnhProtoManager(string typeName, MsnhProtoType _frameType);
    MsnhProtoManager();
    ~MsnhProtoManager();

    void fromBytes(const vector<uint8_t> &dataVec);
    std::vector<uint8_t> serialize();

    std::string getTypeName();
    uint32_t getTypeID();

    uint8_t getU8(const string& key);
    void setU8(const string& key,uint8_t data);

    int8_t getS8(const string& key);
    void setS8(const string& key,int8_t data);

    uint16_t getU16(const string& key);
    void setU16(const string& key,uint16_t data);

    int16_t getS16(const string& key);
    void setS16(const string& key,int16_t data);

    uint32_t getU32(const string& key);
    void setU32(const string& key,uint32_t data);

    int32_t getS32(const string& key);
    void setS32(const string& key,int32_t data);

    uint64_t getU64(const string& key);
    void setU64(const string& key,uint64_t data);

    int64_t getS64(const string& key);
    void setS64(const string& key,int64_t data);

    float getF32(const string& key);
    void setF32(const string& key,float data);

    double getF64(const string& key);
    void setF64(const string& key,double data);

    vector<uint8_t> getU8Vec(const string& key);
    void setU8Vec(const string& key,const vector<uint8_t> &dataVec);

    vector<int8_t> getS8Vec(const string& key);
    void setS8Vec(const string& key,const vector<int8_t> &dataVec);

    vector<uint16_t> getU16Vec(const string& key);
    void setU16Vec(const string& key,const vector<uint16_t> &dataVec);

    vector<int16_t> getS16Vec(const string& key);
    void setS16Vec(const string& key,const vector<int16_t> &dataVec);

    vector<uint32_t> getU32Vec(const string& key);
    void setU32Vec(const string& key,const vector<uint32_t> &dataVec);

    vector<int32_t> getS32Vec(const string& key);
    void setS32Vec(const string& key,const vector<int32_t> &dataVec);

    vector<uint64_t> getU64Vec(const string& key);
    void setU64Vec(const string& key,const vector<uint64_t> &dataVec);

    vector<int64_t> getS64Vec(const string& key);
    void setS64Vec(const string& key,const vector<int64_t> &dataVec);

    vector<float> getF32Vec(const string& key);
    void setF32Vec(const string& key,const vector<float> &dataVec);

    vector<double> getF64Vec(const string& key);
    void setF64Vec(const string& key,const vector<double> &dataVec);

    string getStr(const string& key);
    void setStr(const string& key,string data);

    vector<string> getStrVec(const string& key);
    void setStrVec(const string& key,const vector<string> &dataVec);

    MsnhProtoFrame getFrame() const;

    MsnhProtoType getFrameType() const;

private:
    MsnhProtoFrame _frame;
    MsnhProtoType  _frameType;
protected:
    friend MsnhProtoFile;
};

#endif // MSNHPROTOMANAGER_H
