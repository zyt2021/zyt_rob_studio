﻿#ifndef MSNHPROTOUTIL_H
#define MSNHPROTOUTIL_H


#include <stdint.h>
#include <vector>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "MsnhProtoDataType.h"
#include "MsnhExport.h"
#define STR_WHITESPACE " \t\n\r\v\f"

using namespace std;

class MsnhProtoUtilException:public exception
{
public:

    MsnhProtoUtilException (string str)
    {
        err=str;
    }

    MsnhProtoUtilException()
    {
        err="error";
    }


    virtual const char* what () const noexcept
    {
        return err.data();
    }

private:

    string err;
};

class MSNH_PROTO_API MsnhProtoUtil
{
public:
    MsnhProtoUtil();
    /*
     * @brief get current dir
     */
    static string getPWD();
    /*
     * @brief get current system time (milliseconds)
     */
    static uint32_t getCurrentTimeMsec();
    /*
     * @brief get current system time (str)
     */
    static string getCurrentTime();
    /*
     * @brief get current system time (by Hex)
     */
    static UInt32 getCurrentTimeMsecHex();
    /*
     * @brief uint32_t to hex
     */
    static vector<uint8_t> msecToHex(uint32_t millis);
    /*
     * @brief delete target char in string
     */
    static string deleteAllMark(string s, const string mark);
    /*
     * @brief delete /r /n /t
     */
    static string trim(string s);
    /*
     * @brief split string by mark
     */
    static vector<string> splitString(const string& s, const string& c);
    /*
     * @brief remove left and right space
     */
    static string trimLeftRight(const string& s);
    /*
     * @brief combine spaces
     */
    static string combineSpace(const string& s);

    static bool nameOK(const string& str);

    static bool isPosInt(const string& str);

    static bool isInt(const string& str);

    static bool isFloat(const string& str);

    static string strReplace(const string &str, const string & strsrc, const string &strdst);

    /// \brief 字符串替换
    static bool replace(std::string& str, const std::string& from, const std::string& to);
};

#endif // MSNHPROTOUTIL_H
