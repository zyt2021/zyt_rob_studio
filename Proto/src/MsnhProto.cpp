﻿#include <MsnhProto/MsnhProto.h>

MsnhProto::MsnhProto()
{

}

string MsnhProto::getTimeFromFrame(const vector<uint8_t> &data)
{
    uint32_t time = getMilliseconds(data);
    string timeStr = "";
    uint8_t h  = static_cast<uint8_t>(time/3600000);
    uint8_t m  = static_cast<uint8_t>(time%3600000/60000);
    uint8_t s  = static_cast<uint8_t>(time%3600000%60000/1000);
    uint8_t ms = static_cast<uint8_t>(time%3600000%60000%1000);

    string hStr = "";
    string mStr = "";
    string sStr = "";
    string msStr = "";

    if(h<10)
    {
        hStr = "0"+to_string(h);
    }
    else
    {
        hStr = to_string(h);
    }

    if(m<10)
    {
        mStr = "0"+to_string(m);
    }
    else
    {
        mStr = to_string(m);
    }

    if(s<10)
    {
        sStr = "0"+to_string(s);
    }
    else
    {
        sStr = to_string(s);
    }

    if(ms<10)
    {
        msStr = "00"+to_string(ms);
    }
    else if(ms>=10 && ms<100)
    {
        msStr = "0"+to_string(ms);
    }
    else
    {
        msStr = to_string(ms);
    }

    timeStr = timeStr + hStr + ":" + mStr +":" + sStr + "." +msStr;
    return timeStr;
}

uint32_t MsnhProto::getMilliseconds(const vector<uint8_t> &data)
{
    return  static_cast<uint32_t>(data[1])
            +(static_cast<uint32_t>(data[2])<<8)
            +(static_cast<uint32_t>(data[3])<<16)
            +(static_cast<uint32_t>(data[4])<<24);
}

vector<uint8_t> MsnhProto::serialize(MsnhProtoFrame &frame)
{
    vector<uint8_t> cmd;
    cmd.push_back(frame.funcID);

    UInt32 time = MsnhProtoUtil::getCurrentTimeMsecHex();
    cmd.push_back(time.bytes[0]);
    cmd.push_back(time.bytes[1]);
    cmd.push_back(time.bytes[2]);
    cmd.push_back(time.bytes[3]);

    UInt32 typeID;
    typeID.data = frame.typeID;
    cmd.push_back(typeID.bytes[0]);
    cmd.push_back(typeID.bytes[1]);
    cmd.push_back(typeID.bytes[2]);
    cmd.push_back(typeID.bytes[3]);
    //u8
    UInt32 u8Size;
    u8Size.data = static_cast<uint32_t>(frame.u8Map.size());

    if(u8Size.data>0)
    {
        cmd.push_back(0x00);                            //标记

        cmd.push_back(u8Size.bytes[0]);                 //u8长度
        cmd.push_back(u8Size.bytes[1]);
        cmd.push_back(u8Size.bytes[2]);
        cmd.push_back(u8Size.bytes[3]);
    }


    map<string, uint8_t>::reverse_iterator   u8Iter;
    for(u8Iter = frame.u8Map.rbegin(); u8Iter != frame.u8Map.rend(); u8Iter++)
    {
        cmd.push_back(u8Iter->second);                    //数据
    }


    //s8
    UInt32 s8Size;
    s8Size.data = static_cast<uint32_t>(frame.s8Map.size());

    if(s8Size.data>0)
    {
        cmd.push_back(0x01);                            //标记

        cmd.push_back(s8Size.bytes[0]);                 //s8长度
        cmd.push_back(s8Size.bytes[1]);
        cmd.push_back(s8Size.bytes[2]);
        cmd.push_back(s8Size.bytes[3]);
    }


    map<string, int8_t>::reverse_iterator   s8Iter;
    for(s8Iter = frame.s8Map.rbegin(); s8Iter != frame.s8Map.rend(); s8Iter++)
    {
        cmd.push_back(static_cast<uint8_t>(s8Iter->second));    //数据
    }


    //u16
    UInt32 u16Size;
    u16Size.data = static_cast<uint32_t>(frame.u16Map.size());

    if(u16Size.data>0)
    {
        cmd.push_back(0x02);                            //标记

        cmd.push_back(u16Size.bytes[0]);                 //u16长度
        cmd.push_back(u16Size.bytes[1]);
        cmd.push_back(u16Size.bytes[2]);
        cmd.push_back(u16Size.bytes[3]);
    }


    map<string, uint16_t>::reverse_iterator   u16Iter;
    for(u16Iter = frame.u16Map.rbegin(); u16Iter != frame.u16Map.rend(); u16Iter++)
    {
        UInt16 temp;
        temp.data = u16Iter->second;
        cmd.push_back(temp.bytes[0]);                    //数据
        cmd.push_back(temp.bytes[1]);
    }

    //s16
    UInt32 s16Size;
    s16Size.data = static_cast<uint32_t>(frame.s16Map.size());

    if(s16Size.data>0)
    {
        cmd.push_back(0x03);                            //标记

        cmd.push_back(s16Size.bytes[0]);                 //s16长度
        cmd.push_back(s16Size.bytes[1]);
        cmd.push_back(s16Size.bytes[2]);
        cmd.push_back(s16Size.bytes[3]);
    }


    map<string, int16_t>::reverse_iterator   s16Iter;
    for(s16Iter = frame.s16Map.rbegin(); s16Iter != frame.s16Map.rend(); s16Iter++)
    {
        Int16 temp;
        temp.data = s16Iter->second;
        cmd.push_back(temp.bytes[0]);                    //数据
        cmd.push_back(temp.bytes[1]);
    }

    //u32
    UInt32 u32Size;
    u32Size.data = static_cast<uint32_t>(frame.u32Map.size());

    if(u32Size.data>0)
    {
        cmd.push_back(0x04);                            //标记

        cmd.push_back(u32Size.bytes[0]);                 //u32长度
        cmd.push_back(u32Size.bytes[1]);
        cmd.push_back(u32Size.bytes[2]);
        cmd.push_back(u32Size.bytes[3]);
    }


    map<string, uint32_t>::reverse_iterator   u32Iter;
    for(u32Iter = frame.u32Map.rbegin(); u32Iter != frame.u32Map.rend(); u32Iter++)
    {
        UInt32 temp;
        temp.data = u32Iter->second;
        cmd.push_back(temp.bytes[0]);                    //数据
        cmd.push_back(temp.bytes[1]);
        cmd.push_back(temp.bytes[2]);
        cmd.push_back(temp.bytes[3]);
    }

    //s32
    UInt32 s32Size;
    s32Size.data = static_cast<uint32_t>(frame.s32Map.size());

    if(s32Size.data>0)
    {
        cmd.push_back(0x05);                            //标记

        cmd.push_back(s32Size.bytes[0]);                 //s32长度
        cmd.push_back(s32Size.bytes[1]);
        cmd.push_back(s32Size.bytes[2]);
        cmd.push_back(s32Size.bytes[3]);
    }


    map<string, int32_t>::reverse_iterator   s32Iter;
    for(s32Iter = frame.s32Map.rbegin(); s32Iter != frame.s32Map.rend(); s32Iter++)
    {
        Int32 temp;
        temp.data = s32Iter->second;
        cmd.push_back(temp.bytes[0]);                    //数据
        cmd.push_back(temp.bytes[1]);
        cmd.push_back(temp.bytes[2]);
        cmd.push_back(temp.bytes[3]);
    }

    //u64
    UInt32 u64Size;
    u64Size.data = static_cast<uint32_t>(frame.u64Map.size());

    if(u64Size.data>0)
    {
        cmd.push_back(0x06);                            //标记

        cmd.push_back(u64Size.bytes[0]);                 //u64长度
        cmd.push_back(u64Size.bytes[1]);
        cmd.push_back(u64Size.bytes[2]);
        cmd.push_back(u64Size.bytes[3]);
    }

    map<string, uint64_t>::reverse_iterator   u64Iter;
    for(u64Iter = frame.u64Map.rbegin(); u64Iter != frame.u64Map.rend(); u64Iter++)
    {
        UInt64 temp;
        temp.data = u64Iter->second;
        cmd.push_back(temp.bytes[0]);                    //数据
        cmd.push_back(temp.bytes[1]);
        cmd.push_back(temp.bytes[2]);
        cmd.push_back(temp.bytes[3]);
        cmd.push_back(temp.bytes[4]);
        cmd.push_back(temp.bytes[5]);
        cmd.push_back(temp.bytes[6]);
        cmd.push_back(temp.bytes[7]);
    }

    //s64
    UInt32 s64Size;
    s64Size.data = static_cast<uint32_t>(frame.s64Map.size());

    if(s64Size.data>0)
    {
        cmd.push_back(0x07);                            //标记

        cmd.push_back(s64Size.bytes[0]);                 //s64长度
        cmd.push_back(s64Size.bytes[1]);
        cmd.push_back(s64Size.bytes[2]);
        cmd.push_back(s64Size.bytes[3]);
    }

    map<string, int64_t>::reverse_iterator   s64Iter;
    for(s64Iter = frame.s64Map.rbegin(); s64Iter != frame.s64Map.rend(); s64Iter++)
    {
        Int64 temp;
        temp.data = s64Iter->second;
        cmd.push_back(temp.bytes[0]);                    //数据
        cmd.push_back(temp.bytes[1]);
        cmd.push_back(temp.bytes[2]);
        cmd.push_back(temp.bytes[3]);
        cmd.push_back(temp.bytes[4]);
        cmd.push_back(temp.bytes[5]);
        cmd.push_back(temp.bytes[6]);
        cmd.push_back(temp.bytes[7]);
    }

    //f32
    UInt32 f32Size;
    f32Size.data = static_cast<uint32_t>(frame.f32Map.size());

    if(f32Size.data>0)
    {
        cmd.push_back(0x08);                            //标记

        cmd.push_back(f32Size.bytes[0]);                 //f32长度
        cmd.push_back(f32Size.bytes[1]);
        cmd.push_back(f32Size.bytes[2]);
        cmd.push_back(f32Size.bytes[3]);
    }


    map<string, float>::reverse_iterator   f32Iter;
    for(f32Iter = frame.f32Map.rbegin(); f32Iter != frame.f32Map.rend(); f32Iter++)
    {
        Float32 temp;
        temp.data = f32Iter->second;
        cmd.push_back(temp.bytes[0]);                    //数据
        cmd.push_back(temp.bytes[1]);
        cmd.push_back(temp.bytes[2]);
        cmd.push_back(temp.bytes[3]);
    }

    //f64
    UInt32 f64Size;
    f64Size.data = static_cast<uint32_t>(frame.f64Map.size());

    if(f64Size.data>0)
    {
        cmd.push_back(0x09);                            //标记

        cmd.push_back(f64Size.bytes[0]);                 //f64长度
        cmd.push_back(f64Size.bytes[1]);
        cmd.push_back(f64Size.bytes[2]);
        cmd.push_back(f64Size.bytes[3]);
    }


    map<string, double>::reverse_iterator   f64Iter;
    for(f64Iter = frame.f64Map.rbegin(); f64Iter != frame.f64Map.rend(); f64Iter++)
    {
        Float64 temp;
        temp.data = f64Iter->second;
        cmd.push_back(temp.bytes[0]);                    //数据
        cmd.push_back(temp.bytes[1]);
        cmd.push_back(temp.bytes[2]);
        cmd.push_back(temp.bytes[3]);
        cmd.push_back(temp.bytes[4]);
        cmd.push_back(temp.bytes[5]);
        cmd.push_back(temp.bytes[6]);
        cmd.push_back(temp.bytes[7]);
    }

    //u8List
    UInt32 u8ListSize;
    u8ListSize.data = static_cast<uint32_t>(frame.u8ListMap.size());

    if(u8ListSize.data>0)
    {
        cmd.push_back(0x0a);                            //标记

        cmd.push_back(u8ListSize.bytes[0]);                 //u8List长度
        cmd.push_back(u8ListSize.bytes[1]);
        cmd.push_back(u8ListSize.bytes[2]);
        cmd.push_back(u8ListSize.bytes[3]);
    }


    map<string, vector<uint8_t>>::reverse_iterator   u8ListIter;
    for(u8ListIter = frame.u8ListMap.rbegin(); u8ListIter != frame.u8ListMap.rend(); u8ListIter++)
    {
        vector<uint8_t> tempList = u8ListIter->second;                    //数据
        UInt32 u8ListLen;
        u8ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(u8ListLen.bytes[0]);                 //u8ListLen长度
        cmd.push_back(u8ListLen.bytes[1]);
        cmd.push_back(u8ListLen.bytes[2]);
        cmd.push_back(u8ListLen.bytes[3]);

        for(uint32_t j=0; j<u8ListLen.data; j++)
        {
            cmd.push_back(tempList[j]);
        }
    }


    //s8List
    UInt32 s8ListSize;
    s8ListSize.data = static_cast<uint32_t>(frame.s8ListMap.size());

    if(s8ListSize.data>0)
    {
        cmd.push_back(0x0b);                            //标记

        cmd.push_back(s8ListSize.bytes[0]);                 //s8List长度
        cmd.push_back(s8ListSize.bytes[1]);
        cmd.push_back(s8ListSize.bytes[2]);
        cmd.push_back(s8ListSize.bytes[3]);
    }


    map<string, vector<int8_t>>::reverse_iterator   s8ListIter;
    for(s8ListIter = frame.s8ListMap.rbegin(); s8ListIter != frame.s8ListMap.rend(); s8ListIter++)
    {
        vector<int8_t> tempList = s8ListIter->second;                    //数据
        UInt32 s8ListLen;
        s8ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(s8ListLen.bytes[0]);                 //s8ListLen长度
        cmd.push_back(s8ListLen.bytes[1]);
        cmd.push_back(s8ListLen.bytes[2]);
        cmd.push_back(s8ListLen.bytes[3]);

        for(uint32_t j=0; j<s8ListLen.data; j++)
        {
            cmd.push_back(static_cast<uint8_t>(tempList[j]));
        }
    }

    //u16List
    UInt32 u16ListSize;
    u16ListSize.data = static_cast<uint32_t>(frame.u16ListMap.size());

    if(u16ListSize.data>0)
    {
        cmd.push_back(0x0c);                            //标记

        cmd.push_back(u16ListSize.bytes[0]);                 //u16List长度
        cmd.push_back(u16ListSize.bytes[1]);
        cmd.push_back(u16ListSize.bytes[2]);
        cmd.push_back(u16ListSize.bytes[3]);
    }


    map<string, vector<uint16_t>>::reverse_iterator   u16ListIter;
    for(u16ListIter = frame.u16ListMap.rbegin(); u16ListIter != frame.u16ListMap.rend(); u16ListIter++)
    {
        vector<uint16_t> tempList = u16ListIter->second;                    //数据
        UInt32 u16ListLen;
        u16ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(u16ListLen.bytes[0]);                 //u16ListLen长度
        cmd.push_back(u16ListLen.bytes[1]);
        cmd.push_back(u16ListLen.bytes[2]);
        cmd.push_back(u16ListLen.bytes[3]);

        for(uint32_t j=0; j<u16ListLen.data; j++)
        {
            UInt16 temp;
            temp.data = tempList[j];
            cmd.push_back(temp.bytes[0]);                    //数据
            cmd.push_back(temp.bytes[1]);
        }
    }

    //s16List
    UInt32 s16ListSize;
    s16ListSize.data = static_cast<uint32_t>(frame.s16ListMap.size());

    if(s16ListSize.data>0)
    {
        cmd.push_back(0x0d);                            //标记

        cmd.push_back(s16ListSize.bytes[0]);                 //s16List长度
        cmd.push_back(s16ListSize.bytes[1]);
        cmd.push_back(s16ListSize.bytes[2]);
        cmd.push_back(s16ListSize.bytes[3]);
    }


    map<string, vector<int16_t>>::reverse_iterator   s16ListIter;
    for(s16ListIter = frame.s16ListMap.rbegin(); s16ListIter != frame.s16ListMap.rend(); s16ListIter++)
    {
        vector<int16_t> tempList = s16ListIter->second;                    //数据
        UInt32 s16ListLen;
        s16ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(s16ListLen.bytes[0]);                 //s16ListLen长度
        cmd.push_back(s16ListLen.bytes[1]);
        cmd.push_back(s16ListLen.bytes[2]);
        cmd.push_back(s16ListLen.bytes[3]);

        for(uint32_t j=0; j<s16ListLen.data; j++)
        {
            Int16 temp;
            temp.data = tempList[j];
            cmd.push_back(temp.bytes[0]);                    //数据
            cmd.push_back(temp.bytes[1]);
        }
    }

    //u32List
    UInt32 u32ListSize;
    u32ListSize.data = static_cast<uint32_t>(frame.u32ListMap.size());

    if(u32ListSize.data>0)
    {
        cmd.push_back(0x0e);                            //标记

        cmd.push_back(u32ListSize.bytes[0]);                 //u32List长度
        cmd.push_back(u32ListSize.bytes[1]);
        cmd.push_back(u32ListSize.bytes[2]);
        cmd.push_back(u32ListSize.bytes[3]);
    }


    map<string, vector<uint32_t>>::reverse_iterator   u32ListIter;
    for(u32ListIter = frame.u32ListMap.rbegin(); u32ListIter != frame.u32ListMap.rend(); u32ListIter++)
    {
        vector<uint32_t> tempList = u32ListIter->second;                    //数据
        UInt32 u32ListLen;
        u32ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(u32ListLen.bytes[0]);                 //u32ListLen长度
        cmd.push_back(u32ListLen.bytes[1]);
        cmd.push_back(u32ListLen.bytes[2]);
        cmd.push_back(u32ListLen.bytes[3]);

        for(uint32_t j=0; j<u32ListLen.data; j++)
        {
            UInt32 temp;
            temp.data = tempList[j];
            cmd.push_back(temp.bytes[0]);                    //数据
            cmd.push_back(temp.bytes[1]);
            cmd.push_back(temp.bytes[2]);
            cmd.push_back(temp.bytes[3]);
        }
    }

    //s32List
    UInt32 s32ListSize;
    s32ListSize.data = static_cast<uint32_t>(frame.s32ListMap.size());

    if(s32ListSize.data>0)
    {
        cmd.push_back(0x0f);                            //标记

        cmd.push_back(s32ListSize.bytes[0]);                 //s32List长度
        cmd.push_back(s32ListSize.bytes[1]);
        cmd.push_back(s32ListSize.bytes[2]);
        cmd.push_back(s32ListSize.bytes[3]);
    }


    map<string, vector<int32_t>>::reverse_iterator   s32ListIter;
    for(s32ListIter = frame.s32ListMap.rbegin(); s32ListIter != frame.s32ListMap.rend(); s32ListIter++)
    {
        vector<int32_t> tempList = s32ListIter->second;                    //数据
        UInt32 s32ListLen;
        s32ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(s32ListLen.bytes[0]);                 //s32ListLen长度
        cmd.push_back(s32ListLen.bytes[1]);
        cmd.push_back(s32ListLen.bytes[2]);
        cmd.push_back(s32ListLen.bytes[3]);

        for(uint32_t j=0; j<s32ListLen.data; j++)
        {
            Int32 temp;
            temp.data = tempList[j];
            cmd.push_back(temp.bytes[0]);                    //数据
            cmd.push_back(temp.bytes[1]);
            cmd.push_back(temp.bytes[2]);
            cmd.push_back(temp.bytes[3]);
        }
    }

    //u64List
    UInt32 u64ListSize;
    u64ListSize.data = static_cast<uint32_t>(frame.u64ListMap.size());

    if(u64ListSize.data>0)
    {
        cmd.push_back(0x10);                            //标记

        cmd.push_back(u64ListSize.bytes[0]);                 //u64List长度
        cmd.push_back(u64ListSize.bytes[1]);
        cmd.push_back(u64ListSize.bytes[2]);
        cmd.push_back(u64ListSize.bytes[3]);
    }


    map<string, vector<uint64_t>>::reverse_iterator   u64ListIter;
    for(u64ListIter = frame.u64ListMap.rbegin(); u64ListIter != frame.u64ListMap.rend(); u64ListIter++)
    {
        vector<uint64_t> tempList = u64ListIter->second;                    //数据
        UInt32 u64ListLen;
        u64ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(u64ListLen.bytes[0]);                 //u64ListLen长度
        cmd.push_back(u64ListLen.bytes[1]);
        cmd.push_back(u64ListLen.bytes[2]);
        cmd.push_back(u64ListLen.bytes[3]);

        for(uint32_t j=0; j<u64ListLen.data; j++)
        {
            UInt64 temp;
            temp.data = tempList[j];
            cmd.push_back(temp.bytes[0]);                    //数据
            cmd.push_back(temp.bytes[1]);
            cmd.push_back(temp.bytes[2]);
            cmd.push_back(temp.bytes[3]);
            cmd.push_back(temp.bytes[4]);
            cmd.push_back(temp.bytes[5]);
            cmd.push_back(temp.bytes[6]);
            cmd.push_back(temp.bytes[7]);
        }
    }

    //s64List
    UInt32 s64ListSize;
    s64ListSize.data = static_cast<uint32_t>(frame.s64ListMap.size());

    if(s64ListSize.data>0)
    {
        cmd.push_back(0x11);                            //标记

        cmd.push_back(s64ListSize.bytes[0]);                 //s64List长度
        cmd.push_back(s64ListSize.bytes[1]);
        cmd.push_back(s64ListSize.bytes[2]);
        cmd.push_back(s64ListSize.bytes[3]);
    }


    map<string, vector<int64_t>>::reverse_iterator   s64ListIter;
    for(s64ListIter = frame.s64ListMap.rbegin(); s64ListIter != frame.s64ListMap.rend(); s64ListIter++)
    {
        vector<int64_t> tempList = s64ListIter->second;                    //数据
        UInt32 s64ListLen;
        s64ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(s64ListLen.bytes[0]);                 //s64ListLen长度
        cmd.push_back(s64ListLen.bytes[1]);
        cmd.push_back(s64ListLen.bytes[2]);
        cmd.push_back(s64ListLen.bytes[3]);

        for(uint32_t j=0; j<s64ListLen.data; j++)
        {
            Int64 temp;
            temp.data = tempList[j];
            cmd.push_back(temp.bytes[0]);                    //数据
            cmd.push_back(temp.bytes[1]);
            cmd.push_back(temp.bytes[2]);
            cmd.push_back(temp.bytes[3]);
            cmd.push_back(temp.bytes[4]);
            cmd.push_back(temp.bytes[5]);
            cmd.push_back(temp.bytes[6]);
            cmd.push_back(temp.bytes[7]);
        }
    }

    //f32List
    UInt32 f32ListSize;
    f32ListSize.data = static_cast<uint32_t>(frame.f32ListMap.size());

    if(f32ListSize.data>0)
    {
        cmd.push_back(0x12);                            //标记

        cmd.push_back(f32ListSize.bytes[0]);                 //f32List长度
        cmd.push_back(f32ListSize.bytes[1]);
        cmd.push_back(f32ListSize.bytes[2]);
        cmd.push_back(f32ListSize.bytes[3]);
    }


    map<string, vector<float>>::reverse_iterator   f32ListIter;
    for(f32ListIter = frame.f32ListMap.rbegin(); f32ListIter != frame.f32ListMap.rend(); f32ListIter++)
    {
        vector<float> tempList = f32ListIter->second;                    //数据
        UInt32 f32ListLen;
        f32ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(f32ListLen.bytes[0]);                 //f32ListLen长度
        cmd.push_back(f32ListLen.bytes[1]);
        cmd.push_back(f32ListLen.bytes[2]);
        cmd.push_back(f32ListLen.bytes[3]);

        for(uint32_t j=0; j<f32ListLen.data; j++)
        {
            Float32 temp;
            temp.data = tempList[j];
            cmd.push_back(temp.bytes[0]);                    //数据
            cmd.push_back(temp.bytes[1]);
            cmd.push_back(temp.bytes[2]);
            cmd.push_back(temp.bytes[3]);
        }
    }

    //f64List
    UInt32 f64ListSize;
    f64ListSize.data = static_cast<uint32_t>(frame.f64ListMap.size());

    if(f64ListSize.data>0)
    {
        cmd.push_back(0x13);                            //标记

        cmd.push_back(f64ListSize.bytes[0]);                 //f64List长度
        cmd.push_back(f64ListSize.bytes[1]);
        cmd.push_back(f64ListSize.bytes[2]);
        cmd.push_back(f64ListSize.bytes[3]);
    }


    map<string, vector<double>>::reverse_iterator   f64ListIter;
    for(f64ListIter = frame.f64ListMap.rbegin(); f64ListIter != frame.f64ListMap.rend(); f64ListIter++)
    {
        vector<double> tempList = f64ListIter->second;                    //数据
        UInt32 f64ListLen;
        f64ListLen.data = static_cast<uint32_t>(tempList.size());
        cmd.push_back(f64ListLen.bytes[0]);                 //f64ListLen长度
        cmd.push_back(f64ListLen.bytes[1]);
        cmd.push_back(f64ListLen.bytes[2]);
        cmd.push_back(f64ListLen.bytes[3]);

        for(uint32_t j=0; j<f64ListLen.data; j++)
        {
            Float64 temp;
            temp.data = tempList[j];
            cmd.push_back(temp.bytes[0]);                    //数据
            cmd.push_back(temp.bytes[1]);
            cmd.push_back(temp.bytes[2]);
            cmd.push_back(temp.bytes[3]);
            cmd.push_back(temp.bytes[4]);
            cmd.push_back(temp.bytes[5]);
            cmd.push_back(temp.bytes[6]);
            cmd.push_back(temp.bytes[7]);
        }
    }

    // single str  |  ♣  |       mult str
    //    1 row    |     |    row1   |   |   row2 ...
    // str ♠ str ♠ |  ♣  | str ♠ str | ♥ | str ♠ str  ...


    //str
    if(!frame.strMap.empty() || !frame.strListMap.empty())
    {

        cmd.push_back(0x14);                            //标记

        map<string, string>::reverse_iterator   strIter;
        for(strIter = frame.strMap.rbegin(); strIter != frame.strMap.rend(); strIter++)
        {
            string temp = strIter->second;
            for(size_t j=0; j<temp.length();j++)
            {
                cmd.push_back(static_cast<uint8_t>(temp[j]));
            }
            cmd.push_back(0x06);//♠
        }

        cmd.push_back(0x05);//♣

        map<string, vector<string>>::reverse_iterator   strListIter;
        for(strListIter = frame.strListMap.rbegin(); strListIter != frame.strListMap.rend(); strListIter++)
        {
            vector<string> strList = strListIter->second;
            for(size_t k=0; k<strList.size(); k++)
            {
                string temp = strList[k];
                for(size_t j=0; j<temp.length();j++)
                {
                    cmd.push_back(static_cast<uint8_t>(temp[j]));
                }
                cmd.push_back(0x06);//♠
            }
            cmd.push_back(0x03);//♥
        }
    }

    MD5 md5(static_cast<uint8_t*>(cmd.data()),static_cast<int>(cmd.size()));

    const uint8_t* md5Get = md5.getDigest16();

    cmd.push_back(md5Get[0]);
    cmd.push_back(md5Get[1]);
    cmd.push_back(md5Get[2]);
    cmd.push_back(md5Get[3]);
    cmd.push_back(md5Get[4]);
    cmd.push_back(md5Get[5]);
    cmd.push_back(md5Get[6]);
    cmd.push_back(md5Get[7]);

    return cmd;
}

MsnhProtoFrame MsnhProto::deserialize(const vector<uint8_t> &data)
{
    MsnhProtoFrame frame;
    vector<uint8_t> tmpData = data;
    vector<uint8_t> md5;

    for(int i=0;i<8;i++)
    {
        tmpData.pop_back();
    }

    size_t len = data.size();

    for(size_t i=0;i<8;i++)
    {
        md5.push_back(data[len-8+i]);
    }

    MD5 md5Get(tmpData.data(),static_cast<int>(tmpData.size()));
    const uint8_t* md5p = md5Get.getDigest16();

    if(md5[0]==md5p[0]&&md5[1]==md5p[1]&&md5[2]==md5p[2]&&md5[3]==md5p[3]&&
            md5[4]==md5p[4]&&md5[5]==md5p[5]&&md5[6]==md5p[6]&&md5[7]==md5p[7])
    {
        size_t p = 0;
        uint8_t funcID = tmpData[p];
        p++;
        p++;
        p++;
        p++;
        UInt32 typeID;
        typeID.bytes[0]=tmpData[++p];
        typeID.bytes[1]=tmpData[++p];
        typeID.bytes[2]=tmpData[++p];
        typeID.bytes[3]=tmpData[++p];
        string typeName = typeNamefromTypeID(typeID.data);

        if(typeName=="null")
        {
            throw MsnhProtoException("typeNamefromTypeID err >> line: " + to_string(__LINE__) +" >> ID: " + to_string(typeID.data));
        }

        if(funcID==0)
        {
            frame = MsnhProtoFile::cmdFrameLib[typeName];
        }
        else if(funcID==1)
        {
            frame = MsnhProtoFile::dataFrameLib[typeName];
        }
        else
        {
            throw MsnhProtoException("binary funcID err >> line: " + to_string(__LINE__));
        }

        uint8_t mark = 0;



        if(!frame.u8Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x00)// u8
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.u8Map.size())
                {
                    throw MsnhProtoException("u8 len err >> line: " + to_string(__LINE__));
                }

                map<string, uint8_t>::reverse_iterator   u8Iter;
                for(u8Iter = frame.u8Map.rbegin(); u8Iter != frame.u8Map.rend(); u8Iter++)
                {
                    u8Iter->second = tmpData[++p];
                }
            }
        }

        if(!frame.s8Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x01)// s8
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.s8Map.size())
                {
                    throw MsnhProtoException("s8 len err >> line: " + to_string(__LINE__));
                }

                map<string, int8_t>::reverse_iterator   s8Iter;
                for(s8Iter = frame.s8Map.rbegin(); s8Iter != frame.s8Map.rend(); s8Iter++)
                {
                    s8Iter->second = tmpData[++p];
                }
            }
        }

        if(!frame.u16Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x02)// u16
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.u16Map.size())
                {
                    throw MsnhProtoException("u16 len err >> line: " + to_string(__LINE__));
                }

                map<string, uint16_t>::reverse_iterator   u16Iter;
                for(u16Iter = frame.u16Map.rbegin(); u16Iter != frame.u16Map.rend(); u16Iter++)
                {
                    UInt16 temp;
                    temp.bytes[0] = tmpData[++p];
                    temp.bytes[1] = tmpData[++p];
                    u16Iter->second = temp.data;
                }
            }
        }

        if(!frame.s16Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x03)// s16
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.s16Map.size())
                {
                    throw MsnhProtoException("s16 len err >> line: " + to_string(__LINE__));
                }

                map<string, int16_t>::reverse_iterator   s16Iter;
                for(s16Iter = frame.s16Map.rbegin(); s16Iter != frame.s16Map.rend(); s16Iter++)
                {
                    Int16 temp;
                    temp.bytes[0] = tmpData[++p];
                    temp.bytes[1] = tmpData[++p];
                    s16Iter->second = temp.data;
                }
            }
        }

        if(!frame.u32Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x04)// u32
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.u32Map.size())
                {
                    throw MsnhProtoException("u32 len err >> line: " + to_string(__LINE__));
                }

                map<string, uint32_t>::reverse_iterator   u32Iter;
                for(u32Iter = frame.u32Map.rbegin(); u32Iter != frame.u32Map.rend(); u32Iter++)
                {
                    UInt32 temp;
                    temp.bytes[0] = tmpData[++p];
                    temp.bytes[1] = tmpData[++p];
                    temp.bytes[2] = tmpData[++p];
                    temp.bytes[3] = tmpData[++p];
                    u32Iter->second = temp.data;
                }
            }
        }

        if(!frame.s32Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x05)// s32
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.s32Map.size())
                {
                    throw MsnhProtoException("s32 len err >> line: " + to_string(__LINE__));
                }

                map<string, int32_t>::reverse_iterator   s32Iter;
                for(s32Iter = frame.s32Map.rbegin(); s32Iter != frame.s32Map.rend(); s32Iter++)
                {
                    Int32 temp;
                    temp.bytes[0] = tmpData[++p];
                    temp.bytes[1] = tmpData[++p];
                    temp.bytes[2] = tmpData[++p];
                    temp.bytes[3] = tmpData[++p];
                    s32Iter->second = temp.data;
                }
            }
        }

        if(!frame.u64Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x06)// u64
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.u64Map.size())
                {
                    throw MsnhProtoException("u64 len err >> line: " + to_string(__LINE__));
                }

                map<string, uint64_t>::reverse_iterator   u64Iter;
                for(u64Iter = frame.u64Map.rbegin(); u64Iter != frame.u64Map.rend(); u64Iter++)
                {
                    UInt64 temp;
                    temp.bytes[0] = tmpData[++p];
                    temp.bytes[1] = tmpData[++p];
                    temp.bytes[2] = tmpData[++p];
                    temp.bytes[3] = tmpData[++p];
                    temp.bytes[4] = tmpData[++p];
                    temp.bytes[5] = tmpData[++p];
                    temp.bytes[6] = tmpData[++p];
                    temp.bytes[7] = tmpData[++p];
                    u64Iter->second = temp.data;
                }
            }
        }

        if(!frame.s64Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x07)// s64
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.s64Map.size())
                {
                    throw MsnhProtoException("s64 len err >> line: " + to_string(__LINE__));
                }

                map<string, int64_t>::reverse_iterator   s64Iter;
                for(s64Iter = frame.s64Map.rbegin(); s64Iter != frame.s64Map.rend(); s64Iter++)
                {
                    Int64 temp;
                    temp.bytes[0] = tmpData[++p];
                    temp.bytes[1] = tmpData[++p];
                    temp.bytes[2] = tmpData[++p];
                    temp.bytes[3] = tmpData[++p];
                    temp.bytes[4] = tmpData[++p];
                    temp.bytes[5] = tmpData[++p];
                    temp.bytes[6] = tmpData[++p];
                    temp.bytes[7] = tmpData[++p];
                    s64Iter->second = temp.data;
                }
            }
        }

        if(!frame.f32Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x08)// f32
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.f32Map.size())
                {
                    throw MsnhProtoException("f32 len err >> line: " + to_string(__LINE__));
                }

                map<string, float>::reverse_iterator   f32Iter;
                for(f32Iter = frame.f32Map.rbegin(); f32Iter != frame.f32Map.rend(); f32Iter++)
                {
                    Float32 temp;
                    temp.bytes[0] = tmpData[++p];
                    temp.bytes[1] = tmpData[++p];
                    temp.bytes[2] = tmpData[++p];
                    temp.bytes[3] = tmpData[++p];
                    f32Iter->second = temp.data;
                }
            }
        }

        if(!frame.f64Map.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x09)// f64
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.f64Map.size())
                {
                    throw MsnhProtoException("f64 len err >> line: " + to_string(__LINE__));
                }

                map<string, double>::reverse_iterator   f64Iter;
                for(f64Iter = frame.f64Map.rbegin(); f64Iter != frame.f64Map.rend(); f64Iter++)
                {
                    Float64 temp;
                    temp.bytes[0] = tmpData[++p];
                    temp.bytes[1] = tmpData[++p];
                    temp.bytes[2] = tmpData[++p];
                    temp.bytes[3] = tmpData[++p];
                    temp.bytes[4] = tmpData[++p];
                    temp.bytes[5] = tmpData[++p];
                    temp.bytes[6] = tmpData[++p];
                    temp.bytes[7] = tmpData[++p];
                    f64Iter->second = temp.data;
                }
            }
        }

        if(!frame.u8ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x0a)// u8List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.u8ListMap.size())
                {
                    throw MsnhProtoException("u8List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<uint8_t>>::reverse_iterator   u8ListIter;
                for(u8ListIter = frame.u8ListMap.rbegin(); u8ListIter != frame.u8ListMap.rend(); u8ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<uint8_t> u8List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        uint8_t tmp = tmpData[++p];
                        u8List.push_back(tmp);
                    }
                    u8ListIter->second = u8List;
                }
            }
        }

        if(!frame.s8ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x0b)// s8List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.s8ListMap.size())
                {
                    throw MsnhProtoException("s8List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<int8_t>>::reverse_iterator   s8ListIter;
                for(s8ListIter = frame.s8ListMap.rbegin(); s8ListIter != frame.s8ListMap.rend(); s8ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<int8_t> s8List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        int8_t tmp = static_cast<int8_t>(tmpData[++p]);
                        s8List.push_back(tmp);
                    }
                    s8ListIter->second = s8List;
                }
            }
        }

        if(!frame.u16ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x0c)// u16List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.u16ListMap.size())
                {
                    throw MsnhProtoException("u16List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<uint16_t>>::reverse_iterator   u16ListIter;
                for(u16ListIter = frame.u16ListMap.rbegin(); u16ListIter != frame.u16ListMap.rend(); u16ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<uint16_t> u16List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        UInt16 tmpUion;
                        tmpUion.bytes[0] = tmpData[++p];
                        tmpUion.bytes[1] = tmpData[++p];

                        u16List.push_back(tmpUion.data);
                    }
                    u16ListIter->second = u16List;
                }
            }
        }

        if(!frame.s16ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x0d)// s16List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.s16ListMap.size())
                {
                    throw MsnhProtoException("s16List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<int16_t>>::reverse_iterator   s16ListIter;
                for(s16ListIter = frame.s16ListMap.rbegin(); s16ListIter != frame.s16ListMap.rend(); s16ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<int16_t> s16List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        Int16 tmpUion;
                        tmpUion.bytes[0] = tmpData[++p];
                        tmpUion.bytes[1] = tmpData[++p];

                        s16List.push_back(tmpUion.data);
                    }
                    s16ListIter->second = s16List;
                }
            }
        }

        if(!frame.u32ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x0e)// u32List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.u32ListMap.size())
                {
                    throw MsnhProtoException("u32List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<uint32_t>>::reverse_iterator   u32ListIter;
                for(u32ListIter = frame.u32ListMap.rbegin(); u32ListIter != frame.u32ListMap.rend(); u32ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<uint32_t> u32List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        UInt32 tmpUion;
                        tmpUion.bytes[0] = tmpData[++p];
                        tmpUion.bytes[1] = tmpData[++p];
                        tmpUion.bytes[2] = tmpData[++p];
                        tmpUion.bytes[3] = tmpData[++p];

                        u32List.push_back(tmpUion.data);
                    }
                    u32ListIter->second = u32List;
                }
            }
        }

        if(!frame.s32ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x0f)// s32List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.s32ListMap.size())
                {
                    throw MsnhProtoException("s32List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<int32_t>>::reverse_iterator   s32ListIter;
                for(s32ListIter = frame.s32ListMap.rbegin(); s32ListIter != frame.s32ListMap.rend(); s32ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<int32_t> s32List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        Int32 tmpUion;
                        tmpUion.bytes[0] = tmpData[++p];
                        tmpUion.bytes[1] = tmpData[++p];
                        tmpUion.bytes[2] = tmpData[++p];
                        tmpUion.bytes[3] = tmpData[++p];

                        s32List.push_back(tmpUion.data);
                    }
                    s32ListIter->second = s32List;
                }
            }
        }

        if(!frame.u64ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x10)// u64List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.u64ListMap.size())
                {
                    throw MsnhProtoException("u64List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<uint64_t>>::reverse_iterator   u64ListIter;
                for(u64ListIter = frame.u64ListMap.rbegin(); u64ListIter != frame.u64ListMap.rend(); u64ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<uint64_t> u64List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        UInt64 tmpUion;
                        tmpUion.bytes[0] = tmpData[++p];
                        tmpUion.bytes[1] = tmpData[++p];
                        tmpUion.bytes[2] = tmpData[++p];
                        tmpUion.bytes[3] = tmpData[++p];
                        tmpUion.bytes[4] = tmpData[++p];
                        tmpUion.bytes[5] = tmpData[++p];
                        tmpUion.bytes[6] = tmpData[++p];
                        tmpUion.bytes[7] = tmpData[++p];

                        u64List.push_back(tmpUion.data);
                    }
                    u64ListIter->second = u64List;
                }
            }
        }

        if(!frame.s64ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x11)// s64List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.s64ListMap.size())
                {
                    throw MsnhProtoException("s64List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<int64_t>>::reverse_iterator   s64ListIter;
                for(s64ListIter = frame.s64ListMap.rbegin(); s64ListIter != frame.s64ListMap.rend(); s64ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<int64_t> s64List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        Int64 tmpUion;
                        tmpUion.bytes[0] = tmpData[++p];
                        tmpUion.bytes[1] = tmpData[++p];
                        tmpUion.bytes[2] = tmpData[++p];
                        tmpUion.bytes[3] = tmpData[++p];
                        tmpUion.bytes[4] = tmpData[++p];
                        tmpUion.bytes[5] = tmpData[++p];
                        tmpUion.bytes[6] = tmpData[++p];
                        tmpUion.bytes[7] = tmpData[++p];

                        s64List.push_back(tmpUion.data);
                    }
                    s64ListIter->second = s64List;
                }
            }
        }

        if(!frame.f32ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x12)// f32List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.f32ListMap.size())
                {
                    throw MsnhProtoException("f32List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<float>>::reverse_iterator   f32ListIter;
                for(f32ListIter = frame.f32ListMap.rbegin(); f32ListIter != frame.f32ListMap.rend(); f32ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<float> f32List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        Float32 tmpUion;
                        tmpUion.bytes[0] = tmpData[++p];
                        tmpUion.bytes[1] = tmpData[++p];
                        tmpUion.bytes[2] = tmpData[++p];
                        tmpUion.bytes[3] = tmpData[++p];

                        f32List.push_back(tmpUion.data);
                    }
                    f32ListIter->second = f32List;
                }
            }
        }

        if(!frame.f64ListMap.empty())
        {
            mark =tmpData[++p];
            if(mark == 0x13)// f64List
            {
                UInt32 len;
                len.bytes[0]=tmpData[++p];
                len.bytes[1]=tmpData[++p];
                len.bytes[2]=tmpData[++p];
                len.bytes[3]=tmpData[++p];

                if(len.data != frame.f64ListMap.size())
                {
                    throw MsnhProtoException("f64List len err >> line: " + to_string(__LINE__));
                }

                map<string, vector<double>>::reverse_iterator   f64ListIter;
                for(f64ListIter = frame.f64ListMap.rbegin(); f64ListIter != frame.f64ListMap.rend(); f64ListIter++)
                {
                    UInt32 listLen;
                    listLen.bytes[0]=tmpData[++p];
                    listLen.bytes[1]=tmpData[++p];
                    listLen.bytes[2]=tmpData[++p];
                    listLen.bytes[3]=tmpData[++p];
                    vector<double> f64List;
                    for(uint32_t m=0; m<listLen.data; m++)
                    {
                        Float64 tmpUion;
                        tmpUion.bytes[0] = tmpData[++p];
                        tmpUion.bytes[1] = tmpData[++p];
                        tmpUion.bytes[2] = tmpData[++p];
                        tmpUion.bytes[3] = tmpData[++p];
                        tmpUion.bytes[4] = tmpData[++p];
                        tmpUion.bytes[5] = tmpData[++p];
                        tmpUion.bytes[6] = tmpData[++p];
                        tmpUion.bytes[7] = tmpData[++p];

                        f64List.push_back(tmpUion.data);
                    }
                    f64ListIter->second = f64List;
                }
            }
        }

        if(!frame.strMap.empty() || !frame.strListMap.empty())
        {
            string getStr = "";
            mark =tmpData[++p];
            if(mark == 0x14)// str
            {
                while(p<tmpData.size()-1)
                {
                    getStr+=char(tmpData[++p]);
                }
                vector<string> multStr = MsnhProtoUtil::splitString(getStr,"\005");//♣

                if(!frame.strMap.empty())
                {
                    vector<string> singleStr = MsnhProtoUtil::splitString(multStr[0],"\006");//♠
                    size_t ii = 0;
                    map<string, string>::reverse_iterator   strIter;
                    for(strIter = frame.strMap.rbegin(); strIter != frame.strMap.rend(); strIter++)
                    {
                        strIter->second =singleStr[ii++];
                    }
                }

                if(!frame.strListMap.empty())
                {
                    vector<string> arrayStr = MsnhProtoUtil::splitString(multStr[1],"\003");//♥


                    map<string, vector<string>>::reverse_iterator   strListIter;
                    size_t jj = 0;
                    for(strListIter = frame.strListMap.rbegin(); strListIter != frame.strListMap.rend(); strListIter++)
                    {
                        strListIter->second =MsnhProtoUtil::splitString(arrayStr[jj++],"\006");//♥
                    }
                }

            }
        }
        return frame;
    }
    else
    {
        throw  MsnhProtoException("md5 check error");
    }

}

string MsnhProto::typeNamefromTypeID(uint32_t typeID)
{
    auto dataLib = MsnhProtoFile::dataFrameLib;

    map<string,MsnhProtoFrame>::reverse_iterator   iter;
    for(iter = dataLib.rbegin(); iter != dataLib.rend(); iter++)
    {
        if(iter->second.typeID==typeID)
        {
            return iter->second.typeName;
        }
    }
    return "null";
}
