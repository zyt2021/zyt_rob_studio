﻿#ifdef _WIN32
#include <winsock2.h>
#include <time.h>
#include <direct.h>
#include <io.h>

#endif
#ifdef __linux__
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>
#endif

#include <MsnhProto/MsnhProtoFile.h>
#include <regex>

bool MsnhProtoFile::frameLibInited = false;
map<string,MsnhProtoFrame> MsnhProtoFile::dataFrameLib = map<string,MsnhProtoFrame>();
map<string,MsnhProtoFrame> MsnhProtoFile::cmdFrameLib = map<string,MsnhProtoFrame>();
map<uint32_t,string> MsnhProtoFile::typeID2TypeNameMap = map<uint32_t,string>();
vector<string>  MsnhProtoFile::types = {"u8",  "s8",  "u16",  "s16",  "u32",  "s32",  "u64", "s64",  "f32",   "f64",  "str",
                                        "u8[]","s8[]","u16[]","s16[]","u32[]","s32[]","u64[]","s64[]","f32[]","f64[]","str[]"};

vector<string>  MsnhProtoFile::typesSingle = {"u8",  "s8",  "u16",  "s16",  "u32",  "s32",  "u64", "s64",  "f32",   "f64",  "str"};
MsnhProtoFile::MsnhProtoFile()
{

}

vector<string> MsnhProtoFile::getProtocolFiles()
{
    string PWD = MsnhProtoUtil::getPWD();
    PWD = PWD+"/msnhproto";
    return getProtocolFiles(PWD);
}

vector<string> MsnhProtoFile::getProtocolFiles(const string &path)
{
    vector<string> fileNames = getFiles(path);
    vector<string> finalFiles;
    for(size_t i=0;i<fileNames.size();i++)
    {

        if(fileNames[i].find(".")==string::npos)
        {
            continue;
        }

        vector<string> split = MsnhProtoUtil::splitString(fileNames[i],".");

        if(split[split.size()-1]=="msnhproto")
        {
#ifdef _WIN32
            finalFiles.push_back(path+"\\"+fileNames[i]);
#endif

#ifdef __linux__
            finalFiles.push_back(path+"/"+fileNames[i]);
#endif
        }

    }

    return finalFiles;
}

vector<string> MsnhProtoFile::getFiles(const string &cate_dir)
{
    vector<string> files;//存放文件名
    string find = cate_dir+"\\*";
#ifdef _WIN32
    _finddata_t file;
    long long lf;
    //输入文件夹路径
    if ((lf=_findfirst(find.c_str(), &file)) == -1) {
        throw MsnhProtoFileException("Open dir error >> dir: " + cate_dir);
    } else {
        while(_findnext(lf, &file) == 0) {
            //输出文件名
            cout<<file.name<<endl;
            if (strcmp(file.name, ".") == 0 || strcmp(file.name, "..") == 0)
                continue;
            files.push_back(file.name);
        }
    }
    _findclose(lf);
#endif

#ifdef __linux__
    DIR *dir;
    struct dirent *ptr;
    //char base[1000]={0};

    if ((dir=opendir(cate_dir.c_str())) == nullptr)
    {
        cout<<("Open dir error...");
        throw MsnhProtoFileException("Open dir error");
    }

    while ((ptr=readdir(dir)) != nullptr)
    {
        if(strcmp(ptr->d_name,".")==0 || strcmp(ptr->d_name,"..")==0)    ///current dir OR parrent dir
            continue;
        else if(ptr->d_type == 8)    ///file
            //printf("d_name:%s/%s\n",basePath,ptr->d_name);
            files.push_back(ptr->d_name);
        else if(ptr->d_type == 10)    ///link file
            //printf("d_name:%s/%s\n",basePath,ptr->d_name);
            continue;
        else if(ptr->d_type == 4)    ///dir
        {
            files.push_back(ptr->d_name);

            /*
                memset(base,'\0',sizeof(base));
                strcpy(base,basePath);
                strcat(base,"/");
                strcat(base,ptr->d_nSame);
                readFileList(base);
            */
        }
    }
    closedir(dir);
#endif
    //排序，按从小到大排序
    sort(files.begin(), files.end());
    return files;
}

int8_t MsnhProtoFile::contain(const string &s)
{
    for(size_t i =0; i<types.size(); i++)
    {
        if(types[i]==s)
        {
            return static_cast<int8_t>(i);
        }
    }
    return -1;
}

int8_t MsnhProtoFile::containSingle(const string &s)
{
    for(size_t i =0; i<typesSingle.size(); i++)
    {
        if(typesSingle[i]==s)
        {
            return static_cast<int8_t>(i);
        }
    }
    return -1;
}

void MsnhProtoFile::getFrameBase(const vector<string> &files, bool replace)
{
    if(!(files.size()>0))
        throw MsnhProtoFileException("No msnhproto file");

    for(size_t i = 0;i<files.size();i++)
    {
        MsnhProtoFrame dataFrame;
        MsnhProtoFrame cmdFrame;


        std::regex reg("//.*");

        ifstream t;
        t.open(files[i]);
        if(!t)
            throw MsnhProtoFileException("file "+files[i]+" doesn't exist");
        stringstream buffer;
        buffer << t.rdbuf();
        string contents(buffer.str());

        contents = std::regex_replace(contents,reg, std::string(""));

        if(count(contents.begin(), contents.end(), '#')!=4)
        {
            throw MsnhProtoFileException("file "+files[i]+" error, \'#\' area error");
        }

        contents = MsnhProtoUtil::combineSpace(contents);
        contents = MsnhProtoUtil::trimLeftRight(contents);

        vector<string> cmds =MsnhProtoUtil::splitString(contents,"#");

        if(cmds.size()!=4)
        {
            throw MsnhProtoFileException("file "+files[i]+" err");
        }


        vector<string> names;

        {
            //info part
            string cmd1 = MsnhProtoUtil::trimLeftRight(cmds[0]);
            vector<string> parts = MsnhProtoUtil::splitString(cmd1,";");

            if(parts.size()!=2)
            {
                throw MsnhProtoFileException(files[i]+": file error >> [info area] content error. line: 1-2");
            }

            vector<string> part1 = MsnhProtoUtil::splitString(parts[0]," ");
            vector<string> part2 = MsnhProtoUtil::splitString(parts[1]," ");

            if(part1.size()!=2)
            {
                throw MsnhProtoFileException(files[i]+": file error >> [info area] content error. line: " + to_string(1));
            }
            else
            {
                string temp = MsnhProtoUtil::trimLeftRight(part1[0]);
                if(temp == "typeID")
                {
                    string data = MsnhProtoUtil::trimLeftRight(part1[1]);
                    if(!MsnhProtoUtil::isPosInt(data))
                    {
                        throw MsnhProtoFileException(files[i]+": file error >> [info area] typeID data error. line: " + to_string(1));
                    }

                    dataFrame.typeID = static_cast<uint32_t>(atoi(data.c_str()));
                }
                else
                {
                    throw MsnhProtoFileException(files[i]+": file error >> [info area] must be typeID. line: " + to_string(1));
                }
            }

            if(part2.size()!=2)
            {
                throw MsnhProtoFileException(files[i]+": file error >> [info area] content error. line: " + to_string(2));
            }
            else
            {
                string temp = MsnhProtoUtil::trimLeftRight(part2[0]);
                if(temp == "typeName")
                {
                    string data = MsnhProtoUtil::trimLeftRight(part2[1]);
                    if(!MsnhProtoUtil::nameOK(data))
                    {
                        throw MsnhProtoFileException(files[i]+": file error >> [info area] typeName error. line: " + to_string(2));
                    }
                    dataFrame.typeName = data;
                }
                else
                {
                    throw MsnhProtoFileException(files[i]+": file error >> [info area] must be typeName. line: " + to_string(2));
                }
            }
        }

        //type id不能重复
        map<string,MsnhProtoFrame>::iterator  iter;//定义一个迭代指针iter

        for(iter=dataFrameLib.begin(); iter!=dataFrameLib.end(); iter++)
        {
            if(iter->second.typeID==dataFrame.typeID&&iter->second.typeName!=dataFrame.typeName)
            {
                throw MsnhProtoFileException(files[i]+" Type id exist. Exist id: " + to_string(iter->second.typeID) +
                                             " ,old type name: " + iter->second.typeName + " ,new type name: " +
                                             dataFrame.typeName
                                             );
            }
        }

        {
            //const and enum area
            string cmd1 = MsnhProtoUtil::trimLeftRight(cmds[1]);
            vector<string> parts = MsnhProtoUtil::splitString(cmd1,";");

            for(size_t i=0; i<parts.size(); i++)
            {
                string temp = MsnhProtoUtil::trimLeftRight(parts[i]);
                vector<string> part1 = MsnhProtoUtil::splitString(temp," ");

                if(part1.size()==4)
                {
                    if(part1[0]!="const")
                    {
                        throw MsnhProtoFileException(files[i]+": file error >> [const and enum area] content line: " + to_string(i+1));
                    }
                    std::string dataType = part1[1];
                    int index = containSingle(dataType);

                    if(index == -1)
                    {
                        throw MsnhProtoFileException(files[i]+": file error >> [const and enum area] content line: " + to_string(i+1));
                    }

                    string name = MsnhProtoUtil::trimLeftRight(part1[2]);

                    if(!MsnhProtoUtil::nameOK(name))
                    {
                        throw MsnhProtoFileException(files[i]+": file error >> [const and enum area] typeName error. line: " + to_string(i+1));
                    }

                    if(count(names.begin(), names.end(), name)!=0)
                    {
                        throw MsnhProtoFileException(files[i]+": variable exist >> [const and enum area] line : " + to_string(i+1) + " >> name: " + name);
                    }

                    string data = MsnhProtoUtil::trimLeftRight(part1[3]);

                    if(isPosIntType(dataType))
                    {
                        if(!MsnhProtoUtil::isPosInt(data))
                        {
                            throw MsnhProtoFileException(files[i]+": file error >> [const and enum area] data error. line: " + to_string(i+1));
                        }
                    }
                    else if(isIntType(dataType))
                    {
                        if(!MsnhProtoUtil::isInt(data))
                        {
                            throw MsnhProtoFileException(files[i]+": file error >> [const and enum area] data error. line: " + to_string(i+1));
                        }
                    }
                    else if(isfloatType(dataType))
                    {
                        if(!MsnhProtoUtil::isFloat(data))
                        {
                            throw MsnhProtoFileException(files[i]+": file error >> [const and enum area] data error. line: " + to_string(i+1));
                        }
                    }

                    names.push_back(name);

                }
                else if(part1.size()==3)
                {
                    if(part1[0]!="enum")
                    {
                        throw MsnhProtoFileException(files[i]+": file error >> [const and enum area] content line: " + to_string(i+1));
                    }
                    else
                    {
                        if(part1[2][0]!='{' || part1[2][part1[2].length()-1]!='}')
                        {
                            throw MsnhProtoFileException(files[i]+": file error >> [const and enum area] content line: " + to_string(i+1));
                        }

                        std::string enumStr = part1[2];

                        MsnhProtoUtil::replace(enumStr,"{","");
                        MsnhProtoUtil::replace(enumStr,"}","");

                        std::vector<std::string> enums = MsnhProtoUtil::splitString(enumStr,",");

                        for(auto _enum: enums)
                        {
                            if(!MsnhProtoUtil::nameOK(_enum))
                            {
                                throw MsnhProtoFileException(files[i]+": file error >> [const and enum area] enum name error. line: " + to_string(i+1));
                            }

                            if(count(names.begin(), names.end(), _enum)!=0)
                            {
                                throw MsnhProtoFileException(files[i]+": variable exist >> [const and enum area] line : " + to_string(i+1) + " >> name: " + _enum);
                            }

                            names.push_back(_enum);
                        }
                    }
                }
            }
        }


        {
            string cmd1 = MsnhProtoUtil::trimLeftRight(cmds[2]);
            vector<string> part = MsnhProtoUtil::splitString(cmd1,";");
            for(size_t j=0; j<part.size(); j++)
            {
                string temp = MsnhProtoUtil::trimLeftRight(part[j]);
                vector<string> part1 = MsnhProtoUtil::splitString(temp," ");

                if(part1.size()!=2)
                {
                    throw MsnhProtoFileException(files[i]+": file error >> [data area] content error.  line : " + to_string(j+1));
                }


                int8_t index = contain(part1[0]);
                if(index == -1)
                {
                    throw MsnhProtoFileException(files[i]+": file error >> [data area] type error. line : " + to_string(j+1));
                }

                std::string dataName = part1[1];

                if(!MsnhProtoUtil::nameOK(dataName))
                {
                    throw MsnhProtoFileException(files[i]+": file error >> [data area] name error. line : " + to_string(j+1));
                }

                if(count(names.begin(), names.end(), dataName)!=0)
                {
                    throw MsnhProtoFileException(files[i]+": variable exist >> [data area] line : " + to_string(j+1) + " >> name: " + dataName);;
                }

                names.push_back(dataName);

                switch (index)
                {
                case 0:
                    dataFrame.u8Map[dataName] = 0;
                    break;
                case 1:
                    dataFrame.s8Map[dataName] = 0;
                    break;
                case 2:
                    dataFrame.u16Map[dataName] = 0;
                    break;
                case 3:
                    dataFrame.s16Map[dataName] = 0;
                    break;
                case 4:
                    dataFrame.u32Map[dataName] = 0;
                    break;
                case 5:
                    dataFrame.s32Map[dataName] = 0;
                    break;
                case 6:
                    dataFrame.u64Map[dataName] = 0;
                    break;
                case 7:
                    dataFrame.s64Map[dataName] = 0;
                    break;
                case 8:
                    dataFrame.f32Map[dataName] = 0;
                    break;
                case 9:
                    dataFrame.f64Map[dataName] = 0;
                    break;
                case 10:
                    dataFrame.strMap[dataName] = "null";
                    break;
                case 11:
                    dataFrame.u8ListMap[dataName] = {0};
                    break;
                case 12:
                    dataFrame.s8ListMap[dataName] = {0};
                    break;
                case 13:
                    dataFrame.u16ListMap[dataName] = {0};
                    break;
                case 14:
                    dataFrame.s16ListMap[dataName] = {0};
                    break;
                case 15:
                    dataFrame.u32ListMap[dataName] = {0};
                    break;
                case 16:
                    dataFrame.s32ListMap[dataName] = {0};
                    break;
                case 17:
                    dataFrame.u64ListMap[dataName] = {0};
                    break;
                case 18:
                    dataFrame.s64ListMap[dataName] = {0};
                    break;
                case 19:
                    dataFrame.f32ListMap[dataName] = {0};
                    break;
                case 20:
                    dataFrame.f64ListMap[dataName] = {0};
                    break;
                case 21:
                    dataFrame.strListMap[dataName] = {"null"};
                    break;
                }
            }
        }

        {
            string cmd1 = MsnhProtoUtil::trimLeftRight(cmds[3]);
            vector<string> part = MsnhProtoUtil::splitString(cmd1,";");
            for(size_t j=0; j<part.size(); j++)
            {
                string temp = MsnhProtoUtil::trimLeftRight(part[j]);
                vector<string> part1 = MsnhProtoUtil::splitString(temp," ");

                if(part1.size()!=2)
                {
                    throw MsnhProtoFileException(files[i]+": file error >> [cmd area] content error.  line : " + to_string(j+1));
                }


                int8_t index = contain(part1[0]);
                if(index == -1)
                {
                    throw MsnhProtoFileException(files[i]+": file error >> [cmd area] type error. line : " + to_string(j+1));
                }

                std::string cmdName = part1[1];

                if(!MsnhProtoUtil::nameOK(cmdName))
                {
                    throw MsnhProtoFileException(files[i]+": file error >> [cmd area] name error. line : " + to_string(j+1));
                }

                if(count(names.begin(), names.end(), cmdName)!=0)
                {
                    throw MsnhProtoFileException(files[i]+": variable exist >> [cmd area] line : " + to_string(j+1) + " >> name: " + cmdName);;
                }

                names.push_back(cmdName);

                switch (index)
                {
                case 0:
                    dataFrame.u8Map[cmdName] = 0;
                    break;
                case 1:
                    dataFrame.s8Map[cmdName] = 0;
                    break;
                case 2:
                    dataFrame.u16Map[cmdName] = 0;
                    break;
                case 3:
                    dataFrame.s16Map[cmdName] = 0;
                    break;
                case 4:
                    dataFrame.u32Map[cmdName] = 0;
                    break;
                case 5:
                    dataFrame.s32Map[cmdName] = 0;
                    break;
                case 6:
                    dataFrame.u64Map[cmdName] = 0;
                    break;
                case 7:
                    dataFrame.s64Map[cmdName] = 0;
                    break;
                case 8:
                    dataFrame.f32Map[cmdName] = 0;
                    break;
                case 9:
                    dataFrame.f64Map[cmdName] = 0;
                    break;
                case 10:
                    dataFrame.strMap[cmdName] = "null";
                    break;
                case 11:
                    dataFrame.u8ListMap[cmdName] = {0};
                    break;
                case 12:
                    dataFrame.s8ListMap[cmdName] = {0};
                    break;
                case 13:
                    dataFrame.u16ListMap[cmdName] = {0};
                    break;
                case 14:
                    dataFrame.s16ListMap[cmdName] = {0};
                    break;
                case 15:
                    dataFrame.u32ListMap[cmdName] = {0};
                    break;
                case 16:
                    dataFrame.s32ListMap[cmdName] = {0};
                    break;
                case 17:
                    dataFrame.u64ListMap[cmdName] = {0};
                    break;
                case 18:
                    dataFrame.s64ListMap[cmdName] = {0};
                    break;
                case 19:
                    dataFrame.f32ListMap[cmdName] = {0};
                    break;
                case 20:
                    dataFrame.f64ListMap[cmdName] = {0};
                    break;
                case 21:
                    dataFrame.strListMap[cmdName] = {"null"};
                    break;
                }
            }
        }

        if(dataFrameLib.find(dataFrame.typeName) != dataFrameLib.end())
        {
            if(replace) //替换之前的
            {
                dataFrame.funcID = 1;
                dataFrameLib[dataFrame.typeName]=dataFrame;
                cmdFrame.typeName= dataFrame.typeName;
                cmdFrame.typeID= dataFrame.typeID;
                cmdFrame.funcID = 0;
                cmdFrameLib[cmdFrame.typeName]=cmdFrame;
            }
        }
        else
        {
            dataFrame.funcID = 1;
            dataFrameLib[dataFrame.typeName]=dataFrame;
            cmdFrame.typeName= dataFrame.typeName;
            cmdFrame.typeID= dataFrame.typeID;
            cmdFrame.funcID = 0;
            cmdFrameLib[cmdFrame.typeName]=cmdFrame;
        }
        typeID2TypeNameMap[dataFrame.typeID] = dataFrame.typeName;
    }

}

void MsnhProtoFile::initFrameTypeLib()
{
    vector<string> paths = getProtocolFiles();
    getFrameBase(paths);
    frameLibInited = true;
}

void MsnhProtoFile::initFrameTypeLib(const string &path)
{
    vector<string> paths = getProtocolFiles(path);
    getFrameBase(paths);
    frameLibInited = true;
}

void MsnhProtoFile::initFrameTypeLib(const std::vector<string> &pathVec)
{
    getFrameBase(pathVec);
    frameLibInited = true;
}

void MsnhProtoFile::initFrameTypeLibByContent(const std::vector<string> &contentVec, bool replace)
{
    for(size_t i = 0;i<contentVec.size();i++)
    {
        MsnhProtoFrame dataFrame;
        MsnhProtoFrame cmdFrame;


        std::regex reg("//.*");

        string contents = contentVec[i];

        contents = std::regex_replace(contents,reg, std::string(""));

        if(count(contents.begin(), contents.end(), '#')!=4)
        {
            throw MsnhProtoFileException("================\n "+ contents +"================\n error, \'#\' area error");
        }

        contents = MsnhProtoUtil::combineSpace(contents);
        contents = MsnhProtoUtil::trimLeftRight(contents);

        vector<string> cmds =MsnhProtoUtil::splitString(contents,"#");

        if(cmds.size()!=4)
        {
            throw MsnhProtoFileException("================\n "+ contents +"================\n err");
        }


        vector<string> names;

        {
            //info part
            string cmd1 = MsnhProtoUtil::trimLeftRight(cmds[0]);
            vector<string> parts = MsnhProtoUtil::splitString(cmd1,";");

            if(parts.size()!=2)
            {
                throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [info area] content error. line: 1-2");
            }

            vector<string> part1 = MsnhProtoUtil::splitString(parts[0]," ");
            vector<string> part2 = MsnhProtoUtil::splitString(parts[1]," ");

            if(part1.size()!=2)
            {
                throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [info area] content error. line: " + to_string(1));
            }
            else
            {
                string temp = MsnhProtoUtil::trimLeftRight(part1[0]);
                if(temp == "typeID")
                {
                    string data = MsnhProtoUtil::trimLeftRight(part1[1]);
                    if(!MsnhProtoUtil::isPosInt(data))
                    {
                        throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [info area] typeID data error. line: " + to_string(1));
                    }

                    dataFrame.typeID = static_cast<uint32_t>(atoi(data.c_str()));
                }
                else
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [info area] must be typeID. line: " + to_string(1));
                }
            }

            if(part2.size()!=2)
            {
                throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [info area] content error. line: " + to_string(2));
            }
            else
            {
                string temp = MsnhProtoUtil::trimLeftRight(part2[0]);
                if(temp == "typeName")
                {
                    string data = MsnhProtoUtil::trimLeftRight(part2[1]);
                    if(!MsnhProtoUtil::nameOK(data))
                    {
                        throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [info area] typeName error. line: " + to_string(2));
                    }
                    dataFrame.typeName = data;
                }
                else
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [info area] must be typeName. line: " + to_string(2));
                }
            }
        }

        //type id不能重复
        map<string,MsnhProtoFrame>::iterator  iter;//定义一个迭代指针iter

        for(iter=dataFrameLib.begin(); iter!=dataFrameLib.end(); iter++)
        {
            if(iter->second.typeID==dataFrame.typeID&&iter->second.typeName!=dataFrame.typeName)
            {
                throw MsnhProtoFileException("================\n "+ contents +"================\n Type id exist. Exist id: " + to_string(iter->second.typeID) +
                                             " ,old type name: " + iter->second.typeName + " ,new type name: " +
                                             dataFrame.typeName
                                             );
            }
        }

        {
            //const and enum area
            string cmd1 = MsnhProtoUtil::trimLeftRight(cmds[1]);
            vector<string> parts = MsnhProtoUtil::splitString(cmd1,";");

            for(size_t i=0; i<parts.size(); i++)
            {
                string temp = MsnhProtoUtil::trimLeftRight(parts[i]);
                vector<string> part1 = MsnhProtoUtil::splitString(temp," ");

                if(part1.size()==4)
                {
                    if(part1[0]!="const")
                    {
                        throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [const and enum area] content line: " + to_string(i+1));
                    }
                    std::string dataType = part1[1];
                    int index = containSingle(dataType);

                    if(index == -1)
                    {
                        throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [const and enum area] content line: " + to_string(i+1));
                    }

                    string name = MsnhProtoUtil::trimLeftRight(part1[2]);

                    if(!MsnhProtoUtil::nameOK(name))
                    {
                        throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [const and enum area] typeName error. line: " + to_string(1));
                    }

                    if(count(names.begin(), names.end(), name)!=0)
                    {
                        throw MsnhProtoFileException("================\n "+ contents +"================\n: variable exist >> [const and enum area] line : " + to_string(i+1) + " >> name: " + name);
                    }

                    string data = MsnhProtoUtil::trimLeftRight(part1[3]);

                    if(isPosIntType(dataType))
                    {
                        if(!MsnhProtoUtil::isPosInt(data))
                        {
                            throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [const and enum area] data error. line: " + to_string(1));
                        }
                    }
                    else if(isIntType(dataType))
                    {
                        if(!MsnhProtoUtil::isInt(data))
                        {
                            throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [const and enum area] data error. line: " + to_string(1));
                        }
                    }
                    else if(isfloatType(dataType))
                    {
                        if(!MsnhProtoUtil::isFloat(data))
                        {
                            throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [const and enum area] data error. line: " + to_string(1));
                        }
                    }
                    names.push_back(name);
                }
                else if(part1.size()==3)
                {
                    if(part1[0]!="enum")
                    {
                        throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [const and enum area] content line: " + to_string(i+1));
                    }
                    else
                    {
                        if(part1[2][0]!='{' || part1[2][part1[2].length()-1]!='}')
                        {
                            throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [const and enum area] content line: " + to_string(i+1));
                        }

                        std::string enumStr = part1[2];

                        MsnhProtoUtil::replace(enumStr,"{","");
                        MsnhProtoUtil::replace(enumStr,"}","");

                        std::vector<std::string> enums = MsnhProtoUtil::splitString(enumStr,",");

                        for(auto _enum: enums)
                        {
                            if(!MsnhProtoUtil::nameOK(_enum))
                            {
                                throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [const and enum area] enum name error. line: " + to_string(1));
                            }

                            if(count(names.begin(), names.end(), _enum)!=0)
                            {
                                throw MsnhProtoFileException("================\n "+ contents +"================\n: variable exist >> [const and enum area] line : " + to_string(i+1) + " >> name: " + _enum);
                            }
                            names.push_back(_enum);
                        }
                    }
                }
            }
        }


        {
            string cmd1 = MsnhProtoUtil::trimLeftRight(cmds[2]);
            vector<string> part = MsnhProtoUtil::splitString(cmd1,";");
            for(size_t j=0; j<part.size(); j++)
            {
                string temp = MsnhProtoUtil::trimLeftRight(part[j]);
                vector<string> part1 = MsnhProtoUtil::splitString(temp," ");

                if(part1.size()!=2)
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [data area] content error.  line : " + to_string(j+1));
                }


                int8_t index = contain(part1[0]);
                if(index == -1)
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [data area] type error. line : " + to_string(j+1));
                }

                std::string dataName = part1[1];

                if(!MsnhProtoUtil::nameOK(dataName))
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [data area] name error. line : " + to_string(j+1));
                }

                if(count(names.begin(), names.end(), dataName)!=0)
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: variable exist >> [data area] line : " + to_string(j+1) + " >> name: " + dataName);;
                }

                names.push_back(dataName);

                switch (index)
                {
                case 0:
                    dataFrame.u8Map[dataName] = 0;
                    break;
                case 1:
                    dataFrame.s8Map[dataName] = 0;
                    break;
                case 2:
                    dataFrame.u16Map[dataName] = 0;
                    break;
                case 3:
                    dataFrame.s16Map[dataName] = 0;
                    break;
                case 4:
                    dataFrame.u32Map[dataName] = 0;
                    break;
                case 5:
                    dataFrame.s32Map[dataName] = 0;
                    break;
                case 6:
                    dataFrame.u64Map[dataName] = 0;
                    break;
                case 7:
                    dataFrame.s64Map[dataName] = 0;
                    break;
                case 8:
                    dataFrame.f32Map[dataName] = 0;
                    break;
                case 9:
                    dataFrame.f64Map[dataName] = 0;
                    break;
                case 10:
                    dataFrame.strMap[dataName] = "null";
                    break;
                case 11:
                    dataFrame.u8ListMap[dataName] = {0};
                    break;
                case 12:
                    dataFrame.s8ListMap[dataName] = {0};
                    break;
                case 13:
                    dataFrame.u16ListMap[dataName] = {0};
                    break;
                case 14:
                    dataFrame.s16ListMap[dataName] = {0};
                    break;
                case 15:
                    dataFrame.u32ListMap[dataName] = {0};
                    break;
                case 16:
                    dataFrame.s32ListMap[dataName] = {0};
                    break;
                case 17:
                    dataFrame.u64ListMap[dataName] = {0};
                    break;
                case 18:
                    dataFrame.s64ListMap[dataName] = {0};
                    break;
                case 19:
                    dataFrame.f32ListMap[dataName] = {0};
                    break;
                case 20:
                    dataFrame.f64ListMap[dataName] = {0};
                    break;
                case 21:
                    dataFrame.strListMap[dataName] = {"null"};
                    break;
                }
            }
        }

        {
            string cmd1 = MsnhProtoUtil::trimLeftRight(cmds[3]);
            vector<string> part = MsnhProtoUtil::splitString(cmd1,";");
            for(size_t j=0; j<part.size(); j++)
            {
                string temp = MsnhProtoUtil::trimLeftRight(part[j]);
                vector<string> part1 = MsnhProtoUtil::splitString(temp," ");

                if(part1.size()!=2)
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [cmd area] content error.  line : " + to_string(j+1));
                }


                int8_t index = contain(part1[0]);
                if(index == -1)
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [cmd area] type error. line : " + to_string(j+1));
                }

                std::string cmdName = part1[1];

                if(!MsnhProtoUtil::nameOK(cmdName))
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: file error >> [cmd area] name error. line : " + to_string(j+1));
                }

                if(count(names.begin(), names.end(), cmdName)!=0)
                {
                    throw MsnhProtoFileException("================\n "+ contents +"================\n: variable exist >> [cmd area] line : " + to_string(j+1) + " >> name: " + cmdName);;
                }

                names.push_back(cmdName);

                switch (index)
                {
                case 0:
                    dataFrame.u8Map[cmdName] = 0;
                    break;
                case 1:
                    dataFrame.s8Map[cmdName] = 0;
                    break;
                case 2:
                    dataFrame.u16Map[cmdName] = 0;
                    break;
                case 3:
                    dataFrame.s16Map[cmdName] = 0;
                    break;
                case 4:
                    dataFrame.u32Map[cmdName] = 0;
                    break;
                case 5:
                    dataFrame.s32Map[cmdName] = 0;
                    break;
                case 6:
                    dataFrame.u64Map[cmdName] = 0;
                    break;
                case 7:
                    dataFrame.s64Map[cmdName] = 0;
                    break;
                case 8:
                    dataFrame.f32Map[cmdName] = 0;
                    break;
                case 9:
                    dataFrame.f64Map[cmdName] = 0;
                    break;
                case 10:
                    dataFrame.strMap[cmdName] = "null";
                    break;
                case 11:
                    dataFrame.u8ListMap[cmdName] = {0};
                    break;
                case 12:
                    dataFrame.s8ListMap[cmdName] = {0};
                    break;
                case 13:
                    dataFrame.u16ListMap[cmdName] = {0};
                    break;
                case 14:
                    dataFrame.s16ListMap[cmdName] = {0};
                    break;
                case 15:
                    dataFrame.u32ListMap[cmdName] = {0};
                    break;
                case 16:
                    dataFrame.s32ListMap[cmdName] = {0};
                    break;
                case 17:
                    dataFrame.u64ListMap[cmdName] = {0};
                    break;
                case 18:
                    dataFrame.s64ListMap[cmdName] = {0};
                    break;
                case 19:
                    dataFrame.f32ListMap[cmdName] = {0};
                    break;
                case 20:
                    dataFrame.f64ListMap[cmdName] = {0};
                    break;
                case 21:
                    dataFrame.strListMap[cmdName] = {"null"};
                    break;
                }
            }
        }

        if(dataFrameLib.find(dataFrame.typeName) != dataFrameLib.end())
        {
            if(replace) //替换之前的
            {
                dataFrame.funcID = 1;
                dataFrameLib[dataFrame.typeName]=dataFrame;
                cmdFrame.typeName= dataFrame.typeName;
                cmdFrame.typeID= dataFrame.typeID;
                cmdFrame.funcID = 0;
                cmdFrameLib[cmdFrame.typeName]=cmdFrame;
            }
        }
        else
        {
            dataFrame.funcID = 1;
            dataFrameLib[dataFrame.typeName]=dataFrame;
            cmdFrame.typeName= dataFrame.typeName;
            cmdFrame.typeID= dataFrame.typeID;
            cmdFrame.funcID = 0;
            cmdFrameLib[cmdFrame.typeName]=cmdFrame;
        }
        typeID2TypeNameMap[dataFrame.typeID] = dataFrame.typeName;
    }

}

void MsnhProtoFile::refreshFrameTypeLib(const string &path,bool replace)
{
    vector<string> paths = getProtocolFiles(path);
    getFrameBase(paths,replace);
    frameLibInited = true;
}

bool MsnhProtoFile::isPosIntType(const string &str)
{
    if(str == "u8" || str == "u16" || str == "u32" || str == "u64")
    {
        return true;
    }
    return false;
}

bool MsnhProtoFile::isIntType(const string &str)
{
    if(str == "s8" || str == "s16" || str == "s32" || str == "s64")
    {
        return true;
    }
    return false;
}

bool MsnhProtoFile::isfloatType(const string &str)
{
    if(str == "f32" || str == "f64")
    {
        return true;
    }
    return false;
}

bool MsnhProtoFile::isStrType(const string &str)
{
    if(str == "str")
    {
        return true;
    }
    return false;
}

