﻿#include "MsnhProtoLib.h"

static std::string objNull = "MsnhProtoManager is null!";

int initProtoTypes(char** msg)
{
    try
    {
        MsnhProtoFile::initFrameTypeLib();
    }
    catch(const MsnhProtoFileException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }

    return 1;
}

int initProtoTypesPath(char* path, char** msg)
{
    try
    {
        MsnhProtoFile::initFrameTypeLib(std::string(path));
    }
    catch(const MsnhProtoFileException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;

        return -1;
    }

    return 1;
}

void *createManager()
{
    return new MsnhProtoManager();
}

void *createManagerWithID(uint32_t ID, uint8_t type)
{
    MsnhProtoType tp = (MsnhProtoType)type;
    return new MsnhProtoManager(ID,tp);
}

void *createManagerWithName(char *name, uint8_t type)
{
    MsnhProtoType tp = (MsnhProtoType)type;
    return new MsnhProtoManager(std::string(name),tp);
}

int getTypeName(MsnhProtoManager *manager, char **name)
{
    if(manager)
    {
        std::string tmpMsg = manager->getTypeName();
        char* msg = new char[tmpMsg.size()+1];
        memcpy(msg, tmpMsg.data(),tmpMsg.size());
        msg[tmpMsg.size()] = '\0';
        *name = msg;
        return 1;
    }
    return -1;
}

int getTypeID(MsnhProtoManager *manager, uint32_t *id)
{
    if(manager)
    {
        *id = manager->getTypeID();
        return 1;
    }
    return -1;
}


int getU8(MsnhProtoManager *manager, char *key, uint8_t *u8, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *u8 = manager->getU8(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;

        return -1;
    }
}

int setU8(MsnhProtoManager *manager, char *key, uint8_t data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }
    try
    {
        manager->setU8(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getS8(MsnhProtoManager *manager, char *key, int8_t *i8, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *i8 = manager->getS8(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setS8(MsnhProtoManager *manager, char *key, int8_t data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }
    try
    {
        manager->setS8(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getU16(MsnhProtoManager *manager, char *key, uint16_t *u16, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *u16 = manager->getU16(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setU16(MsnhProtoManager *manager, char *key, uint16_t data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        manager->setU16(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getS16(MsnhProtoManager *manager, char *key, int16_t *i16, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *i16 = manager->getS16(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setS16(MsnhProtoManager *manager, char *key, int16_t data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        manager->setS16(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getU32(MsnhProtoManager *manager, char *key, uint32_t *u32, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *u32 = manager->getU32(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setU32(MsnhProtoManager *manager, char *key, uint32_t data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        manager->setU32(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getS32(MsnhProtoManager *manager, char *key, int32_t *i32, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *i32 = manager->getS32(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setS32(MsnhProtoManager *manager, char *key, int32_t data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        manager->setS32(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getU64(MsnhProtoManager *manager, char *key, uint64_t *u64, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *u64 = manager->getU64(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setU64(MsnhProtoManager *manager, char *key, uint64_t data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        manager->setU64(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getS64(MsnhProtoManager *manager, char *key, int64_t *i64, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *i64 = manager->getS64(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setS64(MsnhProtoManager *manager, char *key, int64_t data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }
    try
    {
        manager->setS64(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getF32(MsnhProtoManager *manager, char *key, float *f, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *f = manager->getF32(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setF32(MsnhProtoManager *manager, char *key, float data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        manager->setF32(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getF64(MsnhProtoManager *manager, char *key, double *d, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        *d = manager->getF64(std::string(key));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setF64(MsnhProtoManager *manager, char *key, double data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        manager->setF64(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getStr(MsnhProtoManager *manager, char *key, char **str, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::string tmpMsg = manager->getStr(std::string(key));
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *str = tmpStr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setStr(MsnhProtoManager *manager, char *key, char *data, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        manager->setStr(std::string(key), std::string(data));
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getU8Vec(MsnhProtoManager *manager, char *key, uint8_t **u8, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint8_t> data = manager->getU8Vec(std::string(key));
        uint8_t* u8Ptr = (uint8_t*)malloc(data.size()*sizeof(uint8_t));
        memcpy(u8Ptr,data.data(), data.size()*sizeof(uint8_t));
        *size = (uint32_t)data.size();
        *u8   = u8Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setU8Vec(MsnhProtoManager *manager, char *key, uint8_t *u8, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint8_t> data(u8, u8+size);
        manager->setU8Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getS8Vec(MsnhProtoManager *manager, char *key, int8_t **i8, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<int8_t> data = manager->getS8Vec(std::string(key));
        int8_t* i8Ptr = (int8_t*)malloc(data.size()*sizeof(int8_t));
        memcpy(i8Ptr,data.data(), data.size()*sizeof(int8_t));
        *size = (uint32_t)data.size();
        *i8   = i8Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setS8Vec(MsnhProtoManager *manager, char *key, int8_t *i8, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<int8_t> data(i8, i8+size);
        manager->setS8Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getU16Vec(MsnhProtoManager *manager, char *key, uint16_t **u16, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint16_t> data = manager->getU16Vec(std::string(key));
        uint16_t* u16Ptr = (uint16_t*)malloc(data.size()*sizeof(uint16_t));
        memcpy(u16Ptr,data.data(), data.size()*sizeof(uint16_t));
        *size = (uint32_t)data.size();
        *u16   = u16Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setU16Vec(MsnhProtoManager *manager, char *key, uint16_t *u16, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint16_t> data(u16, u16+size);
        manager->setU16Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getS16Vec(MsnhProtoManager *manager, char *key, int16_t **i16, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<int16_t> data = manager->getS16Vec(std::string(key));
        int16_t* i16Ptr = (int16_t*)malloc(data.size()*sizeof(int16_t));
        memcpy(i16Ptr,data.data(), data.size()*sizeof(int16_t));
        *size = (uint32_t)data.size();
        *i16   = i16Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setS16Vec(MsnhProtoManager *manager, char *key, int16_t *i16, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<int16_t> data(i16, i16+size);
        manager->setS16Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getU32Vec(MsnhProtoManager *manager, char *key, uint32_t **u32, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint32_t> data = manager->getU32Vec(std::string(key));
        uint32_t* u32Ptr = (uint32_t*)malloc(data.size()*sizeof(uint32_t));
        memcpy(u32Ptr,data.data(), data.size()*sizeof(uint32_t));
        *size = (uint32_t)data.size();
        *u32   = u32Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setU32Vec(MsnhProtoManager *manager, char *key, uint32_t *u32, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint32_t> data(u32, u32+size);
        manager->setU32Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getS32Vec(MsnhProtoManager *manager, char *key, int32_t **i32, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<int32_t> data = manager->getS32Vec(std::string(key));
        int32_t* i32Ptr = (int32_t*)malloc(data.size()*sizeof(int32_t));
        memcpy(i32Ptr,data.data(), data.size()*sizeof(int32_t));
        *size = (uint32_t)data.size();
        *i32   = i32Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setS32Vec(MsnhProtoManager *manager, char *key, int32_t *i32, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<int32_t> data(i32, i32+size);
        manager->setS32Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getU64Vec(MsnhProtoManager *manager, char *key, uint64_t **u64, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint64_t> data = manager->getU64Vec(std::string(key));
        uint64_t* u64Ptr = (uint64_t*)malloc(data.size()*sizeof(uint64_t));
        memcpy(u64Ptr,data.data(), data.size()*sizeof(uint64_t));
        *size = (uint32_t)data.size();
        *u64   = u64Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setU64Vec(MsnhProtoManager *manager, char *key, uint64_t *u64, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint64_t> data(u64, u64+size);
        manager->setU64Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getS64Vec(MsnhProtoManager *manager, char *key, int64_t **i64, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<int64_t> data = manager->getS64Vec(std::string(key));
        int64_t* i64Ptr = (int64_t*)malloc(data.size()*sizeof(int64_t));
        memcpy(i64Ptr,data.data(), data.size()*sizeof(int64_t));
        *size = (uint32_t)data.size();
        *i64   = i64Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setS64Vec(MsnhProtoManager *manager, char *key, int64_t *i64, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<int64_t> data(i64, i64+size);
        manager->setS64Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getF32Vec(MsnhProtoManager *manager, char *key, float **f32, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<float> data = manager->getF32Vec(std::string(key));
        float* f32Ptr = (float*)malloc(data.size()*sizeof(float));
        memcpy(f32Ptr,data.data(), data.size()*sizeof(float));
        *size = (uint32_t)data.size();
        *f32   = f32Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setF32Vec(MsnhProtoManager *manager, char *key, float *f32, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<float> data(f32, f32+size);
        manager->setF32Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int getF64Vec(MsnhProtoManager *manager, char *key, double **f64, uint32_t *size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<double> data = manager->getF64Vec(std::string(key));
        double* f64Ptr = (double*)malloc(data.size()*sizeof(double));
        memcpy(f64Ptr,data.data(), data.size()*sizeof(double));
        *size = (uint32_t)data.size();
        *f64   = f64Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int setF64Vec(MsnhProtoManager *manager, char *key, double *f64, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<double> data(f64, f64+size);
        manager->setF64Vec(std::string(key), data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

void freeMem(void *ptr)
{
    if(ptr)
    {
        free(ptr);
    }
}

int fromBytes(MsnhProtoManager *manager, uint8_t *u8, uint32_t size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint8_t> data(u8, u8+size);
        manager->fromBytes(data);
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}

int serialize(MsnhProtoManager *manager, uint8_t **u8, uint32_t* size, char **msg)
{
    if(!manager)
    {
        *msg = (char*)objNull.data();
        return -1;
    }

    try
    {
        std::vector<uint8_t> data = manager->serialize();
        uint8_t* u8Ptr = (uint8_t*)malloc(data.size()*sizeof(uint8_t));
        memcpy(u8Ptr,data.data(), data.size()*sizeof(uint8_t));
        *size = (uint32_t)data.size();
        *u8   = u8Ptr;
        return 1;
    }
    catch(const MsnhProtoException& ex)
    {
        std::string tmpMsg = std::string(ex.what());
        char* tmpStr = (char*)malloc((tmpMsg.size()+1)*sizeof(char));
        memcpy(tmpStr, tmpMsg.data(),tmpMsg.size());
        tmpStr[tmpMsg.size()] = '\0';
        *msg = tmpStr;
        return -1;
    }
}
