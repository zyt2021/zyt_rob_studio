﻿#ifndef MSNH_PROTO_LIB
#define MSNH_PROTO_LIB

#include <MsnhProto/MsnhProto.h>
#include <MsnhProto/MsnhProtoManager.h>
#include <MsnhProto/MsnhExport.h>

extern "C" MSNH_PROTO_API int initProtoTypes(char** msg);

extern "C" MSNH_PROTO_API int initProtoTypesPath(char* path, char** msg);

extern "C" MSNH_PROTO_API void* createManager();

extern "C" MSNH_PROTO_API void* createManagerWithID(uint32_t ID, uint8_t type);

extern "C" MSNH_PROTO_API void freeMem(void* ptr);

extern "C" MSNH_PROTO_API void* createManagerWithName(char* name, uint8_t type);

extern "C" MSNH_PROTO_API int getTypeName(MsnhProtoManager* manager, char** name);

extern "C" MSNH_PROTO_API int getTypeID(MsnhProtoManager* manager, uint32_t *id);

extern "C" MSNH_PROTO_API int getU8(MsnhProtoManager* manager, char* key, uint8_t* u8, char** msg);

extern "C" MSNH_PROTO_API int setU8(MsnhProtoManager* manager, char* key, uint8_t data, char** msg);

extern "C" MSNH_PROTO_API int getS8(MsnhProtoManager* manager, char* key, int8_t* i8, char** msg);

extern "C" MSNH_PROTO_API int setS8(MsnhProtoManager* manager, char* key, int8_t data, char** msg);

extern "C" MSNH_PROTO_API int getU16(MsnhProtoManager* manager, char* key, uint16_t* u16, char** msg);

extern "C" MSNH_PROTO_API int setU16(MsnhProtoManager* manager, char* key, uint16_t data, char** msg);

extern "C" MSNH_PROTO_API int getS16(MsnhProtoManager* manager, char* key, int16_t* i16, char** msg);

extern "C" MSNH_PROTO_API int setS16(MsnhProtoManager* manager, char* key, int16_t data, char** msg);

extern "C" MSNH_PROTO_API int getU32(MsnhProtoManager* manager, char* key, uint32_t* u32, char** msg);

extern "C" MSNH_PROTO_API int setU32(MsnhProtoManager* manager, char* key, uint32_t data, char** msg);

extern "C" MSNH_PROTO_API int getS32(MsnhProtoManager* manager, char* key, int32_t* i32, char** msg);

extern "C" MSNH_PROTO_API int setS32(MsnhProtoManager* manager, char* key, int32_t data, char** msg);

extern "C" MSNH_PROTO_API int getU64(MsnhProtoManager* manager, char* key, uint64_t* u64, char** msg);

extern "C" MSNH_PROTO_API int setU64(MsnhProtoManager* manager, char* key, uint64_t data, char** msg);

extern "C" MSNH_PROTO_API int getS64(MsnhProtoManager* manager, char* key, int64_t* i64, char** msg);

extern "C" MSNH_PROTO_API int setS64(MsnhProtoManager* manager, char* key, int64_t data, char** msg);

extern "C" MSNH_PROTO_API int getF32(MsnhProtoManager* manager, char* key, float* f, char** msg);

extern "C" MSNH_PROTO_API int setF32(MsnhProtoManager* manager, char* key, float data, char** msg);

extern "C" MSNH_PROTO_API int getF64(MsnhProtoManager* manager, char* key, double *d, char** msg);

extern "C" MSNH_PROTO_API int setF64(MsnhProtoManager* manager, char* key, double data, char** msg);

extern "C" MSNH_PROTO_API int getStr(MsnhProtoManager* manager, char* key, char** str, char** msg);

extern "C" MSNH_PROTO_API int setStr(MsnhProtoManager* manager, char* key, char* data, char** msg);

extern "C" MSNH_PROTO_API int getU8Vec(MsnhProtoManager* manager, char* key, uint8_t** u8, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setU8Vec(MsnhProtoManager* manager, char* key, uint8_t* u8, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int getS8Vec(MsnhProtoManager* manager, char* key, int8_t** i8, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setS8Vec(MsnhProtoManager* manager, char* key, int8_t* i8, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int getU16Vec(MsnhProtoManager* manager, char* key, uint16_t** u16, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setU16Vec(MsnhProtoManager* manager, char* key, uint16_t* u16, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int getS16Vec(MsnhProtoManager* manager, char* key, int16_t** i16, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setS16Vec(MsnhProtoManager* manager, char* key, int16_t* i16, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int getU32Vec(MsnhProtoManager* manager, char* key, uint32_t** u32, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setU32Vec(MsnhProtoManager* manager, char* key, uint32_t* u32, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int getS32Vec(MsnhProtoManager* manager, char* key, int32_t** i32, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setS32Vec(MsnhProtoManager* manager, char* key, int32_t* i32, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int getU64Vec(MsnhProtoManager* manager, char* key, uint64_t** u64, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setU64Vec(MsnhProtoManager* manager, char* key, uint64_t* u64, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int getS64Vec(MsnhProtoManager* manager, char* key, int64_t** i64, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setS64Vec(MsnhProtoManager* manager, char* key, int64_t* i64, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int getF32Vec(MsnhProtoManager* manager, char* key, float** f32, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setF32Vec(MsnhProtoManager* manager, char* key, float* f32, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int getF64Vec(MsnhProtoManager* manager, char* key, double** f64, uint32_t* size, char** msg);

extern "C" MSNH_PROTO_API int setF64Vec(MsnhProtoManager* manager, char* key, double* f64, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int fromBytes(MsnhProtoManager* manager, uint8_t* u8, uint32_t size, char** msg);

extern "C" MSNH_PROTO_API int serialize(MsnhProtoManager* manager, uint8_t** u8, uint32_t* size, char** msg);

#endif // !MSNH_PROTO_LIB
