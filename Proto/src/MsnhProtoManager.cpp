﻿#include <MsnhProto/MsnhProtoManager.h>

MsnhProtoManager::MsnhProtoManager(uint32_t typeID, MsnhProtoType type)
{
    string typeName = MsnhProtoFile::typeID2TypeNameMap[typeID];

    this->_frameType = type;

    if(type == IS_CMD)
    {
        map<string, MsnhProtoFrame> cmdFrameLib = MsnhProtoFile::cmdFrameLib;
        auto iter = cmdFrameLib.find(typeName);
        if(iter == cmdFrameLib.end())
        {
            throw MsnhProtoException("msnh proto cmd frame does not exist >> key: " + typeName);
        }
        _frame = MsnhProtoFile::cmdFrameLib[typeName];
    }
    else if(type == IS_DATA)
    {
        map<string, MsnhProtoFrame> dataFrameLib = MsnhProtoFile::dataFrameLib;

        auto iter = dataFrameLib.find(typeName);

        if(iter == dataFrameLib.end())
        {
            throw MsnhProtoException("msnh proto data frame does not exist >> key: " + typeName);
        }

        _frame = MsnhProtoFile::dataFrameLib[typeName];
    }
}

MsnhProtoManager::MsnhProtoManager(string typeName,MsnhProtoType type)
{
    this->_frameType = type;

    if(type == IS_CMD)
    {
        map<string, MsnhProtoFrame> cmdFrameLib = MsnhProtoFile::cmdFrameLib;
        auto iter = cmdFrameLib.find(typeName);
        if(iter == cmdFrameLib.end())
        {
            throw MsnhProtoException("msnh proto cmd frame does not exist >> key: " + typeName);
        }
        _frame = MsnhProtoFile::cmdFrameLib[typeName];
    }
    else if(type == IS_DATA)
    {
        map<string, MsnhProtoFrame> dataFrameLib = MsnhProtoFile::dataFrameLib;

        auto iter = dataFrameLib.find(typeName);

        if(iter == dataFrameLib.end())
        {
            throw MsnhProtoException("msnh proto data frame does not exist >> key: " + typeName);
        }

        _frame = MsnhProtoFile::dataFrameLib[typeName];
    }
}

MsnhProtoManager::MsnhProtoManager()
{

}

MsnhProtoManager::~MsnhProtoManager()
{

}

void MsnhProtoManager::fromBytes(const vector<uint8_t> &dataVec)
{
    if(dataVec[0]==0)
    {
        _frameType = MsnhProtoType::IS_CMD;
    }
    else if(dataVec[0]==1)
    {
        _frameType = MsnhProtoType::IS_DATA;
    }

    _frame = MsnhProto::deserialize(dataVec);
}

std::vector<uint8_t> MsnhProtoManager::serialize()
{
    return MsnhProto::serialize(_frame);
}

string MsnhProtoManager::getTypeName()
{
    return _frame.typeName;
}

uint32_t MsnhProtoManager::getTypeID()
{
    return _frame.typeID;
}

uint8_t MsnhProtoManager::getU8(const string& key)
{
    auto iter = _frame.u8Map.find(key);
    if(iter == _frame.u8Map.end())
    {
        throw MsnhProtoException("cmd frame u8 does not exist >> key: " + key);
    }

    return _frame.u8Map[key];
}

void MsnhProtoManager::setU8(const string& key, uint8_t data)
{
    auto iter = _frame.u8Map.find(key);
    if(iter == _frame.u8Map.end())
    {
        throw MsnhProtoException("cmd frame u8 does not exist >> key: " + key);
    }

    _frame.u8Map[key] = data;
}

int8_t MsnhProtoManager::getS8(const string& key)
{
    auto iter = _frame.s8Map.find(key);
    if(iter == _frame.s8Map.end())
    {
        throw MsnhProtoException("cmd frame s8 does not exist >> key: " + key);
    }

    return _frame.s8Map[key];
}

void MsnhProtoManager::setS8(const string& key, int8_t data)
{
    auto iter = _frame.s8Map.find(key);
    if(iter == _frame.s8Map.end())
    {
        throw MsnhProtoException("cmd frame s8 does not exist >> key: " + key);
    }

    _frame.s8Map[key] = data;
}

uint16_t MsnhProtoManager::getU16(const string& key)
{
    auto iter = _frame.u16Map.find(key);
    if(iter == _frame.u16Map.end())
    {
        throw MsnhProtoException("cmd frame u16 does not exist >> key: " + key);
    }

    return _frame.u16Map[key];
}

void MsnhProtoManager::setU16(const string& key, uint16_t data)
{
    auto iter = _frame.u16Map.find(key);
    if(iter == _frame.u16Map.end())
    {
        throw MsnhProtoException("cmd frame u16 does not exist >> key: " + key);
    }

    _frame.u16Map[key] = data;
}

int16_t MsnhProtoManager::getS16(const string& key)
{
    auto iter = _frame.s16Map.find(key);
    if(iter == _frame.s16Map.end())
    {
        throw MsnhProtoException("cmd frame s16 does not exist >> key: " + key);
    }

    return _frame.s16Map[key];
}

void MsnhProtoManager::setS16(const string& key, int16_t data)
{
    auto iter = _frame.s16Map.find(key);
    if(iter == _frame.s16Map.end())
    {
        throw MsnhProtoException("cmd frame s16 does not exist >> key: " + key);
    }

    _frame.s16Map[key] = data;
}

uint32_t MsnhProtoManager::getU32(const string& key)
{
    auto iter = _frame.u32Map.find(key);
    if(iter == _frame.u32Map.end())
    {
        throw MsnhProtoException("cmd frame u32 does not exist >> key: " + key);
    }

    return _frame.u32Map[key];
}

void MsnhProtoManager::setU32(const string& key, uint32_t data)
{
    auto iter = _frame.u32Map.find(key);
    if(iter == _frame.u32Map.end())
    {
        throw MsnhProtoException("cmd frame u32 does not exist >> key: " + key);
    }

    _frame.u32Map[key] = data;
}

int32_t MsnhProtoManager::getS32(const string& key)
{
    auto iter = _frame.s32Map.find(key);
    if(iter == _frame.s32Map.end())
    {
        throw MsnhProtoException("cmd frame s32 does not exist >> key: " + key);
    }

    return _frame.s32Map[key];
}

void MsnhProtoManager::setS32(const string& key, int32_t data)
{
    auto iter = _frame.s32Map.find(key);
    if(iter == _frame.s32Map.end())
    {
        throw MsnhProtoException("cmd frame s32 does not exist >> key: " + key);
    }

    _frame.s32Map[key] = data;
}

uint64_t MsnhProtoManager::getU64(const string& key)
{
    auto iter = _frame.u64Map.find(key);
    if(iter == _frame.u64Map.end())
    {
        throw MsnhProtoException("cmd frame u64 does not exist >> key: " + key);
    }

    return _frame.u64Map[key];
}

void MsnhProtoManager::setU64(const string& key, uint64_t data)
{
    auto iter = _frame.u64Map.find(key);
    if(iter == _frame.u64Map.end())
    {
        throw MsnhProtoException("cmd frame u64 does not exist >> key: " + key);
    }

    _frame.u64Map[key] = data;
}

int64_t MsnhProtoManager::getS64(const string& key)
{
    auto iter = _frame.s64Map.find(key);
    if(iter == _frame.s64Map.end())
    {
        throw MsnhProtoException("cmd frame s64 does not exist >> key: " + key);
    }

    return _frame.s64Map[key];
}

void MsnhProtoManager::setS64(const string& key, int64_t data)
{
    auto iter = _frame.s64Map.find(key);
    if(iter == _frame.s64Map.end())
    {
        throw MsnhProtoException("cmd frame s64 does not exist >> key: " + key);
    }

    _frame.s64Map[key] = data;
}

float MsnhProtoManager::getF32(const string& key)
{
    auto iter = _frame.f32Map.find(key);
    if(iter == _frame.f32Map.end())
    {
        throw MsnhProtoException("cmd frame f32 does not exist >> key: " + key);
    }

    return _frame.f32Map[key];
}

void MsnhProtoManager::setF32(const string& key, float data)
{
    auto iter = _frame.f32Map.find(key);
    if(iter == _frame.f32Map.end())
    {
        throw MsnhProtoException("cmd frame f32 does not exist >> key: " + key);
    }

    _frame.f32Map[key] = data;
}

double MsnhProtoManager::getF64(const string& key)
{
    auto iter = _frame.f64Map.find(key);
    if(iter == _frame.f64Map.end())
    {
        throw MsnhProtoException("cmd frame f64 does not exist >> key: " + key);
    }

    return _frame.f64Map[key];
}

void MsnhProtoManager::setF64(const string& key, double data)
{
    auto iter = _frame.f64Map.find(key);
    if(iter == _frame.f64Map.end())
    {
        throw MsnhProtoException("cmd frame f64 does not exist >> key: " + key);
    }

    _frame.f64Map[key] = data;
}

vector<uint8_t> MsnhProtoManager::getU8Vec(const string& key)
{
    auto iter = _frame.u8ListMap.find(key);
    if(iter == _frame.u8ListMap.end())
    {
        throw MsnhProtoException("cmd frame u8 vector does not exist >> key: " + key);
    }

    return _frame.u8ListMap[key];
}

void MsnhProtoManager::setU8Vec(const string& key,const vector<uint8_t> &dataVec)
{
    auto iter = _frame.u8ListMap.find(key);
    if(iter == _frame.u8ListMap.end())
    {
        throw MsnhProtoException("cmd frame u8 vector does not exist >> key: " + key);
    }

    _frame.u8ListMap[key] = dataVec;
}

vector<int8_t> MsnhProtoManager::getS8Vec(const string& key)
{
    auto iter = _frame.s8ListMap.find(key);
    if(iter == _frame.s8ListMap.end())
    {
        throw MsnhProtoException("cmd frame s8 vector does not exist >> key: " + key);
    }

    return _frame.s8ListMap[key];
}

void MsnhProtoManager::setS8Vec(const string& key,const vector<int8_t> &dataVec)
{
    auto iter = _frame.s8ListMap.find(key);
    if(iter == _frame.s8ListMap.end())
    {
        throw MsnhProtoException("cmd frame s8 vector does not exist >> key: " + key);
    }

    _frame.s8ListMap[key] = dataVec;
}

vector<uint16_t> MsnhProtoManager::getU16Vec(const string& key)
{
    auto iter = _frame.u16ListMap.find(key);
    if(iter == _frame.u16ListMap.end())
    {
        throw MsnhProtoException("cmd frame u16 vector does not exist >> key: " + key);
    }

    return _frame.u16ListMap[key];
}

void MsnhProtoManager::setU16Vec(const string& key,const vector<uint16_t> &dataVec)
{
    auto iter = _frame.u16ListMap.find(key);
    if(iter == _frame.u16ListMap.end())
    {
        throw MsnhProtoException("cmd frame u16 vector does not exist >> key: " + key);
    }

    _frame.u16ListMap[key] = dataVec;
}

vector<int16_t> MsnhProtoManager::getS16Vec(const string& key)
{
    auto iter = _frame.s16ListMap.find(key);
    if(iter == _frame.s16ListMap.end())
    {
        throw MsnhProtoException("cmd frame s16 vector does not exist >> key: " + key);
    }

    return _frame.s16ListMap[key];
}

void MsnhProtoManager::setS16Vec(const string& key,const vector<int16_t> &dataVec)
{
    auto iter = _frame.s16ListMap.find(key);
    if(iter == _frame.s16ListMap.end())
    {
        throw MsnhProtoException("cmd frame s16 vector does not exist >> key: " + key);
    }

    _frame.s16ListMap[key] = dataVec;
}

vector<uint32_t> MsnhProtoManager::getU32Vec(const string& key)
{
    auto iter = _frame.u32ListMap.find(key);
    if(iter == _frame.u32ListMap.end())
    {
        throw MsnhProtoException("cmd frame u32 vector does not exist >> key: " + key);
    }

    return _frame.u32ListMap[key];
}

void MsnhProtoManager::setU32Vec(const string& key,const vector<uint32_t> &dataVec)
{
    auto iter = _frame.u32ListMap.find(key);
    if(iter == _frame.u32ListMap.end())
    {
        throw MsnhProtoException("cmd frame u32 vector does not exist >> key: " + key);
    }

    _frame.u32ListMap[key] = dataVec;
}

vector<int32_t> MsnhProtoManager::getS32Vec(const string& key)
{
    auto iter = _frame.s32ListMap.find(key);
    if(iter == _frame.s32ListMap.end())
    {
        throw MsnhProtoException("cmd frame s32 vector does not exist >> key: " + key);
    }

    return _frame.s32ListMap[key];
}

void MsnhProtoManager::setS32Vec(const string& key,const vector<int32_t> &dataVec)
{
    auto iter = _frame.s32ListMap.find(key);
    if(iter == _frame.s32ListMap.end())
    {
        throw MsnhProtoException("cmd frame s32 vector does not exist >> key: " + key);
    }

    _frame.s32ListMap[key] = dataVec;
}

vector<uint64_t> MsnhProtoManager::getU64Vec(const string& key)
{
    auto iter = _frame.u64ListMap.find(key);
    if(iter == _frame.u64ListMap.end())
    {
        throw MsnhProtoException("cmd frame u64 vector does not exist >> key: " + key);
    }

    return _frame.u64ListMap[key];
}

void MsnhProtoManager::setU64Vec(const string& key,const vector<uint64_t> &dataVec)
{
    auto iter = _frame.u64ListMap.find(key);
    if(iter == _frame.u64ListMap.end())
    {
        throw MsnhProtoException("cmd frame u64 vector does not exist >> key: " + key);
    }

    _frame.u64ListMap[key] = dataVec;
}

vector<int64_t> MsnhProtoManager::getS64Vec(const string& key)
{
    auto iter = _frame.s64ListMap.find(key);
    if(iter == _frame.s64ListMap.end())
    {
        throw MsnhProtoException("cmd frame s64 vector does not exist >> key: " + key);
    }

    return _frame.s64ListMap[key];
}

void MsnhProtoManager::setS64Vec(const string& key,const vector<int64_t> &dataVec)
{
    auto iter = _frame.s64ListMap.find(key);
    if(iter == _frame.s64ListMap.end())
    {
        throw MsnhProtoException("cmd frame s64 vector does not exist >> key: " + key);
    }

    _frame.s64ListMap[key] = dataVec;
}

vector<float> MsnhProtoManager::getF32Vec(const string& key)
{
    auto iter = _frame.f32ListMap.find(key);
    if(iter == _frame.f32ListMap.end())
    {
        throw MsnhProtoException("cmd frame f32 vector does not exist >> key: " + key);
    }

    return _frame.f32ListMap[key];
}

void MsnhProtoManager::setF32Vec(const string& key,const vector<float> &dataVec)
{
    auto iter = _frame.f32ListMap.find(key);
    if(iter == _frame.f32ListMap.end())
    {
        throw MsnhProtoException("cmd frame f32 vector does not exist >> key: " + key);
    }

    _frame.f32ListMap[key] = dataVec;
}

vector<double> MsnhProtoManager::getF64Vec(const string& key)
{
    auto iter = _frame.f64ListMap.find(key);
    if(iter == _frame.f64ListMap.end())
    {
        throw MsnhProtoException("cmd frame f64 vector does not exist >> key: " + key);
    }

    return _frame.f64ListMap[key];
}

void MsnhProtoManager::setF64Vec(const string& key,const vector<double> &dataVec)
{
    auto iter = _frame.f64ListMap.find(key);
    if(iter == _frame.f64ListMap.end())
    {
        throw MsnhProtoException("cmd frame f64 vector does not exist >> key: " + key);
    }

    _frame.f64ListMap[key] = dataVec;
}

string MsnhProtoManager::getStr(const string& key)
{
    auto iter = _frame.strMap.find(key);
    if(iter == _frame.strMap.end())
    {
        throw MsnhProtoException("cmd frame str does not exist >> key: " + key);
    }

    return _frame.strMap[key];
}

void MsnhProtoManager::setStr(const string& key, string data)
{
    auto iter = _frame.strMap.find(key);
    if(iter == _frame.strMap.end())
    {
        throw MsnhProtoException("cmd frame str does not exist >> key: " + key);
    }

    _frame.strMap[key] = data;
}

vector<string> MsnhProtoManager::getStrVec(const string& key)
{
    auto iter = _frame.strListMap.find(key);
    if(iter == _frame.strListMap.end())
    {
        throw MsnhProtoException("cmd frame str vector does not exist >> key: " + key);
    }

    return _frame.strListMap[key];
}

void MsnhProtoManager::setStrVec(const string& key,const vector<string> &dataVec)
{
    auto iter = _frame.strListMap.find(key);
    if(iter == _frame.strListMap.end())
    {
        throw MsnhProtoException("cmd frame str vector does not exist >> key: " + key);
    }
    _frame.strListMap[key]=dataVec;
}

MsnhProtoFrame MsnhProtoManager::getFrame() const
{
    return _frame;
}

MsnhProtoType MsnhProtoManager::getFrameType() const
{
    return _frameType;
}

