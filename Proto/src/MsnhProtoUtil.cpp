﻿#ifdef _WIN32
#include <winsock2.h>
#include <time.h>
#include <direct.h>
#include <io.h>

#endif
#ifdef __linux__
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>
#endif
#include <MsnhProto/MsnhProtoUtil.h>

#include <regex>

MsnhProtoUtil::MsnhProtoUtil()
{

}

string MsnhProtoUtil::getPWD()
{
#ifdef _WIN32
    char current_address[999];
    memset(current_address, 0, 999);
    getcwd(current_address, 999); //获取当前路径
    cout<<current_address<<endl;
    return string(current_address);
#endif

#ifdef __linux__
    //DIR *dir;
    char basePath[999];

    ///get the current absoulte path
    memset(basePath, '\0', sizeof(basePath));
    getcwd(basePath, 999);
    return string(basePath);
#endif

}

uint32_t MsnhProtoUtil::getCurrentTimeMsec()
{
    time_t ltime;
    time(&ltime);
    tm* today =localtime(&ltime);
#ifdef _WIN32
    struct timeval tv;
    SYSTEMTIME wtm;
    GetLocalTime(&wtm);
    tv.tv_usec = wtm.wMilliseconds * 1000;
    return (uint32_t)(today->tm_hour*3600*1000+today->tm_min*60*1000+today->tm_sec*1000 + tv.tv_usec / 1000 % 1000);
#else
    struct timeval tv;
    gettimeofday(&tv,nullptr);

    return static_cast<uint32_t>((today->tm_hour*3600*1000+today->tm_min*60*1000+today->tm_sec*1000+tv.tv_usec / 1000 % 1000));
#endif
}

string MsnhProtoUtil::getCurrentTime()
{
    uint32_t time = getCurrentTimeMsec();
    string timeStr = "";
    uint8_t h  = static_cast<uint8_t>(time/3600000);
    uint8_t m  = static_cast<uint8_t>(time%3600000/60000);
    uint8_t s  = static_cast<uint8_t>(time%3600000%60000/1000);
    uint8_t ms = static_cast<uint8_t>(time%3600000%60000%1000);

    string hStr = "";
    string mStr = "";
    string sStr = "";
    string msStr = "";

    if(h<10)
    {
        hStr = "0"+to_string(h);
    }
    else
    {
        hStr = to_string(h);
    }

    if(m<10)
    {
        mStr = "0"+to_string(m);
    }
    else
    {
        mStr = to_string(m);
    }

    if(s<10)
    {
        sStr = "0"+to_string(s);
    }
    else
    {
        sStr = to_string(s);
    }

    if(ms<10)
    {
        msStr = "00"+to_string(ms);
    }
    else if(ms>=10 && ms<100)
    {
        msStr = "0"+to_string(ms);
    }
    else
    {
        msStr = to_string(ms);
    }

    timeStr = timeStr + hStr + ":" + mStr +":" + sStr + "." +msStr;
    return timeStr;
}

UInt32 MsnhProtoUtil::getCurrentTimeMsecHex()
{
    uint32_t millis = getCurrentTimeMsec();

    UInt32 val;
    val.data = millis;
    return val;
}

string MsnhProtoUtil::deleteAllMark(string s, const string mark)
{
    size_t nSize = mark.size();
    while(1)
    {
        size_t pos = s.find(mark);    //  尤其是这里
        if(pos == string::npos)
        {
            return s;
        }

        s.erase(pos, nSize);
    }
}

string MsnhProtoUtil::trim(string s)
{
    s = deleteAllMark(s,"\t");
    s = deleteAllMark(s,"\r");
    s = deleteAllMark(s,"\n");
    s = deleteAllMark(s," ");
    return s;
}

vector<string> MsnhProtoUtil::splitString(const string &s, const string &c)
{
    string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    vector<string> v;
    while(string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2-pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if(pos1 != s.length())
        v.push_back(s.substr(pos1));

    return v;
}

string MsnhProtoUtil::trimLeftRight(const string &s)
{
    string outStr;


    if (!s.empty())
    {
        string::size_type start = s.find_first_not_of(STR_WHITESPACE);


        // If there is only white space we return an empty string
        if (start != string::npos)
        {
            string::size_type end = s.find_last_not_of(STR_WHITESPACE);
            outStr = s.substr(start, end - start + 1);
        }
    }


    return outStr;
}

string MsnhProtoUtil::combineSpace(const string &s)
{
    string result = "";
    const char* str = s.c_str(); //保证以'\0'结尾
    for(int i = 0;str[i] != '\0';i++)
    {
        if(str[i] != ' ' )
            result.append(1,str[i]);
        else
            if(str[i+1] != ' ')
                result.append(1,str[i]);
    }
    return result;
}

bool MsnhProtoUtil::nameOK(const string &str)
{
    std::regex nameReg("_?[A-Za-z_$\\d]+$");//命名规范
    return std::regex_match(str,nameReg);
}

bool MsnhProtoUtil::isPosInt(const string &str)
{
    std::regex uintReg("^[1-9]\\d*|0$");//非负整数
    return std::regex_match(str,uintReg);
}

bool MsnhProtoUtil::isInt(const string &str)
{
    std::regex intReg("^-?[1-9]\\d*$|-?0");//整数
    return std::regex_match(str,intReg);
}

bool MsnhProtoUtil::isFloat(const string &str)
{
    std::regex floatReg("[-+]?[0-9]*.?[0-9]+([eE][-+]?[0-9]+)?");
    return std::regex_match(str,floatReg);
}

string MsnhProtoUtil::strReplace(const string &str, const string &strsrc, const string &strdst)
{
    string result = str;
    string::size_type pos = 0;//位置
    string::size_type srclen = strsrc.size();//要替换的字符串大小
    string::size_type dstlen = strdst.size();//目标字符串大小
    while((pos = result.find(strsrc,pos)) != string::npos)
    {
        result.replace(pos,srclen,strdst);
        pos += dstlen;
    }

    return result;
}

bool MsnhProtoUtil::replace(string &str, const string &from, const string &to)
{
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}
