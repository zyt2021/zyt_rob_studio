from ctypes import *
import platform
import sys
import copy
import numpy as np

osStr = sys.platform

if osStr == "win32" :
    dll = CDLL("MsnhProto.dll")
elif osStr == "darwin" :
    dll = CDLL("libMsnhProto.dylib")
else:
    dll = CDLL("libMsnhProto.so")


class PyMsnhProtoInit:
    def __init__(self, x):
        
        # int initProtoTypes(char** msg)
        dll.initProtoTypes.argtypes   = [POINTER(c_void_p)]
        dll.initProtoTypes.restype    =  c_int

        # int initProtoTypesPath(char* path, char** msg)
        dll.initProtoTypesPath.argtypes  = [c_char_p, POINTER(c_void_p)]
        dll.initProtoTypesPath.restype   = c_int

    @classmethod
    def initLib(self):
        msg  = c_char_p()
        res  = dll.initProtoTypes(byref(msg))
        if res != 1:
            raise Exception(msg.value)
    
    @classmethod
    def initLibPath(self, path):
        msg  = c_char_p()
        m_path = c_char_p()
        m_path.value = bytes(path,encoding='utf-8')
        res  = dll.initProtoTypesPath(m_path, byref(tmpMsg))
        if res != 1:
            raise Exception(msg.value)

class PyMsnhProto:
    DATA = 1
    CMD  = 2
    '''
    type: 1 data, 2 cmd
    '''
    def __init__(self,ID=None,name=None,type=None):
        # void *createManager()
        dll.createManager.restype    = c_void_p

        # void freeMem(void* ptr);
        dll.freeMem.argtypes = [c_void_p]

        # void *createManagerWithID(uint32_t ID, uint8_t type)
        dll.createManagerWithID.argtypes = [c_uint32, c_uint8]
        dll.createManagerWithID.restype  = c_void_p

        # void *createManagerWithName(char *name, uint8_t type)
        dll.createManagerWithName.argtypes = [c_char_p, c_uint8]
        dll.createManagerWithName.restype  = c_void_p

        # int getTypeName(MsnhProtoManager* manager, char** name)
        dll.getTypeName.argtypes = [c_void_p, POINTER(c_void_p)]
        dll.getTypeName.restype  = c_int

        # int getTypeID(MsnhProtoManager* manager, uint32_t* id);
        dll.getTypeID.argtypes = [c_void_p, POINTER(c_uint32)]
        dll.getTypeID.restype  = c_int

        # int getU8(MsnhProtoManager* manager, char* key, uint8_t* u8, char** msg);
        dll.getU8.argtypes = [c_void_p, c_char_p, POINTER(c_uint8), POINTER(c_void_p)]
        dll.getU8.restype  = c_int

        # int setU8(MsnhProtoManager* manager, char* key, uint8_t data, char** msg);
        dll.setU8.argtypes = [c_void_p, c_char_p, c_uint8, POINTER(c_void_p)]
        dll.setU8.restype  = c_int

        # int getS8(MsnhProtoManager* manager, char* key, int8_t* u8, char** msg);
        dll.getS8.argtypes = [c_void_p, c_char_p, POINTER(c_int8), POINTER(c_void_p)]
        dll.getS8.restype  = c_int

        # int setS8(MsnhProtoManager* manager, char* key, int8_t data, char** msg);
        dll.setS8.argtypes = [c_void_p, c_char_p, c_int8, POINTER(c_void_p)]
        dll.setS8.restype  = c_int

        # int getU16(MsnhProtoManager* manager, char* key, uint16_t* u16, char** msg);
        dll.getU16.argtypes = [c_void_p, c_char_p, POINTER(c_uint16), POINTER(c_void_p)]
        dll.getU16.restype  = c_int

        # int setU16(MsnhProtoManager* manager, char* key, uint16_t data, char** msg);
        dll.setU16.argtypes = [c_void_p, c_char_p, c_uint16, POINTER(c_void_p)]
        dll.setU16.restype  = c_int

        # int getS16(MsnhProtoManager* manager, char* key, int16_t* u16, char** msg);
        dll.getS16.argtypes = [c_void_p, c_char_p, POINTER(c_int16), POINTER(c_void_p)]
        dll.getS16.restype  = c_int

        # int setS16(MsnhProtoManager* manager, char* key, int16_t data, char** msg);
        dll.setS16.argtypes = [c_void_p, c_char_p, c_int16, POINTER(c_void_p)]
        dll.setS16.restype  = c_int

        # int getU32(MsnhProtoManager* manager, char* key, uint32_t* u32, char** msg);
        dll.getU32.argtypes = [c_void_p, c_char_p, POINTER(c_uint32), POINTER(c_void_p)]
        dll.getU32.restype  = c_int

        # int setU32(MsnhProtoManager* manager, char* key, uint32_t data, char** msg);
        dll.setU32.argtypes = [c_void_p, c_char_p, c_uint32, POINTER(c_void_p)]
        dll.setU32.restype  = c_int

        # int getS32(MsnhProtoManager* manager, char* key, int32_t* u32, char** msg);
        dll.getS32.argtypes = [c_void_p, c_char_p, POINTER(c_int32), POINTER(c_void_p)]
        dll.getS32.restype  = c_int

        # int setS32(MsnhProtoManager* manager, char* key, int32_t data, char** msg);
        dll.setS32.argtypes = [c_void_p, c_char_p, c_int32, POINTER(c_void_p)]
        dll.setS32.restype  = c_int

        # int getU64(MsnhProtoManager* manager, char* key, uint64_t* u64, char** msg);
        dll.getU64.argtypes = [c_void_p, c_char_p, POINTER(c_uint64), POINTER(c_void_p)]
        dll.getU64.restype  = c_int

        # int setU64(MsnhProtoManager* manager, char* key, uint64_t data, char** msg);
        dll.setU64.argtypes = [c_void_p, c_char_p, c_uint64, POINTER(c_void_p)]
        dll.setU64.restype  = c_int

        # int getS64(MsnhProtoManager* manager, char* key, int64_t* u64, char** msg);
        dll.getS64.argtypes = [c_void_p, c_char_p, POINTER(c_int64), POINTER(c_void_p)]
        dll.getS64.restype  = c_int

        # int setS64(MsnhProtoManager* manager, char* key, int64_t data, char** msg);
        dll.setS64.argtypes = [c_void_p, c_char_p, c_int64, POINTER(c_void_p)]
        dll.setS64.restype  = c_int

        # int getF32(MsnhProtoManager* manager, char* key, float* u64, char** msg);
        dll.getF32.argtypes = [c_void_p, c_char_p, POINTER(c_float), POINTER(c_void_p)]
        dll.getF32.restype  = c_int

        # int setF32(MsnhProtoManager* manager, char* key, float data, char** msg);
        dll.setF32.argtypes = [c_void_p, c_char_p, c_float, POINTER(c_void_p)]
        dll.setF32.restype  = c_int

        # int getF64(MsnhProtoManager* manager, char* key, double* u64, char** msg);
        dll.getF64.argtypes = [c_void_p, c_char_p, POINTER(c_double), POINTER(c_void_p)]
        dll.getF64.restype  = c_int

        # int setF64(MsnhProtoManager* manager, char* key, double data, char** msg);
        dll.setF64.argtypes = [c_void_p, c_char_p, c_double, POINTER(c_void_p)]
        dll.setF64.restype  = c_int
        
        # int getStr(MsnhProtoManager* manager, char* key, char** d, char** msg);
        dll.getStr.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_void_p)]
        dll.getStr.restype  = c_int

        # int setStr(MsnhProtoManager* manager, char* key, char* data, char** msg);
        dll.setStr.argtypes = [c_void_p, c_char_p, c_char_p, POINTER(c_void_p)]
        dll.setStr.restype  = c_int

        # int getU8Vec(MsnhProtoManager* manager, char* key, uint8_t** u8, uint32_t* size, char** msg);
        dll.getU8Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getU8Vec.restype  = c_int

        # int setU8Vec(MsnhProtoManager* manager, char* key, uint8_t* u8, uint32_t size, char** msg);
        dll.setU8Vec.argtypes = [c_void_p, c_char_p, POINTER(c_uint8), c_uint32, POINTER(c_void_p)]
        dll.setU8Vec.restype  = c_int

        # int getU8Vec(MsnhProtoManager* manager, char* key, int8_t** i8, uint32_t* size, char** msg);
        dll.getS8Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getS8Vec.restype  = c_int

        # int setU8Vec(MsnhProtoManager* manager, char* key, int8_t* i8, uint32_t size, char** msg);
        dll.setS8Vec.argtypes = [c_void_p, c_char_p, POINTER(c_int8), c_uint32, POINTER(c_void_p)]
        dll.setS8Vec.restype  = c_int

        # int getU16Vec(MsnhProtoManager* manager, char* key, uint16_t** u16, uint32_t* size, char** msg);
        dll.getU16Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getU16Vec.restype  = c_int

        # int setU16Vec(MsnhProtoManager* manager, char* key, uint16_t* u16, uint32_t size, char** msg);
        dll.setU16Vec.argtypes = [c_void_p, c_char_p, POINTER(c_uint16), c_uint32, POINTER(c_void_p)]
        dll.setU16Vec.restype  = c_int

        # int getU16Vec(MsnhProtoManager* manager, char* key, int16_t** i16, uint32_t* size, char** msg);
        dll.getS16Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getS16Vec.restype  = c_int

        # int setU16Vec(MsnhProtoManager* manager, char* key, int16_t* i16, uint32_t size, char** msg);
        dll.setS16Vec.argtypes = [c_void_p, c_char_p, POINTER(c_int16), c_uint32, POINTER(c_void_p)]
        dll.setS16Vec.restype  = c_int

        # int getU32Vec(MsnhProtoManager* manager, char* key, uint32_t** u32, uint32_t* size, char** msg);
        dll.getU32Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getU32Vec.restype  = c_int

        # int setU32Vec(MsnhProtoManager* manager, char* key, uint32_t* u32, uint32_t size, char** msg);
        dll.setU32Vec.argtypes = [c_void_p, c_char_p, POINTER(c_uint32), c_uint32, POINTER(c_void_p)]
        dll.setU32Vec.restype  = c_int

        # int getU32Vec(MsnhProtoManager* manager, char* key, int32_t** i32, uint32_t* size, char** msg);
        dll.getS32Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getS32Vec.restype  = c_int

        # int setU32Vec(MsnhProtoManager* manager, char* key, int32_t* i32, uint32_t size, char** msg);
        dll.setS32Vec.argtypes = [c_void_p, c_char_p, POINTER(c_int32), c_uint32, POINTER(c_void_p)]
        dll.setS32Vec.restype  = c_int

        # int getU64Vec(MsnhProtoManager* manager, char* key, uint64_t** u64, uint32_t* size, char** msg);
        dll.getU64Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getU64Vec.restype  = c_int

        # int setU64Vec(MsnhProtoManager* manager, char* key, uint64_t* u64, uint32_t size, char** msg);
        dll.setU64Vec.argtypes = [c_void_p, c_char_p, POINTER(c_uint64), c_uint32, POINTER(c_void_p)]
        dll.setU64Vec.restype  = c_int

        # int getU64Vec(MsnhProtoManager* manager, char* key, int64_t** i64, uint32_t* size, char** msg);
        dll.getS64Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getS64Vec.restype  = c_int

        # int setU64Vec(MsnhProtoManager* manager, char* key, int64_t* i64, uint32_t size, char** msg);
        dll.setS64Vec.argtypes = [c_void_p, c_char_p, POINTER(c_int64), c_uint32, POINTER(c_void_p)]
        dll.setS64Vec.restype  = c_int

        # int getF32Vec(MsnhProtoManager* manager, char* key, float** f32, uint32_t* size, char** msg);
        dll.getF32Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getF32Vec.restype  = c_int

        # int setF32Vec(MsnhProtoManager* manager, char* key, float* f32, uint32_t size, char** msg);
        dll.setF32Vec.argtypes = [c_void_p, c_char_p, POINTER(c_float), c_uint32, POINTER(c_void_p)]
        dll.setF32Vec.restype  = c_int

        # int getF64Vec(MsnhProtoManager* manager, char* key, double** f64, uint32_t* size, char** msg);
        dll.getF64Vec.argtypes = [c_void_p, c_char_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.getF64Vec.restype  = c_int

        # int setF64Vec(MsnhProtoManager* manager, char* key, double* f64, uint32_t size, char** msg);
        dll.setF64Vec.argtypes = [c_void_p, c_char_p, POINTER(c_double), c_uint32, POINTER(c_void_p)]
        dll.setF64Vec.restype  = c_int

        # int serialize(MsnhProtoManager* manager, uint8_t** u8, uint32_t* size, char** msg);
        dll.serialize.argtypes = [c_void_p, POINTER(c_void_p), POINTER(c_uint32), POINTER(c_void_p)]
        dll.serialize.restype  = c_int

        # int fromBytes(MsnhProtoManager* manager, uint8_t* u8, uint32_t size, char** msg);
        dll.fromBytes.argtypes = [c_void_p, POINTER(c_uint8), c_uint32, POINTER(c_void_p)]
        dll.fromBytes.restype  = c_int
        
        if ID==None and name==None and type == None :
            self.obj = dll.createManager()
        elif name!=None and type !=None :
            m_name = bytes(name, encoding='utf-8')
            self.obj = dll.createManagerWithName(m_name, type)
        elif ID!=None and type!=None :
            self.obj = dll.createManagerWithID(ID, type)
        else:
            raise Exception('Params error')

    def getTypeName(self):
        # for thread safe
        tmpMsg = c_void_p()
        res = dll.getTypeName(self.obj,byref(tmpMsg))
        if res != 1:
            raise Exception('Object is null')

        mstr = cast(tmpMsg, c_char_p).value
        # to avoid memory leak
        dll.freeMem(tmpMsg)
        return mstr

    def getTypeID(self):
        id = c_uint32()
        res = dll.getTypeID(self.obj, byref(id))
        if res != 1:
            raise Exception('Object is null')
        return id.value

    def getU8(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        u8     = c_uint8()
        res = dll.getU8(self.obj,m_key,byref(u8),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return u8.value

    def setU8(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setU8(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
            
    def getS8(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        i8    = c_int8()
        res = dll.getS8(self.obj,m_key,byref(i8),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return i8.value

    def setS8(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setS8(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)

    def getU16(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        u16    = c_uint16()
        res = dll.getU16(self.obj,m_key,byref(u16),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return u16.value

    def setU16(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setU16(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
            
    def getS16(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        i16    = c_int16()
        res = dll.getS16(self.obj,m_key,byref(i16),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return i16.value

    def setS16(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setS16(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)

    def getU32(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        u32    = c_uint32()
        res = dll.getU32(self.obj,m_key,byref(u32),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return u32.value

    def setU32(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setU32(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
            
    def getS32(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        i32    = c_int32()
        res = dll.getS32(self.obj,m_key,byref(i32),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return i32.value

    def setS32(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setS32(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)

    def getU64(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        u64    = c_uint64()
        res = dll.getU64(self.obj,m_key,byref(u64),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return u64.value

    def setU64(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setU64(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
            
    def getS64(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        i64    = c_int64()
        res = dll.getS64(self.obj,m_key,byref(i64),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return i64.value

    def setS64(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setS64(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)

    def getF32(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        f32   = c_float()
        res = dll.getF32(self.obj,m_key,byref(f32),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return f32.value

    def setF32(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setF32(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)

    def getF64(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        f64   = c_double()
        res = dll.getF64(self.obj,m_key,byref(f64),byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)
        return f64.value

    def setF64(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setF64(self.obj,m_key,data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)

    def getStr(self, key):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        tmpStr = c_void_p()
        res = dll.getStr(self.obj,m_key,byref(tmpStr),byref(tmpMsg))

        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)

        mstr = copy.deepcopy(cast(tmpStr, c_char_p).value)
        dll.freeMem(tmpStr)
        if res != 1:
            raise Exception(msg)
        return mstr

    def setStr(self, key, data):
        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        m_data= bytes(data, encoding='utf-8')
        res = dll.setStr(self.obj,m_key,m_data,byref(tmpMsg))
        msg = copy.deepcopy(cast(tmpMsg, c_char_p).value)
        dll.freeMem(tmpMsg)
        if res != 1:
            raise Exception(msg)

    def getU8Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getU8Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        u8Vec = cast(voidVec,POINTER(c_uint8))
        data = []
        for i in range(size.value):
            data.append(u8Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def setU8Vec(self, key, data):
        m_data = np.array(data).astype(np.uint8)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_uint8))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setU8Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)

    def getS8Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getS8Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        i8Vec = cast(voidVec,POINTER(c_int8))
        data = []
        for i in range(size.value):
            data.append(i8Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def setS8Vec(self, key, data):
        m_data = np.array(data).astype(np.int8)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_int8))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setS8Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)

    def getU16Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getU16Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        u16Vec = cast(voidVec,POINTER(c_uint16))
        data = []
        for i in range(size.value):
            data.append(u16Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def setU16Vec(self, key, data):
        m_data = np.array(data).astype(np.uint16)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_uint16))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setU16Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)

    def getS16Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getS16Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        i16Vec = cast(voidVec,POINTER(c_int16))
        data = []
        for i in range(size.value):
            data.append(i16Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def setS16Vec(self, key, data):
        m_data = np.array(data).astype(np.int16)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_int16))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setS16Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)

    def getU32Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getU32Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        u32Vec = cast(voidVec,POINTER(c_uint32))
        data = []
        for i in range(size.value):
            data.append(u32Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data


    def setU32Vec(self, key, data):
        m_data = np.array(data).astype(np.uint32)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_uint32))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setU32Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)

    def getS32Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getS32Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        i32Vec = cast(voidVec,POINTER(c_int32))
        data = []
        for i in range(size.value):
            data.append(i32Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def setS32Vec(self, key, data):
        m_data = np.array(data).astype(np.int32)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_int32))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setS32Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)

    def getU64Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getU64Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        u64Vec = cast(voidVec,POINTER(c_uint64))
        data = []
        for i in range(size.value):
            data.append(u64Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def setU64Vec(self, key, data):
        m_data = np.array(data).astype(np.uint64)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_uint64))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setU64Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)

    def getS64Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getS64Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        i64Vec = cast(voidVec,POINTER(c_int64))
        data = []
        for i in range(size.value):
            data.append(i64Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def setS64Vec(self, key, data):
        m_data = np.array(data).astype(np.int64)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_int64))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setS64Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)
    
    def getF32Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getF32Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        f32Vec = cast(voidVec,POINTER(c_float))
        data = []
        for i in range(size.value):
            data.append(f32Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def setF32Vec(self, key, data):
        m_data = np.array(data).astype(np.float32)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_float))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setF32Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)

    def getF64Vec(self, key):
        m_key   = bytes(key, encoding='utf-8')
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.getF64Vec(self.obj,m_key,byref(voidVec),byref(size),byref(tmpMsg))
        f32Vec = cast(voidVec,POINTER(c_double))
        data = []
        for i in range(size.value):
            data.append(f32Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def setF64Vec(self, key, data):
        m_data = np.array(data).astype(np.float64)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_double))

        m_key = bytes(key, encoding='utf-8')
        tmpMsg = c_void_p()
        res = dll.setF64Vec(self.obj,m_key,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)

    def serialize(self):
        tmpMsg  = c_void_p()
        voidVec = c_void_p()
        size    = c_uint32()
        res = dll.serialize(self.obj,byref(voidVec),byref(size),byref(tmpMsg))
        u8Vec = cast(voidVec,POINTER(c_uint8))
        data = []
        for i in range(size.value):
            data.append(u8Vec[i])
        if res != 1:
            raise Exception(msg)
        dll.freeMem(voidVec)
        return data

    def fromBytes(self, data):
        m_data = np.array(data).astype(np.uint8)
        if not m_data.flags['C_CONTIGUOUS']:
            m_data = np.ascontiguous(m_data, dtype=m_data.dtype)
        data_ptr = cast(m_data.ctypes.data, POINTER(c_uint8))

        tmpMsg = c_void_p()
        res = dll.fromBytes(self.obj,data_ptr, data.__len__(), byref(tmpMsg))
        if res != 1:
            raise Exception(msg)
