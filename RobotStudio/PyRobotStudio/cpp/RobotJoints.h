#ifndef MSNH_PROTO_ROBOTJOINTS_H
#define MSNH_PROTO_ROBOTJOINTS_H

#include <string>
#include <stdint.h>
#include <MsnhProto/MsnhProtoManager.h>
#include "Base.h"

namespace MsnhProtocal
{
class RobotJoints: public Base
{
public:
    RobotJoints(MsnhProtoType type):Base(2,type){}
    virtual uint32_t getTypeID() override{return 2;}
    virtual std::string getTypeName() override{return "RobotJoints";}
    ///Do not use
    virtual void fromBytes(const std::vector<uint8_t> &bytes) override{(void)bytes;}
    //joints
    std::vector<float> getDataJoints() {return  _manager.getF32Vec("joints");}
    void setDataJoints(const std::vector<float>& value) { _manager.setF32Vec("joints",value);}

};
}


#endif