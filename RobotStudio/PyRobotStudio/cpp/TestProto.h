#ifndef MSNH_PROTO_TESTPROTO_H
#define MSNH_PROTO_TESTPROTO_H

#include <string>
#include <stdint.h>
#include <MsnhProto/MsnhProtoManager.h>
#include "Base.h"

namespace MsnhProtocal
{
class TestProto: public Base
{
public:
    TestProto(MsnhProtoType type):Base(999999999,type){}
    virtual uint32_t getTypeID() override{return 999999999;}
    virtual std::string getTypeName() override{return "TestProto";}
    ///Do not use
    virtual void fromBytes(const std::vector<uint8_t> &bytes) override{(void)bytes;}    //const pi
    float getPi() const {return 3.1415926f;}
    //const _pi
    double get_pi() const {return 3.141592611231321312;}
    //enum axis
    enum axis
    {
       TestProto_x,
       TestProto_y,
       TestProto_z,
    };

    //uu8
    uint8_t getDataUu8() {return  _manager.getU8("uu8");}
    void setDataUu8(const uint8_t& value) { _manager.setU8("uu8",value);}
    //ss8
    int8_t getDataSs8() {return  _manager.getS8("ss8");}
    void setDataSs8(const int8_t& value) { _manager.setS8("ss8",value);}
    //uu16
    uint16_t getDataUu16() {return  _manager.getU16("uu16");}
    void setDataUu16(const uint16_t& value) { _manager.setU16("uu16",value);}
    //ss16
    int16_t getDataSs16() {return  _manager.getS16("ss16");}
    void setDataSs16(const int16_t& value) { _manager.setS16("ss16",value);}
    //uu32
    uint32_t getDataUu32() {return  _manager.getU32("uu32");}
    void setDataUu32(const uint32_t& value) { _manager.setU32("uu32",value);}
    //ss32
    int32_t getDataSs32() {return  _manager.getS32("ss32");}
    void setDataSs32(const int32_t& value) { _manager.setS32("ss32",value);}
    //uu64
    uint64_t getDataUu64() {return  _manager.getU64("uu64");}
    void setDataUu64(const uint64_t& value) { _manager.setU64("uu64",value);}
    //ss64
    int64_t getDataSs64() {return  _manager.getS64("ss64");}
    void setDataSs64(const int64_t& value) { _manager.setS64("ss64",value);}
    //ff32
    float getDataFf32() {return  _manager.getF32("ff32");}
    void setDataFf32(const float& value) { _manager.setF32("ff32",value);}
    //ff64
    double getDataFf64() {return  _manager.getF64("ff64");}
    void setDataFf64(const double& value) { _manager.setF64("ff64",value);}
    //sstr
    std::string getDataSstr() {return  _manager.getStr("sstr");}
    void setDataSstr(const std::string& value) { _manager.setStr("sstr",value);}
    //u8Vec
    std::vector<uint8_t> getDataU8Vec() {return  _manager.getU8Vec("u8Vec");}
    void setDataU8Vec(const std::vector<uint8_t>& value) { _manager.setU8Vec("u8Vec",value);}
    //s8Vec
    std::vector<int8_t> getDataS8Vec() {return  _manager.getS8Vec("s8Vec");}
    void setDataS8Vec(const std::vector<int8_t>& value) { _manager.setS8Vec("s8Vec",value);}
    //u16Vec
    std::vector<uint16_t> getDataU16Vec() {return  _manager.getU16Vec("u16Vec");}
    void setDataU16Vec(const std::vector<uint16_t>& value) { _manager.setU16Vec("u16Vec",value);}
    //s16Vec
    std::vector<int16_t> getDataS16Vec() {return  _manager.getS16Vec("s16Vec");}
    void setDataS16Vec(const std::vector<int16_t>& value) { _manager.setS16Vec("s16Vec",value);}
    //u32Vec
    std::vector<uint32_t> getDataU32Vec() {return  _manager.getU32Vec("u32Vec");}
    void setDataU32Vec(const std::vector<uint32_t>& value) { _manager.setU32Vec("u32Vec",value);}
    //s32Vec
    std::vector<int32_t> getDataS32Vec() {return  _manager.getS32Vec("s32Vec");}
    void setDataS32Vec(const std::vector<int32_t>& value) { _manager.setS32Vec("s32Vec",value);}
    //u64Vec
    std::vector<uint64_t> getDataU64Vec() {return  _manager.getU64Vec("u64Vec");}
    void setDataU64Vec(const std::vector<uint64_t>& value) { _manager.setU64Vec("u64Vec",value);}
    //s64Vec
    std::vector<int64_t> getDataS64Vec() {return  _manager.getS64Vec("s64Vec");}
    void setDataS64Vec(const std::vector<int64_t>& value) { _manager.setS64Vec("s64Vec",value);}
    //f32Vec
    std::vector<float> getDataF32Vec() {return  _manager.getF32Vec("f32Vec");}
    void setDataF32Vec(const std::vector<float>& value) { _manager.setF32Vec("f32Vec",value);}
    //f64Vec
    std::vector<double> getDataF64Vec() {return  _manager.getF64Vec("f64Vec");}
    void setDataF64Vec(const std::vector<double>& value) { _manager.setF64Vec("f64Vec",value);}

};
}


#endif