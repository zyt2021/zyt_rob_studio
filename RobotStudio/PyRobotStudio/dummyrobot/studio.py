import serial
import serial.tools.list_ports
import sys
sys.path.append("..")
import time
from MsnhProto import *
from MsnhProtoDef import *
from nanomsg import Socket, PUB, SUB, SUB_SUBSCRIBE, PUSH
import time

PyMsnhProtoInit.initLib() #初始化MsnhProto库

sub = Socket(SUB)
sub.connect('ipc:///tmp/Dummy-URDF_0_tf00')
sub.set_string_option(SUB, SUB_SUBSCRIBE, b'')

pub = Socket(PUB)
pub.bind('ipc:///tmp/Dummy-URDF_0_joints')

frame1 = PyMsnhProto(robotJoints.Info.typeID, type=PyMsnhProto.DATA)
frame1.setF32Vec(robotJoints.Data.jointsF32Vec, [0,0,0,0,0,0])
cmds = frame1.serialize()
pub.send(bytes(cmds))

MaxBytes=1024*1024
frame = PyMsnhProto(robotJoints.Info.typeID, type=PyMsnhProto.DATA)

while True:#
	res = sub.recv()
	frame.fromBytes(list(res))
	a = frame.getF32Vec(robotJoints.Data.jointsF32Vec)
	print(a)
sub.close()
