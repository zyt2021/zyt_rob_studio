import os
import re
path = os.getcwd()+"\msnhproto"#获取当前路径
all_files = [f for f in os.listdir(path )]#输出根path下的所有文件名到一个列表中
#对各个文件进行处理

files = []

def getTypeStr(type,name):
    tp = type.lstrip().rstrip()
    nm = name.lstrip().rstrip()
    if tp == "u8":
        return nm+"U8"
    elif tp == "s8":
        return nm+"S8"
    elif tp == "u16":
        return nm+"U16"
    elif tp == "s16":
        return nm+"S16"
    elif tp == "u32":
        return nm+"U32"
    elif tp == "s32":
        return nm+"S32"
    elif tp == "u64":
        return nm+"U64"
    elif tp == "s64":
        return nm+"S64"
    elif tp == "f32":
        return nm+"F32"
    elif tp == "f64":
        return nm+"F64"
    elif tp == "str":
        return nm+"Str"
    elif tp == "u8[]":
        return nm+"U8Vec"
    elif tp == "s8[]":
        return nm+"S8Vec"
    elif tp == "u16[]":
        return nm+"U16Vec"
    elif tp == "s16[]":
        return nm+"S16Vec"
    elif tp == "u32[]":
        return nm+"U32Vec"
    elif tp == "s32[]":
        return nm+"S32Vec"
    elif tp == "u64[]":
        return nm+"U64Vec"
    elif tp == "s64[]":
        return nm+"S64Vec"
    elif tp == "f32[]":
        return nm+"F32Vec"
    elif tp == "f64[]":
        return nm+"F64Vec"
    elif tp == "str[]":
        return nm+"StrVec"

def getTypeName(str):
    tp = str.split(" ")[0]

    if tp == "u8":
        return "uint8_t"
    elif tp == "s8":
        return "int8_t"
    elif tp == "u16":
        return "uint16_t"
    elif tp == "s16":
        return "int16_t"
    elif tp == "u32":
        return "uint32_t"
    elif tp == "s32":
        return "int32_t"
    elif tp == "u64":
        return "uint64_t"
    elif tp == "s64":
        return "int64_t"
    elif tp == "f32":
        return "float"
    elif tp == "f64":
        return "double"
    elif tp == "str":
        return "std::string"

for i in all_files:
    if i.split(".")[1] == "msnhproto":
        files.append(path+"\\"+i)
with open("MsnhProtoDef.h", 'w', encoding='utf-8') as f:
    with open("MsnhProtoDef.cpp", 'w', encoding='utf-8') as fcpp:
        f.write("#ifndef MSNH_PROTO_H\n#define MSNH_PROTO_H\n\n#include <string>\n#include <stdint.h>\n\n")
        fcpp.write("#include \"MsnhProtoDef.h\"\n")

        for pf in files:
            with open(pf,'r') as ff:
                reads = ff.readlines()
                contents = []
                for r in reads:
                    if re.match("//.*",r) :
                        continue
                    res = re.findall("//.*",r)
                    if len(res) != 0:
                        for rr in res:
                            r = r.replace(rr,"")

                    contents.append(r.replace('\n','').replace('\r','').lstrip().rstrip())


                if contents.count("#") != 4:
                    raise ValueError("proto file err :"+pf)
                if not contents[0].__contains__("typeID") :
                    raise ValueError("proto file err :"+pf)
                if not contents[1].__contains__("typeName") :
                    raise ValueError("proto file err :"+pf)
                f.write("//============================ -" + pf.split(".")[0].split("-")[1] + "-\n")
                fcpp.write("//============================ -" + pf.split(".")[0].split("-")[1] + "-\n")
                f.write("namespace "+pf.split(".")[0].split("-")[1] + "\n")
                f.write("{\n")
                
                cnt = 0
                idx = []
                for con in contents:
                    if con == "#":
                        idx.append(cnt)
                    cnt = cnt + 1
                
                info = contents[0:2]
                info[0] = ' '.join(info[0].split())
                info[1] = ' '.join(info[1].split())

                const = []
                data = []
                cmd  = []
                const = contents[3:idx[1]]
                data  = contents[idx[1]+1:idx[2]]
                cmd   = contents[idx[2]+1:idx[3]]


                # Info Data Cmd
                f.write("class Info\n{\n")
                f.write( "public:\n")
                f.write( "  static uint32_t typeID;\n")
                f.write( "  static std::string typeName;\n")
                f.write("};\n")

                fcpp.write("uint32_t "+pf.split(".")[0].split("-")[1]+"::Info::typeID = " + info[0].split(" ")[1][0:-1] + ";\n")
                fcpp.write("std::string "+pf.split(".")[0].split("-")[1]+"::Info::typeName = \"" + info[1].split(" ")[1][0:-1] + "\";\n")

                f.write("class Const\n{\n")
                f.write( "public:\n")
                for inf in const:
                    line = inf.split(" ")
                    if line[0] == "const":
                        f.write( "  const static " + getTypeName(line[1])+" "+ getTypeStr(line[1],line[2])+ ";\n")
                    if line[0] == "enum":
                        f.write( "  enum "+line[1]+"\n")
                        f.write( "   {\n")
                        enums = line[2][1:-2].split(",")
                        for e in enums:
                            f.write("       "+pf.split(".")[0].split("-")[1]+"_"+e+",\n")
                        f.write("   };\n")
                f.write("};\n")

                for inf in const:
                    line = inf.split(" ")
                    if line[0] == "const":
                        if line[1] == "str":
                            fcpp.write("const "+getTypeName(line[1]) +" "+pf.split(".")[0].split("-")[1]+"::Const::"+ getTypeStr(line[1],line[2]) + " = \"" + line[3][0:-1]  + "\";\n")
                        elif line[1] == "f32":
                            fcpp.write("const "+getTypeName(line[1]) +" "+pf.split(".")[0].split("-")[1]+"::Const::"+ getTypeStr(line[1],line[2]) + " = " + line[3][0:-1]  + "f;\n")
                        else:
                            fcpp.write("const "+getTypeName(line[1]) +" "+pf.split(".")[0].split("-")[1]+"::Const::"+ getTypeStr(line[1],line[2]) + " = " + line[3][0:-1]  + ";\n")

                f.write("class Data\n{\n")
                f.write( "public:\n")
                for inf in data:
                    line = inf.split(" ")
                    f.write( "  static std::string "+ getTypeStr(line[0],line[1][0:-1])+ ";\n")
                f.write("};\n")

                for inf in data:
                    if inf == '':
                        continue
                    line = inf.split(" ")
                    fcpp.write( "std::string "+pf.split(".")[0].split("-")[1]+"::Data::"+ getTypeStr(line[0],line[1][0:-1]) + " = \"" + line[1][0:-1]  + "\";\n")

                f.write("class Cmd\n{\n")
                f.write( "public:\n")
                for inf in cmd:
                    if inf == '':
                        continue
                    line = inf.split(" ")
                    f.write( "  static std::string "+ getTypeStr(line[0],line[1][0:-1])+ ";\n")
                f.write("};\n")

                for inf in cmd:
                    if inf == "" :
                        continue
                    line = inf.split(" ")
                    fcpp.write( "std::string "+pf.split(".")[0].split("-")[1]+"::Cmd::"+ getTypeStr(line[0],line[1][0:-1]) + " = \"" + line[1][0:-1] + "\";\n")
                f.write("}\n")        
                f.write("//============================\n\n")
                fcpp.write("//============================\n\n")

        fcpp.flush()
        fcpp.close()

    f.write("\n\n#endif")
    f.flush()
    f.close()
