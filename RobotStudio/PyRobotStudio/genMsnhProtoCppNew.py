import os
import re
path = os.getcwd()+"\msnhproto"#获取当前路径
all_files = [f for f in os.listdir(path )]#输出根path下的所有文件名到一个列表中
#对各个文件进行处理

files = []

def create_dir_not_exist(path):
    if not os.path.exists(path):
        os.mkdir(path)

def getTypeStr(type):
    tp = type.lstrip().rstrip()
    if tp == "u8":
        return "U8"
    elif tp == "s8":
        return "S8"
    elif tp == "u16":
        return "U16"
    elif tp == "s16":
        return "S16"
    elif tp == "u32":
        return "U32"
    elif tp == "s32":
        return "S32"
    elif tp == "u64":
        return "U64"
    elif tp == "s64":
        return "S64"
    elif tp == "f32":
        return "F32"
    elif tp == "f64":
        return "F64"
    elif tp == "str":
        return "Str"
    elif tp == "u8[]":
        return "U8Vec"
    elif tp == "s8[]":
        return "S8Vec"
    elif tp == "u16[]":
        return "U16Vec"
    elif tp == "s16[]":
        return "S16Vec"
    elif tp == "u32[]":
        return "U32Vec"
    elif tp == "s32[]":
        return "S32Vec"
    elif tp == "u64[]":
        return "U64Vec"
    elif tp == "s64[]":
        return "S64Vec"
    elif tp == "f32[]":
        return "F32Vec"
    elif tp == "f64[]":
        return "F64Vec"
    elif tp == "str[]":
        return "StrVec"

def getTypeName(str):
    tp = str.split(" ")[0]

    if tp == "u8":
        return "uint8_t"
    elif tp == "s8":
        return "int8_t"
    elif tp == "u16":
        return "uint16_t"
    elif tp == "s16":
        return "int16_t"
    elif tp == "u32":
        return "uint32_t"
    elif tp == "s32":
        return "int32_t"
    elif tp == "u64":
        return "uint64_t"
    elif tp == "s64":
        return "int64_t"
    elif tp == "f32":
        return "float"
    elif tp == "f64":
        return "double"
    elif tp == "str":
        return "std::string"
    elif tp == "u8[]":
        return "std::vector<uint8_t>"
    elif tp == "s8[]":
        return "std::vector<int8_t>"
    elif tp == "u16[]":
        return "std::vector<uint16_t>"
    elif tp == "s16[]":
        return "std::vector<int16_t>"
    elif tp == "u32[]":
        return "std::vector<uint32_t>"
    elif tp == "s32[]":
        return "std::vector<int32_t>"
    elif tp == "u64[]":
        return "std::vector<uint64_t>"
    elif tp == "s64[]":
        return "std::vector<int64_t>"
    elif tp == "f32[]":
        return "std::vector<float>"
    elif tp == "f64[]":
        return "std::vector<double>"
    elif tp == "str[]":
        return "std::vector<std::string>"


for i in all_files:
    if i.split(".")[1] == "msnhproto":
        files.append(path+"\\"+i)

create_dir_not_exist("cpp")

with open("cpp/Base.h", 'w', encoding='utf-8') as f:

    ids = []

    for pf in files:
        with open(pf,'r') as ff:
            reads = ff.readlines()
            contents = []
            for r in reads:
                if re.match("//.*",r) :
                    continue
                res = re.findall("//.*",r)
                if len(res) != 0:
                    for rr in res:
                        r = r.replace(rr,"")

                contents.append(r.replace('\n','').replace('\r','').lstrip().rstrip())

            if contents.count("#") != 4:
                raise ValueError("proto file err :"+pf)
            if not contents[0].__contains__("typeID") :
                raise ValueError("proto file err :"+pf)
            if not contents[1].__contains__("typeName"):
                raise ValueError("proto file err :"+pf)
            
            info = contents[0:2]
            ids.append(info[0].split(" ")[1][0:-1])

        
    f.write("#ifndef MSNH_PROTO_BASE_H\n#define MSNH_PROTO_BASE_H\n\n#include <string>\n#include <stdint.h>\n#include <MsnhProto/MsnhProtoManager.h>\n\n")
    f.write("\nnamespace MsnhProtocal\n{\n")
    for i in files:
        className = i.split("-")[1].split(".")[0]
        className = className[0].upper() + className[1:className.__len__()]
        f.write("class "+  className + ";\n")
    f.write("class Base\n{\npublic:\n    Base():_manager(MsnhProtoManager(0,MsnhProtoType::DEFUALT)){}\n")
    f.write("    Base(uint32_t typeID,MsnhProtoType type):_manager(MsnhProtoManager(typeID,type)){}\n")
    f.write("    uint32_t getTypeID() {return _manager.getTypeID();}\n")
    f.write("    std::string getTypeName() {return _manager.getTypeName();}\n")
    f.write("    virtual void fromBytes(const std::vector<uint8_t> &bytes){_manager.fromBytes(bytes);}\n")
    f.write("    std::vector<uint8_t> encode(){return _manager.serialize();}\n")   
    f.write("    bool isData(){return _manager.getFrameType() == MsnhProtoType::IS_DATA;}\n")
    f.write("    bool isCMD(){return _manager.getFrameType() == MsnhProtoType::IS_CMD;}\n")
    f.write("    bool getSpecialType() const{return _specialType;}\n")
    f.write("    std::string getSpecialName() const{return _specialName;}\n\n")
    tmpCnt = 0
    for i in files:
        className = i.split("-")[1].split(".")[0]
        className = className[0].upper() + className[1:className.__len__()]
        f.write("    "+className + "* to" +className + "(){if(getTypeID()==" + ids[tmpCnt]  + "){return ("+className+ "*)(this);}else{return nullptr;}}\n")
        tmpCnt = tmpCnt + 1

    f.write("protected:\n")
    f.write("    bool _specialType = false;\n")
    f.write("    std::string _specialName = \"NULL\";\n")
    f.write("    MsnhProtoManager _manager;\n};\n\n}\n#endif\n")



for pf in files:
    className = pf.split("-")[1].split(".")[0]
    className = className[0].upper() + className[1:className.__len__()]

    with open("cpp/" + className + ".h", 'w', encoding='utf-8') as f:
        f.write("#ifndef MSNH_PROTO_"+ className.upper() +"_H\n#define MSNH_PROTO_"+ className.upper() +"_H\n\n#include <string>\n#include <stdint.h>\n#include <MsnhProto/MsnhProtoManager.h>\n#include \"Base.h\"\n\n")

        with open(pf,'r') as ff:
            reads = ff.readlines()
            contents = []
            for r in reads:
                if re.match("//.*",r) :
                    continue
                res = re.findall("//.*",r)
                if len(res) != 0:
                    for rr in res:
                        r = r.replace(rr,"")

                contents.append(r.replace('\n','').replace('\r','').lstrip().rstrip())


            if contents.count("#") != 4:
                raise ValueError("proto file err :"+pf)
            if not contents[0].__contains__("typeID") :
                raise ValueError("proto file err :"+pf)
            if not contents[1].__contains__("typeName") :
                raise ValueError("proto file err :"+pf)

            f.write("namespace MsnhProtocal\n")
            f.write("{\n")
            f.write("class " + className +": public Base\n")
            f.write("{\n")
            f.write("public:\n")

            cnt = 0
            idx = []
            for con in contents:
                if con == "#":
                    idx.append(cnt)
                cnt = cnt + 1
        
            info = contents[0:2]
            info[0] = ' '.join(info[0].split())
            info[1] = ' '.join(info[1].split())

            f.write("    " + className + "(MsnhProtoType type):Base("+ info[0].split(" ")[1][0:-1] +",type){}\n")
            f.write("    static uint32_t getID(){return "+ info[0].split(" ")[1][0:-1]  +";}\n")
            f.write("    static std::string getName(){return \""+ info[1].split(" ")[1][0:-1] +"\";}\n")
            f.write("    ///Do not use\n")
            f.write("    virtual void fromBytes(const std::vector<uint8_t> &bytes) override{(void)bytes;}")    
            const = []
            data = []
            cmd  = []
            const = contents[3:idx[1]]
            data  = contents[idx[1]+1:idx[2]]
            cmd   = contents[idx[2]+1:idx[3]]

            for inf in const:
                line = inf.split(" ")
                if line[0] == "const":
                    f.write("    //const "+line[2]+"\n")
                    tmpN = line[2]
                    if tmpN[0] != "_":
                        tmpN = tmpN[0].upper() + tmpN[1:tmpN.__len__()]

                    if line[1] == "str":
                        f.write("    "+getTypeName(line[1]) +" get"+ tmpN + "() const {return \"" + line[3][0:-1]  + "\";}\n")
                    elif line[1] == "f32":
                        f.write("    "+getTypeName(line[1]) +" get"+ tmpN + "() const {return " + line[3][0:-1]  + "f;}\n")
                    else:
                        f.write("    "+getTypeName(line[1]) +" get"+ tmpN + "() const {return " + line[3][0:-1]  + ";}\n")

                if line[0] == "enum":
                    f.write( "    //enum "+line[1]+"\n")
                    f.write( "    enum "+line[1]+"\n")
                    f.write( "    {\n")
                    enums = line[2][1:-2].split(",")
                    for e in enums:
                        f.write("       "+pf.split(".")[0].split("-")[1]+"_"+e+",\n")
                    f.write("    };\n")
            f.write("\n")
            for inf in data:
                if inf == '':
                    continue
                line = inf.split(" ")
                tmpN = line[1][0:line[1].__len__()-1]
                if tmpN[0] != "_":
                    tmpN = tmpN[0].upper() + tmpN[1:tmpN.__len__()]
                
                f.write( "    //"+line[1][0:line[1].__len__()-1]+"\n")
                f.write( "    "+ getTypeName(line[0]) + " getData"+ tmpN + "() {return  _manager.get"+ getTypeStr(line[0]) + "(\"" + line[1][0:-1]  + "\");}\n")
                f.write( "    void setData"+ tmpN + "(const "+getTypeName(line[0])+"& value) { _manager.set"+ getTypeStr(line[0]) + "(\"" + line[1][0:-1]  + "\",value);}\n")
            
            f.write("\n")
            for inf in cmd:
                if inf == '':
                    continue
                line = inf.split(" ")
                tmpN = line[1][0:line[1].__len__()-1]
                if tmpN[0] != "_":
                    tmpN = tmpN[0].upper() + tmpN[1:tmpN.__len__()]
                
                f.write( "    //"+line[1][0:line[1].__len__()-1]+"\n")
                f.write( "    "+ getTypeName(line[0]) + " getCmd"+ tmpN + "() {return  _manager.get"+ getTypeStr(line[0]) + "(\"" + line[1][0:-1]  + "\");}\n")
                f.write( "    void setCmd"+ tmpN + "(const "+getTypeName(line[0])+"& value) { _manager.set"+ getTypeStr(line[0]) + "(\"" + line[1][0:-1]  + "\",value);}\n")
        
            f.write("};\n")   
            f.write("}\n")       
             
        f.write("\n\n#endif")
        f.flush()
        f.close()
