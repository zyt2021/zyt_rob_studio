import serial
import serial.tools.list_ports
from pymycobot.mycobot import MyCobot
import sys
sys.path.append("..")
import time
from MsnhProto import *
from MsnhProtoDef import *
from nanomsg import Socket, PUB, SUB, SUB_SUBSCRIBE, PUSH
import time

PyMsnhProtoInit.initLib("") #初始化MsnhProto库
mc = MyCobot("/dev/ttyUSB0")

sub = Socket(SUB)
sub.connect('ipc:///tmp/mycobot_step.STEP.SLDASM_0_tf00')
sub.set_string_option(SUB, SUB_SUBSCRIBE, b'')

pub = Socket(PUB)
pub.bind('ipc:///tmp/mycobot_step.STEP.SLDASM_0_joints')

frame1 = PyMsnhProto(robotJoints.Info.typeID, type=PyMsnhProto.DATA)
frame1.setF32Vec(robotJoints.Data.jointsF32Vec, [0,0,0,0,0,0])
cmds = frame1.serialize()
pub.send(bytes(cmds))

MaxBytes=1024*1024
mc.send_angles([0,0,0,0,0,0], 100)
# time.sleep(1)
# for i in range(60) :
# 	print(i)
# 	mc.send_angles([i,i,i,i,i,i], 100)


frame = PyMsnhProto(robotJoints.Info.typeID, type=PyMsnhProto.DATA)

while True:#
	res = sub.recv()
	frame.fromBytes(list(res))
	a = frame.getF32Vec(robotJoints.Data.jointsF32Vec)
	print(a)
    
	mc.send_angles(frame.getF32Vec(robotJoints.Data.jointsF32Vec),100)

	frame1.setF32Vec(robotJoints.Data.jointsF32Vec, a)
	cmds = frame1.serialize()
	pub.send(bytes(cmds))

sub.close()



# server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# server.settimeout(60)
# host = '127.0.0.1'


# #host = socket.gethostname()
# port = 9999
# server.bind((host, port))        
# server.listen(1)                     
# try:
#     client,addr = server.accept()         
#     print(addr," 连接上了")
#     while True:
#         data = client.recv(MaxBytes)
#         if not data:
#             print('数据为空，我要退出了')
#             break
#         J = data.decode()
#         jList = J.split(",")

#         data = []

#         for i in range(6):
#             data.append(float(jList[i]))
#         mc.send_angles(data, 80)
#         print(jList)
# except BaseException as e:
#     print("出现异常：")
#     print(repr(e))
# finally:
#     server.close()                    
#     print("我已经退出了，后会无期")
