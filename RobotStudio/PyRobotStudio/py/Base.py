import sys
sys.path.append("..")
from MsnhProto import *
from abc import   ABCMeta, abstractmethod
class Base:
   def __init__(self, typeID=None, type=None):
      if typeID != None and type != None:
         self._manager = PyMsnhProto(typeID,None,type)
      else:
         self._manager = PyMsnhProto()
   @abstractmethod
   def getTypeID(self):
      return self._manager.getTypeID()
   @abstractmethod
   def getTypeName(self):
      return self._manager.getTypeName()
   @abstractmethod
   def fromBytes(self,data):
      return self._manager.fromBytes(data)
   def encode(self):
      return self._manager.serialize()
   def isData(self):
      return self._manager.getType() == PyMsnhProto.DATA
   def isCmd(self):
      return self._manager.getType() == PyMsnhProto.CMD
