import os
import sys
current_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(current_dir)
from Base import Base
from Ping import Ping
from RobotJoints import RobotJoints
from PointsCloud import PointsCloud
from TestProto import TestProto

class MsnhBaseTrans:
   def toPing(bytes):
        val = Ping()
        val.fromBytes(bytes)
        return val

   def toRobotJoints(bytes):
        val = RobotJoints()
        val.fromBytes(bytes)
        return val

   def toPointsCloud(bytes):
        val = PointsCloud()
        val.fromBytes(bytes)
        return val

   def toTestProto(bytes):
        val = TestProto()
        val.fromBytes(bytes)
        return val

