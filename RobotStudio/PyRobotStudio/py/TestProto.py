import os
import sys
current_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(current_dir)
from Base import Base
class TestProto(Base):
    def __init__(self,type=1):
        super().__init__(1,type)
    def getTypeID(self):
        return 999999999 
    def getTypeName(self):
        return 'TestProto' 
    def fromBytes(self,data):
        b = Base()
        b.fromBytes(data)
        if b.getTypeID() == 999999999:
            return self._manager.fromBytes(data)
        else:
            raise Exception("Not a TestProto ")
    #const pi
    def getPi(self):
        return 3.1415926
    #const _pi
    def get_pi(self):
        return 3.141592611231321312
    #enum axis
    class axis:
        TestProto_x = 0
        TestProto_y = 1
        TestProto_z = 2

    #uu8
    def getDataUu8(self):
        return  self._manager.getU8("uu8")
    def setDataUu8(self,value):
        self._manager.setU8("uu8",value)
    #ss8
    def getDataSs8(self):
        return  self._manager.getS8("ss8")
    def setDataSs8(self,value):
        self._manager.setS8("ss8",value)
    #uu16
    def getDataUu16(self):
        return  self._manager.getU16("uu16")
    def setDataUu16(self,value):
        self._manager.setU16("uu16",value)
    #ss16
    def getDataSs16(self):
        return  self._manager.getS16("ss16")
    def setDataSs16(self,value):
        self._manager.setS16("ss16",value)
    #uu32
    def getDataUu32(self):
        return  self._manager.getU32("uu32")
    def setDataUu32(self,value):
        self._manager.setU32("uu32",value)
    #ss32
    def getDataSs32(self):
        return  self._manager.getS32("ss32")
    def setDataSs32(self,value):
        self._manager.setS32("ss32",value)
    #uu64
    def getDataUu64(self):
        return  self._manager.getU64("uu64")
    def setDataUu64(self,value):
        self._manager.setU64("uu64",value)
    #ss64
    def getDataSs64(self):
        return  self._manager.getS64("ss64")
    def setDataSs64(self,value):
        self._manager.setS64("ss64",value)
    #ff32
    def getDataFf32(self):
        return  self._manager.getF32("ff32")
    def setDataFf32(self,value):
        self._manager.setF32("ff32",value)
    #ff64
    def getDataFf64(self):
        return  self._manager.getF64("ff64")
    def setDataFf64(self,value):
        self._manager.setF64("ff64",value)
    #sstr
    def getDataSstr(self):
        return  self._manager.getStr("sstr")
    def setDataSstr(self,value):
        self._manager.setStr("sstr",value)
    #u8Vec
    def getDataU8Vec(self):
        return  self._manager.getU8Vec("u8Vec")
    def setDataU8Vec(self,value):
        self._manager.setU8Vec("u8Vec",value)
    #s8Vec
    def getDataS8Vec(self):
        return  self._manager.getS8Vec("s8Vec")
    def setDataS8Vec(self,value):
        self._manager.setS8Vec("s8Vec",value)
    #u16Vec
    def getDataU16Vec(self):
        return  self._manager.getU16Vec("u16Vec")
    def setDataU16Vec(self,value):
        self._manager.setU16Vec("u16Vec",value)
    #s16Vec
    def getDataS16Vec(self):
        return  self._manager.getS16Vec("s16Vec")
    def setDataS16Vec(self,value):
        self._manager.setS16Vec("s16Vec",value)
    #u32Vec
    def getDataU32Vec(self):
        return  self._manager.getU32Vec("u32Vec")
    def setDataU32Vec(self,value):
        self._manager.setU32Vec("u32Vec",value)
    #s32Vec
    def getDataS32Vec(self):
        return  self._manager.getS32Vec("s32Vec")
    def setDataS32Vec(self,value):
        self._manager.setS32Vec("s32Vec",value)
    #u64Vec
    def getDataU64Vec(self):
        return  self._manager.getU64Vec("u64Vec")
    def setDataU64Vec(self,value):
        self._manager.setU64Vec("u64Vec",value)
    #s64Vec
    def getDataS64Vec(self):
        return  self._manager.getS64Vec("s64Vec")
    def setDataS64Vec(self,value):
        self._manager.setS64Vec("s64Vec",value)
    #f32Vec
    def getDataF32Vec(self):
        return  self._manager.getF32Vec("f32Vec")
    def setDataF32Vec(self,value):
        self._manager.setF32Vec("f32Vec",value)
    #f64Vec
    def getDataF64Vec(self):
        return  self._manager.getF64Vec("f64Vec")
    def setDataF64Vec(self,value):
        self._manager.setF64Vec("f64Vec",value)

