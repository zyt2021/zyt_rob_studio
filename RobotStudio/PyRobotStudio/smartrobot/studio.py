
# -*- coding: utf-8 -*-
import serial
import serial.tools.list_ports
import sys
sys.path.append("..")
import time
from MsnhProto import *
from MsnhProtoDef import *
from nanomsg import Socket, PUB, SUB, SUB_SUBSCRIBE, PUSH
import threading

def calSteps(angles):
	out = []
	out.append(int(angles[0]/240*20480+10240))
	out.append(int((angles[1]+78.5)/198.5*14115))
	out.append(int((-angles[2]+73.9)/253.9*22569))
	out.append(int(-angles[3]/360*17920+8960))
	out.append(int(-angles[4]/270*10080+5040))
	out.append(int(0))
	print(out)
	return out


ser=serial.Serial("COM4",115200,timeout=0.5)

class ThreadDemo(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)

	def run(self):
		while True:
			time.sleep(0.01)
			str1 = ser.read_all()
			if len(str1) > 0:
    				print(str(str1))

th = ThreadDemo()
th.start()
time.sleep(2)

ser.write([0xff,0xfe]) #reset
ser.flush()

# steps = [0,0,0,-90,-135,0]
# steps = calSteps(steps)
# byte = []
# byte.append(0xff)
# byte.append(0xf0)
# for n in steps:
# 	b = n.to_bytes(length=2,byteorder='little',signed=False)
# 	byte.append(b[0])
# 	byte.append(b[1])
# ser.write(byte)
# ser.flush()

PyMsnhProtoInit.initLib() #初始化MsnhProto库

sub = Socket(SUB)
sub.connect('ipc:///tmp/leaf_0_tf00')
sub.set_string_option(SUB, SUB_SUBSCRIBE, b'')

pub = Socket(PUB)
pub.bind('ipc:///tmp/leaf_0_joints')

frame1 = PyMsnhProto(robotJoints.Info.typeID, type=PyMsnhProto.DATA)
frame1.setF32Vec(robotJoints.Data.jointsF32Vec, [0,0,0,0,0,0])
cmds = frame1.serialize()
pub.send(bytes(cmds))

# time.sleep(1)
# for i in range(60) :
# 	print(i)
# 	mc.send_angles([i,i,i,i,i,i], 100)


frame = PyMsnhProto(robotJoints.Info.typeID, type=PyMsnhProto.DATA)

while True:#
	res = sub.recv()
	frame.fromBytes(list(res))
	a = frame.getF32Vec(robotJoints.Data.jointsF32Vec)
	steps = calSteps(a)

	byte = []
	byte.append(0xff)
	byte.append(0xf0)
	for n in steps:
		b = n.to_bytes(length=2,byteorder='little',signed=False)
		byte.append(b[0])
		byte.append(b[1])
	ser.write(byte)
	byte.clear()
	ser.flush()
	time.sleep(0.001)
	#print(sendFrame)
	#mc.send_angles(frame.getF32Vec(robotJoints.Data.jointsF32Vec),100)

	frame1.setF32Vec(robotJoints.Data.jointsF32Vec, a)
	cmds = frame1.serialize()
	pub.send(bytes(cmds))

sub.close()



# server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# server.settimeout(60)
# host = '127.0.0.1'


# #host = socket.gethostname()
# port = 9999
# server.bind((host, port))        
# server.listen(1)                     
# try:
#     client,addr = server.accept()         
#     print(addr," 连接上了")
#     while True:
#         data = client.recv(MaxBytes)
#         if not data:
#             print('数据为空，我要退出了')
#             break
#         J = data.decode()
#         jList = J.split(",")

#         data = []

#         for i in range(6):
#             data.append(float(jList[i]))
#         mc.send_angles(data, 80)
#         print(jList)
# except BaseException as e:
#     print("出现异常：")
#     print(repr(e))
# finally:
#     server.close()                    
#     print("我已经退出了，后会无期")
