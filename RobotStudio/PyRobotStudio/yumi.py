# -*- coding: utf-8 -*- 
import socket
from MsnhProto import *
from MsnhProtoDef import *
from nanomsg import Socket, PUB, SUB_SUBSCRIBE, PUSH
import time

sub = Socket(PUB)
sub.bind('ipc:///tmp/yumi_0_joints')
#sub.set_string_option(SUB, SUB_SUBSCRIBE, b'')

PyMsnhProtoInit.initLib() #初始化MsnhProto库
frame = PyMsnhProto(robotJoints.Info.typeID, type=PyMsnhProto.DATA)

angles1 = [0,0,0,0,10,10,0,0,0,0,0,0,0,0,0,0,0,0]
angles2 = [0,0,0,0,10,10,10,10,0,0,0,0,0,0,0,0,0,0]
angles3 = [0,0,0,0,10,10,10,10,10,10,0,0,0,0,0,0,0,0]
angles4 = [0,0,0,0,10,10,10,10,10,10,10,10,0,0,0,0,0,0]
angles5 = [0,0,0,0,10,10,10,10,10,10,10,10,10,10,0,0,0,0]
angles6 = [0,0,0,0,10,10,10,10,10,10,10,10,10,10,10,10,0,0]

frame.setF32Vec(robotJoints.Data.jointsF32Vec, angles1)
cmds = frame.serialize()
sub.send(bytes(cmds))

time.sleep(1)

frame.setF32Vec(robotJoints.Data.jointsF32Vec, angles2)
cmds = frame.serialize()
sub.send(bytes(cmds))

time.sleep(1)

frame.setF32Vec(robotJoints.Data.jointsF32Vec, angles3)
cmds = frame.serialize()
sub.send(bytes(cmds))

time.sleep(1)

frame.setF32Vec(robotJoints.Data.jointsF32Vec, angles4)
cmds = frame.serialize()
sub.send(bytes(cmds))

time.sleep(1)

frame.setF32Vec(robotJoints.Data.jointsF32Vec, angles5)
cmds = frame.serialize()
sub.send(bytes(cmds))

time.sleep(1)

frame.setF32Vec(robotJoints.Data.jointsF32Vec, angles6)
cmds = frame.serialize()
sub.send(bytes(cmds))

sub.close()
# while True:#
# 	res = sub.recv()
# 	frame.fromBytes(list(res))
# 	print(frame.getF32Vec(robotJoints.Data.jointsF32Vec))
# sub.close()
