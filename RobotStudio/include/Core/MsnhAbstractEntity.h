﻿#ifndef MSNHABSTRACTENTITY_H
#define MSNHABSTRACTENTITY_H
#include <Core/MsnhHeaders.h>
#include <Msnhnet/robot/MsnhFrame.h>

///
/// \brief 抽象实体类, 一切场景对象的根源
///
class AbstractEntity: public QObject
{
    Q_OBJECT

public:
    AbstractEntity(QObject * parent = nullptr);
    //场景物体, 具有拷贝属性
    AbstractEntity(const AbstractEntity& abstractObject3D, QObject * parent = nullptr);
    ~AbstractEntity();

    virtual void translate(QVector3D delta);
    virtual void rotate(QQuaternion rotation);
    virtual void rotate(QVector3D rotation);
    virtual void scale(QVector3D scaling);

    virtual void translateApi(QVector3D delta);
    virtual void rotateApi(QVector3D rotation);
    virtual void scaleApi(QVector3D scaling);

    virtual void dumpObjectInfo(int level = 0) = 0;
    virtual void dumpObjectTree(int level = 0) = 0;

    bool getVisible() const;
    bool getHighlighted() const;
    bool getSelected() const;
    bool getCollisionSelected() const;
    bool getWireFrameMode() const;
    bool getDraggable() const;

    virtual float getAlpha() const;
    virtual bool getDeleteable() const;
    virtual bool getSelectable() const;

    virtual bool isGizmo() const = 0;
    virtual bool isLight() const = 0;
    virtual bool isMesh() const = 0;
    virtual bool isModel() const = 0;

    virtual QVector3D getPosition() const;
    virtual QVector3D getRotation() const;
    virtual QVector3D getScaling() const;

    virtual QVector3D getPositionApi() const;
    virtual QVector3D getRotationApi() const;
    virtual QVector3D getScalingApi() const;

    virtual QVector3D getGlobalPosition() const;
    virtual QVector3D getGlobalRotation() const;
    virtual QVector3D getGlobalPositionApi() const;
    virtual QVector3D getGlobalRotationApi() const;
    virtual Msnhnet::Frame getGlobalFrameApi() const;

    virtual QMatrix4x4 getLocalModelMatrix() const;
    virtual QMatrix4x4 getGlobalModelMatrix() const;

    static AbstractEntity* getHighlightedObject();
    static AbstractEntity* getSelectedObject();
    static AbstractEntity *getCollisionSelectedObject();
signals:
    void visibleChanged(bool visible);
    void highlightedChanged(bool highlighted);
    void selectedChanged(bool selected);
    void wireFrameModeChanged(bool enabled);
    void draggableChanged(bool draggable);
    void deleteableChanged(bool deletable);
    void selectableChanged(bool selectable);

    void positionChanged(QVector3D position);
    void rotationChanged(QVector3D rotation);
    void scalingChanged(QVector3D scaling);

    void alphaChanged(float alpha);


public slots:
    void setVisible(bool visible);
    void setHighlighted(bool highlighted);
    void setSelected(bool selected);
    void setCollisionSelected(bool collisionSelected);
    void setWireFrameMode(bool enabled);
    void setDraggable(bool draggable);
    void setSelectable(bool selectable);
    virtual void setDeleteable(bool deleteable);
    virtual void setAlpha(float alpha);

    virtual void setPosition(QVector3D position);
    virtual void setRotation(QQuaternion rotation);
    virtual void setRotation(QVector3D rotation);
    virtual void setScaling(QVector3D scaling);

    virtual void setPositionApi(QVector3D delta);
    virtual void setRotationApi(QVector3D rotation);
    virtual void setScalingApi(QVector3D scaling);

protected:
    /// \brief      是否可视化, 迭代设置,父类和自己都可见则为true,
    /// \save       非保存项
    /// \interact   用户可更改
    bool _visible       = false;
    /// \brief      高亮参数
    /// \save       非保存项
    /// \interact   用户不可更改
    bool _highlighted   = false;
    /// \brief      选中参数
    /// \save       非保存项
    /// \interact   用户不可更改
    bool _selected      = false;

    bool _collisionSelected = false;
    /// \brief      是否网格显示, 迭代设置,父类和自己任意一个,
    /// \save       非保存项
    /// \interact   用户可更改
    bool _wireFrameMode = false;
    /// \brief      如果model的draggable和自己的都为true,则可拽,
    /// \save       非保存项
    /// \interact   用户不可更改,系统内使用
    bool _draggable     = true;
    /// \brief      只要有一个不可被删除,则整个都不能被删除, 此处由最顶层的Model进行遍历获取
    /// \save       非保存项
    /// \interact   用户不可更改,系统内使用
    bool _deleteable    = true;

    /// \brief      指定不可被选择的对象，如机械臂的轴显示，不需要被选中
    /// \save       非保存项
    /// \interact   用户不可更改,系统内使用
    bool _selectable    = true;
    float _alpha        = 1.0f;

    QVector3D _position;
    QVector3D _rotation;
    QVector3D _scaling;

    static AbstractEntity *_highlightedObject;
    static AbstractEntity *_selectedObject;
    static AbstractEntity *_collisionSelectedObject;
};

#endif // MSNHABSTRACTENTITY_H
