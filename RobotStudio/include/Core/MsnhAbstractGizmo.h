﻿#ifndef ABSTRACTGIZMO_H
#define ABSTRACTGIZMO_H
#include <Core/MsnhMesh.h>

///
/// \brief 所有Gizmo的基类,继承自抽象实体
///
class AbstractGizmo: public AbstractEntity
{
    Q_OBJECT

public:
    enum TransformAxis
    {
        GIZMO_AXIS_NONE = 0,
        GIZMO_AXIS_X ,
        GIZMO_AXIS_Y ,
        GIZMO_AXIS_Z ,
        GIZMO_AXIS_XY ,
        GIZMO_AXIS_YZ ,
        GIZMO_AXIS_ZX ,
        GIZMO_AXIS_XYZ
    };
    //场景唯一对象, 不需要拷贝
    AbstractGizmo(QObject* parent = nullptr);
    ~AbstractGizmo();

    void dumpObjectInfo(int) override {}
    void dumpObjectTree(int) override {}

    bool isGizmo() const override;
    bool isLight() const override;
    bool isMesh() const override;
    bool isModel() const override;

    virtual TransformAxis getAxis() const;
    virtual QVector<Mesh*>& getMarkers();
    virtual void drag(QPoint from, QPoint to, int scnWidth, int scnHeight, QMatrix4x4 proj, QMatrix4x4 view) = 0;
    virtual void bindTo(AbstractEntity* host);
    virtual void unbind();

    AbstractEntity *getHost() const;

public slots:
    virtual void setTransformAxis(TransformAxis axis);
    virtual void setTransformAxis(void* marker);

protected:
    TransformAxis _axis;
    QVector<Mesh*> _markers;
    AbstractEntity* _host;

private slots:
    void hostDestroyed(QObject* host);
};

#endif // ABSTRACTGIZMO_H
