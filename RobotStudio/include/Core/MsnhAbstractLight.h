﻿#ifndef ABSTRACTLIGHT_H
#define ABSTRACTLIGHT_H

#include <Core/MsnhMesh.h>
///
/// \brief 所有灯光的基类
///
class AbstractLight: public QObject
{
    Q_OBJECT

public:
    AbstractLight(QObject *parent = nullptr);
    AbstractLight(QVector3D color, QObject *parent = nullptr);
    //场景物体, 具有拷贝属性
    AbstractLight(const AbstractLight& light, QObject *parent = nullptr);
    ~AbstractLight();

    virtual void dumpObjectInfo(int level = 0) = 0;
    virtual void dumpObjectTree(int level = 0) = 0;

    QVector3D getColor();
    bool getEnabled();
    float getIntensity();
    virtual Mesh* getMarker() const;

public slots:
    virtual void setColor(QVector3D color);
    virtual void setEnabled(bool enabled);
    virtual void setIntensity(float intensity);

signals:
    void colorChanged(QVector3D color);
    void enabledChanged(bool enabled);
    void intensityChanged(float intensity);

protected:
    QVector3D _color;
    bool _enabled       =   false;
    float _intensity    =   0;
};


#endif // ABSTRACTLIGHT_H
