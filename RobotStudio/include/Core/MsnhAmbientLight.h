﻿#ifndef AMBIENTLIGHT_H
#define AMBIENTLIGHT_H
#include <Core/MsnhAbstractLight.h>

///
/// \brief 环境光
///
class AmbientLight: public AbstractLight
{
    Q_OBJECT

public:
    AmbientLight(QObject* parent = nullptr);
    AmbientLight(QVector3D color, QObject* parent = nullptr);
    //场景物体, 具有拷贝属性
    AmbientLight(const AmbientLight& light, QObject* parent = nullptr);
    ~AmbientLight();

    void dumpObjectInfo(int level = 0) override;
    void dumpObjectTree(int level = 0) override;
};

#endif // AMBIENTLIGHT_H
