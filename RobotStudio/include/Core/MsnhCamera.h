﻿#ifndef CAMERA_H
#define CAMERA_H
#include <Core/MsnhHeaders.h>
#include <Core/MsnhModel.h>
#include <Core/MsnhModelLoader.h>

class Camera: public QObject
{
    Q_OBJECT

public:
    Camera(QObject* parent = nullptr);
    Camera(QVector3D getPosition, QVector3D getDirection, QObject* parent = nullptr);
    Camera(const Camera& camera, QObject* parent = nullptr);
    ~Camera();

    void moveForward(float shift);
    void moveRight(float shift);
    void moveUp(float shift);
    void turnLeft(float angle);
    void lookUp(float angle);

    void dumpObjectInfo(int level = 0);
    void dumpObjectTree(int level = 0);

    QVector3D getPosition() const;
    QVector3D getDirection() const;

    QVector3D getPositionApi() const;
    QVector3D getDirectionApi() const;

    float getMovingSpeed() const;
    float getFieldOfView() const;
    float getAspectRatio() const;
    float getNearPlane() const;
    float getfFarPlane() const;

    //  orbit camera
    void turnYaw(float angle);
    void turnPitch(float angle);
    void changeRadius(float r);

    void shiftPosX(float x);
    void shiftPosY(float y);
    void shiftPosZ(float z);

    void setFocusCenter(const QVector3D &focusCenter);
    QVector3D getFocusCenter() const;

    QVector3D getFocusCenterApi() const;
    void setFocusCenterApi(const QVector3D &focusCenter);

    QMatrix4x4 getProjectionMatrix() const;
    QMatrix4x4 getViewMatrix() const;

    bool isOrbitCam() const;
    void setOrbitCam(bool orbitCam);

    float getOrbitPitch() const;
    float getOrbitYaw() const;
    float getOrbitRadius() const;
    float getOrbitMaxRadius() const;
    QVector3D getOrbitFocusCenter() const;

    Mesh *getCenterMarker() const;

    bool getMeshVisible() const;

public slots:
    void reset();
    //void animateReset();

    void initMarker();
    void setMeshVisible(bool meshVisible);
    void setMovingSpeed(float movingSpeed);
    void setFieldOfView(float fieldOfView);
    void setAspectRatio(float aspectRatio);
    void setNearPlane(float nearPlane);
    void setFarPlane(float farPlane);
    void setPosition(QVector3D position);
    void setDirection(QVector3D direction);
    void setPositionApi(QVector3D position);
    void setDirectionApi(QVector3D direction);

    void setOrbitPitch(float orbitPitch);
    void setOrbitYaw(float orbitYaw);
    void setOrbitRadius(float orbitRadius);
    void setOrbitMaxRadius(float orbitMaxRadius);
    void setOrbitFocusCenter(const QVector3D &orbitFocusCenter);

signals:
    void cameraReset();
    void movingSpeedChanged(float movingSpeed);
    void fieldOfViewChanged(float fieldOfView);
    void aspectRatioChanged(float aspectRatio);
    void nearPlaneChanged(float nearPlane);
    void farPlaneChanged(float farPlane);
    void positionChanged(QVector3D position);
    void directionChanged(QVector3D direction);
    void camTypeChanged(bool isOrbitCam);

    void orbitPitchChanged(float orbitPitch);
    void orbitYawChanged(float orbitYaw);
    void orbitRadiusChanged(float orbitRadius);
    void orbitMaxRadiusChanged(float orbitMaxRadius);
    void orbitFocusCenterChanged(const QVector3D &orbitFocusCenter);

private:
    float _movingSpeed = 0;
    float _fieldOfView = 0;
    float _aspectRatio = 0;
    float _nearPlane   = 0;
    float _farPlane    = 0;

    QVector3D _position;
    QVector3D _direction;
    QVector3D _up;

    //  orbit camera
    float _orbitPitch       =  0;
    float _orbitYaw         =  0;
    bool  _orbitCam         =  true;
    float _orbitRadius      =  0;
    float _orbitMaxRadius   =  0;
    QVector3D _orbitFocusCenter;

    float pDelta = 0;
    float yDelta = 0;
    float rDelta = 0;

    bool _meshVisible       = true;

    void setUpVector();

    Mesh *_centerMarker     = nullptr;
};

#endif // CAMERA_H
