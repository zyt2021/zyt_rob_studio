﻿#ifndef CURVE3D_H
#define CURVE3D_H

#include <QObject>
#include <Core/MsnhHeaders.h>
#include <Core/MsnhMesh.h>

class Curve3D : public QObject
{
    Q_OBJECT
public:
    explicit Curve3D(QObject *parent = nullptr);
    Curve3D(const Curve3D &curve, QObject *parent = nullptr);
    ~Curve3D();

    void addData(float x, float y, float z);
    void addData(QVector3D data);
    void addData(const QVector<QVector3D> &data);

    void removeFront();
    void removeBack();
    void removeAt(int index);

    void clear();


    /*  getter */
    bool isCubeLine() const;
    QVector3D getColor() const;
    bool getVisible() const;
    float getLineWidth() const;
    Mesh *getMarker();
    QVector<QVector3D> getLineVertices() const;
    bool getDeleteable() const;

    /* setter */
    void setIsCubeLine(bool isCubeLine);
    void setColor(const QVector3D &color);
    void setVisible(bool visible);
    void setLineWidth(float lineWidth);
    void setLineVertices(const QVector<QVector3D> &lineVertices);
    void setDeleteable(bool deleteable);

signals:
    void colorChanged(QVector3D color);
    void lineWidthChanged(float lineWidth);
    void visiableChanged(bool visible);
    void isCubeLineChanged(bool isCubeLine);
    void lineVerticesChanged(const QVector<QVector3D> &lineVertices);
    void deleteableChanged(bool deleteable);

private slots:
    void update();

private:
    QVector3D _color;
    float  _lineWidth   = 1.f;
    bool   _isCubeLine  = false;
    bool   _deleteable  = true;

    QVector<QVector3D>  _lineVertices;

    Mesh *_marker       = nullptr;
};

#endif // CURVE3D_H
