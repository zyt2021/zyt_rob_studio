﻿#ifndef DIRECTIONLIGHT_H
#define DIRECTIONLIGHT_H

#include <Core/MsnhAbstractLight.h>

///
/// \brief 平行光
///
class DirectionalLight: public AbstractLight
{
    Q_OBJECT

public:
    DirectionalLight(QObject* parent = nullptr);
    DirectionalLight(QVector3D color, QVector3D direction, QObject* parent = nullptr);
    //场景物体, 具有拷贝属性
    DirectionalLight(const DirectionalLight& light, QObject* parent = nullptr);
    ~DirectionalLight();

    void dumpObjectInfo(int level = 0) override;
    void dumpObjectTree(int level = 0) override;

    QVector3D getDirection();

public slots:
    void setDirection(QVector3D direction);

signals:
    void directionChanged(QVector3D direction);

protected:
    QVector3D _direction;
};

#endif // DIRECTIONLIGHT_H
