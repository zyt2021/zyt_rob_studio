﻿#ifndef MSNHEXMATH_H
#define MSNHEXMATH_H
#include <Core/MsnhHeaders.h>

#include <QtMath>
#include <QPoint>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QMatrix3x3>
#include <QMatrix4x4>
#include <QQuaternion>

#define Eps (1e-4f)
#define inf (1000000.0f)
#define rad(n) ((n) * M_PI / 180.0)

bool isEqual(float a, float b);
bool isEqual(QVector3D a, QVector3D b);

bool isnan(QVector2D a);
bool isnan(QVector3D a);
bool isnan(QVector4D a);

///        x(ed)
///       /
///     /   direction(dx,dy,dz)
///   x (st)
///
///
struct Line
{
    QVector3D st;  //起点
    QVector3D dir; //单位方向向量
};

/*
///        | /\    x法向量n(n)
///        /    \
///      / |  x   \ 向量x(v)
///    /   |_____ /____
///    \ x/     /
///     /\    /
/// x /    \/
///
///
*/
struct Plane
{
    QVector3D v; //
    QVector3D n; //
};

Line operator*(const QMatrix4x4 &m, const Line &l);

//求线面的交点 (在线上也在面上),求BA的长，带方向 B单位向量投影和BA的投影之比
// L = st + dir * t;
// `p` is a point on the plane and `n` is the normal vector
QVector3D getIntersectionOfLinePlane(Line l, Plane p);

//获取两线之间的最近点
// L1 = st1 + dir1 * t1
// L2 = st2 + dir2 * t2
QVector3D getClosestPointOfLines(Line l1, Line l2);

//屏幕空间到opengl世界空间
Line screenPosToWorldRay(QVector2D cursorPos, QVector2D windowSize, QMatrix4x4 proj, QMatrix4x4 view);



class GeometryEx
{
public:
    static QVector3D rotatePos(const QVector3D &angles, const QVector3D &pos);
};


#endif
