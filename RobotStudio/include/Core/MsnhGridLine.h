﻿#ifndef GRIDLINE_H
#define GRIDLINE_H

#include <Core/MsnhMesh.h>

///
/// \brief 网格
///
class Gridline: public QObject
{
    Q_OBJECT

public:
    Gridline(QObject* parent = nullptr);
    //场景物体, 具有拷贝属性
    Gridline(const Gridline& gridline, QObject* parent = nullptr);
    ~Gridline();

    enum GridPlane
    {
        GRID_XY_PLANE,
        GRID_YZ_PLANE,
        GRID_ZX_PLANE
    };

    void dumpObjectInfo(int level = 0);
    void dumpObjectTree(int level = 0);

    QPair<float, float> getXRange() const;
    QPair<float, float> getYRange() const;
    QPair<float, float> getZRange() const;
    float getXStride() const;
    float getYStride() const;
    float getZStride() const;

    QPair<float, float> getXRangeApi() const;
    QPair<float, float> getYRangeApi() const;
    QPair<float, float> getZRangeApi() const;
    float getXStrideApi() const;
    float getYStrideApi() const;
    float getZStrideApi() const;

    QVector3D getColor() const;
    Mesh *getMarker();

    float getCanModify() const;
    void setCanModify(float canModify);

    GridPlane getGridPlane() const;
    void setGridPlane(const GridPlane &gridPlane);

public slots:
    void reset();
    void setXArguments(QVector3D xargs);
    void setYArguments(QVector3D yargs);
    void setZArguments(QVector3D zargs);
    void setColor(QVector3D color);

    void setXArgumentsApi(QVector3D xargs);
    void setYArgumentsApi(QVector3D yargs);
    void setZArgumentsApi(QVector3D zargs);

signals:
    void xArgumentsChanged(QVector3D xargs);
    void yArgumentsChanged(QVector3D yargs);
    void zArgumentsChanged(QVector3D zargs);
    void colorChanged(QVector3D color);
    void gridPlaneChanged(GridPlane gridPlane);

private:
    QPair<float, float> _xRange;
    QPair<float, float> _yRange;
    QPair<float, float> _zRange;

    float _xStride      = 0;
    float _yStride      = 0;
    float _zStride      = 0;

    QVector3D _color;
    Mesh *_marker       = nullptr;
    float _canModify    = true;
    void update();

    GridPlane _gridPlane;
};
#endif // GRIDLINE_H
