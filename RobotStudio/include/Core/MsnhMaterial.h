﻿#ifndef MATERIAL_H
#define MATERIAL_H

#include <Core/MsnhTexture.h>
#include <Core/MsnhExMath.h>

///
/// \brief 材质
///
class Material: public QObject
{
    Q_OBJECT

public:
    Material(QObject * parent = nullptr);
    Material(QVector3D color,
             float ambient, float diffuse, float specular,
             QObject * parent = nullptr);
    //场景物体, 具有拷贝属性
    Material(const Material& material, QObject * parent = nullptr);
    ~Material();

    void dumpObjectInfo(int level = 0);
    void dumpObjectTree(int level = 0);

    QVector3D getColor() const;
    float getAmbient();
    float getDiffuse();
    float getSpecular();
    float getShininess();
    QSharedPointer<Texture> getDiffuseTexture();
    QSharedPointer<Texture> getSpecularTexture();
    QSharedPointer<Texture> getBumpTexture();

public slots:
    void setColor(QVector3D color);
    void setAmbient(float ambient);
    void setDiffuse(float diffuse);
    void setSpecular(float specular);
    void setShininess(float shininess);
    void setDiffuseTexture(QSharedPointer<Texture> diffuseTexture);
    void setSpecularTexture(QSharedPointer<Texture> specularTexture);
    void setBumpTexture(QSharedPointer<Texture> bumpTexture);

signals:
    void colorChanged(QVector3D color);
    void ambientChanged(float ambient);
    void diffuseChanged(float diffuse);
    void specularChanged(float specular);
    void shininessChanged(float shininess);
    void diffuseTextureChanged(QSharedPointer<Texture> diffuseTexture);
    void specularTextureChanged(QSharedPointer<Texture> specularTexture);
    void bumpTextureChanged(QSharedPointer<Texture> bumpTexture);

protected:
    QVector3D _color;
    float _ambient      = 0;
    float _diffuse      = 0;
    float _specular     = 0;
    float _shininess    = 0;

    QSharedPointer<Texture> _diffuseTexture;
    QSharedPointer<Texture> _specularTexture;
    QSharedPointer<Texture> _bumpTexture;
};


#endif // MATERIAL_H
