﻿#ifndef MESH_H
#define MESH_H

#include <Core/MsnhAbstractEntity.h>
#include <Core/MsnhVertex.h>
#include <Core/MsnhMaterial.h>

class ModelLoader;
class PointsCloudIO;
class Mesh: public AbstractEntity
{
    Q_OBJECT

public:
    enum MeshType
    {
        TRIANGLE = 0,
        LINE = 1,
        POINT = 2,
        LINE_LOOP = 3
    };

    Mesh(bool isPointCloud=false, QObject * parent = nullptr);
    Mesh(MeshType meshType, bool isPointCloud=false, QObject * parent = nullptr);
    //场景物体, 具有拷贝属性
    Mesh(const Mesh& mesh, QObject * parent = nullptr);
    ~Mesh();

    void dumpObjectInfo(int level = 0) override;
    void dumpObjectTree(int level = 0) override;

    bool isGizmo() const override;
    bool isLight() const override;
    bool isMesh() const override;
    bool isModel() const override;

    void setColor(QVector3D color);
    QVector3D getColor() const;

    QVector3D getCenterOfMass() const;
    float getMass() const;

    MeshType getMeshType() const;
    const QVector<Vertex> &getVertices() const;
    const QVector<uint32_t> &getIndices() const;
    Material* getMaterial() const;
    bool getAlwaysOnTop() const;

    static Mesh* merge(const Mesh* mesh1, const Mesh* mesh2);


public slots:
    void setMeshType(MeshType meshType);
    void setGeometry(const QVector<Vertex>& vertices, const QVector<uint32_t>& indices);
    bool setMaterial(Material *newMaterial);
    void setAlwaysOnTop(bool alwaysOnTop);
    void reverseNormals();
    void reverseTangents();
    void reverseBitangents();

signals:
    void meshTypeChanged(int meshType);
    void geometryChanged(const QVector<Vertex>& vertices, const QVector<uint32_t>& indices);
    void materialChanged(Material* material);
    void alwaysOnTopChanged(bool alwaysOnTop);

private:
    QVector3D _color;

protected:
    void childEvent(QChildEvent *event) override;

protected:
    bool    _alwaysOnTop;
    MeshType _meshType;
    QVector<Vertex> _vertices;
    QVector<uint32_t> _indices;
    Material *_material;

    friend ModelLoader;
    friend PointsCloudIO;
};

QDataStream &operator>>(QDataStream &in, Mesh::MeshType& meshType);
#endif // MESH_H
