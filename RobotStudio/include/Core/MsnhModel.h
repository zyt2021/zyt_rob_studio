﻿#ifndef MSNH_MODEL_H
#define MSNH_MODEL_H

#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/btBulletCollisionCommon.h>

#include <Msnhnet/robot/MsnhRobot.h>
#include <Core/MsnhMesh.h>

class Model: public AbstractEntity
{
    Q_OBJECT
public:
    Model(QObject * parent = nullptr);
    //场景物体, 具有拷贝属性
    Model(const Model& model, QObject * parent = nullptr);
    ~Model();

    bool addChildMesh(Mesh* mesh);
    bool addChildModel(Model* model);

    bool removeChildMesh(QObject* mesh, bool recursive);
    bool removeChildModel(QObject* model, bool recursive);

    void dumpObjectInfo(int level = 0) override;
    void dumpObjectTree(int level = 0) override;

    bool isGizmo() const override;
    bool isLight() const override;
    bool isMesh() const override;
    bool isModel() const override;

    QVector3D getColor() const;

    float getAlpha() const override;

    QVector3D getCenterOfMass() const;

    float getMass() const;

    Mesh* assemble() const;

    Msnhnet::JointType getJointType() const;

    const QVector<Mesh *> &getChildMeshes() const;

    const QVector<Model *> &getChildModels() const;

    double getJointMin() const;

    double getJointMax() const;

    Msnhnet::Frame getEnd2TipFrame() const;

    float getSingleMove() const;

    Msnhnet::Vector3DS getAxis() const;

    bool getDeleteable() const override;

    void releaseRigidBody();

    void initRigidBody(QVector3D shape, float mass,bool isCube);

    btRigidBody *getRigidBody() const;

    void updatePhysical();

public slots:
    void reverseNormals();
    void reverseTangents();
    void reverseBitangents();
    void setDeleteable(bool deleteable) override;
    void setAxis(const Msnhnet::Vector3DS &axis);
    void setFrame(const Msnhnet::Frame& frame);
    void setEnd2TipFrame(const Msnhnet::Frame& frame);
    void setAlpha(float alpha) override;
    void setColor(QVector3D color);
    void singleMove(float value);
    void multiMove(const QVector3D &value);
    void setJointType(Msnhnet::JointType jointType);
    void setJointMinMax(const double& min, const double& max);
    void setPositionApi(QVector3D delta) override;

    Msnhnet::Frame getFrame() const;
signals:
    void childMeshAdded(Mesh* mesh);
    void childMeshRemoved(QObject* object);
    void childModelAdded(Model* model);
    void childModelRemoved(QObject* object);

    void jointTypeChanged(Msnhnet::JointType jointType);
    void jointMinMaxChanged(const double& min, const double& max);
    void end2TipFrameChanged(const Msnhnet::Frame &frame);
    void singleMoved(float value);
    void destroying(Model* obj);

protected:
    void childEvent(QChildEvent *event) override;

private:
    QVector<Mesh  *> _childMeshes;  //走childEvent 添加
    QVector<Model *> _childModels;  //走childEvent 添加

    //非保存参数
    float _modelAlpha               = 1.0f;
    float _singleMove               = 0;
    QVector3D _color;
    Msnhnet::JointType _jointType   =   Msnhnet::JOINT_FIXED;
    Msnhnet::Vector3DS _axis;
    Msnhnet::Frame     _end2TipFrame;
    double             _jointMin    =   -DBL_MAX;
    double             _jointMax    =   DBL_MAX;

    btRigidBody*       _body        =   nullptr;

    btDefaultMotionState *_motionState  = nullptr;
    btCollisionShape* _collisionShape   = nullptr;

    QVector3D          _lastPhysicPos;
    QVector3D          _lastPhysicRot;
};

class GeomModel
{
public:
    GeomModel(){}
    std::shared_ptr<Msnhnet::URDFGeometry> geom;
    Model* model;
};

#endif // MSNH_MODEL_H
