﻿#ifndef MODELEXPORTER_H
#define MODELEXPORTER_H

#include <Core/MsnhModel.h>

struct aiScene;
struct aiNode;
struct aiMesh;
struct aiMaterial;

class ModelExporter
{
public:
    ModelExporter();
    ~ModelExporter();

    void saveToFile(Model* model, QString filePath);
    void saveToFile(Mesh* mesh, QString filePath);

    bool hasErrorLog();
    QString errorLog();

private:
    QString _log;

    Model* _model           = nullptr;
    aiScene* _aiScenePtr    = nullptr;

    QVector<aiMesh *> _tmpAimeshes;
    QVector<aiMaterial *> _tmpAimaterials;
    QVector<QSharedPointer<Texture> > _tmpTextures;

    void getAllTextures(Model* model);
    aiNode* exportModel(Model* model);
    aiMesh* exportMesh(Mesh* mesh);
    aiMaterial* exportMaterial(Material* material);
};


#endif // MODELEXPORTER_H
