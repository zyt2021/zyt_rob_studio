﻿#ifndef MODELLOADER_H
#define MODELLOADER_H
#include <Core/MsnhModel.h>
#include <Core/MsnhTextureLoader.h>
#include <Msnhnet/3rdparty/tinyxml2/tinyxml2.h>

struct aiScene;
struct aiNode;
struct aiMesh;
struct aiMaterial;

class ModelLoader : public QObject
{
    Q_OBJECT
public:
    explicit ModelLoader(QObject *parent = nullptr);


    /// \brief loadModelFromFile 注意指定parent
    Model* loadModelFromFile(QString filePath);

    /// \brief loadMeshFromFile 注意指定parent
    Mesh* loadMeshFromFile(QString filePath);

    static Model* loadConeModel(); //在其他地方setparent
    static Model* loadCubeModel();
    static Model* loadCylinderModel();
    static Model* loadPlaneModel();
    static Model* loadSphereModel();
    static Model* loadTorusModel();
    static Model* loadTransformAxises();

    bool hasErrorLog();
    QString errorLog();

    float getMeshUnitRescale(const std::string& srcPath);

private:
    QDir _dir;
    QString _log;
    TextureLoader textureLoader;

    const aiScene* _aiScenePtr;

    Model* loadModel(const aiNode* aiNodePtr);
    Mesh* loadMesh(const aiMesh* aiMeshPtr);
    Material* loadMaterial(const aiMaterial* aiMaterialPtr);

    bool _needRotate = false;
    float _scale     = 1.f;
};


#endif // MODELLOADER_H
