﻿#ifndef POINTLIGHT_H
#define POINTLIGHT_H
#include <Core/MsnhAbstractLight.h>

class PointLight: public AbstractLight
{
    Q_OBJECT

public:
    PointLight(QObject* parent = nullptr);
    PointLight(QVector3D color, QVector3D position, QObject* parent = nullptr);
    PointLight(const PointLight& light, QObject* parent = nullptr);
    ~PointLight();

    void dumpObjectInfo(int level = 0) override;
    void dumpObjectTree(int level = 0) override;

    bool getVisible() const;
    QVector3D getPosition() const;
    QVector3D getPositionApi() const;
    bool getEnableAttenuation() const;
    QVector3D getAttenuationArguments() const;
    float getAttenuationQuadratic() const;
    float getAttenuationLinear() const;
    float getAttenuationConstant() const;
    Mesh* getMarker() const override;

public slots:
    void setEnabled(bool enabled) override;
    void setColor(QVector3D color) override;
    void setVisible(bool visible);
    void setPosition(QVector3D position);
    void setPositionApi(QVector3D position);
    void setEnableAttenuation(bool getEnabled);
    void setAttenuationArguments(QVector3D value);
    void setAttenuationQuadratic(float value);
    void setAttenuationLinear(float value);
    void setAttenuationConstant(float value);

signals:
    void visibleChanged(bool visible);
    void positionChanged(QVector3D position);
    void enableAttenuationChanged(bool getEnabled);
    void attenuationArgumentsChanged(QVector3D value);
    void attenuationQuadraticChanged(float value);
    void attenuationLinearChanged(float value);
    void attenuationConstantChanged(float value);

protected:
    QVector3D _position;
    bool  _enableAttenuation     = false;

    float _attenuationQuadratic  = 0;
    float _attenuationLinear     = 0;
    float _attenuationConstant   = 0;

    Mesh *_marker                = nullptr;

    void initMarker();
};


#endif // POINTLIGHT_H
