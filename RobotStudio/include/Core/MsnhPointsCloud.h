﻿#ifndef POINTSCLOUD_H
#define POINTSCLOUD_H

#include <QObject>
#include <Core/MsnhHeaders.h>
#include <Core/MsnhMesh.h>
#include <Util/MsnhNNThread.h>
#include <MsnhProto/MsnhProtoManager.h>


class PointsCloud : public QObject
{
    Q_OBJECT
public:
    explicit PointsCloud(QObject *parent = nullptr);
    PointsCloud(const PointsCloud &pointClouds, QObject *parent = nullptr);
    PointsCloud(Mesh *mesh, QObject *parent = nullptr);
    ~PointsCloud();

    void setColorVertex(const QVector<Vertex>& vertices, const QVector<uint32_t> &indices);

    bool isPureColor() const;
    void setPureColor(bool newPureColor);

    const QVector3D &getColor() const;
    void setColor(const QVector3D &color);

    bool getVisible() const;
    void setVisible(bool visible);

    Mesh *getMarker() const;

    // 用于处理构造之后, 外部设置参数
    void init();

    QString getSubAddr() const;

signals:
    void pureColorChanged(bool pureColor);
    void colorChanged(QVector3D color);
    void visiableChanged(bool visible);

private slots:
    void revPointClouds(const std::vector<uint8_t>& data);

private:
    Mesh *_marker       = nullptr;
    bool _pureColor     = true; //使用Material的纯颜色
    QVector3D _color;
    QString _subAddr;
    NNThread* _subNN    = nullptr;
};

#endif // POINTSCLOUD_H
