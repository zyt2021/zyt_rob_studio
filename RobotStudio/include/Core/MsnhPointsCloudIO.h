﻿#ifndef POINTSCLOUDIO_H
#define POINTSCLOUDIO_H

#include <Core/MsnhMesh.h>
#include <Core/MsnhPointsCloud.h>

class PointsCloudIO
{
public:
   Mesh *readPly(const QString& path);
   Mesh *readXYZ(const QString& path);
   Mesh *readXYZRGB(const QString& path);
   PointsCloud *readFromFile(const QString& path);
   bool hasErr();

   const QString &getErr() const;

private:
   QString err = "";
};

#endif // POINTSCLOUDIO_H
