﻿#ifndef MSNH_ROBOT_H
#define MSNH_ROBOT_H
#include <MsnhProto/MsnhProtoManager.h>
#include <Msnhnet/robot/MsnhRobot.h>
#include <Core/MsnhCurve3D.h>
#include <Core/MsnhModel.h>
#include <Core/MsnhModelLoader.h>
#include <Core/MsnhTransform.h>
#include <QMap>
#include <QObject>
#include <QTimer>
#include <QThread>
#include <QTcpSocket>
#include <nanomsg/nn.hpp>
#include <nanomsg/pubsub.h>
#include <Util/MsnhNNThread.h>
#include <QDebug>
#include <Gui/Plot/MsnhPlotPosVecAcc.h>
#include <Physics/Collision/MsnhCollisionEnv.h>
#include <MsnhProto/Protocal/cpp/RobotJoints.h>

struct TfPlanParams
{
    TfPlanParams()
    {
        speed     = 0.5f;
        acc       = 0.2f;
        radius    = 0.05f;
        step      = 0.1f;
        goHome    = false;
        fullCircle= false;
        planType  = Msnhnet::PLAN_JOINT;
        rotInterp = Msnhnet::ROT_INTERP_AXIS_ANGLE;
        loseRate  = 0;
    }
    float                        speed ;
    float                        acc   ;
    float                        radius;
    float                        step  ;
    bool                         goHome;
    bool                         fullCircle;
    Msnhnet::PlanType            planType ;
    Msnhnet::RotationInterpType  rotInterp;
    int                          loseRate;
};

class Robot : public QObject
{
    Q_OBJECT
public:
    explicit Robot(const QString& path,QObject *parent = nullptr);
    Robot(const Robot &robot, QObject *parent = nullptr);
    ~Robot();

    void loadURDF(const QString & path);

    void loadURDF();

    Model *loadModel(const std::shared_ptr<Msnhnet::URDFLinkTree> &link, const std::string &rootPath);

    Model *loadCollision(const std::shared_ptr<Msnhnet::URDFLinkTree> &link, const std::string &rootPath);

    QString getUrdfPath() const;

    Model *getVisualTree() const;

    Model *getEndTree() const;

    Model *getAnimationTree() const;

    Model *getCollisionTree() const;

    Model *getShowModel() const;

    void addChain(const QString& start, const QString& end, const QString& tf);

    const QMap<QString, Model *> &getVisualJoints() const;

    const QMap<QString, Model *> &getEndJoints() const;

    const QMap<QString, Model *> &getAnimationJoints() const;

    bool getShowAxes() const;

    bool getShowEndAxes() const;

    void setShowAxes(bool showAxes);

    void setShowEndAxes(bool showEndAxes);

    void setAxesScale(float scale);

    void setTfToolScale(float scale);

    void setRouteAxesScale(float scale);

    const std::shared_ptr<StringTree> &getStrTree() const;

    const QString &getRobotName() const;

    const QMap<QString, bool> &getTfTaken() const;

    const QMap<QString, Msnhnet::Chain> &getChains() const;

    const QMap<QString, Msnhnet::Frame> &getLastTfFrame() const;

    const QMap<QString, Model *> &getTfModels() const;

    const QMap<QString, Curve3D *> &getCurves() const;

    bool plan(const QString& tfName, float vel, float acc, float dt, float radius, float maxFailedRate, Msnhnet::RotationInterpType rotInterpType, Msnhnet::PlanType planType,bool goHome, bool fullCircle, QString& msg);

    bool runPlan(const QString& tfName, QString& msg);

    bool animatePlan(const QString& tfName, bool run, QString& msg);

    void gotoJoints(const QString &tfName, const Msnhnet::VectorXSDS& joints);

    void gotoZero(const QString& tfName);

    void syncRobot(const QString& tfName);

    void addWayPoint(const QString& tfName);

    void insertWayPoint(const QString& tfName, int index);

    void deleteWayPoint(const QString& tfName, int index);

    void clearWayPoints(const QString& tfName);

    void endEffortToSelected(const QString& tfName, int index);

    void changeEndEffort(const QString& tfName, float x, float y, float z, float rx, float ry, float rz);

    int getPlannedJointsSize(const QString& tfName);

    QString getPublishAddr(const QString& tfName) const;

    const QMap<QString, QVector<Msnhnet::Frame> > &getPathPoints() const;

    QString getJointsCtrlAddr() const;

    // 用于处理构造之后, 外部设置参数,再初始化：如关节的subscriber
    void init();

    int getNotFixJointNum() const;

    void setEnableRemoteJoints(bool enable);

    bool getEnableRemoteJoints() const;

    void showPlot(const QString& name);

    void setUpdateCurvesRealTime(bool updateCurvesRealTime);

    bool getUpdateCurvesRealTime() const;

    QVector<Model *> getEndModels() const;

    QVector<Model *> getAnimationlModels() const;

    void initCollisionEnv();

    void updateCollisionPair();

    std::vector<std::string> getLinkNames() const;

    void selectVisual(const QString& name);

    QMap<QString, bool> getLinkNamePairs() const;

    QMap<QString, bool>& getLinkNamePairsChg();

    //直接返回引用
    QMap<QString, TfPlanParams>& getTfPlaneParams();

    bool getEnableCollisionDetection() const;

    void setEnableCollisionDetection(bool enableCollisionDetection);

    void enableSelect(bool selectable);

    void setExcutionTimer(int delay);

    bool getBodySelectable() const;

    float getAxesScale() const;

    float getTfToolScale() const;

    float getRouteAxesScale() const;

    //关节拖动
    void singleJointMove(const QString& name, float val);

signals:
    void showAxisChanged(bool showAxis);

    void chainAdded(const QString& name, const Msnhnet::Frame& frame);

    void curveAdded(Curve3D* curve);

    void axesGroupAdded(TransformGroup* tfGroup);

    void updateFrame(const QString& name, const Msnhnet::Frame& frame);

    void pathPlanned(const QString& name, int maxStep);

    void updateStep(const QString& name, int step);

    void refreshPoints(const QString& name, const QVector<Msnhnet::Frame>& frames);

    void excutionDone(const QString& name);

private slots:

    void doIkTimer();

    void executePlanTimer();

    Msnhnet::Frame getFrameFromModel(const Model* md);

    void revNNJoints(const std::vector<uint8_t>& data);

    void initNotFixJointNum();

    void updateTfTools();

private:
    //保存参数
    QString _urdfPath       = "";           //URDF路径
    QString _urdfContent    = "null";       //读入的URDF内容,用与保存后读入
    QMap<QString,Model*>    _modelList;     //用于环境所有的模型文件,减少模型Load时间
    QVector<QString>        _chainGroup;    //根据chain group重构规划组,规划信息等
    bool _enableCollisionDetection = false;  //允许碰撞检测


    //TODO: 复制或者保存场景后读入，使用urdfContent和chainGroup重构 2021/07/11


    //非保存参数
    Model *_visualTree      = nullptr;
    Model *_endTree         = nullptr;
    Model *_animationTree   = nullptr;
    Model *_collisionTree   = nullptr;

    Model *_showModel       = nullptr;

    Mesh *_xAxis            = nullptr;
    Mesh *_yAxis            = nullptr;
    Mesh *_zAxis            = nullptr;

    Model *_tf              = nullptr;

    QMap<QString,Model*> _visualJoints; //实际机器人
    QMap<QString,Model*> _endJoints;    //规划机器人
    QMap<QString,Model*> _animationJoints; //动画机器人
    QMap<QString,Model*> _collisionJoints; //碰撞

    QMap<QString,Model*> _collisionLinks; //碰撞
    QVector<Model*> _endModels; //用于修改颜色等
    QVector<Model*> _animationlModels; //用于修改颜色等
    QMap<QString, QVector<GeomModel>> _collisionGeomModel;//用于碰撞检测
    std::vector<std::string> _linkNames; //所有link的名称
    QMap<QString,bool> _linkNamePairs;

    // chain tf tfTaken绑定
    QMap<QString,Msnhnet::Chain> _chains;
    QMap<QString,Model*> _tfModels;
    QMap<QString,TransformGroup*> _tfAxesGroup;
    QMap<QString,int> _tfGroupAxesCnt;
    QMap<QString,bool> _tfTaken;
    QMap<QString,Msnhnet::Frame> _lastTfFrame;
    QMap<QString,Msnhnet::VectorXSDS> _joints; //实时IK用,当前
    QMap<QString,QSharedPointer<nn::socket>> _tfPubSockets;
    QMap<QString,QString> _tfPubAddrs;
    QMap<QString,QVector<QString>> _tfChainNames;

    // plan 轨迹规划
    QMap<QString,QVector<Msnhnet::Frame>> _pathPoints;
    QMap<QString,QVector<Msnhnet::VectorXSDS>> _pathPlanJoints; //规划完关节序列
    QMap<QString,bool> _excutionPlan; //运行模式
    QMap<QString,bool> _animationPlan; //动画模式
    QMap<QString,int> _excutionStep; //当前步数
    QMap<QString,int> _animationStep; //当前动画步数
    QMap<QString,Curve3D*> _curves;
    // tf plan面板参数
    QMap<QString,TfPlanParams> _tfPlaneParams;

    QVector<Mesh*> _axes;
    QVector<Mesh*> _endAxes;
    std::shared_ptr<StringTree> _strTree;
    std::shared_ptr<Msnhnet::URDFModel> _model;
    std::shared_ptr<Msnhnet::URDFLinkTree> _linkTree;

    float _axesScale        = 0.02f;
    float _tfToolScale      = 0.02f;
    float _routeAxesScale   = 0.01f;
    bool  _showAxes         = true;
    bool  _showEndAxes      = true;
    int   _loadType         = 0; //0 visual; 1 end; 2 animation
    QString _robotName      = "";

    QTimer* _timer              = nullptr;
    QTimer* _executeTimer       = nullptr;
    NNThread* _jointsNN         = nullptr;

    int _notFixJointNum         = 0;
    bool _enableRemoteJoints    = false;

    //更新曲线
    bool _updateCurvesRealTime  = false;
    uint8_t timerTick           = 0;
    bool _bodySelectable        = false;

    QWidget *_parentWidget      = nullptr;
    QMap<QString, MsnhPlotPosVecAcc*> _jointsPlot;
    QMap<QString, MsnhPlotPosVecAcc*> _cartsPlot;

    //碰撞环境
    CollisionEnv* _collisionEnv;
    AllowedCollisionMatrix* _acm;
};

#endif // MSNH_ROBOT_H
