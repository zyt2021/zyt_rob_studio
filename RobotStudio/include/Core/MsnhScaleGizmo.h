﻿#ifndef SCALEGIZMO_H
#define SCALEGIZMO_H
#include <Core/MsnhAbstractGizmo.h>
#include <Core/MsnhExMath.h>

class ScaleGizmo: public AbstractGizmo
{
    Q_OBJECT

public:
    ScaleGizmo(QObject* parent = nullptr);
    ~ScaleGizmo();

    void translate(QVector3D delta) override;
    void rotate(QQuaternion rotation) override;
    void rotate(QVector3D rotation) override;
    void scale(QVector3D scaling) override;

    QVector3D getPosition() const override;
    QVector3D getRotation() const override;
    QVector3D getScaling() const override;

    QMatrix4x4 getGlobalSpaceMatrix() const;
    QMatrix4x4 getGlobalModelMatrix() const override;

    void drag(QPoint from, QPoint to, int scnWidth, int scnHeight, QMatrix4x4 proj, QMatrix4x4 view) override;

public slots:
    void setPosition(QVector3D position) override;
    void setRotation(QQuaternion rotation) override;
    void setRotation(QVector3D rotation) override;
    void setScaling(QVector3D scaling) override;
};


#endif // SCALEGIZMO_H
