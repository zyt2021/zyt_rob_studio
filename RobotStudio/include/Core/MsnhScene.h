﻿#ifndef SCENE_H
#define SCENE_H
#include <Core/MsnhCamera.h>
#include <Core/MsnhModel.h>
#include <Core/MsnhGridLine.h>
#include <Core/MsnhAbstractLight.h>
#include <Core/MsnhAmbientLight.h>
#include <Core/MsnhPointLight.h>
#include <Core/MsnhSpotLight.h>
#include <Core/MsnhDirectionLight.h>
#include <Core/MsnhTransformGizmo.h>
#include <Core/MsnhCurve3D.h>
#include <Core/MsnhPointsCloud.h>
#include <Core/MsnhRobot.h>
#include <Core/MsnhTransform.h>

class SceneLoader;
class SceneSaver;

class Scene: public QObject
{
    Q_OBJECT

public:
    Scene(QObject *parent = nullptr);
    Scene(const Scene& scene, QObject *parent = nullptr);
    ~Scene();

    // add new item step 1
    bool setCamera(Camera* getCamera);
    bool addGridline(Gridline* gridline);
    bool addCurve(Curve3D *curve);
    bool addTf(Transform* tf);
    bool addTfGroup(TransformGroup* tfGroup);
    bool addPointsCloud(PointsCloud *pointsCloud);
    bool addLight(AbstractLight* light);
    bool addAmbientLight(AmbientLight* light);
    bool addDirectionalLight(DirectionalLight* light);
    bool addPointLight(PointLight* light);
    bool addSpotLight(SpotLight* light);
    bool addModel(Model* model);
    bool addRobot(Robot* robot);

    // add new item step 2
    bool removeGridline(QObject* gridline);
    bool removeLight(QObject* light);
    bool removeModel(QObject* model, bool recursive);
    bool removeCurve(QObject *curve);
    bool removeTf(QObject *tf);
    bool removeTfGroup(QObject* tfGroup);
    bool removePointsCloud(QObject *pointsCloud);
    bool removeRobot(QObject* robot);

    void dumpObjectInfo(int level = 0);
    void dumpObjectTree(int level = 0);

    TransformGizmo *getGizmo() const;
    Camera *getCamera() const;

    // add new item step 3
    const QVector<Gridline *> &getGridlines() const;
    const QVector<AmbientLight *> &getAmbientLights() const;
    const QVector<DirectionalLight *> &getDirectionalLights() const;
    const QVector<PointLight *> &getPointLights() const;
    const QVector<SpotLight *> &getSpotLights() const;
    const QVector<Model *> &getModels() const;
    const QVector<Curve3D *> &getCurves() const;
    const QVector<Transform *> &getTfs() const;
    const QVector<TransformGroup *> &getTfGroups() const;
    const QVector<PointsCloud *> &getPointsClouds() const;
    const QVector<Robot *> &getRobots() const;
signals:
    // add new item step 4
    void cameraChanged(Camera* getCamera);
    void gridlineAdded(Gridline* gridline);
    void gridlineRemoved(QObject* object);
    void lightAdded(AbstractLight* light);
    void lightRemoved(QObject* object);
    void modelAdded(Model* model);
    void modelRemoved(QObject* object);
    void curveAdded(Curve3D *curve);
    void curveRemoved(QObject *object);
    void tfAdded(Transform* tf);
    void tfRemoved(QObject *curve);
    void tfGroupAdded(TransformGroup* tfGroup);
    void tfGroupsRemoved(QObject* tfGroup);
    void pointsCloudAdded(PointsCloud *pointCloud);
    void pointsCloudRemoved(QObject *object);
    void robotAdded(Robot* robot);
    void robotRemoved(QObject* object);

protected:
    void childEvent(QChildEvent *event) override;

private:
    TransformGizmo * _gizmo = nullptr;
    Camera * _camera        = nullptr;

    // add new item step 0
    QVector<Gridline *>             _gridlines;
    QVector<AmbientLight *>         _ambientLights;
    QVector<DirectionalLight *>     _directionalLights;
    QVector<PointLight *>           _pointLights;
    QVector<SpotLight *>            _spotLights;
    QVector<Model *>                _models;
    QVector<Curve3D *>              _curves;
    QVector<Transform *>            _tfs;
    QVector<TransformGroup *>       _tfGroups;
    QVector<PointsCloud*>           _pointsClouds;
    QVector<Robot*>                 _robots;

    int _gridlineNameCounter           =   0;
    int _ambientLightNameCounter       =   0;
    int _directionalLightNameCounter   =   0;
    int _pointLightNameCounter         =   0;
    int _spotLightNameCounter          =   0;
    int _curveNameCounter              =   0;
    int _tfNameCounter                 =   0;
    int _tfGroupNameCounter            =   0;
    int _pointsCloudNameCounter        =   0;
    int _robotNameCounter              =   0;

    friend SceneLoader;
    friend SceneSaver;
};

#endif // SCENE_H
