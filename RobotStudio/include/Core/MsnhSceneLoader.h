﻿#ifndef SCENELOADER_H
#define SCENELOADER_H

#include <Core/MsnhScene.h>

class SceneLoader : public QObject
{
    Q_OBJECT
public:
    SceneLoader(QObject *parent=nullptr);

    Scene* loadFromFile(QString filePath);

    bool hasErrorLog();
    QString errorLog();

signals:
    void startProgressBarSig();
    void updateProgressBarSig(int val);
    void stopProgressBarSig();

private:
    Camera* loadCamera(QDataStream& in);
    Gridline* loadGridline(QDataStream& in);
    Curve3D* loadCurve(QDataStream& in);
    PointsCloud* loadPointClouds(QDataStream& in);
    AmbientLight* loadAmbientLight(QDataStream& in);
    DirectionalLight* loadDirectionalLight(QDataStream& in);
    PointLight* loadPointLight(QDataStream& in);
    SpotLight* loadSpotLight(QDataStream& in);
    Model* loadModel(QDataStream& in);
    Mesh* loadMesh(QDataStream& in);
    Material* loadMaterial(QDataStream& in);
    QSharedPointer<Texture> loadTexture(QDataStream& in);

    QVector<QSharedPointer<Texture>> _textures;
    QString _log;
};


#endif // SCENELOADER_H
