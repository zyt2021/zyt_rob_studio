﻿#ifndef SCENESAVER_H
#define SCENESAVER_H

#include <Core/MsnhScene.h>

class SceneSaver : public QObject
{
    Q_OBJECT
public:
    SceneSaver(Scene* scene,QObject *parent=nullptr);

    void setScene(Scene* scene);
    bool saveToFile(QString filePath);

    bool hasErrorLog();
    QString errorLog();

signals:
    void startProgressBarSig();
    void updateProgressBarSig(int val);
    void stopProgressBarSig();

private:
    void getAllTextures(Model* model);

    void saveCamera(Camera* camera, QDataStream& out);
    void saveGridline(Gridline* gridline, QDataStream& out);
    void saveCurve(Curve3D* curve, QDataStream& out);
    void savePointCloud(PointsCloud* pointCloud, QDataStream& out);
    void saveAmbientLight(AmbientLight* light, QDataStream& out);
    void saveDirectionalLight(DirectionalLight* light, QDataStream& out);
    void savePointLight(PointLight* light, QDataStream& out);
    void saveSpotLight(SpotLight* light, QDataStream& out);
    void saveModel(Model* model, QDataStream& out);
    void saveMesh(Mesh* mesh, QDataStream& out);
    void saveMaterial(Material* material, QDataStream& out);
    void saveTexture(QSharedPointer<Texture> texture, QDataStream& out);

    Scene *_scene   =   nullptr;
    QVector<QSharedPointer<Texture>> _textures;
    QString _log;
};
#endif // SCENESAVER_H
