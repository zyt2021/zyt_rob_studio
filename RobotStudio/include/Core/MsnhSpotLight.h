﻿#ifndef SPOTLIGHT_H
#define SPOTLIGHT_H

#include <Core/MsnhAbstractLight.h>

class SpotLight: public AbstractLight {
    Q_OBJECT

public:
    SpotLight(QObject* parent = nullptr);
    SpotLight(QVector3D color, QVector3D position, QVector3D direction, QObject* parent = nullptr);
    SpotLight(const SpotLight& light, QObject* parent = nullptr);
    ~SpotLight();

    void translate(QVector3D delta);

    void dumpObjectInfo(int level = 0) override;
    void dumpObjectTree(int level = 0) override;

    bool getVisible() const;
    QVector3D getPosition() const;
    QVector3D getDirection() const;
    float getInnerCutOff() const;
    float getOuterCutOff() const;
    bool getEnableAttenuation() const;
    QVector3D getAttenuationArguments() const;
    float getAttenuationQuadratic() const;
    float getAttenuationLinear() const;
    float getAttenuationConstant() const;
    Mesh* getMarker() const override;

    QVector3D getPositionApi() const;
    QVector3D getDirectionApi() const;

public slots:
    void setEnabled(bool getEnabled) override;
    void setColor(QVector3D getColor) override;
    void setVisible(bool visible);
    void setPosition(QVector3D position);
    void setDirection(QVector3D direction);
    void setInnerCutOff(float innerCutOff);
    void setOuterCutOff(float outerCutOff);
    void setEnableAttenuation(bool getEnabled);
    void setAttenuationArguments(QVector3D value);
    void setAttenuationQuadratic(float value);
    void setAttenuationLinear(float value);
    void setAttenuationConstant(float value);

    void setPositionApi(QVector3D position);
    void setDirectionApi(QVector3D direction);
signals:
    void visibleChanged(bool visible);
    void positionChanged(QVector3D position);
    void directionChanged(QVector3D direction);
    void innerCutOffChanged(float innerCutOff);
    void outerCutOffChanged(float outerCutOff);
    void enableAttenuationChanged(bool getEnabled);
    void attenuationArgumentsChanged(QVector3D value);
    void attenuationQuadraticChanged(float value);
    void attenuationLinearChanged(float value);
    void attenuationConstantChanged(float value);

protected:
    QVector3D _position;
    QVector3D _direction;
    float _innerCutOff          =   0;
    float _outerCutOff          =   0;
    bool  _enableAttenuation    =   false;

    float _attenuationQuadratic =   0;
    float _attenuationLinear    =   0;
    float _attenuationConstant  =   0;

    Mesh *_marker                = nullptr;

    void initMarker();

private slots:
    void setDirectionFromRotation(QVector3D rotation);
};


#endif // SPOTLIGHT_H
