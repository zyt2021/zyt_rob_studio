﻿#ifndef TEXTURE_H
#define TEXTURE_H

#include <Core/MsnhHeaders.h>

class Texture: public QObject {
    Q_OBJECT

public:
    enum TextureType {
        DIFFUSE = 0,
        SPECULAR = 1,
        BUMP = 2
    };

    Texture(TextureType textureType = DIFFUSE, QObject *parent = nullptr);
    Texture(const Texture& texture, QObject *parent = nullptr);
    ~Texture();

    void dumpObjectInfo(int level = 0);
    void dumpObjectTree(int level = 0);

    bool getEnabled() const;
    TextureType getTextureType() const;
    const QImage &getImage() const;

public slots:
    void setEnabled(bool enabled);
    void setTextureType(TextureType textureType);
    void setImage(const QImage& image);

signals:
    void enabledChanged(bool enabled);
    void textureTypeChanged(int textureType);
    void imageChanged(const QImage& image);

private:
    bool _enabled   =   false;
    TextureType _textureType;
    QImage _image;
};

QDataStream &operator>>(QDataStream &in, Texture::TextureType& textureType);

#endif // TEXTURE_H
