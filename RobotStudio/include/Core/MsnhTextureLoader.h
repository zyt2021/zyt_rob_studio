﻿#ifndef TEXTURELOADER_H
#define TEXTURELOADER_H
#include <Core/MsnhTexture.h>

class TextureLoader {
public:
    TextureLoader() {}

    QSharedPointer<Texture> loadFromFile(Texture::TextureType textureType, QString filePath);

    bool hasErrorLog();
    QString errorLog();

private:
    QString _log;
    static QHash<QString, QWeakPointer<Texture> > cache;
};

#endif // TEXTURELOADER_H
