﻿#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <QObject>
#include <Core/MsnhModel.h>

class Transform : public QObject
{
    Q_OBJECT
public:
    explicit Transform(QObject *parent = nullptr);
    Transform(const Transform &tf, QObject *parent = nullptr);
    ~Transform();
    Model *getTfModel() const;

signals:

private:
    Model *_tfModel     =   nullptr;
};


class TransformGroup : public QObject
{
    Q_OBJECT
public:
    explicit TransformGroup(QObject *parent = nullptr);
    TransformGroup(const TransformGroup &tfGroup, QObject *parent = nullptr);

    void addTf(const QString& name);

    void addTf(const QString &name, const Msnhnet::Frame& frame, float scale=0.01f);

    void deleteTf(int index);

    const QVector<Transform *> &getTfVecs() const;

    int getTfVecSize() const;

signals:
    void tfAdded(Transform* name);
    void tfDeleted(const QString& name);
private:
    QVector<Transform*> _tfVecs;
};

#endif // TRANSFORM_H
