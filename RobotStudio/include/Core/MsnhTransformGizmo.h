﻿#ifndef TRANSFORMGIZMO_H
#define TRANSFORMGIZMO_H

#include <Core/MsnhTranslateGizmo.h>
#include <Core/MsnhRotateGizmo.h>
#include <Core/MsnhScaleGizmo.h>

class TransformGizmo: public AbstractGizmo {
    Q_OBJECT

public:
    enum TransformMode
    {
        GIZMO_TRANSLATE,
        GIZMO_ROTATE,
        GIZMO_SCALE
    };

    TransformGizmo(QObject* parent = nullptr);
    ~TransformGizmo();

    void translate(QVector3D delta) override;
    void rotate(QQuaternion rotation) override;
    void rotate(QVector3D rotation) override;
    void scale(QVector3D scaling) override;

    QVector3D getPosition() const override;
    QVector3D getRotation() const override;
    QVector3D getScaling() const override;

    QMatrix4x4 getGlobalModelMatrix() const override;

    TransformAxis getAxis() const override;
    TransformMode getGizmoMode() const;

    bool getAlwaysOnTop() const;

    QVector<Mesh*>& getMarkers() override;
    void drag(QPoint from, QPoint to, int scnWidth, int scnHeight, QMatrix4x4 proj, QMatrix4x4 view) override;

    void bindTo(AbstractEntity* host) override;
    void unbind() override;

public slots:
    void setTransformAxis(TransformAxis axis) override;
    void setTransformAxis(void* marker) override;
    void setTransformMode(TransformMode mode);

    void setPosition(QVector3D position) override;
    void setRotation(QQuaternion rotation) override;
    void setRotation(QVector3D rotation) override;
    void setScaling(QVector3D scaling) override;

    void setAlwaysOnTop(bool alwaysOnTop);

private:
    TranslateGizmo *_translateGizmo =   nullptr;
    RotateGizmo *_rotateGizmo       =   nullptr;
    ScaleGizmo *_scaleGizmo         =   nullptr;
    AbstractGizmo *_activatedGizmo  =   nullptr;
    bool _alwaysOnTop               =   false;
};

#endif // TRANSFORMGIZMO_H
