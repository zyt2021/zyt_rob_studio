﻿#ifndef TRANSLATEGIZMO_H
#define TRANSLATEGIZMO_H

#include <Core/MsnhAbstractGizmo.h>
#include <Core/MsnhExMath.h>

class TranslateGizmo: public AbstractGizmo
{
    Q_OBJECT

public:
    TranslateGizmo(QObject* parent = nullptr);
    ~TranslateGizmo();

    void translate(QVector3D delta) override;
    void rotate(QQuaternion getRotation) override;
    void rotate(QVector3D getRotation) override;
    void scale(QVector3D scaling) override;

    QVector3D getPosition() const override;
    QVector3D getRotation() const override;
    QVector3D getScaling() const override;

    QMatrix4x4 getGlobalSpaceMatrix() const;
    QMatrix4x4 getGlobalModelMatrix() const override;//此处获取模型空间的Matrix4x4的mat重写，使用AbstractGizmo._host作为旋转,移动，缩放的基础

    void drag(QPoint from, QPoint to, int scnWidth, int scnHeight, QMatrix4x4 proj, QMatrix4x4 view) override;

public slots:
    void setPosition(QVector3D position) override;
    void setRotation(QQuaternion getRotation) override;
    void setRotation(QVector3D rot) override;
    void setScaling(QVector3D scaling) override;

};
#endif // TRANSLATEGIZMO_H
