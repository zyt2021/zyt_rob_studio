﻿#ifndef XCOLORDIALOG_H
#define XCOLORDIALOG_H

#include <QDialog>
#include "MsnhColorPreview.h"
#include "MsnhColorSquare.h"
#include "MsnhGradientSlider.h"
#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QRadioButton>

class ColorDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ColorDialog(QWidget *parent = nullptr);

    QColor  color() const;

private:
    enum CheckedColor
    {
        H,S,V,R,G,B
    }checkedColor;

    QHBoxLayout*        hLayoutAll;
    ColorSquare*       colorSquare;
    GradientSlider*    verticalSlider;

    QVBoxLayout*        vLayoutPreview;
    ColorPreview*      colorPreview;

    QVBoxLayout*        vLayoutButtons;
    QPushButton*        okButton;
    QPushButton*        cancelButton;

    QHBoxLayout*        hLayoutPreviewButton;
    QVBoxLayout*        vLayoutPreviewSlider;

    QGridLayout*        gLayoutSlider;
    QLabel*             labelAlpha;
    QSpinBox*           spinAlpha;
    QLabel*             labelAlphaSuffix;
    GradientSlider*    sliderAlpha;

    QRadioButton*       radioHue;
    QSpinBox*           spinHue;
    QLabel*             labelHueSuffix;
    GradientSlider*    sliderHue;

    QRadioButton*       radioSat;
    QSpinBox*           spinSat;
    QLabel*             labelSatSuffix;
    GradientSlider*    sliderSat;

    QRadioButton*       radioVal;
    QSpinBox*           spinVal;
    QLabel*             labelValSuffix;
    GradientSlider*    sliderVal;

    QRadioButton*       radioRed;
    QSpinBox*           spinRed;
    GradientSlider*    sliderRed;
    QRadioButton*       radioGreen;
    QSpinBox*           spinGreen;
    GradientSlider*    sliderGreen;
    QRadioButton*       radioBlue;
    QSpinBox*           spinBlue;
    GradientSlider*    sliderBlue;

private:
    void    SetupUI();
    void    SetConnect();

signals:
    void    colorChanged(QColor);
    void    checkedChanged(char);
    void    colorSelected(QColor);

public slots:
    void    UpdateWidgets();
    void    SetHSV();
    void    SetRGB();

    void    SetColor(QColor color);
    void    SetVerticalSlider();

    void    ClickedOkButton();
};

#endif // XCOLORDIALOG_H
