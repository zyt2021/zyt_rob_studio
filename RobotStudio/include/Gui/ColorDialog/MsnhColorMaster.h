﻿#ifndef XCOLORMASTER_H
#define XCOLORMASTER_H

#include <QWidget>
#include <QColor>
#include "MsnhColorDialog.h"

class ColorMaster : public QWidget
{
    Q_OBJECT
public:
    explicit ColorMaster(QWidget *parent = nullptr);

    void SetSquareWidth(int width);

    QColor getColor() const;

    void setColor(const QColor &value);

signals:
    void colorChanged(QColor color);

public slots:
    void ChangeColor(QColor color);

protected:
    void paintEvent(QPaintEvent *);
    //    void mousePressEvent(QMouseEvent *);
    bool eventFilter(QObject *, QEvent *);

private:
    QColor          _color;
    int             _nWidth;
    ColorDialog*    _colorDialog;
    QBrush          _background;

};

#endif // XCOLORMASTER_H
