﻿#ifndef MSNHCARTESIANWIDGET_H
#define MSNHCARTESIANWIDGET_H

#include <QDialog>
#include <QTimer>
#include <QVector3D>
#include <Msnhnet/robot/MsnhFrame.h>

namespace Ui {
class MsnhCartesianWidget;
}

class MsnhCartesianWidget : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhCartesianWidget(const QString& name, QWidget *parent = nullptr);
    ~MsnhCartesianWidget();
    void updateFrame(const Msnhnet::Frame& frame);

signals:
    void changePosAngle(const QString& name,bool isRot, const QVector3D& value);

private slots:
    void doTimer();

    void on_worldXAddBtn_pressed();

    void on_worldYAddBtn_pressed();

    void on_worldZAddBtn_pressed();

    void on_worldRxAddBtn_pressed();

    void on_worldRyAddBtn_pressed();

    void on_worldRzAddBtn_pressed();

    void on_worldXDecBtn_pressed();

    void on_worldYDecBtn_pressed();

    void on_worldZDecBtn_pressed();

    void on_worldRxDecBtn_pressed();

    void on_worldRyDecBtn_pressed();

    void on_worldRzDecBtn_pressed();

    void on_toolXAddBtn_pressed();

    void on_toolYAddBtn_pressed();

    void on_toolZAddBtn_pressed();

    void on_toolRxAddBtn_pressed();

    void on_toolRyAddBtn_pressed();

    void on_toolRzAddBtn_pressed();

    void on_toolXDecBtn_pressed();

    void on_toolYDecBtn_pressed();

    void on_toolZDecBtn_pressed();

    void on_toolRxDecBtn_pressed();

    void on_toolRyDecBtn_pressed();

    void on_toolRzDecBtn_pressed();



    void on_worldXAddBtn_released();

    void on_worldYAddBtn_released();

    void on_worldZAddBtn_released();

    void on_worldRxAddBtn_released();

    void on_worldRyAddBtn_released();

    void on_worldRzAddBtn_released();

    void on_worldXDecBtn_released();

    void on_worldYDecBtn_released();

    void on_worldZDecBtn_released();

    void on_worldRxDecBtn_released();

    void on_worldRyDecBtn_released();

    void on_worldRzDecBtn_released();

    void on_toolXAddBtn_released();

    void on_toolYAddBtn_released();

    void on_toolZAddBtn_released();

    void on_toolRxAddBtn_released();

    void on_toolRyAddBtn_released();

    void on_toolRzAddBtn_released();

    void on_toolXDecBtn_released();

    void on_toolYDecBtn_released();

    void on_toolZDecBtn_released();

    void on_toolRxDecBtn_released();

    void on_toolRyDecBtn_released();

    void on_toolRzDecBtn_released();

    void showFrame();

private:
    bool worldXAdd = false;
    bool worldYAdd = false;
    bool worldZAdd = false;
    bool worldRxAdd = false;
    bool worldRyAdd = false;
    bool worldRzAdd = false;
    bool worldXDec = false;
    bool worldYDec = false;
    bool worldZDec = false;
    bool worldRxDec = false;
    bool worldRyDec = false;
    bool worldRzDec = false;

    bool toolXAdd = false;
    bool toolYAdd = false;
    bool toolZAdd = false;
    bool toolRxAdd = false;
    bool toolRyAdd = false;
    bool toolRzAdd = false;
    bool toolXDec = false;
    bool toolYDec = false;
    bool toolZDec = false;
    bool toolRxDec = false;
    bool toolRyDec = false;
    bool toolRzDec = false;

    Ui::MsnhCartesianWidget *ui;
    QTimer* _timer;
    QString _name;
    Msnhnet::Frame _frame;
};

#endif // MSNHCARTESIANWIDGET_H
