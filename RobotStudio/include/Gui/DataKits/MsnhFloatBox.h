﻿#ifndef FLOATBOX_H
#define FLOATBOX_H

#include <QLineEdit>
#include <QRegExpValidator>
#include <QRegExp>
#include <QMouseEvent>

class MsnhFloatBox : public QLineEdit
{
    Q_OBJECT
public:
    explicit MsnhFloatBox(QWidget *parent = nullptr);

    float getVal() const;

    void setVal(float value);

    void updateVal(float value);

    float getMax() const;

    void setMax(float value);

    float getMin() const;

    void setMin(float value);

    float getPrecision() const;

    void setPrecision(float value);

    void setMinMax(float min, float max);

signals:
    void valueChanged(float val);

protected:
    void mousePressEvent(QMouseEvent *event);

    void mouseMoveEvent(QMouseEvent *event);

    void mouseReleaseEvent(QMouseEvent *event);

private slots:
    void changedVal();

private:
    bool _isDragging = false;
    QPoint _pressed;
    float _precision = 0.01f;
    float _val       = 0;
    float _max       = 100.f;
    float _min       = 0;
};

#endif // FLOATBOX_H
