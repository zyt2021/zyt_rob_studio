﻿#ifndef MSNHFLOATWIDGET_H
#define MSNHFLOATWIDGET_H

#include <Msnhnet/config/MsnhnetCfg.h>
#include <QWidget>
#include <Gui/DataKits/MsnhFloatBox.h>

namespace Ui {
class MsnhFloatWidget;
}

class MsnhFloatWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MsnhFloatWidget(QWidget *parent = nullptr);
    ~MsnhFloatWidget();

public slots:
    void setName(const QString& name);
    void setMax(const float& max,const bool& needRad2Deg);
    void setMin(const float& min,const bool& needRad2Deg);
    void setVal(const float& val,const bool& needRad2Deg);
    void updateVal(float value);
    float getVal();
signals:
    void valueChanged(float val);
    void valueChangedWithName(const QString &name,float val);

private slots:
    void changedWithName(float val);
private:
    Ui::MsnhFloatWidget *ui;
    QString _name;
};

#endif // MSNHFLOATWIDGET_H
