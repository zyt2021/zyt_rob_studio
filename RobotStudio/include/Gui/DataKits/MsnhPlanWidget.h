﻿#ifndef MSNHPLANWIDGET_H
#define MSNHPLANWIDGET_H

#include <QDialog>
#include <QMessageBox>
#include <Msnhnet/robot/MsnhRobot.h>
#include <Core/MsnhRobot.h>
#include <QListWidget>
#include <QDebug>

namespace Ui {
class MsnhPlanWidget;
}

class MsnhPlanWidget : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhPlanWidget(const QString& name, QWidget *parent = nullptr);
    ~MsnhPlanWidget();

    void updateFrame(const Msnhnet::Frame& frame, bool updateNow = false);
    void addFrame(const Msnhnet::Frame& frame);
    void setMaxStep(int maxStep);
    void setStep(int step);
    void setAnimated(bool ok);
    void refreshWayPoints(const QVector<Msnhnet::Frame>& frames);
    void setPublishAddr(const QString& addr);
    void refreshPlanParams(const TfPlanParams& tfPlanParam);
    void executionDone();

signals:
    void plan(const QString& tfName,float vel, float acc, float dt, float radius, float maxFailedRate, Msnhnet::RotationInterpType rotInterpType, Msnhnet::PlanType planType, bool goHome, bool fullCircle);
    void runPlan(const QString& tfName);
    void animatePlan(const QString& tfName, bool run);
    void addWayPoint(const QString& tfName);
    void insertWayPoint(const QString& tfName, int index);
    void deleteWayPoint(const QString& tfName, int index);
    void clearWayPoints(const QString& tfName);
    void endEffortToSelect(const QString& tfName, int index);
    void updateTfPlanParams(const QString& tfName, const TfPlanParams& params);
    void gotoZero(const QString& tfName);
    void syncRobot(const QString& tfName);
    void changeEndEffort(const QString& tfName, float x, float y, float z, float rx, float ry, float rz);
private slots:
    void syncFrame();

    void onChangeEndEffort(float val);

    void on_planBtn_clicked();

    void on_runBtn_clicked();

    void on_animateCkbx_clicked();

    void on_addPointBtn_clicked();

    void on_insertPointBtn_clicked();

    void on_deletePointBtn_clicked();

    void on_clearPointsBtn_clicked();

    void on_wayPointsList_itemDoubleClicked(QListWidgetItem *item);

    void on_gotoZeroBtn_clicked();

    void on_syncBtn_clicked();

    void on_updateBtn_clicked();
private:
    Ui::MsnhPlanWidget *ui;
    QString _name;
    Msnhnet::Frame _frame;
    bool _jogging;
    int _joggingState = 0; // 0 idle,
};

#endif // MSNHPLANWIDGET_H
