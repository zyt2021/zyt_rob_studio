﻿#ifndef MSNHLISTWIGET_H
#define MSNHLISTWIGET_H
#include <QListWidget>
#include <QAction>
#include <QMenu>
#include <QContextMenuEvent>
#include <QMouseEvent>

class MsnhListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit MsnhListWidget(QWidget *parent=nullptr);
    ~MsnhListWidget();
    QMenu *contextMenu;
    QAction *delAction;
protected: void mousePressEvent(QMouseEvent *event);
private:

};

#endif // MSNHLISTWIGET_H
