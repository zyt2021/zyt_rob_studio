﻿#ifndef MSNHROBOTSTUDIO_H
#define MSNHROBOTSTUDIO_H

#include <QMainWindow>
#include <QPlainTextEdit>
#include <QDebug>
#include <OpenGL/MsnhOpenGLWindow.h>
#include <Core/MsnhSceneSaver.h>
#include <Core/MsnhSceneLoader.h>
#include <Core/MsnhModelLoader.h>
#include <Core/MsnhModelExporter.h>
#include <QProgressBar>
#include <QTimer>

#include <Gui/Properties/MsnhGridLineProperty.h>
#include <Gui/Properties/MsnhCameraProperty.h>
#include <Gui/Properties/MsnhAmbientLightProperty.h>
#include <Gui/Properties/MsnhDirectionalLightProperty.h>
#include <Gui/Properties/MsnhMaterialProperty.h>
#include <Gui/Properties/MsnhMeshProperty.h>
#include <Gui/Properties/MsnhModelProperty.h>
#include <Gui/Properties/MsnhPointLightProperty.h>
#include <Gui/Properties/MsnhSpotLightProperty.h>
#include <Gui/Properties/MsnhCurveProperty.h>
#include <Gui/Properties/MsnhTransformProperty.h>
#include <Gui/Properties/MsnhPointsCloudProperty.h>
#include <Gui/Properties/MsnhRobotProperty.h>
#include <Gui/DataKits/MsnhCartesianWidget.h>
#include <Gui/MsnhUpdateRecord.h>
#include <Gui/MsnhOptionsWindow.h>
#include <Gui/Plot/MsnhPlotPosVecAcc.h>
#include <Gui/Vision/MsnhCalibrateIntrinWindow.h>
#include <Gui/Vision/MsnhCalibrateStereoWindow.h>
#include <Physics/MsnhPhysicalEngine.h>
#include "DockingPaneManager.h"
#include "DockingPaneBase.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MsnhMainWindow; }
QT_END_NAMESPACE

class MsnhMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    static QPlainTextEdit *logTxt;
    MsnhMainWindow(QWidget *parent = nullptr);
    ~MsnhMainWindow();
signals:
    void startProgressBarSig();
    void updateProgressBarSig(int val);
    void stopProgressBarSig();

private slots:
    void doTimer();

    void startProgressBar();
    void stopProgressBar();
    void updateProgressBar(int val);

    void newScene(bool firstTime);
    void openScene();
    void openScenePath(QString filePath);
    void saveScene();
    void saveAsScene();
    bool askToSaveScene();
    void importModel();
    void exportModel();

    void addMaterial();
    void addGridLine();
    void addCube();
    void addCylinder();
    void addPlane();
    void addSphere();
    void addCone();
    void addTorus();
    void addPointClouds();
    void addCurve();

    void addPointLight();
    void addAmbientLight();
    void addDirectionalLight();
    void addSpotLight();
    void addRobot();

    void onCopy();
    void onPaste();
    void onDelete();

    void itemSelected(QVariant item);

    void itemsDeselected(QVariant item);

    void configSignal();

    void fpsChanged(int fps);

    void on_actionSelectTransGizmo_triggered();

    void on_actionSelectRotGizmo_triggered();

    void on_actionSelectScaleGizmo_triggered();

    void on_actionGizmoOnTop_triggered();

    void on_actionOrbitCam_triggered();

    void on_actionFPSCam_triggered();

    void on_actionFullScreen_triggered();

    void on_actionQuit_triggered();

    void on_actionNewProj_triggered();

    void on_actionSaveProj_triggered();

    void on_actionSaveAsProj_triggered();

    void on_actionOpenScene_triggered();

    void on_actionImportModel_triggered();

    void on_actionExportModel_triggered();

    void on_actionAddPointLight_triggered();

    void on_actionAddAmbientLight_triggered();

    void on_actionAddDirectionalLight_triggered();

    void on_actionSpotLight_triggered();

    void on_actionAddBox_triggered();

    void on_actionAddCylinder_triggered();

    void on_actionAddPlane_triggered();

    void on_actionSphere_triggered();

    void on_actionAddCone_triggered();

    void on_actionAddTorus_triggered();

    void on_actionAddGridLine_triggered();

    void on_actionCopy_triggered();

    void on_actionPaste_triggered();

    void on_actionDelete_triggered();

    void on_actionAboutQt_triggered();

    void on_actionAbout_triggered();

    void on_actionAboutSys_triggered();

    void on_actionCreateMaterial_triggered();

    Model *loadURDF(const QString & path);

    Model *loadModel(const std::shared_ptr<Msnhnet::URDFLinkTree> &link, const std::string & rootPath);

    void on_actionUpdateLog_triggered();

    void on_actionAddPointClouds_triggered();

    void on_actionOptions_triggered();

    void on_actionImportURDF_triggered();

    void loadProcesses();

    void loadPlugins();

    void on_actionAddCurve_triggered();

    void on_actionCamIntrinCalibrate_triggered();

    void on_actionCamStereoCalibrate_triggered();

    QPlainTextEdit *createLabel(QString string);
private:
    Ui::MsnhMainWindow *ui;
    OpenGLWindow *_openGLWindow =   nullptr;
    Scene * _host               =   nullptr;
    QTimer *timer;

    QString _savePath = "";
    QVariant _copyedItem;

    SceneLoader *_sceneLoader   =   nullptr;
    SceneSaver  *_sceneSaver    =   nullptr;

    QLabel *_fpsLabel           =   nullptr;
    QLabel *_statusLabel        =   nullptr;
    QLabel *_status             =   nullptr;

    QLabel *_leftMouseIcon      =   nullptr;
    QLabel *_leftMouseNotice    =   nullptr;

    QLabel *_middleMouseIcon    =   nullptr;
    QLabel *_middleMouseNotice  =   nullptr;

    QProgressBar *progressBar   =   nullptr;

    DockingPaneManager *m_dockingPaneManager;

    MsnhUpdateRecord *_updateLogWindow          = nullptr;

    MsnhOptionsWindow* _optionsWindow           =  nullptr;

    MsnhCalibrateIntrinWindow* _calibIntrinWindow   = nullptr;

    MsnhCalibrateStereoWindow* _calibStereoWindow   = nullptr;
};
#endif // MSNHROBOTSTUDIO_H
