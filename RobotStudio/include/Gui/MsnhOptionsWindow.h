#ifndef MSNHOPTIONSWINDOW_H
#define MSNHOPTIONSWINDOW_H

#include <QDialog>
#include <Core/MsnhScene.h>

namespace Ui {
class MsnhOptionsWindow;
}

class MsnhOptionsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhOptionsWindow(QWidget *parent = nullptr, Scene* scene = nullptr);
    ~MsnhOptionsWindow();

signals:
    void changeDragColor(const QColor& color);
    void changeAnimationColor(const QColor& color);

private slots:
    void on_listWidget_currentRowChanged(int currentRow);

private:
    Ui::MsnhOptionsWindow *ui;
    Scene* _scene;
};

#endif // MSNHOPTIONSWINDOW_H
