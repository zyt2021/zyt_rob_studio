﻿#ifndef SCENETREEWIDGET_H
#define SCENETREEWIDGET_H

#include <Core/MsnhScene.h>
// add new item step 1
#define CAMERA_PRIORITY             0
#define GRIDLINEGROUP_PRIORITY      -1
#define LIGHTGROUP_PRIORITY         -2
#define CURVE3D_PRIORITY            -3
#define MESH_PRIORITY               -4
#define MODEL_PRIORITY              -5
#define POINTS_CLOUD_PRIORITY       -6
#define TRANSFORM_PRIORITY          -7
#define TRANSFORM_GROUP_PRIORITY    -8

#define AMBIENTLIGHT_PRIORITY		0
#define DIRECTIONALLIGHT_PRIORITY	-1
#define POINTLIGHT_PRIORITY         -2
#define SPOTLIGHT_PRIORITY          -3

class SceneTreeWidget : public QTreeWidget {
    Q_OBJECT

public:
    SceneTreeWidget( QWidget* parent = 0 );
    SceneTreeWidget( Scene* scene, QWidget* parent = 0 );

    void setScene( Scene* scene );


public slots:
    void reload();

protected:
    void keyPressEvent( QKeyEvent *e ) override;


signals:
    void itemSelected( QVariant item );
    void itemDeselected( QVariant item );

private:
    Scene* _host;

private slots:
    // add new item step 0
    void currentItemChanged( QTreeWidgetItem *current, QTreeWidgetItem *previous );

    void gridlineAdded( Gridline* gridline );

    void curveAdded(Curve3D* curve);

    void tfAdded(Transform* tf);

    void tfGroupAdded(TransformGroup* tfGroup);

    void pointsCloudAdded(PointsCloud* pointCloud);

    void lightAdded( AbstractLight* light );

    void modelAdded( Model* model );

    void robotAdded(Robot* robot);

    void hostDestroyed( QObject* host );
};

class BaseItem : public QObject, public QTreeWidgetItem {
    Q_OBJECT

public:
    BaseItem( QObject* host, QTreeWidgetItem* parent ) : QObject( 0 ), QTreeWidgetItem( parent )
    {
        m_priority = 0;
        setData( 0, Qt::UserRole, QVariant::fromValue( host ) ); //此处用于设置被选对象的内容, onDelete需要
        setText( 0, host->objectName() );
        connect( host, SIGNAL( destroyed( QObject* ) ), this, SLOT( hostDestroyed( QObject* ) ) );
    }


    int priority() const
    {
        return(m_priority);
    }


    void setPriority( int priority )
    {
        m_priority = priority;
    }


    virtual void selectHost()
    {
    }


private:
    int m_priority;

private slots:
    void hostDestroyed( QObject* )
    {
        /* Commit suicide */
        delete this;  //add( new BaseItem ), 在add的时候已经把parent设置给base了,删除会一并删除
    }
};

class CameraItem : public BaseItem {
    Q_OBJECT

public:
    CameraItem( Camera* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( CAMERA_PRIORITY );
        setIcon( 0, QIcon( ":/icon/camera.png" ) );
    }


private:
    Camera* _host;
};

class GridlineItem : public BaseItem {
    Q_OBJECT

public:
    GridlineItem( Gridline* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( GRIDLINEGROUP_PRIORITY );
        setIcon( 0, QIcon( ":/icon/gridLine.png" ) );
    }
private:
    Gridline* _host;
};

class GridlineItemGroup : public BaseItem
{
    Q_OBJECT
public: //使用scene进行gridline多合一，固有的属性，代表网格的集合
    GridlineItemGroup( Scene* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( GRIDLINEGROUP_PRIORITY );
        setIcon( 0, QIcon( ":/icon/gridlineGroup.png" ) );
        setText( 0, "GridlineGroup");
        for (int i = 0; i < host->getGridlines().size(); ++i)
        {
            new GridlineItem(host->getGridlines()[i],this);
        }

        connect(_host, &Scene::gridlineAdded, this, &GridlineItemGroup::gridlineAdd);
    }

private slots:
    void gridlineAdd(Gridline *gridline)
    {
        addChild(new GridlineItem(gridline, nullptr));
    }

private:
    Scene* _host;
};

class Curve3DItem : public BaseItem {
    Q_OBJECT

public:
    Curve3DItem( Curve3D* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( CURVE3D_PRIORITY );
        setIcon( 0, QIcon( ":/icon/line.png" ) );
    }

private:
    Curve3D* _host;
};

class PointsCloudItem : public BaseItem
{
    Q_OBJECT
public:
    PointsCloudItem( PointsCloud* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( POINTS_CLOUD_PRIORITY );
        setIcon( 0, QIcon( ":/icon/pointsCloud.png" ) );
    }

private:
    PointsCloud* _host;
};

class AmbientLightItem : public BaseItem {
    Q_OBJECT

public:
    AmbientLightItem( AmbientLight* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( AMBIENTLIGHT_PRIORITY );
        setIcon( 0, QIcon( ":/icon/ambientLight.png" ) );
    }


private:
    AmbientLight* _host;
};

class DirectionalLightItem : public BaseItem {
    Q_OBJECT

public:
    DirectionalLightItem( DirectionalLight* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( DIRECTIONALLIGHT_PRIORITY );
        setIcon( 0, QIcon( ":/icon/directionLight.png" ) );
    }


private:
    DirectionalLight* _host;
};

class PointLightItem : public BaseItem {
    Q_OBJECT

public:
    PointLightItem( PointLight* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( POINTLIGHT_PRIORITY );
        setIcon( 0, QIcon( ":/icon/pointLight.png" ) );
        connect( _host->getMarker(), SIGNAL( selectedChanged( bool ) ), this, SLOT( selectedChanged( bool ) ) );
    }


    void selectHost() override
    {
        _host->getMarker()->setSelected( true );
    }


private:
    PointLight* _host;

private slots:
    void selectedChanged( bool selected )
    {
        if ( !selected )
            return;
        treeWidget()->setCurrentItem( this );
    }
};

class SpotLightItem : public BaseItem {
    Q_OBJECT

public:
    SpotLightItem( SpotLight* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( SPOTLIGHT_PRIORITY );
        setIcon( 0, QIcon( ":/icon/spotLight.png" ) );
        connect( _host->getMarker(), SIGNAL( selectedChanged( bool ) ), this, SLOT( selectedChanged( bool ) ) );
    }


    void selectHost() override
    {
        _host->getMarker()->setSelected( true );
    }


private:
    SpotLight* _host;

private slots:
    void selectedChanged( bool selected )
    {
        if ( !selected )
            return;
        treeWidget()->setCurrentItem( this );
    }
};

class LightItemGroup : public BaseItem
{
    Q_OBJECT
public: //使用scene进行灯光多合一，固有的属性，代表灯光的集合
    LightItemGroup( Scene* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( LIGHTGROUP_PRIORITY );
        setIcon( 0, QIcon( ":/icon/lightGroup.png" ) );
        setText( 0, "LightGroup");
        for (int i = 0; i < host->getAmbientLights().size(); ++i)
        {
            new AmbientLightItem(host->getAmbientLights()[i],this);
        }

        for (int i = 0; i < host->getDirectionalLights().size(); ++i)
        {
            new DirectionalLightItem(host->getDirectionalLights()[i],this);
        }

        for (int i = 0; i < host->getPointLights().size(); ++i)
        {
            new PointLightItem(host->getPointLights()[i],this);
        }

        for (int i = 0; i < host->getSpotLights().size(); ++i)
        {
            new SpotLightItem(host->getSpotLights()[i],this);
        }

        connect(_host, &Scene::lightAdded, this, &LightItemGroup::lightAdded);
    }

private slots:
    void lightAdded(AbstractLight *l)
    {
        if ( SpotLight * light = qobject_cast<SpotLight*>( l ) )
        {
            for ( int i = 0; i < childCount(); i++ )
            {
                if ( static_cast<BaseItem*>(child( i ) )->priority() < SPOTLIGHT_PRIORITY )
                {
                    insertChild(i, new SpotLightItem( light, nullptr ) );
                    return;
                }
            }
            addChild(new SpotLightItem( light, nullptr) );
        }
        else if ( AmbientLight * light = qobject_cast<AmbientLight*>( l ) )
        {
            for ( int i = 0; i < childCount(); i++ )
            {
                if ( static_cast<BaseItem*>(child( i ) )->priority() < AMBIENTLIGHT_PRIORITY )
                {
                    insertChild( i, new AmbientLightItem( light, nullptr ) );
                    return;
                }
            }
            addChild( new AmbientLightItem( light, nullptr ) );
        }
        else if ( DirectionalLight * light = qobject_cast<DirectionalLight*>( l ) )
        {
            for ( int i = 0; i < childCount(); i++ )
            {
                if ( static_cast<BaseItem*>(child( i ) )->priority() < DIRECTIONALLIGHT_PRIORITY )
                {
                    insertChild( i, new DirectionalLightItem( light, nullptr ) );
                    return;
                }
            }
            addChild( new DirectionalLightItem( light, nullptr ) );
        }
        else if ( PointLight * light = qobject_cast<PointLight*>( l ) )
        {
            for ( int i = 0; i < childCount(); i++ )
            {
                if ( static_cast<BaseItem*>(child( i ) )->priority() < POINTLIGHT_PRIORITY )
                {
                    insertChild( i, new PointLightItem( light, nullptr ) );
                    return;
                }
            }
            addChild( new PointLightItem( light, nullptr ) );
        }
    }

private:
    Scene* _host;
};

class MaterialItem : public BaseItem {
    Q_OBJECT

public:
    MaterialItem( Material* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setIcon( 0, QIcon( ":/icon/material.png" ) );
    }


private:
    Material* _host;
};

class MeshItem : public BaseItem {
    Q_OBJECT

public:
    MeshItem( Mesh* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( MESH_PRIORITY );
        setIcon( 0, QIcon( ":/icon/mesh.png" ) );
        if ( _host->getMaterial() )
            new MaterialItem( _host->getMaterial(), this );
        connect( _host, SIGNAL( selectedChanged( bool ) ), this, SLOT( selectedChanged( bool ) ) );
        connect( _host, SIGNAL( materialChanged( Material* ) ), this, SLOT( materialChanged( Material* ) ) );
    }


    void selectHost() override
    {
        _host->setSelected( true );
    }


private:
    Mesh* _host;

private slots:
    void selectedChanged( bool selected )
    {
        if ( selected )
        {
            if ( !isExpanded() )
                setExpanded( true );
            treeWidget()->setCurrentItem( this );
        } else
            treeWidget()->setCurrentItem( 0 );
    }


    void materialChanged( Material* material )
    {
        if ( material )
            new MaterialItem( material, this );
    }
};

class ModelItem : public BaseItem {
    Q_OBJECT

public:
    ModelItem( Model* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( MODEL_PRIORITY );
        setIcon( 0, QIcon( ":/icon/model.png" ) );
        for ( int i = 0; i < _host->getChildMeshes().size(); i++ )
            new MeshItem( _host->getChildMeshes()[i], this );
        for ( int i = 0; i < _host->getChildModels().size(); i++ )
            new ModelItem( _host->getChildModels()[i], this );
        connect( _host, SIGNAL( selectedChanged( bool ) ), this, SLOT( selectedChanged( bool ) ) );
        connect( _host, SIGNAL( childMeshAdded( Mesh* ) ), this, SLOT( childMeshAdded( Mesh* ) ) );
        connect( _host, SIGNAL( childModelAdded( Model* ) ), this, SLOT( childModelAdded( Model* ) ) );
    }


    void selectHost() override
    {
        _host->setSelected( true );
    }


private:
    Model* _host;

private slots:
    void selectedChanged( bool selected )
    {
        if ( selected )
        {
            if ( !isExpanded() )
                setExpanded( true );
            treeWidget()->setCurrentItem( this );
        } else
            treeWidget()->setCurrentItem( 0 );
    }


    void childMeshAdded( Mesh* mesh )
    {
        new MeshItem( mesh, this );
    }


    void childModelAdded( Model* model )
    {
        new ModelItem( model, this );
    }
};

class TransformItem : public BaseItem {
    Q_OBJECT

public:
    TransformItem( Transform* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( TRANSFORM_PRIORITY );
        setIcon( 0, QIcon( ":/icon/axes.png" ) );

        new ModelItem(host->getTfModel(),this);
    }

private:
    Transform* _host;
};

class TransformGroupItem : public BaseItem {
    Q_OBJECT

public:
    TransformGroupItem( TransformGroup* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( TRANSFORM_GROUP_PRIORITY );
        setIcon( 0, QIcon( ":/icon/axesGroup.png" ) );

        auto tfVecs = _host->getTfVecs();

        for(auto val : tfVecs)
        {
            new TransformItem(val,this);
        }

        connect(host, &TransformGroup::tfAdded, this, &TransformGroupItem::tfAdded);

    }

    void tfAdded(Transform *tf)
    {
        new TransformItem(tf,this);
    }

private:
    TransformGroup* _host;
};

class RobotItem:public BaseItem
{
    Q_OBJECT
public:
    RobotItem( Robot* host, QTreeWidgetItem* parent ) : BaseItem( host, parent )
    {
        _host = host;
        setPriority( MODEL_PRIORITY );
        setIcon( 0, QIcon( ":/icon/robot.png" ) );

        new ModelItem(host->getShowModel(),this);

        auto curvesMap = host->getCurves();

        if(!curvesMap.empty())
        {
            auto iter = curvesMap.begin();

            while (iter!=curvesMap.end())
            {
                new Curve3DItem(iter.value(),this);
            }
        }

        connect(_host, &Robot::curveAdded, this, &RobotItem::curveAdded);
        connect(_host, &Robot::axesGroupAdded, this, &RobotItem::tfGroupAdded);
    }

    void curveAdded(Curve3D *curve)
    {
        new Curve3DItem(curve,this);
    }

    void tfGroupAdded(TransformGroup *tfGroup)
    {
        new TransformGroupItem(tfGroup,this);
    }
private:
    Robot* _host;

};

#endif /* SCENETREEWIDGET_H */
