﻿#ifndef MSNHUPDATERECORD_H
#define MSNHUPDATERECORD_H

#include <QDialog>

namespace Ui
{
class MsnhUpdateRecord;
}

class MsnhUpdateRecord : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhUpdateRecord(QWidget *parent = nullptr);
    ~MsnhUpdateRecord();

private:
    Ui::MsnhUpdateRecord *ui;
};

#endif // MSNHUPDATERECORD_H
