﻿#ifndef MSNHOPENGLOPTION_H
#define MSNHOPENGLOPTION_H

#include <QDialog>
namespace Ui {
class MsnhOpenglOption;
}

class MsnhOpenglOption : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhOpenglOption(QWidget *parent = nullptr);
    ~MsnhOpenglOption();

private slots:
    void on_AACkbx_clicked();

    void on_gamaCkbx_clicked();

    void changeSelectedColor(const QColor& color);

    void changeHighlightColor(const QColor& color);

    void changePointSize(float size);

    void on_setDefaultBtn_clicked();

    void update();

private:
    Ui::MsnhOpenglOption *ui;
};

#endif // MSNHOPENGLOPTION_H
