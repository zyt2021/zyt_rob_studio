#ifndef MSNHROBOTOPTION_H
#define MSNHROBOTOPTION_H

#include <QDialog>
#include <Core/MsnhScene.h>

namespace Ui {
class MsnhRobotOption;
}

class MsnhRobotOption : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhRobotOption(QWidget *parent = nullptr, Scene* scene = nullptr);
    ~MsnhRobotOption();
    void update();
private slots:
    void on_setDefaultBtn_clicked();

    void changeDragColor(const QColor& color);

    void changeAnimationColor(const QColor& color);
private:
    Ui::MsnhRobotOption *ui;
    Scene* _scene;
};

#endif // MSNHROBOTOPTION_H
