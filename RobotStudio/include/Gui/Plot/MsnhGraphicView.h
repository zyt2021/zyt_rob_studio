﻿#ifndef MSNHGRAPHICVIEW_H
#define MSNHGRAPHICVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QWheelEvent>
#include <QDebug>
#include <QVector>
class MsnhGraphicView:public QGraphicsView
{
public:
    explicit MsnhGraphicView(QWidget *parent=nullptr);
    void setImage(QImage _image);
    void zoom(bool flag);
    void manualZoom(int size);
    void manualZoom();
    bool fitView = false;
    QImage getImage() const;

private:
    QGraphicsScene * _scene;
    int _scaleFactor = 100;
    QPixmap _pix;
    QImage _image;
protected:
    void wheelEvent(QWheelEvent * event);

};

#endif // MSNHGRAPHICVIEW_H
