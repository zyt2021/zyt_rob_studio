﻿#ifndef MSNHIMAGEFORM_H
#define MSNHIMAGEFORM_H

#include <QWidget>
#include <opencv2/opencv.hpp>

namespace Ui {
class MsnhImageForm;
}

class MsnhImageForm : public QWidget
{
    Q_OBJECT

public:
    explicit MsnhImageForm(QWidget *parent = nullptr);
    ~MsnhImageForm();

    void setImage(const cv::Mat &mat);

    QImage getImage() const;

    cv::Mat getMat() const;

private slots:
    void on_saveBtn_clicked();

    void on_scaleUpBtn_clicked();

    void on_scaleDownBtn_clicked();

    void on_fixBtn_clicked();
private:
    int _w = 0;
    int _h = 0;
    Ui::MsnhImageForm *ui;
    cv::Mat _mat;
};

#endif // MSNHIMAGEFORM_H
