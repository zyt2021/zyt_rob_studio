#ifndef MSNHPLOTPOSVECACC_H
#define MSNHPLOTPOSVECACC_H

#include <QDialog>
#include <Util/MsnhEngineUtil.h>
#include <Msnhnet/robot/MsnhFrame.h>

namespace Ui {
class MsnhPlotPosVecAcc;
}

class MsnhPlotPosVecAcc : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhPlotPosVecAcc(PlotType plotType = PLOT_SINGLE,QWidget *parent = nullptr);
    ~MsnhPlotPosVecAcc();

    void configPlots();

    void setDatas(const QVector<QVector<double>> &pos, const QVector<QVector<double>>& vel, const QVector<QVector<double>>& acc);

    void addData(const QVector<double> &pos, const QVector<double> &vel, const QVector<double> &acc);

    void addPos(double pos);
    void addFrame(const Msnhnet::Frame& frame);

private:
    Ui::MsnhPlotPosVecAcc *ui;
    PlotType _plotType;
    int      _maxDataSize;
    int      _currentDataSize;
    double   _lastPos;
    double   _lastVel;

    double   _lastX;
    double   _lastXVel;
    double   _lastY;
    double   _lastYVel;
    double   _lastZ;
    double   _lastZVel;
    double   _lastRX;
    double   _lastRXVel;
    double   _lastRY;
    double   _lastRYVel;
    double   _lastRZ;
    double   _lastRZVel;

    Msnhnet::RotationMatDS   _lastRot;
    Msnhnet::RotationMatDS   _lastRotV;
};

#endif // MSNHPLOTPOSVECACC_H
