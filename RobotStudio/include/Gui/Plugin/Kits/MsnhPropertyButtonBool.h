﻿#ifndef MSNHPROPERTYBUTTONBOOL_H
#define MSNHPROPERTYBUTTONBOOL_H

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QDebug>

#include "MsnhPropertyWidget.h"

class MsnhPropertyButtonBool : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    MsnhPropertyButtonBool(MsnhProcessPropertyBool* property, QWidget *parent) :
        MsnhPropertyWidget(property, parent),
        _button(new QPushButton(this)),
        _value(false),
        _property(property)
    {
        setLayout(new QHBoxLayout);
        layout()->setMargin(0);

        _button->setText(property->title());
        layout()->addWidget(_button);

        connect(_button, &QPushButton::clicked, this, &MsnhPropertyButtonBool::onClick );
    }

    bool value() const      { return _value; }
    void saveValue()        { _property->setValue(value()); }
    void resetValue()       { _property->resetValue(); }

    void click()
    {
        _value = true;
        saveValue();
        emitChanged();
    }

protected:
    void emitChanged() { emit changed(); }

private slots:
    void onClick() { click(); }

private:
    QPushButton* _button;
    bool         _value;             // true if pressed during the last cycle. is set to false once value is accessed

    MsnhProcessPropertyBool* _property;
};


#endif // MSNHPROPERTYBUTTONBOOL_H
