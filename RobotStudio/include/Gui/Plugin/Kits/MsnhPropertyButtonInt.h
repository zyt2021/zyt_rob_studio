﻿#ifndef MSNHPROPERTYBUTTONUNSIGNEDINT_H
#define MSNHPROPERTYBUTTONUNSIGNEDINT_H

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QDebug>

#include "MsnhPropertyWidget.h"

class MsnhPropertyButton : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    MsnhPropertyButton(MsnhProcessProperty* property, QWidget *parent) :
        MsnhPropertyWidget(property, parent),
        _button(new QPushButton(this))
    {
        setLayout(new QHBoxLayout);
        layout()->setMargin(0);

        _button->setText(QString::fromLocal8Bit(property->title()));
        layout()->addWidget(_button);

        connect(_button, &QPushButton::clicked, this, &MsnhPropertyButton::onClick );
    }

    virtual void click() {}

protected:
    void emitChanged() { emit changed(); }

private slots:
    void onClick() { click(); }

private:
    QPushButton* _button;
};

template<class T>
class MsnhPropertyButtonImpl : public MsnhPropertyButton
{
public:
    typedef T Property;
    typedef decltype(std::declval<T>().value()) Value;

    MsnhPropertyButtonImpl(Property* property, QWidget *parent) :
        MsnhPropertyButton(property,parent),
        _property(property),
        _value(property->value())
    {}

    Value value() const { return _value; }
    void saveValue()        { _property->setValue(value()); }
    void resetValue()       { _property->resetValue(); }

    void click()
    {
        _value++;
        saveValue();
        emitChanged();
    }

private:
    int _value;

    Property* _property;
    QPushButton* _button;
};

typedef MsnhPropertyButtonImpl<MsnhProcessPropertyInt> MsnhPropertyButtonInt;
typedef MsnhPropertyButtonImpl<MsnhProcessPropertyUnsignedInt> MsnhPropertyButtonUnsignedInt;

#endif // MSNHPROPERTYBUTTONUNSIGNEDINT_H
