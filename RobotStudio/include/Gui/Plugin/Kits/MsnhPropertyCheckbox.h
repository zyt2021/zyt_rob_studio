﻿#ifndef MSNHPROPERTYCHECKBOX_H
#define MSNHPROPERTYCHECKBOX_H

#include <QWidget>
#include <QSlider>
#include <QLabel>
#include <QHBoxLayout>
#include <QCheckBox>

#include <QDebug>

#include "MsnhPropertyWidget.h"

class MsnhPropertyCheckbox : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    explicit MsnhPropertyCheckbox(MsnhProcessPropertyBool* property, QString title, QWidget *parent) : MsnhPropertyWidget(property, parent)
    {
        _property = property;

        _layout = new QGridLayout;
        _layout->setMargin(0);
        setLayout(_layout);

        _checkbox = new QCheckBox(title, this);

        // set up layout
        _layout->addWidget(_checkbox);

        init();

        // connect signals and slots
        connect(_checkbox, &QCheckBox::stateChanged, this, &MsnhPropertyCheckbox::updateValue );

    }

    void init()
    {
        bool value = ((MsnhProcessPropertyBool*) _property)->value();
        _checkbox->setChecked(value);
    }

    void setMinimum(int)    {  }
    void setMaximum(int)    {  }
    bool value()            { return _checkbox->isChecked(); }

    void saveValue()        { _property->setValue(value()); }
    void resetValue()       { _property->resetValue(); init(); }

signals:

public slots:
    void updateValue()
    {
        if(_property)
            saveValue();

        emit changed();
    }

private:
    MsnhProcessPropertyBool* _property;
    QGridLayout* _layout;
    QCheckBox* _checkbox;
};

#endif // MSNHPROPERTYCHECKBOX_H
