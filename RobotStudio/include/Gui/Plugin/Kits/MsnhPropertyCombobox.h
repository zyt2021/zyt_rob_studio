﻿#ifndef MSNHPROPERTYCOMBOBOX_H
#define MSNHPROPERTYCOMBOBOX_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QButtonGroup>
#include <QComboBox>

#include <QDebug>

#include "MsnhPropertyWidget.h"

class MsnhPropertyCombobox : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    MsnhPropertyCombobox(MsnhProcessPropertyInt* property, QWidget *parent) : MsnhPropertyWidget(property, parent)
    {
        _property = property;

        setLayout(new QVBoxLayout(this));

        _combobox = new QComboBox(this);
        layout()->addWidget(_combobox);
        layout()->setMargin(0);

        // split the properties
        QString rawName = QString::fromLocal8Bit(property->title());

        // check if structure is Title:Option1|Option2
        if(!(rawName.contains(":") && rawName.contains("|")))
        {
            qWarning() << "MsnhPropertyCombobox: Invalid title structure " << rawName;
            return;
        }

        QString name = rawName.split(":").at(0);
        QString rawOptions = rawName.split(":").at(1);
        QStringList options = rawOptions.split("|");

        for (int i = 0; i < options.length(); ++i)
        {
            QString opt = QString(options[i]);
            if(opt!="")
            {
                _combobox->addItem(opt);
            }
        }


        init();

        connect(_combobox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &MsnhPropertyCombobox::valueChanged);

    }

    void init()
    {
        int value = _property->value();

        _combobox->setCurrentIndex(value);
    }

    void setMinimum(int)  {  }
    void setMaximum(int)  {  }
    int value()             { return _combobox->currentIndex(); }

    void saveValue()        { _property->setValue(value()); }
    void resetValue()       { _property->resetValue(); init(); }

signals:

public slots:
    void valueChanged()
    {
        saveValue();

        emit changed();
    }

private:
    MsnhProcessPropertyInt*  _property;
    QComboBox*              _combobox;
};

#endif // MSNHPROPERTYCOMBOBOX_H
