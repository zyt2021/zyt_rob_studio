﻿#ifndef MSNHPROPERTYFILE_H
#define MSNHPROPERTYFILE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>

#include "MsnhPropertyWidget.h"
#include "MsnhProcessProperty.h"

class MsnhPropertyFileOpen : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    MsnhPropertyFileOpen(MsnhProcessPropertyString* property, QString defaultDirectory, QWidget* parent) : MsnhPropertyWidget(property, parent)
    {
        setLayout(new QHBoxLayout);
        layout()->setMargin(0);

        _property = property;

        _defaultDirectory = defaultDirectory;

        _lineEdit = new QLineEdit(this);
        _lineEdit->setReadOnly(true);

        _button = new QPushButton("...");
        _button->setMaximumWidth(30);

        layout()->addWidget(_lineEdit);
        layout()->addWidget(_button);

        init();

        connect(_button, &QPushButton::pressed, this, &MsnhPropertyFileOpen::onPressButton );

    }

    void init()
    {
        std::string value = _property->value();

        _lineEdit->setText(QString::fromStdString(value));
    }

    QString value()         { return _lineEdit->text(); }
    void saveValue()        { _property->setValue(value().toStdString()); }
    void resetValue()       { _property->resetValue(); init(); }

signals:

public slots:
    void onPressButton()
    {
        QString directory;
        if(_defaultDirectory.length() > 0 && _lineEdit->text().length() == 0)
            directory = _defaultDirectory;

        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), directory, "Images (*.bmp *.gif *.hdr *.jpg *.jpeg *.png *.psd *.tif *.tiff *.tga *.webp *.wbmp *.xpm *.cr2 *.arw);;All Files(*.*)");

        if(fileName.length() > 0)
        {
            _lineEdit->setText(fileName);
            saveValue();

            emit changed();
        }
    }

private:
    MsnhProcessPropertyString*   _property;
    QLineEdit*                  _lineEdit;
    QPushButton*                _button;
    QString                     _defaultDirectory;

};

#endif // MSNHPROPERTYFILE_H
