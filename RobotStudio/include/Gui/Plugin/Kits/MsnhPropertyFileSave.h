﻿#ifndef MSNHPROPERTYFILECREATE_H
#define MSNHPROPERTYFILECREATE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>

#include "MsnhPropertyWidget.h"
#include "MsnhProcessProperty.h"

class MsnhPropertyFileSave : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    MsnhPropertyFileSave(MsnhProcessPropertyString* property, QWidget* parent) : MsnhPropertyWidget(property, parent)
    {
        setLayout(new QHBoxLayout);
        layout()->setMargin(0);

        _property = property;

        // split the properties
        QString rawName(QString::fromLocal8Bit(property->title()));
        _filetypeString = rawName.split(":").at(1);

        _lineEdit = new QLineEdit(this);
        _lineEdit->setReadOnly(true);

        _button = new QPushButton("...");
        _button->setMaximumWidth(30);

        layout()->addWidget(_lineEdit);
        layout()->addWidget(_button);

        connect(_button, &QPushButton::pressed,
                this, &MsnhPropertyFileSave::onPressButton );

        init();
    }

    void init()
    {
        std::string value = _property->value();

        _lineEdit->setText(QString::fromStdString(value));
    }

    QString value()         { return _lineEdit->text(); }
    void saveValue()        { _property->setValue(value().toStdString()); }
    void resetValue()       { _property->resetValue(); init(); }

signals:

public slots:
    void onPressButton()
    {
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", _filetypeString);

        if(fileName.length() > 0)
        {
            _lineEdit->setText(fileName);
            saveValue();

            emit changed();
        }
    }

private:
    MsnhProcessPropertyString*   _property;
    QLineEdit*                  _lineEdit;
    QPushButton*                _button;
    QString                     _filetypeString;
};

#endif // MSNHPROPERTYFILECREATE_H
