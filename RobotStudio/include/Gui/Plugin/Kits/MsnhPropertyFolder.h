﻿#ifndef MSNHPROPERTYFOLDER_H
#define MSNHPROPERTYFOLDER_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>

#include "MsnhPropertyWidget.h"
#include "MsnhProcessProperty.h"

class MsnhPropertyFolder : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    MsnhPropertyFolder(MsnhProcessPropertyString* property, QWidget* parent) : MsnhPropertyWidget(property, parent)
    {
        setLayout(new QHBoxLayout);
        layout()->setMargin(0);

        _property = property;

        _lineEdit = new QLineEdit(this);
        _lineEdit->setReadOnly(true);

        _button = new QPushButton("...");
        _button->setMaximumWidth(30);

        layout()->addWidget(_lineEdit);
        layout()->addWidget(_button);

        init();

        connect(_button, &QPushButton::pressed, this, &MsnhPropertyFolder::onPressButton );

    }

    void init()
    {
        std::string value = _property->value();

        _lineEdit->setText(QString::fromStdString(value));
    }

    QString value()         { return _lineEdit->text(); }
    void saveValue()        { _property->setValue(value().toStdString()); }
    void resetValue()       { _property->resetValue(); init(); }

signals:

public slots:
    void onPressButton()
    {
        QString folder = QFileDialog::getExistingDirectory(this, tr("Choose Folder"), "");

        if(folder.length() > 0)
        {
            _lineEdit->setText(folder);
            saveValue();

            emit changed();
        }
    }

private:
    MsnhProcessPropertyString* _property;
    QLineEdit* _lineEdit;
    QPushButton* _button;

};

#endif // MSNHPROPERTYFOLDER_H
