﻿#ifndef MSNHPROPERTYGROUP_H
#define MSNHPROPERTYGROUP_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QButtonGroup>
#include <QComboBox>
#include <QTimer>

#include <QDebug>

#include "MsnhPropertyWidget.h"

class MsnhPropertyGroup : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    MsnhPropertyGroup(MsnhProcessPropertyInt* property, QWidget *parent) : MsnhPropertyWidget(property, parent)
    {
        _property = property;

        setLayout(new QVBoxLayout(this));

        _combobox = new QComboBox(this);
        layout()->addWidget(_combobox);
        layout()->setMargin(0);

        // split the properties
        QString rawName(QString::fromLocal8Bit(property->title()));

        // check if structure is Title:Option1|Option2
        if(!(rawName.contains(":") && rawName.contains("|")))
        {
            qWarning() << "IPPropertyGroup: Invalid title structure " << rawName;
            return;
        }

        QString rawDescription(property->description());
        if(!(rawDescription.contains("|")))
        {
            qWarning() << "IPPropertyGroup: Invalid group structure " << rawDescription;
            return;
        }

        QString name = rawName.split(":").at(0);
        QString rawOptions = rawName.split(":").at(1);
        QStringList options = rawOptions.split("|");

        _groupPrefixes = rawDescription.split("|");

        _combobox->addItems(options);

        init();

        connect(_combobox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &MsnhPropertyGroup::valueChanged);

        // force GUI update
        QTimer::singleShot(50, this, SLOT(indexChanged()));
    }

    void init()
    {
        int value = _property->value();

        _combobox->setCurrentIndex(value);
    }

    void setMinimum(int)  {  }
    void setMaximum(int)  {  }
    int value()             { return _combobox->currentIndex(); }

    void saveValue()        { _property->setValue(value()); }
    void resetValue()       { _property->resetValue(); init(); }

    QString currentGroup()  { return _groupPrefixes[value()]; }

signals:
    void groupChanged(QString);

public slots:
    void valueChanged()
    {
        saveValue();

        emit changed();
        emit groupChanged(currentGroup());
    }

    void indexChanged()
    {
        emit groupChanged(currentGroup());
    }

private:
    MsnhProcessPropertyInt*  _property;
    QComboBox*              _combobox;
    QStringList             _groupPrefixes;
};

#endif // MSNHPROPERTYGROUP_H
