﻿#ifndef MSNHPROPERTYRADIOINT_H
#define MSNHPROPERTYRADIOINT_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QButtonGroup>
#include <QRadioButton>

#include <QDebug>

#include "MsnhPropertyWidget.h"

class MsnhPropertyRadioInt : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    MsnhPropertyRadioInt(MsnhProcessPropertyInt* property, QWidget *parent) : MsnhPropertyWidget(property, parent)
    {
        _ignoreEvents = true;

        _property = property;

        setLayout(new QVBoxLayout);
        layout()->setMargin(0);

        _buttonGroup = new QButtonGroup(this);

        // split the properties
        QString rawName(QString::fromLocal8Bit(property->title()));

        // check if structure is Title:Option1|Option2
        if(!(rawName.contains(":") && rawName.contains("|")))
        {
            qWarning() << "IPPropertyRadioInt: Invalid title structure " << rawName;
            return;
        }

        QString name = rawName.split(":").at(0);
        QString rawOptions = rawName.split(":").at(1);
        QStringList options = rawOptions.split("|");

        for(int i=0; i<options.size(); i++)
        {
            QRadioButton* btn = new QRadioButton(options[i], this);
            _buttonGroup->addButton(btn, i);
            layout()->addWidget(btn);

            _inputs.push_back(btn);

            connect(btn, &QRadioButton::clicked, this, &MsnhPropertyRadioInt::valueChanged);
        }

        init();


        _ignoreEvents = false;
    }

    void init()
    {
        int value = _property->value();

        int i=0;
        for(QRadioButton* btn : _inputs)
        {
            btn->setChecked(i==value);
            i++;
        }
    }

    void setMinimum(int)  {  }
    void setMaximum(int)  {  }
    int value()             { return _buttonGroup->checkedId(); }

    void saveValue()        { _property->setValue(value()); }
    void resetValue()       { _property->resetValue(); init(); }

signals:

public slots:
    void valueChanged()
    {
        if(_ignoreEvents)
            return;

        saveValue();

        emit changed();
    }

private:
    MsnhProcessPropertyInt*      _property;
    QButtonGroup*               _buttonGroup;
    QList<QRadioButton*>        _inputs;
    bool                        _ignoreEvents;
};

#endif // MSNHPROPERTYRADIOINT_H
