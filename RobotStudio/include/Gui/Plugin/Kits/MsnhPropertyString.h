﻿
#ifndef MSNHPROPERTYSTRING_H
#define MSNHPROPERTYSTRING_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>

#include "MsnhPropertyWidget.h"
#include "MsnhProcessProperty.h"

class MsnhPropertyString : public MsnhPropertyWidget
{
    Q_OBJECT
public:
    MsnhPropertyString(MsnhProcessPropertyString* property, QWidget* parent) : MsnhPropertyWidget(property, parent)
    {
        setLayout(new QHBoxLayout);
        layout()->setMargin(0);

        _property = property;

        _lineEdit = new QLineEdit(this);
        layout()->addWidget(_lineEdit);

        init();

        connect(_lineEdit, &QLineEdit::editingFinished, this, &MsnhPropertyString::onEditingFinished);

    }

    void init()
    {

        std::string value = _property->value();

        _lineEdit->setText(QString::fromStdString(value));
    }

    QString value()         { return _lineEdit->text(); }
    void saveValue()        { _property->setValue(value().toStdString()); }
    void resetValue()       { _property->resetValue(); init(); }

signals:

public slots:
    void onEditingFinished()
    {
        saveValue();

        emit changed();
    }

private:
    MsnhProcessPropertyString* _property;
    QLineEdit* _lineEdit;

};

#endif // MSNHPROPERTYSTRING_H
