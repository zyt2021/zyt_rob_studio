#ifndef MSNHPROPERTYWIDGET_H
#define MSNHPROPERTYWIDGET_H

#include <QWidget>
#include <MsnhProcessProperty.h>

class MsnhPropertyWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MsnhPropertyWidget(MsnhProcessProperty* processProperty, QWidget* parent=0) : QWidget(parent)
    {
        _processProperty = processProperty;
    }
    ~MsnhPropertyWidget()
    {
        _processProperty = NULL;
    }

    virtual void saveValue() = 0;
    virtual void resetValue() = 0;
    MsnhProcessProperty* processProperty() { return _processProperty; }

signals:
    void changed();
private:
    MsnhProcessProperty* _processProperty;

};

#endif // MSNHPROPERTYWIDGET_H
