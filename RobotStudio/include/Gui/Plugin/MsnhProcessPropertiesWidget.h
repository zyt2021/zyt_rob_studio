﻿#ifndef MSNHPROCESSPROPERTIESWIDGET_H
#define MSNHPROCESSPROPERTIESWIDGET_H

#include <QWidget>
#include <QFormLayout>
#include <QSpinBox>
#include <QLineEdit>
#include <MsnhProcess.h>

#include <Gui/Plugin/Kits/MsnhPropertyButtonBool.h>
#include <Gui/Plugin/Kits/MsnhPropertyButtonInt.h>
#include <Gui/Plugin/Kits/MsnhPropertyCheckbox.h>
#include <Gui/Plugin/Kits/MsnhPropertyCheckboxInt.h>
#include <Gui/Plugin/Kits/MsnhPropertyCombobox.h>
#include <Gui/Plugin/Kits/MsnhPropertyFileOpen.h>
#include <Gui/Plugin/Kits/MsnhPropertyFileSave.h>
#include <Gui/Plugin/Kits/MsnhPropertyFolder.h>
#include <Gui/Plugin/Kits/MsnhPropertyGroup.h>
#include <Gui/Plugin/Kits/MsnhPropertyRadioInt.h>
#include <Gui/Plugin/Kits/MsnhPropertySliderDouble.h>
#include <Gui/Plugin/Kits/MsnhPropertySliderInt.h>
#include <Gui/Plugin/Kits/MsnhPropertySpinnerInt.h>
#include <Gui/Plugin/Kits/MsnhPropertyString.h>
#include <Gui/Plugin/Kits/MsnhPropertyWidget.h>

class MsnhProcessPropertiesWidget:public QWidget
{
    Q_OBJECT
public:
    explicit MsnhProcessPropertiesWidget(QWidget *parent = 0);
    ~MsnhProcessPropertiesWidget();
    void init(MsnhProcess* process);
    void closeSettings();
    void resetSettings();
    void addPropertyWidget(QString label, QString description, MsnhPropertyWidget *widget);
private:
    static bool sortByPosition(MsnhProcessProperty* x, MsnhProcessProperty* y);
    QList<MsnhPropertyWidget*> _propertyWidgets;
    MsnhProcess* _process;
};

#endif // MSNHPROCESSPROPERTIESWIDGET_H
