﻿#ifndef MSNHAMBIENTLIGHTPROPERTY_H
#define MSNHAMBIENTLIGHTPROPERTY_H

#include <QDialog>
#include <Core/MsnhAmbientLight.h>

namespace Ui {
class MsnhAmbientLightProperty;
}

class MsnhAmbientLightProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhAmbientLightProperty(AmbientLight *light, QWidget *parent = nullptr);
    ~MsnhAmbientLightProperty();

private slots:
    void on_enableCkbx_clicked(bool checked);

private:
    Ui::MsnhAmbientLightProperty *ui;
    AmbientLight *_host;
    void configSignals();
    void updateVals();

private slots:
    void hostDestroyed(QObject* host);
};

#endif // MSNHAMBIENTLIGHTPROPERTY_H
