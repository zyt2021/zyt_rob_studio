﻿#ifndef MSNHCAMERAPROPERTY_H
#define MSNHCAMERAPROPERTY_H

#include <QDialog>
#include <Core/MsnhCamera.h>

namespace Ui {
class MsnhCameraProperty;
}

class MsnhCameraProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhCameraProperty(Camera *cam, QWidget *parent = nullptr);
    ~MsnhCameraProperty();

private:
    Ui::MsnhCameraProperty *ui;
    Camera *_host;
    void configSignals();
    void updateVals();

private slots:
    void hostDestroyed(QObject* host);

    void positonChanged(float);
    void directionChanged(float);
    void focusChanged(float);
    void on_resetBtn_clicked();

    void updatePosition(QVector3D val);
    void updateDirection(QVector3D val);
    void updateFocus(QVector3D val);
    void on_setCenterBtn_clicked();
};

#endif // MSNHCAMERAPROPERTY_H
