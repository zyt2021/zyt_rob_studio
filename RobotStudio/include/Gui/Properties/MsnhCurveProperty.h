﻿#ifndef MSNHCURVEPROPERTY_H
#define MSNHCURVEPROPERTY_H

#include <QDialog>
#include <Core/MsnhCurve3D.h>
#include <QFileDialog>

namespace Ui {
class MsnhCurveProperty;
}

class MsnhCurveProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhCurveProperty(Curve3D *curve, QWidget *parent = nullptr);
    ~MsnhCurveProperty();

private:
    Ui::MsnhCurveProperty *ui;
    Curve3D *_host      = nullptr;

    void configSignals();
    void updateVals();

private slots:
    void hostDestroyed(QObject *);
    void on_saveDataBtn_clicked();
    void on_loadDataBtn_clicked();
    void on_syncDataBtn_clicked();
    void on_addDataBtn_clicked();
    void on_deleteDataBtn_clicked();
};

#endif // MSNHCURVEPROPERTY_H
