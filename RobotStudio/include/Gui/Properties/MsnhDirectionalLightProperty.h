﻿#ifndef MSNHDIRECTIONALLIGHTPROPERTY_H
#define MSNHDIRECTIONALLIGHTPROPERTY_H

#include <QDialog>
#include <Core/MsnhDirectionLight.h>

namespace Ui {
class MsnhDirectionalLightProperty;
}

class MsnhDirectionalLightProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhDirectionalLightProperty(DirectionalLight *light, QWidget *parent = nullptr);
    ~MsnhDirectionalLightProperty();


private:
    Ui::MsnhDirectionalLightProperty *ui;
    DirectionalLight *_host;

    void configSignals();
    void updateVals();
    void directionChanged(float);

private slots:
    void hostDestroyed(QObject* host);
    void on_enableCkbx_clicked(bool checked);
};

#endif // MSNHDIRECTIONALLIGHTPROPERTY_H
