﻿#ifndef MSNHGRIDLINEPROPERTY_H
#define MSNHGRIDLINEPROPERTY_H

#include <QDialog>
#include <Core/MsnhGridLine.h>

namespace Ui {
class MsnhGridLineProperty;
}

class MsnhGridLineProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhGridLineProperty(Gridline *gridline, QWidget *parent = nullptr);
    ~MsnhGridLineProperty();

private:
    Gridline *_host;
    Ui::MsnhGridLineProperty *ui;
    void configSignals();
    void updateVals();

private slots:
    void xChanged(float);
    void yChanged(float);
    void zChanged(float);

    void hostDestroyed(QObject *host);
    void on_resetBtn_clicked();
};

#endif // MSNHGRIDLINEPROPERTY_H
