﻿#ifndef MSNHMATERIALPROPERTY_H
#define MSNHMATERIALPROPERTY_H

#include <Core/MsnhMaterial.h>
#include <QDialog>

namespace Ui {
class MsnhMaterialProperty;
}

class MsnhMaterialProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhMaterialProperty(Material *material,QWidget *parent = nullptr);
    ~MsnhMaterialProperty();

private slots:
    void hostDestroyed(QObject *host);

    void configSignals();
    void updateVals();
private:
    Ui::MsnhMaterialProperty *ui;
    Material *_host;
};

#endif // MSNHMATERIALPROPERTY_H
