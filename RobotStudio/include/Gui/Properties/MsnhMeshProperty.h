﻿#ifndef MSNHMESHPROPERTY_H
#define MSNHMESHPROPERTY_H

#include <QDialog>
#include <Core/MsnhMesh.h>

namespace Ui {
class MsnhMeshProperty;
}

class MsnhMeshProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhMeshProperty(Mesh *mesh, QWidget *parent = nullptr);
    ~MsnhMeshProperty();

    void hostDestroyed(QObject *host);

private:
    Ui::MsnhMeshProperty *ui;
    Mesh * _host = nullptr;

private slots:
    void configSignals();
    void updateVals();

    void changePos(float);
    void changeRot(float);
    void changeScaling(float);

    void updatePos(QVector3D pos);
    void updateRot(QVector3D rot);
    void updateScaling(QVector3D scale);

};

#endif // MSNHMESHPROPERTY_H
