﻿#ifndef MSNHMODELPROPERTY_H
#define MSNHMODELPROPERTY_H

#include <QDialog>
#include <Core/MsnhModel.h>

namespace Ui {
class MsnhModelProperty;
}

class MsnhModelProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhModelProperty(Model *model, QWidget *parent = nullptr);
    ~MsnhModelProperty();

    void hostDestroyed(QObject *host);

private:
    Ui::MsnhModelProperty *ui;
    Model *_host = nullptr;

private slots:
    void configSignals();
    void updateVals();

    void changePos(float);
    void changeRot(float);
    void changeScaling(float);

    void updatePos(QVector3D);
    void updateRot(QVector3D);
    void updateScaling(QVector3D);
};

#endif // MSNHMODELPROPERTY_H
