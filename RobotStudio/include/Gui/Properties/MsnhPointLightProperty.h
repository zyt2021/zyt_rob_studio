﻿#ifndef MSNHPOINTLIGHTPROPERTY_H
#define MSNHPOINTLIGHTPROPERTY_H

#include <QDialog>
#include <Core/MsnhPointLight.h>

namespace Ui {
class MsnhPointLightProperty;
}

class MsnhPointLightProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhPointLightProperty(PointLight *light, QWidget *parent = nullptr);
    ~MsnhPointLightProperty();

private:
    Ui::MsnhPointLightProperty *ui;
    PointLight *_host = nullptr;

private slots:
    void configSignals();
    void updateVals();

    void changePos(float);
    void updatePos(QVector3D);

    void hostDestroyed();
};

#endif // MSNHPOINTLIGHTPROPERTY_H
