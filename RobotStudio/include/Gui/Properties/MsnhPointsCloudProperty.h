﻿#ifndef MSNHPOINTSCLOUDPROPERTY_H
#define MSNHPOINTSCLOUDPROPERTY_H

#include <Core/MsnhPointsCloud.h>
#include <QDialog>

namespace Ui {
class MsnhPointsCloudProperty;
}

class MsnhPointsCloudProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhPointsCloudProperty(PointsCloud *pointsCloud,QWidget *parent = nullptr);
    ~MsnhPointsCloudProperty();

private:
    Ui::MsnhPointsCloudProperty *ui;
    PointsCloud *_host = nullptr;

private slots:
    void configSignals();
    void updateVals();
    void hostDestroyed();
};

#endif // MSNHPOINTSCLOUDPROPERTY_H
