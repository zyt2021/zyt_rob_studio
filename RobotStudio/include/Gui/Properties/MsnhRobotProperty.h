﻿#ifndef MSNHROBOTPROPERTY_H
#define MSNHROBOTPROPERTY_H

#include <QDialog>
#include <Core/MsnhRobot.h>
#include <QtWidgets/QVBoxLayout>
#include <Gui/DataKits/MsnhFloatWidget.h>
#include <Gui/DataKits/MsnhCartesianWidget.h>
#include <Gui/DataKits/MsnhPlanWidget.h>
#include <QMap>
#include <QMessageBox>
#include <Gui/Plot/MsnhPlotPosVecAcc.h>

namespace Ui {
class MsnhRobotProperty;
}


class MsnhRobotProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhRobotProperty(Robot *robot,QWidget *parent = nullptr);
    ~MsnhRobotProperty();

private slots:
    void on_setStartJointBtn_clicked();

    void on_setStopJointBtn_clicked();

    void on_addPlanBtn_clicked();

    void updateTfTaken();

    void doneExecution(const QString& name);

    void addCartesian(const QString& name, const Msnhnet::Frame& frame);

    void addPlan(const QString& name, const Msnhnet::Frame &frame);

    void updateFrame(const QString& name, const Msnhnet::Frame& frame);

    void updateTF(const QString& name, bool isRot, const QVector3D& val);

    void plan(const QString& tfName,float vel, float acc, float dt, float radius, float maxFailedRate, Msnhnet::RotationInterpType rotInterpType, Msnhnet::PlanType planType, bool goHome, bool fullCircle);

    void runPlan(const QString& tfName);

    void animatePlan(const QString& tfName, bool run);

    void pathPlanned(const QString& tfName, int maxStep);

    void updateStep(const QString& tfName, int step);

    void addWayPoint(const QString& tfName);

    void insertWayPoint(const QString& tfName, int index);

    void deleteWayPoint(const QString& tfName, int index);

    void clearWayPoints(const QString& tfName);

    void gotoZero(const QString& tfName);

    void syncRobot(const QString &tfName);

    void endEffortToSelect(const QString& tfName, int index);

    void changeEndEffort(const QString& tfName, float x, float y, float z, float rx, float ry, float rz);

    void refreshWayPoints(const QString& tfName, const QVector<Msnhnet::Frame>& frames);

    void updateTfPlanParams(const QString& tfName, const TfPlanParams& tfPlanParams);

    void on_remoteJointsCkbx_clicked();

    void on_showJointCurveBtn_clicked();

    void on_showCartCurveBtn_clicked();

    void on_realTimeCurvesCkbx_clicked();

    void on_updateCollisionPairBtn_clicked();

    void on_linkPairsRegExpTxt_textChanged(const QString &arg1);

    void on_linksPairListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_linksPairListWidget_itemClicked(QListWidgetItem *item);

    void on_enableCollisionDetCkbx_clicked();

    void on_enableBodySelectCkbx_clicked();

    void on_setMoveDelay_clicked();

private:
    void loadStrTree(const std::shared_ptr<StringTree>& strTree, QTreeWidgetItem *parent);

    Ui::MsnhRobotProperty *ui;
    Robot *_host;
    QVBoxLayout* _vLayout;
    QMap<QString, MsnhCartesianWidget*> _cartTools;
    QMap<QString, MsnhPlanWidget*> _planTools;
    QMap<QString, MsnhFloatWidget*> _JointsWidget;

    QVBoxLayout* _chainGroupVLayout;
    QVBoxLayout* _planVLayout;
};

#endif // MSNHROBOTPROPERTY_H
