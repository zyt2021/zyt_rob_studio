﻿#ifndef MSNHSPOTLIGHTPROPERTY_H
#define MSNHSPOTLIGHTPROPERTY_H

#include <QDialog>
#include <Core/MsnhSpotLight.h>

namespace Ui {
class MsnhSpotLightProperty;
}

class MsnhSpotLightProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhSpotLightProperty(SpotLight *light, QWidget *parent = nullptr);
    ~MsnhSpotLightProperty();

private:
    Ui::MsnhSpotLightProperty *ui;

    SpotLight *_host = nullptr;

private slots:
    void configSignals();
    void updateVals();

    void changePos(float);
    void updatePos(QVector3D);

    void changeDirection(float);
    void updateDirection(QVector3D);

    void hostDestroyed();
};

#endif // MSNHSPOTLIGHTPROPERTY_H
