#ifndef MSNHTRANSFORMPROPERTY_H
#define MSNHTRANSFORMPROPERTY_H

#include <QDialog>
#include <Core/MsnhTransform.h>

namespace Ui {
class MsnhTransformProperty;
}

class MsnhTransformProperty : public QDialog
{
    Q_OBJECT

public:
    explicit MsnhTransformProperty(Transform *tf, QWidget *parent = nullptr);
    ~MsnhTransformProperty();

    void hostDestroyed(QObject *host);

private slots:
    void on_visibleCkbx_clicked();

    void updateVals();
    void configSignals();

    void changePos(float);
    void changeRot(float);
    void changeScaling(float);

    void updatePos(QVector3D);
    void updateRot(QVector3D);
    void updateScaling(QVector3D);
private:
    Transform *_host      = nullptr;
    Ui::MsnhTransformProperty *ui;
};

#endif // MSNHTRANSFORMPROPERTY_H
