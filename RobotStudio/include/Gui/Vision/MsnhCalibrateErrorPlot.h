﻿#ifndef MSNHCALIBRATEERRORPLOT_H
#define MSNHCALIBRATEERRORPLOT_H

#include <QDialog>
#include <QVector>
#include <Gui/Plot/QCustomPlot.h>

namespace Ui {
class MsnhCalibrateErrorPlot;
}

class MsnhCalibrateErrorPlot : public QDialog
{
    Q_OBJECT
public:
    explicit MsnhCalibrateErrorPlot(QWidget *parent = nullptr);
    ~MsnhCalibrateErrorPlot();
    void clearDatas();

    void addValue(double value);
    void init();
private slots:
    void on_savePicBtn_clicked();

private:
    Ui::MsnhCalibrateErrorPlot *ui;
    QCPBars *_singleError = nullptr;
    QVector<double> _ticks;
    QVector<QString> _labels;
    QVector<double> _barValues;
    int _count = 0;
    double _min = 0;
    double _max = 0;
};

#endif // MSNHCALIBRATEERRORPLOT_H
