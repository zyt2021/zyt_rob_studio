﻿#ifndef MSNH_CALIBRATE_INTRIN_WINDOW_H
#define MSNH_CALIBRATE_INTRIN_WINDOW_H
#include "Msnhnet/Msnhnet.h"
#include <QMainWindow>
#include <QFileDialog>
#include <QLabel>
#include <opencv2/opencv.hpp>
#include <QVector>
#include <Gui/Vision/MsnhIntrinParamsWindow.h>
#include <Gui/Vision/MsnhCalibrateErrorPlot.h>

class MsnhLoopThread;
class MsnhProcess;
namespace MsnhProtocal {
class Base;
}

namespace Ui {
class MsnhCalibrateIntrinWindow;
}

class MsnhCalibrateIntrinWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MsnhCalibrateIntrinWindow(QWidget *parent = nullptr);
    ~MsnhCalibrateIntrinWindow();
    void showImage(const cv::Mat &mat);

protected:
    void closeEvent(QCloseEvent * event);

private slots:
    void revData(MsnhProtocal::Base* data);

    bool setParams();

    void on_openCamBtn_clicked();

    void on_closeCamBtn_clicked();

    void on_refreshBtn_clicked();

    void on_switchBtn_clicked();

    void on_takePicBtn_clicked();

    /**@brief 图片列表更新 */
    void refreshImgList();

    void on_clearImgListBtn_clicked();

    void on_calibBtn_clicked();

    void on_importImgBtn_clicked();

    void on_imgList_doubleClicked(const QModelIndex &index);

    void on_checkChessCbx_clicked();

    void on_undistortCbx_clicked();

    void on_importIntrinParamBtn_clicked();

private:
    Ui::MsnhCalibrateIntrinWindow *ui;
    MsnhLoopThread* _loopThread;
    MsnhProcess* _process;
    QString _currentIOType;


    //
    QLabel *_status;
    QLabel *_statusCaption;

    // ======== 内参标定
    cv::Size _boardSize;   //角点个数
    cv::Size2f _boxSize;    //单个棋盘方块长宽
    QVector<cv::Mat> _imgListBuff;    //imgList缓存

    /**@brief 保存检测到的所有角点 */                      //第1个板在图像坐标系下位置..第n个板在图像坐标系下位置
    std::vector<std::vector<cv::Point2f>> _image2DPoints;//{{[222.9,156.3],...},...{[421.0,213.5],...}}
     /**@brief 保存标定板上角点的三维坐标 */               //第1个标定板所有角点三维位置..第n个标定板所有角点三维位置
    std::vector<std::vector<cv::Point3f>> _object3DPoints; //{{[0,0,0],[0,20,0]...},....{{[0,0,0],[0,20,0]...}}

    /*内外参数*/

    /**@brief 摄像机内参数矩阵 */
    cv::Mat _cameraMatrix = cv::Mat(3, 3, CV_64F, cv::Scalar::all(0));
    /**@brief 摄像机的5/8/12/14个畸变系数：k1,k2,p1,p2,k3 */
    cv::Mat _distCoeffs = cv::Mat(1, 14, CV_64F, cv::Scalar::all(0));
    /**@brief 每幅图像的旋转向量 */
    std::vector<cv::Mat> _tvecsMat;
    /**@brief 每幅图像的平移向量 */
    std::vector<cv::Mat> _rvecsMat;
    /**@brief 每幅图像的标定误差 */
    std::vector<double> _errList;
    /**@brief 整体平均误差 */
    double _allErr = 0;
    /**@brief 是否已标定 */
    bool _calibrated = false;

    bool _checkChessBoard = false;

    int _coeffsNum = 5;

    MsnhIntrinParamsWindow *_paramsWindow = nullptr;
    MsnhCalibrateErrorPlot *_intrinCalibPlot = nullptr;

//    Msnhnet::NetBuilder  msnhNet;
//    std::vector<std::string> labels ;
};

#endif // MSNH_CALIBRATE_INTRIN_WINDOW_H
