﻿#ifndef MSNHCALIBRATESTEREOWINDOW_H
#define MSNHCALIBRATESTEREOWINDOW_H
#include "Msnhnet/Msnhnet.h"
#include <QMainWindow>
#include <QFileDialog>
#include <QLabel>
#include <opencv2/opencv.hpp>
#include <QVector>
#include <Gui/Vision/MsnhIntrinParamsWindow.h>
#include <Gui/Vision/MsnhCalibrateErrorPlot.h>

class MsnhLoopThread;
class MsnhProcess;
namespace MsnhProtocal {
class Base;
}

namespace Ui {
class MsnhCalibrateStereoWindow;
}

class MsnhCalibrateStereoWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MsnhCalibrateStereoWindow(QWidget *parent = nullptr);
    ~MsnhCalibrateStereoWindow();

private slots:
    void showImage1(const cv::Mat &mat);

    void showImage2(const cv::Mat &mat);

    void revData1(MsnhProtocal::Base* data);

    void revData2(MsnhProtocal::Base* data);

    bool refreshImgList();

    bool setParams();

    void on_singleCamDoubleImgCkbx_clicked();

    void on_refreshBtn_clicked();

    void on_switchBtn_clicked();

    void on_openCamBtn_clicked();

    void on_closeCamBtn_clicked();

    void on_refreshBtn_2_clicked();

    void on_switchBtn_2_clicked();

    void on_openCamBtn_2_clicked();

    void on_closeCamBtn_2_clicked();

    void on_takePicBtn_clicked();

    void on_importLeftImgBtn_clicked();

    void on_importRightImgBtn_clicked();

    void on_collectImgBtn_clicked();

    void on_clearImgListBtn_clicked();

    void on_calibBtn_clicked();

private:
    Ui::MsnhCalibrateStereoWindow *ui;
    MsnhLoopThread* _loopThread;
    MsnhProcess* _process;
    QString _currentIOType;

    MsnhLoopThread* _loopThread2;
    MsnhProcess* _process2;
    QString _currentIOType2;
    //
    QLabel *_status;
    QLabel *_statusCaption;

    // ======== 内参标定
    cv::Size _boardSize;   //角点个数
    cv::Size2f _boxSize;    //单个棋盘方块长宽
    QVector<cv::Mat> _imgListBuff1;    //imgList缓存
    QVector<cv::Mat> _imgListBuff2;    //imgList缓存

    QList<QString> leftCamPaths;
    QList<QString> rightCamPaths;

    QVector<cv::Mat> leftImages;
    QVector<cv::Mat> rightImages;

    int tmpWL = 0;
    int tmpWR = 0;
    int tmpHL = 0;
    int tmpHR = 0;

    /**@brief 每幅L图像的旋转向量 */
    std::vector<cv::Mat> tvecsMatL;
    /**@brief 每幅R图像的旋转向量 */
    std::vector<cv::Mat> tvecsMatR;
    /**@brief 每幅L图像的平移向量 */
    std::vector<cv::Mat> rvecsMatL;
    /**@brief 每幅R图像的平移向量 */
    std::vector<cv::Mat> rvecsMatR;
    /**@brief 每幅L图像的标定误差 */
    std::vector<double> errListL;
    /**@brief 每幅R图像的标定误差 */
    std::vector<double> errListR;
    /**@brief L整体平均误差 */
    double allErrL = 0;
    /**@brief T整体平均误差 */
    double allErrR = 0;

    //stereoCalibrate参数
    /**@brief 相机1内参 */
    cv::Mat cameraMatrixL=cv::Mat(3, 3, CV_64F, cv::Scalar::all(0));
    /**@brief 相机2内参 */
    cv::Mat cameraMatrixR=cv::Mat(3, 3, CV_64F, cv::Scalar::all(0));
    /**@brief 相机1畸变参数 */
    cv::Mat distCoeffsL = cv::Mat(1, 14, CV_64F, cv::Scalar::all(0));
    /**@brief 相机2畸变参数 */
    cv::Mat distCoeffsR = cv::Mat(1, 14, CV_64F, cv::Scalar::all(0));
    /**@brief 相机1到2的旋转矩阵 */
    cv::Mat R;
    /**@brief 相机1到2的偏移矩阵 */
    cv::Vec3d T;
    /**@brief 本征矩阵 */
    cv::Mat E;
    /**@brief 基础矩阵 */
    cv::Mat F;
    /**@brief L矫正矩阵 */
    cv::Mat RL;
    /**@brief R矫正矩阵 */
    cv::Mat RR;
    /**@brief L投影矩阵 */
    cv::Mat PL;
    /**@brief R投影矩阵 */
    cv::Mat PR;
    /**@brief  */
    cv::Mat Q;

    cv::Rect roiL;

    cv::Rect roiR;

    MsnhIntrinParamsWindow *_paramsWindow = nullptr;
    MsnhCalibrateErrorPlot *_leftCalibPlot = nullptr;
    MsnhCalibrateErrorPlot *_rightCalibPlot = nullptr;

    bool _singleCamDoubleImgMode = false;
    bool _checkChessBoard = false;
    bool _calibrated       = false;
};

#endif // MSNHCALIBRATESTEREOWINDOW_H
