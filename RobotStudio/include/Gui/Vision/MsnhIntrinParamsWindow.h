﻿#ifndef MSNHINTRINPARAMSWINDOW_H
#define MSNHINTRINPARAMSWINDOW_H

#include <QDialog>
#include <opencv2/opencv.hpp>

namespace Ui {
class MsnhIntrinParamsWindow;
}

class MsnhIntrinParamsWindow : public QDialog
{
    Q_OBJECT
public:
    explicit MsnhIntrinParamsWindow(QWidget *parent = nullptr);
    ~MsnhIntrinParamsWindow();

    void setParams(const cv::Mat& cameraMat, const cv::Mat& coeffs, int coeffsNum);

private slots:
    void on_saveBtn_clicked();

private:
    Ui::MsnhIntrinParamsWindow *ui;
    int _coeffsNum = 5;
    cv::Mat _cameraMat;
    cv::Mat _coeffs;
};

#endif // MSNHINTRINPARAMSWINDOW_H
