﻿#ifndef FPSCOUNTER_H
#define FPSCOUNTER_H
#include <Core/MsnhHeaders.h>

class FpsCounter : public QObject
{
    Q_OBJECT
public:
    FpsCounter(QObject *parent = nullptr);

signals:
    void fpsChanged(int fps);

public slots:
    void increase();

private:
    QTimer *timer = nullptr;
    int fps       = 0;

private slots:
    void timeout();
};

#endif // FPSCOUNTER_H
