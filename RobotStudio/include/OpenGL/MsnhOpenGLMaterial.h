﻿#ifndef OPENGLMATERIAL_H
#define OPENGLMATERIAL_H
#include <Core/MsnhMaterial.h>
#include <OpenGL/MsnhOpenGLUBO.h>
#include <OpenGL/MsnhOpenGLTexture.h>

class OpenGLMaterial: public QObject
{
    Q_OBJECT

public:
    OpenGLMaterial(Material* material, QObject* parent = nullptr);
    ~OpenGLMaterial();
    Material* getHost() const;

    void bind();
    void release();

private:
    Material *_host                        =   nullptr;
    OpenGLTexture *_openGLDiffuseTexture   =   nullptr;
    OpenGLTexture *_openGLSpecularTexture  =   nullptr;
    OpenGLTexture *_openGLBumpTexture      =   nullptr;
    static OpenGLUBO *_materialInfo;  //全局UBO

private slots:
    void diffuseTextureChanged(QSharedPointer<Texture> diffuseTexture);
    void specularTextureChanged(QSharedPointer<Texture> specularTexture);
    void bumpTextureChanged(QSharedPointer<Texture> bumpTexture);
    void hostDestroyed(QObject* host);
};

#endif // OPENGLMATERIAL_H
