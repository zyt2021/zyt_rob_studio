﻿#ifndef OPENGLMESH_H
#define OPENGLMESH_H

#include <Core/MsnhMesh.h>
#include <OpenGL/MsnhOpenGLMaterial.h>

class OpenGLMesh: public QObject {
    Q_OBJECT

public:
    OpenGLMesh(Mesh* mesh, QObject* parent = nullptr);
    ~OpenGLMesh();

    Mesh* getHost() const;

    void create();
    void commit();

    /// \brief render
    /// \param pickingPass 渲染pick层
    /// \param renderLines  渲染polygon模式的线框, 直接渲染线框会出现线乱连的情况
    void render(bool pickingPass = false);
    void destroy();

    void setSizeFixed(bool sizeFixed);
    void setPickingID(uint id);

protected:
    void childEvent(QChildEvent *event) override;

private:
    Mesh *_host        =   nullptr;
    bool _sizeFixed    =   false;
    uint _pickingID    =   0;

    QOpenGLVertexArrayObject *_vao      =   nullptr;
    QOpenGLBuffer *_vbo                 =   nullptr;
    QOpenGLBuffer *_ebo                 =   nullptr;
    MSNH_OPENGL *glFuncs  =   nullptr;
    OpenGLMaterial *_openGLMaterial     =   nullptr;

    static OpenGLUBO *_modelInfo;  //全局UBO

private slots:
    void materialChanged(Material* material);
    void geometryChanged(const QVector<Vertex>& vertices, const QVector<uint32_t>& indices);
    void hostDestroyed(QObject* host);
};


#endif // OPENGLMESH_H
