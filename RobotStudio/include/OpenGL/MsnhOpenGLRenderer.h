﻿#ifndef OPENGLRENDERER_H
#define OPENGLRENDERER_H

#include <OpenGL/MsnhOpenGLScene.h>

class OpenGLRenderer: public QObject {
    Q_OBJECT

public:
    OpenGLRenderer(QObject* parent = nullptr);
    OpenGLRenderer(const OpenGLRenderer& renderer, QObject* parent = nullptr);
    ~OpenGLRenderer();
    bool hasErrorLog();
    QString errorLog();

    bool reloadShaders();
    void reloadFrameBuffers();

    uint32_t pickingPass(OpenGLScene* openGLScene, QPoint cursorPos);
    void render(OpenGLScene* openGLScene);

    void setScreenSize(const QVector2D &screenSize);

private:
    QString _log;
    QOpenGLShaderProgram *_basicShader          =   nullptr;
    QOpenGLShaderProgram *_pointCloudShader     =   nullptr;
    QOpenGLShaderProgram *_pickingShader        =   nullptr;
    QOpenGLShaderProgram *_phongShader          =   nullptr;
    QOpenGLShaderProgram *_skyShader            =   nullptr;
    QOpenGLFramebufferObject *_pickingPassFBO   =   nullptr;

    QOpenGLShaderProgram * loadShaderFromFile(
                                                QString vertexShaderFilePath,
                                                QString fragmentShaderFilePath,
                                                QString geometryShaderFilePath = "");

    QVector2D _screenSize;
};


#endif // OPENGLRENDERER_H
