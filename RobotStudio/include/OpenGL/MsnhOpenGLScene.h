﻿#ifndef OPENGLSCENE_H
#define OPENGLSCENE_H

#include <Core/MsnhScene.h>
#include <OpenGL/MsnhOpenGLMesh.h>
#include <OpenGL/MsnhOpenGLUBO.h>
#include <OpenGL/MsnhOpenGLSky.h>

// pick pass
// background   : 0
// gizmo        : [01-29] 29
// light        : [30-99] 70
// models       : [100~16777215] 1000w+
#define GIZMO_START_ID 1
#define LIGHT_START_ID 30
#define MODELS_START_ID 100

class OpenGLScene: public QObject
{
    Q_OBJECT

public:
    OpenGLScene(QObject *parent = nullptr);
    OpenGLScene(Scene *scene, QObject *parent = nullptr);
    ~OpenGLScene();
    Scene *getHost() const;

    OpenGLMesh* pick(uint32_t pickingID);

    // add new item step 2
    void renderAxis(bool pickPass);
    void renderGridlines();
    void renderCurves(bool onTopRender);
    void renderLights();
    void renderModels(bool pickingPass = false);
    void renderCloudPoints(bool renderPureColor = true); //是否使用Material的纯颜色
    void renderSky();
    void renderSelectAndHighLight();
    void renderCamera();

    void commitCameraInfo();
    void commitLightInfo();

    Sky *getSky() const;

    void setHost(Scene *host);

protected:
    void childEvent(QChildEvent *event) override;

private:

    // add new item step 0 想让模型能像model一样能被点选, 就直接push给normalMeshes就行
    QVector<OpenGLMesh *> _gizmoMeshes;
    QVector<OpenGLMesh *> _curveMeshes;
    QVector<OpenGLMesh *> _robotCurveMeshes;
    QVector<OpenGLMesh *> _gridlineMeshes;
    QVector<OpenGLMesh *> _lightMeshes;
    QVector<OpenGLMesh *> _normalMeshes;
    QVector<OpenGLMesh *> _pointsCloudMeshes;

    OpenGLMesh *_cameraCenterMesh       = nullptr;

    //全局UBO
    static OpenGLUBO *_cameraInfo;
    static OpenGLUBO *_lightInfo;

    Scene *_host    =   nullptr;
    Sky *_sky       =   nullptr;

private slots:
    // add new item step 1
    void gizmoAdded(AbstractGizmo* gizmo);
    void cameraAdded(Camera* camera);
    void gridlineAdded(Gridline* gridline);
    void curveAdded(Curve3D* curve);
    void tfAdded(Transform* tf);
    void tfGroupAdded(TransformGroup* tfGroup);
    void lightAdded(AbstractLight* light);
    void modelAdded(Model* model);
    void meshAdded(Mesh* mesh);
    void pointsCloudAdded(PointsCloud* poinsClouds);
    void robotAdded(Robot* robot);
    void hostDestroyed(QObject* getHost);
};


#endif // OPENGLSCENE_H
