﻿#ifndef SKY_H
#define SKY_H

#include <Core/MsnhAbstractEntity.h>
#include <QObject>

class Sky : public QObject
{
    Q_OBJECT
public:
    explicit Sky(QObject *parent = nullptr);
    ~Sky();

    void create();
    void destroy();

    QVector2D getAspectRatio() const;

    QVector3D getLightPos() const;

    void setAspectRatio(const QVector2D &aspectRatio);

    void setLightPos(const QVector3D &lightPos);

    void render();

private:
    QOpenGLVertexArrayObject *_vao      =   nullptr;
    QOpenGLBuffer *_vbo                 =   nullptr;
    MSNH_OPENGL *glFuncs  =   nullptr;

    QVector2D _aspectRatio;
    QVector3D _lightPos;
};

#endif // SKY_H
