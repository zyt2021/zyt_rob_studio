﻿#ifndef OPENGLTEXTURE_H
#define OPENGLTEXTURE_H
#include <Core/MsnhTexture.h>

class OpenGLTexture: public QObject
{
    Q_OBJECT
public:
    OpenGLTexture(Texture* texture);
    ~OpenGLTexture();

    void create();
    bool bind();
    void release();

private:
    Texture *_host;
    QOpenGLTexture *_openGLTexture;

private slots:
    void imageChanged(const QImage& image);
    void hostDestroyed(QObject* host);
};
#endif // OPENGLTEXTURE_H
