﻿#ifndef OPENGLWINDOW_H
#define OPENGLWINDOW_H

#include <OpenGL/MsnhOpenGLScene.h>
#include <OpenGL/MsnhOpenGLRenderer.h>
#include <OpenGL/MsnhOpenGLFpsCounter.h>
#include <QPainter>
#include <QOpenGLPaintDevice>
#include <QOpenGLDebugLogger>
#include <Msnhnet/robot/MsnhRobot.h>
#include <Util/MsnhTree.h>
#include <Physics/MsnhPhysicalEngine.h>
#include <QOpenGLWidget>

class MsnhTool
{
public:
    MsnhTool(QString name, QIcon icon):_name(name),_icon(icon){}

    MsnhTool() {}

    QIcon getIcon()
    {
        return _icon;
    }

    QString getName()
    {
        return _name;
    }

    void addSubTool(MsnhTool t)
    {
        _subTools.push_back(t);
    }

    void swap(int index)
    {
        MsnhTool temp = _subTools[index];
        _subTools.remove(index);
        MsnhTool t(_name, _icon);
        _subTools.push_front(t);
        _icon = temp.getIcon();
        _name = temp.getName();
    }

    QVector<MsnhTool> getSubTools() const
    {
        return _subTools;
    }

private:
    QString _name;
    QIcon _icon;
    QVector<MsnhTool> _subTools;
};

class JointNameTree
{
    QString name;
    QVector<QSharedPointer<JointNameTree>> next;
};

class OpenGLWindow: public QOpenGLWindow,public btIDebugDraw, protected MSNH_OPENGL
{
    Q_OBJECT

public:
    OpenGLWindow();
    ~OpenGLWindow();
    OpenGLWindow(OpenGLScene* openGLScene, OpenGLRenderer* renderer);

    QString getVendor();
    QString getRendererName();
    QString getOpenGLVersion();
    QString getShadingLanguageVersion();

    void setAxisMode(AxisMode axisMode);
    void setScene(OpenGLScene* openGLScene);
    void setRenderer(OpenGLRenderer* renderer);
    void setEnableMousePicking(bool enabled);
    void setCustomRenderingLoop(void (*customRenderingLoop)(Scene*));

    Model *loadURDF(const QString & path);
    Model *loadModel(const std::shared_ptr<Msnhnet::URDFLinkTree> &link, const std::string & rootPath);

    void resetOpenglScene();
    void resetOpenglScene(Scene *scene);

    OpenGLScene *getOpenGLScene() const;
    OpenGLRenderer *getRenderer() const;

    void setDynamicWorld(btDynamicsWorld* world);

    void setDebugMode(int debugMode) override{_debugMode=debugMode;}

    int getDebugMode() const override{return _debugMode;}

    void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color) override;

    void drawContactPoint(const btVector3 &pointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color) override;

    void ToggleDebugFlag(int flag);

    virtual void  reportErrorWarning(const char* ) override {}

    virtual void  draw3dText(const btVector3 &,const char* ) override {}

    void addTool(MsnhTool t);

    void addBreak();

    void setToolActivated(int activated);

protected:
    void initializeGL() override;
    void paintGL() override;
    //void paintEvent(QPaintEvent *event) override;
    bool event(QEvent *ev) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void focusOutEvent(QFocusEvent *event) override;

signals:
    void fpsChanged(int fps);
    void openScene(QString path);
    //Tool Box
    void toolChanged(int index);

private:
    void (*_customRenderingLoop)(Scene *);
    void processUserInput();
    void configSignals();

private:
    QHash<int, bool> _keyPressed;
    QPoint _lastCursorPos;
    QTime _lastMousePressTime;
    OpenGLScene *_openGLScene;
    OpenGLRenderer *_renderer;
    FpsCounter *_fpsCounter;
    QOpenGLDebugLogger *logger = nullptr;
    btDynamicsWorld* _world;

    QMap<QString,Model*> _axis;
//  QMap<QString,Model*> _joints;

    bool _enableMousePicking    =   false;

// toolBox
    int _activated;
    int _padding;
    int _subToolActivated;
    QSize _buttonSize;
    QVector<QRect> _toolRects;
    QVector<QRect> _secondaryTools;
    QVector<MsnhTool> _tools;
    QVector<int> _breaks;
    QTimer _timer;
    bool _longPressed;
    bool _toolBoxHandlePressed;
    QPoint _lastMousePos;
    bool _toolArea;
// ==

private slots:
    void sceneDestroyed(QObject* host);
    void longPressEvent();

protected:
    int _debugMode = 1;
};


#endif // OPENGLWINDOW_H
