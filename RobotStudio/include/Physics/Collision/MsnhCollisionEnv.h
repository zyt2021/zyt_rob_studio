#ifndef COLLISIONENV_H
#define COLLISIONENV_H
#include <Msnhnet/robot/MsnhURDF.h>
#include <Physics/Collision/MsnhCollisionManager.h>
#include <Core/MsnhModel.h>

class CollisionEnv
{
public:
    CollisionEnv(const QMap<QString,QVector<GeomModel>>& geommeshes);

    void checkSelfCollision(const CollisionRequest & req, CollisionResult& res, const QMap<QString,Model*>& joints, const AllowedCollisionMatrix& acm);

    void checkSelfCollision(const CollisionRequest & req, CollisionResult& res, const QMap<QString,Model*>& joints1, const QMap<QString,Model*>& joints2, const AllowedCollisionMatrix& acm);

    void checkSelfCollisionHelper(const CollisionRequest & req, CollisionResult& res, const QMap<QString,Model*>& joints, const AllowedCollisionMatrix * acm);

    void checkSelfCollisionHelperCCD(const CollisionRequest & req, CollisionResult& res, const QMap<QString,Model*>& joints1, const QMap<QString,Model*>& joints2, const AllowedCollisionMatrix * acm);
protected:
    //已激活的碰撞物体列表
    std::vector<std::string> _activeCollisionNames;

    //被mutable修饰的变量，将永远处于可变的状态，即使在一个const函数中。
    mutable std::shared_ptr<BulletDiscreteBVHManager>   _manager    {new BulletDiscreteBVHManager()};
    mutable std::shared_ptr<BulletCastBVHManager>       _managerCCD {new BulletCastBVHManager()};
};

#endif // COLLISIONENV_H
