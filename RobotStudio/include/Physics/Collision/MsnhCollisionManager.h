﻿#ifndef COLLISIONMANAGER_H
#define COLLISIONMANAGER_H

#include <Physics/Collision/MsnhCollisionUtils.h>
#include <memory>
#include <map>

/// \brief 层次包围盒接触管理器
class BulletBVHManager
{
public:
    BulletBVHManager();
    virtual ~BulletBVHManager();

    /// \brief 这将用于多线程应用程序。用户应该为每个线程创建一个克隆
    std::shared_ptr<BulletBVHManager> clone() const;
    /// \brief 查找碰撞对象是否已存在
    bool hasCollisionObject(const std::string& name) const;

    /// \brief 从检测器里删除对象
    bool removeCollisionObject(const std::string& name);

    /// \brief 使能一个对象
    bool enableCollisionObject(const std::string& name);

    /// \brief 失能一个对象
    bool disableCollisionObject(const std::string& name);

    /// \brief 设置单个静态碰撞对象的tansform
    void setCollisionObjectsTransform(const std::string& name, const Msnhnet::Frame& pose);

    /// \brief 设置哪些碰撞对象处于活动状态
    void setActiveCollisionObjects(const std::vector<std::string>& names);

    /// \brief 获取哪些碰撞对象处于活动状态
    const std::vector<std::string>& getActiveCollisionObjects() const;

    /// \brief 设置应考虑碰撞的接触距离阈值
    void setContactDistanceThreshold(double contactDistance);

    /// \brief 获取应考虑碰撞的接触距离阈值
    double getContactDistanceThreshold() const;


    /// \brief contactTest 碰撞检测
    /// \param collisions
    /// \param req 配置
    /// \param acm AllowedCollisionMatrix
    /// \param self 用于指示自碰撞检查
    virtual void contactTest(CollisionResult& collisions,
                             const CollisionRequest& req,
                             const AllowedCollisionMatrix* acm, bool self) = 0;

    /// \brief addCollisionObject 将碰撞对象添加到检测器
    /// \param collisionObj
    virtual void addCollisionObject(const std::shared_ptr<CollisionObject>& collisionObj) = 0;

    const std::map<std::string, std::shared_ptr<CollisionObject>>& getCollisionObjects() const;

protected:

    /// \brief 管理碰撞检测物体的映射
    std::map<std::string,CollisionObjectPtr> _link2CollisionObj;

    /// \brief 激活的碰撞检测连杆 的列表
    std::vector<std::string> _active;

    /// \brief 碰撞检测距离阈值
    double _contactDistance;

    /// \brief 用于获取对象到对象碰撞算法的子弹碰撞调度器
    std::unique_ptr<btCollisionDispatcher> _dispatcher;

    /// \brief 碰撞检测调度器配置信息
    btDispatcherInfo _dispatcherInfo;

    /// \brief 碰撞配置
    btDefaultCollisionConfiguration _collisionConfig;

    /// \brief broadphase接口
    std::unique_ptr<btBroadphaseInterface> _broadphase;

    /// \brief 回调函数用于在broadphase检查之前剔除对象
    BroadPhaseFilterCallback _filterCallback;
};

/// \brief tesseract碰撞管理器扩展
class BulletCastBVHManager : public BulletBVHManager
{
public:
    BulletCastBVHManager() = default;

    ~BulletCastBVHManager() override = default;

    /// \brief clone 这将用于多线程应用程序。用户应该为每个线程创建一个克隆
    std::shared_ptr<BulletCastBVHManager> clone() const;

    /// \brief setCastCollisionObjectsTransform  设置单个cast (moving)碰撞对象的tansforms **这只能用于移动对象。
    /// \param name
    /// \param pose1
    /// \param pose2
    void setCastCollisionObjectsTransform(const std::string& name, const Msnhnet::Frame& pose1,
                                              const Msnhnet::Frame& pose2);

    /// \brief contactTest 碰撞检测
    /// \param collisions
    /// \param req 配置
    /// \param acm AllowedCollisionMatrix
    /// \param self 用于指示自碰撞检查
    void contactTest(CollisionResult& collisions, const CollisionRequest& req,
                     const AllowedCollisionMatrix* acm, bool self) override;

    /// \brief addCollisionObject 将tesseract碰撞对象添加到管理器
    /// \param collisionObj  碰撞检测物体
    void addCollisionObject(const CollisionObjectPtr& collisionObj) override;
};


class BulletDiscreteBVHManager : public BulletBVHManager
{
public:
    BulletDiscreteBVHManager() = default;

    ~BulletDiscreteBVHManager() override = default;

    /// \brief clone 这将用于多线程应用程序。用户应该为每个线程创建一个克隆
    std::shared_ptr<BulletDiscreteBVHManager> clone() const;

    /// \brief contactTest 碰撞检测
    /// \param collisions
    /// \param req 配置
    /// \param acm AllowedCollisionMatrix
    /// \param self 用于指示自碰撞检查
    void contactTest(CollisionResult& collisions, const CollisionRequest& req,
        const AllowedCollisionMatrix* acm, bool self) override;

    /// \brief addCollisionObject 将tesseract碰撞对象添加到管理器
    /// \param collisionObj  碰撞检测物体
    void addCollisionObject(const CollisionObjectPtr& collisionObj) override;
};
#endif // COLLISIONMANAGER_H
