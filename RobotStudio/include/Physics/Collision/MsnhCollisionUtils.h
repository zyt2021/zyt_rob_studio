﻿#ifndef COLLISIONOBJECT_H
#define COLLISIONOBJECT_H
#include <bullet/btBulletCollisionCommon.h>
#include <Msnhnet/math/MsnhGeometryS.h>
#include <Msnhnet/robot/MsnhRobot.h>
#include <map>
#include <set>
#include <functional>
#include <limits.h>

///                    | btCompundShape 🧡
/// btCollisionShape --| btConcaveShape ⚪
///                    | btConvexShape  ⭐
///
/// 🧡btCompundShape -- | btConpoundFromeGimpactShape
///
///                      | btEmptyShape                    | btGImpactCompoundShape
///                      | btGImpactShapeInterface     --- | btGImpactMeshShape
///  凹面                 | btHeightfieldTerrainShape       | btGImpactMeshShapePart
/// ⚪btConcaveShape --- | btScaledBvhTriangleMeshShape
///                      | btSdfCollisionShape
///                      | btSoftBodyCollisionShape
///                      | btStaticPlaneShape   --- btPlaneShape
///                      | btTraingleMeshShape  --- btBvhTriangleMeshShape --- btMultimaterialTriangleMeshShape
///
///                      | btConvex2dShape
/// ⭐btConvexShape ---  | btConvexInternalShape ⭐
///                      | btUniformScalingShape
///
///
///
///                            | btCapsuleShape
///                            | btConeShape
///                            | btConvexInternalAABBCachingShape
/// ⭐btConvexInternalShape ---| btCylinderShape                       | btBox2dShape
///                            | btMinkowskiSumShape                   | bxBoxShape
///                            | btPolyhedraConvexShape            ----| btPolyhedralconvexAABBCachingShape ⭐
///                            | btSoftClusterCollisionShape           | btTriangleShape --- btTriangleShapeEx
///                            | btSphereShape
///
///                                          | btBU_Simplex1to4
/// ⭐btPolyhedralconvexAABBCachingShape --- | btConvexHullShape
///                                          | btConvexPointCloudShape
///                                          | btConvexTriangleMeshShape


///                      | btGhostObject --- btPairCachingGhostObject
/// btCollisionObject ---| btMultiBodyLinkCollider
///                      | btRigidBody
///                      | btSoftBody

#define BULLET_DEFAULT_CONTACT_DISTANCE 0.00f
#define BULLET_EPSILON 1e-3f
#define BULLET_COMPOUND_USE_DYNAMIC_AABB true
#define BULLET_MARGIN 0.0f
#define BULLET_SUPPORT_FUNC_TOLERANCE 0.01f //米
#define BULLET_LENGTH_TOLERANCE 0.001f //米

enum BodyType
{
    /// \brief 机器人上的连杆
    BODY_ROBOT_LINK,
    /// \brief 黏附在机器人上的物体
    BODY_ROBOT_ATTACHED,
    /// \brief 世界环境的物体
    BODY_WORLD_OBJECT
};

enum CollisionObjectType
{
    /// \brief 使用物体形状最为碰撞Object
    COLLISION_USE_SHAPE,
    /// \brief 使用凸包作为碰撞Object
    COLLISION_CONVEX_HULL,
    /// \brief 使用多个球作为碰撞的Object
    COLLISION_MULTI_SPHERE
};

enum AllowedCollisionType
{
    /// \brief 表示一对物体之间永远不会发生碰撞
    ALLOW_COLLISION_NEVER,
    /// \brief 一对特定物体之间的碰撞并不意味着机器人配置处于碰撞状态。不需要对这对物体进行显式计算（检查接触）
    ALLOW_COLLISION_ALWAYS,
    /// \brief
    ALLOW_COLLISION_CONDITIONAL
};

/// \brief 接触点定义
struct Contact
{
    /// \brief 接触位置
    Msnhnet::TranslationDS pos;

    /// \brief 接触点的单位方向向量
    Msnhnet::Vector3DS normal;

    /// \brief 穿透深度
    double depth;

    /// \brief 两两接触的第一个物体名称
    std::string bodyName1;

    /// \brief 两两接触的第一个物体类型
    BodyType bodyType1;

    /// \brief 两两接触的第二个物体名称
    std::string bodyName2;

    /// \brief 两两接触的第二个物体类型
    BodyType bodyType2;

    /// \brief 在碰撞之前投射姿势之间的距离百分比。[0~1]如果该值为0，则碰撞发生在起始姿势。如果该值为1，则碰撞发生在结束姿势。
    double percentInterp;

    /// \brief 两个物体间最近的两个点
    Msnhnet::Vector3DS nearestPoints[2];
};

/// \brief ALLOW_COLLISION_CONDITIONAL参数下contact函数
typedef std::function<bool(Contact&)> DecideContactFun;

/// Entry: 以键值对（key-value pair）的形式定义。
/// \brief 此类表示允许发生哪些碰撞，不允许发生哪些碰撞
class AllowedCollisionMatrix
{
public:
    AllowedCollisionMatrix(){}

    /// \brief setEntry 设置一对Entry
    /// \param name1
    /// \param name2
    /// \param allowed 如果为false，则表示必须检查两个元素之间的碰撞，并且不会忽略任何碰撞（AllowedCollision:：NEVER）。如果为true，则表示两个元素之间的碰撞正常，不需要显式碰撞计算（AllowedCollision:：ALWAYS）。
    void setEntry(const std::string& name1, const std::string& name2, bool allowed);

    /// \brief setEntry 设置一对Entry.类型设置为AllowedCollision:：CONDITIONAL。
    /// \param name1
    /// \param name2
    /// \param fun 这里需要一个回调函数，用于确定两个元素之间是否允许碰撞
    void setEntry(const std::string& name1, const std::string& name2, const DecideContactFun& fun);

    /// \brief setEntry 设置与名称对应的Entry
    /// \param name
    /// \param allowed 如果为false，则表示必须检查两个元素之间的碰撞，并且不会忽略任何碰撞（AllowedCollision:：NEVER）。如果为true，则表示两个元素之间的碰撞正常，不需要显式碰撞计算（AllowedCollision:：ALWAYS）。
    void setEntry(const std::string& name, bool allowed);

    ///
    /// \brief setEntry 设置多对Entry,1对多
    /// \param name
    /// \param otheNames 其他的name与name逐个一一对应形成碰撞对
    /// \param allowed 如果为false，则表示必须检查两个元素之间的碰撞，并且不会忽略任何碰撞（AllowedCollision:：NEVER）。如果为true，则表示两个元素之间的碰撞正常，不需要显式碰撞计算（AllowedCollision:：ALWAYS）。
    void setEntry(const std::string& name, const std::vector<std::string>& otheNames, bool allowed);


    /// \brief setEntry 设置多对Entry,多对多
    /// \param names1
    /// \param names2
    /// \param allowed 如果为false，则表示必须检查两个元素之间的碰撞，并且不会忽略任何碰撞（AllowedCollision:：NEVER）。如果为true，则表示两个元素之间的碰撞正常，不需要显式碰撞计算（AllowedCollision:：ALWAYS）。
    void setEntry(const std::vector<std::string>& names1, const std::vector<std::string>& names2, bool allowed);

    /// \brief setEntry 设置已知的Entry对为allowed
    /// \param allowed  如果为false，则表示必须检查两个元素之间的碰撞，并且不会忽略任何碰撞（AllowedCollision:：NEVER）。如果为true，则表示两个元素之间的碰撞正常，不需要显式碰撞计算（AllowedCollision:：ALWAYS）。
    void setEntry(bool allowed);


    /// \brief setDefaultEntry 设置默认Entry
    /// \param name
    /// \param allowed 如果为false，则表示必须检查两个元素之间的碰撞，并且不会忽略任何碰撞（AllowedCollision:：NEVER）。如果为true，则表示两个元素之间的碰撞正常，不需要显式碰撞计算（AllowedCollision:：ALWAYS）。
    void setDefaultEntry(const std::string& name, bool allowed);

    /// \brief setDefaultEntry 设置默认Entry
    /// \param name
    /// \param fun 这里需要一个回调函数，用于确定两个元素之间是否允许碰撞
    void setDefaultEntry(const std::string& name, const DecideContactFun& fun);

    /// \brief getEntry 获取两个元素之间允许的碰撞类型。如果Entry包含在碰撞矩阵中，则返回true
    /// \param name1
    /// \param name2
    /// \param allowedCollisionType 此处将填写允许的碰撞类型
    bool getEntry(const std::string& name1, const std::string& name2, AllowedCollisionType& allowedCollisionType) const;


    /// \brief getEntry 获取两个元素之间允许的操作函数,如果Entry包含在碰撞矩阵中，则返回true
    /// \param name1
    /// \param name2
    /// \param fun 这里填充了一个回调函数，用于确定两个元素之间是否允许碰撞
    bool getEntry(const std::string& name1, const std::string& name2, DecideContactFun& fun) const;


    /// \brief hasEntry 检查允许的碰撞矩阵是否有name的entry。如果包含元素，则返回true
    /// \param name
    bool hasEntry(const std::string& name) const;

    /// \brief hasEntry 检查允许的碰撞矩阵是否有一对元素的entry。如果包含该对，则返回true。
    /// \param name1
    /// \param name2
    bool hasEntry(const std::string& name1, const std::string& name2) const;

    /// \brief removeEntry 删除与名称对应的所有Entry（包括此名称的所有对）
    /// \param name
    void removeEntry(const std::string& name);

    /// \brief removeEntry 删除与一对元素对应的Entry。如果碰撞矩阵中不存在该对，则不会发生任何情况
    /// \param name1
    /// \param name2
    void removeEntry(const std::string& name1, const std::string& name2);

    /// \brief getAllEntryNames 获取碰撞矩阵已知的所有名称
    /// \param names
    void getAllEntryNames(std::vector<std::string>& names) const;

    /// \brief clear 清除允许碰撞的矩阵
    void clear();

    /// \brief getSize 获取允许碰撞的矩阵的大小（指定的Entry数）
    std::size_t getSize() const;

    /// \brief getDefaultEntry 获取元素默认情况下要考虑的允许碰撞的类型。如果为指定元素找到默认值，则返回true。否则返回false。
    /// \param name
    /// \param allowedCollision 允许的碰撞默认允许的碰撞类型将在此处返回
    bool getDefaultEntry(const std::string& name, AllowedCollisionType& allowedCollision) const;

    /// \brief getDefaultEntry 获取元素默认情况下要考虑的允许碰撞的类型。如果为指定元素找到默认值，则返回true。否则返回false。
    /// \param name
    /// \param fn 这里返回一个回调函数，用于确定两个元素之间是否允许碰撞
    bool getDefaultEntry(const std::string& name, DecideContactFun& fn) const;


    /// \brief getAllowedCollision 获取两个元素之间允许的碰撞操作。如果碰撞矩阵中包含Entry的函数（如果类型为AllowedCollision::CONDITIONAL），或者已经默认计算了，则返回true。如果未找到Entry，则返回false。
    /// 默认值优先。如果为name1或name2指定了默认值，则返回该值（如果两个元素都指定了默认值，则必须同时满足两个函数）。如果未指定默认值，则使用getEntry（）查找对（name1，name2）。
    /// \param name1
    /// \param name2
    /// \param fun 返回
    bool getAllowedCollision(const std::string& name1, const std::string& name2, DecideContactFun& fun) const;


    /// \brief getAllowedCollision 获取两个元素之间允许的碰撞类型。如果条目包含在碰撞矩阵中或找到指定的默认值，则返回true。如果未找到条目，则返回false。默认值优先。如果为name1或name2指定了默认值，
    /// 则返回该值（如果两个元素都指定了默认值，则AllowedCollision::NEVER）。如果未指定默认值，则使用getEntry（）查找对（name1name2）。
    /// \param name1
    /// \param name2
    /// \param allowedCollision 返回
    bool getAllowedCollision(const std::string& name1, const std::string& name2, AllowedCollisionType& allowedCollision) const;

private:
    std::map<std::string, std::map<std::string, AllowedCollisionType> > _entries;
    std::map<std::string, std::map<std::string, DecideContactFun> > _allowedContacts;

    std::map<std::string, AllowedCollisionType> _defaultEntries;
    std::map<std::string, DecideContactFun> _defaultAllowedContacts;
};

/// \brief When collision costs are computed, this structure contains information about the partial cost incurred in a particular volume
struct CostSource
{
    /// The minimum bound of the AABB defining the volume responsible for this partial cost
    Msnhnet::Vector3DS aabbMin;

    /// The maximum bound of the AABB defining the volume responsible for this partial cost
    Msnhnet::Vector3DS aabbMax;

    /// The partial cost (the probability of existance for the object there is a collision with)
    double cost;

    /// Get the volume of the AABB around the cost source
    double getVolume() const
    {
        return (aabbMax[0] - aabbMin[0]) * (aabbMax[1] - aabbMin[1]) * (aabbMax[2] - aabbMin[2]);
    }

    /// Order cost sources so that the most costly source is at the top
    bool operator<(const CostSource& other) const
    {
        double c1 = cost * getVolume();
        double c2 = other.cost * other.getVolume();
        if (c1 > c2)
            return true;
        if (c1 < c2)
            return false;
        if (cost < other.cost)
            return false;
        if (cost > other.cost)
            return true;
        return aabbMin.length() < other.aabbMin.length();
    }

};


/// \brief 碰撞检测结果
struct CollisionResult
{
    CollisionResult():collision(false),distance(DBL_MAX),contactCnt(0)
    {
    }

    typedef std::map<std::pair<std::string, std::string>, std::vector<Contact> > ContactMap;

    /// \brief 是否检测到碰撞
    bool collision;

    /// \brief 两个物体最近的点
    double distance;

    /// \brief 接触点个数
    std::size_t contactCnt;

    /// \brief 一对接触物体,包含碰撞信息
    ContactMap contacts;

    /// \brief These are the individual cost sources when costs are computed
    std::set<CostSource> costSources;
};

/// \brief 碰撞检测申请配置
struct CollisionRequest
{
    /// \brief 是否计算临近距离
    bool computeDis;

    /// \brief 是否计算碰撞成本
    bool computeCost;

    /// \brief 是否计算碰撞信息,如果为false,则只计算有没有碰撞,不计算碰撞详细信息
    bool computeContacts;

    /// \brief 需要计算接触碰撞的个数
    std::size_t maxContacts;

    /// \brief 每对实体要计算的最大接触数（多个实体可能在不同配置下接触）
    std::size_t maxContactsPerPair;

    /// \brief 当计算成本时,需要返回最大前多少个cost source
    std::size_t maxCostSource;

    /// \brief When costs are computed, this is the minimum cost density for a CostSource to be included in the results
    std::size_t minCostDensity;

    /// \brief 用于决定什么时候停止碰撞的函数
    std::function<bool(const CollisionResult&)> isDone;

    /// \brief 是否报告检测到的碰撞的信息
    bool verbose;
};

struct ContactTestData
{
    ContactTestData(const std::vector<std::string>& active, const double& contactDistance, CollisionResult& res, const CollisionRequest& req)
        :active(active),contactDistance(contactDistance),res(res),req(req),done(false),pairDone(false)
    {}

    const std::vector<std::string>& active;

    /// \brief 如果在正宽相位检查后距离低于此阈值，则会添加触点。
    const double& contactDistance;

    CollisionResult& res;
    const CollisionRequest& req;

    /// \brief 指示搜索是否完成
    bool done;

    /// \brief 指示单对之间的搜索是否完成
    bool pairDone;
};

inline btVector3 cvtMsnhTrans2Bt(const Msnhnet::TranslationDS& trans)
{
    return btVector3(btScalar(trans[0]),btScalar(trans[1]),btScalar(trans[2]));
}

inline Msnhnet::Vector3DS cvtBt2MsnhTrans(const btVector3& vec)
{
    return Msnhnet::Vector3DS(vec.x(),vec.y(),vec.z());
}

inline btMatrix3x3 cvtMsnhRotMat2Bt(const Msnhnet::RotationMatDS& rot)
{
    return btMatrix3x3(btScalar(rot.val[0]),btScalar(rot.val[1]),btScalar(rot.val[2]),
            btScalar(rot.val[3]),btScalar(rot.val[4]),btScalar(rot.val[5]),
            btScalar(rot.val[6]),btScalar(rot.val[7]),btScalar(rot.val[8]));
}

inline btTransform cvtMsnhFrame2Bt(const Msnhnet::Frame& frame)
{
    return btTransform(cvtMsnhRotMat2Bt(frame.rotMat),cvtMsnhTrans2Bt(frame.trans));
}

/*
 * 围绕bullet的碰撞对象的包装，其中包含与bullet相关的特定信息。主要区别之一是，子弹碰撞对象具有单个世界变换，并且所有形状都具有相对于此世界变换的变换。
 * 默认的碰撞对象类别是活动的，活动对象将根据活动对象和静态对象进行检查，而静态对象仅根据活动对象进行检查。
 */
class CollisionObject : public btCollisionObject
{
public:
    /** \brief Standard constructor
   *
   *  \param shape_poses Assumes all poses are in a single global frame */
    CollisionObject(const std::string& name,
                    const BodyType& type,
                    const std::vector<std::shared_ptr<Msnhnet::URDFGeometry> >& shapes,
                    const std::vector<Msnhnet::Frame>& shapePoses,
                    const std::vector<CollisionObjectType>& collisionObjectTypes,
                    bool active = true);

    /** \brief Constructor for attached robot objects */
    CollisionObject(const std::string& name,
                    const BodyType& type,
                    const std::vector<std::shared_ptr<Msnhnet::URDFGeometry> > &shapes,
                    const std::vector<Msnhnet::Frame>& shapePoses,
                    const std::vector<CollisionObjectType>& collisionObjectTypes,
                    const std::set<std::string>& touchLinks);


    const std::string getName() const
    {
        return _name;
    }

    BodyType getBodyType() const
    {
        return _bodyType;
    }

    /// \brief getAABB 获取碰撞物体的AABB
    /// \param aabbMin 最小的point
    /// \param aabbMax 最大的point
    void getAABB(btVector3& aabbMin, btVector3& aabbMax) const
    {
        getCollisionShape()->getAabb(getWorldTransform(), aabbMin, aabbMax);
        const btScalar& distance = getContactProcessingThreshold();

        /// \brief 请注意，bullet将每个AABB膨胀4厘米
        btVector3 contatcThreshold(distance, distance, distance);
        aabbMin -= contatcThreshold;
        aabbMax += contatcThreshold;
    }

    bool sameObject(const CollisionObject& other)
    {
        for (size_t i = 0; i < _shapes.size(); ++i)
        {
            if(_shapes[i]->type != other._shapes[i]->type)
            {
                return false;
            }
        }

        for (size_t i = 0; i < _shapePoses.size(); ++i)
        {
            if(_shapePoses[i] != other._shapePoses[i])
            {
                return false;
            }
        }

        return _name == other._name &&
                _bodyType == other._bodyType &&
                _shapes.size() == other._shapes.size() &&
                _shapePoses.size() == other._shapePoses.size();

    }

    std::shared_ptr<CollisionObject> clone()
    {
        std::shared_ptr<CollisionObject> cloneCollisonObj(new CollisionObject(_name, _bodyType, _shapes, _shapePoses, _collisionObjectTypes, _data));
        cloneCollisonObj->setCollisionShape(getCollisionShape());
        cloneCollisonObj->setWorldTransform(getWorldTransform());
        cloneCollisonObj->collisionFilterGroup  = collisionFilterGroup;
        cloneCollisonObj->collisionFilterMask   = collisionFilterMask;
        cloneCollisonObj->enabled               = enabled;
        cloneCollisonObj->setBroadphaseHandle(nullptr);
        cloneCollisonObj->touchLinks            = touchLinks;
        cloneCollisonObj->setContactProcessingThreshold(this->getContactProcessingThreshold());
        return cloneCollisonObj;
    }


    //// \brief Manage memory of a raw pointer shape */
    template <class T>
    void manage(T* t)
    {
        _data.push_back(std::shared_ptr<T>(t));
    }

    //// \brief Manage memory of a shared pointer shape
    template <class T>
    void manage(std::shared_ptr<T> t)
    {
        _data.push_back(t);
    }

    /// \brief 位字段指定对象所属的组
    int16_t collisionFilterGroup;

    /// \brief 位字段指定对对象进行碰撞检查的其他组
    int16_t collisionFilterMask;

    /// \brief 指示碰撞对象是否用于碰撞检查
    bool enabled{true};

    /// \brief 机器人连杆允许碰撞接触的对象
    std::set<std::string> touchLinks;

protected:

    /// clone用
    CollisionObject(const std::string& name,
                    const BodyType& type,
                    const std::vector<std::shared_ptr<Msnhnet::URDFGeometry> >& shapes,
                    const std::vector<Msnhnet::Frame>& shapePoses,
                    const std::vector<CollisionObjectType>& collisionObjectTypes,
                    const std::vector<std::shared_ptr<void>>& data);

    /// \brief 碰撞物体的名字
    std::string _name;

    /// \brief 碰撞物体的类型
    BodyType _bodyType;

    /// \brief 碰撞检测形状
    std::vector<std::shared_ptr<Msnhnet::URDFGeometry> > _shapes;

    /// \brief 碰撞检测形状的位姿，个数和碰撞检测形状一样
    std::vector<Msnhnet::Frame> _shapePoses;

    /// \brief 要使用的形状碰撞对象类型。
    std::vector<CollisionObjectType> _collisionObjectTypes;

    /// \brief  管理碰撞形状的指针，以便它们被销毁
    std::vector<std::shared_ptr<void>> _data;
};

typedef std::shared_ptr<CollisionObject> CollisionObjectPtr;
/** \brief
 * btConvexShape 是所有形状类的基类，用于创建如btBoxShape等形状，这里相当于创建了一个新的形状;
 * CastHullShape 投射形状用于检测两个离散姿势之间没有碰撞 */
struct CastHullShape : public btConvexShape //凸包
{
public:
    /// \brief 投射形状
    btConvexShape* shape;
    /// \brief 从第一个姿势到第二个姿势的变换
    btTransform transform;

    CastHullShape(btConvexShape *shape, const btTransform& transform);

    void updateCastTransform(const btTransform& castTransform)
    {
        transform = castTransform;
    }

    /// ======================= 重载函数 ========================

    /// \brief 从两个形状姿势计算支撑顶点并返回较大的顶点
    btVector3 localGetSupportingVertex(const btVector3& vec) const override;

    void batchedUnitVectorGetSupportingVertexWithoutMargin(const btVector3* /*vectors*/,
                                                           btVector3* /*supportVerticesOut*/,
                                                           int /*numVectors*/) const override
    {
      throw std::runtime_error("not implemented");
    }

    /// \brief AABB不是从头开始重新计算，而是根据给定的转换进行更新
    void getAabb(const btTransform& transformWorld, btVector3& aabbMin, btVector3& aabbMax) const override;

    void getAabbSlow(const btTransform& /*t*/, btVector3& /*aabbMin*/, btVector3& /*aabbMax*/) const override
    {
      throw std::runtime_error("shouldn't happen");
    }

    void setLocalScaling(const btVector3& /*scaling*/) override{}

    const btVector3 &getLocalScaling() const override;

    void setMargin(btScalar /*margin*/) override
    {
    }

    btScalar getMargin() const override
    {
        return 0;
    }

    int getNumPreferredPenetrationDirections() const override
    {
        return 0;
    }

    void getPreferredPenetrationDirection(int /*index*/, btVector3& /*penetrationVector*/) const override
    {
        throw std::runtime_error("not implemented");
    }

    void calculateLocalInertia(btScalar /*mass*/, btVector3& /*inertia*/) const override
    {
        throw std::runtime_error("not implemented");
    }

    const char* getName() const override
    {
        return "CastHull";
    }

    btVector3 localGetSupportingVertexWithoutMargin(const btVector3& v) const override
    {
        return localGetSupportingVertex(v);
    }
};

/// \brief getAverageSupport 计算凸面形状的局部支撑顶点。如果存在具有相等支撑乘积的多个顶点，则计算并返回它们的平均值.
/// \param shape 要检查的凸面形状
/// \param localNormal 要搜索形状内局部坐标的支撑方向
/// \param outsupport 计算的支持映射的值
/// \param outpt 计算支撑点
inline void getAverageSupport(const btConvexShape* shape, const btVector3& localNormal, float& outsupport, btVector3& outpt)
{
    btVector3 ptSum(0, 0, 0);
    float ptCount = 0;
    float maxSupport = -1000;

    const btPolyhedralConvexShape* pshape = dynamic_cast<const btPolyhedralConvexShape*>(shape);

    if (pshape)
    {
        int n_pts = pshape->getNumVertices();

        for (int i = 0; i < n_pts; ++i)
        {
            btVector3 pt;
            pshape->getVertex(i, pt);

            float sup = pt.dot(localNormal);
            if (sup > maxSupport + BULLET_EPSILON)
            {
                ptCount = 1;
                ptSum = pt;
                maxSupport = sup;
            }
            else if (sup < maxSupport - BULLET_EPSILON)
            {
            }
            else
            {
                ptCount += 1;
                ptSum += pt;
            }
        }
        outsupport = maxSupport;
        outpt = ptSum / ptCount;
    }
    else
    {
        outpt = shape->localGetSupportingVertexWithoutMargin(localNormal);
        outsupport = localNormal.dot(outpt);
    }
}

inline std::pair<std::string, std::string> getObjectPairKey(const std::string& obj1, const std::string& obj2)
{
    return obj1 < obj2 ? std::make_pair(obj1, obj2) : std::make_pair(obj2, obj1);
}


/** \brief 以请求的方式存储单个碰撞结果。
*   \param found 指示是否已找到这对对象的碰撞对象
*   \return 指向新的碰撞检测的指针 */
inline Contact* processResult(ContactTestData& cdata, Contact& contact, const std::pair<std::string, std::string>& key, bool found)
{
    // add deepest penetration / smallest distance to result
    if (cdata.req.computeDis)
    {
        if (contact.depth < cdata.res.distance)
        {
            cdata.res.distance = contact.depth;
        }
    }
    // case if pair hasn't a contact yet
    if (!found)
    {
        if (contact.depth <= 0)
        {
            cdata.res.collision = true;
        }

        std::vector<Contact> data;

        // if we dont want contacts we are done here
        if (!cdata.req.computeContacts)
        {
            if (!cdata.req.computeDis)
            {
                cdata.done = true;
            }
            return nullptr;
        }
        else
        {
            data.reserve(cdata.req.maxContactsPerPair);
            data.emplace_back(contact);
            cdata.res.contactCnt++;
        }

        if (cdata.res.contactCnt >= cdata.req.maxContacts)
        {
            if (!cdata.req.computeDis)
            {
                cdata.done = true;
            }
        }

        if (cdata.req.maxContactsPerPair == 1u)
        {
            cdata.pairDone = true;
        }

        return &(cdata.res.contacts.insert(std::make_pair(key, data)).first->second.back());
    }
    else
    {
        std::vector<Contact>& dr = cdata.res.contacts[key];
        dr.emplace_back(contact);
        cdata.res.contactCnt++;

        if (dr.size() >= cdata.req.maxContactsPerPair)
        {
            cdata.pairDone = true;
        }

        if (cdata.res.contactCnt >= cdata.req.maxContacts)
        {
            if (!cdata.req.computeDis)
            {
                cdata.done = true;
            }
        }

        return &(dr.back());
    }

    return nullptr;
}
//TODO:
/** \brief 将碰撞检测结果转换为MoveIt格式，并将其添加到结果数据结构中 */
inline btScalar addDiscreteSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap,
                                        const btCollisionObjectWrapper* colObj1Wrap, ContactTestData& collisions)
{
    assert(dynamic_cast<const CollisionObject*>(colObj0Wrap->getCollisionObject()) != nullptr);
    assert(dynamic_cast<const CollisionObject*>(colObj1Wrap->getCollisionObject()) != nullptr);
    const CollisionObject* cd0 = static_cast<const CollisionObject*>(colObj0Wrap->getCollisionObject());
    const CollisionObject* cd1 = static_cast<const CollisionObject*>(colObj1Wrap->getCollisionObject());

    std::pair<std::string, std::string> pc = getObjectPairKey(cd0->getName(), cd1->getName());

    const auto& it = collisions.res.contacts.find(pc);
    bool found = (it != collisions.res.contacts.end());

    Contact contact;
    contact.bodyName1           = cd0->getName();
    contact.bodyName2           = cd1->getName();
    contact.depth               = static_cast<double>(cp.m_distance1);
    contact.normal              = cvtBt2MsnhTrans(-1 * cp.m_normalWorldOnB);
    contact.pos                 = cvtBt2MsnhTrans(cp.m_positionWorldOnA);
    contact.nearestPoints[0]    = contact.pos;
    contact.nearestPoints[1]    = cvtBt2MsnhTrans(cp.m_positionWorldOnB);

    contact.bodyType1           = cd0->getBodyType();
    contact.bodyType2           = cd0->getBodyType();

    if (!processResult(collisions, contact, pc, found))
    {
        return 0;
    }

    return 1;
}

inline btScalar addCastSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap, int /*index0*/,
                                    const btCollisionObjectWrapper* colObj1Wrap, int /*index1*/,
                                    ContactTestData& collisions)
{
    assert(dynamic_cast<const CollisionObject*>(colObj0Wrap->getCollisionObject()) != nullptr);
    assert(dynamic_cast<const CollisionObject*>(colObj1Wrap->getCollisionObject()) != nullptr);

    const CollisionObject* cd0 = static_cast<const CollisionObject*>(colObj0Wrap->getCollisionObject());
    const CollisionObject* cd1 = static_cast<const CollisionObject*>(colObj1Wrap->getCollisionObject());

    std::pair<std::string, std::string> pc = getObjectPairKey(cd0->getName(), cd1->getName());

    const auto& it = collisions.res.contacts.find(pc);
    bool found = (it != collisions.res.contacts.end());

    Contact contact;
    contact.bodyName1           = cd0->getName();
    contact.bodyName2           = cd1->getName();
    contact.depth               = static_cast<double>(cp.m_distance1);
    contact.normal              = cvtBt2MsnhTrans(-1 * cp.m_normalWorldOnB);
    contact.pos                 = cvtBt2MsnhTrans(cp.m_positionWorldOnA);

    contact.bodyType1           = cd0->getBodyType();
    contact.bodyType2           = cd0->getBodyType();


    Contact* col = processResult(collisions, contact, pc, found);

    if (!col)
    {
        return 0;
    }

    assert(!((cd0->collisionFilterGroup == btBroadphaseProxy::KinematicFilter) &&
             (cd1->collisionFilterGroup == btBroadphaseProxy::KinematicFilter)));

    bool cast_shape_is_first = cd0->collisionFilterGroup == btBroadphaseProxy::KinematicFilter;

    btVector3 normal_world_from_cast = (cast_shape_is_first ? -1 : 1) * cp.m_normalWorldOnB;
    const btCollisionObjectWrapper* first_col_obj_wrap = (cast_shape_is_first ? colObj0Wrap : colObj1Wrap);

    // we want the contact information of the non-casted object come first, therefore swap values
    if (cast_shape_is_first)
    {
        std::swap(col->nearestPoints[0], col->nearestPoints[1]);
        contact.pos = cvtBt2MsnhTrans(cp.m_positionWorldOnB);
        std::swap(col->bodyName1, col->bodyName2);
        std::swap(col->bodyType1, col->bodyType2);
        col->normal *= -1;
    }

    btTransform tf_world0, tf_world1;
    const CastHullShape* shape = static_cast<const CastHullShape*>(first_col_obj_wrap->getCollisionShape());

    tf_world0 = first_col_obj_wrap->getWorldTransform();
    tf_world1 = first_col_obj_wrap->getWorldTransform() * shape->transform;

    // transform normals into pose local coordinate systems
    btVector3 normal_local0 = normal_world_from_cast * tf_world0.getBasis();
    btVector3 normal_local1 = normal_world_from_cast * tf_world1.getBasis();

    btVector3 pt_local0;
    float localsup0;
    getAverageSupport(shape->shape, normal_local0, localsup0, pt_local0);
    btVector3 pt_world0 = tf_world0 * pt_local0;
    btVector3 pt_local1;
    float localsup1;
    getAverageSupport(shape->shape, normal_local1, localsup1, pt_local1);
    btVector3 pt_world1 = tf_world1 * pt_local1;

    float sup0 = normal_world_from_cast.dot(pt_world0);
    float sup1 = normal_world_from_cast.dot(pt_world1);

    // TODO: this section is potentially problematic. think hard about the math
    if (sup0 - sup1 > BULLET_SUPPORT_FUNC_TOLERANCE)
    {
        col->percentInterp = 0;
    }
    else if (sup1 - sup0 > BULLET_SUPPORT_FUNC_TOLERANCE)
    {
        col->percentInterp = 1;
    }
    else
    {
        const btVector3& pt_on_cast = cast_shape_is_first ? cp.m_positionWorldOnA : cp.m_positionWorldOnB;
        float l0c = (pt_on_cast - pt_world0).length();
        float l1c = (pt_on_cast - pt_world1).length();

        if (l0c + l1c < BULLET_LENGTH_TOLERANCE)
        {
            col->percentInterp = .5;
        }
        else
        {
            col->percentInterp = static_cast<double>(l0c / (l0c + l1c));
        }
    }

    return 1;
}


/// \brief isOnlyKinematic 检查一对碰撞是不是动力学vs动力学物体
inline bool isOnlyKinematic(const CollisionObject* cow0, const CollisionObject* cow1)
{
    return cow0->collisionFilterGroup == btBroadphaseProxy::KinematicFilter &&
            cow1->collisionFilterGroup == btBroadphaseProxy::KinematicFilter;
}

/// \brief 允许碰撞检测为true
inline bool acmCheck(const std::string& body1, const std::string& body2, const AllowedCollisionMatrix* acm)
{
    AllowedCollisionType allowedType;

    if (acm != nullptr)
    {
        if (acm->getEntry(body1, body2, allowedType))
        {
            if (allowedType == AllowedCollisionType::ALLOW_COLLISION_NEVER)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}


/// @brief Callback 离散和连续宽相位碰撞对的结构
/// needsCollision 是在执行narrowphase检查之前执行的回调。
/// addSingleResult 是在narrowphase检查传递结果后执行的回调。
struct BroadphaseContactResultCallback
{
    ContactTestData &_collisions;
    double _contactDistance;
    const AllowedCollisionMatrix* _acm{ nullptr };

    /// \brief 指示回调是否仅用于自碰撞检查
    bool _self;

    /// \brief 指示回调是否用于casted collisions
    bool _cast{ false };

    BroadphaseContactResultCallback(ContactTestData& collisions, double contact_distance, const AllowedCollisionMatrix* acm, bool self, bool cast = false)
        : _collisions(collisions), _contactDistance(contact_distance), _acm(acm), _self(self), _cast(cast)
    {
    }

    ~BroadphaseContactResultCallback() = default;

    /// \brief 回调用于broadphase接口的对缓存中的每个重叠对,是否一个碰撞检查已经完成
    // TODO: Add check for two objects attached to the same link
    bool needsCollision(const CollisionObject* cow0, const CollisionObject* cow1) const
    {
        if (_cast)
        {
            return !_collisions.done && !isOnlyKinematic(cow0, cow1) && !acmCheck(cow0->getName(), cow1->getName(), _acm);
        }
        else
        {
            return !_collisions.done && (_self ? isOnlyKinematic(cow0, cow1) : !isOnlyKinematic(cow0, cow1)) &&!acmCheck(cow0->getName(), cow1->getName(), _acm);
        }
    }

    /// \brief 此回调在btManifoldResult处理冲突结果后使用。
    btScalar addSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0,
                             const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1)
    {
        (void)partId0;
        (void)partId1;
        if (cp.m_distance1 > static_cast<btScalar>(_contactDistance))
        {
            return 0;
        }

        //?需要转Moveit?
        if (_cast)
        {
            return addCastSingleResult(cp, colObj0Wrap, index0, colObj1Wrap, index1, _collisions);
        }
        else
        {
            return addDiscreteSingleResult(cp, colObj0Wrap, colObj1Wrap, _collisions);
        }
    }
};

struct TesseractBroadphaseBridgedManifoldResult : public btManifoldResult
{
    BroadphaseContactResultCallback& _resultCallback;

    TesseractBroadphaseBridgedManifoldResult(const btCollisionObjectWrapper* obj0Wrap,
                                             const btCollisionObjectWrapper* obj1Wrap,
                                             BroadphaseContactResultCallback& result_callback)
        : btManifoldResult(obj0Wrap, obj1Wrap), _resultCallback(result_callback)
    {
    }

    void addContactPoint(const btVector3& normalOnBInWorld, const btVector3& pointInWorld, btScalar depth) override
    {
        if (_resultCallback._collisions.done || _resultCallback._collisions.pairDone ||
                depth > static_cast<btScalar>(_resultCallback._contactDistance))
        {
            return;
        }

        bool is_swapped = m_manifoldPtr->getBody0() != m_body0Wrap->getCollisionObject();
        btVector3 point_a = pointInWorld + normalOnBInWorld * depth;
        btVector3 local_a;
        btVector3 local_b;
        if (is_swapped)
        {
            local_a = m_body1Wrap->getCollisionObject()->getWorldTransform().invXform(point_a);
            local_b = m_body0Wrap->getCollisionObject()->getWorldTransform().invXform(pointInWorld);
        }
        else
        {
            local_a = m_body0Wrap->getCollisionObject()->getWorldTransform().invXform(point_a);
            local_b = m_body1Wrap->getCollisionObject()->getWorldTransform().invXform(pointInWorld);
        }

        btManifoldPoint new_pt(local_a, local_b, normalOnBInWorld, depth);
        new_pt.m_positionWorldOnA = point_a;
        new_pt.m_positionWorldOnB = pointInWorld;

        // BP mod, store contact triangles.
        if (is_swapped)
        {
            new_pt.m_partId0 = m_partId1;
            new_pt.m_partId1 = m_partId0;
            new_pt.m_index0 = m_index1;
            new_pt.m_index1 = m_index0;
        }
        else
        {
            new_pt.m_partId0 = m_partId0;
            new_pt.m_partId1 = m_partId1;
            new_pt.m_index0 = m_index0;
            new_pt.m_index1 = m_index1;
        }

        //?需要转Moveit?

        // experimental feature info, for per-triangle material etc.
        const btCollisionObjectWrapper* obj0_wrap = is_swapped ? m_body1Wrap : m_body0Wrap;
        const btCollisionObjectWrapper* obj1_wrap = is_swapped ? m_body0Wrap : m_body1Wrap;
        _resultCallback.addSingleResult(new_pt, obj0_wrap, new_pt.m_partId0, new_pt.m_index0, obj1_wrap, new_pt.m_partId1,new_pt.m_index1);
    }
};

class TesseractCollisionPairCallback : public btOverlapCallback
{
    const btDispatcherInfo& _dispatchInfo;
    btCollisionDispatcher* _dispatcher;

    /// \brief 检查每个broadphase对是否需要进行碰撞检测
    BroadphaseContactResultCallback& _resultsCallback;

public:
    TesseractCollisionPairCallback(const btDispatcherInfo& dispatchInfo, btCollisionDispatcher* dispatcher,
                                   BroadphaseContactResultCallback& results_callback)
        : _dispatchInfo(dispatchInfo), _dispatcher(dispatcher), _resultsCallback(results_callback)
    {
    }

    ~TesseractCollisionPairCallback() override = default;

    bool processOverlap(btBroadphasePair& pair) override
    {
        _resultsCallback._collisions.pairDone = false;

        if (_resultsCallback._collisions.done)
        {
            return false;
        }

        const CollisionObject* collisionObj0 = static_cast<const CollisionObject*>(pair.m_pProxy0->m_clientObject);
        const CollisionObject* collisionObj1 = static_cast<const CollisionObject*>(pair.m_pProxy1->m_clientObject);

        std::pair<std::string, std::string> pair_names{ collisionObj0->getName(), collisionObj1->getName() };
        if (_resultsCallback.needsCollision(collisionObj0, collisionObj1))
        {
            btCollisionObjectWrapper obj0_wrap(nullptr, collisionObj0->getCollisionShape(), collisionObj0, collisionObj0->getWorldTransform(), -1, -1);
            btCollisionObjectWrapper obj1_wrap(nullptr, collisionObj1->getCollisionShape(), collisionObj1, collisionObj1->getWorldTransform(), -1, -1);

            // dispatcher will keep algorithms persistent in the collision pair
            if (!pair.m_algorithm)
            {
                pair.m_algorithm = _dispatcher->findAlgorithm(&obj0_wrap, &obj1_wrap, nullptr, BT_CLOSEST_POINT_ALGORITHMS);
            }

            if (pair.m_algorithm)
            {
                TesseractBroadphaseBridgedManifoldResult contact_point_result(&obj0_wrap, &obj1_wrap, _resultsCallback);
                contact_point_result.m_closestPointDistanceThreshold = static_cast<btScalar>(_resultsCallback._contactDistance);

                // discrete collision detection query
                pair.m_algorithm->processCollision(&obj0_wrap, &obj1_wrap, _dispatchInfo, &contact_point_result);
            }
        }
        else
        {
        }
        return false;
    }
};


/// \brief isLinkActive 这将检查链接是否处于活动状态，并提供一个列表。如果列表为空，则认为链接处于活动状态。
/// \param active
/// \param name
/// \return
inline bool isLinkActive(const std::vector<std::string>& active, const std::string& name)
{
    return active.empty() || (std::find(active.begin(), active.end(), name) != active.end());
}

/// \brief updateCollisionObjectFilters 更新碰撞对象过滤器
/// \param active 活动碰撞对象的列表
/// \param collisionObj 要更新的碰撞对象。
/// 当前，只能针对静态对象检查连续碰撞对象。当前不支持连续到连续的冲突检查。
inline void updateCollisionObjectFilters(const std::vector<std::string>& active, CollisionObject& collisionObj)
{
    // if not active make cow part of static
    if (!isLinkActive(active, collisionObj.getName()))
    {
        collisionObj.collisionFilterGroup = btBroadphaseProxy::StaticFilter;
        collisionObj.collisionFilterMask = btBroadphaseProxy::KinematicFilter;
    }
    else
    {
        collisionObj.collisionFilterGroup = btBroadphaseProxy::KinematicFilter;
        collisionObj.collisionFilterMask = btBroadphaseProxy::KinematicFilter | btBroadphaseProxy::StaticFilter;
    }

    if (collisionObj.getBroadphaseHandle())
    {
        collisionObj.getBroadphaseHandle()->m_collisionFilterGroup = collisionObj.collisionFilterGroup;
        collisionObj.getBroadphaseHandle()->m_collisionFilterMask = collisionObj.collisionFilterMask;
    }
}

inline CollisionObjectPtr makeCastCollisionObject(const CollisionObjectPtr& collisionObj)
{
    CollisionObjectPtr newCollisionObj = collisionObj->clone();

    btTransform tf;
    tf.setIdentity();

    if (btBroadphaseProxy::isConvex(newCollisionObj->getCollisionShape()->getShapeType()))
    {
        assert(dynamic_cast<btConvexShape*>(newCollisionObj->getCollisionShape()) != nullptr);
        btConvexShape* convex = static_cast<btConvexShape*>(newCollisionObj->getCollisionShape());

        // This checks if the collision object is already a cast collision object
        assert(convex->getShapeType() != CUSTOM_CONVEX_SHAPE_TYPE);

        CastHullShape* shape = new CastHullShape(convex, tf);

        newCollisionObj->manage(shape);
        newCollisionObj->setCollisionShape(shape);
    }
    else if (btBroadphaseProxy::isCompound(newCollisionObj->getCollisionShape()->getShapeType()))
    {
        btCompoundShape* compound       = static_cast<btCompoundShape*>(newCollisionObj->getCollisionShape());
        btCompoundShape* newCompound    = new btCompoundShape(BULLET_COMPOUND_USE_DYNAMIC_AABB, compound->getNumChildShapes());

        for (int i = 0; i < compound->getNumChildShapes(); ++i)
        {
            if (btBroadphaseProxy::isConvex(compound->getChildShape(i)->getShapeType()))
            {
                btConvexShape* convex = static_cast<btConvexShape*>(compound->getChildShape(i));
                assert(convex->getShapeType() != CUSTOM_CONVEX_SHAPE_TYPE);  // This checks if already a cast collision object

                btTransform geomTrans = compound->getChildTransform(i);

                btCollisionShape* subshape = new CastHullShape(convex, tf);

                newCollisionObj->manage(subshape);
                subshape->setMargin(BULLET_MARGIN);
                newCompound->addChildShape(geomTrans, subshape);
            }
            else if (btBroadphaseProxy::isCompound(compound->getChildShape(i)->getShapeType()))
            {
                btCompoundShape* secondCompound = static_cast<btCompoundShape*>(compound->getChildShape(i));
                btCompoundShape* newSecondCompound = new btCompoundShape(BULLET_COMPOUND_USE_DYNAMIC_AABB, secondCompound->getNumChildShapes());

                for (int j = 0; j < secondCompound->getNumChildShapes(); ++j)
                {
                    assert(!btBroadphaseProxy::isCompound(secondCompound->getChildShape(j)->getShapeType()));

                    btConvexShape* convex = static_cast<btConvexShape*>(secondCompound->getChildShape(j));
                    assert(convex->getShapeType() != CUSTOM_CONVEX_SHAPE_TYPE);  // This checks if already a cast collision object

                    btTransform geom_trans = secondCompound->getChildTransform(j);

                    btCollisionShape* subshape = new CastHullShape(convex, tf);

                    newCollisionObj->manage(subshape);
                    subshape->setMargin(BULLET_MARGIN);
                    newSecondCompound->addChildShape(geom_trans, subshape);
                }

                btTransform geom_trans = compound->getChildTransform(i);

                newCollisionObj->manage(newSecondCompound);

                // margin on compound seems to have no effect when positive but has an effect when negative
                newSecondCompound->setMargin(BULLET_MARGIN);
                newCompound->addChildShape(geom_trans, newSecondCompound);
            }
            else
            {
                throw std::runtime_error("I can only collision check convex shapes and compound shapes made of convex shapes");
            }
        }

        // margin on compound seems to have no effect when positive but has an effect when negative
        newCompound->setMargin(BULLET_MARGIN);
        newCollisionObj->manage(newCompound);
        newCollisionObj->setCollisionShape(newCompound);
        newCollisionObj->setWorldTransform(collisionObj->getWorldTransform());
    }
    else
    {
        throw std::runtime_error("I can only collision check convex shapes and compound shapes made of convex shapes");
    }

    return newCollisionObj;
}

/// \brief 更新输入碰撞对象的Broadphase AABB
/// \param collisionObj 碰撞物体
/// \param broadphase bullet broadphase接口
/// \param dispatcher bullet dispatcher接口
inline void updateBroadphaseAABB(const CollisionObjectPtr& collisionObj,
                                 const std::unique_ptr<btBroadphaseInterface>& broadphase,
                                 const std::unique_ptr<btCollisionDispatcher>& dispatcher)
{
    btVector3 aabbMin, aabbMax;
    collisionObj->getAABB(aabbMin, aabbMax);

    assert(collisionObj->getBroadphaseHandle() != nullptr);
    broadphase->setAabb(collisionObj->getBroadphaseHandle(), aabbMin, aabbMax, dispatcher.get());
}

/// \brief removeCollisionObjectFromBroadphase 从broadphase中删除碰撞对象
/// \param collisionObj 碰撞物体
/// \param broadphase bullet broadphase接口
/// \param dispatcher bullet dispatcher接口
inline void removeCollisionObjectFromBroadphase(const CollisionObjectPtr& collisionObj,
                                                const std::unique_ptr<btBroadphaseInterface>& broadphase,
                                                const std::unique_ptr<btCollisionDispatcher>& dispatcher)
{
    btBroadphaseProxy* bp = collisionObj->getBroadphaseHandle();
    if (bp)
    {
        // only clear the cached algorithms
        broadphase->getOverlappingPairCache()->cleanProxyFromPairs(bp, dispatcher.get());
        broadphase->destroyProxy(bp, dispatcher.get());
        collisionObj->setBroadphaseHandle(nullptr);
    }
}

/// \brief addCollisionObjectToBroadphase  将碰撞对象添加到broadphase
/// \param collisionObj 碰撞物体
/// \param broadphase bullet broadphase接口
/// \param dispatcher bullet dispatcher接口
inline void addCollisionObjectToBroadphase(const CollisionObjectPtr& collisionObj,
                                           const std::unique_ptr<btBroadphaseInterface>& broadphase,
                                           const std::unique_ptr<btCollisionDispatcher>& dispatcher)
{
    btVector3 aabb_min, aabb_max;
    collisionObj->getAABB(aabb_min, aabb_max);

    int type = collisionObj->getCollisionShape()->getShapeType();
    collisionObj->setBroadphaseHandle(broadphase->createProxy(aabb_min, aabb_max, type, collisionObj.get(), collisionObj->collisionFilterGroup,
                                                              collisionObj->collisionFilterMask, dispatcher.get()));
}

struct BroadPhaseFilterCallback : public btOverlapFilterCallback
{
    /// \brief 测试一对是否需要碰撞检测
    bool needBroadphaseCollision(btBroadphaseProxy* proxy0, btBroadphaseProxy* proxy1) const override
    {
        bool cull = !(proxy0->m_collisionFilterMask & proxy1->m_collisionFilterGroup);
        cull = cull || !(proxy1->m_collisionFilterMask & proxy0->m_collisionFilterGroup);

        if(cull)
            return false;

        const CollisionObject* collisionObj0 = static_cast<const CollisionObject*>(proxy0->m_clientObject);
        const CollisionObject* collisionObj1 = static_cast<const CollisionObject*>(proxy0->m_clientObject);

        if(!collisionObj0->enabled)
            return false;

        if(!collisionObj1->enabled)
            return false;

        if(collisionObj0->getBodyType() == BodyType::BODY_ROBOT_ATTACHED &&
                collisionObj1->getBodyType() == BodyType::BODY_ROBOT_LINK)
        {
            if(collisionObj0->touchLinks.find(collisionObj1->getName()) != collisionObj0->touchLinks.end())
            {
                return false;
            }
        }

        if(collisionObj1->getBodyType() == BodyType::BODY_ROBOT_ATTACHED &&
                collisionObj0->getBodyType() == BodyType::BODY_ROBOT_LINK)
        {
            if(collisionObj1->touchLinks.find(collisionObj0->getName()) != collisionObj1->touchLinks.end())
            {
                return false;
            }
        }

        if(collisionObj0->getBodyType() == BodyType::BODY_ROBOT_ATTACHED &&
                collisionObj1->getBodyType() == BodyType::BODY_ROBOT_ATTACHED)
        {
            if(collisionObj0->touchLinks == collisionObj1->touchLinks)
            {
                return false;
            }
        }

        return true;
    }
};

#endif // COLLISIONOBJECT_H
