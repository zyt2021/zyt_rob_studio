﻿#ifndef PHYSICENGINE_H
#define PHYSICENGINE_H

#include <QObject>
#include <QMutex>
#include <QSharedPointer>
#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/btBulletCollisionCommon.h>
#include <QTimer>
#include <Core/MsnhModel.h>

class PhysicalEngine : public QObject
{
    Q_OBJECT
public:
    static QSharedPointer<PhysicalEngine>& getInstance();

    void addRigidBody(Model* model);

    void startEngine();

    void stopEngine();

    void setDebugDrawer(btIDebugDraw* debugDrawer);

    ~PhysicalEngine();

    btDynamicsWorld *getWorld() const;

private:
    PhysicalEngine();
    PhysicalEngine(const PhysicalEngine&) = delete;

    void removeModel(Model *model);
private slots:
    void runSimulation();

private:
    static QMutex _mutex;
    static QSharedPointer<PhysicalEngine> _instance; //自动析构

    /// \brief broadphase碰撞器
    btBroadphaseInterface*  _broadPhase     = nullptr;
    /// \brief 碰撞配置器
    btCollisionConfiguration* _collisionCfg = nullptr;
    /// \brief 事件调度器
    btCollisionDispatcher* _dispatcher      = nullptr;
    /// \brief 约束求解器
    btConstraintSolver* _solver             = nullptr;
    /// \brief 仿真世界
    btDynamicsWorld* _world                 = nullptr;


    QTimer* _timer                           = nullptr;

    QVector<Model*> _models;
    QVector<btRigidBody*> _rigidBodies;
    btClock btclock;
};

#endif // PHYSICENGINE_H
