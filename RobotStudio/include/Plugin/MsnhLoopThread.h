﻿#ifndef MSNHLOOPTHREAD_H
#define MSNHLOOPTHREAD_H

#include <QThread>
#include <MsnhProcess.h>

class MsnhLoopThread : public QThread
{
    Q_OBJECT
public:
    explicit MsnhLoopThread(MsnhProcess* process = nullptr, bool needSleep = false, unsigned long sleepMS = 10, QObject *parent = nullptr);
    ~MsnhLoopThread();
    void startIt();
    void stopIt();
    void run();

    void setProcess(MsnhProcess* process);
    MsnhProcess *getProcess() const;

    bool getNeedSleep() const;
    void setNeedSleep(bool newNeedSleep);

    unsigned long getSleepMS() const;
    void setSleepMS(unsigned long newSleepMS);

signals:
    void revData(MsnhProtocal::Base *data);

private:
    MsnhProcess* _process;
    MsnhProtocal::Base* _revData;
    bool _running;
    bool _needSleep;
    unsigned long _sleepMS;
};

#endif // MSNHLOOPTHREAD_H
