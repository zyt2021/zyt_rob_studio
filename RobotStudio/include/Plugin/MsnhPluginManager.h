#ifndef MSNH_PLUGINMANAGER_H
#define MSNH_PLUGINMANAGER_H

#include <pugg/Kernel.h>
#include <Plugin/MsnhProcessFactory.h>
#include <QString>

class MsnhPluginManager
{
    friend class MsnhSingleton<MsnhPluginManager>;
public:
    MsnhPluginManager();
    ~MsnhPluginManager();

    void loadPlugins(const QString& pluginPath, MsnhProcessFactory* factory);
    void unloadPlugins();
    bool removeDir(const QString &dirName);

    QList<QString>*                 loadedPlugins() { return &_loadedPlugins; }
    std::vector<MsnhProcessDriver*>* loadedDrivers() { return &_drivers; }

private:
    QList<QString>                 _loadedPlugins;
    QString                        _pluginPath;
    QString                        _pluginTmpPath;
    MsnhProcessFactory*            _factory;
    QList<pugg::Kernel*>           _kernels;
    std::vector<MsnhProcessDriver*> _drivers;
};

#endif // MSNH_PLUGINMANAGER_H
