﻿#ifndef MSNH_PROCESSFACTOR_H
#define MSNH_PROCESSFACTOR_H

#include <Util/MsnhSingleton.h>
#include <pugg/Kernel.h>
#include <MsnhProcess.h>
#include <QString>
#include <QMap>
#include <QVector>

class MsnhProcessFactory
{
    friend class MsnhSingleton<MsnhProcessFactory>;
public:
    MsnhProcessFactory();
    ~MsnhProcessFactory();

    void registerProcess(const QString& name, MsnhProcess* process);
    void unregisterProcess(const QString& name);
    void unregisterAllProcess();
    MsnhProcess* getProcInstance(const QString& name);

    QVector<QString> get2dCamIO() const;

private:
    QMap<QString, MsnhProcess*> _processMap;
    QVector<QString> _2dCamIO; //TODO: 添加各种类型
};

#endif // MSNH_PROCESSFACTOR_H
