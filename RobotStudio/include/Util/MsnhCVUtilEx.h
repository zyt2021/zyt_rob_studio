﻿#ifndef MSNHCVUTILEX_H
#define MSNHCVUTILEX_H

#include <QImage>
namespace cv {
class Mat;
}

class MsnhCVUtil
{
public:

    enum Sequence{
        XYZ=0,
        ZYX=1,
        ZYZ=2
    };
    MsnhCVUtil();
    static cv::Mat QImageToMat(const QImage &image);
    static QImage MatToQImage(const cv::Mat& mat);
    static bool isRotationMatrix(cv::Mat &R);
    static void cvDelay(int mSec);
    static cv::Mat rot2euler(cv::Mat & R, Sequence seq);
    static cv::Mat rot2quat(cv::Mat & R);
    static cv::Mat quat2rot(cv::Mat & quat);
    static cv::Mat euler2rot(cv::Mat & euler, Sequence seq);
    static cv::Mat euler2quat(cv::Mat & euler, Sequence seq);
    static cv::Mat quat2euler(cv::Mat & quat, Sequence seq);
};

#endif // MSNHCVUTILEX_H
