﻿#ifndef MSNHDEBUGHANDLER_H
#define MSNHDEBUGHANDLER_H
#include <Gui/MsnhMainWindow.h>
#include <QDateTime>

const QString htmlOKHead      =  ("<p style='color: rgb(0, 220, 0)'>");
const QString htmlErrorHead   =  ("<p style='color: rgb(255, 50, 0)'>");
const QString htmlWarningHead =  ("<p style='color: rgb(255, 191, 36)'>");
const QString htmlEnd         =  ("</p>");

void logger(QString msg, int type)
{
    QDateTime current_date_time =QDateTime::currentDateTime();
    QString current_date =current_date_time.toString("yyyy/MM/dd  hh:mm:ss");

    switch (type) {
    case 2:
    {
        if(MsnhMainWindow::logTxt)
            MsnhMainWindow::logTxt->appendHtml(htmlErrorHead + current_date +"[ERR] :" + msg + htmlEnd);
        break;
    }
    case 1:
    {
        if(MsnhMainWindow::logTxt)
            MsnhMainWindow::logTxt->appendHtml(htmlWarningHead + current_date +"[WAR] :"  + msg + htmlEnd);
        break;
    }
    case 0:
    {
        if(MsnhMainWindow::logTxt)
            MsnhMainWindow::logTxt->appendHtml(htmlOKHead + current_date +"[INF] :"  + msg + htmlEnd);
        break;
    }
    }
}

void messageHandler(QtMsgType type, const QMessageLogContext& context, const QString &msg)
{
    (void)context;
    switch (type)
    {
    case QtDebugMsg:    logger(msg,0); break;
    case QtInfoMsg:     logger(msg,0); break;
    case QtWarningMsg:  logger(msg,1); break;
    case QtCriticalMsg: logger(msg,2); break;
    case QtFatalMsg:    logger(msg,2); break;
    }
}



#endif
