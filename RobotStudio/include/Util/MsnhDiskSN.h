﻿#ifndef GETDISKSN_H
#define GETDISKSN_H
#include <QProcess>
#include <QException>
#include <string>
#include <QRegExp>
#include <exception>

class DiskSNException:public std::exception
{
public:

    DiskSNException (std::string str)
    {
        err=str;
    }

    DiskSNException()
    {
        err="error";
    }


    virtual const char* what () const noexcept
    {
        return err.data();
    }

private:
    std::string err;
};

class DiskSN
{
public:
    DiskSN();
    static QString getSN();
};

#endif // GETDISKSN_H
