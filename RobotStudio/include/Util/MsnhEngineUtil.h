﻿#ifndef ROBOTUTIL_H
#define ROBOTUTIL_H
#include <QColor>
#include <QApplication>

enum AxisMode
{
    AXIS_Y_UP,
    AXIS_Z_UP
};

enum PlotType
{
    PLOT_SINGLE,
    PLOT_CARTESIAN
};

class EngineUtil
{
public:
    static AxisMode axisMode;
    static QColor   highlightColor;
    static QColor   selectColor;
    static float    pointSize;
    static QApplication* app;
    static void setYAxisUp();
    static void setZAxisUp();
    static bool enableGama;
    static bool enableAnti;
    static bool physicalEngineRunning;
    static void setOpenGLDefault();
};

class RobotUtil
{
public:
    static uint8_t  chainNums;
    static QColor   dragColor;
    static QColor   animationColor;

    static void setRobotDefault();
};

#endif // ROBOTUTIL_H
