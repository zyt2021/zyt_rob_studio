﻿#ifndef MSNHNNTHREAD_H
#define MSNHNNTHREAD_H
#include <nanomsg/nn.hpp>
#include <nanomsg/pipeline.h>
#include <nanomsg/pubsub.h>
#include <QThread>
#include <QMainWindow>
#include <iostream>
#include <QDebug>
#include <QMetaType>
#include <nanomsg/pair.h>

class NNThread:public QThread
{
    Q_OBJECT
public:
    explicit NNThread(QObject *parent=0);
    ~NNThread();

    void startIt();

    void stopIt();

    void send(const std::vector<uint8_t>& data);

    void send(void *data, size_t len);

    void setAddr(const QString& addr);

    const QString &getAddr() const;

signals:
    void revMsg(const std::vector<uint8_t>& data);

protected:
    void run();

private:
    nn::socket *_so;
    bool _running;
    QString _addr;
    int _eid;
};

#endif // MSNHNNTHREAD_H
