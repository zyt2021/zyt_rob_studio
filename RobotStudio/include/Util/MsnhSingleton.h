#ifndef MSNHSINGLETON_H
#define MSNHSINGLETON_H

#include <QMutex>

template <class T>
class MsnhSingleton
{
public:
    // Constructors
    MsnhSingleton(void) = delete;
    MsnhSingleton(MsnhSingleton const &) = delete;
    MsnhSingleton(MsnhSingleton &&) = delete;
    // Assignment Operators
    MsnhSingleton &operator=(MsnhSingleton const &) = delete;
    MsnhSingleton &operator=(MsnhSingleton &&) = delete;
    // Destructor
    virtual ~MsnhSingleton(void) = default;

    static QMutex mutex;
    static T* inst;

    template<typename... Args>
    static T* getInstance(Args &&...args)
    {
        if (!inst)
        {
            mutex.lock();
            if (!inst)
            {
                inst = new T(std::forward<Args>(args)...);
            }
            mutex.unlock();
        }
        return inst;
    }

    static void dispose()
    {
        delete inst;
    }
};


template<class T>
QMutex MsnhSingleton<T>::mutex;

template<class T>
T* MsnhSingleton<T>::inst = nullptr;
#endif // MSNHSINGLETON_H
