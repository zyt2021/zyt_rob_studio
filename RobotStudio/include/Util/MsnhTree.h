﻿#ifndef MSNHTREE_H
#define MSNHTREE_H

#include <string>
#include <memory>
#include <vector>
#include <algorithm>

struct StringTree
{
    std::string name;
    std::vector<std::shared_ptr<StringTree>> children;

    StringTree(const std::string& name):name(name){}
    StringTree(){}

    void addChild(const std::shared_ptr<StringTree>& tree)
    {
        children.push_back(tree);
    }

    static bool deepFirstSearch(const std::shared_ptr<StringTree>& root, const std::string& dest, std::vector<std::string> &result)
    {
        result.push_back(root->name);

        if(root->name == dest)
        {
            return true;
        }

        if(root->children.empty())
        {
            return false;
        }

        for(size_t i = 0; i < root->children.size(); i++)
        {
            std::vector<std::string> pt = result;
            bool res = deepFirstSearch(root->children[i], dest, pt);

            if(!res)
            {
                continue;
            }
            else
            {
                result = pt;
                return res;
            }
        }

        return false;
    }

    static bool getShortestPath(const std::shared_ptr<StringTree>& root, const std::string& start, const std::string& end, std::vector<std::string>& result)
    {
        std::vector<std::string> way1;
        std::vector<std::string> way2;

        bool search1 = deepFirstSearch(root,start,way1);
        bool search2 = deepFirstSearch(root,end,  way2);

        if(!search1||!search2)
        {
            return false;
        }

        for (int i = 0; i < way1.size(); ++i)
        {
            for (int j = 0; j < way2.size(); ++j)
            {
                size_t ii = way1.size() - 1 - i;
                size_t jj = way2.size() - 1 - j;

                if(way1[ii]==way2[jj])
                {
                    std::vector<std::string> res;
                    for (size_t m = ii; m < way1.size(); ++m)
                    {
                        res.push_back(way1[m]);
                    }

                    std::reverse(res.begin(), res.end());

                    for (size_t n = jj+1; n < way2.size(); ++n)
                    {
                        res.push_back(way2[n]);
                    }

                    result = res;
                    return true;
                }
            }
        }
        return false;
    }

};
#endif // MSNHTREE_H
