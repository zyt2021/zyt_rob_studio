﻿#include <Core/MsnhAbstractEntity.h>

AbstractEntity	* AbstractEntity::_highlightedObject	= nullptr;
AbstractEntity	* AbstractEntity::_selectedObject       = nullptr;
AbstractEntity  * AbstractEntity::_collisionSelectedObject = nullptr;

AbstractEntity::AbstractEntity( QObject * parent ) : QObject( parent )
{
    _visible        = true;
    _highlighted	= false;
    _selected       = false;
    _wireFrameMode  = false;
    _position       = QVector3D( 0, 0, 0 );
    _rotation       = QVector3D( 0, 0, 0 );
    _scaling        = QVector3D( 1, 1, 1 );
    setObjectName( "Untitled Entity" );
}


AbstractEntity::AbstractEntity(const AbstractEntity & another , QObject *parent) : QObject( parent )
{
    _visible        = true;
    _highlighted	= false;
    _selected       = false;
    _wireFrameMode  = another._wireFrameMode;
    _position       = another._position;
    _rotation       = another._rotation;
    _scaling        = another._scaling;
    setObjectName( another.objectName() );
}


AbstractEntity::~AbstractEntity()
{
    setHighlighted( false );
    setSelected( false );
}


void AbstractEntity::translate( QVector3D delta )
{
    setPosition( _position + delta );
}


void AbstractEntity::rotate( QQuaternion rotation )
{
    setRotation( QQuaternion::fromEulerAngles( _rotation ) * rotation );
}


void AbstractEntity::rotate( QVector3D rotation )
{
    setRotation( QQuaternion::fromEulerAngles( _rotation ) * QQuaternion::fromEulerAngles( rotation ) );
}


void AbstractEntity::scale( QVector3D scaling )
{
    setScaling( _scaling * scaling );
}

void AbstractEntity::translateApi(QVector3D delta)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        translate(delta);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        translate(QVector3D(delta.y(),delta.z(),delta.x()));
    }
}

void AbstractEntity::rotateApi(QVector3D rotation)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        rotate(rotation);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        qDebug()<<rotation;
        rotate(QVector3D(rotation.y(),rotation.z(),rotation.x()));
    }
}

void AbstractEntity::scaleApi(QVector3D scaling)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        scale(scaling);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        scale(QVector3D(scaling.y(),scaling.z(),scaling.x()));
    }
}



//============= 父项影响子项 ============================ //
// Model
//   |_(add)subModel
//   |          |_(add)subMesh
//   |_(add)subMesh
//如果model的visibale和自己的都为true,则可见
//如果model的highlight或自己的为true,则高亮
bool AbstractEntity::getVisible() const
{
    if ( AbstractEntity * par = qobject_cast<AbstractEntity*>( parent() ) )
        return(par->getVisible() && _visible);
    else
        return(_visible);
}


bool AbstractEntity::getHighlighted() const
{
    if ( AbstractEntity * par = qobject_cast<AbstractEntity*>( parent() ) )
        return(par->getHighlighted() || _highlighted);
    else
        return(_highlighted);
}


bool AbstractEntity::getSelected() const
{
    if ( AbstractEntity * par = qobject_cast<AbstractEntity*>( parent() ) )
        return(par->getSelected() || _selected);
    else
        return(_selected);
}


bool AbstractEntity::getWireFrameMode() const
{
    if ( AbstractEntity * par = qobject_cast<AbstractEntity*>( parent() ) )
        return(par->getWireFrameMode() || _wireFrameMode);
    else
        return(_wireFrameMode);
}

//如果model的draggable和自己的都为true,则可拽
bool AbstractEntity::getDraggable() const
{
    if ( AbstractEntity * par = qobject_cast<AbstractEntity*>( parent() ) )
        return(par->getDraggable() && _draggable);
    else
        return(_draggable);
}

float AbstractEntity::getAlpha() const
{
    return _alpha;
}

bool AbstractEntity::getDeleteable() const
{
    return _deleteable;
}

bool AbstractEntity::getSelectable() const
{
    if ( AbstractEntity * par = qobject_cast<AbstractEntity*>( parent() ) )
        return(par->getSelectable() && _selectable);
    else
        return(_selectable);
}


QVector3D AbstractEntity::getPosition() const
{
    return(_position);
}


QVector3D AbstractEntity::getRotation() const
{
    return(_rotation);
}


QVector3D AbstractEntity::getScaling() const
{
    return(_scaling);
}

QVector3D AbstractEntity::getPositionApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getPosition();
    }
    else
    {
        auto position = getPosition();
        return QVector3D(position.z(),position.x(),position.y());
    }
}

QVector3D AbstractEntity::getRotationApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getRotation();
    }
    else
    {
        auto rotation = getRotation();
        return QVector3D(rotation.z(),rotation.x(),rotation.y());
    }
}

QVector3D AbstractEntity::getScalingApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getScaling();
    }
    else
    {
        auto scaling = getScaling();
        return QVector3D(scaling.z(),scaling.x(),scaling.y());
    }
}

QVector3D AbstractEntity::getGlobalPosition() const
{
    QMatrix4x4 mat  = getGlobalModelMatrix();
    QVector3D pos   = QVector3D(mat(0,3),mat(1,3),mat(2,3));
    return pos;
}

QVector3D AbstractEntity::getGlobalRotation() const
{
    QMatrix4x4 mat  = getGlobalModelMatrix();
    QMatrix3x3 rotMat;

    rotMat(0,0) = mat(0,0);
    rotMat(0,1) = mat(0,1);
    rotMat(0,2) = mat(0,2);

    rotMat(1,0) = mat(1,0);
    rotMat(1,1) = mat(1,1);
    rotMat(1,2) = mat(1,2);

    rotMat(2,0) = mat(2,0);
    rotMat(2,1) = mat(2,1);
    rotMat(2,2) = mat(2,2);

    QVector3D rotation = QQuaternion::fromRotationMatrix(rotMat).toEulerAngles();
    return rotation;
}

QVector3D AbstractEntity::getGlobalPositionApi() const
{
    QVector3D pos = getGlobalPosition();
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return pos;
    }
    else
    {
        return QVector3D(pos.z(),pos.x(),pos.y());
    }
}

QVector3D AbstractEntity::getGlobalRotationApi() const
{

    QVector3D rotation = getGlobalRotation();
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return rotation;
    }
    else
    {
        return QVector3D(rotation.z(),rotation.x(),rotation.y());
    }
}

Msnhnet::Frame AbstractEntity::getGlobalFrameApi() const
{
    QVector3D pos = getGlobalPositionApi();
    QVector3D rot = getGlobalRotationApi();
    rot = rot * MSNH_DEG_2_RAD;

    Msnhnet::Frame frame(Msnhnet::GeometryS::euler2RotMat(Msnhnet::EulerDS(rot.x(),rot.y(),rot.z()), Msnhnet::ROT_ZYX),
    {pos.x(),pos.y(),pos.z()});

    return frame;
}



QMatrix4x4 AbstractEntity::getLocalModelMatrix() const
{
    QMatrix4x4 model;

    model.translate( getPosition() );
    model.rotate( QQuaternion::fromEulerAngles( getRotation() ) );
    model.scale( getScaling() );

    return(model);
}


QMatrix4x4 AbstractEntity::getGlobalModelMatrix() const
{
    if ( AbstractEntity * par = qobject_cast<AbstractEntity*>( parent() ) )
    {
        return(par->getGlobalModelMatrix() * getLocalModelMatrix() );
    }
    else
        return(getLocalModelMatrix() );
}


AbstractEntity * AbstractEntity::getHighlightedObject()
{
    return(_highlightedObject);
}


AbstractEntity * AbstractEntity::getSelectedObject()
{
    return(_selectedObject);
}


void AbstractEntity::setVisible( bool visible )
{
    if ( _visible != visible )
    {
        _visible = visible;
        if ( logLevel > LOG_LEVEL_INFO )
            dout << this->objectName() << "is" << (visible ? "visible" : "invisible");
        emit visibleChanged( _visible );
    }

}


void AbstractEntity::setHighlighted( bool highlighted )
{
    if ( _highlighted == highlighted )
        return;

    if ( highlighted )
    {
        if ( _highlightedObject && _highlightedObject != this )
            _highlightedObject->setHighlighted( false );
        _highlightedObject = this;
    } else if ( _highlightedObject == this )
        _highlightedObject = nullptr;

    if ( logLevel >= LOG_LEVEL_INFO && highlighted )
        dout << this->objectName() << "is highlighted";

    _highlighted = highlighted;
    emit highlightedChanged( _highlighted );
}


void AbstractEntity::setSelected( bool selected )
{
    if ( _selected == selected )
        return;

    if ( selected )
    {
        if ( _selectedObject && _selectedObject != this )
            _selectedObject->setSelected( false );
        _selectedObject = this;
    } else if ( _selectedObject == this )
        _selectedObject = nullptr;

    if ( logLevel >= LOG_LEVEL_INFO && selected )
        dout << this->objectName() << "is selected";

    _selected = selected;
    emit selectedChanged( _selected );
}

void AbstractEntity::setCollisionSelected(bool collisionSelected)
{
    if ( _collisionSelected == collisionSelected )
        return;

    if ( collisionSelected )
    {
        if ( _collisionSelectedObject && _collisionSelectedObject != this )
            _collisionSelectedObject->setCollisionSelected( false );
        _collisionSelectedObject = this;
    } else if ( _collisionSelectedObject == this )
        _collisionSelectedObject = nullptr;

    if ( logLevel >= LOG_LEVEL_INFO && collisionSelected )
        dout << this->objectName() << "is collsion selected";

    _collisionSelected = collisionSelected;
}


void AbstractEntity::setWireFrameMode( bool enabled )
{
    if ( _wireFrameMode != enabled )
    {
        _wireFrameMode = enabled;
        if ( logLevel > LOG_LEVEL_INFO )
            dout << "Wireframe mode of " << this->objectName() << "is" << (enabled ? "enabled" : "disabled");
        emit wireFrameModeChanged( _wireFrameMode );
    }
}

void AbstractEntity::setDraggable(bool draggable)
{
    if(draggable!=_draggable)
    {
        _draggable = draggable;
        if ( logLevel > LOG_LEVEL_INFO )
            dout << "Drag mode of " << this->objectName() << "is" << (draggable ? "enabled" : "disabled");
        emit draggableChanged(draggable);
    }
}

void AbstractEntity::setSelectable(bool selectable)
{
    if(_selectable != selectable)
    {
        _selectable = selectable;
        if ( logLevel > LOG_LEVEL_INFO )
            dout << "Selected mode of " << this->objectName() << "is" << (selectable ? "enabled" : "disabled");
        emit selectableChanged(selectable);
    }
}

void AbstractEntity::setDeleteable(bool deleteable)
{
    if(_deleteable!=deleteable)
    {
        _deleteable = deleteable;
        if ( logLevel > LOG_LEVEL_INFO )
            dout << "Delete mode of " << this->objectName() << "is" << (_deleteable ? "enabled" : "disabled");
        emit deleteableChanged(_deleteable);
    }
}

void AbstractEntity::setAlpha(float alpha)
{
    if(alpha >1 || alpha<0)
    {
        return;
    }

    if(!isEqual(_alpha, alpha))
    {
        _alpha = alpha;
        emit alphaChanged(_alpha);
    }
}


void AbstractEntity::setPosition( QVector3D position )
{
    if ( isnan( position ) )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to set position: NaN detected";
        return;
    }

    if ( !isEqual( _position, position ) )
    {
        _position = position;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The position of" << this->objectName() << "is set to" << position;
        emit positionChanged( _position );
    }
}


void AbstractEntity::setRotation( QQuaternion rotation )
{
    setRotation( rotation.toEulerAngles() );
}


void AbstractEntity::setRotation( QVector3D rotation )
{
    if ( isnan( rotation ) )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to set rotation: NaN detected";
        return;
    }

    if ( !isEqual( _rotation, rotation ) )
    {
        _rotation = rotation;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The rotation of" << this->objectName() << "is set to" << rotation;
        emit rotationChanged( _rotation );
    }
}


void AbstractEntity::setScaling( QVector3D scaling )
{
    if ( isnan( scaling ) )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to set scaling: NaN detected";
        return;
    }

    if ( !isEqual( _scaling, scaling ) )
    {
        _scaling = scaling;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The scaling of" << this->objectName() << "is set to" << scaling;
        emit scalingChanged( _scaling );
    }
}

void AbstractEntity::setPositionApi(QVector3D delta)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setPosition(delta);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        setPosition(QVector3D(delta.y(),delta.z(),delta.x()));
    }
}

void AbstractEntity::setRotationApi(QVector3D rotation)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setRotation(rotation);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        setRotation(QVector3D(rotation.y(),rotation.z(),rotation.x()));
    }
}

void AbstractEntity::setScalingApi(QVector3D scaling)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setScaling(scaling);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        setScaling(QVector3D(scaling.y(),scaling.z(),scaling.x()));
    }
}

bool AbstractEntity::getCollisionSelected() const
{
    if ( AbstractEntity * par = qobject_cast<AbstractEntity*>( parent() ) )
        return(par->getCollisionSelected() || _collisionSelected);
    else
        return(_collisionSelected);
}

AbstractEntity *AbstractEntity::getCollisionSelectedObject()
{
    return _collisionSelectedObject;
}




