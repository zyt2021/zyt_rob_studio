﻿#include <Core/MsnhAbstractGizmo.h>

AbstractGizmo::AbstractGizmo( QObject* parent ) : AbstractEntity( parent )
{
    _visible    = false;
    _axis		= GIZMO_AXIS_NONE;
    _host		= nullptr;
    setParent( parent );
    setObjectName( "Untitled Gizmo" );
}


AbstractGizmo::~AbstractGizmo()
{
}


bool AbstractGizmo::isGizmo() const
{
    return(true);
}


bool AbstractGizmo::isLight() const
{
    return(false);
}


bool AbstractGizmo::isMesh() const
{
    return(false);
}


bool AbstractGizmo::isModel() const
{
    return(false);
}


AbstractGizmo::TransformAxis AbstractGizmo::getAxis() const
{
    return(_axis);
}


QVector<Mesh*> & AbstractGizmo::getMarkers()
{
    return(_markers);
}


void AbstractGizmo::bindTo( AbstractEntity * host )
{
    if ( _host == host )
        return;
    if ( _host )
        unbind();
    if ( host )
    {
        _host = host;
        connect( _host, SIGNAL( destroyed( QObject* ) ), this, SLOT( hostDestroyed( QObject* ) ) );

        if ( logLevel >= LOG_LEVEL_INFO )
            dout << this->objectName() << "is bound to" << host->objectName();
    }
}


void AbstractGizmo::unbind()
{
    if ( _host )
    {
        disconnect( _host, nullptr, this, nullptr );
        _host = nullptr;
    }
}


void AbstractGizmo::setTransformAxis( void* marker )
{
    for ( int i = 0; i < _markers.size(); i++ )//通过marker的mesh顺序来指定 gizmo的功能
        if ( _markers[i] == marker )
            _axis = static_cast<AbstractGizmo::TransformAxis>(AbstractGizmo::GIZMO_AXIS_X + i);
}

AbstractEntity *AbstractGizmo::getHost() const
{
    return _host;
}


void AbstractGizmo::setTransformAxis( TransformAxis axis )
{
    _axis = axis;
}


void AbstractGizmo::hostDestroyed( QObject * )
{
    unbind();
}


