﻿#include <Core/MsnhAbstractLight.h>

AbstractLight::AbstractLight( QObject *parent ) : QObject( parent )
{
    _color	= QVector3D( 1.0f, 1.0f, 1.0f );
    _enabled	= true;
    _intensity	= 1.0f;
    setObjectName( "Untitled Light" );
}


AbstractLight::AbstractLight( QVector3D color, QObject *parent ) : QObject( parent )
{
    _color	= color;
    _enabled	= true;
    _intensity	= 1.0f;
    setObjectName( "Untitled Light" );
}


AbstractLight::AbstractLight(const AbstractLight & light , QObject *parent) : QObject( parent )
{
    _color		= light._color;
    _enabled	= light._enabled;
    _intensity	= light._intensity;
    setObjectName( light.objectName() );
}


AbstractLight::~AbstractLight()
{
}


QVector3D AbstractLight::getColor()
{
    return(_color);
}


bool AbstractLight::getEnabled()
{
    return(_enabled);
}


float AbstractLight::getIntensity()
{
    return(_intensity);
}


Mesh * AbstractLight::getMarker() const
{
    return(nullptr);
}


void AbstractLight::setColor( QVector3D color )
{
    if ( !isEqual( _color, color ) )
    {
        _color = color;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The color of" << this->objectName() << "is set to" << color;
        colorChanged( _color );
    }
}


void AbstractLight::setEnabled( bool enabled )
{
    if ( _enabled != enabled )
    {
        _enabled = enabled;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << this->objectName() << "is" << (enabled ? "enabled" : "disabled");
        enabledChanged( _enabled );
    }
}


void AbstractLight::setIntensity( float intensity )
{
    if ( !isEqual( _intensity, intensity ) )
    {
        _intensity = intensity;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The intensity of" << this->objectName() << "is set to" << intensity;
        intensityChanged( _intensity );
    }
}


