﻿#include <Core/MsnhAmbientLight.h>

AmbientLight::AmbientLight( QObject * parent ) : AbstractLight( parent )
{
    _color          = QVector3D( 1.0f, 1.0f, 1.0f );
    setObjectName( "Untitled Ambient Light" );
}


AmbientLight::AmbientLight( QVector3D color, QObject * parent ) : AbstractLight( color, parent )
{
    setObjectName( "Untitled Ambient Light" );
    setParent( parent );
}


AmbientLight::AmbientLight( const AmbientLight & light, QObject* parent) : AbstractLight( light, parent )
{
    setObjectName( light.objectName() );
}


AmbientLight::~AmbientLight()
{
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Ambient light" << this->objectName() << "is destroyed";
}

void AmbientLight::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Ambient Light: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "Enabled: " << _enabled;
    qDebug().nospace() << tab( l + 1 ) << "Color: " << _color;
    qDebug().nospace() << tab( l + 1 ) << "Intensity: " << _intensity;
}


void AmbientLight::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
}


