﻿

#include <Core/MsnhCamera.h>

Camera::Camera( QObject* parent ) : QObject( parent )
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    setObjectName( "Camera" );

    logLevel = tmp_log_level;

    initMarker();
    reset();
}


Camera::Camera( QVector3D position, QVector3D direction, QObject* parent ) : QObject( parent )
{
    setObjectName( "Camera" );
    setMovingSpeed( 0.1f );
    setFieldOfView( 45.0f );
    setNearPlane( 0.1f );
    setFarPlane( 100000.0f );
    setPosition( position );
    setDirection( direction );

    setOrbitPitch( 80.f );
    setOrbitYaw( 45.f );
    setOrbitCam( true );
    setOrbitRadius( 40.f );
    setOrbitMaxRadius( 1000.f );
    setOrbitFocusCenter(QVector3D(0,0,0));

    initMarker();
    reset();
}


Camera::Camera(const Camera & camera , QObject *parent) : QObject( parent )
{
    _movingSpeed	= camera._movingSpeed;
    _fieldOfView	= camera._fieldOfView;
    _aspectRatio	= camera._aspectRatio;
    _nearPlane      = camera._nearPlane;
    _farPlane       = camera._farPlane;
    _position       = camera._position;
    _direction      = camera._direction;
    _up             = camera._up;
    setObjectName( camera.objectName() );
}


Camera::~Camera()
{
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Camera" << this->objectName() << "is destroyed";
}


void Camera::moveForward( float shift )
{
    setPosition( _position + _direction * shift * _movingSpeed );
}


void Camera::moveRight( float shift )
{
    setPosition( _position + QVector3D::crossProduct( _direction, _up ) * shift * _movingSpeed );
}


void Camera::moveUp( float shift )
{
    setPosition( _position + _up * shift * _movingSpeed );
}


void Camera::turnLeft( float angle )
{
    QMatrix4x4 mat;
    mat.rotate( angle, QVector3D( 0, 1, 0 ) );
    setDirection( mat * _direction );
}


void Camera::lookUp( float angle )
{
    if ( _direction[1] < -0.99 && angle < 0 )
    {
        return;
    }else if ( _direction[1] > 0.99 && angle > 0 )
    {
        return;
    }
    QMatrix4x4 mat;
    mat.rotate( angle, QVector3D::crossProduct( _direction, _up ) );
    setDirection( mat * _direction );
}

void Camera::turnYaw(float angle)
{
    float tmp = _orbitYaw + angle;

    if(tmp>=360)
        tmp = tmp - 360;
    if(tmp<=0)
        tmp = 360 + tmp;

    setOrbitYaw(tmp);
}

void Camera::turnPitch(float angle)
{
    float tmp = _orbitPitch + angle;
    if(tmp>179)
        tmp = 179;
    if(tmp<1)
        tmp = 1;

    setOrbitPitch(tmp);
}

void Camera::changeRadius(float r)
{
    float tmp = _orbitRadius + r;

    if(tmp<0.1f)
    {
        tmp = 0.1f;
    }
    else if(tmp>_orbitMaxRadius)
    {
        tmp = _orbitMaxRadius;
    }

    setOrbitRadius(tmp);
}

void Camera::shiftPosX(float x)
{
    QVector3D tmp = _orbitFocusCenter + QVector3D(x,0,0);
    setOrbitFocusCenter(tmp);
}

void Camera::shiftPosY(float y)
{
    QVector3D tmp = _orbitFocusCenter + QVector3D(0,y,0);
    setOrbitFocusCenter(tmp);
}

void Camera::shiftPosZ(float z)
{
    QVector3D tmp = _orbitFocusCenter + QVector3D(0,0,z);
    setOrbitFocusCenter(tmp);
}

void Camera::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Camera: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "Position:      " << _position;
    qDebug().nospace() << tab( l + 1 ) << "Direction:     " << _direction;
    qDebug().nospace() << tab( l + 1 ) << "Up:            " << _up;
    qDebug().nospace() << tab( l + 1 ) << "Moving Speed:  " << _movingSpeed;
    qDebug().nospace() << tab( l + 1 ) << "Field of View: " << _fieldOfView;
    qDebug().nospace() << tab( l + 1 ) << "Aspect Ratio:  " << _aspectRatio;
    qDebug().nospace() << tab( l + 1 ) << "Near Plane:    " << _nearPlane;
    qDebug().nospace() << tab( l + 1 ) << "Far Plane:     " << _farPlane;
}


void Camera::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
}


/* Get properties */

float Camera::getMovingSpeed() const
{
    return(_movingSpeed);
}


float Camera::getFieldOfView() const
{
    return(_fieldOfView);
}


float Camera::getAspectRatio() const
{
    return(_aspectRatio);
}


float Camera::getNearPlane() const
{
    return(_nearPlane);
}


float Camera::getfFarPlane() const
{
    return(_farPlane);
}


QVector3D Camera::getPosition() const
{
    return(_position);
}


QVector3D Camera::getDirection() const
{
    return(_direction);
}

QVector3D Camera::getPositionApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getPosition();
    }
    else
    {
        auto position = getPosition();
        return QVector3D(position.z(),position.x(),position.y());
    }
}

QVector3D Camera::getDirectionApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getDirection();
    }
    else
    {
        auto direction = getDirection();
        return QVector3D(direction.z(),direction.x(),direction.y());
    }
}


/* Get projection and view matrix */

QMatrix4x4 Camera::getProjectionMatrix() const
{
    QMatrix4x4 mat;
    mat.perspective( _fieldOfView, _aspectRatio, _nearPlane, _farPlane );
    return(mat);
}


QMatrix4x4 Camera::getViewMatrix() const
{
    QMatrix4x4 mat;
    if(_orbitCam)
    {
        float camX   = cosf(rad(_orbitYaw)) * sinf(rad(_orbitPitch)) * _orbitRadius + _orbitFocusCenter[0] ;
        float camY   = cosf(rad(_orbitPitch)) * _orbitRadius  + _orbitFocusCenter[1];
        float camZ   = sinf(rad(_orbitYaw)) * sinf(rad(_orbitPitch)) * _orbitRadius  + _orbitFocusCenter[2] ;

        mat.lookAt( QVector3D(camX,camY,camZ), _orbitFocusCenter, QVector3D( 0, 1, 0 ) );
        return(mat);

    }
    else
    {
        mat.lookAt( _position,  _position + _direction, _up );
        return(mat);
    }
}

QVector3D Camera::getFocusCenter() const
{
    return _orbitFocusCenter;
}

QVector3D Camera::getFocusCenterApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getFocusCenter();
    }
    else
    {
        auto center = getFocusCenter();
        return QVector3D(center.z(),center.x(),center.y());
    }
}

void Camera::setFocusCenterApi(const QVector3D &focusCenter)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setFocusCenter(focusCenter);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        setFocusCenter(QVector3D(focusCenter.y(),focusCenter.z(),focusCenter.x()));
    }
}

void Camera::setFocusCenter(const QVector3D &focusCenter)
{
    if(!isEqual(_orbitFocusCenter, focusCenter))
    {
        _orbitFocusCenter = focusCenter;
        emit orbitFocusCenterChanged(_orbitFocusCenter);
    }
}


/* Public slots */

void Camera::reset()
{
    setMovingSpeed( 0.1f );
    setFieldOfView( 45.0f );
    setAspectRatio( 1.0f );
    setNearPlane( 0.1f );
    setFarPlane( 100000.0f );
    setPosition( QVector3D( 27.8546f, 6.94593f, 27.8546f ) );
    setDirection( QVector3D( -1, -0.25f, -1 ) );

    setOrbitPitch( 80.f );
    setOrbitYaw( 45.f );
    setOrbitCam( true );
    setOrbitRadius( 40.f );
    setOrbitMaxRadius( 1000.f );
    setOrbitFocusCenter(QVector3D(0,0,0));

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << this->objectName() << "is reset";

    emit cameraReset();
}

//void Camera::animateReset()
//{

//    if(animateCnt == 0)
//    {
//        pDelta = -(_orbitPitch-80.f)/30.f;
//        yDelta = -(_orbitYaw-45.f)/30.f;
//        rDelta = -(_orbitRadius-40.f)/30.f;
//    }

//    _orbitPitch  += pDelta;
//    _orbitYaw    += yDelta;
//    _orbitRadius += rDelta;

//    animateCnt ++;

//    if(animateCnt==30)
//    {
//        animateCnt = 0;
//        timer->stop();
//    }
//}

void Camera::initMarker()
{
    if(_centerMarker)
    {
        //nullptr 表示任意的信号或者接收者对象
        delete _centerMarker;
        _centerMarker = nullptr;
    }
    ModelLoader loader;
    _centerMarker = loader.loadMeshFromFile(":/shapes/Cone.obj");
    _centerMarker->getMaterial()->setColor(QVector3D(0.88f,0.88f,0));
    _centerMarker->setObjectName("Obrit Center");
    _centerMarker->setAlpha(0.86f);
    _centerMarker->setScaling(QVector3D(1.6f,1.6f,1.6f));
    _centerMarker->setPosition(_orbitFocusCenter);
    _centerMarker->setParent(this);
}

void Camera::setMeshVisible(bool meshVisible)
{
    _meshVisible = meshVisible;
}

void Camera::setMovingSpeed( float movingSpeed )
{
    if ( !isEqual( _movingSpeed, movingSpeed ) )
    {
        _movingSpeed = movingSpeed;
        movingSpeedChanged( _movingSpeed );
    }
}


void Camera::setFieldOfView( float fieldOfView )
{
    if ( !isEqual( _fieldOfView, fieldOfView ) )
    {
        _fieldOfView = fieldOfView;
        fieldOfViewChanged( _fieldOfView );
    }
}


void Camera::setAspectRatio( float aspectRatio )
{
    if ( !isEqual( _aspectRatio, aspectRatio ) )
    {
        _aspectRatio = aspectRatio;
        aspectRatioChanged( _aspectRatio );
    }
}


void Camera::setNearPlane( float nearPlane )
{
    if ( !isEqual( _nearPlane, nearPlane ) )
    {
        _nearPlane = nearPlane;
        nearPlaneChanged( _nearPlane );
    }
}


void Camera::setFarPlane( float farPlane )
{
    if ( !isEqual( _farPlane, farPlane ) )
    {
        _farPlane = farPlane;
        farPlaneChanged( _farPlane );
    }
}


void Camera::setPosition( QVector3D position )
{
    if ( !isEqual( _position, position ) )
    {
        _position = position;
        positionChanged( _position );
    }
}


void Camera::setDirection( QVector3D direction )
{
    direction.normalize();
    if ( !isEqual( _direction, direction ) )
    {
        _direction = direction;
        setUpVector();
        directionChanged( _direction );
    }
}

void Camera::setPositionApi(QVector3D position)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setPosition(position);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        setPosition(QVector3D(position.y(),position.z(),position.x()));
    }
}

void Camera::setDirectionApi(QVector3D direction)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setDirection(direction);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        setDirection(QVector3D(direction.y(),direction.z(),direction.x()));
    }
}

void Camera::setOrbitFocusCenter(const QVector3D &orbitFocusCenter)
{
    if(!isEqual(_orbitFocusCenter, orbitFocusCenter))
    {
        _orbitFocusCenter = orbitFocusCenter;
        emit orbitFocusCenterChanged(_orbitFocusCenter);
    }
}


void Camera::setOrbitMaxRadius(float orbitMaxRadius)
{
    if(!isEqual(_orbitMaxRadius, orbitMaxRadius))
    {
        if(orbitMaxRadius < 0.1f)
        {
            return;
        }

        if(_orbitRadius>orbitMaxRadius)
        {
            _orbitRadius = orbitMaxRadius;
        }

        _orbitMaxRadius = orbitMaxRadius;
        emit orbitMaxRadiusChanged(_orbitMaxRadius);
    }
}

void Camera::setOrbitRadius(float orbitRadius)
{
    if(!isEqual(_orbitRadius, orbitRadius))
    {
        _orbitRadius = orbitRadius;
        emit orbitRadiusChanged(_orbitRadius);
    }
}

void Camera::setOrbitYaw(float orbitYaw)
{
    if(!isEqual(_orbitYaw, orbitYaw))
    {
        _orbitYaw = orbitYaw;
        emit orbitYawChanged(_orbitYaw);
    }
}

void Camera::setOrbitPitch(float orbitPitch)
{
    if(!isEqual(_orbitPitch, orbitPitch))
    {
        _orbitPitch = orbitPitch;
        emit orbitPitchChanged(_orbitPitch);
    }
}


bool Camera::getMeshVisible() const
{
    return _meshVisible;
}

Mesh *Camera::getCenterMarker() const
{
    return _centerMarker;
}

QVector3D Camera::getOrbitFocusCenter() const
{
    return _orbitFocusCenter;
}

float Camera::getOrbitMaxRadius() const
{
    return _orbitMaxRadius;
}

float Camera::getOrbitRadius() const
{
    return _orbitRadius;
}

float Camera::getOrbitYaw() const
{
    return _orbitYaw;
}

float Camera::getOrbitPitch() const
{
    return _orbitPitch;
}

bool Camera::isOrbitCam() const
{
    return _orbitCam;
}

void Camera::setOrbitCam(bool orbitCam)
{
    if(orbitCam!=_orbitCam)
    {
        _orbitCam = orbitCam;
        emit camTypeChanged(_orbitCam);
    }
}

/* Private functions */

void Camera::setUpVector()
{
    QVector3D t = QVector3D::crossProduct( _direction, QVector3D( 0, 1, 0 ) );
    _up = QVector3D::crossProduct( t, _direction );
}




