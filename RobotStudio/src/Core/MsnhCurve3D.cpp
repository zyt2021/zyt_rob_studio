﻿#include <Core/MsnhCurve3D.h>

Curve3D::Curve3D(QObject *parent) : QObject(parent)
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;


    _lineWidth = 0.05f;

    _marker = new Mesh( Mesh::LINE,false, this );
    _marker->setObjectName( "Curve Marker" );
    _marker->setMaterial( new Material );

    setObjectName( "Untitled Curve" );

    logLevel = tmp_log_level;

    _marker->getMaterial()->setColor(QVector3D(0,0,0));
}

Curve3D::Curve3D(const Curve3D &curve, QObject *parent) : QObject(parent)
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    _marker = new Mesh( Mesh::LINE, 0 );
    _marker->setMaterial( new Material() );

    _color          = curve._color;
    _lineWidth      = curve._lineWidth;
    _lineVertices   = curve._lineVertices;
    _isCubeLine     = curve._isCubeLine;
    _deleteable     = curve._deleteable;

    _marker->setColor(_color);

    setObjectName(curve.objectName());

    logLevel = tmp_log_level;

    update();
}

Curve3D::~Curve3D()
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    delete _marker;

    logLevel = tmp_log_level;

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Curve" << this->objectName() << "is destroyed";
}


void Curve3D::addData(float x, float y, float z)
{
    addData(QVector3D(x,y,z));
}

void Curve3D::addData(QVector3D data)
{
    _lineVertices.push_back(data);
    update();
}

void Curve3D::addData(const QVector<QVector3D> &data)
{
    for (int i = 0; i < data.size(); ++i)
    {
        addData(data[i]);
    }
}

void Curve3D::removeFront()
{
    _lineVertices.pop_front();
    update();
}

void Curve3D::removeBack()
{
    _lineVertices.pop_back();
    update();
}

void Curve3D::removeAt(int index)
{
    _lineVertices.removeAt(index);
    update();
}

void Curve3D::clear()
{
    _lineVertices.clear();
}

bool Curve3D::isCubeLine() const
{
    return _isCubeLine;
}

QVector3D Curve3D::getColor() const
{
    return _color;
}

bool Curve3D::getVisible() const
{
    return _marker->getVisible();
}

float Curve3D::getLineWidth() const
{
    return _lineWidth*100.f;
}

Mesh *Curve3D::getMarker()
{
    return _marker;
}

QVector<QVector3D> Curve3D::getLineVertices() const
{
    return _lineVertices;
}

bool Curve3D::getDeleteable() const
{
    return _deleteable;
}

void Curve3D::setIsCubeLine(bool isCubeLine)
{
    if(_isCubeLine!=isCubeLine)
    {
        if(isCubeLine)
        {
            _marker->setMeshType(Mesh::TRIANGLE);
        }
        else
        {
            _marker->setMeshType(Mesh::LINE);
        }
        _isCubeLine = isCubeLine;
        update();
        emit isCubeLineChanged(_isCubeLine);
    }
}

void Curve3D::setColor(const QVector3D &color)
{
    if(!isEqual(_color, color))
    {
        _color = color;
        _marker->getMaterial()->setColor(color);
        emit colorChanged(_color);
    }
}

void Curve3D::setVisible(bool visible)
{
    if(getVisible()!=visible)
    {
        _marker->setVisible(visible);
        emit visiableChanged(visible);
    }
}

void Curve3D::setLineWidth(float lineWidth)
{
    lineWidth = lineWidth/100.f;
    if(!isEqual(_lineWidth, lineWidth))
    {
        _lineWidth = lineWidth;
        update();
        emit lineWidthChanged(lineWidth*100);
    }
}


void Curve3D::setLineVertices(const QVector<QVector3D> &lineVertices)
{
    _lineVertices = lineVertices;
    update();
    emit lineVerticesChanged(lineVertices);
}

void Curve3D::setDeleteable(bool deleteable)
{
    if(_deleteable!=deleteable)
    {
        _deleteable = deleteable;
        emit deleteableChanged(_deleteable);
    }
}

void Curve3D::update()
{
    if(!_isCubeLine)
    {
        QVector<Vertex>	vertices;
        QVector<uint32_t> indices;
        if(EngineUtil::axisMode == AXIS_Y_UP)
        {
            for (int i = 0; i < _lineVertices.size()-1; ++i)
            {
                vertices.push_back(Vertex(QVector3D(_lineVertices[i].x(), _lineVertices[i].y(), _lineVertices[i].z())));
                vertices.push_back(Vertex(QVector3D(_lineVertices[i+1].x(), _lineVertices[i+1].y(), _lineVertices[i+1].z())));
                indices.push_back(vertices.length()-2);
                indices.push_back(vertices.length()-1);
            }
        }
        else
        {
            for (int i = 0; i < _lineVertices.size()-1; ++i)
            {
                vertices.push_back(Vertex(QVector3D(_lineVertices[i].y(), _lineVertices[i].z(), _lineVertices[i].x())));
                vertices.push_back(Vertex(QVector3D(_lineVertices[i+1].y(), _lineVertices[i+1].z(), _lineVertices[i+1].x())));
                indices.push_back(vertices.length()-2);
                indices.push_back(vertices.length()-1);
            }
        }

        _marker->setGeometry(vertices, indices);
    }
    else
    {
        QVector<Vertex>	vertices;
        QVector<uint32_t> indices;
        if(EngineUtil::axisMode == AXIS_Z_UP)
        {
            ///
            ///             4 /| \ 5
            ///             /\ |  /\
            ///       v1  /   /\/ \  \  v4
            ///         /   /  /\   \  \
            ///       /   /  /3 6 \   \  \
            ///    1/   /  /        \   \  \ 8
            ///     \ /  / v2      v3 \   \/
            ///       \/                \/
            ///       2                 7

            for (int i = 0; i < _lineVertices.size()-1; ++i)
            {
                if(i == 0)
                {
                    vertices.push_back(Vertex(QVector3D(_lineVertices[i].y(), _lineVertices[i].z(), _lineVertices[i].x()+_lineWidth)));
                    vertices.push_back(Vertex(QVector3D(_lineVertices[i].y(), _lineVertices[i].z(), _lineVertices[i].x()-_lineWidth)));
                }

                vertices.push_back(Vertex(QVector3D(_lineVertices[i+1].y(), _lineVertices[i+1].z(), _lineVertices[i+1].x()+_lineWidth)));
                vertices.push_back(Vertex(QVector3D(_lineVertices[i+1].y(), _lineVertices[i+1].z(), _lineVertices[i+1].x()-_lineWidth)));

                indices.push_back(i*2+0);
                indices.push_back(i*2+1);
                indices.push_back(i*2+2);

                indices.push_back(i*2+1);
                indices.push_back(i*2+2);
                indices.push_back(i*2+3);
            }

            int all = _lineVertices.size()*2;

            for (int i = 0; i < _lineVertices.size()-1; ++i)
            {
                if(i == 0)
                {
                    vertices.push_back(Vertex(QVector3D(_lineVertices[i].y(), _lineVertices[i].z()+_lineWidth, _lineVertices[i].x())));
                    vertices.push_back(Vertex(QVector3D(_lineVertices[i].y(), _lineVertices[i].z()-_lineWidth, _lineVertices[i].x())));
                }

                vertices.push_back(Vertex(QVector3D(_lineVertices[i+1].y(), _lineVertices[i+1].z()+_lineWidth, _lineVertices[i+1].x())));
                vertices.push_back(Vertex(QVector3D(_lineVertices[i+1].y(), _lineVertices[i+1].z()-_lineWidth, _lineVertices[i+1].x())));

                indices.push_back(i*2+0+all);
                indices.push_back(i*2+1+all);
                indices.push_back(i*2+2+all);

                indices.push_back(i*2+1+all);
                indices.push_back(i*2+2+all);
                indices.push_back(i*2+3+all);
            }

            int all1 = _lineVertices.size()*2;

            for (int i = 0; i < _lineVertices.size()-1; ++i)
            {
                if(i == 0)
                {
                    vertices.push_back(Vertex(QVector3D(_lineVertices[i].y()+_lineWidth, _lineVertices[i].z(), _lineVertices[i].x())));
                    vertices.push_back(Vertex(QVector3D(_lineVertices[i].y()-_lineWidth, _lineVertices[i].z(), _lineVertices[i].x())));
                }

                vertices.push_back(Vertex(QVector3D(_lineVertices[i+1].y()+_lineWidth, _lineVertices[i+1].z(), _lineVertices[i+1].x())));
                vertices.push_back(Vertex(QVector3D(_lineVertices[i+1].y()-_lineWidth, _lineVertices[i+1].z(), _lineVertices[i+1].x())));

                indices.push_back(i*2+0+all1);
                indices.push_back(i*2+1+all1);
                indices.push_back(i*2+2+all1);

                indices.push_back(i*2+1+all1);
                indices.push_back(i*2+2+all1);
                indices.push_back(i*2+3+all1);
            }

            _marker->setGeometry(vertices, indices);
        }
        else
        {
            for (int i = 0; i < _lineVertices.size()-1; ++i)
            {
                vertices.push_back(Vertex(QVector3D(_lineVertices[i].y(), _lineVertices[i].z(), _lineVertices[i].x())));
                vertices.push_back(Vertex(QVector3D(_lineVertices[i+1].y(), _lineVertices[i+1].z(), _lineVertices[i+1].x())));
                indices.push_back(vertices.length()-2);
                indices.push_back(vertices.length()-1);
            }
        }
    }
}

