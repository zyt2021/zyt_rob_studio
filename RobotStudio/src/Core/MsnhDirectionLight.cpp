﻿#include <Core/MsnhDirectionLight.h>

DirectionalLight::DirectionalLight( QObject * parent ) : AbstractLight( parent )
{
    _color	  = QVector3D( 1.0f, 1.0f, 1.0f );
    _direction    = QVector3D( -1.0f, -1.0f, -1.0f );

    setObjectName( "Untitled Directional Light" );
}


DirectionalLight::DirectionalLight( QVector3D color, QVector3D direction, QObject * parent ) : AbstractLight( color, parent )
{
    _direction = direction;

    setObjectName( "Untitled Directional Light" );
}


DirectionalLight::DirectionalLight(const DirectionalLight & light , QObject *parent) : AbstractLight( light, parent )
{
    _direction = light._direction;

    setObjectName( light.objectName() );
}


DirectionalLight::~DirectionalLight()
{
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Directional light" << this->objectName() << "is destroyed";
}


void DirectionalLight::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Directional Light: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "Enabled: " << _enabled;
    qDebug().nospace() << tab( l + 1 ) << "Color: " << _color;
    qDebug().nospace() << tab( l + 1 ) << "Direction: " << _direction;
    qDebug().nospace() << tab( l + 1 ) << "Intensity: " << _intensity;
}


void DirectionalLight::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
}


QVector3D DirectionalLight::getDirection()
{
    return(_direction);
}

void DirectionalLight::setDirection( QVector3D direction )
{
    if ( !isEqual( _direction, direction ) )
    {
        _direction = direction;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The direction of" << this->objectName() << "is set to" << direction;
        directionChanged( _direction );
    }
}


