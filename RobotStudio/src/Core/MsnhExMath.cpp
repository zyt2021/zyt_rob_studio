﻿#include <Core/MsnhExMath.h>

bool isEqual( float a, float b )
{
    return(qAbs( a - b ) < Eps);
}


bool isEqual( QVector3D a, QVector3D b )
{
    return(isEqual( a[0], b[0] ) && isEqual( a[1], b[1] ) && isEqual( a[2], b[2] ) );
}


bool isnan( QVector2D a )
{
    return(qIsNaN( a[0] ) || qIsNaN( a[1] ) );
}


bool isnan( QVector3D a )
{
    return(qIsNaN( a[0] ) || qIsNaN( a[1] ) || qIsNaN( a[2] ) );
}


bool isnan( QVector4D a )
{
    return(qIsNaN( a[0] ) || qIsNaN( a[1] ) || qIsNaN( a[2] ) || qIsNaN( a[3] ) );
}


Line operator*(const QMatrix4x4 &m, const Line &l)
{
    QVector3D st = l.st, ed = l.st + l.dir;
    st = m * st;
    ed = m * ed;
    return Line({ st, ed - st });
}

//求线面的交点 (在线上也在面上),求BA的长，带方向 B单位向量投影和BA的投影之比
QVector3D getIntersectionOfLinePlane( Line l, Plane p )
{
    ///                                    / (direction)
    ///                                  B (st)
    ///          a                   / /
    ///        /|                  / A  \    (normal)
    ///      /  |                 \   C--\----->      BC在平面法向量的投影/direction向量在平面法向量的投影
    ///   O/____|______ b          \    /            如果BC在一点，则为0就是B点
    ///                             \ /
    ///                                   BC在法向量投影 / 单位长度投影 = B->A关于方向向量的比率
    float t = QVector3D::dotProduct( p.n, p.v - l.st ) / QVector3D::dotProduct( p.n, l.dir );
    if ( std::isnan( t ) && logLevel >= LOG_LEVEL_WARNING )
        dout << "Warning: NaN detected";

    // 线的原点 + 方向向量*比率（比率）
    return l.st + l.dir * t; //B ->A
}


QVector3D getClosestPointOfLines( Line l1, Line l2 )
{
    ///   |
    ///   |_____l2        ____________          1.垂直l1和l2的向量normal
    ///   \               |    |normal          2.l2和normal构成平面
    ///    \l1            |    |______ l2       3.l1到平面的交点
    ///                   |  \
    ///                   |____\_______
    ///                          \ l1
    ///
    l1.dir.normalize();
    l2.dir.normalize();
    QVector3D n = QVector3D::crossProduct( l1.dir, l2.dir );
    if ( isnan( n ) && logLevel >= LOG_LEVEL_WARNING )
        dout << "Warning: NaN detected";
    return(getIntersectionOfLinePlane( { l1.st, l1.dir },
                       { l2.st, QVector3D::crossProduct( n, l2.dir ) } ) );
}

//屏幕空间到opengl世界空间
Line screenPosToWorldRay( QVector2D cursorPos, QVector2D windowSize, QMatrix4x4 proj, QMatrix4x4 view )
{
    cursorPos.setY( windowSize.y() - cursorPos.y() );
    QMatrix4x4	inv = (proj * view).inverted();
    QVector4D	st( (cursorPos / windowSize - QVector2D( 0.5, 0.5 ) ) * 2.0f, -1.0, 1.0f );
    QVector4D	ed( (cursorPos / windowSize - QVector2D( 0.5, 0.5 ) ) * 2.0f, 0.0, 1.0f );
    st	= inv * st; st /= st.w();
    ed	= inv * ed; ed /= ed.w();
    if ( isnan( st ) && logLevel >= LOG_LEVEL_WARNING )
        dout << "Warning: NaN detected";
    if ( isnan( ed ) && logLevel >= LOG_LEVEL_WARNING )
        dout << "Warning: NaN detected";
    QVector3D dir = (ed - st).toVector3D();
    if ( dir.isNull() && logLevel >= LOG_LEVEL_WARNING )
        dout << "Warning: dir is null";
    return(Line { st.toVector3D(), dir.normalized() });
}


QVector3D GeometryEx::rotatePos(const QVector3D &angles, const QVector3D &pos)
{
    QMatrix3x3 modelRotMat = QQuaternion::fromEulerAngles(angles).toRotationMatrix();
    QVector3D  deltaTmp(modelRotMat(0,0)*pos.x() + modelRotMat(0,1)*pos.y() + modelRotMat(0,2)*pos.z(),
                        modelRotMat(1,0)*pos.x() + modelRotMat(1,1)*pos.y() + modelRotMat(1,2)*pos.z(),
                        modelRotMat(2,0)*pos.x() + modelRotMat(2,1)*pos.y() + modelRotMat(2,2)*pos.z());
    return deltaTmp;
}
