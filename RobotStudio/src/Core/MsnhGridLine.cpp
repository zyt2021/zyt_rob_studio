﻿#include <Core/MsnhGridLine.h>

Gridline::Gridline( QObject* parent ) : QObject( parent )
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    _marker = new Mesh( Mesh::LINE, false, this );
    _marker->setObjectName( "Gridline Marker" );
    _marker->setMaterial( new Material );

    setObjectName( "Untitled Gridline" );
    reset();

    logLevel = tmp_log_level;
}


/* Dump info */

Gridline::Gridline(const Gridline & gridline , QObject *parent) : QObject( parent )
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    _marker = new Mesh( Mesh::LINE, 0 );
    _marker->setMaterial( new Material );
    _xRange		= gridline._xRange;
    _yRange		= gridline._yRange;
    _zRange		= gridline._zRange;
    _xStride    = gridline._xStride;
    _yStride    = gridline._yStride;
    _zStride    = gridline._zStride;
    _color		= gridline._color;

    update();
    setObjectName( gridline.objectName() );

    logLevel = tmp_log_level;
}


Gridline::~Gridline()
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    delete _marker;

    logLevel = tmp_log_level;

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Gridline" << this->objectName() << "is destroyed";
}


void Gridline::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Gridline: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "X Range: " << _xRange;
    qDebug().nospace() << tab( l + 1 ) << "Y Range: " << _yRange;
    qDebug().nospace() << tab( l + 1 ) << "Z Range: " << _zRange;
    qDebug().nospace() << tab( l + 1 ) << "X Stride: " << _xStride;
    qDebug().nospace() << tab( l + 1 ) << "Y Stride: " << _yStride;
    qDebug().nospace() << tab( l + 1 ) << "Z Stride: " << _zStride;
    qDebug().nospace() << tab( l + 1 ) << "Color: " << _color;
}


void Gridline::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
}


/* Get properties */

QPair<float, float> Gridline::getXRange() const
{
    return(_xRange);
}


QPair<float, float> Gridline::getYRange() const
{
    return(_yRange);
}


QPair<float, float> Gridline::getZRange() const
{
    return(_zRange);
}


float Gridline::getXStride() const
{
    return(_xStride);
}


float Gridline::getYStride() const
{
    return(_yStride);
}


float Gridline::getZStride() const
{
    return(_zStride);
}

QPair<float, float> Gridline::getXRangeApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getXRange();
    }
    else
    {
        return getZRange();
    }
}

QPair<float, float> Gridline::getYRangeApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getYRange();
    }
    else
    {
        return getXRange();
    }
}

QPair<float, float> Gridline::getZRangeApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getZRange();
    }
    else
    {
        return getYRange();
    }
}

float Gridline::getXStrideApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getXStride();
    }
    else
    {
        return getZStride();
    }
}

float Gridline::getYStrideApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getYStride();
    }
    else
    {
        return getXStride();
    }
}

float Gridline::getZStrideApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getZStride();
    }
    else
    {
        return getYStride();
    }
}


QVector3D Gridline::getColor() const
{
    return(_color);
}


Mesh * Gridline::getMarker()
{
    return(_marker);
}


/* Public slots */

void Gridline::reset()
{
    _xRange		= { -20, 20 };
    _yRange		= { 0, 0 };
    _zRange		= { -20, 20 };
    _xStride            = 1;
    _yStride            = 1;
    _zStride            = 1;
    _color		= QVector3D( 0.98f, 0.98f, 0.98f );
    _gridPlane          = GRID_XY_PLANE;
    update();
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << this->objectName() << "is reset";
}


void Gridline::setXArguments( QVector3D xargs )
{
    if ( !isEqual( _xRange.first, xargs[0] ) || !isEqual( _xRange.second, xargs[1] ) || !isEqual( _xStride, xargs[2] ) )
    {
        _xRange		= { xargs[0], xargs[1] };
        _xStride	= xargs[2];
        update();
        xArgumentsChanged( xargs );
    }
}


void Gridline::setYArguments( QVector3D yargs )
{
    if ( !isEqual( _yRange.first, yargs[0] ) || !isEqual( _yRange.second, yargs[1] ) || !isEqual( _yStride, yargs[2] ) )
    {
        _yRange		= { yargs[0], yargs[1] };
        _yStride	= yargs[2];
        update();
        yArgumentsChanged( yargs );
    }
}


void Gridline::setZArguments( QVector3D zargs )
{
    if ( !isEqual( _zRange.first, zargs[0] ) || !isEqual( _zRange.second, zargs[1] ) || !isEqual( _zStride, zargs[2] ) )
    {
        _zRange		= { zargs[0], zargs[1] };
        _zStride	= zargs[2];
        update();
        zArgumentsChanged( zargs );
    }
}


void Gridline::setColor( QVector3D color )
{
    if ( !isEqual( _color, color ) )
    {
        _color = color;
        _marker->getMaterial()->setColor( _color );
        colorChanged( _color );
    }
}

void Gridline::setXArgumentsApi(QVector3D xargs)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setXArguments(xargs);
    }
    else
    {
        setZArguments(xargs);
    }
}

void Gridline::setYArgumentsApi(QVector3D yargs)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setYArguments(yargs);
    }
    else
    {
        setXArguments(yargs);
    }
}

void Gridline::setZArgumentsApi(QVector3D zargs)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setZArguments(zargs);
    }
    else
    {
        setYArguments(zargs);
    }
}

void Gridline::setCanModify(float canModify)
{
    _canModify = canModify;
}

float Gridline::getCanModify() const
{
    return _canModify;
}


void Gridline::update()
{
    QVector<Vertex>	vertices;
    QVector<uint32_t>	indices;
    for ( float yValue = _yRange.first; yValue < _yRange.second + 0.01f; yValue += _yStride )
    {
        for ( float xValue = _xRange.first; xValue < _xRange.second + 0.01f; xValue += _xStride )
        {
            vertices.push_back( Vertex( QVector3D( xValue, yValue, _zRange.first ) ) );
            vertices.push_back( Vertex( QVector3D( xValue, yValue, _zRange.second ) ) );
            indices.push_back( (uint32_t) vertices.size() - 2 );
            indices.push_back( (uint32_t) vertices.size() - 1 );
        }
        for ( float zValue = _zRange.first; zValue < _zRange.second + 0.01f; zValue += _zStride )
        {
            vertices.push_back( Vertex( QVector3D( _xRange.first, yValue, zValue ) ) );
            vertices.push_back( Vertex( QVector3D( _xRange.second, yValue, zValue ) ) );
            indices.push_back( (uint32_t) vertices.size() - 2 );
            indices.push_back( (uint32_t) vertices.size() - 1 );
        }
    }

    if(_gridPlane == GRID_YZ_PLANE)
    {
        for (int i = 0; i < vertices.size(); ++i)
        {
            vertices[i].position = QVector3D(vertices[i].position.x(), vertices[i].position.z(),vertices[i].position.y());
        }
    }
    else if(_gridPlane == GRID_ZX_PLANE)
    {
        for (int i = 0; i < vertices.size(); ++i)
        {
            vertices[i].position = QVector3D(vertices[i].position.y(), vertices[i].position.x(),vertices[i].position.z());
        }
    }

    _marker->setGeometry( vertices, indices );
    _marker->getMaterial()->setColor( _color );
}

Gridline::GridPlane Gridline::getGridPlane() const
{
    return _gridPlane;
}

void Gridline::setGridPlane(const GridPlane &gridPlane)
{
    if(_gridPlane != gridPlane)
    {
        _gridPlane = gridPlane;
        emit gridPlaneChanged(_gridPlane);
        update();
    }
}


