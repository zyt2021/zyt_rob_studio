﻿#include <Core/MsnhMaterial.h>

Material::Material( QObject* parent ) : QObject( parent )
{
    _color		= QVector3D( 1.0f, 1.0f, 1.0f );
    _ambient	= 0.2f;
    _diffuse	= 1.0f;
    _specular	= 0.5f;
    _shininess	= 32.0f;
    setObjectName( "Untitled Material" );
}


Material::Material( QVector3D color,
                    float ambient, float diffuse, float specular,
                    QObject * parent ) : QObject( parent )
{
    _color		= color;
    _ambient	= ambient;
    _diffuse	= diffuse;
    _specular	= specular;
    _shininess	= 32.0f;
    setObjectName( "Untitled Material" );
}


Material::Material(const Material & material , QObject *parent) : QObject( parent )
{
    _color              = material._color;
    _ambient            = material._ambient;
    _diffuse            = material._diffuse;
    _specular           = material._specular;
    _shininess          = material._shininess;
    _diffuseTexture     = material._diffuseTexture;
    _specularTexture	= material._specularTexture;
    _bumpTexture		= material._bumpTexture;
    setObjectName( material.objectName() );
}


Material::~Material()
{
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Material" << this->objectName() << "is destroyed";
}


void Material::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Material: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "Color: " << _color;
    qDebug().nospace() << tab( l + 1 ) << "Ambient Intensity: " << _ambient;
    qDebug().nospace() << tab( l + 1 ) << "Diffuse Intensity: " << _diffuse;
    qDebug().nospace() << tab( l + 1 ) << "Specular Intensity: " << _specular;
    qDebug().nospace() << tab( l + 1 ) << "Shininess: " << _shininess;
}


void Material::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
    if ( !_diffuseTexture.isNull() )
        _diffuseTexture->dumpObjectTree( l + 1 );
    if ( !_specularTexture.isNull() )
        _specularTexture->dumpObjectTree( l + 1 );
    if ( !_bumpTexture.isNull() )
        _bumpTexture->dumpObjectTree( l + 1 );
}


QVector3D Material::getColor() const
{
    return(_color);
}


float Material::getAmbient()
{
    return(_ambient);
}


float Material::getDiffuse()
{
    return(_diffuse);
}


float Material::getSpecular()
{
    return(_specular);
}


float Material::getShininess()
{
    return(_shininess);
}


QSharedPointer<Texture> Material::getDiffuseTexture()
{
    return(_diffuseTexture);
}


QSharedPointer<Texture> Material::getSpecularTexture()
{
    return(_specularTexture);
}


QSharedPointer<Texture> Material::getBumpTexture()
{
    return(_bumpTexture);
}


void Material::setColor( QVector3D color )
{
    if ( !isEqual( _color, color ) )
    {
        _color = color;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The color of" << this->objectName() << "is set to" << color;
        emit colorChanged( _color );
    }
}


void Material::setAmbient( float ambient )
{
    if ( !isEqual( _ambient, ambient ) )
    {
        _ambient = ambient;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The ambient weight of" << this->objectName() << "is set to" << ambient;
        emit ambientChanged( _ambient );
    }
}


void Material::setDiffuse( float diffuse )
{
    if ( !isEqual( _diffuse, diffuse ) )
    {
        _diffuse = diffuse;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The diffuse weight of" << this->objectName() << "is set to" << diffuse;
        emit diffuseChanged( _diffuse );
    }
}


void Material::setSpecular( float specular )
{
    if ( !isEqual( _specular, specular ) )
    {
        _specular = specular;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The specular weight of" << this->objectName() << "is set to" << specular;
        emit specularChanged( _specular );
    }
}


void Material::setShininess( float shininess )
{
    if ( !isEqual( _shininess, shininess ) )
    {
        _shininess = shininess;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The shininess of" << this->objectName() << "is set to" << shininess;
        emit shininessChanged( _shininess );
    }
}


void Material::setDiffuseTexture( QSharedPointer<Texture> diffuseTexture )
{
    if ( _diffuseTexture != diffuseTexture )
    {
        _diffuseTexture = diffuseTexture;
        if ( logLevel >= LOG_LEVEL_INFO && !diffuseTexture.isNull() )
            dout	<< "Diffuse texture" << diffuseTexture->objectName()
                    << "is assigned to material" << this->objectName();
        emit diffuseTextureChanged( _diffuseTexture );
    }
}


void Material::setSpecularTexture( QSharedPointer<Texture> specularTexture )
{
    if ( _specularTexture != specularTexture )
    {
        _specularTexture = specularTexture;
        if ( logLevel >= LOG_LEVEL_INFO && !specularTexture.isNull() )
            dout	<< "Specular texture" << specularTexture->objectName()
                    << "is assigned to material" << this->objectName();
        emit specularTextureChanged( _specularTexture );
    }
}


void Material::setBumpTexture( QSharedPointer<Texture> bumpTexture )
{
    if ( _bumpTexture != bumpTexture )
    {
        _bumpTexture = bumpTexture;
        if ( logLevel >= LOG_LEVEL_INFO && !bumpTexture.isNull() )
            dout	<< "Bump texture" << bumpTexture->objectName()
                    << "is assigned to material" << this->objectName();
        emit bumpTextureChanged( _bumpTexture );
    }
}



