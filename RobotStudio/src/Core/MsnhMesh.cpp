﻿#include <Core/MsnhMesh.h>
#include <Core/MsnhAbstractGizmo.h>
#include <Core/MsnhAbstractLight.h>

Mesh::Mesh(bool isPointCloud, QObject * parent ) : AbstractEntity( parent )
{
    _meshType	= TRIANGLE;
    _material	= nullptr;
    _material	= new Material(this );
    setObjectName( "Untitled Mesh" );
}


Mesh::Mesh(MeshType meshType, bool isPointCloud, QObject * parent ) : AbstractEntity( parent )
{
    _meshType	= meshType;
    _material	= nullptr;
    _material	= new Material(parent );
    setObjectName( "Untitled Mesh" );
}


Mesh::Mesh(const Mesh & mesh , QObject *parent) : AbstractEntity( mesh, parent )
{
    _meshType	= mesh._meshType;
    _vertices	= mesh._vertices;
    _indices	= mesh._indices;
    _material	= new Material( *mesh._material, this );
    setObjectName( mesh.objectName() );
}


Mesh::~Mesh()
{
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Mesh" << this->objectName() << "is destroyed";
}


/* Dump info */

void Mesh::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Mesh: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "Type: " <<
    (_meshType == TRIANGLE ? "Triangle" : (_meshType == LINE ? "Line" : "Point") );
    qDebug().nospace() << tab( l + 1 ) << "Visible: " << _visible;
    qDebug().nospace() << tab( l + 1 ) << "Position: " << _position;
    qDebug().nospace() << tab( l + 1 ) << "Rotation: " << _rotation;
    qDebug().nospace() << tab( l + 1 ) << "Scaling:  " << _scaling;
    qDebug( "%s%d vertices, %d indices, %d material",
        tab( l + 1 ), _vertices.size(), _indices.size(), _material != 0 );
}


void Mesh::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
    if ( _material )
        _material->dumpObjectTree( l + 1 );
}


bool Mesh::isGizmo() const
{
    if ( qobject_cast<AbstractGizmo*>( parent() ) )
        return(true);
    return(false);
}


bool Mesh::isLight() const
{
    if ( qobject_cast<AbstractLight*>( parent() ) )
        return(true);
    return(false);
}


bool Mesh::isMesh() const
{
    if ( isGizmo() )
        return(false);
    if ( isLight() )
        return(false);
    return(true);
}


bool Mesh::isModel() const
{
    return(false);
}

void Mesh::setColor(QVector3D color)
{
    _color = color;
    _material->setColor(color);
}

QVector3D Mesh::getColor() const
{
    return _color;
}


QVector3D Mesh::getCenterOfMass() const
{
    QVector3D	centerOfMass;
    float		totalMass	= 0;
    QMatrix4x4	modelMat	= getGlobalModelMatrix();
    for ( int i = 0; i < _indices.size(); )
    {
        QVector3D	centroid;
        float		mass = 0;
        if ( _meshType == POINT )
        {
            centroid	= modelMat * _vertices[_indices[i + 0]].position;
            mass		= 1.0f;
            i		+= 1;
        } else if ( _meshType == LINE )
        {
            QVector3D	p0	= modelMat * _vertices[_indices[i + 0]].position;
            QVector3D	p1	= modelMat * _vertices[_indices[i + 1]].position;
            centroid	= (p0 + p1) / 2;
            mass		= p0.distanceToPoint( p1 );
            i		+= 2;
        } else if ( _meshType == TRIANGLE )
        {
            QVector3D	p0	= modelMat * _vertices[_indices[i + 0]].position;
            QVector3D	p1	= modelMat * _vertices[_indices[i + 1]].position;
            QVector3D	p2	= modelMat * _vertices[_indices[i + 2]].position;
            centroid	= (p0 + p1 + p2) / 3;
            mass		= QVector3D::crossProduct( p1 - p0, p2 - p0 ).length() / 2;
            i		+= 3;
        }
        centerOfMass	+= centroid * mass;
        totalMass	+= mass;
    }
    return(centerOfMass / totalMass);
}


float Mesh::getMass() const
{
    float		totalMass	= 0;
    QMatrix4x4	modelMat	= getGlobalModelMatrix();
    for ( int i = 0; i < _indices.size(); )
    {
        if ( _meshType == POINT )
        {
            totalMass	+= 1.0f;
            i		+= 1;
        } else if ( _meshType == LINE )
        {
            QVector3D	p0	= modelMat * _vertices[_indices[i + 0]].position;
            QVector3D	p1	= modelMat * _vertices[_indices[i + 1]].position;
            totalMass	+= p0.distanceToPoint( p1 );
            i		+= 2;
        } else if ( _meshType == TRIANGLE )
        {
            QVector3D	p0	= modelMat * _vertices[_indices[i + 0]].position;
            QVector3D	p1	= modelMat * _vertices[_indices[i + 1]].position;
            QVector3D	p2	= modelMat * _vertices[_indices[i + 2]].position;
            totalMass	+= QVector3D::crossProduct( p1 - p0, p2 - p0 ).length() / 2;
            i		+= 3;
        }
    }
    return(totalMass);
}


Mesh::MeshType Mesh::getMeshType() const
{
    return(_meshType);
}


const QVector<Vertex> & Mesh::getVertices() const
{
    return(_vertices);
}


const QVector<uint32_t> & Mesh::getIndices() const
{
    return(_indices);
}


Material * Mesh::getMaterial() const
{
    return(_material);
}

bool Mesh::getAlwaysOnTop() const
{
    return _alwaysOnTop;
}

Mesh * Mesh::merge( const Mesh * mesh1, const Mesh * mesh2 )
{
    if ( mesh1 == nullptr && mesh2 == nullptr )
        return (nullptr);
    else if ( mesh1 == 0 || mesh2 == 0 )
    {
        if ( mesh1 == 0 )
            mesh1 = mesh2;
        Mesh* mergedMesh = new Mesh( mesh1->getMeshType() );
        mergedMesh->setObjectName( mesh1->objectName() );
        mergedMesh->setMaterial( new Material( *mesh1->getMaterial(), mergedMesh ) );
        for ( int i = 0; i < mesh1->_vertices.size(); i++ )
            mergedMesh->_vertices.push_back( mesh1->getGlobalModelMatrix() * mesh1->_vertices[i] );
        mergedMesh->_indices = mesh1->_indices;
        return(mergedMesh);
    }

    if ( mesh1->getMeshType() != mesh2->getMeshType() )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to merge" << mesh1->objectName() << "and" << mesh2->objectName() << ": type not match";
        return (nullptr);
    }

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Merging" << mesh1->objectName() << "and" << mesh2->objectName();

    Mesh* mergedMesh = new Mesh( mesh1->getMeshType() );
    mergedMesh->setObjectName( mesh1->objectName() + mesh2->objectName() );
    mergedMesh->setMaterial( new Material );

    for ( int i = 0; i < mesh1->_vertices.size(); i++ )
        mergedMesh->_vertices.push_back( mesh1->getGlobalModelMatrix() * mesh1->_vertices[i] );

    for ( int i = 0; i < mesh2->_vertices.size(); i++ )
        mergedMesh->_vertices.push_back( mesh2->getGlobalModelMatrix() * mesh2->_vertices[i] );

    mergedMesh->_indices = mesh1->_indices;
    for ( int i = 0; i < mesh2->_indices.size(); i++ )
        mergedMesh->_indices.push_back( mesh2->_indices[i] + mesh1->_vertices.size() );

    return(mergedMesh);
}


void Mesh::setMeshType( MeshType meshType )
{
    if ( _meshType != meshType )
    {
        _meshType = meshType;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The type of mesh" << this->objectName() << "is set to"
                << (_meshType == TRIANGLE ? "Triangle" : (_meshType == LINE ? "Line" : "Point") );
       emit meshTypeChanged( _meshType );
    }
}


void Mesh::setGeometry( const QVector<Vertex> & vertices, const QVector<uint32_t> & indices )
{
    if ( _vertices != vertices || _indices != indices )
    {
        _vertices	= vertices;
        _indices	= indices;
        emit geometryChanged( _vertices, _indices );
    }
}


bool Mesh::setMaterial( Material * material )
{
    if ( _material == material )
        return(false);

    if ( _material )
    {
        Material* tmp = _material;
        _material = nullptr;
        delete tmp;
    }

    if ( material )
    {
        _material = material;
        _material->setParent( this );
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "Material" << material->objectName() << "is assigned to mesh" << objectName();
    }

    emit materialChanged( _material );
    return(true);
}

void Mesh::setAlwaysOnTop(bool alwaysOnTop)
{
    if(_alwaysOnTop != alwaysOnTop)
    {
        _alwaysOnTop = alwaysOnTop;
        emit(_alwaysOnTop);
    }
}


void Mesh::reverseNormals()
{
    for ( int i = 0; i < _vertices.size(); i++ )
        _vertices[i].normal = -_vertices[i].normal;
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Normals of" << this->objectName() << "is reversed";
    emit geometryChanged( _vertices, _indices );
}


void Mesh::reverseTangents()
{
    for ( int i = 0; i < _vertices.size(); i++ )
        _vertices[i].tangent = -_vertices[i].tangent;
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Tangents of" << this->objectName() << "is reversed";
    emit geometryChanged( _vertices, _indices );
}


void Mesh::reverseBitangents()
{
    for ( int i = 0; i < _vertices.size(); i++ )
        _vertices[i].bitangent = -_vertices[i].bitangent;
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Bitangents of" << this->objectName() << "is reversed";
    emit geometryChanged( _vertices, _indices );
}

void Mesh::childEvent( QChildEvent * e )
{
    if ( e->added() )
    {
        if ( Material * material = qobject_cast<Material*>( e->child() ) )
            setMaterial( material );
    }
    else if ( e->removed() )
    {
        if ( e->child() == _material )
        {
            _material = nullptr;
            emit materialChanged( 0 );
        }
    }
}

QDataStream & operator>>( QDataStream & in, Mesh::MeshType & meshType )
{
    qint32 t;
    in >> t;
    if ( t == 0 )
        meshType = Mesh::TRIANGLE;
    else if ( t == 1 )
        meshType = Mesh::LINE;
    else
        meshType = Mesh::POINT;
    return(in);
}

