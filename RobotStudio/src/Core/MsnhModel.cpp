﻿#include <Core/MsnhModel.h>
#include <Core/MsnhModelLoader.h>

Model::Model( QObject * parent ) : AbstractEntity( parent )
{
    setObjectName( "Untitled model" );
}


Model::Model(const Model & model , QObject *parent) : AbstractEntity( model, parent )
{
    for ( int i = 0; i < model._childMeshes.size(); i++ )
        addChildMesh( new Mesh( *model._childMeshes[i], parent) );
    for ( int i = 0; i < model._childModels.size(); i++ )
        addChildModel( new Model( *model._childModels[i], parent ) );
}


Model::~Model()
{
    emit destroying(this); //防止bullet配置被删除后,bullet的world再次释放时出现错误

    releaseRigidBody();

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Model" << this->objectName() << "is destroyed";
}


bool Model::addChildMesh( Mesh * mesh )
{
    if ( !mesh || _childMeshes.contains( mesh ) )
        return(false);

    _childMeshes.push_back( mesh );
    mesh->setParent( this ); //触发childEvent事件
    emit childMeshAdded( mesh );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Mesh" << mesh->objectName() << "is added to model" << this->objectName() << "as child";

    return(true);
}


bool Model::addChildModel( Model * model ) //add多级model的问题
{
    if ( !model || _childModels.contains( model ) )
        return(false);

    _childModels.push_back( model );
    model->setParent( this ); //触发childEvent事件
    emit childModelAdded( model );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Model" << model->objectName() << "is added to model" << this->objectName() << "as child";

    return(true);
}


bool Model::removeChildMesh( QObject * mesh, bool recursive )
{
    for ( int i = 0; i < _childMeshes.size(); i++ )
    {
        if ( _childMeshes[i] == mesh )
        {
            _childMeshes.erase( _childMeshes.begin() + i );
            childMeshRemoved( mesh );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Child mesh" << mesh->objectName() << "is removed from model" << this->objectName();
            if ( _childMeshes.size() == 0 && _childModels.size() == 0 )
                delete this;
            return(true);
        }
    }

    if ( !recursive )
        return(false);

    for ( int i = 0; i < _childModels.size(); i++ )
    {
        if ( _childModels[i]->removeChildMesh( mesh, recursive ) )
            return(true);
    }
    return(false);
}


bool Model::removeChildModel( QObject * model, bool recursive )
{
    for ( int i = 0; i < _childModels.size(); i++ )
    {
        if ( _childModels[i] == model )
        {
            _childModels.erase( _childModels.begin() + i );
            childModelRemoved( model );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Child model" << model->objectName() << "is removed from model" << this->objectName();
            if ( _childMeshes.size() == 0 && _childModels.size() == 0 )
                delete this;
            return(true);
        }
    }
    if ( !recursive )
        return(false);
    for ( int i = 0; i < _childModels.size(); i++ )
    {
        if ( _childModels[i]->removeChildModel( model, recursive ) )
            return(true);
    }
    return(false);
}


/* Dump info */

void Model::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Model: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "Visible: " << _visible;
    qDebug().nospace() << tab( l + 1 ) << "Position: " << _position;
    qDebug().nospace() << tab( l + 1 ) << "Rotation: " << _rotation;
    qDebug().nospace() << tab( l + 1 ) << "Scaling:  " << _scaling;
    qDebug( "%s%d child mesh(es), %d child model(s)",
            tab( l + 1 ), _childMeshes.size(), _childModels.size() );
}


void Model::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
    for ( int i = 0; i < _childMeshes.size(); i++ )
        _childMeshes[i]->dumpObjectTree( l + 1 );
    for ( int i = 0; i < _childModels.size(); i++ )
        _childModels[i]->dumpObjectTree( l + 1 );
}


bool Model::isGizmo() const
{
    return(false);
}


bool Model::isLight() const
{
    return(false);
}


bool Model::isMesh() const
{
    return(false);
}


bool Model::isModel() const
{
    return(true);
}

void Model::setColor(QVector3D color)
{
    _color = color;
    for ( int i = 0; i < _childMeshes.size(); i++ )
        _childMeshes[i]->setColor(_color);
    for ( int i = 0; i < _childModels.size(); i++ )
        _childModels[i]->setColor(_color);
}

QVector3D Model::getColor() const
{
    return _color;
}

void Model::setAlpha(float alpha)
{
    if(alpha>1 || alpha <0)
    {
        return;
    }

    _modelAlpha = alpha;
    for ( int i = 0; i < _childMeshes.size(); i++ )
        _childMeshes[i]->setAlpha(alpha);
    for ( int i = 0; i < _childModels.size(); i++ )
        _childModels[i]->setAlpha(alpha);
}

float Model::getAlpha() const
{
    return _modelAlpha;
}


QVector3D Model::getCenterOfMass() const
{
    QVector3D	centerOfMass;
    float		totalMass = 0;
    for ( int i = 0; i < _childMeshes.size(); i++ )
    {
        centerOfMass	+= _childMeshes[i]->getCenterOfMass() * _childMeshes[i]->getMass();
        totalMass	+= _childMeshes[i]->getMass();
    }
    for ( int i = 0; i < _childModels.size(); i++ )
    {
        centerOfMass	+= _childModels[i]->getCenterOfMass() * _childModels[i]->getMass();
        totalMass	+= _childModels[i]->getMass();
    }
    return(centerOfMass / totalMass);
}


float Model::getMass() const
{
    float totalMass = 0;
    for ( int i = 0; i < _childMeshes.size(); i++ )
        totalMass += _childMeshes[i]->getMass();
    for ( int i = 0; i < _childModels.size(); i++ )
        totalMass += _childModels[i]->getMass();
    return(totalMass);
}


Mesh * Model::assemble() const
{
    Mesh* assembledMesh = nullptr;
    for ( int i = 0; i < _childMeshes.size(); i++ )
    {
        Mesh* old = assembledMesh;
        assembledMesh = Mesh::merge( old, _childMeshes[i] );
        if ( old )
            delete old;
    }
    for ( int i = 0; i < _childModels.size(); i++ )
    {
        Mesh	* old1	= assembledMesh;
        Mesh	* old2	= _childModels[i]->assemble();
        assembledMesh = Mesh::merge( old1, old2 );
        if ( old1 )
            delete old1;
        if ( old2 )
            delete old2;
    }
    return(assembledMesh);
}

void Model::setJointType(Msnhnet::JointType jointType)
{
    if(_jointType!=jointType)
    {
        _jointType = jointType;
        emit jointTypeChanged(_jointType);
    }
}

Msnhnet::JointType Model::getJointType() const
{
    return _jointType;
}


const QVector<Mesh*> & Model::getChildMeshes() const
{
    return(_childMeshes);
}


const QVector<Model*> & Model::getChildModels() const
{
    return(_childModels);
}

void Model::setJointMinMax(const double &min, const double &max)
{
    assert(min < max);

    if(fabs(_jointMin - min)> MSNH_F64_EPS || fabs(_jointMax - max)> MSNH_F64_EPS)
    {
        _jointMin = min;
        _jointMax = max;
        emit jointMinMaxChanged(_jointMin, _jointMax);
    }
}

void Model::setPositionApi(QVector3D delta)
{

    if(!EngineUtil::physicalEngineRunning)
    {
        btTransform trans;

        _body->getMotionState()->getWorldTransform(trans);

        trans.setOrigin({delta.x(),delta.y(),delta.z()});

        _motionState->setWorldTransform(trans);
    }

    AbstractEntity::setPositionApi(delta);
}

Msnhnet::Frame Model::getFrame() const
{
    auto rot = getRotationApi()*MSNH_DEG_2_RAD;
    auto pos = getPositionApi();

    Msnhnet::EulerDS euler;
    euler.setval(rot.x(),rot.y(),rot.z());
    Msnhnet::RotationMatDS rotMat = Msnhnet::GeometryS::euler2RotMat(euler,Msnhnet::ROT_ZYX);
    return Msnhnet::Frame(rotMat,{pos.x(),pos.y(),pos.z()});
}


void Model::reverseNormals()
{
    for ( int i = 0; i < _childMeshes.size(); i++ )
        _childMeshes[i]->reverseNormals();
    for ( int i = 0; i < _childModels.size(); i++ )
        _childModels[i]->reverseNormals();
}


void Model::reverseTangents()
{
    for ( int i = 0; i < _childMeshes.size(); i++ )
        _childMeshes[i]->reverseTangents();
    for ( int i = 0; i < _childModels.size(); i++ )
        _childModels[i]->reverseTangents();
}


void Model::reverseBitangents()
{
    for ( int i = 0; i < _childMeshes.size(); i++ )
        _childMeshes[i]->reverseBitangents();
    for ( int i = 0; i < _childModels.size(); i++ )
        _childModels[i]->reverseBitangents();
}

void Model::setDeleteable(bool deleteable)
{
    for ( int i = 0; i < _childMeshes.size(); i++ )
        _childMeshes[i]->setDeleteable(deleteable);
    for ( int i = 0; i < _childModels.size(); i++ )
        _childModels[i]->setDeleteable(deleteable);
}


void Model::childEvent( QChildEvent * e )
{
    if ( e->added() )
    {
        if ( Model * model = qobject_cast<Model*>( e->child() ) )
            addChildModel( model );
        else if ( Mesh * mesh = qobject_cast<Mesh*>( e->child() ) )
            addChildMesh( mesh );
    }
    else if ( e->removed() )
    {
        if ( removeChildModel( e->child(), false ) )
            return;
        if ( removeChildMesh( e->child(), false ) )
            return;
    }
}

btRigidBody *Model::getRigidBody() const
{
    return _body;
}

void Model::updatePhysical()
{
    btTransform trans;
    _motionState->getWorldTransform(trans);

    float rz = 0.f;
    float ry = 0.f;
    float rx = 0.f;

    trans.getRotation().getEulerZYX(rz, ry, rx);
    btVector3 pos = trans.getOrigin();

    QVector3D currentPos;
    QVector3D currentRot;

    currentPos.setX(pos.x());
    currentPos.setY(pos.y());
    currentPos.setZ(pos.z());

    currentRot.setX(rx*MSNH_RAD_2_DEG);
    currentRot.setY(ry*MSNH_RAD_2_DEG);
    currentRot.setZ(rz*MSNH_RAD_2_DEG);

    if((currentPos - _lastPhysicPos).length() < MSNH_F32_EPS && (currentRot - _lastPhysicRot).length() < MSNH_F32_EPS)
    {
        return;
    }

    _lastPhysicPos = currentPos;
    _lastPhysicRot = currentRot;
    setPositionApi(currentPos);
    setRotationApi(currentRot);
}

Msnhnet::Vector3DS Model::getAxis() const
{
    return _axis;
}

bool Model::getDeleteable() const
{

    for ( int i = 0; i < _childMeshes.size(); i++ )
    {
        bool deleteable = _childMeshes[i]->getDeleteable();
        if(!deleteable)
        {
            return false;
        }
    }

    for ( int i = 0; i < _childModels.size(); i++ )
    {
        bool deleteable = _childModels[i]->getDeleteable();
        if(!deleteable)
        {
            return false;
        }
    }

    return true;
}

void Model::releaseRigidBody()
{
    if(_body)
    {
        delete _body;
        _body = nullptr;
    }

    if(_motionState)
    {
        delete _motionState;
        _motionState = nullptr;
    }

    if(_collisionShape)
    {
        delete _collisionShape;
        _collisionShape =  nullptr;
    }
}

void Model::initRigidBody(QVector3D shape, float mass, bool isCube)
{
    //TODO: 所有参数由属性传入，考虑物理系统重新仿真的问题，每次start重新添加，停止后删除 2021/08/18
    if(!_collisionShape)
    {
        if(isCube)
        {
            _collisionShape = new btBoxShape(btVector3(shape.x()/2,shape.y()/2,shape.z()/2));
        }
        else
        {
            _collisionShape = new btSphereShape(1);
        }

    }
    //当前位置
    btTransform trans;
    trans.setIdentity();
    QVector3D pos = getGlobalPositionApi();

    trans.setOrigin(btVector3(pos.x(), pos.y(), pos.z()));
    QVector3D rot = getGlobalRotationApi();
    rot = rot * (float)MSNH_DEG_2_RAD;
    trans.setRotation(btQuaternion(rot.x(),rot.y(),rot.z()));

    if(!_motionState)
    {
        //运动状态
        _motionState = new btDefaultMotionState(trans);
    }

    //转动惯量
    btVector3 localInteria(0,0,0);

    _collisionShape->calculateLocalInertia(mass, localInteria);

    if(!_body)
    {
        //创建刚体描述
        btRigidBody::btRigidBodyConstructionInfo cInfo(mass, _motionState, _collisionShape, localInteria);
        //创建一个刚体
        _body = new btRigidBody(cInfo);
        _body->setAngularVelocity(btVector3(0,10,0));
        //        _body->setFriction(0.1);
    }


    ModelLoader loader;

    Mesh* mesh;
    if(isCube)
    {
        mesh = loader.loadMeshFromFile(":/shapes/Cube.obj");
    }
    else
    {
        mesh = loader.loadMeshFromFile(":/shapes/Sphere.obj");
        mesh->setRotationApi({90,0,0});
    }


    Material *mat = new Material();
    mat->setColor({1,0,0});
    mesh->setMaterial(mat);
    mesh->setWireFrameMode(true);

    addChildMesh(mesh);
}

void Model::setAxis(const Msnhnet::Vector3DS &axis)
{
    _axis = axis;
}

void Model::setFrame(const Msnhnet::Frame &frame)
{
    auto trans  = frame.trans;
    auto rotmat = frame.rotMat;

    auto euler  = Msnhnet::GeometryS::rotMat2Euler(rotmat,Msnhnet::ROT_ZYX);
    setPositionApi({(float)trans[0],(float)trans[1],(float)trans[2]});
    setRotationApi({(float)(euler[0]*MSNH_RAD_2_DEG),(float)(euler[1]*MSNH_RAD_2_DEG),(float)(euler[2]*MSNH_RAD_2_DEG)});
}

double Model::getJointMax() const
{
    return _jointMax;
}

void Model::setEnd2TipFrame(const Msnhnet::Frame &frame)
{
    if(_end2TipFrame != frame)
    {
        _end2TipFrame = frame;
        emit end2TipFrameChanged(_end2TipFrame);
    }
}

Msnhnet::Frame Model::getEnd2TipFrame() const
{
    return _end2TipFrame;
}

void Model::singleMove(float value)
{
    //TODO:
    if(_jointType == Msnhnet::JOINT_FIXED)
    {
        qDebug()<<"This joint can not move: "+objectName();
        return;
    }

    if(fabsf(_singleMove-value)>MSNH_F32_EPS)
    {
        _singleMove = value;

        if(_jointType == Msnhnet::JOINT_ROT_AXIS)
        {
            if(_axis[0]>0 && _axis[1]==0 && _axis[2]==0)
            {
                setRotationApi(QVector3D({value, 0, 0}));
            }
            else if(_axis[0]<0 && _axis[1]==0 && _axis[2]==0)
            {
                setRotationApi(QVector3D({-value, 0, 0}));
            }
            else if(_axis[0]==0 && _axis[1]>0 && _axis[2]==0)
            {
                setRotationApi(QVector3D({0, value, 0}));
            }
            else if(_axis[0]==0 && _axis[1]<0 && _axis[2]==0)
            {
                setRotationApi(QVector3D({0, -value, 0}));
            }
            else if(_axis[0]==0 && _axis[1]==0 && _axis[2]>0)
            {
                setRotationApi(QVector3D({0, 0, value}));
            }
            else if(_axis[0]==0 && _axis[1]==0 && _axis[2]<0)
            {
                setRotationApi(QVector3D({0, 0, -value}));
            }
        }
        else if(_jointType == Msnhnet::JOINT_TRANS_AXIS)
        {
            if(_axis[0]>0 && _axis[1]==0 && _axis[2]==0)
            {
                setPositionApi(QVector3D({value, 0, 0}));
            }
            else if(_axis[0]<0 && _axis[1]==0 && _axis[2]==0)
            {
                setPositionApi(QVector3D({-value, 0, 0}));
            }
            else if(_axis[0]==0 && _axis[1]>0 && _axis[2]==0)
            {
                setPositionApi(QVector3D({0, value, 0}));
            }
            else if(_axis[0]==0 && _axis[1]<0 && _axis[2]==0)
            {
                setPositionApi(QVector3D({0, -value, 0}));
            }
            else if(_axis[0]==0 && _axis[1]==0 && _axis[2]>0)
            {
                setPositionApi(QVector3D({0, 0, value}));
            }
            else if(_axis[0]==0 && _axis[1]==0 && _axis[2]<0)
            {
                setPositionApi(QVector3D({0, 0, -value}));
            }
        }

        emit singleMoved(_singleMove);
    }


}

void Model::multiMove(const QVector3D &value)
{

    //TODO:
    if(_jointType == Msnhnet::JOINT_FIXED)
    {
        qDebug()<<"This joint can not move: "+objectName();
        return;
    }

    float flag1 = _axis[0]>=0?1:-1;
    float flag2 = _axis[1]>=0?1:-1;
    float flag3 = _axis[2]>=0?1:-1;

    if(_jointType == Msnhnet::JOINT_ROT_AXIS)
    {
        setRotationApi(QVector3D({flag1*value[0], flag2*value[1], flag3*value[2]}));
    }
    else if(_jointType == Msnhnet::JOINT_TRANS_AXIS)
    {
        setPositionApi(QVector3D({flag1*value[0], flag2*value[1], flag3*value[2]}));
    }
}

float Model::getSingleMove() const
{
    return _singleMove;
}


double Model::getJointMin() const
{
    return _jointMin;
}


