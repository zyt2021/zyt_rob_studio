﻿#include <Core/MsnhModelExporter.h>

/* Assimp: 3D model export */
#include <assimp/Exporter.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define toAiString( str )	(aiString( (str).toStdString() ) )
#define toAiColor( color )	(aiColor3D( (color)[0], (color)[1], (color)[2] ) )
#define toAiVector3D( vec )	(aiVector3D( (vec)[0], (vec)[1], (vec)[2] ) )

ModelExporter::ModelExporter()
{
    //MSVC MD调试
    delete _aiScenePtr;
    _aiScenePtr = nullptr;
}


ModelExporter::~ModelExporter()
{
    _aiScenePtr->~aiScene();
}


void ModelExporter::saveToFile( Model* model, QString filePath )
{
    _model = model;

    _tmpAimeshes.clear();
    _tmpAimaterials.clear();
    _tmpTextures.clear();
    getAllTextures( _model );

    _aiScenePtr                         = new aiScene();
    _aiScenePtr->mRootNode		= exportModel( _model );
    _aiScenePtr->mNumMeshes		= _tmpAimeshes.size();
    _aiScenePtr->mMeshes		= new aiMesh*[_tmpAimeshes.size()]();
    _aiScenePtr->mNumMaterials          = _tmpAimaterials.size();
    _aiScenePtr->mMaterials		= new aiMaterial*[_tmpAimaterials.size()]();

    for ( int i = 0; i < _tmpAimeshes.size(); i++ )
        _aiScenePtr->mMeshes[i] = _tmpAimeshes[i];
    for ( int i = 0; i < _tmpAimaterials.size(); i++ )
        _aiScenePtr->mMaterials[i] = _tmpAimaterials[i];

    Assimp::Exporter exporter;
    if ( AI_SUCCESS != exporter.Export( _aiScenePtr,
                        QFileInfo( filePath.toLocal8Bit() ).suffix().toStdString(),
                        filePath.toLocal8Bit().toStdString() ) )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << exporter.GetErrorString();
        _log += exporter.GetErrorString();
        return;
    }

    for ( int i = 0; i < _tmpTextures.size(); i++ )
    {
        QString fileName;
        if ( _tmpTextures[i]->getTextureType() == Texture::DIFFUSE )
            fileName = "D_" + QString::number( (intptr_t) _tmpTextures[i].data() ) + ".png";
        else if ( _tmpTextures[i]->getTextureType() == Texture::SPECULAR )
            fileName = "S_" + QString::number( (intptr_t) _tmpTextures[i].data() ) + ".png";
        else if ( _tmpTextures[i]->getTextureType() == Texture::BUMP )
            fileName = "N_" + QString::number( (intptr_t) _tmpTextures[i].data() ) + ".png";
        _tmpTextures[i]->getImage().save( QFileInfo( filePath.toLocal8Bit() ).absoluteDir().absoluteFilePath( fileName ) );
    }
}


void ModelExporter::saveToFile( Mesh * mesh, QString filePath )
{
    _tmpAimeshes.clear();
    _tmpAimaterials.clear();
    _tmpTextures.clear();

    _aiScenePtr                             = new aiScene();

    _aiScenePtr->mRootNode                  = new aiNode();
    _aiScenePtr->mRootNode->mName           = toAiString( mesh->objectName() );
    _aiScenePtr->mRootNode->mNumMeshes      = 1;
    _aiScenePtr->mRootNode->mMeshes         = new uint32_t[1]();
    _aiScenePtr->mRootNode->mMeshes[0]      = 0;
    _aiScenePtr->mRootNode->mNumChildren    = (uint32_t) 0;
    _aiScenePtr->mRootNode->mChildren       = 0;

    _aiScenePtr->mNumMeshes                 = 1;
    _aiScenePtr->mMeshes                    = new aiMesh*[1]();
    _aiScenePtr->mMeshes[0]                 = exportMesh( mesh );
    _aiScenePtr->mNumMaterials              = 1;
    _aiScenePtr->mMaterials                 = new aiMaterial*[1]();
    _aiScenePtr->mMaterials[0]	= exportMaterial( mesh->getMaterial() );

    Assimp::Exporter exporter;
    if ( AI_SUCCESS != exporter.Export( _aiScenePtr,
                        QFileInfo( filePath.toLocal8Bit() ).suffix().toStdString(),
                        filePath.toLocal8Bit().toStdString() ) )
    {
        _log += exporter.GetErrorString();
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << exporter.GetErrorString();
        return;
    }

    if ( mesh->getMaterial() )
    {
        if ( Texture * texture = mesh->getMaterial()->getDiffuseTexture().data() )
        {
            QString fileName = "D_" + QString::number( (intptr_t) texture ) + ".png";
            texture->getImage().save( QFileInfo( filePath.toLocal8Bit()).absoluteDir().absoluteFilePath( fileName ) );
        }
        if ( Texture * texture = mesh->getMaterial()->getSpecularTexture().data() )
        {
            QString fileName = "S_" + QString::number( (intptr_t) texture ) + ".png";
            texture->getImage().save( QFileInfo( filePath.toLocal8Bit() ).absoluteDir().absoluteFilePath( fileName ) );
        }
        if ( Texture * texture = mesh->getMaterial()->getBumpTexture().data() )
        {
            QString fileName = "N_" + QString::number( (intptr_t) texture ) + ".png";
            texture->getImage().save( QFileInfo( filePath.toLocal8Bit() ).absoluteDir().absoluteFilePath( fileName ) );
        }
    }
}


bool ModelExporter::hasErrorLog()
{
    return(_log != "");
}


QString ModelExporter::errorLog()
{
    QString tmp = _log;
    _log = "";
    return(tmp);
}


void ModelExporter::getAllTextures( Model * model )
{
    for ( int i = 0; i < model->getChildMeshes().size(); i++ )
    {
        if ( model->getChildMeshes()[i]->getMaterial() )
        {
            Material * material = model->getChildMeshes()[i]->getMaterial();
            if ( material == 0 )
                continue;
            if ( !material->getDiffuseTexture().isNull() && !_tmpTextures.contains( material->getDiffuseTexture() ) )
                _tmpTextures.push_back( material->getDiffuseTexture() );
            if ( !material->getSpecularTexture().isNull() && !_tmpTextures.contains( material->getSpecularTexture() ) )
                _tmpTextures.push_back( material->getSpecularTexture() );
            if ( !material->getBumpTexture().isNull() && !_tmpTextures.contains( material->getBumpTexture() ) )
                _tmpTextures.push_back( material->getBumpTexture() );
        }
    }

    for ( int i = 0; i < model->getChildModels().size(); i++ )
        getAllTextures( model->getChildModels()[i] );
}


aiNode * ModelExporter::exportModel( Model * model )
{
    aiNode* aiNodePtr = new aiNode(); //自动删除子项
    aiNodePtr->mName = toAiString( model->objectName() );

    aiNodePtr->mNumMeshes	= (uint32_t) model->getChildMeshes().size();
    aiNodePtr->mMeshes          = new uint32_t[aiNodePtr->mNumMeshes]();

    for ( int i = 0; i < model->getChildMeshes().size(); i++ )
    {
        aiNodePtr->mMeshes[i] = _tmpAimeshes.size();
        _tmpAimeshes.push_back( exportMesh( model->getChildMeshes()[i] ) );
    }

    aiNodePtr->mNumChildren     = (uint32_t) model->getChildModels().size();
    aiNodePtr->mChildren	= new aiNode*[aiNodePtr->mNumChildren]();

    for ( int i = 0; i < model->getChildModels().size(); i++ )
        aiNodePtr->mChildren[i] = exportModel( model->getChildModels()[i] );

    return(aiNodePtr);
}


aiMesh * ModelExporter::exportMesh( Mesh * mesh )
{
    aiMesh* aiMeshPtr = new aiMesh(); //自动删除子项

    aiMeshPtr->mNumVertices		= (uint32_t) mesh->getVertices().size();
    aiMeshPtr->mVertices		= new aiVector3D[aiMeshPtr->mNumVertices]();
    aiMeshPtr->mNormals                 = new aiVector3D[aiMeshPtr->mNumVertices]();
    aiMeshPtr->mTextureCoords[0]	= new aiVector3D[aiMeshPtr->mNumVertices]();

    for ( uint32_t i = 0; i < aiMeshPtr->mNumVertices; i++ )
    {
        Vertex vertex = mesh->getGlobalModelMatrix() * mesh->getVertices()[i];
        aiMeshPtr->mVertices[i]		= toAiVector3D( vertex.position );
        aiMeshPtr->mNormals[i]		= toAiVector3D( vertex.normal );
        aiMeshPtr->mTextureCoords[0][i] = toAiVector3D( QVector3D( vertex.texCoords ) );
    }

    int indicesEachFace = 0;
    if ( mesh->getMeshType() == Mesh::TRIANGLE )
        indicesEachFace = 3;
    else if ( mesh->getMeshType() == Mesh::LINE )
        indicesEachFace = 2;
    else
        indicesEachFace = 1;

    aiMeshPtr->mNumFaces	= (uint32_t) mesh->getIndices().size() / indicesEachFace;
    aiMeshPtr->mFaces	= new aiFace[aiMeshPtr->mNumFaces]();

    for ( uint32_t i = 0; i < aiMeshPtr->mNumFaces; i++ )
    {
        aiFace & face = aiMeshPtr->mFaces[i];
        face.mNumIndices	= indicesEachFace;
        face.mIndices		= new uint32_t[indicesEachFace]();
        for ( int j = 0; j < indicesEachFace; j++ )
            face.mIndices[j] = mesh->getIndices()[i * indicesEachFace + j];
    }

    aiMeshPtr->mMaterialIndex = _tmpAimaterials.size();
    _tmpAimaterials.push_back( exportMaterial( mesh->getMaterial() ) );
    return(aiMeshPtr);
}


aiMaterial * ModelExporter::exportMaterial( Material * material )
{
    aiMaterial* aiMaterialPtr = new aiMaterial();
    if ( material == 0 )
        return(aiMaterialPtr);

    aiString	aiName		= toAiString( material->objectName() );
    aiColor3D	ambientColor	= toAiColor( material->getAmbient() * material->getColor() );
    aiColor3D	diffuseColor	= toAiColor( material->getDiffuse() * material->getColor() );
    aiColor3D	specularColor	= toAiColor( material->getSpecular() * material->getColor() );
    float	shininess	= material->getShininess();

    aiMaterialPtr->AddProperty( &aiName, AI_MATKEY_NAME );
    aiMaterialPtr->AddProperty( &ambientColor, 1, AI_MATKEY_COLOR_AMBIENT );
    aiMaterialPtr->AddProperty( &diffuseColor, 1, AI_MATKEY_COLOR_DIFFUSE );
    aiMaterialPtr->AddProperty( &specularColor, 1, AI_MATKEY_COLOR_SPECULAR );
    aiMaterialPtr->AddProperty( &shininess, 1, AI_MATKEY_SHININESS );

    const int uvwIndex = 0;
    if ( material->getDiffuseTexture() )
    {
        QString		filePath	= "D_" + QString::number( (intptr_t) material->getDiffuseTexture().data() ) + ".png";
        aiString	aiFilePath	= toAiString( filePath );
        aiMaterialPtr->AddProperty( &aiFilePath, AI_MATKEY_TEXTURE_DIFFUSE( 0 ) );
        aiMaterialPtr->AddProperty( &uvwIndex, 1, AI_MATKEY_UVWSRC_DIFFUSE( 0 ) );
    }
    if ( material->getSpecularTexture() )
    {
        QString		filePath	= "S_" + QString::number( (intptr_t) material->getSpecularTexture().data() ) + ".png";
        aiString	aiFilePath	= toAiString( filePath );
        aiMaterialPtr->AddProperty( &aiFilePath, AI_MATKEY_TEXTURE_SPECULAR( 0 ) );
        aiMaterialPtr->AddProperty( &uvwIndex, 1, AI_MATKEY_UVWSRC_SPECULAR( 0 ) );
    }
    if ( material->getBumpTexture() )
    {
        QString		filePath	= "N_" + QString::number( (intptr_t) material->getBumpTexture().data() ) + ".png";
        aiString	aiFilePath	= toAiString( filePath );
        aiMaterialPtr->AddProperty( &aiFilePath, AI_MATKEY_TEXTURE_HEIGHT( 0 ) );
        aiMaterialPtr->AddProperty( &uvwIndex, 1, AI_MATKEY_UVWSRC_HEIGHT( 0 ) );
    }

    return(aiMaterialPtr);
}


