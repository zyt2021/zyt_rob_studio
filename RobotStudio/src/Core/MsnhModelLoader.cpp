﻿#include <Core/MsnhModelLoader.h>

/* Assimp: 3D model loader */
#include <assimp/Importer.hpp>
#include <assimp/importerdesc.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

ModelLoader::ModelLoader(QObject *parent):QObject(parent)
{
    _aiScenePtr = nullptr;
}


Model * ModelLoader::loadModelFromFile( QString filePath )
{
    QFile file(filePath);

    if(!file.exists())
    {
        _log += "Filepath is empty.";
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to load model: filepath is empty";
        return (nullptr);
    }

    if ( filePath.length() == 0 )
    {
        _log += "Filepath is empty.";
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to load model: filepath is empty";
        return (nullptr);
    }

    QStringList split = filePath.split(".");

    if(QString(split[split.length()-1]).toLower() == "dae" ||QString(split[split.length()-1]).toLower() == "stl")
    {
        _needRotate = true;
    }

    _scale = 1.f;

    if(QString(split[split.length()-1]).toLower() == "dae")
    {
        _scale = getMeshUnitRescale(filePath.toLocal8Bit().toStdString());
    }


    Assimp::Importer	importer;
    unsigned int        flags =
            aiProcess_Triangulate |
            aiProcess_CalcTangentSpace |
            aiProcess_GenSmoothNormals |
            aiProcess_JoinIdenticalVertices |
            aiProcess_OptimizeGraph |
            aiProcess_GenUVCoords;

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Loading" << filePath;

    if ( filePath[0] == ':' ) /* qrc */
    {
        QFile file( filePath );
        if ( !file.open( QIODevice::ReadOnly ) )
        {
            if ( logLevel >= LOG_LEVEL_ERROR )
                dout << "FATAL: failed to open internal file" << filePath;
            exit( -1 );
        }

        QByteArray bytes = file.readAll();
        _aiScenePtr = importer.ReadFileFromMemory( bytes.constData(), bytes.length(), flags );
    }
    else
    {
        _dir		= QFileInfo( filePath ).absoluteDir();
        _aiScenePtr	= importer.ReadFile( filePath.toStdString(), flags );
    }

    if ( !_aiScenePtr || !_aiScenePtr->mRootNode || _aiScenePtr->mFlags == AI_SCENE_FLAGS_INCOMPLETE )
    {
        _log += importer.GetErrorString();
        if ( logLevel >= LOG_LEVEL_ERROR )
        {
            dout << importer.GetErrorString();
        }
        return (nullptr);
    }

    Model* model = loadModel( _aiScenePtr->mRootNode );
    model->setObjectName( QFileInfo( filePath ).baseName() );
    return(model);
}


Mesh * ModelLoader::loadMeshFromFile( QString filePath )
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    Model	* model         = loadModelFromFile( filePath );

    if(!model)
    {
        throw Msnhnet::Exception(1,"Open model failed! path: " + filePath.toStdString(), __FILE__, __LINE__, __FUNCTION__);
    }

    Mesh	* assembledMesh = model->assemble();
    delete model;
    return(assembledMesh);
}

Model * ModelLoader::loadConeModel()
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    ModelLoader	loader;
    Model	* model		= loader.loadModelFromFile( ":/shapes/Cone.obj" ); //走childEvent
    return(model);
}


Model * ModelLoader::loadCubeModel()
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    ModelLoader	loader;
    Model	* model		= loader.loadModelFromFile( ":/shapes/Cube.obj" ); //走childEvent
    return(model);
}


Model * ModelLoader::loadCylinderModel()
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    ModelLoader	loader;
    Model	* model		= loader.loadModelFromFile( ":/shapes/Cylinder.obj" ); //走childEvent
    return(model);
}


Model * ModelLoader::loadPlaneModel()
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    ModelLoader	loader;
    Model	* model		= loader.loadModelFromFile( ":/shapes/Plane.obj" ); //走childEvent
    return(model);
}


Model * ModelLoader::loadSphereModel()
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    ModelLoader	loader;
    Model	* model		= loader.loadModelFromFile( ":/shapes/Sphere.obj" ); //走childEvent
    return(model);
}

Model *ModelLoader::loadTorusModel()
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    ModelLoader	loader;
    Model	* model		= loader.loadModelFromFile( ":/shapes/Torus.obj" ); //走childEvent
    return(model);
}

Model *ModelLoader::loadTransformAxises()
{
    Model	* model = new Model();
    ModelLoader loader;
    auto _xAxis = loader.loadMeshFromFile( ":/shapes/AxesX.obj" );
    auto _yAxis = loader.loadMeshFromFile( ":/shapes/AxesY.obj" );
    auto _zAxis = loader.loadMeshFromFile( ":/shapes/AxesZ.obj" );

    model->addChildMesh(_xAxis);
    model->addChildMesh(_yAxis);
    model->addChildMesh(_zAxis);
    return model;
}


bool ModelLoader::hasErrorLog()
{
    return(_log.length() != 0 || textureLoader.hasErrorLog() );
}


QString ModelLoader::errorLog()
{
    QString tmp = _log + textureLoader.errorLog();
    _log = "";
    return(tmp);
}

#ifdef WIN32
std::wstring ToUtf16(std::string str)
{
    std::wstring ret;
    int len = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.length(), NULL, 0);
    if (len > 0)
    {
        ret.resize(len);
        MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.length(), &ret[0], len);
    }
    return ret;
}
#endif

float ModelLoader::getMeshUnitRescale(const std::string &srcPath)
{
    float unitScale(1.0);

    tinyxml2::XMLDocument xmlDoc;

    std::string path = srcPath;
#ifdef WIN32
    std::ifstream modelFile(ToUtf16(path.c_str()));
#else
    std::ifstream modelFile(path.c_str());
#endif

    if (!modelFile)
    {
        std::string errMsg = "Error opening file <" + path + ">.";
        _log += QString::fromStdString(errMsg);
        return 1;
    }

    std::string modelXmlString;
    modelFile.seekg(0, std::ios::end);
    modelXmlString.reserve(modelFile.tellg());
    modelFile.seekg(0, std::ios::beg);
    modelXmlString.assign((std::istreambuf_iterator<char>(modelFile)), std::istreambuf_iterator<char>());
    modelFile.close();

    xmlDoc.Parse(modelXmlString.c_str());

    if (xmlDoc.Error())
    {
        std::string errMsg = xmlDoc.ErrorStr();
        _log += QString::fromStdString(errMsg);
        return 1;
    }

    auto meter = xmlDoc.FirstChildElement("COLLADA")->FirstChildElement("asset")->FirstChildElement("unit")->Attribute("meter");

    auto scale = xmlDoc.FirstChildElement("COLLADA")->FirstChildElement("library_visual_scenes")->FirstChildElement("visual_scene")->Attribute("scale");

    if(meter != nullptr)
    {
        unitScale = std::stof(meter);
    }

    if(scale != nullptr)
    {
        unitScale = unitScale*std::stof(scale);
    }

    //    const XNode* root = XFILE(resource_path.data()).getRoot();

    //    try
    //    {
    //        float scale = std::stof(std::string(root->getNode("COLLADA").getNode("asset").getChild("unit").getAttribute("meter").value));
    //        unitScale  = scale;
    //        std::cout<<"|-------"<<scale<<std::endl;
    //    }
    //    catch (Exception&)
    //    {

    //    }

    //    try
    //    {
    //        std::vector<float> scale = root->getNode("COLLADA").getNode("library_visual_scenes").getNode("visual_scene").getNode("scale").value.toFloatVector();

    //        if(scale.size()>1)
    //        {
    //            unitScale = unitScale*scale[0];
    //        }
    //        std::cout<<"-------|"<<unitScale<<std::endl;
    //    }
    //    catch (Exception&)
    //    {
    //    }

    return unitScale;
}


Model * ModelLoader::loadModel( const aiNode * aiNodePtr )
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    Model* model = new Model();
    model->setObjectName( aiNodePtr->mName.length ? aiNodePtr->mName.C_Str() : "Untitled" );
    for ( uint32_t i = 0; i < aiNodePtr->mNumMeshes; i++ )
    {
        model->addChildMesh( loadMesh( _aiScenePtr->mMeshes[aiNodePtr->mMeshes[i]] ) );
    }

    for ( uint32_t i = 0; i < aiNodePtr->mNumChildren; i++ )
    {
        model->addChildModel( loadModel( aiNodePtr->mChildren[i] ) );
    }


    //    QVector3D center = model->getCenterOfMass();

    //    for ( int i = 0; i < model->getChildMeshes().size(); i++ )
    //    {
    //        model->getChildMeshes()[i]->translate( -center );
    //    }

    //    for ( int i = 0; i < model->getChildModels().size(); i++ )
    //    {
    //        model->getChildModels()[i]->translate( -center );
    //    }

    //    model->translate( center );

    return(model);
}


Mesh * ModelLoader::loadMesh( const aiMesh * aiMeshPtr )
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    Mesh* mesh = new Mesh();
    mesh->setObjectName( aiMeshPtr->mName.length ? aiMeshPtr->mName.C_Str() : "Untitled" );

    if(_needRotate)
    {
        for ( uint32_t i = 0; i < aiMeshPtr->mNumVertices; i++ )
        {
            Vertex vertex;
            if ( aiMeshPtr->HasPositions() )
            {
                vertex.position = QVector3D( aiMeshPtr->mVertices[i].y*_scale, aiMeshPtr->mVertices[i].z*_scale, aiMeshPtr->mVertices[i].x*_scale );
            }

            if ( aiMeshPtr->HasNormals() )
            {
                vertex.normal = QVector3D( aiMeshPtr->mNormals[i].y, aiMeshPtr->mNormals[i].z, aiMeshPtr->mNormals[i].x );
            }

            if ( aiMeshPtr->HasTangentsAndBitangents() )
            {
                /* Use left-handed tangent space */
                vertex.tangent		= QVector3D( aiMeshPtr->mTangents[i].y, aiMeshPtr->mTangents[i].z, aiMeshPtr->mTangents[i].x );
                vertex.bitangent        = QVector3D( aiMeshPtr->mBitangents[i].y, aiMeshPtr->mBitangents[i].z, aiMeshPtr->mBitangents[i].x );

                /* Gram-Schmidt process, re-orthogonalize the TBN vectors */
                vertex.tangent -= QVector3D::dotProduct( vertex.tangent, vertex.normal ) * vertex.normal;
                vertex.tangent.normalize();

                /* Deal with mirrored texture coordinates */
                if ( QVector3D::dotProduct( QVector3D::crossProduct( vertex.tangent, vertex.normal ), vertex.bitangent ) < 0.0f )
                    vertex.tangent = -vertex.tangent;
            }
            if ( aiMeshPtr->HasTextureCoords( 0 ) )
                vertex.texCoords = QVector2D( aiMeshPtr->mTextureCoords[0][i].x, aiMeshPtr->mTextureCoords[0][i].y );


            mesh->_vertices.push_back( vertex );
        }
    }
    else
    {
        for ( uint32_t i = 0; i < aiMeshPtr->mNumVertices; i++ )
        {
            Vertex vertex;
            if ( aiMeshPtr->HasPositions() )
            {
                vertex.position = QVector3D( aiMeshPtr->mVertices[i].x*_scale, aiMeshPtr->mVertices[i].y*_scale, aiMeshPtr->mVertices[i].z*_scale );
            }

            if ( aiMeshPtr->HasNormals() )
            {
                vertex.normal = QVector3D( aiMeshPtr->mNormals[i].x, aiMeshPtr->mNormals[i].y, aiMeshPtr->mNormals[i].z );
            }

            if ( aiMeshPtr->HasTangentsAndBitangents() )
            {
                /* Use left-handed tangent space */
                vertex.tangent		= QVector3D( aiMeshPtr->mTangents[i].x, aiMeshPtr->mTangents[i].y, aiMeshPtr->mTangents[i].z );
                vertex.bitangent            = QVector3D( aiMeshPtr->mBitangents[i].x, aiMeshPtr->mBitangents[i].y, aiMeshPtr->mBitangents[i].z );

                /* Gram-Schmidt process, re-orthogonalize the TBN vectors */
                vertex.tangent -= QVector3D::dotProduct( vertex.tangent, vertex.normal ) * vertex.normal;
                vertex.tangent.normalize();

                /* Deal with mirrored texture coordinates */
                if ( QVector3D::dotProduct( QVector3D::crossProduct( vertex.tangent, vertex.normal ), vertex.bitangent ) < 0.0f )
                    vertex.tangent = -vertex.tangent;
            }
            if ( aiMeshPtr->HasTextureCoords( 0 ) )
                vertex.texCoords = QVector2D( aiMeshPtr->mTextureCoords[0][i].x, aiMeshPtr->mTextureCoords[0][i].y );

            mesh->_vertices.push_back( vertex );
        }
    }

    for ( uint32_t i = 0; i < aiMeshPtr->mNumFaces; i++ )
    {
        for ( uint32_t j = 0; j < 3; j++ )
        {
            mesh->_indices.push_back( aiMeshPtr->mFaces[i].mIndices[j] );
        }
    }

    //QVector3D center = mesh->getCenterOfMass();

    //    for ( int i = 0; i < mesh->_vertices.size(); i++ )
    //        mesh->_vertices[i].position -= center;

    //    mesh->_position = center;

    mesh->setMaterial( loadMaterial( _aiScenePtr->mMaterials[aiMeshPtr->mMaterialIndex] ) );

    return (mesh);
}

Material * ModelLoader::loadMaterial( const aiMaterial * aiMaterialPtr )
{
    //触发childEvent, 调用setParent, 不用担心资源释放问题
    Material	* material = new Material();
    aiColor4D	color;
    float       value   =   0;
    aiString    aiStr;

    if ( AI_SUCCESS == aiMaterialPtr->Get( AI_MATKEY_NAME, aiStr ) )
        material->setObjectName( aiStr.length ? aiStr.C_Str() : "Untitled" );
    if ( AI_SUCCESS == aiMaterialPtr->Get( AI_MATKEY_COLOR_AMBIENT, color ) )
        material->setAmbient( (color.r + color.g + color.b) / 3.0f );
    if ( AI_SUCCESS == aiMaterialPtr->Get( AI_MATKEY_COLOR_DIFFUSE, color ) )
    {
        material->setDiffuse( (color.r + color.g + color.b) / 3.0f );
        material->setColor( QVector3D( color.r, color.g, color.b ) / material->getDiffuse() );
    }
    if ( AI_SUCCESS == aiMaterialPtr->Get( AI_MATKEY_COLOR_SPECULAR, color ) )
        material->setSpecular( (color.r + color.g + color.b) / 3.0f );
    if ( AI_SUCCESS == aiMaterialPtr->Get( AI_MATKEY_SHININESS, value ) && !qFuzzyIsNull( value ) )
        material->setShininess( value );
    if ( AI_SUCCESS == aiMaterialPtr->GetTexture( aiTextureType_DIFFUSE, 0, &aiStr ) )
    {
        QString filePath = _dir.absolutePath() + '/' + QString( aiStr.C_Str() ).replace( '\\', '/' );
        material->setDiffuseTexture( textureLoader.loadFromFile( Texture::DIFFUSE, filePath ) );
    }
    if ( AI_SUCCESS == aiMaterialPtr->GetTexture( aiTextureType_SPECULAR, 0, &aiStr ) )
    {
        QString filePath = _dir.absolutePath() + '/' + QString( aiStr.C_Str() ).replace( '\\', '/' );
        material->setSpecularTexture( textureLoader.loadFromFile( Texture::SPECULAR, filePath ) );
    }
    if ( AI_SUCCESS == aiMaterialPtr->GetTexture( aiTextureType_HEIGHT, 0, &aiStr ) )
    {
        QString filePath = _dir.absolutePath() + '/' + QString( aiStr.C_Str() ).replace( '\\', '/' );
        material->setBumpTexture( textureLoader.loadFromFile( Texture::BUMP, filePath ) );
    }
    return (material);
}


