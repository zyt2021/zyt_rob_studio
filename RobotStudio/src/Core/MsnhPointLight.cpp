﻿#include <Core/MsnhPointLight.h>
#include <Core/MsnhModelLoader.h>

PointLight::PointLight( QObject * parent ) : AbstractLight( parent )
{
    _enableAttenuation          = false;
    _attenuationQuadratic       = 0.0007f;
    _attenuationLinear          = 0.014f;
    _attenuationConstant        = 1.0f;

    initMarker();
    setObjectName( "Untitled Point Light" );
}


PointLight::PointLight( QVector3D color, QVector3D position, QObject * parent ) : AbstractLight( color, parent )
{
    _position                   = position;
    _enableAttenuation          = false;
    _attenuationQuadratic       = 0.0007f;
    _attenuationLinear          = 0.014f;
    _attenuationConstant        = 1.0f;

    initMarker();
    setObjectName( "Untitled Point Light" );
}


PointLight::PointLight(const PointLight & light , QObject *parent) : AbstractLight( light, parent )
{
    _position                   = light._position;
    _enableAttenuation          = light._enableAttenuation;
    _attenuationQuadratic       = light._attenuationQuadratic;
    _attenuationLinear          = light._attenuationLinear;
    _attenuationConstant        = light._attenuationConstant;

    initMarker();
    setObjectName( light.objectName() );
}


PointLight::~PointLight()
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    delete _marker;

    logLevel = tmp_log_level;

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Point light" << objectName() << "is destroyed";
}


void PointLight::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Point Light: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "Enabled: " << _enabled;
    qDebug().nospace() << tab( l + 1 ) << "Color: " << _color;
    qDebug().nospace() << tab( l + 1 ) << "Position: " << _position;
    qDebug().nospace() << tab( l + 1 ) << "Intensity: " << _intensity;
    qDebug().nospace() << tab( l + 1 ) << "Enable attenuation: " << _enableAttenuation;
    if ( _enableAttenuation )
    {
        qDebug().nospace() << tab( l + 2 ) << "Quadratic value: " << _attenuationQuadratic;
        qDebug().nospace() << tab( l + 2 ) << "Linear value:    " << _attenuationLinear;
        qDebug().nospace() << tab( l + 2 ) << "Constant value:  " << _attenuationConstant;
    }
}


void PointLight::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
}


bool PointLight::getVisible() const
{
    return(_marker->getVisible() );
}


QVector3D PointLight::getPosition() const
{
    return(_position);
}

QVector3D PointLight::getPositionApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getPosition();
    }
    else
    {
        auto position = getPosition();
        return QVector3D(position.z(),position.x(),position.y());
    }
}


bool PointLight::getEnableAttenuation() const
{
    return(_enableAttenuation);
}


QVector3D PointLight::getAttenuationArguments() const
{
    return(QVector3D( _attenuationQuadratic, _attenuationLinear, _attenuationConstant ) );
}


float PointLight::getAttenuationQuadratic() const
{
    return(_attenuationQuadratic);
}


float PointLight::getAttenuationLinear() const
{
    return(_attenuationLinear);
}


float PointLight::getAttenuationConstant() const
{
    return(_attenuationConstant);
}


Mesh * PointLight::getMarker() const
{
    return(_marker);
}


void PointLight::setColor( QVector3D color )
{
    AbstractLight::setColor( color );
    _marker->getMaterial()->setColor( color );
}


void PointLight::setVisible( bool visible )
{
    _marker->setVisible( visible );
}


void PointLight::setPosition( QVector3D position )
{
    if ( !isEqual( _position, position ) )
    {
        _position = position;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The position of" << this->objectName() << "is set to" << position;
        _marker->setPosition( position );
        positionChanged( _position );
    }
}

void PointLight::setPositionApi(QVector3D position)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setPosition(position);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        setPosition(QVector3D(position.y(),position.z(),position.x()));
    }
}


void PointLight::setEnabled( bool enabled )
{
    AbstractLight::setEnabled( enabled );
    _marker->setVisible( enabled );
}


void PointLight::setEnableAttenuation( bool enabled )
{
    if ( _enableAttenuation != enabled )
    {
        _enableAttenuation = enabled;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The attenuation of" << this->objectName()
                << "is" << (enabled ? "enabled" : "disabled");
        enableAttenuationChanged( _enableAttenuation );
    }
}


void PointLight::setAttenuationArguments( QVector3D value )
{
    setAttenuationQuadratic( value[0] );
    setAttenuationLinear( value[1] );
    setAttenuationConstant( value[2] );
}


void PointLight::setAttenuationQuadratic( float value )
{
    if ( !isEqual( _attenuationQuadratic, value ) )
    {
        _attenuationQuadratic = value;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The quadratic attenuation arg of" << this->objectName()
                << "is set to" << value;
        attenuationQuadraticChanged( _attenuationQuadratic );
        attenuationArgumentsChanged( this->getAttenuationArguments() );
    }
}


void PointLight::setAttenuationLinear( float value )
{
    if ( !isEqual( _attenuationLinear, value ) )
    {
        _attenuationLinear = value;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The linear attenuation arg of" << this->objectName()
                << "is set to" << value;
        attenuationLinearChanged( _attenuationLinear );
        attenuationArgumentsChanged( this->getAttenuationArguments() );
    }
}


void PointLight::setAttenuationConstant( float value )
{
    if ( !isEqual( _attenuationConstant, value ) )
    {
        _attenuationConstant = value;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The constant attenuation arg of" << this->objectName()
                << "is set to" << value;
        attenuationConstantChanged( _attenuationConstant );
        attenuationArgumentsChanged( this->getAttenuationArguments() );
    }
}


void PointLight::initMarker()
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    ModelLoader loader;

    _marker = loader.loadMeshFromFile( ":/shapes/PointLight.obj" );
    _marker->setPosition( this->getPosition() );
    _marker->getMaterial()->setColor( this->getColor() );
    _marker->setObjectName( "Point Light Marker" );
    _marker->setParent( this );

    logLevel = tmp_log_level;

    connect( _marker, SIGNAL( visibleChanged( bool ) ), this, SIGNAL( visibleChanged( bool ) ) );
    connect( _marker, SIGNAL( positionChanged( QVector3D ) ), this, SLOT( setPosition( QVector3D ) ) );
}


