﻿#include <Core/MsnhPointsCloud.h>
#include <Core/MsnhPointsCloudIO.h>
#include <MsnhProto/Protocal/cpp/PointsCloud.h>

PointsCloud::PointsCloud(QObject *parent):QObject(parent)
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    _marker = new Mesh( Mesh::MeshType::POINT, true, this );
    _marker->setObjectName( "Point Clouds Marker" );//marker parent是point clouds
    _marker->setMaterial( new Material(_marker) );
    _marker->getMaterial()->setColor(QVector3D(0,0,0));
    setObjectName( "Untitled Point Clouds" );
    logLevel = tmp_log_level;
}

PointsCloud::PointsCloud(const PointsCloud &pointClouds, QObject *parent): QObject(parent)
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;
    _marker     = new Mesh(*pointClouds._marker,this); //marker parent是point clouds
    _pureColor  = pointClouds._pureColor;
    setObjectName(pointClouds.objectName());
    logLevel = tmp_log_level;
}

PointsCloud::PointsCloud(Mesh *mesh, QObject *parent):QObject(parent)
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;
    _marker = mesh;
    _marker->setParent(this);                        //marker parent是point clouds
    setObjectName( "Untitled Point Clouds" );
    logLevel = tmp_log_level;
}


PointsCloud::~PointsCloud()
{
}

void PointsCloud::setColorVertex(const QVector<Vertex> &vertices, const QVector<uint32_t> &indices)
{
    _marker->setGeometry(vertices, indices);
}

bool PointsCloud::isPureColor() const
{
    return _pureColor;
}

void PointsCloud::setPureColor(bool newPureColor)
{
    if(_pureColor != newPureColor)
    {
        _pureColor = newPureColor;
        emit pureColorChanged(_pureColor);
    }
}

const QVector3D &PointsCloud::getColor() const
{
    return _color;
}

void PointsCloud::setColor(const QVector3D &color)
{
    if(!isEqual(_color, color))
    {
        _color = color;
        _marker->getMaterial()->setColor(color);
        emit colorChanged(_color);
    }
}

bool PointsCloud::getVisible() const
{
    return _marker->getVisible();
}

void PointsCloud::setVisible(bool visible)
{
    if(getVisible()!=visible)
    {
        _marker->setVisible(visible);
        emit visiableChanged(visible);
    }
}

Mesh *PointsCloud::getMarker() const
{
    return _marker;
}

void PointsCloud::init()
{
    _subNN = new NNThread(this);
    _subNN->setAddr("ipc:///tmp/"+ objectName() + "_sub");
    connect(_subNN, &NNThread::revMsg, this, &PointsCloud::revPointClouds);
    _subNN->startIt();
}

QString PointsCloud::getSubAddr() const
{
    return _subNN->getAddr();
}

void PointsCloud::revPointClouds(const std::vector<uint8_t> &data)
{
    MsnhProtocal::Base base;
    base.fromBytes(data);
    MsnhProtocal::PointsCloud* points = base.toPointsCloud();
    if(points )
    {
        QVector<Vertex> verts;
        QVector<uint32_t> indices;
        std::vector<float> x = points->getDataX();
        std::vector<float> y = points->getDataY();
        std::vector<float> z = points->getDataZ();

        std::vector<float> r = points->getDataR();
        std::vector<float> g = points->getDataG();
        std::vector<float> b = points->getDataB();

        if(x.size()!=y.size() || y.size()!=z.size() || x.size()!=z.size())
        {
            return;
        }


        if(r.size()==g.size() && g.size()==b.size() && b.size()==r.size() && r.size() == x.size())
        {

            qDebug()<<1;
            for (int i = 0; i < x.size(); ++i)
            {
                Vertex vert;
                vert.position = QVector3D(x[i],y[i],z[i]);
                vert.normal   = QVector3D(r[i],g[i],b[i]);
                verts.push_back(vert);
                indices.push_back(i);
            }
            setColorVertex(verts,indices);
        }
        else
        {
            for (int i = 0; i < x.size(); ++i)
            {
                Vertex vert;
                vert.position = QVector3D(x[i],y[i],z[i]);
                vert.normal   = QVector3D(1,1,1);
                verts.push_back(vert);
                indices.push_back(i);
            }

            setColorVertex(verts,indices);
        }
    }
}

