﻿
#include <Core/MsnhPointsCloudIO.h>
#define MSH_PLY_IMPLEMENTATION
#define MSH_PLY_INCLUDE_HEADERS
#include "ply.h"

typedef struct Vec3f
{
    float x,y,z;
} Vec3f;

typedef struct Vec6f
{
    float x,y,z,r,g,b;
} Vec6f;

typedef struct Vec3i
{
    int x,y,z;
} Vec3i;

Mesh* PointsCloudIO::readPly(const QString &path)
{
    Mesh *mesh;

    err = "";

    try
    {
        happly::PLYData plyIn(path.toLocal8Bit().toStdString().c_str());

        // Get mesh-style data from the object
        std::vector<std::array<double, 3>> vPos = plyIn.getVertexPositions();
        //std::vector<std::vector<size_t>> fInd = plyIn.getFaceIndices<size_t>();

        mesh = new Mesh(Mesh::POINT,true);
        mesh->setMaterial( new Material(mesh));
        mesh->getMaterial()->setColor(QVector3D(0,0,0));

        for (int i = 0; i < vPos.size(); ++i)
        {
            Vertex vert;
            vert.position = {(float)vPos[i][0],(float)vPos[i][1],(float)vPos[i][2],};
            vert.normal   = {1,1,1};
            mesh->_vertices.push_back(vert);
            mesh->_indices.push_back(i);
        }

    //    for (int i = 0; i < fInd.size(); ++i)
    //    {
    //        for (int j = 0; j < fInd[i].size(); ++j)
    //        {
    //            mesh->_indices.push_back(fInd[i][j]);
    //        }
    //    }
    }
    catch (std::runtime_error ex)
    {
        err = QString(ex.what());
        return nullptr;
    }

    return mesh;
}

Mesh *PointsCloudIO::readXYZ(const QString &path)
{
    QFile file(path);
    Mesh *mesh;

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        err = "Can't open the file!";
        return nullptr;
    }
    std::vector<Vec3f> vec;

    while(!file.atEnd())
    {
        QByteArray line = file.readLine();
        QString str(line);

        str = str.replace(QRegExp("[\\s]+"), " ");
        QStringList list = str.trimmed().split(" ");

        if(list.length()!=3)
        {
            err = "Format err!";
            return nullptr;
        }

        Vec3f v3;
        v3.x = QString(list[0]).toFloat();
        v3.y = QString(list[1]).toFloat();
        v3.z = QString(list[2]).toFloat();
        vec.push_back(v3);
    }

    mesh = new Mesh(Mesh::POINT,true);
    mesh->setMaterial( new Material(mesh));
    mesh->getMaterial()->setColor(QVector3D(0,0,0));

    for (int i = 0; i < vec.size(); ++i)
    {
        Vertex vert;
        vert.position = {vec[i].x,vec[i].y,vec[i].z};
        vert.normal   = {1,1,1};
        mesh->_vertices.push_back(vert);
        mesh->_indices.push_back(i);
    }
    return mesh;
}

Mesh *PointsCloudIO::readXYZRGB(const QString &path)
{
    QFile file(path);
    Mesh *mesh;

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        err = "Can't open the file!";
        return nullptr;
    }
    std::vector<Vec6f> vec;

    while(!file.atEnd())
    {
        QByteArray line = file.readLine();
        QString str(line);

        str = str.replace(QRegExp("[\\s]+"), " ");

        QStringList list = str.trimmed().split(" ");
        if(list.length()!=6)
        {
            err = "Format err!";
            return nullptr;
        }

        Vec6f v6;
        v6.x = QString(list[0]).toFloat();
        v6.y = QString(list[1]).toFloat();
        v6.z = QString(list[2]).toFloat();
        v6.r = QString(list[3]).toFloat();
        v6.g = QString(list[4]).toFloat();
        v6.b = QString(list[5]).toFloat();

        vec.push_back(v6);
    }

    mesh = new Mesh(Mesh::POINT,true);
    mesh->setMaterial( new Material(mesh) );
    mesh->getMaterial()->setColor(QVector3D(0,0,0));

    for (int i = 0; i < vec.size(); ++i)
    {
        Vertex vert;
        vert.position = {vec[i].x,vec[i].y,vec[i].z};
        vert.normal   = {vec[i].r,vec[i].g,vec[i].b};
        mesh->_vertices.push_back(vert);
        mesh->_indices.push_back(i);
    }
    return mesh;
}

PointsCloud *PointsCloudIO::readFromFile(const QString &path)
{
    QStringList pathList = path.split(".");
    QString end = QString(pathList[pathList.length()-1]).toLower();

    Mesh* mesh = nullptr;

    if(end=="ply")
    {
        mesh = readPly(path);
    }
    else if(end=="xyz")
    {
        mesh = readXYZ(path);
    }
    else if(end=="xyzrgb")
    {
        mesh = readXYZRGB(path);
    }

    if(mesh!=nullptr)
    {
        PointsCloud *pointCloud = new PointsCloud(mesh);
        pointCloud->setPureColor(false);
        return pointCloud;
    }
    else
    {
        return nullptr;
    }
}

bool PointsCloudIO::hasErr()
{
    if(err!="")
    {
        return true;
    }
    return false;
}

const QString &PointsCloudIO::getErr() const
{
    return err;
}
