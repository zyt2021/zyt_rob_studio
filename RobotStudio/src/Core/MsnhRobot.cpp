﻿#include <Core/MsnhRobot.h>


#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

Robot::Robot(const QString &path, QObject *parent):QObject(parent)
{
    ModelLoader loader;
    _xAxis = loader.loadMeshFromFile( ":/shapes/AxesX.obj" );
    _yAxis = loader.loadMeshFromFile( ":/shapes/AxesY.obj" );
    _zAxis = loader.loadMeshFromFile( ":/shapes/AxesZ.obj" );

    _xAxis->setParent(this);
    _yAxis->setParent(this);
    _zAxis->setParent(this);

    _tf    = ModelLoader::loadSphereModel();
    _tf->setParent(this);

    loadURDF(path);

    _animationTree->setVisible(false);
    _collisionTree->setVisible(false);

    initNotFixJointNum();

    _parentWidget = new QWidget();

    auto begin = _visualJoints.begin();
    while (begin != _visualJoints.end())
    {
        auto joint = begin.value();
        if(joint->getJointType()!=Msnhnet::JOINT_FIXED)
        {
            MsnhPlotPosVecAcc *plot = new MsnhPlotPosVecAcc(PLOT_SINGLE,_parentWidget);
            plot->setWindowTitle(begin.key());
            _jointsPlot[begin.key()]   = plot;
        }
        begin++;
    }

    _timer = new QTimer(this);
    connect(_timer, &QTimer::timeout, this, &Robot::doIkTimer);
    _timer->start(10);

    _executeTimer = new QTimer(this);
    connect(_executeTimer, &QTimer::timeout, this, &Robot::executePlanTimer);
    _executeTimer->start(10);
}

Robot::Robot(const Robot &robot, QObject *parent):QObject(parent)
{

    //TODO:
    _xAxis = new Mesh(*(robot._xAxis),this);
    _yAxis = new Mesh(*(robot._yAxis),this);
    _zAxis = new Mesh(*(robot._zAxis),this);

    _tf    = new Model(*(robot._tf),this);

    auto iter = robot._modelList.begin();
    while (iter != robot._modelList.end())
    {
        _modelList[iter.key()] = new Model(*(iter.value()),this);
        iter++;
    }

    _urdfContent = robot._urdfContent;

    loadURDF();

    _animationTree->setVisible(false);
    _collisionTree->setVisible(false);

    initNotFixJointNum();
    _parentWidget = new QWidget();

    auto begin = _visualJoints.begin();
    while (begin != _visualJoints.end())
    {
        auto joint = begin.value();
        if(joint->getJointType()!=Msnhnet::JOINT_FIXED)
        {
            MsnhPlotPosVecAcc *plot = new MsnhPlotPosVecAcc(PLOT_SINGLE,_parentWidget);
            plot->setWindowTitle(begin.key());
            _jointsPlot[begin.key()]   = plot;
        }
        begin++;
    }
}

Robot::~Robot()
{
    delete _parentWidget;
    _parentWidget = nullptr;

    _timer->stop();
    _tfPubSockets.clear();
}

void Robot::loadURDF(const QString &path)
{
    _axes.clear();
    _urdfContent    = QString::fromStdString(Msnhnet::URDF::getURDFStr(path.toUtf8().toStdString()));
    _model          = Msnhnet::URDF::parseFromUrdfFile(path.toUtf8().toStdString());
    _linkTree       = Msnhnet::URDF::getLinkTree(_model->rootLink);
    _strTree        = Msnhnet::URDF::getStringTree(_linkTree);

    _robotName      = QString::fromStdString(_model->getName());

    this->setObjectName(QString::fromStdString(_model->name));

    _loadType = 0;
    _visualTree     = loadModel(_linkTree,_model->rootPath);
    _visualTree->setParent(this);
    _visualTree->setDraggable(false);
    _visualTree->setObjectName("visual");

    _loadType = 1;
    _endTree        = loadModel(_linkTree,_model->rootPath);
    _endTree->setParent(this);
    _endTree->setDraggable(false);
    _endTree->setObjectName("end");

    _loadType = 2;
    _animationTree  = loadModel(_linkTree,_model->rootPath);
    _animationTree->setParent(this);
    _animationTree->setDraggable(false);
    _animationTree->setObjectName("animation");

    _collisionTree  = loadCollision(_linkTree, _model->rootPath);
    _collisionTree->setParent(this);
    _collisionTree->setDraggable(false);
    _collisionTree->setObjectName("collision");

    _showModel      = new Model(this);
    _showModel->setObjectName("Robot Models");
    _showModel->addChildModel(_visualTree);
    _showModel->addChildModel(_endTree);
    _showModel->addChildModel(_animationTree);
    _showModel->addChildModel(_collisionTree);


    for (int i = 0; i < RobotUtil::chainNums; ++i)
    {
        QString tfStr;
        if(i < 10)
        {
            tfStr = "tf0"+QString::number(i);
        }
        else
        {
            tfStr = "tf"+QString::number(i);
        }
        _tfTaken[tfStr]         = false;
        _excutionPlan[tfStr]    = false;
        _animationPlan[tfStr]   = false;
    }

    _showModel->setDeleteable(false);

    initCollisionEnv();

    enableSelect(false);
}

void Robot::loadURDF()
{
    if(_urdfContent == "null")
    {
        throw Msnhnet::Exception(1,"URDF content not initialized!" , __FILE__, __LINE__, __FUNCTION__);
    }
    _axes.clear();
    _model          = Msnhnet::URDF::parseFromUrdfStr(_urdfContent.toStdString(), "NULL");
    _linkTree       = Msnhnet::URDF::getLinkTree(_model->rootLink);
    _strTree        = Msnhnet::URDF::getStringTree(_linkTree);

    this->setObjectName(QString::fromStdString(_model->name));
    _robotName      = QString::fromStdString(_model->getName());

    _loadType = 0;
    _visualTree     = loadModel(_linkTree,_model->rootPath);
    _visualTree->setParent(this);
    _visualTree->setDraggable(false);
    _visualTree->setObjectName("visual");

    _loadType = 1;
    _endTree        = loadModel(_linkTree,_model->rootPath);
    _endTree->setParent(this);
    _endTree->setDraggable(false);
    _endTree->setObjectName("end");

    _loadType = 2;
    _animationTree  = loadModel(_linkTree,_model->rootPath);
    _animationTree->setParent(this);
    _animationTree->setDraggable(false);
    _animationTree->setObjectName("animation");

    _collisionTree  = loadCollision(_linkTree, _model->rootPath);
    _collisionTree->setParent(this);
    _collisionTree->setDraggable(false);
    _collisionTree->setObjectName("collision");

    _showModel      = new Model(this);
    _showModel->setObjectName("Robot Models");
    _showModel->addChildModel(_visualTree);
    _showModel->addChildModel(_endTree);
    _showModel->addChildModel(_animationTree);
    _showModel->addChildModel(_collisionTree);

    for (int i = 0; i < 10; ++i)
    {
        QString tfStr;
        if(i < 10)
        {
            tfStr = "tf0"+QString::number(i);
        }
        else
        {
            tfStr = "tf"+QString::number(i);
        }

        _tfTaken[tfStr]      = false;
        _excutionPlan[tfStr] = false;
    }

    _showModel->setDeleteable(false);

    initCollisionEnv();

    enableSelect(false);
}

Model *Robot::loadModel(const std::shared_ptr<Msnhnet::URDFLinkTree> &link, const std::string &rootPath)
{
    Model* md       = new Model(this);
    Model* origin   = new Model(this);
    Model* joint    = new Model(this);

    origin->setObjectName("origin");
    joint->setObjectName(QString::fromStdString(link->joint.value()->name));

    Msnhnet::Frame frame = link->joint.value()->parentToJointTransform;

    joint->setEnd2TipFrame(frame);

    Msnhnet::Vector3DS axis = link->joint.value()->axis;

    Msnhnet::URDFJointType jointType = link->joint.value()->type;

    if(jointType == Msnhnet::URDFJointType::URDF_FIXED || jointType == Msnhnet::URDFJointType::URDF_UNKNOWN)
    {
        joint->setJointType(Msnhnet::JointType::JOINT_FIXED);
    }
    else if(jointType == Msnhnet::URDFJointType::URDF_CONTINUOUS)
    {
        if(axis == Msnhnet::Vector3DS(0,0,0))
        {
            throw Msnhnet::Exception(1,"Continuous axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        else
        {
            joint->setJointType(Msnhnet::JOINT_ROT_AXIS);
        }
    }
    else if(jointType == Msnhnet::URDFJointType::URDF_REVOLUTE)
    {
        if(axis == Msnhnet::Vector3DS(0,0,0))
        {
            throw Msnhnet::Exception(1,"Revolute axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        else
        {
            joint->setJointType(Msnhnet::JOINT_ROT_AXIS);
            joint->setAxis(axis);
        }

        double min = link->joint.value()->limits.value()->lower;
        double max = link->joint.value()->limits.value()->upper;
        joint->setJointMinMax(min,max);
    }
    else if(jointType == Msnhnet::URDFJointType::URDF_PRISMATIC)
    {
        if(axis == Msnhnet::Vector3DS(0,0,0))
        {
            throw Msnhnet::Exception(1,"Prismatic axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        else
        {
            joint->setJointType(Msnhnet::JOINT_TRANS_AXIS);
            joint->setAxis(axis);
        }
    }
    else
    {
        throw Msnhnet::Exception(1,"Type is not supported!",__FILE__, __LINE__, __FUNCTION__);
    }

    Mesh* xMesh = new Mesh(*_xAxis,this);
    Mesh* yMesh = new Mesh(*_yAxis,this);
    Mesh* zMesh = new Mesh(*_zAxis,this);

    //轴不可被选，防止点点点误操作
    xMesh->setSelectable(false);
    yMesh->setSelectable(false);
    zMesh->setSelectable(false);

    if(link->nextLink.size()>0)
    {
        _axes.push_back(xMesh);
        _axes.push_back(yMesh);
        _axes.push_back(zMesh);
    }
    else
    {
        _endAxes.push_back(xMesh);
        _endAxes.push_back(yMesh);
        _endAxes.push_back(zMesh);
    }

    joint->addChildMesh(xMesh);//addchild会自动指定parent
    joint->addChildMesh(yMesh);
    joint->addChildMesh(zMesh);

    joint->getChildMeshes()[0]->scaleApi({_axesScale,_axesScale,_axesScale});
    joint->getChildMeshes()[1]->scaleApi({_axesScale,_axesScale,_axesScale});
    joint->getChildMeshes()[2]->scaleApi({_axesScale,_axesScale,_axesScale});

    joint->getChildMeshes()[0]->setObjectName(QString::fromStdString(link->joint.value()->name)+"_X");
    joint->getChildMeshes()[1]->setObjectName(QString::fromStdString(link->joint.value()->name)+"_Y");
    joint->getChildMeshes()[2]->setObjectName(QString::fromStdString(link->joint.value()->name)+"_Z");

    joint->getChildMeshes()[2]->getMaterial()->setColor( QVector3D( 0.66f, 0, 0 ) );
    joint->getChildMeshes()[0]->getMaterial()->setColor( QVector3D( 0, 0.66f, 0 ) );
    joint->getChildMeshes()[1]->getMaterial()->setColor( QVector3D( 0, 0, 0.66f ) );


    Msnhnet::EulerDS euler = Msnhnet::GeometryS::rotMat2Euler(frame.rotMat,Msnhnet::ROT_ZYX);
    origin->setPositionApi({(float)frame.trans[0],(float)frame.trans[1],(float)frame.trans[2]});
    origin->setRotationApi({(float)(euler[0]*MSNH_RAD_2_DEG),(float)(euler[1]*MSNH_RAD_2_DEG),(float)(euler[2]*MSNH_RAD_2_DEG)});

    origin->addChildModel(joint);
    md->addChildModel(origin);

    if(_loadType == 0)
    {
        _visualJoints[QString::fromStdString(link->joint.value()->name)] = joint;
    }
    else if(_loadType == 1)
    {
        _endJoints[QString::fromStdString(link->joint.value()->name)] = joint;
    }
    else
    {
        _animationJoints[QString::fromStdString(link->joint.value()->name)] = joint;
    }

    for (size_t i = 0; i < link->visuals.size(); ++i)
    {

        bool hasMaterial     =  link->visuals[i]->material.has_value();

        //使用URDF文件中的材质颜色
        Msnhnet::Color color;

        if(hasMaterial)
        {
            color = link->visuals[i]->material.value()->color;
        }

        auto geometry = link->visuals[i]->geometry->get();

        Msnhnet::Frame linkFrame     = link->visuals[i]->origin;
        Msnhnet::EulerDS linkEuler   = Msnhnet::GeometryS::rotMat2Euler(linkFrame.rotMat,Msnhnet::ROT_ZYX);
        Msnhnet::Vector3DS linkTrans = linkFrame.trans;

        //TODO: 不同的类型MESH 其他 不同的if设置不同的颜色.
        if(geometry->type == Msnhnet::URDFGeometryType::URDF_MESH)
        {
            auto mesh = (Msnhnet::URDFMesh*)geometry;

            std::string path = mesh->filename;

            Msnhnet::Vector3DS scale = mesh->scale;

            Msnhnet::ExString::replace(path,"package:/","");

            std::string fullPath = rootPath + path;

            QString mapModelKey = QString::fromStdString(path);

            if(_modelList.contains(mapModelKey))
            {
                Model* mod = new Model(*_modelList[mapModelKey]);  //从内存中提取,被add不用指定parent

                if(_loadType == 1)                                                         //End mesh,纯色,不影响axis
                {
                    mod->setColor(QVector3D({RobotUtil::dragColor.red()/256.f,
                        RobotUtil::dragColor.green()/256.f,
                        RobotUtil::dragColor.blue()/256.f}));
                    _endModels.push_back(mod);
                }
                else if(_loadType == 2)                                                   //动画 mesh,线框模式,不影响axis
                {
                    mod->setColor(QVector3D({RobotUtil::animationColor.red()/256.f,
                        RobotUtil::animationColor.green()/256.f,
                        RobotUtil::animationColor.blue()/256.f}));
                    _animationlModels.push_back(mod);
                }

                mod->setScalingApi(QVector3D((float)scale[0],(float)scale[1],(float)scale[2]));
                mod->setPositionApi(QVector3D((float)linkTrans[0],(float)linkTrans[1],(float)linkTrans[2]));
                mod->setRotationApi(QVector3D((float)(linkEuler[0]*MSNH_RAD_2_DEG),(float)(linkEuler[1]*MSNH_RAD_2_DEG),(float)(linkEuler[2]*MSNH_RAD_2_DEG)));


                md->getChildModels()[0]->getChildModels()[0]->addChildModel(mod);
            }
            else
            {
                ModelLoader loader;
                Model* model = loader.loadModelFromFile(QString::fromStdString(fullPath)); //读取
                if(!model)
                {
                    throw Msnhnet::Exception(1,"Open model failed! path: " + fullPath, __FILE__, __LINE__, __FUNCTION__);
                }
                model->setParent(this);                                                    // modellist的parent永远是this
                _modelList[mapModelKey] = model;                                           //保存
                Model* mod = new Model(*model);                                            //复制一份

                if(_loadType == 1)                                                         //End mesh,纯色,不影响axis
                {
                    mod->setColor(QVector3D({RobotUtil::dragColor.red()/256.f,
                        RobotUtil::dragColor.green()/256.f,
                        RobotUtil::dragColor.blue()/256.f}));
                    _endModels.push_back(mod);
                }
                else if(_loadType == 2)                                                   //动画 mesh,线框模式,不影响axis
                {
                    mod->setColor(QVector3D({RobotUtil::animationColor.red()/256.f,
                        RobotUtil::animationColor.green()/256.f,
                        RobotUtil::animationColor.blue()/256.f}));
                    _animationlModels.push_back(mod);
                }
                else
                {
                    //mod->setColor(QVector3D(color.r,color.g,color.b));                  //Mesh模式暂时不用
                }

                mod->setScalingApi(QVector3D((float)scale[0],(float)scale[1],(float)scale[2]));
                mod->setPositionApi(QVector3D((float)linkTrans[0],(float)linkTrans[1],(float)linkTrans[2]));
                mod->setRotationApi(QVector3D((float)(linkEuler[0]*MSNH_RAD_2_DEG),(float)(linkEuler[1]*MSNH_RAD_2_DEG),(float)(linkEuler[2]*MSNH_RAD_2_DEG)));


                md->getChildModels()[0]->getChildModels()[0]->addChildModel(mod);
            }
        }
        else
        {
            QString mapModelKey;

            if(geometry->type == Msnhnet::URDFGeometryType::URDF_SPHERE)                 //如果内存中有，就从内存里提取
            {
                mapModelKey = "MSNH_SPHERE";
            }
            else if(geometry->type == Msnhnet::URDFGeometryType::URDF_BOX)
            {
                mapModelKey = "URDF_BOX";
            }
            else if(geometry->type == Msnhnet::URDFGeometryType::URDF_CYLINDER)
            {
                mapModelKey = "URDF_CYLINDER";
            }

            if(_modelList.contains(mapModelKey))
            {
                Model* mod = new Model(*_modelList[mapModelKey]);  //从内存中提取

                if(geometry->type == Msnhnet::URDFGeometryType::URDF_SPHERE)               //设置默认模型参数
                {
                    auto sphere = (Msnhnet::URDFSphere*)geometry;
                    float radius = (float)sphere->radius;
                    mod->setScalingApi(QVector3D(radius,radius,radius));
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_BOX)
                {
                    auto box = (Msnhnet::URDFBox*)geometry;
                    auto dim = box->dim;
                    mod->setScalingApi(QVector3D((float)dim[0],(float)dim[1],(float)dim[2]));
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_CYLINDER)
                {
                    auto cylinder = (Msnhnet::URDFCylinder*)geometry;
                    float radius  = (float)cylinder->radius;
                    float lenght  = (float)cylinder->length;
                    mod->setScalingApi(QVector3D(radius,radius,lenght));
                }

                if(_loadType == 1)                                                         //End mesh,纯色,不影响axis
                {
                    mod->setColor(QVector3D({RobotUtil::dragColor.red()/256.f,
                        RobotUtil::dragColor.green()/256.f,
                        RobotUtil::dragColor.blue()/256.f}));
                    _endModels.push_back(mod);
                }
                else if(_loadType == 2)                                                   //动画 mesh,线框模式,不影响axis
                {
                    mod->setColor(QVector3D({RobotUtil::animationColor.red()/256.f,
                        RobotUtil::animationColor.green()/256.f,
                        RobotUtil::animationColor.blue()/256.f}));
                    _animationlModels.push_back(mod);
                }
                else
                {
                    mod->setColor(QVector3D(color.r,color.g,color.b));                                          //设置模型颜色
                }

                mod->setPositionApi(QVector3D((float)linkTrans[0],(float)linkTrans[1],(float)linkTrans[2]));
                mod->setRotationApi(QVector3D((float)(linkEuler[0]*MSNH_RAD_2_DEG),(float)(linkEuler[1]*MSNH_RAD_2_DEG),(float)(linkEuler[2]*MSNH_RAD_2_DEG)));

                md->getChildModels()[0]->getChildModels()[0]->addChildModel(mod);
            }
            else
            {
                Model* model;

                if(geometry->type == Msnhnet::URDFGeometryType::URDF_SPHERE)            //根据不同的默认类型，加载不同的默认模型
                {
                    model = ModelLoader::loadSphereModel();
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_BOX)
                {
                    model = ModelLoader::loadCubeModel();
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_CYLINDER)
                {
                    model = ModelLoader::loadCylinderModel();
                }

                model->setParent(this);                                                    // modellist的parent永远是this
                _modelList[mapModelKey] = model;                                           //保存
                Model* mod = new Model(*model);                                            //复制一份

                if(geometry->type == Msnhnet::URDFGeometryType::URDF_SPHERE)               //设置默认模型参数
                {
                    auto sphere = (Msnhnet::URDFSphere*)geometry;
                    float radius = (float)sphere->radius;
                    mod->setScalingApi(QVector3D(radius,radius,radius));
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_BOX)
                {
                    auto box = (Msnhnet::URDFBox*)geometry;
                    auto dim = box->dim;
                    mod->setScalingApi(QVector3D((float)dim[0],(float)dim[1],(float)dim[2]));
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_CYLINDER)
                {
                    auto cylinder = (Msnhnet::URDFCylinder*)geometry;
                    float radius  = (float)cylinder->radius;
                    float lenght  = (float)cylinder->length;
                    mod->setScalingApi(QVector3D(radius,radius,lenght));
                }

                if(_loadType == 1)                                                         //End mesh,纯色,不影响axis
                {
                    mod->setColor(QVector3D({RobotUtil::dragColor.red()/256.f,
                        RobotUtil::dragColor.green()/256.f,
                        RobotUtil::dragColor.blue()/256.f}));
                    _endModels.push_back(mod);
                }
                else if(_loadType == 2)                                                   //动画 mesh,线框模式,不影响axis
                {
                    mod->setColor(QVector3D({RobotUtil::animationColor.red()/256.f,
                        RobotUtil::animationColor.green()/256.f,
                        RobotUtil::animationColor.blue()/256.f}));
                    _animationlModels.push_back(mod);
                }
                else
                {
                    mod->setColor(QVector3D(color.r,color.g,color.b));                    //设置模型颜色
                }

                mod->setPositionApi(QVector3D((float)linkTrans[0],(float)linkTrans[1],(float)linkTrans[2]));
                mod->setRotationApi(QVector3D((float)(linkEuler[0]*MSNH_RAD_2_DEG),(float)(linkEuler[1]*MSNH_RAD_2_DEG),(float)(linkEuler[2]*MSNH_RAD_2_DEG)));

                md->getChildModels()[0]->getChildModels()[0]->addChildModel(mod);
            }
        }
    }

    if(link->nextLink.size()>0)
    {
        for (size_t i = 0; i < link->nextLink.size(); ++i)
        {
            md->getChildModels()[0]->getChildModels()[0]->addChildModel(loadModel(link->nextLink[i],rootPath));
        }
    }
    return md;
}

Model *Robot::loadCollision(const std::shared_ptr<Msnhnet::URDFLinkTree> &link, const string &rootPath)
{
    Model* md       = new Model(this);
    Model* origin   = new Model(this);
    Model* joint    = new Model(this);

    QString jointName = QString::fromStdString(link->joint.value()->name);

    origin->setObjectName("origin");
    joint->setObjectName(jointName);

    Msnhnet::Frame frame = link->joint.value()->parentToJointTransform;

    joint->setEnd2TipFrame(frame);

    Msnhnet::Vector3DS axis = link->joint.value()->axis;

    Msnhnet::URDFJointType jointType = link->joint.value()->type;

    if(jointType == Msnhnet::URDFJointType::URDF_FIXED || jointType == Msnhnet::URDFJointType::URDF_UNKNOWN)
    {
        joint->setJointType(Msnhnet::JointType::JOINT_FIXED);
    }
    else if(jointType == Msnhnet::URDFJointType::URDF_CONTINUOUS)
    {
        if(axis == Msnhnet::Vector3DS(0,0,0))
        {
            throw Msnhnet::Exception(1,"Continuous axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        else
        {
            joint->setJointType(Msnhnet::JOINT_ROT_AXIS);
        }
    }
    else if(jointType == Msnhnet::URDFJointType::URDF_REVOLUTE)
    {
        if(axis == Msnhnet::Vector3DS(0,0,0))
        {
            throw Msnhnet::Exception(1,"Revolute axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        else
        {
            joint->setJointType(Msnhnet::JOINT_ROT_AXIS);
            joint->setAxis(axis);
        }

        double min = link->joint.value()->limits.value()->lower;
        double max = link->joint.value()->limits.value()->upper;
        joint->setJointMinMax(min,max);
    }
    else if(jointType == Msnhnet::URDFJointType::URDF_PRISMATIC)
    {
        if(axis == Msnhnet::Vector3DS(0,0,0))
        {
            throw Msnhnet::Exception(1,"Prismatic axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        else
        {
            joint->setJointType(Msnhnet::JOINT_TRANS_AXIS);
            joint->setAxis(axis);
        }
    }
    else
    {
        throw Msnhnet::Exception(1,"Type is not supported!",__FILE__, __LINE__, __FUNCTION__);
    }

    Mesh* xMesh = new Mesh(*_xAxis,this);
    Mesh* yMesh = new Mesh(*_yAxis,this);
    Mesh* zMesh = new Mesh(*_zAxis,this);

    //轴不可被选，防止点点点误操作
    xMesh->setSelectable(false);
    yMesh->setSelectable(false);
    zMesh->setSelectable(false);

    if(link->nextLink.size()>0)
    {
        _axes.push_back(xMesh);
        _axes.push_back(yMesh);
        _axes.push_back(zMesh);
    }
    else
    {
        _endAxes.push_back(xMesh);
        _endAxes.push_back(yMesh);
        _endAxes.push_back(zMesh);
    }

    joint->addChildMesh(xMesh);//addchild会自动指定parent
    joint->addChildMesh(yMesh);
    joint->addChildMesh(zMesh);

    joint->getChildMeshes()[0]->scaleApi({_axesScale,_axesScale,_axesScale});
    joint->getChildMeshes()[1]->scaleApi({_axesScale,_axesScale,_axesScale});
    joint->getChildMeshes()[2]->scaleApi({_axesScale,_axesScale,_axesScale});

    joint->getChildMeshes()[0]->setObjectName(jointName+"_X");
    joint->getChildMeshes()[1]->setObjectName(jointName+"_Y");
    joint->getChildMeshes()[2]->setObjectName(jointName+"_Z");

    joint->getChildMeshes()[2]->getMaterial()->setColor( QVector3D( 0.66f, 0, 0 ) );
    joint->getChildMeshes()[0]->getMaterial()->setColor( QVector3D( 0, 0.66f, 0 ) );
    joint->getChildMeshes()[1]->getMaterial()->setColor( QVector3D( 0, 0, 0.66f ) );


    Msnhnet::EulerDS euler = Msnhnet::GeometryS::rotMat2Euler(frame.rotMat,Msnhnet::ROT_ZYX);
    origin->setPositionApi({(float)frame.trans[0],(float)frame.trans[1],(float)frame.trans[2]});
    origin->setRotationApi({(float)(euler[0]*MSNH_RAD_2_DEG),(float)(euler[1]*MSNH_RAD_2_DEG),(float)(euler[2]*MSNH_RAD_2_DEG)});

    origin->addChildModel(joint);
    md->addChildModel(origin);

    _collisionJoints[jointName] = joint;

    QVector<GeomModel> geoms;

    for (size_t i = 0; i < link->collisions.size(); ++i)
    {

        auto geometry = link->collisions[i]->geometry->get();

        GeomModel geommesh;
        geommesh.geom = link->collisions[i]->geometry.value();

        Msnhnet::Frame linkFrame     = link->collisions[i]->origin;
        Msnhnet::EulerDS linkEuler   = Msnhnet::GeometryS::rotMat2Euler(linkFrame.rotMat,Msnhnet::ROT_ZYX);
        Msnhnet::Vector3DS linkTrans = linkFrame.trans;

        //TODO: 不同的类型MESH 其他 不同的if设置不同的颜色.
        if(geometry->type == Msnhnet::URDFGeometryType::URDF_MESH)
        {
            auto mesh = (Msnhnet::URDFMesh*)geometry;

            std::string path = mesh->filename;

            Msnhnet::Vector3DS scale = mesh->scale;

            Msnhnet::ExString::replace(path,"package:/","");

            std::string fullPath = rootPath + path;

            auto ptr = (Msnhnet::URDFMesh*)geommesh.geom.get();

            ptr->filename = fullPath;

            QString mapModelKey = QString::fromStdString(path);

            if(_modelList.contains(mapModelKey))
            {
                Model* mod = new Model(*_modelList[mapModelKey]);  //从内存中提取,被add不用指定parent

                mod->setScalingApi(QVector3D((float)scale[0],(float)scale[1],(float)scale[2]));
                mod->setPositionApi(QVector3D((float)linkTrans[0],(float)linkTrans[1],(float)linkTrans[2]));
                mod->setRotationApi(QVector3D((float)(linkEuler[0]*MSNH_RAD_2_DEG),(float)(linkEuler[1]*MSNH_RAD_2_DEG),(float)(linkEuler[2]*MSNH_RAD_2_DEG)));

                mod->setWireFrameMode(true);
                geommesh.model = mod;


                md->getChildModels()[0]->getChildModels()[0]->addChildModel(mod);
            }
            else
            {
                ModelLoader loader;
                Model* model = loader.loadModelFromFile(QString::fromStdString(fullPath)); //读取
                if(!model)
                {
                    throw Msnhnet::Exception(1,"Open model failed! path: " + fullPath, __FILE__, __LINE__, __FUNCTION__);
                }
                model->setParent(this);                                                    // modellist的parent永远是this
                _modelList[mapModelKey] = model;                                           //保存
                Model* mod = new Model(*model);                                            //复制一份

                mod->setScalingApi(QVector3D((float)scale[0],(float)scale[1],(float)scale[2]));
                mod->setPositionApi(QVector3D((float)linkTrans[0],(float)linkTrans[1],(float)linkTrans[2]));
                mod->setRotationApi(QVector3D((float)(linkEuler[0]*MSNH_RAD_2_DEG),(float)(linkEuler[1]*MSNH_RAD_2_DEG),(float)(linkEuler[2]*MSNH_RAD_2_DEG)));

                mod->setWireFrameMode(true);
                geommesh.model = mod;


                md->getChildModels()[0]->getChildModels()[0]->addChildModel(mod);
            }
        }
        else
        {
            QString mapModelKey;

            if(geometry->type == Msnhnet::URDFGeometryType::URDF_SPHERE)                 //如果内存中有，就从内存里提取
            {
                mapModelKey = "MSNH_SPHERE";
            }
            else if(geometry->type == Msnhnet::URDFGeometryType::URDF_BOX)
            {
                mapModelKey = "URDF_BOX";
            }
            else if(geometry->type == Msnhnet::URDFGeometryType::URDF_CYLINDER)
            {
                mapModelKey = "URDF_CYLINDER";
            }

            if(_modelList.contains(mapModelKey))
            {
                Model* mod = new Model(*_modelList[mapModelKey]);  //从内存中提取

                if(geometry->type == Msnhnet::URDFGeometryType::URDF_SPHERE)               //设置默认模型参数
                {
                    auto sphere = (Msnhnet::URDFSphere*)geometry;
                    float radius = (float)sphere->radius;
                    mod->setScalingApi(QVector3D(radius,radius,radius));
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_BOX)
                {
                    auto box = (Msnhnet::URDFBox*)geometry;
                    auto dim = box->dim;
                    mod->setScalingApi(QVector3D((float)dim[0],(float)dim[1],(float)dim[2]));
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_CYLINDER)
                {
                    auto cylinder = (Msnhnet::URDFCylinder*)geometry;
                    float radius  = (float)cylinder->radius;
                    float lenght  = (float)cylinder->length;
                    mod->setScalingApi(QVector3D(radius,radius,lenght));
                }

                mod->setPositionApi(QVector3D((float)linkTrans[0],(float)linkTrans[1],(float)linkTrans[2]));
                mod->setRotationApi(QVector3D((float)(linkEuler[0]*MSNH_RAD_2_DEG),(float)(linkEuler[1]*MSNH_RAD_2_DEG),(float)(linkEuler[2]*MSNH_RAD_2_DEG)));

                mod->setWireFrameMode(true);
                geommesh.model = mod;


                md->getChildModels()[0]->getChildModels()[0]->addChildModel(mod);
            }
            else
            {
                Model* model;

                if(geometry->type == Msnhnet::URDFGeometryType::URDF_SPHERE)            //根据不同的默认类型，加载不同的默认模型
                {
                    model = ModelLoader::loadSphereModel();
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_BOX)
                {
                    model = ModelLoader::loadCubeModel();
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_CYLINDER)
                {
                    model = ModelLoader::loadCylinderModel();
                }

                model->setParent(this);                                                    // modellist的parent永远是this
                _modelList[mapModelKey] = model;                                           //保存
                Model* mod = new Model(*model);                                            //复制一份

                if(geometry->type == Msnhnet::URDFGeometryType::URDF_SPHERE)               //设置默认模型参数
                {
                    auto sphere = (Msnhnet::URDFSphere*)geometry;
                    float radius = (float)sphere->radius;
                    mod->setScalingApi(QVector3D(radius,radius,radius));
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_BOX)
                {
                    auto box = (Msnhnet::URDFBox*)geometry;
                    auto dim = box->dim;
                    mod->setScalingApi(QVector3D((float)dim[0],(float)dim[1],(float)dim[2]));
                }
                else if(geometry->type == Msnhnet::URDFGeometryType::URDF_CYLINDER)
                {
                    auto cylinder = (Msnhnet::URDFCylinder*)geometry;
                    float radius  = (float)cylinder->radius;
                    float lenght  = (float)cylinder->length;
                    mod->setScalingApi(QVector3D(radius,radius,lenght));
                }

                mod->setPositionApi(QVector3D((float)linkTrans[0],(float)linkTrans[1],(float)linkTrans[2]));
                mod->setRotationApi(QVector3D((float)(linkEuler[0]*MSNH_RAD_2_DEG),(float)(linkEuler[1]*MSNH_RAD_2_DEG),(float)(linkEuler[2]*MSNH_RAD_2_DEG)));

                mod->setWireFrameMode(true);
                geommesh.model = mod;


                md->getChildModels()[0]->getChildModels()[0]->addChildModel(mod);
            }
        }

        geoms.push_back(geommesh);
    }

    _collisionGeomModel[jointName] = geoms;

    _linkNames.push_back(jointName.toLocal8Bit().toStdString());

    if(link->nextLink.size()>0)
    {
        for (size_t i = 0; i < link->nextLink.size(); ++i)
        {
            md->getChildModels()[0]->getChildModels()[0]->addChildModel(loadCollision(link->nextLink[i],rootPath));
        }
    }

    if(!md->getChildModels()[0]->getChildModels()[0]->getChildModels().isEmpty())
        _collisionLinks[jointName] = md->getChildModels()[0]->getChildModels()[0]->getChildModels()[0];
    return md;
}

QString Robot::getUrdfPath() const
{
    return _urdfPath;
}

Model *Robot::getVisualTree() const
{
    return _visualTree;
}

Model *Robot::getEndTree() const
{
    return _endTree;
}

Model *Robot::getAnimationTree() const
{
    return _animationTree;
}

Model *Robot::getCollisionTree() const
{
    return _collisionTree;
}

Model *Robot::getShowModel() const
{
    return _showModel;
}

void Robot::addChain(const QString &start, const QString &end, const QString &tf)
{
    if(_chains.size() > RobotUtil::chainNums)
    {
        throw Msnhnet::Exception(1,"Only " + std::to_string(RobotUtil::chainNums) + " chains supported!",__FILE__,__LINE__,__FUNCTION__);
    }

    Msnhnet::Chain chain = Msnhnet::URDF::getChain(_model, _strTree, start.toStdString(), end.toStdString());

    std::vector<std::string> chainName = Msnhnet::URDF::getChainNames(_strTree, start.toStdString(), end.toStdString());

    for(auto name : chainName)
    {
        _tfChainNames[tf].push_back(QString::fromStdString(name));
    }

    auto chainBg = _chains.begin();

    while (chainBg!=_chains.end())
    {
        auto tmpChain = chainBg.value();
        chainBg++;

        for (uint32_t j = 0; j < chain.getNumOfSegments(); ++j)
        {
            if(chain.getSegment(j).getMoveType() ==  Msnhnet::MOVE_CAN_NOT)
            {
                continue;
            }

            for (uint32_t k = 0; k < tmpChain.getNumOfSegments(); ++k)
            {
                if(tmpChain.getSegment(k).getMoveType() ==  Msnhnet::MOVE_CAN_NOT)
                {
                    continue;
                }

                if(chain.getSegment(j).getName() == tmpChain.getSegment(k).getName())
                {
                    throw Msnhnet::Exception(1,"Joint already exist!",__FILE__,__LINE__,__FUNCTION__);
                }
            }
        }
    }

    _tfTaken[tf] = true;
    _chains[tf] = chain;

    Msnhnet::VectorXSDS angle(chain.getNumOfJoints());

    Msnhnet::Frame fk = chain.fk(angle);

    Msnhnet::EulerDS euler = Msnhnet::GeometryS::rotMat2Euler(fk.rotMat,Msnhnet::ROT_ZYX);

    euler = euler/MSNH_PI*180;

    _chainGroup.push_back(QString("%1*%2*%3").arg(start).arg(end).arg(tf));

    Model* tfTool= new Model(*_tf);//被add不用指parent

    tfTool->setPositionApi({(float)(fk.trans[0]),(float)(fk.trans[1]),(float)(fk.trans[2])});
    tfTool->setRotationApi({(float)(euler[0]),(float)(euler[1]),(float)(euler[2])});
    tfTool->setScalingApi(QVector3D({_tfToolScale,_tfToolScale,_tfToolScale}));
    tfTool->setObjectName(tf);
    tfTool->setColor(QVector3D({1.0f,0.616f,0}));


    QSharedPointer<nn::socket> socket(new nn::socket(AF_SP, NN_PUB));
    QString ipc = "ipc:///tmp/"+ objectName() + "_"+tf;
    socket->bind(ipc.toStdString().data());
    _tfPubSockets[tf]   = socket;
    _tfPubAddrs[tf]     = ipc;

    _lastTfFrame[tf] = fk;
    _tfModels[tf] = tfTool;
    _joints[tf] = angle;
    _showModel->addChildModel(tfTool);

    // axes 组
    TransformGroup* tfGroup = new TransformGroup(this);
    tfGroup->setObjectName("AxesGroup_"+tf);
    _tfAxesGroup[tf] = tfGroup;
    _tfGroupAxesCnt[tf] = 0;
    emit axesGroupAdded(tfGroup);

    emit chainAdded(tf,fk);

    Curve3D* curve = new Curve3D(this);
    curve->setObjectName(tf+"_curve");
    curve->setDeleteable(false); //不可删

    curve->setIsCubeLine(true); //立体
    curve->setLineWidth(0.5f);  //线宽
    curve->setColor(QVector3D(0,0.4f,0.4f));

    _curves[tf] = curve;

    emit curveAdded(curve);

    //tf轨迹规划面板参数
    _tfPlaneParams[tf] = TfPlanParams();

    //曲线显示
    MsnhPlotPosVecAcc *cartPlot = new MsnhPlotPosVecAcc(PLOT_CARTESIAN,_parentWidget);
    cartPlot->setWindowTitle(tf);
    _cartsPlot[tf] = cartPlot;
}

const QMap<QString, Model *> &Robot::getVisualJoints() const
{
    return _visualJoints;
}

const QMap<QString, Model *> &Robot::getEndJoints() const
{
    return _endJoints;
}

const QMap<QString, Model *> &Robot::getAnimationJoints() const
{
    return _animationJoints;
}

bool Robot::getShowAxes() const
{
    return _showAxes;
}

bool Robot::getShowEndAxes() const
{
    return _showEndAxes;
}

void Robot::setShowAxes(bool showAxes)
{
    if(showAxes != _showAxes)
    {
        _showAxes = showAxes;
        emit showAxisChanged(_showAxes);

        for (int i = 0; i < _axes.size(); ++i)
        {
            _axes[i]->setVisible(showAxes);
        }
    }
}

void Robot::setShowEndAxes(bool showEndAxes)
{
    if(_showEndAxes != showEndAxes)
    {
        _showEndAxes = showEndAxes;
        //        emit showEndAxisChanged(_showEndAxes); ? TODO:

        for (int i = 0; i < _endAxes.size(); ++i)
        {
            _endAxes[i]->setVisible(showEndAxes);
        }
    }
}

void Robot::setAxesScale(float scale)
{
    if(abs(_axesScale - scale) > MSNH_F32_EPS)
    {
        _axesScale = scale;
        for (int i = 0; i < _axes.size(); ++i)
        {
            _axes[i]->setScalingApi({scale,scale,scale});
        }

        for (int i = 0; i < _endAxes.size(); ++i)
        {
            _endAxes[i]->setScalingApi({scale,scale,scale});
        }
    }
}

void Robot::setTfToolScale(float scale)
{
    if(abs(_tfToolScale - scale) > MSNH_F32_EPS)
    {
        _tfToolScale = scale;

        auto bg = _tfModels.begin();
        while(bg!=_tfModels.end())
        {
            bg.value()->setScalingApi({scale,scale,scale});
            bg++;
        }
    }
}

void Robot::setRouteAxesScale(float scale)
{
    if(abs(_routeAxesScale - scale) > MSNH_F32_EPS)
    {
        _routeAxesScale = scale;

        auto bg = _tfAxesGroup.begin();

        while(bg!=_tfAxesGroup.end())
        {
            auto group      = bg.value();
            auto axesVec    = group->getTfVecs();
            for(auto axes : axesVec)
            {
                axes->getTfModel()->setScalingApi({scale,scale,scale});
            }
            bg++;
        }

    }
}

const std::shared_ptr<StringTree> &Robot::getStrTree() const
{
    return _strTree;
}

const QString &Robot::getRobotName() const
{
    return _robotName;
}

const QMap<QString, bool> &Robot::getTfTaken() const
{
    return _tfTaken;
}

const QMap<QString, Msnhnet::Chain> &Robot::getChains() const
{
    return _chains;
}

void Robot::doIkTimer()
{
    std::vector<double> joints;

    timerTick++;

    if(_updateCurvesRealTime)
    {
        if(timerTick%3==0)
        {
            auto begin = _visualJoints.begin();
            while(begin != _visualJoints.end())
            {
                QString key = begin.key();
                auto md     = begin.value();
                if(md->getJointType() != Msnhnet::JOINT_FIXED)
                {
                    _jointsPlot[key]->addPos(md->getSingleMove());
                    joints.push_back(md->getSingleMove()*MSNH_DEG_2_RAD);
                }
                begin ++;
            }
        }
    }

    try
    {
        for (int i = 0; i < RobotUtil::chainNums; ++i)
        {
            QString tfStr;

            if(i < 10)
            {
                tfStr = "tf0"+QString::number(i);
            }
            else
            {
                tfStr = "tf"+QString::number(i);
            }

            if(_tfTaken[tfStr])
            {
                //动画演示
                if(_animationPlan[tfStr])
                {
                    if(_pathPlanJoints[tfStr].size() <= _animationStep[tfStr])
                    {
                        _animationStep[tfStr]   = _excutionStep[tfStr];
                    }
                    else
                    {
                        for (int i = 0; i < _joints[tfStr].mN; ++i)
                        {
                            QString jointName = QString::fromStdString(_chains[tfStr].getSegment(i).getName());
                            int step          = _animationStep[tfStr];
                            float joint       =  _pathPlanJoints[tfStr][step][i]/MSNH_PI*180.f;
                            _animationJoints[jointName]->singleMove(joint);
                        }
                        _animationStep[tfStr]++;
                    }
                }

//                //执行演示
//                if(_excutionPlan[tfStr]) //如果该tf开始运行JointPlan
//                {
//                    if(_pathPlanJoints[tfStr].size() <= _excutionStep[tfStr])
//                    {
//                        _excutionPlan[tfStr]    = false;
//                    }
//                    else
//                    {
//                        emit updateStep(tfStr,_excutionStep[tfStr]+1);

//                        std::vector<float> joints;

//                        QString jointStr = "";
//                        for (int i = 0; i < _joints[tfStr].mN; ++i)
//                        {
//                            QString jointName = QString::fromStdString(_chains[tfStr].getSegment(i).getName());
//                            int step          = _excutionStep[tfStr];
//                            float joint       =  _pathPlanJoints[tfStr][step][i]/MSNH_PI*180.f;
//                            if(!_enableRemoteJoints)
//                                _visualJoints[jointName]->singleMove(joint);

//                            joints.push_back(joint);
//                            jointStr += QString::number(joint)+",";
//                        }

//                        MsnhProtocal::RobotJoints robJoints(MsnhProtoType::IS_DATA);
//                        robJoints.setDataJoints(joints);
//                        std::vector<uint8_t> datas = robJoints.encode();

//                        _tfPubSockets[tfStr]->send(datas.data(), datas.size(),0);
//                        _excutionStep[tfStr]++;
//                    }
//                }

                Model* md = _tfModels[tfStr];

                auto chain = _chains[tfStr];
                auto chain1 = _chains[tfStr];

                //当前机器人的关节坐标
                Msnhnet::VectorXSDS fkJoints(joints.size());

                fkJoints.setVal(joints);

                if(_updateCurvesRealTime)
                {
                    //如果强制获取当前的末端位姿且到了更新时间
                    if(timerTick%3==0)
                    {
                        Msnhnet::Frame fkFrame = chain1.fk(fkJoints);
                        _cartsPlot[tfStr]->addFrame(fkFrame);
                    }
                }


                //实时规划的frame
                Msnhnet::Frame rtPlanFrame = getFrameFromModel(md);

                //确实发生变化
                if(Msnhnet::Frame::diff(_lastTfFrame[tfStr], rtPlanFrame).length() < MSNH_F32_EPS)
                {
                    continue;
                }

                emit updateFrame(tfStr, rtPlanFrame);

                //逆解
                int res = chain.ikLM(rtPlanFrame, _joints[tfStr]);

                //不成功
                if( res< 0)
                {
                    md->setFrame(_lastTfFrame[tfStr]);
                    qDebug()<<res;
                }
                else //成功
                {
                    _lastTfFrame[tfStr] = rtPlanFrame;
                    for (int i = 0; i < _joints[tfStr].mN; ++i)
                    {
                        //_visualJoints[QString::fromStdString(_chains[tfStr].getSegment(i).getName())]->singleMove((float)_joints[tfStr][i]/MSNH_PI*180.f);
                        _collisionJoints[QString::fromStdString(_chains[tfStr].getSegment(i).getName())]->singleMove((float)_joints[tfStr][i]/MSNH_PI*180.f);
                        _endJoints[QString::fromStdString(chain.getSegment(i).getName())]->singleMove((float)_joints[tfStr][i]*MSNH_RAD_2_DEG); //运行
                    }

                    if(_enableCollisionDetection)
                    {
                        CollisionRequest req;
                        req.computeDis = false;
                        req.maxContactsPerPair = 10;
                        CollisionResult res;

                        if(_collisionEnv)
                            _collisionEnv->checkSelfCollision(req,res,_collisionJoints,*_acm);

                        if(res.collision)
                        {
                            QString msg = "检测到碰撞! ";
                            auto beg = res.contacts.begin();
                            while(beg != res.contacts.end())
                            {
                                msg = msg + QString::fromStdString(beg->first.first) + "与" + QString::fromStdString(beg->first.second) + "; ";
                                beg++;
                            }

                            qWarning()<<msg;
                        }
                    }
                }

            }

        }
    }
    catch(Msnhnet::Exception ex)
    {
        qDebug()<<ex.what();
    }

    if(timerTick==9)
    {
        timerTick = 0;
    }

}

void Robot::executePlanTimer()
{

    for (int i = 0; i < RobotUtil::chainNums; ++i)
    {
        QString tfStr;

        if(i < 10)
        {
            tfStr = "tf0"+QString::number(i);
        }
        else
        {
            tfStr = "tf"+QString::number(i);
        }

        if(_tfTaken[tfStr])
        {
            //执行演示
            if(_excutionPlan[tfStr]) //如果该tf开始运行JointPlan
            {
                if(_pathPlanJoints[tfStr].size() <= _excutionStep[tfStr])
                {
                    _excutionPlan[tfStr]    = false;
                    emit excutionDone(tfStr);
                }
                else
                {
                    emit updateStep(tfStr,_excutionStep[tfStr]+1);

                    std::vector<float> joints;

                    QString jointStr = "";
                    for (int i = 0; i < _joints[tfStr].mN; ++i)
                    {
                        QString jointName = QString::fromStdString(_chains[tfStr].getSegment(i).getName());
                        int step          = _excutionStep[tfStr];
                        float joint       =  _pathPlanJoints[tfStr][step][i]/MSNH_PI*180.f;
                        if(!_enableRemoteJoints)
                            _visualJoints[jointName]->singleMove(joint);

                        joints.push_back(joint);
                        jointStr += QString::number(joint)+",";
                    }

                    MsnhProtocal::RobotJoints robJoints(MsnhProtoType::IS_DATA);
                    robJoints.setDataJoints(joints);
                    std::vector<uint8_t> datas = robJoints.encode();

                    _tfPubSockets[tfStr]->send(datas.data(), datas.size(),0);
                    _excutionStep[tfStr]++;
                }
            }
        }
    }
}

Msnhnet::Frame Robot::getFrameFromModel(const Model *md)
{

    QVector3D pos = md->getPositionApi();
    QVector3D rot = md->getRotationApi();
    Msnhnet::Frame frame;
    frame.trans = {pos.x(), pos.y(), pos.z()};
    Msnhnet::EulerDS euler = {rot.x()/180.f*MSNH_PI, rot.y()/180.f*MSNH_PI,rot.z()/180.f*MSNH_PI};
    frame.rotMat = Msnhnet::GeometryS::euler2RotMat(euler, Msnhnet::ROT_ZYX);
    return frame;

}

//处理nanomsg发过来的数据
void Robot::revNNJoints(const std::vector<uint8_t> &data)
{
    if(_enableRemoteJoints)
    {
        MsnhProtocal::Base manager;
        manager.fromBytes(data);
        MsnhProtocal::RobotJoints* robJoints = manager.toRobotJoints();

        if(robJoints)
        {
            std::vector<float> joints = robJoints->getDataJoints();

            if(joints.size() != _notFixJointNum)
            {
                return;
            }

            int cnt = 0;

            auto visualBegin = _visualJoints.begin();

            while(visualBegin!=_visualJoints.end())
            {
                if(visualBegin.value()->getJointType()!=Msnhnet::JOINT_FIXED)
                {
                    visualBegin.value()->singleMove(joints[cnt]);
                    //_endJoints[visualBegin.key()]->singleMove(joints[cnt]);
                    cnt ++;
                }
                visualBegin++;
            }

            //updateTfTools();
        }
    }
}

void Robot::initNotFixJointNum()
{
    _notFixJointNum = 0;
    auto visualBegin = _visualJoints.begin();
    while(visualBegin!=_visualJoints.end())
    {
        if(visualBegin.value()->getJointType() !=Msnhnet::JOINT_FIXED)
        {
            _notFixJointNum ++;
        }
        visualBegin++;
    }
}

void Robot::updateTfTools()
{
    auto chainBegin = _chains.begin();

    while(chainBegin != _chains.end())
    {
        auto chain = chainBegin.value();
        auto segments = chain.segments;

        Msnhnet::VectorXSDS joints(chainBegin.value().getNumOfJoints());

        int j = 0;

        for (size_t i = 0; i < segments.size(); ++i)
        {
            if(segments[i].getMoveType()!=Msnhnet::JOINT_FIXED)
            {
                float angle = _visualJoints[QString::fromStdString(segments[i].getName())]->getSingleMove();
                joints[j] = angle/180*MSNH_PI;
                j++;
            }
        }

        Msnhnet::Frame res = chain.fk(joints);
        Msnhnet::TranslationDS trans    = res.trans;
        Msnhnet::RotationMatDS rotMat   = res.rotMat;
        Msnhnet::EulerDS       euler    = Msnhnet::GeometryS::rotMat2Euler(rotMat, Msnhnet::ROT_ZYX);

        Model* model =  _tfModels[chainBegin.key()];

        _lastTfFrame[chainBegin.key()] = res;
        model->setPositionApi(QVector3D((float)trans[0],(float)trans[1],(float)trans[2]));
        model->setRotationApi(QVector3D((float)(euler[0]*MSNH_RAD_2_DEG),(float)(euler[1]*MSNH_RAD_2_DEG),(float)(euler[2]*MSNH_RAD_2_DEG)));
        chainBegin++;
    }
}

float Robot::getRouteAxesScale() const
{
    return _routeAxesScale;
}

void Robot::singleJointMove(const QString &name, float val)
{
    //如果物理模式
    if(_enableRemoteJoints)
    {
        QVector<QString> tfs;

        // 关节控制的点在tf中？
        auto bg = _tfChainNames.begin();
        while(bg != _tfChainNames.end())
        {
            if(bg.value().contains(name))
            {
                tfs.push_back(bg.key());
            }
            bg++;
        }

        //不在，直接控制visual
        if(tfs.isEmpty())
        {
            _visualJoints[name]->singleMove(val);
        }
        else //在的话，需要发送给物理机器人
        {
            for(auto tfStr : tfs)
            {
                std::vector<float> joints;

                QString jointStr = "";
                for (int i = 0; i < _joints[tfStr].mN; ++i)
                {
                    QString jointName = QString::fromStdString(_chains[tfStr].getSegment(i).getName());
                    float joint       = 0;

                    if(jointName != name)
                    {
                        joint =  _visualJoints[jointName]->getSingleMove(); //模型位置
                    }
                    else
                    {
                        joint =  val; //拖动角度
                    }

                    joints.push_back(joint);
                    jointStr += QString::number(joint)+",";
                }

                MsnhProtocal::RobotJoints robJoints(MsnhProtoType::IS_DATA);
                robJoints.setDataJoints(joints);
                std::vector<uint8_t> datas = robJoints.encode();

                _tfPubSockets[tfStr]->send(datas.data(), datas.size(),0);
            }

        }

    }
    else
    {
        _visualJoints[name]->singleMove(val);
    }
}

float Robot::getTfToolScale() const
{
    return _tfToolScale;
}

float Robot::getAxesScale() const
{
    return _axesScale;
}

bool Robot::getBodySelectable() const
{
    return _bodySelectable;
}

bool Robot::getEnableCollisionDetection() const
{
    return _enableCollisionDetection;
}

void Robot::setEnableCollisionDetection(bool enableCollisionDetection)
{
    _enableCollisionDetection = enableCollisionDetection;
}

void Robot::enableSelect(bool selectable)
{
    if(_visualTree && _endTree && _animationTree && _collisionTree)
    {
        _visualTree->setSelectable(selectable);
        _endTree->setSelectable(selectable);
        _animationTree->setSelectable(selectable);
        _collisionTree->setSelectable(selectable);
        _bodySelectable = selectable;
    }
}

void Robot::setExcutionTimer(int delay)
{
    _executeTimer->stop();
    _executeTimer->start(delay);
}

QMap<QString, TfPlanParams>& Robot::getTfPlaneParams()
{
    return _tfPlaneParams;
}

QMap<QString, bool> Robot::getLinkNamePairs() const
{
    return _linkNamePairs;
}

QMap<QString, bool> &Robot::getLinkNamePairsChg()
{
    return _linkNamePairs;
}

std::vector<std::string> Robot::getLinkNames() const
{
    return _linkNames;
}

void Robot::selectVisual(const QString &name)
{
    auto iter = _collisionLinks.begin();
    while(iter != _collisionLinks.end())
    {
        if(iter.key() == name)
        {
            iter.value()->setWireFrameMode(false);
            iter.value()->setCollisionSelected(true);
        }
        else
        {
            iter.value()->setWireFrameMode(true);
        }
        iter ++;
    }
}

QVector<Model *> Robot::getAnimationlModels() const
{
    return _animationlModels;
}

void Robot::initCollisionEnv()
{
    _collisionEnv = new CollisionEnv(_collisionGeomModel);
    _acm = new AllowedCollisionMatrix();

    for (int i = 0; i < _linkNames.size(); ++i)
    {
        for (int j = i+1; j < _linkNames.size(); ++j)
        {
            _linkNamePairs[QString::fromStdString(_linkNames[i]+","+_linkNames[j])] = true;
        }
    }
    //_acm->setEntry(_linkNames, _linkNames, false);
}

void Robot::updateCollisionPair()
{
    auto iter = _linkNamePairs.begin();
    while(iter != _linkNamePairs.end())
    {
        if(iter.value()) //需要检测的碰撞对
        {
            QStringList pair = iter.key().split(",");
            _acm->setEntry(QString(pair[0]).toLocal8Bit().toStdString(), QString(pair[1]).toLocal8Bit().toStdString(), false);
        }
        else//不需要检测的碰撞对
        {
            QStringList pair = iter.key().split(",");
            _acm->setEntry(QString(pair[0]).toLocal8Bit().toStdString(), QString(pair[1]).toLocal8Bit().toStdString(), true);
        }
        iter++;
    }
}

QVector<Model *> Robot::getEndModels() const
{
    return _endModels;
}

bool Robot::getUpdateCurvesRealTime() const
{
    return _updateCurvesRealTime;
}

void Robot::setUpdateCurvesRealTime(bool updateCurvesRealTime)
{
    _updateCurvesRealTime = updateCurvesRealTime;
}

int Robot::getNotFixJointNum() const
{
    return _notFixJointNum;
}

void Robot::setEnableRemoteJoints(bool enable)
{
    _enableRemoteJoints = enable;
}

bool Robot::getEnableRemoteJoints() const
{
    return _enableRemoteJoints;
}

void Robot::showPlot(const QString &name)
{
    if(_jointsPlot.contains(name))
    {
        auto wnd = _jointsPlot[name];
        wnd->show();
        return;
    }

    if(_cartsPlot.contains(name))
    {
        auto wnd = _cartsPlot[name];
        wnd->show();
        return;
    }
    QMessageBox::warning(_parentWidget, "警告",name + "曲线不存在！");
}

const QMap<QString, QVector<Msnhnet::Frame> > &Robot::getPathPoints() const
{
    return _pathPoints;
}

QString Robot::getJointsCtrlAddr() const
{
    return _jointsNN->getAddr();
}

void Robot::init()
{
    _jointsNN = new NNThread(this);
    _jointsNN->setAddr("ipc:///tmp/"+ objectName() + "_joints");
    connect(_jointsNN, &NNThread::revMsg, this, &Robot::revNNJoints);
    _jointsNN->startIt();
}

const QMap<QString, Curve3D *> &Robot::getCurves() const
{
    return _curves;
}

const QMap<QString, Model *> &Robot::getTfModels() const
{
    return _tfModels;
}

bool Robot::plan(const QString &tfName, float vel, float acc, float dt, float radius, float maxFailedRate, Msnhnet::RotationInterpType rotInterpType, Msnhnet::PlanType planType, bool goHome, bool fullCircle, QString &msg)
{
    Msnhnet::Frame baseFrame = _showModel->getFrame();

    Msnhnet::RotationInterp* rotInterp;

    if(planType != Msnhnet::PLAN_JOINT)
    {
        switch (rotInterpType)
        {

        case Msnhnet::ROT_INTERP_AXIS_ANGLE:
            rotInterp = new Msnhnet::RotationInterpSingleAxis();
            break;
        case Msnhnet::ROT_INTERP_AXIS_QUAT:
            rotInterp = new Msnhnet::RotationInterpQuat();
            break;
        }
    }
    switch (planType)
    {
    case Msnhnet::PLAN_JOINT:    //关节空间规划
    {
        std::vector<double> startJoints;
        std::vector<double> endJoints;

        for (uint32_t i = 0; i <_chains[tfName].getNumOfJoints(); ++i)
        {
            double joint1 = _visualJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            double joint2 = _endJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            startJoints.push_back(joint1);
            endJoints.push_back(joint2);
        }

        Msnhnet::VectorXSDS start(startJoints);
        Msnhnet::VectorXSDS end(endJoints);
        Msnhnet::Chain chain = _chains[tfName];
        Curve3D* curve = _curves[tfName];
        _pathPlanJoints[tfName].clear();
        //更新显示3D直线
        QVector<QVector3D> datas;

        {
            Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(start,end); //由traject删除指针
            qDebug()<<vel;
            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                Msnhnet::VectorXSDS joints = traject->getPos(t);
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            // 保证最后一个
            {
                Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            delete traject;
            traject = nullptr;
        }

        if(goHome)
        {
            Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(end,start); //由traject删除指针
            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                Msnhnet::VectorXSDS joints = traject->getPos(t);
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            // 保证最后一个
            {
                Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            delete traject;
            traject = nullptr;
        }

        curve->setLineVertices(datas);

        emit pathPlanned(tfName, _pathPlanJoints[tfName].size());

        _excutionStep[tfName]    = 0;
        _animationStep[tfName]   = 0;

        return true;
    }
    case Msnhnet::PLAN_JOINT_GROUP: //关节空间组规划
    {
        std::vector<double> startJoints;
        std::vector<double> endJoints;

        for (uint32_t i = 0; i <_chains[tfName].getNumOfJoints(); ++i)
        {
            double joint1 = _visualJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            double joint2 = _endJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            startJoints.push_back(joint1);
            endJoints.push_back(joint2);
        }

        Msnhnet::VectorXSDS start(startJoints);
        Msnhnet::VectorXSDS end(endJoints);
        Msnhnet::Chain chain = _chains[tfName];
        Curve3D* curve = _curves[tfName];
        _pathPlanJoints[tfName].clear();
        //更新显示3D直线
        QVector<QVector3D> datas;

        if(_pathPoints[tfName].empty())
        {
            Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(start,end); //由traject删除指针
            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                Msnhnet::VectorXSDS joints = traject->getPos(t);
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            // 保证最后一个
            {
                Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            delete traject;
            traject = nullptr;
        }
        else
        {
            Msnhnet::VectorXSDS startJoints = start;
            Msnhnet::VectorXSDS endJoints   = start;
            auto points = _pathPoints[tfName];
            for(size_t i=0; i<points.size(); i++)
            {
                if(chain.ikLM(points[i], endJoints)<0)
                {
                    return false;
                }

                Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(startJoints,endJoints); //由traject删除指针
                Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

                velProf->setProfile(0,path->getPathLength());

                Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

                for (double t=0.0; t <= traject->getDuration(); t+= dt)
                {
                    Msnhnet::VectorXSDS joints = traject->getPos(t);
                    _pathPlanJoints[tfName].push_back(joints);

                    //更新显示3D直线
                    Msnhnet::Frame frame = chain.fk(joints);
                    frame = baseFrame*frame;
                    datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
                }

                // 保证最后一个
                {
                    Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                    _pathPlanJoints[tfName].push_back(joints);

                    //更新显示3D直线
                    Msnhnet::Frame frame = chain.fk(joints);
                    frame = baseFrame*frame;
                    datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
                }
                delete traject;
                traject = nullptr;

                startJoints = endJoints;
            }

            {
                Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(endJoints,end); //由traject删除指针
                Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

                velProf->setProfile(0,path->getPathLength());

                Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

                for (double t=0.0; t <= traject->getDuration(); t+= dt)
                {
                    Msnhnet::VectorXSDS joints = traject->getPos(t);
                    _pathPlanJoints[tfName].push_back(joints);

                    //更新显示3D直线
                    Msnhnet::Frame frame = chain.fk(joints);
                    frame = baseFrame*frame;
                    datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
                }

                // 保证最后一个
                {
                    Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                    _pathPlanJoints[tfName].push_back(joints);

                    //更新显示3D直线
                    Msnhnet::Frame frame = chain.fk(joints);
                    frame = baseFrame*frame;
                    datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
                }

                delete traject;
                traject = nullptr;
            }
        }

        if(goHome)
        {
            Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(end,start); //由traject删除指针
            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                Msnhnet::VectorXSDS joints = traject->getPos(t);
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            // 保证最后一个
            {
                Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }
            delete traject;
            traject = nullptr;
        }

        curve->setLineVertices(datas);

        emit pathPlanned(tfName, _pathPlanJoints[tfName].size());

        _excutionStep[tfName]    = 0;
        _animationStep[tfName]   = 0;

        return true;
    }
    case Msnhnet::PLAN_LINE: //直线规划
    {
        std::vector<double> startJoints;
        std::vector<double> endJoints;

        for (uint32_t i = 0; i <_chains[tfName].getNumOfJoints(); ++i)
        {
            double joint1 = _visualJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            double joint2 = _endJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            startJoints.push_back(joint1);
            endJoints.push_back(joint2);
        }

        Msnhnet::VectorXSDS start(startJoints);
        Msnhnet::VectorXSDS end(endJoints);

        Msnhnet::Frame startFrame   = _chains[tfName].fk(start);
        Msnhnet::Frame endFrame     = _chains[tfName].fk(end);

        Msnhnet::CartesianPathLine* path    = new Msnhnet::CartesianPathLine(startFrame, endFrame, rotInterp->clone(), 0.01);  //由traject删除指针
        Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc);  //由traject删除指针

        velProf->setProfile(0,path->getPathLength());

        Msnhnet::CartesianTrajectory* traject   = new Msnhnet::CartesianTrajectorySegment(path, velProf);

        _pathPlanJoints[tfName].clear();

        //更新显示3D直线
        QVector<QVector3D> datas;
        Msnhnet::Chain chain = _chains[tfName];
        Curve3D* curve       = _curves[tfName];
        Msnhnet::VectorXSDS joints(startJoints);

        int failed  = 0;
        int count   = 0;

        for (double t=0.0; t <= traject->getDuration(); t+= dt)
        {
            count++;

            Msnhnet::Frame pos = traject->getPos(t);

            if(chain.ikLM(pos, joints)<0)
            {
                failed++;
                continue;
            }

            _pathPlanJoints[tfName].push_back(joints);
            pos = baseFrame*pos;
            //更新显示3D直线
            datas.push_back(QVector3D((float)(pos.trans[0]),(float)(pos.trans[1]),(float)(pos.trans[2])));
        }

        //保证最后一个点
        {
            Msnhnet::Frame pos = traject->getPos(traject->getDuration());
            if(chain.ikLM(pos, joints)<0)
            {
                failed++;
            }
            else
            {
                _pathPlanJoints[tfName].push_back(joints);
                pos = baseFrame*pos;
                //更新显示3D直线
                datas.push_back(QVector3D((float)(pos.trans[0]),(float)(pos.trans[1]),(float)(pos.trans[2])));
            }
        }

        delete traject;
        traject = nullptr;

        if(goHome)
        {
            Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(end,start); //由traject删除指针
            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                Msnhnet::VectorXSDS joints = traject->getPos(t);
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            // 保证最后一个
            {
                Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            delete traject;
            traject = nullptr;
        }

        if(1.0*failed/count > maxFailedRate)
        {
            msg = "Plan failed more than " + QString::number(maxFailedRate*100) +"%!";

            if(rotInterp)
            {
                delete rotInterp;
                rotInterp = nullptr;
            }
            return false;
        }
        else
        {
            curve->setLineVertices(datas);
            emit pathPlanned(tfName, _pathPlanJoints[tfName].size());
            _excutionStep[tfName]    = 0;
            _animationStep[tfName]   = 0;

            if(rotInterp)
            {
                delete rotInterp;
                rotInterp = nullptr;
            }

            return true;
        }
    }
    case Msnhnet::PLAN_LINE_GROUP: //直线组规划
    {
        std::vector<double> startJoints;
        std::vector<double> endJoints;

        for (uint32_t i = 0; i <_chains[tfName].getNumOfJoints(); ++i)
        {
            double joint1 = _visualJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            double joint2 = _endJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            startJoints.push_back(joint1);
            endJoints.push_back(joint2);
        }

        Msnhnet::VectorXSDS start(startJoints);
        Msnhnet::VectorXSDS end(endJoints);

        Msnhnet::Frame startFrame   = _chains[tfName].fk(start);
        Msnhnet::Frame endFrame     = _chains[tfName].fk(end);

        Msnhnet::CartesianPathComposite *path = new Msnhnet::CartesianPathComposite();

        if(!_pathPoints[tfName].empty())
        {
            //首点
            Msnhnet::CartesianPathLine* lineStart = new Msnhnet::CartesianPathLine(startFrame,  _pathPoints[tfName][0], rotInterp->clone(), 0.01);
            path->add(lineStart);


            if(_pathPoints[tfName].size()>1)
            {
                for (int i = 0; i < _pathPoints[tfName].size()-1; ++i) //内点
                {
                    Msnhnet::CartesianPathLine* line   = new Msnhnet::CartesianPathLine(_pathPoints[tfName][i], _pathPoints[tfName][i+1], rotInterp->clone(), 0.01);
                    path->add(line);
                }

                Msnhnet::CartesianPathLine* lineEnd = new Msnhnet::CartesianPathLine(_pathPoints[tfName][_pathPoints[tfName].size()-1], endFrame, rotInterp->clone(), 0.01);
                path->add(lineEnd);
            }
            else //尾点
            {
                Msnhnet::CartesianPathLine* lineEnd = new Msnhnet::CartesianPathLine(_pathPoints[tfName][0], endFrame, rotInterp->clone(), 0.01);
                path->add(lineEnd);
            }

        }
        else
        {
            Msnhnet::CartesianPathLine* line = new Msnhnet::CartesianPathLine(startFrame, endFrame, rotInterp->clone(), 0.01);
            path->add(line);
        }

        Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc);

        velProf->setProfile(0,path->getPathLength());

        Msnhnet::CartesianTrajectory* traject   = new Msnhnet::CartesianTrajectorySegment(path, velProf);

        _pathPlanJoints[tfName].clear();

        //更新显示3D直线
        QVector<QVector3D> datas;
        Msnhnet::Chain chain = _chains[tfName];
        Curve3D* curve       = _curves[tfName];
        Msnhnet::VectorXSDS joints(startJoints);

        int failed  = 0;
        int count   = 0;

        for (double t=0.0; t <= traject->getDuration(); t+= dt)
        {
            count++;

            Msnhnet::Frame pos = traject->getPos(t);

            if(chain.ikLM(pos, joints)<0)
            {
                failed++;
                continue;
            }

            _pathPlanJoints[tfName].push_back(joints);

            //更新显示3D直线
            pos = baseFrame*pos;
            datas.push_back(QVector3D((float)(pos.trans[0]),(float)(pos.trans[1]),(float)(pos.trans[2])));
        }

        //保证最后一个点
        {
            Msnhnet::Frame pos = traject->getPos(traject->getDuration());
            if(chain.ikLM(pos, joints)<0)
            {
                failed++;
            }
            else
            {
                _pathPlanJoints[tfName].push_back(joints);
                pos = baseFrame*pos;
                //更新显示3D直线
                datas.push_back(QVector3D((float)(pos.trans[0]),(float)(pos.trans[1]),(float)(pos.trans[2])));
            }
        }

        delete traject;
        traject = nullptr;

        if(goHome)
        {
            Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(joints,start); //由traject删除指针
            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                Msnhnet::VectorXSDS joints = traject->getPos(t);
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }
            // 保证最后一个
            {
                Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            delete traject;
            traject = nullptr;
        }

        if(1.0*failed/count >maxFailedRate)
        {
            msg = "Plan failed more than " + QString::number(maxFailedRate*100) +"%!";

            if(rotInterp)
            {
                delete rotInterp;
                rotInterp = nullptr;
            }

            return false;
        }
        else
        {
            curve->setLineVertices(datas);
            emit pathPlanned(tfName, _pathPlanJoints[tfName].size());
            _excutionStep[tfName]    = 0;
            _animationStep[tfName]   = 0;

            if(rotInterp)
            {
                delete rotInterp;
                rotInterp = nullptr;
            }
            return true;
        }
    }
    case Msnhnet::PLAN_ROUND_LINE_GROUP: //圆角直线组规划
    {
        try
        {
            std::vector<double> startJoints;
            std::vector<double> endJoints;

            for (uint32_t i = 0; i <_chains[tfName].getNumOfJoints(); ++i)
            {
                double joint1 = _visualJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
                double joint2 = _endJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
                startJoints.push_back(joint1);
                endJoints.push_back(joint2);
            }

            Msnhnet::VectorXSDS start(startJoints);
            Msnhnet::VectorXSDS end(endJoints);

            Msnhnet::Frame startFrame   = _chains[tfName].fk(start);
            Msnhnet::Frame endFrame     = _chains[tfName].fk(end);

            Msnhnet::CartesianPathRoundComposite* path = new Msnhnet::CartesianPathRoundComposite(radius,0.01, rotInterp->clone(),true);  //由traject删除指针

            path->add(startFrame);
            for (int i = 0; i < _pathPoints[tfName].size(); ++i) //内点
            {
                path->add(_pathPoints[tfName][i]);
            }
            path->add(endFrame);
            path->finish();

            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc);    //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::CartesianTrajectory* traject   = new Msnhnet::CartesianTrajectorySegment(path, velProf);

            _pathPlanJoints[tfName].clear();

            //更新显示3D直线
            QVector<QVector3D> datas;
            Msnhnet::Chain chain = _chains[tfName];
            Curve3D* curve       = _curves[tfName];
            Msnhnet::VectorXSDS joints(startJoints);

            int failed  = 0;
            int count   = 0;

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                count++;

                Msnhnet::Frame pos = traject->getPos(t);

                if(chain.ikLM(pos, joints)<0)
                {
                    failed++;
                    continue;
                }

                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                pos = baseFrame*pos;
                datas.push_back(QVector3D((float)(pos.trans[0]),(float)(pos.trans[1]),(float)(pos.trans[2])));
            }
            //保证最后一个点
            {
                Msnhnet::Frame pos = traject->getPos(traject->getDuration());
                if(chain.ikLM(pos, joints)<0)
                {
                    failed++;
                }
                else
                {
                    _pathPlanJoints[tfName].push_back(joints);
                    pos = baseFrame*pos;
                    //更新显示3D直线
                    datas.push_back(QVector3D((float)(pos.trans[0]),(float)(pos.trans[1]),(float)(pos.trans[2])));
                }
            }

            delete traject;
            traject = nullptr;

            if(goHome)
            {
                Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(joints,start); //由traject删除指针
                Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

                velProf->setProfile(0,path->getPathLength());

                Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

                for (double t=0.0; t <= traject->getDuration(); t+= dt)
                {
                    Msnhnet::VectorXSDS joints = traject->getPos(t);
                    _pathPlanJoints[tfName].push_back(joints);

                    //更新显示3D直线
                    Msnhnet::Frame frame = chain.fk(joints);
                    frame = baseFrame*frame;
                    datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
                }

                // 保证最后一个
                {
                    Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                    _pathPlanJoints[tfName].push_back(joints);

                    //更新显示3D直线
                    Msnhnet::Frame frame = chain.fk(joints);
                    frame = baseFrame*frame;
                    datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
                }

                delete traject;
                traject = nullptr;
            }

            if(1.0*failed/count > maxFailedRate)
            {
                if(rotInterp)
                {
                    delete rotInterp;
                    rotInterp = nullptr;
                }
                msg = "Plan failed more than " + QString::number(maxFailedRate*100) +"%!";
                return false;
            }
            else
            {
                curve->setLineVertices(datas);
                emit pathPlanned(tfName, _pathPlanJoints[tfName].size());

                _excutionStep[tfName]    = 0;
                _animationStep[tfName]   = 0;

                if(rotInterp)
                {
                    delete rotInterp;
                    rotInterp = nullptr;
                }

                return true;
            }
        }
        catch(Msnhnet::Exception ex)
        {
            if(rotInterp)
            {
                delete rotInterp;
                rotInterp = nullptr;
            }
            msg = QString(ex.what());
            return false;
        }
    }
    case Msnhnet::PLAN_CIRCLE:  //圆弧规划
    {
        if(_pathPoints[tfName].size()!=2)
        {
            msg = "3点定圆, 记录点个数必须为2";
            return false;
        }

        std::vector<double> startJoints;
        std::vector<double> endJoints;

        for (uint32_t i = 0; i <_chains[tfName].getNumOfJoints(); ++i)
        {
            double joint1 = _visualJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            double joint2 = _endJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
            startJoints.push_back(joint1);
            endJoints.push_back(joint2);
        }

        Msnhnet::VectorXSDS start(startJoints);
        Msnhnet::VectorXSDS end(endJoints);
        Msnhnet::VectorXSDS startCircle(startJoints);

        Msnhnet::Frame endFrame     = _chains[tfName].fk(end);

        if(_chains[tfName].ikLM(_pathPoints[tfName][0],startCircle)<0)
        {
            msg = "圆起点逆解失败！";
            return false;
        }

        _pathPlanJoints[tfName].clear();

        //更新显示3D直线
        QVector<QVector3D> datas;
        Msnhnet::Chain chain = _chains[tfName];
        Curve3D* curve       = _curves[tfName];

        // 起始到圆的第一个点
        {
            Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(start,startCircle); //由traject删除指针
            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                Msnhnet::VectorXSDS joints = traject->getPos(t);
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }
            delete traject;
            traject = nullptr;
        }


        int failed  = 0;
        int count   = 0;

        // 圆
        {
            Msnhnet::CartesianPathCircle* path    = new Msnhnet::CartesianPathCircle(_pathPoints[tfName][0],_pathPoints[tfName][1],endFrame,rotInterp->clone(),0.01,fullCircle);  //由traject删除指针
            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc);  //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::CartesianTrajectory* traject   = new Msnhnet::CartesianTrajectorySegment(path, velProf);
            Msnhnet::VectorXSDS joints(startJoints);

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                count++;

                Msnhnet::Frame pos = traject->getPos(t);

                if(chain.ikLM(pos, joints)<0)
                {
                    failed++;
                    continue;
                }

                _pathPlanJoints[tfName].push_back(joints);
                pos = baseFrame*pos;
                //更新显示3D直线
                datas.push_back(QVector3D((float)(pos.trans[0]),(float)(pos.trans[1]),(float)(pos.trans[2])));
            }

            //保证最后一个点
            {
                Msnhnet::Frame pos = traject->getPos(traject->getDuration());
                if(chain.ikLM(pos, joints)<0)
                {
                    failed++;
                }
                else
                {
                    _pathPlanJoints[tfName].push_back(joints);
                    pos = baseFrame*pos;
                    //更新显示3D直线
                    datas.push_back(QVector3D((float)(pos.trans[0]),(float)(pos.trans[1]),(float)(pos.trans[2])));
                }
            }

            if(fullCircle)
            {
                end = joints;
            }

            delete traject;
            traject = nullptr;
        }

        if(goHome)
        {
            Msnhnet::SingleJointPath* path      = new Msnhnet::SingleJointPath(end,start); //由traject删除指针
            Msnhnet::VelocityProfile* velProf   = new Msnhnet::VelocityProfileTrap(vel,acc); //由traject删除指针

            velProf->setProfile(0,path->getPathLength());

            Msnhnet::JointTrajectory* traject   = new Msnhnet::JointTrajectorySegment(path, velProf);

            for (double t=0.0; t <= traject->getDuration(); t+= dt)
            {
                Msnhnet::VectorXSDS joints = traject->getPos(t);
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            // 保证最后一个
            {
                Msnhnet::VectorXSDS joints = traject->getPos(traject->getDuration());
                _pathPlanJoints[tfName].push_back(joints);

                //更新显示3D直线
                Msnhnet::Frame frame = chain.fk(joints);
                frame = baseFrame*frame;
                datas.push_back(QVector3D((float)(frame.trans[0]),(float)(frame.trans[1]),(float)(frame.trans[2])));
            }

            delete traject;
            traject = nullptr;
        }

        if(1.0*failed/count > maxFailedRate)
        {
            msg = "Plan failed more than " + QString::number(maxFailedRate*100) +"%!";

            if(rotInterp)
            {
                delete rotInterp;
                rotInterp = nullptr;
            }
            return false;
        }
        else
        {
            curve->setLineVertices(datas);
            emit pathPlanned(tfName, _pathPlanJoints[tfName].size());
            _excutionStep[tfName]    = 0;
            _animationStep[tfName]   = 0;

            if(rotInterp)
            {
                delete rotInterp;
                rotInterp = nullptr;
            }

            return true;
        }
    }
    }

    return false;
}

bool Robot::runPlan(const QString &tfName, QString &msg)
{
    if(_pathPlanJoints[tfName].empty())
    {
        msg = "No joint plan joints";
        return false;
    }

    _excutionPlan[tfName]   = true;
    _excutionStep[tfName]   = 0;

    return true;
}

bool Robot::animatePlan(const QString &tfName, bool run, QString &msg)
{
    if(run)
    {
        if(_pathPlanJoints[tfName].empty())
        {
            msg = "No joint plan joints";
            return false;
        }
        _animationPlan[tfName]   = true;
        _animationStep[tfName]  = _excutionStep[tfName];
        _animationTree->setVisible(true);
    }
    else
    {
        _animationPlan[tfName]   = false;
        _animationStep[tfName]  = _excutionStep[tfName];
        _animationTree->setVisible(false);
    }
    return true;
}

void Robot::gotoJoints(const QString &tfName,const Msnhnet::VectorXSDS &joints)
{
    Model* md = _tfModels[tfName];
    auto chain = _chains[tfName];

    if(((uint32_t)joints.mN) != chain.getNumOfJoints())
    {
        throw Msnhnet::Exception(1,"Num joints != input joints", __FILE__, __LINE__, __FUNCTION__);
    }

    auto frame = chain.fk(joints);

    md->setFrame(frame);

    emit updateFrame(tfName, frame);

    for (int i = 0; i < joints.mN; ++i)
    {
        _collisionJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->singleMove((float)joints[i]*MSNH_RAD_2_DEG);
        _endJoints[QString::fromStdString(chain.getSegment(i).getName())]->singleMove((float)joints[i]*MSNH_RAD_2_DEG); //运行
    }
}

void Robot::gotoZero(const QString &tfName)
{
    auto chain = _chains[tfName];
    Msnhnet::VectorXSDS jointsZero(chain.getNumOfJoints());
    gotoJoints(tfName, jointsZero);
}

void Robot::syncRobot(const QString &tfName)
{

    QVector<QString> names = _tfChainNames[tfName];
    int cnt = 0;
    for(QString name : names)
    {
        if(_visualJoints[name]->getJointType() != Msnhnet::JointType::JOINT_FIXED)
        {
            float singleMove = _visualJoints[name]->getSingleMove();
            _endJoints[name]->singleMove(singleMove);
            _joints[tfName][++cnt] = singleMove*MSNH_DEG_2_RAD;
        }
    }

    updateTfTools();
}

void Robot::addWayPoint(const QString &tfName)
{
    std::vector<double> endJoints;

    for (uint32_t i = 0; i <_chains[tfName].getNumOfJoints(); ++i)
    {
        double joint = _endJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
        endJoints.push_back(joint);
    }
    Msnhnet::VectorXSDS end(endJoints);

    Msnhnet::Frame frame = _chains[tfName].fk(end);

    int axesCnt = (_tfGroupAxesCnt[tfName] += 1);

    _tfAxesGroup[tfName]->addTf(tfName+"_Axes_"+QString::number(axesCnt),frame,_routeAxesScale);

    _pathPoints[tfName].push_back(frame);

    emit refreshPoints(tfName,_pathPoints[tfName]);
}

void Robot::insertWayPoint(const QString &tfName, int index)
{
    std::vector<double> endJoints;

    for (uint32_t i = 0; i <_chains[tfName].getNumOfJoints(); ++i)
    {
        double joint = _endJoints[QString::fromStdString(_chains[tfName].getSegment(i).getName())]->getSingleMove()/180.f*MSNH_PI;
        endJoints.push_back(joint);
    }
    Msnhnet::VectorXSDS end(endJoints);

    Msnhnet::Frame frame = _chains[tfName].fk(end);

    int axesCnt = (_tfGroupAxesCnt[tfName] += 1);

    _tfAxesGroup[tfName]->addTf(tfName+"_Axes_"+QString::number(axesCnt),frame,_routeAxesScale);

    _pathPoints[tfName].insert(index,frame);

    emit refreshPoints(tfName,_pathPoints[tfName]);
}

void Robot::deleteWayPoint(const QString &tfName, int index)
{
    _tfAxesGroup[tfName]->deleteTf(index);
    _pathPoints[tfName].removeAt(index);
}

void Robot::clearWayPoints(const QString &tfName)
{
    _pathPoints[tfName].clear();
    auto group = _tfAxesGroup[tfName];
    int num = group->getTfVecSize();

    for (int i = 0; i < num; ++i)
    {
        group->deleteTf(0);
    }
}

void Robot::endEffortToSelected(const QString &tfName, int index)
{
    Msnhnet::Frame frame = _pathPoints[tfName][index];
    auto trans = frame.trans;
    auto euler = Msnhnet::GeometryS::rotMat2Euler(frame.rotMat, Msnhnet::ROT_ZYX);

    //末端球
    _tfModels[tfName]->setPositionApi(QVector3D((float)trans[0],(float)trans[1],(float)trans[2]));
    _tfModels[tfName]->setRotationApi(QVector3D((float)(euler[0]/MSNH_PI*180.0),(float)(euler[1]/MSNH_PI*180.0),(float)(euler[2]/MSNH_PI*180.0)));
}

void Robot::changeEndEffort(const QString &tfName, float x, float y, float z, float rx, float ry, float rz)
{
    _tfModels[tfName]->setPositionApi(QVector3D(x,y,z));
    _tfModels[tfName]->setRotationApi(QVector3D(rx,ry,rz));
}

int Robot::getPlannedJointsSize(const QString &tfName)
{
    if(_pathPlanJoints.contains(tfName))
    {
        return _pathPlanJoints[tfName].size();
    }
    else
    {
        return -1;
    }
}

QString Robot::getPublishAddr(const QString &tfName) const
{
    if(_tfPubAddrs.contains(tfName))
    {
        return _tfPubAddrs[tfName];
    }
    else
    {
        return "";
    }
}

const QMap<QString, Msnhnet::Frame> &Robot::getLastTfFrame() const
{
    return _lastTfFrame;
}
