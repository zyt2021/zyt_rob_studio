﻿#include <Core/MsnhRotateGizmo.h>
#include <Core/MsnhModelLoader.h>

RotateGizmo::RotateGizmo( QObject* parent ) : AbstractGizmo( parent )
{
    setObjectName( "Rotation Gizmo" );
    _markers.resize( 3 );

    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    ModelLoader loader;
    _markers[0]	= loader.loadMeshFromFile( ":/shapes/RotX.obj" );
    _markers[1]	= loader.loadMeshFromFile( ":/shapes/RotY.obj" );
    _markers[2]	= loader.loadMeshFromFile( ":/shapes/RotZ.obj" );

    _markers[0]->getMaterial()->setColor( QVector3D( 0.66f, 0, 0 ) );
    _markers[1]->getMaterial()->setColor( QVector3D( 0, 0.66f, 0 ) );
    _markers[2]->getMaterial()->setColor( QVector3D( 0, 0, 0.66f ) );

    for ( int i = 0; i < _markers.size(); i++ )
    {
        _markers[i]->setObjectName( "Rotate Marker" );
        _markers[i]->setParent( this );
    }

    logLevel = tmp_log_level;
}


RotateGizmo::~RotateGizmo()
{
}


void RotateGizmo::translate( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Translating a ROTATION ONLY gizmo is not allowed";
}


void RotateGizmo::rotate( QQuaternion rotation )
{
    if ( _host )
        _host->rotate( rotation );
}


void RotateGizmo::rotate( QVector3D rotation )
{
    if ( _host )
        _host->rotate( rotation );
}


void RotateGizmo::scale( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Scaling a ROTATION ONLY gizmo is not allowed";
}


QVector3D RotateGizmo::getPosition() const
{
    if ( _host )
        return(_host->getPosition() );
    else
        return(QVector3D( 0, 0, 0 ) );
}


QVector3D RotateGizmo::getRotation() const
{
    if ( _host )
        return(_host->getRotation() );
    else
        return(QVector3D( 0, 0, 0 ) );
}


QVector3D RotateGizmo::getScaling() const
{
    return(QVector3D( 1, 1, 1 ) );
}


QMatrix4x4 RotateGizmo::getGlobalSpaceMatrix() const
{
    QMatrix4x4 model = getLocalModelMatrix();
    if ( _host )
    {
        AbstractEntity* parent = qobject_cast<AbstractEntity*>( _host->parent() );
        //        while ( parent )
        //        {
        //            model	= parent->getGlobalModelMatrix() * model;
        //            parent	= qobject_cast<AbstractEntity*>( parent->parent() );
        //        }

        //只取最近的一个parent的位置信息就行
        while ( parent )
        {
            if(parent->isModel()) //找到了就退出
            {
                model	= parent->getGlobalModelMatrix() * model;
                parent	= nullptr;
            }
            else //没找到继续找
            {
                parent	= qobject_cast<AbstractEntity*>( parent->parent() );
            }
        }
    }

    return(model);
}


QMatrix4x4 RotateGizmo::getGlobalModelMatrix() const
{
    QMatrix4x4 model;
    model.translate( getGlobalSpaceMatrix() * QVector3D( 0, 0, 0 ) );

    if ( _host )
    {
        QQuaternion	globalRotation	= QQuaternion::fromEulerAngles( getRotation() ); //旋转叠加，gizmo同步model
        AbstractEntity	* parent	= qobject_cast<AbstractEntity*>( _host->parent() );
        while ( parent )
        {
            globalRotation	= QQuaternion::fromEulerAngles( parent->getRotation() ) * globalRotation;
            parent          = qobject_cast<AbstractEntity*>( parent->parent() );
        }
        model.rotate( globalRotation );
    }

    return(model);
}


void RotateGizmo::drag( QPoint from, QPoint to, int scnWidth, int scnHeight, QMatrix4x4 proj, QMatrix4x4 view )
{
    if ( _host == nullptr )
        return;
    Line		l1          = screenPosToWorldRay( QVector2D( from ), QVector2D( scnWidth, scnHeight ), proj, view );
    Line		l2          = screenPosToWorldRay( QVector2D( to ), QVector2D( scnWidth, scnHeight ), proj, view );
    QMatrix4x4	invModelMat         = getGlobalSpaceMatrix().inverted();
    l1                              = invModelMat * l1;
    l2                              = invModelMat * l2;
    if ( _axis == GIZMO_AXIS_X )
    {
        QVector3D	p1      = getIntersectionOfLinePlane( l1, { QVector3D( 0, 0, 0 ), QVector3D( 1, 0, 0 ) } );
        QVector3D	p2      = getIntersectionOfLinePlane( l2, { QVector3D( 0, 0, 0 ), QVector3D( 1, 0, 0 ) } );
        float		theta	= qAcos( qMin( qMax( QVector3D::dotProduct( p1, p2 ) / p1.length() / p2.length(), -1.0f ), 1.0f ) );
        if ( QVector3D::dotProduct( QVector3D( 1, 0, 0 ), QVector3D::crossProduct( p1, p2 ) ) < 0 )
            theta = -theta;
        rotate( theta * QVector3D( 180.0f / 3.1415926f, 0.0f, 0.0f ) );
    } else if ( _axis == GIZMO_AXIS_Y )
    {
        QVector3D	p1      = getIntersectionOfLinePlane( l1, { QVector3D( 0, 0, 0 ), QVector3D( 0, 1, 0 ) } );
        QVector3D	p2      = getIntersectionOfLinePlane( l2, { QVector3D( 0, 0, 0 ), QVector3D( 0, 1, 0 ) } );
        float		theta	= qAcos( qMin( qMax( QVector3D::dotProduct( p1, p2 ) / p1.length() / p2.length(), -1.0f ), 1.0f ) );
        if ( QVector3D::dotProduct( QVector3D( 0, 1, 0 ), QVector3D::crossProduct( p1, p2 ) ) < 0 )
            theta = -theta;
        rotate( theta * QVector3D( 0.0f, 180.0f / 3.1415926f, 0.0f ) );
    } else if ( _axis == GIZMO_AXIS_Z )
    {
        QVector3D	p1      = getIntersectionOfLinePlane( l1, { QVector3D( 0, 0, 0 ), QVector3D( 0, 0, 1 ) } );
        QVector3D	p2      = getIntersectionOfLinePlane( l2, { QVector3D( 0, 0, 0 ), QVector3D( 0, 0, 1 ) } );
        float		theta	= qAcos( qMin( qMax( QVector3D::dotProduct( p1, p2 ) / p1.length() / p2.length(), -1.0f ), 1.0f ) );
        if ( QVector3D::dotProduct( QVector3D( 0, 0, 1 ), QVector3D::crossProduct( p1, p2 ) ) < 0 )
            theta = -theta;
        rotate( theta * QVector3D( 0.0f, 0.0f, 180.0f / 3.1415926f ) );
    }
}


void RotateGizmo::setPosition( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Setting the position of a ROTATION ONLY gizmo is not allowed";
}


void RotateGizmo::setRotation( QQuaternion rotation )
{
    if ( _host )
        _host->setRotation( rotation );
}


void RotateGizmo::setRotation( QVector3D rotation )
{
    if ( _host )
        _host->setRotation( rotation );
}


void RotateGizmo::setScaling( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Setting the scaling of a ROTATION ONLY gizmo is not allowed";
}


