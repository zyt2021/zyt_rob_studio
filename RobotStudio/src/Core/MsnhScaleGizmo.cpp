﻿#include <Core/MsnhScaleGizmo.h>
#include <Core/MsnhModelLoader.h>

ScaleGizmo::ScaleGizmo( QObject* parent ) : AbstractGizmo( parent )
{
    setObjectName( "Scaling Gizmo" );
    _markers.resize( 7 );

    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    ///              y
    ///              |
    ///              |   ___
    ///          /|  |  |   |
    ///         | |  |  |___|
    ///         |/   |_____________ x
    ///             /    __
    ///            /   /   /
    ///           /   /__ /
    ///          /
    ///         z
    ModelLoader loader;
    _markers[0]	= loader.loadMeshFromFile( ":/shapes/ScaleX.obj" );
    _markers[1]	= loader.loadMeshFromFile( ":/shapes/ScaleY.obj" );
    _markers[2]	= loader.loadMeshFromFile( ":/shapes/ScaleZ.obj" );

    _markers[3]	= loader.loadMeshFromFile( ":/shapes/XY.obj" );
    _markers[4]	= loader.loadMeshFromFile( ":/shapes/YZ.obj" );
    _markers[5]	= loader.loadMeshFromFile( ":/shapes/ZX.obj" );
    _markers[6]	= loader.loadMeshFromFile( ":/shapes/XYZ.obj" );

    _markers[3]->setAlpha(0.6f);
    _markers[4]->setAlpha(0.6f);
    _markers[5]->setAlpha(0.6f);

    _markers[0]->getMaterial()->setColor( QVector3D( 0.66f, 0, 0 ) );
    _markers[1]->getMaterial()->setColor( QVector3D( 0, 0.66f, 0 ) );
    _markers[2]->getMaterial()->setColor( QVector3D( 0, 0, 0.66f ) );

    _markers[3]->getMaterial()->setColor( QVector3D( 0, 0, 0.66f ) );
    _markers[4]->getMaterial()->setColor( QVector3D( 0.66f, 0, 0 ) );
    _markers[5]->getMaterial()->setColor( QVector3D( 0, 0.66f, 0 ) );
    _markers[6]->getMaterial()->setColor( QVector3D( 0.66f, 0.66f, 0.66f ) );

    for ( int i = 0; i < _markers.size(); i++ )
    {
        _markers[i]->setObjectName( "Scale Marker" );
        _markers[i]->setParent( this );
    }

    logLevel = tmp_log_level;
}


ScaleGizmo::~ScaleGizmo()
{
}


void ScaleGizmo::translate( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Translating a SCALING ONLY gizmo is not allowed.";
}


void ScaleGizmo::rotate( QQuaternion )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Rotating a SCALING ONLY gizmo is not allowed.";
}


void ScaleGizmo::rotate( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Rotating a SCALING ONLY gizmo is not allowed.";
}


void ScaleGizmo::scale( QVector3D scaling )
{
    if ( _host )
        _host->scale( scaling );
}


QVector3D ScaleGizmo::getPosition() const
{
    if ( _host )
        return(_host->getPosition() );
    else
        return(QVector3D( 0, 0, 0 ) );
}


QVector3D ScaleGizmo::getRotation() const
{
    if ( _host )
        return(_host->getRotation() );
    else
        return(QVector3D( 0, 0, 0 ) );
}


QVector3D ScaleGizmo::getScaling() const
{
    if ( _host )
        return(_host->getScaling() );
    else
        return(QVector3D( 0, 0, 0 ) );
}


QMatrix4x4 ScaleGizmo::getGlobalSpaceMatrix() const
{
    QMatrix4x4 model = getLocalModelMatrix();

    if ( _host )
    {
        AbstractEntity* parent = qobject_cast<AbstractEntity*>( _host->parent() );
        //        while ( parent )
        //        {
        //            model	= parent->getGlobalModelMatrix() * model;
        //            parent	= qobject_cast<AbstractEntity*>( parent->parent() );
        //        }

        //只取最近的一个parent的位置信息就行
        while ( parent )
        {
            if(parent->isModel()) //找到了就退出
            {
                model	= parent->getGlobalModelMatrix() * model;
                parent	= nullptr;
            }
            else //没找到继续找
            {
                parent	= qobject_cast<AbstractEntity*>( parent->parent() );
            }
        }
    }

    return(model);
}


QMatrix4x4 ScaleGizmo::getGlobalModelMatrix() const
{
    QMatrix4x4 model;
    model.translate( getGlobalSpaceMatrix() * QVector3D( 0, 0, 0 ) );

    if ( _host )
    {
        QQuaternion	globalRotation	= QQuaternion::fromEulerAngles( getRotation() );  //旋转叠加，gizmo同步model
        AbstractEntity	* parent	= qobject_cast<AbstractEntity*>( _host->parent() );
        while ( parent )
        {
            globalRotation	= QQuaternion::fromEulerAngles( parent->getRotation() ) * globalRotation;
            parent		= qobject_cast<AbstractEntity*>( parent->parent() );
        }
        model.rotate( globalRotation );
    }

    return(model);
}


void ScaleGizmo::drag( QPoint from, QPoint to, int scnWidth, int scnHeight, QMatrix4x4 proj, QMatrix4x4 view )
{
    if ( _host == nullptr )
        return;

    if(_host->isLight())
    {
        return;
    }

    Line		l1	= screenPosToWorldRay( QVector2D( from ), QVector2D( scnWidth, scnHeight ), proj, view );
    Line		l2	= screenPosToWorldRay( QVector2D( to ), QVector2D( scnWidth, scnHeight ), proj, view );
    QMatrix4x4	invModelMat	= getGlobalSpaceMatrix().inverted();
    l1	= invModelMat * l1;
    l2	= invModelMat * l2;
    if ( _axis == GIZMO_AXIS_X )
    {
        Line		x	= { QVector3D( 0, 0, 0 ), QVector3D( 1, 0, 0 ) };
        QVector3D	p1	= getClosestPointOfLines( x, l1 );
        QVector3D	p2	= getClosestPointOfLines( x, l2 );
        scale( QVector3D( p2.x() / p1.x(), 1.0f, 1.0f ) );
    }
    else if ( _axis == GIZMO_AXIS_Y )
    {
        Line		y	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 1, 0 ) };
        QVector3D	p1	= getClosestPointOfLines( y, l1 );
        QVector3D	p2	= getClosestPointOfLines( y, l2 );
        scale( QVector3D( 1.0f, p2.y() / p1.y(), 1.0f ) );
    }
    else if ( _axis == GIZMO_AXIS_Z )
    {
        Line		z	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 0, 1 ) };
        QVector3D	p1	= getClosestPointOfLines( z, l1 );
        QVector3D	p2	= getClosestPointOfLines( z, l2 );
        scale( QVector3D( 1.0f, 1.0f, p2.z() / p1.z() ) );
    }
    else if ( _axis == GIZMO_AXIS_XY )
    {
        Line		x	= { QVector3D( 0, 0, 0 ), QVector3D( 1, 0, 0 ) };
        QVector3D	p1	= getClosestPointOfLines( x, l1 );
        QVector3D	p2	= getClosestPointOfLines( x, l2 );


        scale( QVector3D( p2.x() / p1.x(), p2.x() / p1.x(), 1.0f ) );
    }
    else if ( _axis == GIZMO_AXIS_YZ )
    {
        Line		x	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 0, 1 ) };
        QVector3D	p1	= getClosestPointOfLines( x, l1 );
        QVector3D	p2	= getClosestPointOfLines( x, l2 );
        scale( QVector3D( 1.0f, p2.z() / p1.z(), p2.z() / p1.z()) );
    }
    else if ( _axis == GIZMO_AXIS_ZX )
    {
        Line		x	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 1, 0 ) };
        QVector3D	p1	= getClosestPointOfLines( x, l1 );
        QVector3D	p2	= getClosestPointOfLines( x, l2 );
        scale( QVector3D( p2.y() / p1.y(), 1.0f, p2.y() / p1.y()) );
    }
    else if ( _axis == GIZMO_AXIS_XYZ )
    {
        Line		x	= { QVector3D( 0, 0, 0 ), QVector3D( 1, 0, 0 ) };
        QVector3D	p1	= getClosestPointOfLines( x, l1 );
        QVector3D	p2	= getClosestPointOfLines( x, l2 );
        scale( QVector3D( p2.x() / p1.x(), p2.x() / p1.x(), p2.x() / p1.x()) );
    }

}


void ScaleGizmo::setPosition( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Setting the position of a SCALING ONLY gizmo is not allowed";
}


void ScaleGizmo::setRotation( QQuaternion )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Setting the rotation of a SCALING ONLY gizmo is not allowed";
}


void ScaleGizmo::setRotation( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Setting the rotation of a SCALING ONLY gizmo is not allowed";
}


void ScaleGizmo::setScaling( QVector3D scaling )
{
    if ( _host )
        _host->setScaling( scaling );
}


