﻿#include <Core/MsnhScene.h>

Scene::Scene(QObject *parent) : QObject( parent )
{
    setObjectName( "Untitled Scene" );
    _gizmo                          = new TransformGizmo( this );
    _camera                         = new Camera( this );
    _gridlineNameCounter            = 0;
    _ambientLightNameCounter        = 0;
    _directionalLightNameCounter    = 0;
    _pointLightNameCounter          = 0;
    _spotLightNameCounter           = 0;
    _curveNameCounter               = 0;
    _robotNameCounter               = 0;
    _pointsCloudNameCounter         = 0;
}


/* Add & remove members */

Scene::Scene(const Scene & scene , QObject *parent) : QObject( parent )
{
    setObjectName( scene.objectName() );

    _gizmo                         = new TransformGizmo( this );
    _camera                        = new Camera( *scene._camera, this);
    _gridlineNameCounter           = scene._gridlineNameCounter;
    _ambientLightNameCounter       = scene._ambientLightNameCounter;
    _directionalLightNameCounter   = scene._directionalLightNameCounter;
    _pointLightNameCounter         = scene._pointLightNameCounter;
    _spotLightNameCounter          = scene._spotLightNameCounter;
    _curveNameCounter              = scene._curveNameCounter;
    _robotNameCounter              = scene._robotNameCounter;
    _pointsCloudNameCounter        = scene._pointsCloudNameCounter;

    // add new item step 0
    for ( int i = 0; i < scene._gridlines.size(); i++ )
        addGridline( new Gridline( *scene._gridlines[i] ) );
    for ( int i = 0; i < scene._curves.size(); i++ )
        addCurve(new Curve3D( *scene._curves[i] ) );
    for ( int i = 0; i < scene._tfs.size(); i++ )
        addTf(new Transform( *scene._tfs[i] ) );
    for ( int i = 0; i < scene._tfs.size(); i++ )
        addTfGroup(new TransformGroup( *scene._tfGroups[i] ));
    for ( int i = 0; i < scene._ambientLights.size(); i++ )
        addAmbientLight( new AmbientLight( *scene._ambientLights[i] ) );
    for ( int i = 0; i < scene._directionalLights.size(); i++ )
        addDirectionalLight( new DirectionalLight( *scene._directionalLights[i] ) );
    for ( int i = 0; i < scene._pointLights.size(); i++ )
        addPointLight( new PointLight( *scene._pointLights[i] ) );
    for ( int i = 0; i < scene._spotLights.size(); i++ )
        addSpotLight( new SpotLight( *scene._spotLights[i] ) );
    for ( int i = 0; i < scene._models.size(); i++ )
        addModel( new Model( *scene._models[i] ) );
    for ( int i = 0; i < scene._robots.size(); i++ )
        addRobot(new Robot( *scene._robots[i] ) );
    for (int i = 0; i < scene._pointsClouds.size(); ++i)
        addPointsCloud(new PointsCloud( *scene._pointsClouds[i]));
}


Scene::~Scene()
{

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Scene" << this->objectName() << "is destroyed";
}


bool Scene::setCamera( Camera * camera )
{
    if ( _camera == camera )
        return(false);

    if ( _camera )
    {
        Camera* tmp = _camera;
        _camera = nullptr;
        delete tmp;
    }

    if ( camera )
    {
        _camera = camera;
        _camera->setParent( this );
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "Camera" << camera->objectName() << "is assigned to scene" << this->objectName();
    }

    emit cameraChanged( _camera );
    return(true);
}


bool Scene::addGridline( Gridline * gridline )
{
    if ( !gridline || _gridlines.contains( gridline ) )
        return(false);

    _gridlines.push_back( gridline );
    gridline->setParent( this );

    gridline->setObjectName( "Gridline" + QString::number( _gridlineNameCounter++ ) );

    emit gridlineAdded( gridline );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Gridline" << gridline->objectName() << "is added to scene" << this->objectName();

    return(true);
}


bool Scene::addCurve(Curve3D *curve)
{
    if ( !curve || _curves.contains( curve ) )
    {
        return (false);
    }

    _curves.push_back( curve );
    curve->setParent( this );

    curve->setObjectName( "Curve" + QString::number( _curveNameCounter++ ) );

    emit curveAdded( curve );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Curve" << curve->objectName() << "is added to scene" << this->objectName();

    return(true);
}

bool Scene::addTf(Transform *tf)
{
    if ( !tf || _tfs.contains( tf ) )
    {
        return (false);
    }

    _tfs.push_back( tf );
    tf->setParent( this );

    tf->setObjectName( "Transform " + QString::number( _tfNameCounter++ ) );

    emit tfAdded( tf );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Transform " << tf->objectName() << "is added to scene" << this->objectName();

    return(true);
}

bool Scene::addTfGroup(TransformGroup *tfGroup)
{
    if ( !tfGroup || _tfGroups.contains( tfGroup ) )
    {
        return (false);
    }

    _tfGroups.push_back( tfGroup );
    tfGroup->setParent( this );

    tfGroup->setObjectName( "TransformGroup " + QString::number( _tfGroupNameCounter++ ) );

    emit tfGroupAdded( tfGroup );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "TransformGroup " << tfGroup->objectName() << "is added to scene" << this->objectName();

    return(true);
}

bool Scene::addPointsCloud(PointsCloud *pointsCloud)
{
    if(!pointsCloud || _pointsClouds.contains(pointsCloud))
    {
        return false;
    }

    _pointsClouds.push_back(pointsCloud);
    pointsCloud->setParent(this);

    pointsCloud->setObjectName( "PointsCloud" + QString::number( _pointsCloudNameCounter++ ) );
    emit pointsCloudAdded(pointsCloud);

    pointsCloud->init(); //此处由于重新设置了object name, 而关节的subscriber需要使用新的名字,内部解决,直接再addRobot时处理,不把问题暴露出来

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Points Cloud" << pointsCloud->objectName() << "is added to scene" << this->objectName();

    return(true);
}

bool Scene::addLight( AbstractLight * l )
{
    if ( SpotLight * light = qobject_cast<SpotLight*>( l ) )
        return(addSpotLight( light ) );
    else if ( AmbientLight * light = qobject_cast<AmbientLight*>( l ) )
        return(addAmbientLight( light ) );
    else if ( DirectionalLight * light = qobject_cast<DirectionalLight*>( l ) )
        return(addDirectionalLight( light ) );
    else if ( PointLight * light = qobject_cast<PointLight*>( l ) )
        return(addPointLight( light ) );
    return(false);
}


bool Scene::addAmbientLight( AmbientLight * light )
{
    if ( !light || _ambientLights.contains( light ) )
        return(false);
    if ( _ambientLights.size() >= 8 )
    {
        if ( logLevel >= LOG_LEVEL_WARNING )
            dout << "The amount of ambient lights has reached the upper limit of 8.";
        return(false);
    }

    _ambientLights.push_back( light );
    light->setParent( this );
    light->setObjectName( "AmbientLight" + QString::number( _ambientLightNameCounter++ ) );
    emit lightAdded( light );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Ambient light" << light->objectName() << "is added to scene" << this->objectName();

    return(true);
}


bool Scene::addDirectionalLight( DirectionalLight * light )
{
    if ( !light || _directionalLights.contains( light ) )
        return(false);
    if ( _directionalLights.size() >= 8 )
    {
        if ( logLevel >= LOG_LEVEL_WARNING )
            dout << "The amount of directional lights has reached the upper limit of 8.";
        return(false);
    }

    _directionalLights.push_back( light );
    light->setParent( this );
    light->setObjectName( "DirectionalLight" + QString::number( _directionalLightNameCounter++ ) );
    emit lightAdded( light );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Directional light" << light->objectName() << "is added to scene" << this->objectName();

    return(true);
}


bool Scene::addPointLight( PointLight * light )
{
    if ( !light || _pointLights.contains( light ) )
        return(false);
    if ( _pointLights.size() >= 8 )
    {
        if ( logLevel >= LOG_LEVEL_WARNING )
            dout << "The amount of point lights has reached the upper limit of 8.";
        return(false);
    }

    _pointLights.push_back( light );
    light->setParent( this );
    light->setObjectName( "PointLight" + QString::number( _pointLightNameCounter++ ) );
    emit lightAdded( light );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Point light" << light->objectName() << "is added to scene" << this->objectName();

    return(true);
}


bool Scene::addSpotLight( SpotLight * light )
{
    if ( !light || _spotLights.contains( light ) )
        return(false);
    if ( _spotLights.size() >= 8 )
    {
        if ( logLevel >= LOG_LEVEL_WARNING )
            dout << "The amount of spotlights has reached the upper limit of 8.";
        return(false);
    }

    _spotLights.push_back( light );
    light->setParent( this );
    light->setObjectName( "SpotLight" + QString::number( _spotLightNameCounter++ ) );
    emit lightAdded( light );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Spot light" << light->objectName() << "is added to scene" << this->objectName();

    return(true);
}


bool Scene::addModel( Model * model )
{
    if ( !model || _models.contains( model ) )
    {
        return(false);
    }

    _models.push_back( model );
    model->setParent( this );
    emit modelAdded( model );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Model" << model->objectName() << "is added to scene" << this->objectName();

    return(true);
}

bool Scene::addRobot(Robot *robot)
{
    if ( !robot || _robots.contains( robot ) )
    {
        return(false);
    }

    _robots.push_back( robot );
    robot->setParent( this );
    robot->setObjectName(robot->objectName()+"_"+QString::number( _robotNameCounter++));//设置新的objectname
    robot->init(); //此处由于重新设置了object name, 而关节的subscriber需要使用新的名字,内部解决,直接再addRobot时处理,不把问题暴露出来

    emit robotAdded( robot );

    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Robot" << robot->objectName() << "is added to scene" << this->objectName();

    return(true);
}

bool Scene::removeGridline( QObject * gridline )
{
    for ( int i = 0; i < _gridlines.size(); i++ )
    {
        if ( _gridlines[i] == gridline )
        {
            _gridlines.erase( _gridlines.begin() + i );
            emit gridlineRemoved( gridline );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Gridline" << gridline->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }
    return(false);
}


bool Scene::removeLight( QObject * light )
{
    for ( int i = 0; i < _ambientLights.size(); i++ )
    {
        if ( _ambientLights[i] == light )
        {
            _ambientLights.erase( _ambientLights.begin() + i );
            emit lightRemoved( light );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Ambient light" << light->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }

    for ( int i = 0; i < _directionalLights.size(); i++ )
    {
        if ( _directionalLights[i] == light )
        {
            _directionalLights.erase( _directionalLights.begin() + i );
            emit lightRemoved( light );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Directional light" << light->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }

    for ( int i = 0; i < _pointLights.size(); i++ )
    {
        if ( _pointLights[i] == light )
        {
            _pointLights.erase( _pointLights.begin() + i );
            emit lightRemoved( light );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Point light" << light->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }

    for ( int i = 0; i < _spotLights.size(); i++ )
    {
        if ( _spotLights[i] == light )
        {
            _spotLights.erase( _spotLights.begin() + i );
            emit lightRemoved( light );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Spot light" << light->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }
    return(false);
}


bool Scene::removeModel( QObject * model, bool recursive )
{
    for ( int i = 0; i < _models.size(); i++ )
    {
        if ( _models[i] == model )
        {
            _models.erase( _models.begin() + i );
            emit modelRemoved( model );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Model" << model->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }

    if ( !recursive )
        return(false);

    for ( int i = 0; i < _models.size(); i++ )
    {
        if ( _models[i]->removeChildModel( model, recursive ) )
            return(true);
    }
    return(false);
}

bool Scene::removeCurve(QObject *curve)
{
    for ( int i = 0; i < _curves.size(); i++ )
    {
        if ( _curves[i] == curve )
        {
            _curves.erase( _curves.begin() + i );
            emit curveRemoved( curve );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Curve" << curve->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }
    return(false);
}

bool Scene::removeTf(QObject *tf)
{
    for ( int i = 0; i < _tfs.size(); i++ )
    {
        if ( _tfs[i] == tf )
        {
            _tfs.erase( _tfs.begin() + i );
            emit tfRemoved( tf );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Transform" << tf->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }
    return(false);
}

bool Scene::removeTfGroup(QObject *tfGroup)
{
    for ( int i = 0; i < _tfGroups.size(); i++ )
    {
        if ( _tfGroups[i] == tfGroup )
        {
            _tfGroups.erase( _tfGroups.begin() + i );
            emit tfGroupsRemoved( tfGroup );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "TransformGroup" << tfGroup->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }
    return(false);
}

bool Scene::removePointsCloud(QObject *pointsCloud)
{
    for ( int i = 0; i < _pointsClouds.size(); i++ )
    {
        if ( _pointsClouds[i] == pointsCloud )
        {
            _pointsClouds.erase( _pointsClouds.begin() + i );
            emit curveRemoved( pointsCloud );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Point Clouds" << pointsCloud->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }
    return(false);
}

bool Scene::removeRobot(QObject *robot)
{
    for ( int i = 0; i < _pointsClouds.size(); i++ )
    {
        if ( _robots[i] == robot )
        {
            _robots.erase( _robots.begin() + i );
            emit robotRemoved( robot );
            if ( logLevel >= LOG_LEVEL_INFO )
                dout << "Robot " << robot->objectName() << "is removed from scene" << this->objectName();
            return(true);
        }
    }
    return(false);
}

/* Dump info */

void Scene::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Scene: " << objectName();
    qDebug( "%s%d gridline(s), %d ambient light(s), %d directional light(s), %d point light(s), %d spotlights(s), %d model(s),, %d curves(s)",
            tab( l ),
            _gridlines.size(),
            _ambientLights.size(),
            _directionalLights.size(),
            _pointLights.size(),
            _spotLights.size(),
            _models.size() ,
            _curves.size()
            );
}


void Scene::dumpObjectTree( int l )
{
    dumpObjectInfo( l );

    _gizmo->dumpObjectTree( l + 1 );

    _camera->dumpObjectTree( l + 1 );

    for ( int i = 0; i < _gridlines.size(); i++ )
    {
        _gridlines[i]->dumpObjectTree( l + 1 );
    }

    for ( int i = 0; i < _ambientLights.size(); i++ )
    {
        _ambientLights[i]->dumpObjectTree( l + 1 );
    }

    for ( int i = 0; i < _directionalLights.size(); i++ )
    {
        _directionalLights[i]->dumpObjectTree( l + 1 );
    }

    for ( int i = 0; i < _pointLights.size(); i++ )
    {
        _pointLights[i]->dumpObjectTree( l + 1 );
    }

    for ( int i = 0; i < _spotLights.size(); i++ )
    {
        _spotLights[i]->dumpObjectTree( l + 1 );
    }

    for ( int i = 0; i < _models.size(); i++ )
    {
        _models[i]->dumpObjectTree( l + 1 );
    }

}


/* Get properties */

TransformGizmo * Scene::getGizmo() const
{
    return(_gizmo);
}


Camera * Scene::getCamera() const
{
    return(_camera);
}


const QVector<Gridline*> & Scene::getGridlines() const
{
    return(_gridlines);
}


const QVector<AmbientLight*> & Scene::getAmbientLights() const
{
    return(_ambientLights);
}


const QVector<DirectionalLight*> & Scene::getDirectionalLights() const
{
    return(_directionalLights);
}


const QVector<PointLight*> & Scene::getPointLights() const
{
    return(_pointLights);
}


const QVector<SpotLight*> & Scene::getSpotLights() const
{
    return(_spotLights);
}


const QVector<Model*> & Scene::getModels() const
{
    return(_models);
}

const QVector<Curve3D *> &Scene::getCurves() const
{
    return _curves;
}

const QVector<Transform *> &Scene::getTfs() const
{
    return _tfs;
}

const QVector<TransformGroup *> &Scene::getTfGroups() const
{
    return _tfGroups;
}

const QVector<PointsCloud *> &Scene::getPointsClouds() const
{
    return _pointsClouds;
}

const QVector<Robot *> &Scene::getRobots() const
{
    return _robots;
}


// add new item step 1
void Scene::childEvent( QChildEvent * e )
{
    if ( e->added() )
    {
        if ( Camera * camera = qobject_cast<Camera*>( e->child() ) )
        {
            setCamera( camera );
        }
        else if ( Gridline * gridline = qobject_cast<Gridline*>( e->child() ) )
        {
            addGridline( gridline );
        }
        else if ( Curve3D * curve = qobject_cast<Curve3D*>( e->child() ) )
        {
            addCurve( curve );
        }
        else if ( Transform * tf = qobject_cast<Transform*>( e->child() ) )
        {
            addTf( tf );
        }
        else if ( TransformGroup * tfGroup = qobject_cast<TransformGroup*>( e->child() ) )
        {
            addTfGroup( tfGroup );
        }
        else if ( PointsCloud * pointsCloud = qobject_cast<PointsCloud*>( e->child() ) )
        {
            addPointsCloud( pointsCloud );
        }
        else if ( AbstractLight * light = qobject_cast<AbstractLight*>( e->child() ) )
        {
            addLight( light );
        }
        else if ( Model * model = qobject_cast<Model*>( e->child() ) )
        {
            addModel( model );
        }
        else if ( Robot * robot = qobject_cast<Robot*>( e->child() ) )
        {
            addRobot( robot );
        }
    }
    else if ( e->removed() )
    {
        if ( _camera == e->child() )
        {
            _camera = nullptr;
            if ( logLevel >= LOG_LEVEL_WARNING )
                dout << "Warning: Camera is removed from scene" << this->objectName();
            emit cameraChanged( 0 );
            return;
        }
        if ( removeGridline( e->child() ) )
            return;
        if ( removeCurve( e->child() ) )
            return;
        if ( removeTf( e->child() ) )
            return;
        if ( removeLight( e->child() ) )
            return;
        if ( removeModel( e->child(), false ) )
            return;
        if ( removePointsCloud( e->child() ) )
            return;
        if ( removeRobot( e->child() ) )
            return;
    }
}


