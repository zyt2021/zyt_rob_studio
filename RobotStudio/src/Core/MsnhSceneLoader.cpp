﻿#include <Core/MsnhSceneLoader.h>

SceneLoader::SceneLoader(QObject *parent):QObject(parent)
{

}


Scene * SceneLoader::loadFromFile( QString filePath )
{
    _textures.clear();

    QFile file( filePath.toLocal8Bit() );
    file.open( QIODevice::ReadOnly );

    if ( !file.isOpen() )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
        {
            dout << "Failed to load file:" << file.errorString();
        }
        _log += file.errorString() + "\n";
        return (nullptr);
    }

    emit startProgressBarSig();

    QDataStream in( &file );

    quint32 magicNumber;
    in >> magicNumber;
    if ( magicNumber != 0xA0B0C0D0 )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to load file: Invalid File Format";
        _log += "Invalid File Format.\n";
        return (nullptr);
    }

    quint32 versionNumber;
    in >> versionNumber;
    if ( versionNumber > 100 )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to load file: Version not supported";
        _log += "Version not supported.\n";
        return (nullptr);
    }

    int textureNum;
    in >> textureNum;
    for ( int i = 0; i < textureNum; i++ )
    {
        QSharedPointer<Texture> texture( loadTexture( in ) );
        _textures.push_back( texture );
    }

    emit updateProgressBarSig(5);

    int cameraNum;
    in >> cameraNum;
    if ( cameraNum != 1 )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to load file: Unknown error";
        _log += "Unknown error.\n";
        return (nullptr);
    }

    Scene* scene = new Scene;
    scene->setCamera( loadCamera( in ) );

    int gridlineNum;
    in >> gridlineNum;
    for ( int i = 0; i < gridlineNum; i++ )
    {
        Gridline* gridline = loadGridline( in );
        scene->_gridlines.push_back( gridline );
        gridline->setParent( scene );
    }

    int curveNum;
    in >> curveNum;
    for (int i = 0; i < curveNum; ++i)
    {
        Curve3D* curve = loadCurve( in );
        scene->_curves.push_back(curve);
        curve->setParent(scene);
    }

    int pointsCloudNum;
    in >> pointsCloudNum;
    for (int i = 0; i < pointsCloudNum; ++i)
    {
        PointsCloud* pointsCloud = loadPointClouds( in );
        scene->_pointsClouds.push_back(pointsCloud);
        pointsCloud->setParent(scene);
    }

    emit updateProgressBarSig(16);

    int ambientLightNum;
    in >> ambientLightNum;
    for ( int i = 0; i < ambientLightNum; i++ )
    {
        AmbientLight* light = loadAmbientLight( in );
        scene->_ambientLights.push_back( light );
        light->setParent( scene );
    }

    emit updateProgressBarSig(32);

    int directionalLightNum;
    in >> directionalLightNum;
    for ( int i = 0; i < directionalLightNum; i++ )
    {
        DirectionalLight* light = loadDirectionalLight( in );
        scene->_directionalLights.push_back( light );
        light->setParent( scene );
    }

    emit updateProgressBarSig(48);

    int pointLightNum;
    in >> pointLightNum;
    for ( int i = 0; i < pointLightNum; i++ )
    {
        PointLight* light = loadPointLight( in );
        scene->_pointLights.push_back( light );
        light->setParent( scene );
    }

    emit updateProgressBarSig(64);

    int spotLightNum;
    in >> spotLightNum;
    for ( int i = 0; i < spotLightNum; i++ )
    {
        SpotLight* light = loadSpotLight( in );
        scene->_spotLights.push_back( light );
        light->setParent( scene );
    }

    emit updateProgressBarSig(80);

    int modelNum;
    in >> modelNum;
    for ( int i = 0; i < modelNum; i++ )
    {
        Model* model = loadModel( in );
        scene->_models.push_back( model );
        model->setParent( scene );
    }

    emit updateProgressBarSig(100);

    in >> scene->_gridlineNameCounter;
    in >> scene->_ambientLightNameCounter;
    in >> scene->_directionalLightNameCounter;
    in >> scene->_pointLightNameCounter;
    in >> scene->_spotLightNameCounter;
    in >> scene->_curveNameCounter;

    emit stopProgressBarSig();

    return(scene);
}


bool SceneLoader::hasErrorLog()
{
    return(_log != "");
}


QString SceneLoader::errorLog()
{
    QString tmp = _log;
    _log = "";
    return(tmp);
}


Camera * SceneLoader::loadCamera( QDataStream & in )
{
    Camera* camera = new Camera;

    float		movingSpeed =   0;
    float       fieldOfView =   0;
    float       aspectRatio =   0;
    float       nearPlane   =   0;
    float       farPlane    =   0;

    QVector3D	position;
    QVector3D   direction;

    bool        isOrbitCam  =   false;
    QVector3D   orbitFocusRadius;

    float       orbitMaxRadius  =   0;
    float       orbitPitch      =   0;
    float       orbitRadius     =   0;
    float       orbitYaw        =   0;

    in >> movingSpeed >> fieldOfView >> aspectRatio >> nearPlane >> farPlane;
    in >> position >> direction;
    in >> isOrbitCam >> orbitFocusRadius >> orbitMaxRadius >> orbitPitch >> orbitRadius >> orbitYaw;

    camera->setMovingSpeed( movingSpeed );
    camera->setFieldOfView( fieldOfView );
    camera->setAspectRatio( aspectRatio );
    camera->setNearPlane( nearPlane );
    camera->setFarPlane( farPlane );
    camera->setPosition( position );
    camera->setDirection( direction );

    // orbit camera area
    camera->setOrbitCam(isOrbitCam);
    camera->setOrbitFocusCenter(orbitFocusRadius);
    camera->setOrbitMaxRadius(orbitMaxRadius);
    camera->setOrbitPitch(orbitPitch);
    camera->setOrbitRadius(orbitRadius);
    camera->setOrbitYaw(orbitYaw);

    return(camera);
}


Gridline * SceneLoader::loadGridline( QDataStream & in )
{
    Gridline* gridline = new Gridline();

    QString		name;
    QVector3D	xargs;
    QVector3D   yargs;
    QVector3D   zargs;
    QVector3D   color;
    int         gridPlane;
    in >> name >> xargs >> yargs >> zargs >> color >> gridPlane;

    gridline->setObjectName( name );
    gridline->setXArguments( xargs );
    gridline->setYArguments( yargs );
    gridline->setZArguments( zargs );
    gridline->setColor(color);
    gridline->setGridPlane((Gridline::GridPlane)gridPlane);

    return(gridline);
}

Curve3D *SceneLoader::loadCurve(QDataStream &in)
{
    Curve3D *curve = new Curve3D();

    QString     name;
    QVector3D   color;
    float       lineWidth;
    bool        isCubeLine;
    int         verticesNum;
    QVector<QVector3D> lineVertices;

    in >> name >> color >> lineWidth >> isCubeLine >> verticesNum;

    QVector3D vec;
    for (int i = 0; i < verticesNum; ++i)
    {
        in >> vec;
        lineVertices.push_back(vec);
    }
    curve->setObjectName(name);
    curve->setColor(color);
    curve->setLineWidth(lineWidth);
    curve->setIsCubeLine(isCubeLine);
    curve->setLineVertices(lineVertices);

    return curve;
}

PointsCloud *SceneLoader::loadPointClouds(QDataStream &in)
{
    QString     name;
    QVector3D   color;
    bool        isPureColor;

    in >> name >> color >> isPureColor;

    PointsCloud *pointClouds = new PointsCloud(loadMesh(in));
    pointClouds->setObjectName(name);
    pointClouds->setColor(color);
    pointClouds->setPureColor(isPureColor);
    return pointClouds;
}


AmbientLight * SceneLoader::loadAmbientLight( QDataStream & in )
{
    AmbientLight* light = new AmbientLight;

    QString		name;
    QVector3D   color;
    bool		enabled;
    float		intensity;
    in >> name >> color >> enabled >> intensity;

    light->setObjectName( name );
    light->setColor( color );
    light->setEnabled( enabled );
    light->setIntensity( intensity );

    return(light);
}


DirectionalLight * SceneLoader::loadDirectionalLight( QDataStream & in )
{
    DirectionalLight* light = new DirectionalLight;

    QString		name;
    QVector3D   color;
    QVector3D   direction;
    bool		enabled;
    float		intensity;
    in >> name >> color >> enabled >> intensity >> direction;

    light->setObjectName( name );
    light->setColor( color );
    light->setEnabled( enabled );
    light->setIntensity( intensity );
    light->setDirection( direction );

    return(light);
}


PointLight * SceneLoader::loadPointLight( QDataStream & in )
{
    PointLight* light = new PointLight;

    QString		name;
    QVector3D   color;
    QVector3D   position;
    QVector3D   attenuationArgs;
    bool		enabled, enableAttenuation;
    float		intensity;

    in >> name >> color >> enabled >> intensity;
    in >> position >> enableAttenuation >> attenuationArgs;

    light->setObjectName( name );
    light->setColor( color );
    light->setEnabled( enabled );
    light->setIntensity( intensity );
    light->setPosition( position );
    light->setEnableAttenuation( enableAttenuation );
    light->setAttenuationArguments( attenuationArgs );

    return(light);
}


SpotLight * SceneLoader::loadSpotLight( QDataStream & in )
{
    SpotLight* light = new SpotLight;

    QString		name;
    QVector3D   color;
    QVector3D   position;
    QVector3D   direction;
    QVector3D   attenuationArgs;
    bool		enabled;
    bool        enableAttenuation;

    float		intensity;
    float       innerCutOff;
    float       outerCutOff;

    in >> name >> color >> enabled >> intensity;
    in >> position >> direction >> innerCutOff >> outerCutOff;
    in >> enableAttenuation >> attenuationArgs;

    light->setObjectName( name );
    light->setColor( color );
    light->setEnabled( enabled );
    light->setIntensity( intensity );
    light->setPosition( position );
    light->setDirection( direction );
    light->setInnerCutOff( innerCutOff );
    light->setOuterCutOff( outerCutOff );
    light->setEnableAttenuation( enableAttenuation );
    light->setAttenuationArguments( attenuationArgs );

    return(light);
}


Model * SceneLoader::loadModel( QDataStream & in )
{
    Model* model = new Model;

    QString	name;
    bool	visible;
    QVector3D	position;
    QVector3D   rotation;
    QVector3D   scaling;
    in >> name >> visible >> position >> rotation >> scaling;

    model->setObjectName( name );
    model->setVisible( visible );
    model->setPosition( position );
    model->setRotation( rotation );
    model->setScaling( scaling );

    int childMeshNum;
    in >> childMeshNum;
    for ( int i = 0; i < childMeshNum; i++ )
    {
        Mesh* mesh = loadMesh( in );
        model->addChildMesh( mesh );
    }

    int childModelNum;
    in >> childModelNum;
    for ( int i = 0; i < childModelNum; i++ )
    {
        Model* tmpModel = loadModel( in );
        //QObject 0x21057b82390 (class: 'Model', object name: 'Cube') may have a loop in its parent-child chain; this is undefined behavior
        model->addChildModel( tmpModel );
    }

    return(model);
}


Mesh * SceneLoader::loadMesh( QDataStream & in )
{
    Mesh* mesh = new Mesh;

    QString             name;
    bool                visible;
    Mesh::MeshType      meshType;
    QVector3D           position;
    QVector3D           rotation;
    QVector3D           scaling;
    QVector<Vertex>     vertices;
    QVector<uint32_t>	indices;

    in >> name >> visible >> meshType;
    in >> position >> rotation >> scaling;
    in >> vertices >> indices;

    mesh->setObjectName( name );
    mesh->setVisible( visible );
    mesh->setMeshType( meshType );
    mesh->setPosition( position );
    mesh->setRotation( rotation );
    mesh->setScaling( scaling );
    mesh->setGeometry( vertices, indices );

    bool hasMaterial;
    in >> hasMaterial;
    if ( hasMaterial )
    {
        mesh->setMaterial( loadMaterial( in ) );
    }

    return(mesh);
}


Material * SceneLoader::loadMaterial( QDataStream & in )
{
    Material* material = new Material;

    QString		name;
    QVector3D   color;
    float		ambient;
    float       diffuse;
    float       specular;
    float       shininess;

    in >> name >> color;
    in >> ambient >> diffuse >> specular >> shininess;

    material->setObjectName( name );
    material->setColor( color );
    material->setAmbient( ambient );
    material->setDiffuse( diffuse );
    material->setSpecular( specular );
    material->setShininess( shininess );

    int textureNum;
    in >> textureNum;
    for ( int i = 0; i < textureNum; i++ )
    {
        Texture::TextureType	textureType;
        qint32			indx;
        in >> textureType >> indx;
        if ( textureType == Texture::DIFFUSE )
        {
            material->setDiffuseTexture( _textures[indx] );
        } else if ( textureType == Texture::SPECULAR )
        {
            material->setSpecularTexture( _textures[indx] );
        } else {
            material->setBumpTexture( _textures[indx] );
        }
    }

    return(material);
}


QSharedPointer<Texture> SceneLoader::loadTexture( QDataStream & in )
{
    Texture* texture = new Texture;

    QString			name;
    bool			enabled;
    Texture::TextureType	textureType;
    QImage			image;

    in >> name >> enabled >> textureType >> image;

    texture->setObjectName( name );
    texture->setEnabled( enabled );
    texture->setTextureType( textureType );
    texture->setImage( image );

    return(QSharedPointer<Texture>( texture ) );
}


