﻿#include <Core/MsnhSceneSaver.h>

SceneSaver::SceneSaver(Scene *scene, QObject *parent):QObject(parent)
{
    _scene = scene;
}

void SceneSaver::setScene(Scene *scene)
{
    _scene = scene;
}


bool SceneSaver::saveToFile( QString filePath )
{
    emit startProgressBarSig();

    _textures.clear();
    for ( int i = 0; i < _scene->getModels().size(); i++ )
    {
        getAllTextures( _scene->getModels()[i] );
    }

    QFile file( filePath );
    file.open( QIODevice::WriteOnly );

    if ( !file.isOpen() )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to write to file:" << file.errorString();
        _log += file.errorString();
        return(false);
    }

    emit updateProgressBarSig(16);

    QDataStream out( &file );

    out << quint32( 0xA0B0C0D0 );   /* magic number */
    out << quint32( 100 );          /* version 1.0.0 */

    out << _textures.size();
    for ( int i = 0; i < _textures.size(); i++ )
        saveTexture( _textures[i], out );

    out << 1;
    saveCamera( _scene->getCamera(), out );

    out << _scene->getGridlines().size();
    for ( int i = 0; i < _scene->getGridlines().size(); i++ )
        saveGridline( _scene->getGridlines()[i], out );

    out << _scene->getCurves().size();
    for (int i = 0; i < _scene->getCurves().size(); ++i)
    {
        saveCurve(_scene->getCurves()[i], out);
    }

    out << _scene->getPointsClouds().size();

    for (int i = 0; i < _scene->getPointsClouds().size(); ++i)
    {
        savePointCloud(_scene->getPointsClouds()[i],out);
    }

    out << _scene->getAmbientLights().size();
    for ( int i = 0; i < _scene->getAmbientLights().size(); i++ )
        saveAmbientLight( _scene->getAmbientLights()[i], out );

    emit updateProgressBarSig(32);

    out << _scene->getDirectionalLights().size();
    for ( int i = 0; i < _scene->getDirectionalLights().size(); i++ )
        saveDirectionalLight( _scene->getDirectionalLights()[i], out );

    emit updateProgressBarSig(48);

    out << _scene->getPointLights().size();
    for ( int i = 0; i < _scene->getPointLights().size(); i++ )
        savePointLight( _scene->getPointLights()[i], out );

    emit updateProgressBarSig(64);

    out << _scene->getSpotLights().size();
    for ( int i = 0; i < _scene->getSpotLights().size(); i++ )
        saveSpotLight( _scene->getSpotLights()[i], out );

    emit updateProgressBarSig(80);

    out << _scene->getModels().size();

    for ( int i = 0; i < _scene->getModels().size(); i++ )
    {
        saveModel( _scene->getModels()[i], out );
    }


    emit updateProgressBarSig(100);

    out << _scene->_gridlineNameCounter;
    out << _scene->_ambientLightNameCounter;
    out << _scene->_directionalLightNameCounter;
    out << _scene->_pointLightNameCounter;
    out << _scene->_spotLightNameCounter;
    out << _scene->_curveNameCounter;

    file.close();

    emit stopProgressBarSig();

    return(true);
}


bool SceneSaver::hasErrorLog()
{
    return(_log != "");
}


QString SceneSaver::errorLog()
{
    QString tmp = _log;
    _log = "";
    return(tmp);
}


void SceneSaver::getAllTextures( Model * model )
{
    for ( int i = 0; i < model->getChildMeshes().size(); i++ )
        if ( model->getChildMeshes()[i]->getMaterial() )
        {
            Material * material = model->getChildMeshes()[i]->getMaterial();
            if ( material == nullptr )
                continue;
            if ( !material->getDiffuseTexture().isNull() && !_textures.contains( material->getDiffuseTexture() ) )
                _textures.push_back( material->getDiffuseTexture() );
            if ( !material->getSpecularTexture().isNull() && !_textures.contains( material->getSpecularTexture() ) )
                _textures.push_back( material->getSpecularTexture() );
            if ( !material->getBumpTexture().isNull() && !_textures.contains( material->getBumpTexture() ) )
                _textures.push_back( material->getBumpTexture() );
        }
    for ( int i = 0; i < model->getChildModels().size(); i++ )
        getAllTextures( model->getChildModels()[i] );
}


void SceneSaver::saveCamera( Camera * camera, QDataStream & out )
{
    out << camera->getMovingSpeed();
    out << camera->getFieldOfView();
    out << camera->getAspectRatio();
    out << camera->getNearPlane();
    out << camera->getfFarPlane();
    out << camera->getPosition();
    out << camera->getDirection();
    // orbit camera area
    out << camera->isOrbitCam();
    out << camera->getOrbitFocusCenter();
    out << camera->getOrbitMaxRadius();
    out << camera->getOrbitPitch();
    out << camera->getOrbitRadius();
    out << camera->getOrbitYaw();
}


void SceneSaver::saveGridline( Gridline * gridline, QDataStream & out )
{
    out << gridline->objectName();
    out << QVector3D( gridline->getXRange().first, gridline->getXRange().second, gridline->getXStride() );
    out << QVector3D( gridline->getYRange().first, gridline->getYRange().second, gridline->getYStride() );
    out << QVector3D( gridline->getZRange().first, gridline->getZRange().second, gridline->getZStride() );
    out << gridline->getColor();
    out << (int)gridline->getGridPlane();
}

void SceneSaver::saveCurve(Curve3D *curve, QDataStream &out)
{
    out << curve->objectName();
    out << curve->getColor();
    out << curve->getLineWidth();
    out << curve->isCubeLine();
    out << (int)curve->getLineVertices().size();

    for (int i = 0; i < curve->getLineVertices().size(); ++i)
    {
        out << curve->getLineVertices()[i];
    }
}

void SceneSaver::savePointCloud(PointsCloud *pointCloud, QDataStream &out)
{
    out << pointCloud->objectName();
    out << pointCloud->getColor();
    out << pointCloud->isPureColor();

    saveMesh(pointCloud->getMarker(), out);
}


void SceneSaver::saveAmbientLight( AmbientLight * light, QDataStream & out )
{
    out << light->objectName();
    out << light->getColor();
    out << light->getEnabled();
    out << light->getIntensity();
}


void SceneSaver::saveDirectionalLight( DirectionalLight * light, QDataStream & out )
{
    out << light->objectName();
    out << light->getColor();
    out << light->getEnabled();
    out << light->getIntensity();
    out << light->getDirection();
}


void SceneSaver::savePointLight( PointLight * light, QDataStream & out )
{
    out << light->objectName();
    out << light->getColor();
    out << light->getEnabled();
    out << light->getIntensity();
    out << light->getPosition();
    out << light->getEnableAttenuation();
    out << light->getAttenuationArguments();
}


void SceneSaver::saveSpotLight( SpotLight * light, QDataStream & out )
{
    out << light->objectName();
    out << light->getColor();
    out << light->getEnabled();
    out << light->getIntensity();
    out << light->getPosition();
    out << light->getDirection();
    out << light->getInnerCutOff();
    out << light->getOuterCutOff();
    out << light->getEnableAttenuation();
    out << light->getAttenuationArguments();
}


void SceneSaver::saveModel( Model * model, QDataStream & out )
{
    out << model->objectName();
    out << model->getVisible();
    out << model->getPosition();
    out << model->getRotation();
    out << model->getScaling();

    out << model->getChildMeshes().size();
    for ( int i = 0; i < model->getChildMeshes().size(); i++ )
        saveMesh( model->getChildMeshes()[i], out );

    out << model->getChildModels().size();
    for ( int i = 0; i < model->getChildModels().size(); i++ )
        saveModel( model->getChildModels()[i], out );
}


void SceneSaver::saveMesh( Mesh * mesh, QDataStream & out )
{
    out << mesh->objectName();
    out << mesh->getVisible();
    out << mesh->getMeshType();
    out << mesh->getPosition();
    out << mesh->getRotation();
    out << mesh->getScaling();
    out << mesh->getVertices();
    out << mesh->getIndices();

    out << bool(mesh->getMaterial() != nullptr);
    if ( mesh->getMaterial() )
        saveMaterial( mesh->getMaterial(), out );
}


void SceneSaver::saveMaterial( Material * material, QDataStream & out )
{
    out << material->objectName();
    out << material->getColor();
    out << material->getAmbient();
    out << material->getDiffuse();
    out << material->getSpecular();
    out << material->getShininess();

    int textureNum = 0;
    if ( !material->getDiffuseTexture().isNull() )
        textureNum++;
    if ( !material->getSpecularTexture().isNull() )
        textureNum++;
    if ( !material->getBumpTexture().isNull() )
        textureNum++;
    out << textureNum;

    if ( !material->getDiffuseTexture().isNull() )
    {
        out << material->getDiffuseTexture()->getTextureType();
        out << _textures.indexOf( material->getDiffuseTexture() );
    }

    if ( !material->getSpecularTexture().isNull() )
    {
        out << material->getSpecularTexture()->getTextureType();
        out << _textures.indexOf( material->getSpecularTexture() );
    }

    if ( !material->getBumpTexture().isNull() )
    {
        out << material->getBumpTexture()->getTextureType();
        out << _textures.indexOf( material->getBumpTexture() );
    }
}


void SceneSaver::saveTexture( QSharedPointer<Texture> texture, QDataStream & out )
{
    out << texture->objectName();
    out << texture->getEnabled();
    out << texture->getTextureType();
    out << texture->getImage();
}


