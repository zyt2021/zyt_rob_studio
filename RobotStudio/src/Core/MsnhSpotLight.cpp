﻿#include <Core/MsnhSpotLight.h>
#include <Core/MsnhModelLoader.h>

SpotLight::SpotLight( QObject * parent ) : AbstractLight( parent )
{
    _position               = QVector3D( 0.0f, 0.0f, 0.0f );
    _direction              = QVector3D( 0.0f, -1.0f, 0.0f );
    _innerCutOff            = 30.0f;
    _outerCutOff            = 45.0f;
    _enableAttenuation      = false;
    _attenuationQuadratic	= 0.0007f;
    _attenuationLinear      = 0.014f;
    _attenuationConstant	= 1.0f;

    initMarker();
    setObjectName( "Untitled Spotlight" );
}


SpotLight::SpotLight( QVector3D color, QVector3D position, QVector3D direction, QObject * parent )
    : AbstractLight( color, parent )
{
    _position               = position;
    _direction              = direction;
    _innerCutOff            = 30.0f;
    _outerCutOff            = 45.0f;
    _enableAttenuation      = false;
    _attenuationQuadratic	= 0.0007f;
    _attenuationLinear      = 0.014f;
    _attenuationConstant	= 1.0f;

    initMarker();
    setObjectName( "Untitled Spotlight" );
    setParent( parent );
}


SpotLight::SpotLight(const SpotLight & light , QObject *parent) : AbstractLight( light, parent )
{
    _position               = light._position;
    _direction              = light._direction;
    _innerCutOff            = light._innerCutOff;
    _outerCutOff            = light._outerCutOff;
    _enableAttenuation      = light._enableAttenuation;
    _attenuationQuadratic	= light._attenuationQuadratic;
    _attenuationLinear      = light._attenuationLinear;
    _attenuationConstant	= light._attenuationConstant;

    initMarker();
}


SpotLight::~SpotLight()
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    delete _marker;

    logLevel = tmp_log_level;

    if ( logLevel >= LOG_LEVEL_ERROR )
        dout << "Spotlight" << this->objectName() << "is destroyed";
}


void SpotLight::translate( QVector3D delta )
{
    setPosition( _position + delta );
}


void SpotLight::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Spot Light: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "Enabled: " << _enabled;
    qDebug().nospace() << tab( l + 1 ) << "Color: " << _color;
    qDebug().nospace() << tab( l + 1 ) << "Position: " << _position;
    qDebug().nospace() << tab( l + 1 ) << "Direction: " << _direction;
    qDebug().nospace() << tab( l + 1 ) << "Intensity: " << _intensity;
    qDebug().nospace() << tab( l + 1 ) << "Inner Cut Off Angle: " << _innerCutOff;
    qDebug().nospace() << tab( l + 1 ) << "Outer Cut Off Angle: " << _outerCutOff;
    qDebug().nospace() << tab( l + 1 ) << "Enable attenuation: " << _enableAttenuation;
    if ( _enableAttenuation )
    {
        qDebug().nospace() << tab( l + 2 ) << "Quadratic value: " << _attenuationQuadratic;
        qDebug().nospace() << tab( l + 2 ) << "Linear value:    " << _attenuationLinear;
        qDebug().nospace() << tab( l + 2 ) << "Constant value:  " << _attenuationConstant;
    }
}


void SpotLight::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
}


bool SpotLight::getVisible() const
{
    return(_marker->getVisible() );
}


QVector3D SpotLight::getPosition() const
{
    return(_position);
}


QVector3D SpotLight::getDirection() const
{
    return(_direction);
}


float SpotLight::getInnerCutOff() const
{
    return(_innerCutOff);
}


float SpotLight::getOuterCutOff() const
{
    return(_outerCutOff);
}


bool SpotLight::getEnableAttenuation() const
{
    return(_enableAttenuation);
}


QVector3D SpotLight::getAttenuationArguments() const
{
    return(QVector3D( _attenuationQuadratic, _attenuationLinear, _attenuationConstant ) );
}


float SpotLight::getAttenuationQuadratic() const
{
    return(_attenuationQuadratic);
}


float SpotLight::getAttenuationLinear() const
{
    return(_attenuationLinear);
}


float SpotLight::getAttenuationConstant() const
{
    return(_attenuationConstant);
}


Mesh * SpotLight::getMarker() const
{
    return(_marker);
}

QVector3D SpotLight::getPositionApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getPosition();
    }
    else
    {
        auto position = getPosition();
        return QVector3D(position.z(),position.x(),position.y());
    }
}

QVector3D SpotLight::getDirectionApi() const
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        return getDirection();
    }
    else
    {
        auto direction = getDirection();
        return QVector3D(direction.z(),direction.x(),direction.y());
    }
}


void SpotLight::setColor( QVector3D color )
{
    AbstractLight::setColor( color );
    _marker->getMaterial()->setColor( color );
}


void SpotLight::setVisible( bool visible )
{
    _marker->setVisible( visible );
}


void SpotLight::setEnabled( bool enabled )
{
    AbstractLight::setEnabled( enabled );
    _marker->setVisible( enabled );
}


void SpotLight::setPosition( QVector3D position )
{
    if ( !isEqual( _position, position ) )
    {
        _position = position;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The position of" << this->objectName() << "is set to" << position;
        _marker->setPosition( _position );
        positionChanged( _position );
    }
}


void SpotLight::setDirection( QVector3D direction )
{
    if ( !isEqual( _direction, direction ) )
    {
        _direction = direction;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The direction of" << this->objectName() << "is set to" << direction;
        _marker->setRotation( QQuaternion::rotationTo( QVector3D( 0, -1, 0 ), _direction ) );
        directionChanged( _direction );
    }
}


void SpotLight::setInnerCutOff( float innerCutOff )
{
    if ( !isEqual( _innerCutOff, innerCutOff ) )
    {
        _innerCutOff = innerCutOff;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The inner cut-off of" << this->objectName() << "is set to" << innerCutOff;
        innerCutOffChanged( _innerCutOff );
    }
}


void SpotLight::setOuterCutOff( float outerCutOff )
{
    if ( !isEqual( _outerCutOff, outerCutOff ) )
    {
        _outerCutOff = outerCutOff;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The outer cut-off of" << this->objectName() << "is set to" << outerCutOff;
        outerCutOffChanged( _outerCutOff );
    }
}


void SpotLight::setEnableAttenuation( bool enabled )
{
    if ( _enableAttenuation != enabled )
    {
        _enableAttenuation = enabled;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The attenuation of" << this->objectName()
                << "is" << (enabled ? "enabled" : "disabled");
        enableAttenuationChanged( _enableAttenuation );
    }
}


void SpotLight::setAttenuationArguments( QVector3D value )
{
    setAttenuationQuadratic( value[0] );
    setAttenuationLinear( value[1] );
    setAttenuationConstant( value[2] );
}


void SpotLight::setAttenuationQuadratic( float value )
{
    if ( !isEqual( _attenuationQuadratic, value ) )
    {
        _attenuationQuadratic = value;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The quadratic attenuation arg of" << this->objectName()
                << "is set to" << value;
        attenuationQuadraticChanged( _attenuationQuadratic );
        attenuationArgumentsChanged( this->getAttenuationArguments() );
    }
}


void SpotLight::setAttenuationLinear( float value )
{
    if ( !isEqual( _attenuationLinear, value ) )
    {
        _attenuationLinear = value;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The linear attenuation arg of" << this->objectName()
                << "is set to" << value;
        attenuationLinearChanged( _attenuationLinear );
        attenuationArgumentsChanged( this->getAttenuationArguments() );
    }
}


void SpotLight::setAttenuationConstant( float value )
{
    if ( !isEqual( _attenuationConstant, value ) )
    {
        _attenuationConstant = value;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The constant attenuation arg of" << this->objectName()
                << "is set to" << value;
        attenuationConstantChanged( _attenuationConstant );
        attenuationArgumentsChanged( this->getAttenuationArguments() );
    }
}

void SpotLight::setPositionApi(QVector3D position)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setPosition(position);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        setPosition(QVector3D(position.y(),position.z(),position.x()));
    }
}

void SpotLight::setDirectionApi(QVector3D direction)
{
    if(EngineUtil::axisMode == AXIS_Y_UP)
    {
        setDirection(direction);
    }
    else if(EngineUtil::axisMode == AXIS_Z_UP)
    {
        setDirection(QVector3D(direction.y(),direction.z(),direction.x()));
    }
}



void SpotLight::setDirectionFromRotation( QVector3D rotation )
{
    QVector3D direction = QQuaternion::fromEulerAngles( rotation ) * QVector3D( 0, -1, 0 );
    if ( !isEqual( _direction, direction ) )
    {
        _direction = direction;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "The direction of" << this->objectName() << "is set to" << direction;
        directionChanged( _direction );
    }
}


void SpotLight::initMarker()
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    ModelLoader loader;
    _marker = loader.loadMeshFromFile( ":/shapes/SpotLight.obj" );
    _marker->setPosition( this->getPosition() );
    _marker->getMaterial()->setColor( this->getColor() );
    _marker->setRotation( QQuaternion::rotationTo( QVector3D( 0, -1, 0 ), this->getDirection() ) );
    _marker->setObjectName( "Spotlight Marker" );
    _marker->setParent( this );

    logLevel = tmp_log_level;

    connect( _marker, SIGNAL( visibleChanged( bool ) ), this, SIGNAL( visibleChanged( bool ) ) );
    connect( _marker, SIGNAL( positionChanged( QVector3D ) ), this, SLOT( setPosition( QVector3D ) ) );
    connect( _marker, SIGNAL( rotationChanged( QVector3D ) ), this, SLOT( setDirectionFromRotation( QVector3D ) ) );
}


