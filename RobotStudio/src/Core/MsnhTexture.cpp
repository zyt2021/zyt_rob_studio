﻿#include <Core/MsnhTexture.h>

Texture::Texture(TextureType textureType , QObject *parent) : QObject( parent )
{
    setObjectName( "Untitled Texture" );
    _enabled        = true;
    _textureType	= textureType;
}


Texture::Texture(const Texture & texture , QObject *parent) : QObject( parent )
{
    setObjectName( texture.objectName() );
    _enabled        = true;
    _textureType	= texture._textureType;
    _image          = texture._image;
}


Texture::~Texture()
{
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << "Texture" << objectName() << "is destroyed";
}


void Texture::dumpObjectInfo( int l )
{
    qDebug().nospace() << tab( l ) << "Texture: " << objectName();
    qDebug().nospace() << tab( l + 1 ) << "Enabled: " << _enabled;
    qDebug().nospace() << tab( l + 1 ) << "Type: " <<
    (_textureType == DIFFUSE ? "Diffuse" : (_textureType == SPECULAR ? "Specular" : "Height") );
    qDebug().nospace() << tab( l + 1 ) << "Resolution: " << _image.width() << "*" << _image.height();
}


void Texture::dumpObjectTree( int l )
{
    dumpObjectInfo( l );
}


bool Texture::getEnabled() const
{
    return(_enabled);
}


Texture::TextureType Texture::getTextureType() const
{
    return(_textureType);
}


const QImage & Texture::getImage() const
{
    return(_image);
}


void Texture::setEnabled( bool enabled )
{
    if ( _enabled != enabled )
    {
        _enabled = enabled;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << this->objectName() << "is" << (enabled ? "enabled" : "disabled");
        enabledChanged( _enabled );
    }
}


void Texture::setTextureType( TextureType textureType )
{
    if ( _textureType != textureType )
    {
        _textureType = textureType;
        if ( logLevel >= LOG_LEVEL_INFO )
            dout	<< "The type of texture" << this->objectName() << "is set to"
                << (_textureType == DIFFUSE ? "Diffuse" : (_textureType == SPECULAR ? "Specular" : "Height") );
        textureTypeChanged( _textureType );
    }
}


void Texture::setImage( const QImage & image )
{
    if ( _image != image )
    {
        _image = image;
        imageChanged( _image );
    }
}


QDataStream & operator>>( QDataStream & in, Texture::TextureType & textureType )
{
    qint32 t;
    in >> t;
    if ( t == 0 )
        textureType = Texture::DIFFUSE;
    else if ( t == 1 )
        textureType = Texture::SPECULAR;
    else
        textureType = Texture::BUMP;
    return(in);
}


