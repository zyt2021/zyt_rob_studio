﻿#include <Core/MsnhTextureLoader.h>

QHash<QString, QWeakPointer<Texture> > TextureLoader::cache;

QSharedPointer<Texture> TextureLoader::loadFromFile( Texture::TextureType textureType, QString filePath )
{
    if ( cache[filePath].isNull() )
    {
        if ( logLevel >= LOG_LEVEL_INFO )
            dout << "Loading" << filePath;
        QSharedPointer<Texture> texture( new Texture( textureType ) );
        QImageReader		reader( filePath );
        texture->setObjectName( filePath );
        texture->setImage( reader.read() );

        if ( texture->getImage().isNull() )
        {
            _log += "Failed to load texture " + filePath + ": " + reader.errorString() + '\n';
            if ( logLevel >= LOG_LEVEL_ERROR )
            {
                dout << _log;
                dout << "Failed to load texture:" << reader.errorString();
            }
            return(QSharedPointer<Texture>() );
        }

        cache[filePath] = texture;
        return(texture);
    }
    if ( logLevel >= LOG_LEVEL_INFO )
        dout << filePath << "found in cache";
    return(cache[filePath]);
}


bool TextureLoader::hasErrorLog()
{
    return(_log != "");
}


QString TextureLoader::errorLog()
{
    QString tmp = _log;
    _log = "";
    return(tmp);
}


