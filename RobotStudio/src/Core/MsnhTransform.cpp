﻿#include <Core/MsnhTransform.h>
#include <Core/MsnhModelLoader.h>

Transform::Transform(QObject *parent) : QObject(parent)
{
    _tfModel = ModelLoader::loadTransformAxises();
    _tfModel->setDeleteable(false);
    _tfModel->setParent(this);
    _tfModel->getChildMeshes()[2]->getMaterial()->setColor( QVector3D( 0.66f, 0, 0 ) );
    _tfModel->getChildMeshes()[0]->getMaterial()->setColor( QVector3D( 0, 0.66f, 0 ) );
    _tfModel->getChildMeshes()[1]->getMaterial()->setColor( QVector3D( 0, 0, 0.66f ) );
    _tfModel->getChildMeshes()[2]->setSelectable(false);
    _tfModel->getChildMeshes()[0]->setSelectable(false);
    _tfModel->getChildMeshes()[1]->setSelectable(false);
    _tfModel->setScaling({0.01f,0.01f,0.01f});
}

Transform::Transform(const Transform &tf, QObject *parent) : QObject(parent)
{
    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;

    _tfModel = new Model(tf.getTfModel(),this);
    _tfModel->setDeleteable(false);

    setObjectName(tf.objectName());

    logLevel = tmp_log_level;
}

Transform::~Transform()
{

}


Model *Transform::getTfModel() const
{
    return _tfModel;
}

TransformGroup::TransformGroup(QObject *parent): QObject(parent)
{

}

TransformGroup::TransformGroup(const TransformGroup &tfGroup, QObject *parent): QObject(parent)
{
    for(auto val : _tfVecs)
    {
        _tfVecs.push_back(new Transform(*val,this));
    }
}

void TransformGroup::addTf(const QString& name)
{
    Transform* trans = new Transform(this);
    trans->getTfModel()->setScalingApi({0.01f,0.01f,0.01f});
    trans->setObjectName(name);
    _tfVecs.push_back(trans);
    emit tfAdded(trans);
}

void TransformGroup::addTf(const QString &name, const Msnhnet::Frame &frame, float scale)
{
    Transform* trans = new Transform(this);
    trans->getTfModel()->setScalingApi({scale,scale,scale});

    trans->setObjectName(name);
    trans->getTfModel()->setFrame(frame);
    _tfVecs.push_back(trans);
    emit tfAdded(trans);
}

void TransformGroup::deleteTf(int index)
{
    if(_tfVecs[index])
    {
        delete _tfVecs[index];
    }
    _tfVecs.remove(index);
}

const QVector<Transform *> &TransformGroup::getTfVecs() const
{
    return _tfVecs;
}

int TransformGroup::getTfVecSize() const
{
    return _tfVecs.size();
}
