﻿#include <Core/MsnhTransformGizmo.h>

TransformGizmo::TransformGizmo( QObject* parent ) : AbstractGizmo( parent )
{
    setObjectName( "Transform Gizmo" );

    _translateGizmo             = new TranslateGizmo( this );
    _rotateGizmo                = new RotateGizmo( this );
    _scaleGizmo                 = new ScaleGizmo( this );

    _activatedGizmo = _translateGizmo;
    _activatedGizmo->setVisible( true );

    _alwaysOnTop = true;

    //GIZMO：
    //  |Transfrom:
    //    00 x
    //    01 y
    //    02 z
    //    03 xy
    //    04 yz
    //    05 zx
    //  |Scale:
    //    06 x
    //    07 y
    //    08 z
    //    09 xy
    //    10 yz
    //    11 zx
    //    12 xyz
    //  |Rotate:
    //    13 x
    //    14 y
    //    15 z
    for ( int i = 0; i < _translateGizmo->getMarkers().size(); i++ )
        _markers.push_back( _translateGizmo->getMarkers()[i] );

    for ( int i = 0; i < _scaleGizmo->getMarkers().size(); i++ )
        _markers.push_back( _scaleGizmo->getMarkers()[i] );

    for ( int i = 0; i < _rotateGizmo->getMarkers().size(); i++ )
        _markers.push_back( _rotateGizmo->getMarkers()[i] );
}


TransformGizmo::~TransformGizmo()
{
}


void TransformGizmo::translate( QVector3D delta )
{
    if ( _activatedGizmo )
    {
        _activatedGizmo->translate( delta );
    }
}


void TransformGizmo::rotate( QQuaternion rotation )
{
    if ( _activatedGizmo )
        _activatedGizmo->rotate( rotation );
}


void TransformGizmo::rotate( QVector3D rotation )
{
    if ( _activatedGizmo )
        _activatedGizmo->rotate( rotation );
}


void TransformGizmo::scale( QVector3D scaling )
{
    if ( _activatedGizmo )
        _activatedGizmo->scale( scaling );
}


QVector3D TransformGizmo::getPosition() const
{
    if ( _activatedGizmo )
        return(_activatedGizmo->getPosition() );
    else
        return(QVector3D( 0, 0, 0 ) );
}


QVector3D TransformGizmo::getRotation() const
{
    if ( _activatedGizmo )
        return(_activatedGizmo->getRotation() );
    else
        return(QVector3D( 0, 0, 0 ) );
}


QVector3D TransformGizmo::getScaling() const
{
    if ( _activatedGizmo )
        return(_activatedGizmo->getScaling() );
    else
        return(QVector3D( 1, 1, 1 ) );
}


QMatrix4x4 TransformGizmo::getGlobalModelMatrix() const
{
    if ( _activatedGizmo )
        return(_activatedGizmo->getGlobalModelMatrix() );
    else
        return(QMatrix4x4() );
}


TransformGizmo::TransformAxis TransformGizmo::getAxis() const
{
    if ( _activatedGizmo )
        return(_activatedGizmo->getAxis() );
    else
        return(GIZMO_AXIS_NONE);
}


TransformGizmo::TransformMode TransformGizmo::getGizmoMode() const
{
    if ( _activatedGizmo == _translateGizmo )
        return(GIZMO_TRANSLATE);
    if ( _activatedGizmo == _rotateGizmo )
        return(GIZMO_ROTATE);
    return(GIZMO_SCALE);
}


bool TransformGizmo::getAlwaysOnTop() const
{
    return(_alwaysOnTop);
}


QVector<Mesh*> & TransformGizmo::getMarkers()
{
    return(_markers);
}


void TransformGizmo::drag( QPoint from, QPoint to, int scnWidth, int scnHeight, QMatrix4x4 proj, QMatrix4x4 view )
{
    if ( _activatedGizmo && _activatedGizmo->getHost()->getDraggable())
        _activatedGizmo->drag( from, to, scnWidth, scnHeight, proj, view );
}


void TransformGizmo::bindTo( AbstractEntity * host )
{    
    if ( !host )
    {
        return;
    }

    //如果绑定对象是mesh, 则选择其parent即Model进行绑定, 此处设置禁止修改mesh的姿态属性
    if(host->isMesh())
    {
        host = qobject_cast<AbstractEntity*>(host->parent());
        if(!host->isModel())
        {
            return;
        }
    }

    AbstractGizmo::bindTo( host ); //TransformGizmo调用父类的方法
    _translateGizmo->bindTo( host );
    _rotateGizmo->bindTo( host );
    _scaleGizmo->bindTo( host );
    _visible = (host != nullptr);
}


void TransformGizmo::unbind()
{
    AbstractGizmo::unbind();
    _translateGizmo->unbind();
    _rotateGizmo->unbind();
    _scaleGizmo->unbind();
    _visible = false;
}


void TransformGizmo::setTransformAxis( TransformAxis axis )
{
    if ( _activatedGizmo )
        _activatedGizmo->setTransformAxis( axis );
}


void TransformGizmo::setTransformAxis( void* marker )
{
    if ( _activatedGizmo )
        _activatedGizmo->setTransformAxis( marker );
}


void TransformGizmo::setTransformMode( TransformMode mode )
{    
    _activatedGizmo->setVisible( false );
    if ( mode == GIZMO_TRANSLATE )
    {
        _activatedGizmo = _translateGizmo;
    }
    else if ( mode == GIZMO_ROTATE )
    {
        _activatedGizmo = _rotateGizmo;
    }
    else if ( mode == GIZMO_SCALE )
    {
        _activatedGizmo = _scaleGizmo;
    }
    _activatedGizmo->setVisible( true );
}


void TransformGizmo::setPosition( QVector3D position )
{
    if ( _activatedGizmo )
        _activatedGizmo->setPosition( position );
}


void TransformGizmo::setRotation( QQuaternion rotation )
{
    if ( _activatedGizmo )
        _activatedGizmo->setRotation( rotation );
}


void TransformGizmo::setRotation( QVector3D rotation )
{
    if ( _activatedGizmo )
        _activatedGizmo->setRotation( rotation );
}


void TransformGizmo::setScaling( QVector3D scaling )
{
    if ( _activatedGizmo )
        _activatedGizmo->setScaling( scaling );
}


void TransformGizmo::setAlwaysOnTop( bool alwaysOnTop )
{
    _alwaysOnTop = alwaysOnTop;
}


