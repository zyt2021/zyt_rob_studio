﻿#include <Core/MsnhTranslateGizmo.h>
#include <Core/MsnhModelLoader.h>

TranslateGizmo::TranslateGizmo( QObject* parent ) : AbstractGizmo( parent )
{
    setObjectName( "Translation Gizmo" );
    _markers.resize( 6 );

    int tmp_log_level = logLevel;
    logLevel = LOG_LEVEL_WARNING;


    ///              y
    ///              |
    ///              |   ___
    ///          /|  |  |   |
    ///         | |  |  |___|
    ///         |/   |_____________ x
    ///             /    __
    ///            /   /   /
    ///           /   /__ /
    ///          /
    ///         z

    ModelLoader loader;
    _markers[0]	= loader.loadMeshFromFile( ":/shapes/TransX.obj" );
    _markers[1]	= loader.loadMeshFromFile( ":/shapes/TransY.obj" );
    _markers[2]	= loader.loadMeshFromFile( ":/shapes/TransZ.obj" );

    _markers[3]	= loader.loadMeshFromFile( ":/shapes/XY.obj" );
    _markers[4]	= loader.loadMeshFromFile( ":/shapes/YZ.obj" );
    _markers[5]	= loader.loadMeshFromFile( ":/shapes/ZX.obj" );

    _markers[3]->setAlpha(0.6f);
    _markers[4]->setAlpha(0.6f);
    _markers[5]->setAlpha(0.6f);

    _markers[0]->getMaterial()->setColor( QVector3D( 0.66f, 0, 0 ) );
    _markers[1]->getMaterial()->setColor( QVector3D( 0, 0.66f, 0 ) );
    _markers[2]->getMaterial()->setColor( QVector3D( 0, 0, 0.66f ) );

    _markers[3]->getMaterial()->setColor( QVector3D( 0, 0, 0.66f ) );
    _markers[4]->getMaterial()->setColor( QVector3D( 0.66f, 0, 0 ) );
    _markers[5]->getMaterial()->setColor( QVector3D( 0, 0.66f, 0 ) );


    for ( int i = 0; i < _markers.size(); i++ )
    {
        _markers[i]->setObjectName( "Translate Marker" );
        _markers[i]->setParent( this );
    }

    logLevel = tmp_log_level;

    setParent( parent );
}


TranslateGizmo::~TranslateGizmo()
{
}


void TranslateGizmo::translate( QVector3D delta )
{
    //TODO: setRotation对不上，即界面对不上，参考blender
    if ( _host )
    {
        //局部坐标系
        _host->translate( GeometryEx::rotatePos(getRotation(), delta) );

        //全局坐标系
        //_host->translate( delta );
    }
}


void TranslateGizmo::rotate( QQuaternion )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Rotating a TRANSLATION ONLY gizmo is not allowed.";
}


void TranslateGizmo::rotate( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Rotating a TRANSLATION ONLY gizmo is not allowed.";
}


void TranslateGizmo::scale( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Scaling a TRANSLATION ONLY gizmo is not allowed.";
}


QVector3D TranslateGizmo::getPosition() const
{
    if ( _host )
        return(_host->getPosition() );
    else
        return(QVector3D( 0, 0, 0 ) );
}


QVector3D TranslateGizmo::getRotation() const
{
    if ( _host )
        return(_host->getRotation() );
    else
        return(QVector3D( 0, 0, 0 ) );
}


QVector3D TranslateGizmo::getScaling() const
{
    return(QVector3D( 1, 1, 1 ) );
}


QMatrix4x4 TranslateGizmo::getGlobalSpaceMatrix() const
{
    QMatrix4x4 model = getLocalModelMatrix();

    if ( _host )
    {
        AbstractEntity* parent = qobject_cast<AbstractEntity*>( _host->parent() );

        //        while ( parent )
        //        {
        //            model	= parent->getGlobalModelMatrix() * model;
        //            parent	= qobject_cast<AbstractEntity*>( parent->parent() );
        //        }

        //只取最近的一个parent的位置信息就行
        while ( parent )
        {
            if(parent->isModel()) //找到了就退出
            {
                model	= parent->getGlobalModelMatrix() * model;
                parent	= nullptr;
            }
            else //没找到继续找
            {
                parent	= qobject_cast<AbstractEntity*>( parent->parent() );
            }
        }
    }

    return (model);
}


QMatrix4x4 TranslateGizmo::getGlobalModelMatrix() const
{
    QMatrix4x4 model;
    model.translate( getGlobalSpaceMatrix() * QVector3D( 0, 0, 0 ) );

    if ( _host ) //获取父项累加的旋转
    {
        QQuaternion	globalRotation  = QQuaternion::fromEulerAngles( getRotation() ); //叠加旋转
        AbstractEntity	* parent        = qobject_cast<AbstractEntity*>( _host->parent() );
        while ( parent )
        {
            globalRotation	= QQuaternion::fromEulerAngles( parent->getRotation() ) * globalRotation;
            parent		= qobject_cast<AbstractEntity*>( parent->parent() );
        }
        model.rotate( globalRotation );
    }

    return(model);
}


void TranslateGizmo::drag( QPoint from, QPoint to, int scnWidth, int scnHeight, QMatrix4x4 proj, QMatrix4x4 view )
{
    if (_host == nullptr)
        return;

    Line		l1	= screenPosToWorldRay( QVector2D( from ), QVector2D( scnWidth, scnHeight ), proj, view );
    Line		l2	= screenPosToWorldRay( QVector2D( to ), QVector2D( scnWidth, scnHeight ), proj, view );
    QMatrix4x4	invModelMat	= getGlobalSpaceMatrix().inverted();
    l1	= invModelMat * l1;
    l2	= invModelMat * l2;
    if ( _axis == TranslateGizmo::GIZMO_AXIS_X )
    {
        Line		x	= { QVector3D( 0, 0, 0 ), QVector3D( 1, 0, 0 ) };
        QVector3D	p1	= getClosestPointOfLines( x, l1 );
        QVector3D	p2	= getClosestPointOfLines( x, l2 );
        translate( p2 - p1 );
    }
    else if ( _axis == TranslateGizmo::GIZMO_AXIS_Y )
    {
        Line		y	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 1, 0 ) };
        QVector3D	p1	= getClosestPointOfLines( y, l1 );
        QVector3D	p2	= getClosestPointOfLines( y, l2 );
        translate( p2 - p1 );
    }
    else if ( _axis == TranslateGizmo::GIZMO_AXIS_Z )
    {
        Line		z	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 0, 1 ) };
        QVector3D	p1	= getClosestPointOfLines( z, l1 );
        QVector3D	p2	= getClosestPointOfLines( z, l2 );
        translate( p2 - p1 );
    }
    else if( _axis == TransformAxis::GIZMO_AXIS_XY)
    {
        Line		x	= { QVector3D( 0, 0, 0 ), QVector3D( 1, 0, 0 ) };
        QVector3D	p1	= getClosestPointOfLines( x, l1 );
        QVector3D	p2	= getClosestPointOfLines( x, l2 );
        translate( p2 - p1 );

        x	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 1, 0 ) };
        p1	= getClosestPointOfLines( x, l1 );
        p2	= getClosestPointOfLines( x, l2 );
        translate( p2 - p1 );
    }
    else if( _axis == TransformAxis::GIZMO_AXIS_YZ)
    {
        Line		yz	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 1, 0 ) };
        QVector3D	p1	= getClosestPointOfLines( yz, l1 );
        QVector3D	p2	= getClosestPointOfLines( yz, l2 );
        translate( p2 - p1 );

        yz	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 0, 1 ) };
        p1	= getClosestPointOfLines( yz, l1 );
        p2	= getClosestPointOfLines( yz, l2 );
        translate( p2 - p1 );
    }
    else if( _axis == TransformAxis::GIZMO_AXIS_ZX)
    {
        Line		yz	= { QVector3D( 0, 0, 0 ), QVector3D( 0, 0, 1 ) };
        QVector3D	p1	= getClosestPointOfLines( yz, l1 );
        QVector3D	p2	= getClosestPointOfLines( yz, l2 );
        translate( p2 - p1 );

        yz                      = { QVector3D( 0, 0, 0 ), QVector3D( 1, 0, 0 ) };
        p1                      = getClosestPointOfLines( yz, l1 );
        p2                      = getClosestPointOfLines( yz, l2 );
        translate( p2 - p1 );
    }
}


void TranslateGizmo::setPosition( QVector3D position )
{
    if ( _host )
        _host->setPosition( position );
}


void TranslateGizmo::setRotation( QQuaternion )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Setting the rotation of a TRANSLATION ONLY gizmo is not allowed";
}


void TranslateGizmo::setRotation( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Setting the rotation of a TRANSLATION ONLY gizmo is not allowed";
}


void TranslateGizmo::setScaling( QVector3D )
{
    if ( logLevel >= LOG_LEVEL_WARNING )
        dout << "Setting the scaling of a TRANSLATION ONLY gizmo is not allowed";
}


