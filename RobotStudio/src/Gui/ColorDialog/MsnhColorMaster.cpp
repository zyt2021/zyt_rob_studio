﻿#include <Gui/ColorDialog/MsnhColorMaster.h>
#include <QPainter>
#include <QMouseEvent>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

ColorMaster::ColorMaster(QWidget *parent) :
    QWidget(parent),_color(Qt::black), _nWidth(25),_colorDialog(new ColorDialog(this)),
    _background(Qt::darkGray, Qt::DiagCrossPattern)
{
    _colorDialog->hide();
    _background.setTexture(QPixmap(QString(":/images/alphaback.png")));
    connect(_colorDialog, SIGNAL(colorSelected(QColor)), this, SLOT(ChangeColor(QColor)));
    this->installEventFilter(this);
}

void ColorMaster::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen(Qt::white,1.5));
    painter.setBrush(_background);
    painter.drawRect(0,0,_nWidth,_nWidth);
    painter.setBrush(_color);
    painter.drawRect(1,1,_nWidth, _nWidth);
}

void ColorMaster::SetSquareWidth(int width)
{
    this->_nWidth = width;
}

void ColorMaster::ChangeColor(QColor color)
{
    this->_color = color;
    emit colorChanged(color);
    update();
}

bool ColorMaster::eventFilter(QObject *obj, QEvent *e)
{
    if(e->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *event = static_cast<QMouseEvent*> (e);
        if(event->button() == Qt::LeftButton)
        {
            _colorDialog->SetColor(_color);
            _colorDialog->show();
        }
        return true;
    }
    return QWidget::eventFilter(obj,e);
}

void ColorMaster::setColor(const QColor &value)
{
    _color = value;
}

QColor ColorMaster::getColor() const
{
    return _color;
}

//void XColorMaster::mousePressEvent(QMouseEvent *event)
//{
//    if(event->button() == Qt::LeftButton)
//    {
//        colorDialog->SetColor(color);
//        colorDialog->show();
//    }
//}
