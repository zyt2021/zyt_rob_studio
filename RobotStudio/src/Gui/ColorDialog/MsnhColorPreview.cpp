﻿#include <Gui/ColorDialog/MsnhColorPreview.h>
#include <QStylePainter>
#include <QStyleOptionFrame>
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

ColorPreview::ColorPreview(QWidget *parent) :
    QWidget(parent), col(Qt::red), back( Qt::darkGray, Qt::DiagCrossPattern ),
    alpha_mode(NoAlpha), colorPrevious(Qt::blue)
{
    back.setTexture(QPixmap(QString(":/qss/psblack/alpha_back.png")));
}

QSize ColorPreview::sizeHint() const
{
    return QSize(24,24);
}

void ColorPreview::setColor(QColor c)
{
    col = c;
    update();
    emit colorChanged(c);
}

void ColorPreview::setPreviousColor(QColor colorPre)
{
    colorPrevious = colorPre;
    update();
}

void ColorPreview::paintEvent(QPaintEvent *)
{
    QStylePainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing,true);

    painter.setPen(QPen(Qt::black, 1));
    QSize rectSize(62,64);
    painter.setBrush(back);
    painter.drawRect(1,1,rectSize.width(),rectSize.height());

    int h = rectSize.height() / 2;
    painter.setPen(Qt::NoPen);
    painter.setBrush(col);
    painter.drawRect(1,1,rectSize.width(),h);
    painter.setBrush(colorPrevious);
    painter.drawRect(1,h + 1,rectSize.width(),rectSize.height() - h);
}


