﻿#include "Gui/DataKits/MsnhCartesianWidget.h"
#include "ui_MsnhCartesianWidget.h"

MsnhCartesianWidget::MsnhCartesianWidget(const QString &name, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhCartesianWidget)
{
    ui->setupUi(this);

    _timer = new QTimer(this);

    connect(_timer, &QTimer::timeout, this, &MsnhCartesianWidget::doTimer);

    _timer->start(20);

    _name = name;
    ui->tfName->setText(name);

    ui->linearSpeed->setVal(0.001f);
    ui->linearSpeed->setMin(0.001f);
    ui->linearSpeed->setPrecision(0.001f);
    ui->linearSpeed->setMax(1.f);
    ui->angleSpeed->setVal(0.01f);
    ui->angleSpeed->setPrecision(0.01f);
    ui->angleSpeed->setMin(0.01f);
    ui->angleSpeed->setMax(10.f);
}

MsnhCartesianWidget::~MsnhCartesianWidget()
{
    delete ui;
}

void MsnhCartesianWidget::updateFrame(const Msnhnet::Frame &frame)
{
    if(Msnhnet::Frame::diff(frame, _frame).length() > MSNH_F32_EPS)
    {
        _frame = frame;
        showFrame();
    }
}

void MsnhCartesianWidget::doTimer()
{
    if(worldXAdd)
    {
        double value = _frame.trans[0] + ui->linearSpeed->text().toDouble() ;
        QVector3D pos((float)value, (float)(_frame.trans[1]), (float)(_frame.trans[2]));
        emit changePosAngle(_name, false, pos);
    }
    else if(worldYAdd)
    {
        double value = _frame.trans[1] + ui->linearSpeed->text().toDouble() ;
        QVector3D pos((float)(_frame.trans[0]), (float)value, (float)(_frame.trans[2]));
        emit changePosAngle(_name, false, pos);
    }
    else if(worldZAdd)
    {
        double value = _frame.trans[2] + ui->linearSpeed->text().toDouble() ;
        QVector3D pos((float)(_frame.trans[0]), (float)(_frame.trans[1]), (float)value);
        emit changePosAngle(_name, false, pos);
    }
    else if(worldRxAdd)
    {
        Msnhnet::RotationMatDS rotmat = _frame.rotMat;
        Msnhnet::EulerDS       euler  = Msnhnet::GeometryS::rotMat2Euler(rotmat, Msnhnet::ROT_ZYX);

        euler = euler*MSNH_RAD_2_DEG;

        double angle = euler[0] + ui->angleSpeed->text().toDouble() ;
        if(angle<-360)
        {
            angle =angle + 720;
        }
        else if(angle>360)
        {
            angle = angle - 720;
        }

        QVector3D pos(angle, (float)(euler[1]), (float)(euler[2]));
        emit changePosAngle(_name, true, pos);
    }
    else if(worldRyAdd)
    {
        Msnhnet::RotationMatDS rotmat = _frame.rotMat;
        Msnhnet::EulerDS       euler  = Msnhnet::GeometryS::rotMat2Euler(rotmat, Msnhnet::ROT_ZYX);

        euler = euler*MSNH_RAD_2_DEG;

        double angle = euler[1] + ui->angleSpeed->text().toDouble() ;
        if(angle<-360)
        {
            angle =angle + 720;
        }
        else if(angle>360)
        {
            angle = angle - 720;
        }

        QVector3D pos((float)(euler[0]), angle, (float)(euler[2]));
        emit changePosAngle(_name, true, pos);
    }
    else if(worldRzAdd)
    {
        Msnhnet::RotationMatDS rotmat = _frame.rotMat;
        Msnhnet::EulerDS       euler  = Msnhnet::GeometryS::rotMat2Euler(rotmat, Msnhnet::ROT_ZYX);

        euler = euler*MSNH_RAD_2_DEG;

        double angle = euler[2] + ui->angleSpeed->text().toDouble() ;
        if(angle<-360)
        {
            angle =angle + 720;
        }
        else if(angle>360)
        {
            angle = angle - 720;
        }

        QVector3D pos((float)(euler[0]), (float)(euler[1]), angle);
        emit changePosAngle(_name, true, pos);
    }
    else if(worldXDec)
    {
        double value = _frame.trans[0] - ui->linearSpeed->text().toDouble() ;
        QVector3D pos((float)value, (float)(_frame.trans[1]), (float)(_frame.trans[2]));
        emit changePosAngle(_name, false, pos);
    }
    else if(worldYDec)
    {
        double value = _frame.trans[1] - ui->linearSpeed->text().toDouble() ;
        QVector3D pos((float)(_frame.trans[0]), (float)value, (float)(_frame.trans[2]));
        emit changePosAngle(_name, false, pos);
    }
    else if(worldZDec)
    {
        double value = _frame.trans[2] - ui->linearSpeed->text().toDouble() ;
        QVector3D pos((float)(_frame.trans[0]), (float)(_frame.trans[1]), (float)value);
        emit changePosAngle(_name, false, pos);
    }
    else if(worldRxDec)
    {
        Msnhnet::RotationMatDS rotmat = _frame.rotMat;
        Msnhnet::EulerDS       euler  = Msnhnet::GeometryS::rotMat2Euler(rotmat, Msnhnet::ROT_ZYX);

        euler = euler*MSNH_RAD_2_DEG;

        double angle = euler[0] - ui->angleSpeed->text().toDouble() ;
        if(angle<-360)
        {
            angle =angle + 720;
        }
        else if(angle>360)
        {
            angle = angle - 720;
        }

        QVector3D pos(angle, (float)(euler[1]), (float)(euler[2]));
        emit changePosAngle(_name, true, pos);
    }
    else if(worldRyDec)
    {
        Msnhnet::RotationMatDS rotmat = _frame.rotMat;
        Msnhnet::EulerDS       euler  = Msnhnet::GeometryS::rotMat2Euler(rotmat, Msnhnet::ROT_ZYX);

        euler = euler*MSNH_RAD_2_DEG;

        double angle = euler[1] - ui->angleSpeed->text().toDouble() ;
        if(angle<-360)
        {
            angle =angle + 720;
        }
        else if(angle>360)
        {
            angle = angle - 720;
        }

        QVector3D pos((float)(euler[0]), angle, (float)(euler[2]));
        emit changePosAngle(_name, true, pos);
    }
    else if(worldRzDec)
    {
        Msnhnet::RotationMatDS rotmat = _frame.rotMat;
        Msnhnet::EulerDS       euler  = Msnhnet::GeometryS::rotMat2Euler(rotmat, Msnhnet::ROT_ZYX);

        euler = euler*MSNH_RAD_2_DEG;

        double angle = euler[2] - ui->angleSpeed->text().toDouble() ;
        if(angle<-360)
        {
            angle =angle + 720;
        }
        else if(angle>360)
        {
            angle = angle - 720;
        }

        QVector3D pos((float)(euler[0]), (float)(euler[1]), angle);
        emit changePosAngle(_name, true, pos);
    }
}

void MsnhCartesianWidget::on_worldXAddBtn_pressed()
{
    worldXAdd = true;
}

void MsnhCartesianWidget::on_worldYAddBtn_pressed()
{
    worldYAdd = true;
}

void MsnhCartesianWidget::on_worldZAddBtn_pressed()
{
    worldZAdd = true;
}

void MsnhCartesianWidget::on_worldRxAddBtn_pressed()
{
    worldRxAdd = true;
}

void MsnhCartesianWidget::on_worldRyAddBtn_pressed()
{
    worldRyAdd = true;
}

void MsnhCartesianWidget::on_worldRzAddBtn_pressed()
{
    worldRzAdd = true;
}

void MsnhCartesianWidget::on_worldXDecBtn_pressed()
{
    worldXDec = true;
}

void MsnhCartesianWidget::on_worldYDecBtn_pressed()
{
    worldYDec = true;
}

void MsnhCartesianWidget::on_worldZDecBtn_pressed()
{
    worldZDec = true;
}

void MsnhCartesianWidget::on_worldRxDecBtn_pressed()
{
    worldRxDec = true;
}

void MsnhCartesianWidget::on_worldRyDecBtn_pressed()
{
    worldRyDec = true;
}

void MsnhCartesianWidget::on_worldRzDecBtn_pressed()
{
    worldRzDec = true;
}

void MsnhCartesianWidget::on_toolXAddBtn_pressed()
{
    toolXAdd = true;
}

void MsnhCartesianWidget::on_toolYAddBtn_pressed()
{
    toolYAdd = true;
}

void MsnhCartesianWidget::on_toolZAddBtn_pressed()
{
    toolZAdd = true;
}

void MsnhCartesianWidget::on_toolRxAddBtn_pressed()
{
    toolRxAdd = true;
}

void MsnhCartesianWidget::on_toolRyAddBtn_pressed()
{
    toolRyAdd = true;
}

void MsnhCartesianWidget::on_toolRzAddBtn_pressed()
{
    toolRzAdd = true;
}

void MsnhCartesianWidget::on_toolXDecBtn_pressed()
{
    toolXDec = true;
}

void MsnhCartesianWidget::on_toolYDecBtn_pressed()
{
    toolYDec = true;
}

void MsnhCartesianWidget::on_toolZDecBtn_pressed()
{
    toolZDec = true;
}

void MsnhCartesianWidget::on_toolRxDecBtn_pressed()
{
    toolRxDec = true;
}

void MsnhCartesianWidget::on_toolRyDecBtn_pressed()
{
    toolRyDec = true;
}

void MsnhCartesianWidget::on_toolRzDecBtn_pressed()
{
    toolRzDec = true;
}

void MsnhCartesianWidget::on_worldXAddBtn_released()
{
    worldXAdd = false;
}

void MsnhCartesianWidget::on_worldYAddBtn_released()
{
    worldYAdd = false;
}

void MsnhCartesianWidget::on_worldZAddBtn_released()
{
    worldZAdd = false;
}

void MsnhCartesianWidget::on_worldRxAddBtn_released()
{
    worldRxAdd = false;
}

void MsnhCartesianWidget::on_worldRyAddBtn_released()
{
    worldRyAdd = false;
}

void MsnhCartesianWidget::on_worldRzAddBtn_released()
{
    worldRzAdd = false;
}

void MsnhCartesianWidget::on_worldXDecBtn_released()
{
    worldXDec = false;
}

void MsnhCartesianWidget::on_worldYDecBtn_released()
{
    worldYDec = false;
}

void MsnhCartesianWidget::on_worldZDecBtn_released()
{
    worldZDec = false;
}

void MsnhCartesianWidget::on_worldRxDecBtn_released()
{
    worldRxDec = false;
}

void MsnhCartesianWidget::on_worldRyDecBtn_released()
{
    worldRyDec = false;
}

void MsnhCartesianWidget::on_worldRzDecBtn_released()
{
    worldRzDec = false;
}

void MsnhCartesianWidget::on_toolXAddBtn_released()
{
    toolXAdd = false;
}

void MsnhCartesianWidget::on_toolYAddBtn_released()
{
    toolYAdd = false;
}

void MsnhCartesianWidget::on_toolZAddBtn_released()
{
    toolZAdd = false;
}

void MsnhCartesianWidget::on_toolRxAddBtn_released()
{
    toolRxAdd = false;
}

void MsnhCartesianWidget::on_toolRyAddBtn_released()
{
    toolRyAdd = false;
}

void MsnhCartesianWidget::on_toolRzAddBtn_released()
{
    toolRzAdd = false;
}

void MsnhCartesianWidget::on_toolXDecBtn_released()
{
    toolXDec = false;
}

void MsnhCartesianWidget::on_toolYDecBtn_released()
{
    toolYDec = false;
}

void MsnhCartesianWidget::on_toolZDecBtn_released()
{
    toolZDec = false;
}

void MsnhCartesianWidget::on_toolRxDecBtn_released()
{
    toolRxDec = false;
}

void MsnhCartesianWidget::on_toolRyDecBtn_released()
{
    toolRyDec = false;
}

void MsnhCartesianWidget::on_toolRzDecBtn_released()
{
    toolRzDec = false;
}

void MsnhCartesianWidget::showFrame()
{
    auto trans = _frame.trans;
    float x    = static_cast<float>(trans[0]);
    float y    = static_cast<float>(trans[1]);
    float z    = static_cast<float>(trans[2]);

    auto euler = Msnhnet::GeometryS::rotMat2Euler(_frame.rotMat, Msnhnet::ROT_ZYX);
    euler      = euler*MSNH_RAD_2_DEG;

    float rx   = static_cast<float>(euler[0]);
    float ry   = static_cast<float>(euler[1]);
    float rz   = static_cast<float>(euler[2]);

    ui->xTxt->setText(QString::number(x,'f',3));
    ui->yTxt->setText(QString::number(y,'f',3));
    ui->zTxt->setText(QString::number(z,'f',3));

    ui->rxTxt->setText(QString::number(rx,'f',3));
    ui->ryTxt->setText(QString::number(ry,'f',3));
    ui->rzTxt->setText(QString::number(rz,'f',3));
}
