﻿#include <Gui/DataKits/MsnhFloatBox.h>

MsnhFloatBox::MsnhFloatBox(QWidget *parent):QLineEdit(parent)
{
    QRegExpValidator *limit = new QRegExpValidator(QRegExp("[+-]?([0-9]*[.])?[0-9]+"),this);
    this->setValidator(limit);
    setCursor(Qt::SizeHorCursor);
    connect(this,&MsnhFloatBox::editingFinished,this,&MsnhFloatBox::changedVal);
    setVal(_val);
}

float MsnhFloatBox::getVal() const
{
    return _val;
}

void MsnhFloatBox::setVal(float value)
{
    if(value < _min)
    {
        value = _min;
    }

    if(value > _max)
    {
        value = _max;
    }

    _val = value;
    emit valueChanged(_val);
    setText(QString::number(value,'f',3));
}

void MsnhFloatBox::updateVal(float value)
{
    if(value < _min)
    {
        value = _min;
    }

    if(value > _max)
    {
        value = _max;
    }

    _val = value;
    setText(QString::number(value,'f',3));
}

float MsnhFloatBox::getMax() const
{
    return _max;
}

void MsnhFloatBox::setMax(float value)
{
    if(value <= _min)
    {
        return;
    }
    _max = value;
}

float MsnhFloatBox::getMin() const
{
    return _min;
}

void MsnhFloatBox::setMin(float value)
{
    if(value >= _max)
    {
        return;
    }
    _min = value;
}

void MsnhFloatBox::setPrecision(float value)
{
    _precision = value;
}

float MsnhFloatBox::getPrecision() const
{
    return _precision;
}

void MsnhFloatBox::mousePressEvent(QMouseEvent *event)
{
    event->accept();
    if (!(event->buttons() & Qt::LeftButton))
    {
        return;
    }
    _isDragging = true;
    _pressed = QCursor::pos();
    this->selectAll();
}

void MsnhFloatBox::mouseMoveEvent(QMouseEvent *event)
{
    if (!(event->buttons() & Qt::LeftButton))
    {
        return;
    }

    if(_isDragging)
    {
        int dragDist = QCursor::pos().x() - _pressed.x();
        _pressed = QCursor::pos();

        if (dragDist == 0)
        {
            return;
        }
        _val += dragDist * _precision;

        if(_val > this->getMax())
        {
            _val = this->getMax();
        }

        if(_val < this->getMin())
        {
            _val = this->getMin();
        }
        setVal(_val);
    }
    event->accept();
}

void MsnhFloatBox::mouseReleaseEvent(QMouseEvent *event)
{

    if (!(event->buttons() & Qt::LeftButton))
    {
        return;
    }

    _isDragging = false;
    event->accept();
}

void MsnhFloatBox::changedVal()
{
    QRegExp rx("[+-]?([0-9]*[.])?[0-9]+");
    QString str = text().trimmed();
    if(rx.exactMatch(str))
    {
        setVal(str.toFloat());
    }
    else
    {
        setVal(_val);
    }
}


void MsnhFloatBox::setMinMax(float min, float max)
{
    if(min >= _max)
    {
        return;
    }
    this->_min = min;
    this->_max = max;
}
