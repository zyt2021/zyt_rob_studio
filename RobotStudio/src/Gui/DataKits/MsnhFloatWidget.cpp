﻿#include <Gui/DataKits/MsnhFloatWidget.h>
#include "ui_MsnhFloatWidget.h"

MsnhFloatWidget::MsnhFloatWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MsnhFloatWidget)
{
    ui->setupUi(this);
    connect(ui->floatVal,&MsnhFloatBox::valueChanged,this,&MsnhFloatWidget::valueChanged);
    connect(ui->floatVal,&MsnhFloatBox::valueChanged,this,&MsnhFloatWidget::changedWithName);
}

MsnhFloatWidget::~MsnhFloatWidget()
{
    delete ui;
}

void MsnhFloatWidget::setName(const QString &name)
{
    ui->name->setText(name);
    _name = name;
}

void MsnhFloatWidget::setMax(const float &max, const bool &needRad2Deg)
{
    if(max == DBL_MAX)
    {
        ui->floatVal->setMax(max);
        ui->maxValTxt->setText("inf");
    }
    else
    {
        if(needRad2Deg)
        {
            float tmp = max*MSNH_RAD_2_DEG;
            ui->floatVal->setMax(tmp);
            ui->maxValTxt->setText(QString::number(tmp));
        }
        else
        {
            ui->floatVal->setMax(max);
            ui->maxValTxt->setText(QString::number(max));
        }
    }

}

void MsnhFloatWidget::setMin(const float &min, const bool &needRad2Deg)
{
    if(min == -DBL_MAX)
    {
        ui->floatVal->setMin(min);
        ui->minValTxt->setText("inf");
    }
    else
    {
        if(needRad2Deg)
        {
            float tmp = min*MSNH_RAD_2_DEG;
            ui->floatVal->setMin(tmp);
            ui->minValTxt->setText(QString::number(tmp));
        }
        else
        {
            ui->floatVal->setMin(min);
            ui->minValTxt->setText(QString::number(min));
        }
    }
}

void MsnhFloatWidget::setVal(const float &val, const bool &needRad2Deg)
{
    if(needRad2Deg)
    {
        float tmp = val*MSNH_RAD_2_DEG;
        ui->floatVal->setVal(tmp);
    }
    else
    {
        ui->floatVal->setVal(val);
    }
}

void MsnhFloatWidget::updateVal(float value)
{
    ui->floatVal->updateVal(value);
}

float MsnhFloatWidget::getVal()
{
    return ui->floatVal->getVal();
}

void MsnhFloatWidget::changedWithName(float val)
{
    emit valueChangedWithName(_name,val);
}

