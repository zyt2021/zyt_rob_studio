﻿#include <Gui/DataKits/MsnhPlanWidget.h>
#include "ui_MsnhPlanWidget.h"

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhPlanWidget::MsnhPlanWidget(const QString &name, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhPlanWidget)
{
    ui->setupUi(this);

    _name    = name;

    _jogging = false;

    ui->tfName->setText(_name);

    ui->planMethodCbx->addItems({"关节","关节组","直线","直线组","圆角直线组","圆弧规划"});
    ui->rotationInterpMethodCbx->addItems({"轴角","四元数"});

    ui->maxSpeed->setVal(0.5f);
    ui->maxSpeed->setMin(0.001f);
    ui->maxSpeed->setPrecision(0.001f);

    ui->maxAcc->setVal(0.2f);
    ui->maxAcc->setMin(0.001f);
    ui->maxAcc->setPrecision(0.001f);

    ui->deltaTime->setVal(0.1f);
    ui->deltaTime->setMin(0.001f);
    ui->deltaTime->setPrecision(0.001f);

    ui->rounRadius->setVal(0.05f);
    ui->rounRadius->setMin(0.001f);
    ui->rounRadius->setPrecision(0.001f);

    ui->xTxt->setMinMax(-FLT_MAX, FLT_MAX);
    ui->yTxt->setMinMax(-FLT_MAX, FLT_MAX);
    ui->zTxt->setMinMax(-FLT_MAX, FLT_MAX);

    ui->xTxt->setPrecision(0.001f);
    ui->yTxt->setPrecision(0.001f);
    ui->zTxt->setPrecision(0.001f);

    ui->rxTxt->setMinMax(-FLT_MAX, FLT_MAX);
    ui->ryTxt->setMinMax(-FLT_MAX, FLT_MAX);
    ui->rzTxt->setMinMax(-FLT_MAX, FLT_MAX);

    connect(ui->xTxt, &MsnhFloatBox::valueChanged, this, &MsnhPlanWidget::onChangeEndEffort);
    connect(ui->yTxt, &MsnhFloatBox::valueChanged, this, &MsnhPlanWidget::onChangeEndEffort);
    connect(ui->zTxt, &MsnhFloatBox::valueChanged, this, &MsnhPlanWidget::onChangeEndEffort);

    connect(ui->rxTxt, &MsnhFloatBox::valueChanged, this, &MsnhPlanWidget::onChangeEndEffort);
    connect(ui->ryTxt, &MsnhFloatBox::valueChanged, this, &MsnhPlanWidget::onChangeEndEffort);
    connect(ui->rzTxt, &MsnhFloatBox::valueChanged, this, &MsnhPlanWidget::onChangeEndEffort);
}

MsnhPlanWidget::~MsnhPlanWidget()
{
    TfPlanParams tfPlanParam;
    tfPlanParam.speed       = ui->maxSpeed->getVal();
    tfPlanParam.acc         = ui->maxAcc->getVal();
    tfPlanParam.radius      = ui->rounRadius->getVal();
    tfPlanParam.step        = ui->deltaTime->getVal();
    tfPlanParam.goHome      = ui->gotoHomeCkbx->isChecked();
    tfPlanParam.fullCircle  = ui->fullCircleCkbx->isChecked();
    tfPlanParam.planType    = (Msnhnet::PlanType)(ui->planMethodCbx->currentIndex());
    tfPlanParam.rotInterp   = (Msnhnet::RotationInterpType)ui->rotationInterpMethodCbx->currentIndex();
    emit updateTfPlanParams(_name, tfPlanParam);
    delete ui;
}

void MsnhPlanWidget::updateFrame(const Msnhnet::Frame &frame, bool updateNow)
{
    if(Msnhnet::Frame::diff(frame, _frame).length() > MSNH_F32_EPS)
    {
        _frame = frame;
    }

    if(updateNow)
    {
        syncFrame();
    }
}

void MsnhPlanWidget::setMaxStep(int maxStep)
{
    ui->stepSlider->setMaximum(maxStep);
}

void MsnhPlanWidget::setStep(int step)
{
    ui->stepSlider->setValue(step);
}

void MsnhPlanWidget::setAnimated(bool ok)
{
    ui->animateCkbx->setChecked(ok);
}

void MsnhPlanWidget::refreshWayPoints(const QVector<Msnhnet::Frame> &frames)
{
    ui->wayPointsList->clear();

    QString wayPointStr = "";

    for (int i = 0; i < frames.size(); ++i)
    {
        double x = frames[i].trans[0];
        double y = frames[i].trans[1];
        double z = frames[i].trans[2];

        Msnhnet::EulerDS euler = Msnhnet::GeometryS::rotMat2Euler(frames[i].rotMat,Msnhnet::ROT_ZYX);
        double rx = euler[0]*MSNH_RAD_2_DEG;
        double ry = euler[1]*MSNH_RAD_2_DEG;
        double rz = euler[2]*MSNH_RAD_2_DEG;

        wayPointStr = QString::number(x, 'f', 2) + "," +
                QString::number(y, 'f', 2) + "," +
                QString::number(z, 'f', 2) + "," +
                QString::number(rx, 'f', 2) + "," +
                QString::number(ry, 'f', 2) + "," +
                QString::number(rz, 'f', 2);

        ui->wayPointsList->addItem(wayPointStr);
    }
}

void MsnhPlanWidget::setPublishAddr(const QString &addr)
{
    ui->pubJointAddrTxt->setText(addr);
}

void MsnhPlanWidget::refreshPlanParams(const TfPlanParams &tfPlanParam)
{
    ui->maxSpeed->setVal(tfPlanParam.speed);
    ui->maxAcc->setVal(tfPlanParam.acc);
    ui->rounRadius->setVal(tfPlanParam.radius);
    ui->deltaTime->setVal(tfPlanParam.step);
    ui->planMethodCbx->setCurrentIndex((int)tfPlanParam.planType);
    ui->rotationInterpMethodCbx->setCurrentIndex((int)tfPlanParam.rotInterp);
    ui->gotoHomeCkbx->setChecked(tfPlanParam.goHome);
    ui->fullCircleCkbx->setChecked(tfPlanParam.fullCircle);
}

void MsnhPlanWidget::executionDone()
{
    if(_jogging)
    {

    }
}

void MsnhPlanWidget::syncFrame()
{
    auto trans = _frame.trans;
    float x    = static_cast<float>(trans[0]);
    float y    = static_cast<float>(trans[1]);
    float z    = static_cast<float>(trans[2]);

    auto euler = Msnhnet::GeometryS::rotMat2Euler(_frame.rotMat, Msnhnet::ROT_ZYX);
    euler      = euler*MSNH_RAD_2_DEG;

    float rx   = static_cast<float>(euler[0]);
    float ry   = static_cast<float>(euler[1]);
    float rz   = static_cast<float>(euler[2]);

    ui->xTxt->updateVal(x);
    ui->yTxt->updateVal(y);
    ui->zTxt->updateVal(z);

    ui->rxTxt->updateVal(rx);
    ui->ryTxt->updateVal(ry);
    ui->rzTxt->updateVal(rz);
}

void MsnhPlanWidget::onChangeEndEffort(float val)
{
    (void)val;
    emit changeEndEffort(_name, ui->xTxt->getVal(), ui->yTxt->getVal(), ui->zTxt->getVal(),
                                ui->rxTxt->getVal(), ui->ryTxt->getVal(), ui->rzTxt->getVal());
}

void MsnhPlanWidget::on_planBtn_clicked()
{
    Msnhnet::PlanType planType = static_cast<Msnhnet::PlanType>(ui->planMethodCbx->currentIndex());
    Msnhnet::RotationInterpType rotType = static_cast<Msnhnet::RotationInterpType>(ui->rotationInterpMethodCbx->currentIndex());

    float maxFailedRate = ui->failedSbx->value()/100.0f;

    emit plan(_name, ui->maxSpeed->getVal(), ui->maxAcc->getVal(), ui->deltaTime->getVal(), ui->rounRadius->getVal(), maxFailedRate, rotType, planType, ui->gotoHomeCkbx->isChecked(), ui->fullCircleCkbx->isChecked());

    ui->stepSlider->setValue(0);
}

void MsnhPlanWidget::on_runBtn_clicked()
{
    emit runPlan(_name);
    return;
}

void MsnhPlanWidget::on_animateCkbx_clicked()
{
    emit animatePlan(_name,ui->animateCkbx->isChecked());
}


void MsnhPlanWidget::on_addPointBtn_clicked()
{
    emit addWayPoint(_name);
}

void MsnhPlanWidget::on_insertPointBtn_clicked()
{
    if(ui->wayPointsList->currentItem())
    {
        int index = ui->wayPointsList->currentRow();
        emit insertWayPoint(_name, index);
    }
    else
    {
        QMessageBox::warning(this,"警告","请选择一项再继续");
    }
}

void MsnhPlanWidget::on_deletePointBtn_clicked()
{
    if(ui->wayPointsList->currentItem())
    {
        int index = ui->wayPointsList->currentRow();
        emit deleteWayPoint(_name, index);

        QListWidgetItem* item = ui->wayPointsList->takeItem(index);
        ui->wayPointsList->removeItemWidget(item);
        delete item;
    }
    else
    {
        QMessageBox::warning(this,"警告","请选择一项再继续");
    }
}

void MsnhPlanWidget::on_clearPointsBtn_clicked()
{
    emit clearWayPoints(_name);

    ui->wayPointsList->clear();
}


void MsnhPlanWidget::on_wayPointsList_itemDoubleClicked(QListWidgetItem *item)
{
    Q_UNUSED(item);
    emit endEffortToSelect(_name,ui->wayPointsList->currentRow());
}


void MsnhPlanWidget::on_gotoZeroBtn_clicked()
{
    emit gotoZero(_name);
}


void MsnhPlanWidget::on_syncBtn_clicked()
{
    emit syncRobot(_name);
}

void MsnhPlanWidget::on_updateBtn_clicked()
{
    syncFrame();
}
