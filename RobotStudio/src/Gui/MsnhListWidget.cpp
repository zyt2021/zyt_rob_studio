﻿#include <Gui/MsnhListWidget.h>
#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhListWidget::MsnhListWidget(QWidget *parent):QListWidget(parent)
{
    contextMenu = new QMenu(this);
    delAction = new QAction("删除",this);
    contextMenu->addAction(delAction);
}

MsnhListWidget::~MsnhListWidget()
{

}

void MsnhListWidget::mousePressEvent(QMouseEvent *event)
{
    //确保右键点击，然后跳出菜单.
    if (event->button() == Qt::RightButton)
    {
        contextMenu->exec(event->globalPos());
    }
    //要继续保留QListWidget原有的点击事件.
    QListWidget::mousePressEvent(event);
}
