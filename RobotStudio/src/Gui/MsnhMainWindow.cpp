﻿#include "Gui/MsnhMainWindow.h"
#include "ui_MsnhMainWindow.h"

#include <QVector3D>
#include <QPixmap>

#include <Core/MsnhSceneSaver.h>
#include <Core/MsnhPointsCloudIO.h>
#include <Gui/DataKits/MsnhPlanWidget.h>


#include <lib/MsnhTestBase.h>
#include <lib/MsnhTestImage.h>
#include <lib/MsnhOpenCVCamera.h>
#include <Plugin/MsnhProcessFactory.h>
#include <Plugin/MsnhPluginManager.h>
#include <Msnhnet/cv/MsnhCVVideo.h>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

QPlainTextEdit* MsnhMainWindow::logTxt  = nullptr;

MsnhMainWindow::MsnhMainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MsnhMainWindow)
{
    ui->setupUi(this);
    this->showMaximized();

    _openGLWindow = new OpenGLWindow();
    _openGLWindow->setAxisMode(AXIS_Z_UP);
    auto wd = QWidget::createWindowContainer(_openGLWindow);
    ui->mainGLArea->addWidget(wd);

    // Tool Box
    MsnhTool transTool("Transform", QIcon (":/icon/transform.png"));
    MsnhTool rotTool("Rotation", QIcon (":/icon/rotate.png"));
    MsnhTool scaleTool("Scale", QIcon (":/icon/scale.png"));
    _openGLWindow->addTool(transTool);
    _openGLWindow->addTool(rotTool);
    _openGLWindow->addTool(scaleTool);
    _openGLWindow->setToolActivated(0);
    connect(_openGLWindow, &OpenGLWindow::toolChanged,[this](int index)->void{
        if(index == 0)
            ui->actionSelectTransGizmo->triggered(true);
        else if(index == 1)
            ui->actionSelectRotGizmo->triggered(true);
        else if(index == 2)
            ui->actionSelectScaleGizmo->triggered(true);
    });

    //    this->showFullScreen();
    logTxt = ui->loggerTxt;
    logTxt->setReadOnly(true);

    newScene(true);

    _sceneLoader    = new SceneLoader(_openGLWindow);
    _sceneSaver     = new SceneSaver(_host, _openGLWindow);

    _fpsLabel       = new QLabel(this);
    _statusLabel    = new QLabel(this);
    _status         = new QLabel(this);

    _leftMouseIcon      = new QLabel(this);
    _leftMouseNotice    = new QLabel(this);

    _middleMouseIcon    = new QLabel(this);
    _middleMouseNotice  = new QLabel(this);

    progressBar     = new QProgressBar(this);
    progressBar->setMinimum(0);
    progressBar->setMaximum(100);
    progressBar->setMaximumWidth(200);
    progressBar->setMinimumWidth(200);
    progressBar->hide();

    _statusLabel->setText(" 状态: ");
    _status->setText("NULL");

    QPixmap leftMouse(":/icon/leftMouse.png");
    leftMouse = leftMouse.scaled({20,20});
    _leftMouseIcon->setPixmap(leftMouse);
    _leftMouseNotice->setText("旋转视图");

    QPixmap middleMouse(":/icon/middleMouse.png");
    middleMouse = middleMouse.scaled({20,20});
    _middleMouseIcon->setPixmap(middleMouse);
    _middleMouseNotice->setText("平移视图");

    statusBar()->addPermanentWidget(_fpsLabel);
    statusBar()->addWidget(_statusLabel);
    statusBar()->addWidget(_status);
    statusBar()->addWidget(_leftMouseIcon);
    statusBar()->addWidget(_leftMouseNotice);
    statusBar()->addWidget(_middleMouseIcon);
    statusBar()->addWidget(_middleMouseNotice);
    statusBar()->addWidget(progressBar);

    ui->itemsProperty->setWidget(nullptr);

    configSignal();
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MsnhMainWindow::doTimer);
    timer->start(30);

    _updateLogWindow    = new MsnhUpdateRecord(this);
    _optionsWindow      = new MsnhOptionsWindow(this,_host);

    PhysicalEngine::getInstance()->startEngine();
    PhysicalEngine::getInstance()->setDebugDrawer(_openGLWindow);
    _openGLWindow->ToggleDebugFlag(btIDebugDraw::DBG_DrawAabb);
    _openGLWindow->setDynamicWorld(PhysicalEngine::getInstance()->getWorld());

    //    Tool transformTool("Transform", QIcon (":/icon/transform.png"));
    //    ToolBox *tb = ui->widget;
    //    tb->addTool(transformTool);

    m_dockingPaneManager = new DockingPaneManager();
    m_dockingPaneManager->setClientWidget(wd);
    setCentralWidget(m_dockingPaneManager->widget());
    auto dockingWindow_1 = m_dockingPaneManager->createPane(QUuid::createUuid().toString(), "测试 1", createLabel("Hello World 1"), QSize(200, 200), DockingPaneManager::dockBottom);
    auto dockingWindow_2 = m_dockingPaneManager->createPane(QUuid::createUuid().toString(), "测试 2", createLabel("Hello World 2"), QSize(200, 200), DockingPaneManager::dockLeft, nullptr);


    loadProcesses();
    loadPlugins();

    _calibIntrinWindow = nullptr;
}

MsnhMainWindow::~MsnhMainWindow()
{
    delete _openGLWindow;
    MsnhSingleton<MsnhProcessFactory>::dispose();
    MsnhSingleton<MsnhPluginManager>::dispose();
    delete ui;
}

void MsnhMainWindow::doTimer()
{
    static int i = 0;
    i++;
    //_openGLWindow->getOpenGLScene()->getHost()->getModels()[0]->getChildModels()[0]->setRotationApi({0.1f*i,0,0});
    //qDebug()<<_openGLWindow->getOpenGLScene()->getHost()->getModels()[0]->getChildModels()[0]->getPositionApi();
    //    static float  i = 0;
    //    i = i+ 0.1;

    //        float x = 4*sin(i);
    //        float y = 4*cos(i);
    //    _host->getCamera()->setFocusCenter(_host->getGizmo()->getPosition());
    //    _host->getPointLights()[0]->setPos(Vector3F({x,x,y}));
    //    _host->getModels()[0]->setRotation(EulerF({0,0,i}));
}

void MsnhMainWindow::startProgressBar()
{
    progressBar->show();
    progressBar->setValue(0);
}

void MsnhMainWindow::stopProgressBar()
{
    progressBar->hide();
    progressBar->setValue(0);
}

void MsnhMainWindow::updateProgressBar(int val)
{
    progressBar->setValue(val);
    QApplication::processEvents();
}

void MsnhMainWindow::newScene(bool firstTime)
{
    if( _openGLWindow->getOpenGLScene() && !firstTime )
    {
        if(!askToSaveScene())
        {
            return;
        }
        _openGLWindow->resetOpenglScene();
    }

    _host = _openGLWindow->getOpenGLScene()->getHost();
    _openGLWindow->setAxisMode(EngineUtil::axisMode);
    _host->addDirectionalLight(new DirectionalLight(QVector3D(1, 1, 1), QVector3D(-2, -4, -3)));
    _host->addGridline(new Gridline(this));

    auto gridX = new Gridline(this);
    gridX->setColor(QVector3D(1.f,0,0));
    gridX->setXArgumentsApi(QVector3D(inf,0,1));
    gridX->setYArgumentsApi(QVector3D(0,0,1));
    gridX->setZArgumentsApi(QVector3D(0.01f,0.01f,1));
    gridX->setCanModify(false);
    _host->addGridline(gridX);

    auto gridY = new Gridline(this);
    gridY->setColor(QVector3D(0,1.f,0));
    gridY->setXArgumentsApi(QVector3D(0,0,1));
    gridY->setYArgumentsApi(QVector3D(inf,0,1));
    gridY->setZArgumentsApi(QVector3D(0.01f,0.01f,1));
    gridY->setCanModify(false);
    _host->addGridline(gridY);

    auto gridZ = new Gridline(this);
    gridZ->setColor(QVector3D(0,0,1.f));
    gridZ->setXArgumentsApi(QVector3D(0,0,1));
    gridZ->setYArgumentsApi(QVector3D(inf,0.01f,1));
    gridZ->setZArgumentsApi(QVector3D(0,0,1));
    gridZ->setCanModify(false);
    gridZ->setGridPlane(Gridline::GRID_ZX_PLANE);
    _host->addGridline(gridZ);

        auto line = new Curve3D(this);
        line->setIsCubeLine(true);
        //line->setLineWidth(1.f);
        line->addData(QVector3D(1.0f,2.0f,20.0f ));
        line->addData(QVector3D(3.0f,3.0f,30.0f));
        line->addData(QVector3D(4.0f,5.0f,40.0f));
        line->addData(QVector3D(5.0f,6.0f,70.0f));
        line->addData(QVector3D(6.0f,13.0f,10.0f));
        line->setColor(QVector3D(1,0,0));
        _host->addCurve(line);

    //    Model *model = ModelLoader::loadSphereModel();
    //    model->translateApi({0,0,100});


    //    Model *model1 = ModelLoader::loadCubeModel();

    //    _host->addModel(model);
    //    _host->addModel(model1);

    //    model->initRigidBody({1,1,1},1,false);
    //    model1->setScalingApi({50,50,0.1});
    //    model1->rotateApi({30,0,0});
    //    model1->initRigidBody({50,50,0.1},0,true);

    //    PhysicalEngine::getInstance()->addRigidBody(model);
    //    PhysicalEngine::getInstance()->addRigidBody(model1);

    //    Model *model1 = ModelLoader::loadCubeModel();
    //    Model *model2 = ModelLoader::loadCubeModel();
    //    Model *model3 = ModelLoader::loadCubeModel();
    //    Model *model4 = ModelLoader::loadCubeModel();

    //    model1->addChildModel(model);
    //    model2->addChildModel(model1);
    //    model3->addChildModel(model2);
    //    _host->addModel(model3);




    //    model1->translate({0,0,1});
    //    model2->translate({0,0,1});
    //    model3->translate({0,0,1});
    //    PointsCloudIO IO;
    //    PointsCloud *cloud =  IO.readFromFile("C:/Users/msnh/Desktop/opencv-4.3.0/opencv_contrib-4.3.0/modules/cnn_3dobj/samples/data/3Dmodel/horse.ply");
    //    cloud->getMarker()->scale({10,10,10});
    //    cloud->setPureColor(false);
    //    _host->addPointsCloud(cloud);

    ui->sceneTree->setScene(_host);

    //    model3->addChildModel(model4);
}

void MsnhMainWindow::openScene()
{
    if (!askToSaveScene())
    {
        return;
    }

    QString filePath = QFileDialog::getOpenFileName(this, "打开工程", "", "Msnh Robot Project(*.msnhrob)");
    if (filePath == "")
    {
        return;
    }

    Scene* scene = _sceneLoader->loadFromFile(filePath);

    if (_sceneLoader->hasErrorLog())
    {
        QString log = _sceneLoader->errorLog();
        QMessageBox::critical(this, "错误", log);
        if (logLevel >= LOG_LEVEL_ERROR)
        {
            dout << log;
        }
    }

    if (scene)
    {
        _openGLWindow->resetOpenglScene(scene);
        _host = _openGLWindow->getOpenGLScene()->getHost();
        ui->sceneTree->setScene(_host);
        _savePath = filePath;
    }
}

void MsnhMainWindow::openScenePath(QString filePath)
{
    qDebug()<<"打开工程文件:"+filePath;
    if (!askToSaveScene())
    {
        return;
    }

    Scene* scene = _sceneLoader->loadFromFile(filePath);

    if (_sceneLoader->hasErrorLog())
    {
        QString log = _sceneLoader->errorLog();
        QMessageBox::critical(this, "错误", log);
        if (logLevel >= LOG_LEVEL_ERROR)
        {
            dout << log;
        }
    }

    if (scene)
    {
        _openGLWindow->resetOpenglScene(scene);
        _host = _openGLWindow->getOpenGLScene()->getHost();
        ui->sceneTree->setScene(_host);
        _savePath = filePath;
    }
}

void MsnhMainWindow::saveScene()
{
    if(!_host)
    {
        return;
    }

    if(_savePath!="")
    {
        _sceneSaver->setScene(_host);
        _sceneSaver->saveToFile(_savePath);
    }
    else
    {
        saveAsScene();
    }
}

void MsnhMainWindow::saveAsScene()
{
    if(!_host)
    {
        return;
    }

    QString filepath = QFileDialog::getSaveFileName(this, "保存工程", "", "Msnh Robot Project(*.msnhrob)");

    if(filepath == "")
    {
        return;
    }

    _savePath = filepath;

    _sceneSaver->setScene(_host);
    _sceneSaver->saveToFile(filepath);

    if(_sceneSaver->hasErrorLog())
    {
        QString log = _sceneSaver->errorLog();
        QMessageBox::critical(this,"错误",log);
        if(logLevel >= LOG_LEVEL_ERROR)
        {
            dout << log;
        }
    }
}

bool MsnhMainWindow::askToSaveScene()
{
    int answer = QMessageBox::question(this,
                                       "未保存的工程",
                                       "是否保存当前工程？",
                                       QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

    if (answer == QMessageBox::Cancel)
        return false;
    if (answer == QMessageBox::Yes)
        saveScene();
    return true;
}

void MsnhMainWindow::importModel()
{
    QString filePath = QFileDialog::getOpenFileName(this, "导入模型", "", "All Files (*)");
    if (filePath == "")
    {
        return;
    }

    ModelLoader loader;
    Model* model = loader.loadModelFromFile(filePath);

    if (loader.hasErrorLog())
    {
        QString log = loader.errorLog();
        QMessageBox::critical(this, "错误", log);
        if (logLevel >= LOG_LEVEL_ERROR)
            dout << log;
    }

    if (_host && model)
    {
        _host->addModel(model);
    }
}

void MsnhMainWindow::exportModel()
{
    if (!_host)
    {
        return;
    }

    if (AbstractEntity::getSelectedObject() == nullptr || (!AbstractEntity::getSelectedObject()->isMesh() && !AbstractEntity::getSelectedObject()->isModel()))
    {
        QMessageBox::critical(this, "错误", "请选择一个模型或者网格来导出.");
        return;
    }

    QString filter = "X Files (*.x);;";
    filter += "Step Files (*.stp);;";
    filter += "Wavefront OBJ format (*.obj);;";
    filter += "Stereolithography (*.stl);;";
    filter += "Stanford Polygon Library (*.ply);;";
    filter += "Extensible 3D (*.x3d);;";

    QString filePath = QFileDialog::getSaveFileName(this, "导出模型", "", filter);
    if (filePath == "")
    {
        return;
    }

    ModelExporter exporter;
    if (Model* model = qobject_cast<Model*>(AbstractEntity::getSelectedObject()))
    {
        exporter.saveToFile(model, filePath);
    }
    else if (Mesh* mesh = qobject_cast<Mesh*>(AbstractEntity::getSelectedObject()))
    {
        exporter.saveToFile(mesh, filePath);
    }

    if (exporter.hasErrorLog())
    {
        QString log = exporter.errorLog();
        QMessageBox::critical(this, "错误", log);
        if (logLevel >= LOG_LEVEL_ERROR)
        {
            dout << log;
        }
    }
}

void MsnhMainWindow::addMaterial()
{
    if(Mesh::getSelectedObject() && Mesh::getSelectedObject()->isMesh())
    {
        qobject_cast<Mesh*>(Mesh::getSelectedObject())->setMaterial(new Material());//setMaterial会执行setParent
    }
    else
    {
        QMessageBox::critical(this, "错误", "请选择一个网格后再进行操作");
    }
}


void MsnhMainWindow::addGridLine()
{
    if(_host)
    {
        _host->addGridline(new Gridline(this));
    }
}

void MsnhMainWindow::addCube()
{
    if(_host)
    {
        _host->addModel(ModelLoader::loadCubeModel());
    }
}

void MsnhMainWindow::addCylinder()
{
    if(_host)
    {
        _host->addModel(ModelLoader::loadCylinderModel());
    }
}

void MsnhMainWindow::addPlane()
{
    if(_host)
    {
        _host->addModel(ModelLoader::loadPlaneModel());
    }
}

void MsnhMainWindow::addSphere()
{
    if(_host)
    {
        _host->addModel(ModelLoader::loadSphereModel());
    }
}

void MsnhMainWindow::addCone()
{
    if(_host)
    {
        _host->addModel(ModelLoader::loadConeModel());
    }
}

void MsnhMainWindow::addTorus()
{
    if(_host)
    {
        _host->addModel(ModelLoader::loadTorusModel());
    }
}

void MsnhMainWindow::addPointClouds()
{
    if(_host)
    {
        _host->addPointsCloud(new PointsCloud());
    }
}

void MsnhMainWindow::addCurve()
{
    if(_host)
    {
        _host->addCurve(new Curve3D());
    }
}

void MsnhMainWindow::addPointLight()
{
    if(_host)
    {
        if(_host->getPointLights().size()>=8)
        {
            QMessageBox::critical(this, "错误", "最多支持8个点光源.");
            return;
        }
        _host->addLight(new PointLight(this));
    }
}

void MsnhMainWindow::addAmbientLight()
{
    if(_host)
    {
        if(_host->getAmbientLights().size()>=8)
        {
            QMessageBox::critical(this, "错误", "最多支持8个环境光源.");
            return;
        }
        _host->addLight(new AmbientLight(this));
    }
}

void MsnhMainWindow::addDirectionalLight()
{
    if(_host)
    {
        if(_host->getDirectionalLights().size()>=8)
        {
            QMessageBox::critical(this, "错误", "最多支持8个平行光源.");
            return;
        }
        _host->addLight(new DirectionalLight(this));
    }
}

void MsnhMainWindow::addSpotLight()
{
    if(_host)
    {
        if(_host->getSpotLights().size()>=8)
        {
            QMessageBox::critical(this, "错误", "最多支持8个聚光灯光源.");
            return;
        }
        _host->addLight(new SpotLight(this));
    }
}

void MsnhMainWindow::addRobot()
{
    QString filePath = QFileDialog::getOpenFileName(this, "导入URDF模型", "", "urdf files(*.urdf)");
    if (filePath == "")
    {
        return;
    }

    try
    {
        Robot* robot = new Robot(filePath);
        _host->addRobot( robot );
    }
    catch(Msnhnet::Exception ex)
    {
        QMessageBox::warning(nullptr,"错误",QString::fromStdString(ex.what()));
    }
}

void MsnhMainWindow::onCopy()
{
    if (ui->sceneTree->hasFocus())
    {
        QVariant item = ui->sceneTree->currentItem()->data(0, Qt::UserRole);

        //相机、材质、场景等不能被拷贝
        if (item.canConvert<Camera*>() || item.canConvert<Material*>() || item.canConvert<Scene*>())
        {
            _copyedItem.clear(); // Copy is not allowed
        }
        else
        {
            if(item.value<QObject*>()->parent()->inherits("Scene"))
            {
                _copyedItem = item;
            }
        }
    }
    else if (AbstractEntity::getSelectedObject())
    {
        QVariant item = ui->sceneTree->currentItem()->data(0, Qt::UserRole);

        if(item.value<QObject*>()->parent()->inherits("Scene"))
        {
            _copyedItem = item;
        }
    }
    else
    {
        _copyedItem.clear();
    }

    if (logLevel >= LOG_LEVEL_INFO)
    {
        if (_copyedItem.isValid())
            dout << _copyedItem.value<QObject*>()->objectName() << "is copyed";
        else
            dout << "Nothing is copyed";
    }
}

void MsnhMainWindow::onPaste()
{
    if (!_copyedItem.isValid()) return;

    if (_copyedItem.canConvert<AmbientLight*>())
    {
        if (_host->getAmbientLights().size() >= 8)
        {
            QMessageBox::critical(this, "错误", "最多支持8个环境光源.");
            return;
        }
        AmbientLight* light = _copyedItem.value<AmbientLight*>();
        AmbientLight* newLight = new AmbientLight(*light);
        newLight->setParent(light->parent());
    }
    else if (_copyedItem.canConvert<DirectionalLight*>())
    {
        if (_host->getDirectionalLights().size() >= 8)
        {
            QMessageBox::critical(this, "错误", "最多支持8个平行光源.");
            return;
        }
        DirectionalLight* light = _copyedItem.value<DirectionalLight*>();
        DirectionalLight* newLight = new DirectionalLight(*light);
        newLight->setParent(light->parent());
    }
    else if (_copyedItem.canConvert<PointLight*>())
    {
        if (_host->getPointLights().size() >= 8)
        {
            QMessageBox::critical(this, "错误", "最多支持8个点光源.");
            return;
        }
        PointLight* light = _copyedItem.value<PointLight*>();
        PointLight* newLight = new PointLight(*light);
        newLight->setParent(light->parent());
    }
    else if (_copyedItem.canConvert<SpotLight*>())
    {
        if (_host->getSpotLights().size() >= 8)
        {
            QMessageBox::critical(this, "错误", "最多支持8个聚光灯光源.");
            return;
        }
        SpotLight* light = _copyedItem.value<SpotLight*>();
        SpotLight* newLight = new SpotLight(*light);
        newLight->setParent(light->parent());
    }
    else if (_copyedItem.canConvert<Gridline*>())
    {
        Gridline* gridline = _copyedItem.value<Gridline*>();
        if(gridline->getCanModify())
        {
            Gridline* newGridline = new Gridline(*gridline);
            newGridline->setParent(gridline->parent());
        }
    }
    else if (_copyedItem.canConvert<Curve3D*>())
    {
        Curve3D* curve = _copyedItem.value<Curve3D*>();
        Curve3D* newCurve = new Curve3D(*curve);
        newCurve->setParent(curve->parent());
    }
    else if (_copyedItem.canConvert<PointsCloud*>())
    {
        PointsCloud* pointsCloud = _copyedItem.value<PointsCloud*>();
        PointsCloud* newPointsCloud = new PointsCloud(*pointsCloud);

        newPointsCloud->setParent(pointsCloud->parent());
    }
    else if (_copyedItem.canConvert<Model*>())
    {
        Model* model = _copyedItem.value<Model*>();
        Model* newModel = new Model(*model);
        newModel->setParent(model->parent());
    }
    else if (_copyedItem.canConvert<Mesh*>())
    {
        Mesh* mesh = _copyedItem.value<Mesh*>();
        Mesh* newMesh = new Mesh(*mesh);
        newMesh->setParent(mesh->parent()); //所有对象的parent都是scene
    }
    else if (_copyedItem.canConvert<Robot*>())
    {
        Robot* robot = _copyedItem.value<Robot*>();
        Robot* newRobot = new Robot(*robot);
        newRobot->setParent(robot->parent()); //所有对象的parent都是scene
    }
    else
    {
        if (logLevel >= LOG_LEVEL_ERROR)
            dout << "Failed to paste: Unknown type";
    }
}

void MsnhMainWindow::onDelete()
{
    if (!_host || !ui->sceneTree->currentItem())
    {
        return;
    }
    //此处获取的是每个BaseItem的host,选择时,会把被选择对象的host赋值给currentItem;
    QVariant item = ui->sceneTree->currentItem()->data(0, Qt::UserRole);

    //获取选中物体的names
    //qDebug()<<ui->sceneTree->currentItem()->text(0);

    //scene固有的对象如网格组,其host是scene,不能被删除
    if(item.value<QObject*>() == _host) //设置的地方 BaseItem类初始化
    {
        QMessageBox::warning(this,"警告","无法删除该对象");
        return;
    }

    if (item.value<QObject*>() == _host->getCamera())
    {
        QMessageBox::warning(this, "警告", "摄像机不能被删除.");
        if (logLevel >= LOG_LEVEL_WARNING)
        {
            dout << "警告: 摄像机不能被删除";
        }
        return;
    }

    if (item.canConvert<Gridline*>())
    {
        Gridline *grid = item.value<Gridline*>();
        if(!grid->getCanModify())
        {
            QMessageBox::warning(this, "警告", "此网格不可被删除.");
            if (logLevel >= LOG_LEVEL_WARNING)
            {
                dout << "警告: 此网格不可被删除";
            }
            return;
        }
    }

    if (item.canConvert<Curve3D*>())
    {
        Curve3D *curve = item.value<Curve3D*>();
        if(!curve->getDeleteable())
        {
            QMessageBox::warning(this, "警告", "此曲线不可被删除.");
            if (logLevel >= LOG_LEVEL_WARNING)
            {
                dout << "警告: 此曲线不可被删除";
            }
            return;
        }
    }

    if( item.canConvert<AbstractEntity*>())
    {
        AbstractEntity *entity = item.value<AbstractEntity*>();

        if(!entity->getDeleteable())
        {
            QMessageBox::warning(this, "警告", "此对象不可被删除.");
            if (logLevel >= LOG_LEVEL_WARNING)
            {
                dout << "警告: 此对象不可被删除";
            }
            return;
        }
    }

    ui->sceneTree->setCurrentItem(0);

    if (_copyedItem == item)
    {
        _copyedItem.clear(); //清除正在复制物体
    }

    //    if( item.canConvert<Model*>())
    //    {
    //        Model *model = item.value<Model*>();
    //        model->deleting();
    //    }

    delete item.value<QObject*>(); //delete的时候会触发childEvent的event->removed事件
}

void MsnhMainWindow::itemSelected(QVariant item)
{
    delete ui->itemsProperty->takeWidget();

    if(item.canConvert<Gridline*>())
    {
        ui->itemsProperty->setWidget(new MsnhGridLineProperty(item.value<Gridline*>(),this));
    }
    else if(item.canConvert<Curve3D*>())
    {
        ui->itemsProperty->setWidget(new MsnhCurveProperty(item.value<Curve3D*>(),this));
    }
    else if(item.canConvert<Transform*>())
    {
        ui->itemsProperty->setWidget(new MsnhTransformProperty(item.value<Transform*>(),this));
    }
    else if(item.canConvert<Camera*>())
    {
        ui->itemsProperty->setWidget(new MsnhCameraProperty(item.value<Camera*>(),this));
    }
    else if(item.canConvert<AmbientLight*>())
    {
        ui->itemsProperty->setWidget(new MsnhAmbientLightProperty(item.value<AmbientLight*>(),this));
    }
    else if(item.canConvert<DirectionalLight*>())
    {
        ui->itemsProperty->setWidget(new MsnhDirectionalLightProperty(item.value<DirectionalLight*>(),this));
    }
    else if(item.canConvert<Material*>())
    {
        ui->itemsProperty->setWidget(new MsnhMaterialProperty(item.value<Material*>(),this));
    }
    else if(item.canConvert<Mesh*>())
    {
        ui->itemsProperty->setWidget(new MsnhMeshProperty(item.value<Mesh*>(),this));
    }
    else if(item.canConvert<Model*>())
    {
        ui->itemsProperty->setWidget(new MsnhModelProperty(item.value<Model*>(),this));
    }
    else if(item.canConvert<PointLight*>())
    {
        ui->itemsProperty->setWidget(new MsnhPointLightProperty(item.value<PointLight*>(),this));
    }
    else if(item.canConvert<SpotLight*>())
    {
        ui->itemsProperty->setWidget(new MsnhSpotLightProperty(item.value<SpotLight*>(),this));
    }
    else if(item.canConvert<PointsCloud*>())
    {
        ui->itemsProperty->setWidget(new MsnhPointsCloudProperty(item.value<PointsCloud*>(),this));
    }
    else if(item.canConvert<Robot*>())
    {
        ui->itemsProperty->setWidget(new MsnhRobotProperty(item.value<Robot*>(),this));
    }
}

void MsnhMainWindow::itemsDeselected(QVariant)
{
    delete ui->itemsProperty->takeWidget();
    ui->itemsProperty->setWidget(nullptr);
}


void MsnhMainWindow::configSignal()
{
    connect(_openGLWindow, &OpenGLWindow::fpsChanged,this,&MsnhMainWindow::fpsChanged);
    connect(_openGLWindow, &OpenGLWindow::openScene, this, &MsnhMainWindow::openScenePath);
    connect(_openGLWindow->getOpenGLScene()->getHost()->getCamera(), &Camera::cameraReset, [this]()->void
    {
                ui->actionFPSCam->setChecked(false);
                ui->actionOrbitCam->setChecked(true);
            });

    connect(_sceneLoader, &SceneLoader::startProgressBarSig,this,&MsnhMainWindow::startProgressBar);
    connect(_sceneLoader, &SceneLoader::updateProgressBarSig,this,&MsnhMainWindow::updateProgressBar);
    connect(_sceneLoader, &SceneLoader::stopProgressBarSig,this,&MsnhMainWindow::stopProgressBar);

    connect(_sceneSaver, &SceneSaver::startProgressBarSig,this,&MsnhMainWindow::startProgressBar);
    connect(_sceneSaver, &SceneSaver::updateProgressBarSig,this,&MsnhMainWindow::updateProgressBar);
    connect(_sceneSaver, &SceneSaver::stopProgressBarSig,this,&MsnhMainWindow::stopProgressBar);

    //属性窗口
    connect(ui->sceneTree, &SceneTreeWidget::itemSelected, this, &MsnhMainWindow::itemSelected);
    connect(ui->sceneTree, &SceneTreeWidget::itemDeselected, this, &MsnhMainWindow::itemsDeselected);
}

void MsnhMainWindow::fpsChanged(int fps)
{
    _fpsLabel->setText("FPS: " + QString::number(fps));
}

void MsnhMainWindow::on_actionSelectTransGizmo_triggered()
{
    if(!ui->actionSelectTransGizmo->isChecked())
    {
        ui->actionSelectTransGizmo->setChecked(true);
    }

    _host->getGizmo()->setTransformMode(TransformGizmo::GIZMO_TRANSLATE);
    ui->actionSelectRotGizmo->setChecked(false);
    ui->actionSelectScaleGizmo->setChecked(false);

    _openGLWindow->setToolActivated(0);
}

void MsnhMainWindow::on_actionSelectRotGizmo_triggered()
{
    if(!ui->actionSelectRotGizmo->isChecked())
    {
        ui->actionSelectRotGizmo->setChecked(true);
    }

    _host->getGizmo()->setTransformMode(TransformGizmo::GIZMO_ROTATE);
    ui->actionSelectTransGizmo->setChecked(false);
    ui->actionSelectScaleGizmo->setChecked(false);

    _openGLWindow->setToolActivated(1);
}

void MsnhMainWindow::on_actionSelectScaleGizmo_triggered()
{
    if(!ui->actionSelectScaleGizmo->isChecked())
    {
        ui->actionSelectScaleGizmo->setChecked(true);
    }
    _host->getGizmo()->setTransformMode(TransformGizmo::GIZMO_SCALE);
    ui->actionSelectTransGizmo->setChecked(false);
    ui->actionSelectRotGizmo->setChecked(false);

    _openGLWindow->setToolActivated(2);
}

void MsnhMainWindow::on_actionGizmoOnTop_triggered()
{
    _host->getGizmo()->setAlwaysOnTop(ui->actionGizmoOnTop->isChecked());
}

void MsnhMainWindow::on_actionOrbitCam_triggered()
{
    if(ui->actionOrbitCam->isChecked())
    {
        ui->actionFPSCam->setChecked(false);
        _host->getCamera()->setOrbitCam(true);
    }
    else
    {
        ui->actionFPSCam->setChecked(true);
    }
}

void MsnhMainWindow::on_actionFPSCam_triggered()
{
    if(ui->actionFPSCam->isChecked())
    {
        ui->actionOrbitCam->setChecked(false);
        _host->getCamera()->setOrbitCam(false);
    }
    else
    {
        ui->actionOrbitCam->setChecked(true);
    }
}

void MsnhMainWindow::on_actionFullScreen_triggered()
{
    if(ui->actionFullScreen->isChecked())
    {
        this->showFullScreen();
    }
    else
    {
        this->showNormal();
    }
}

void MsnhMainWindow::on_actionQuit_triggered()
{
    QApplication::exit(0);
}

void MsnhMainWindow::on_actionNewProj_triggered()
{
    newScene(false);
}

void MsnhMainWindow::on_actionSaveProj_triggered()
{
    saveScene();
}

void MsnhMainWindow::on_actionSaveAsProj_triggered()
{
    saveAsScene();
}

void MsnhMainWindow::on_actionOpenScene_triggered()
{
    openScene();
}

void MsnhMainWindow::on_actionImportModel_triggered()
{
    importModel();
}

void MsnhMainWindow::on_actionExportModel_triggered()
{
    exportModel();
}

void MsnhMainWindow::on_actionAddPointLight_triggered()
{
    addPointLight();
}

void MsnhMainWindow::on_actionAddAmbientLight_triggered()
{
    addAmbientLight();
}

void MsnhMainWindow::on_actionAddDirectionalLight_triggered()
{
    addDirectionalLight();
}

void MsnhMainWindow::on_actionSpotLight_triggered()
{
    addSpotLight();
}

void MsnhMainWindow::on_actionAddBox_triggered()
{
    addCube();
}

void MsnhMainWindow::on_actionAddCylinder_triggered()
{
    addCylinder();
}

void MsnhMainWindow::on_actionAddPlane_triggered()
{
    addPlane();
}

void MsnhMainWindow::on_actionSphere_triggered()
{
    addSphere();
}

void MsnhMainWindow::on_actionAddCone_triggered()
{
    addCone();
}

void MsnhMainWindow::on_actionAddTorus_triggered()
{
    addTorus();
}

void MsnhMainWindow::on_actionAddGridLine_triggered()
{
    addGridLine();
}

void MsnhMainWindow::on_actionCopy_triggered()
{
    onCopy();
}

void MsnhMainWindow::on_actionPaste_triggered()
{
    onPaste();
}

void MsnhMainWindow::on_actionDelete_triggered()
{
    onDelete();
}

void MsnhMainWindow::on_actionAboutQt_triggered()
{
    QApplication::aboutQt();
}

void MsnhMainWindow::on_actionAbout_triggered()
{
    QString info = "Msnh Robot Engine\n\n";
    info += "介绍: 一款基于Qt的机器人综合仿真部署引擎\n";
    info += "当前版本: " + QString(APP_VERSION) + "\n";
    info += "作者: 穆士凝魂\n";
    info += "Email: zhongjun_msnh@163.com";
    QMessageBox::about(this, "关于", info);

    PhysicalEngine::getInstance()->startEngine();
}

void MsnhMainWindow::on_actionAboutSys_triggered()
{
    QString info = "";
    info += "显卡厂商: " + _openGLWindow->getVendor() + "\n\n";
    info += "显卡信息: " + _openGLWindow->getRendererName() + "\n\n";
    info += "OpenGL版本: " + _openGLWindow->getOpenGLVersion() + "\n\n";
    info += "GLSL版本: " + _openGLWindow->getShadingLanguageVersion();
    QMessageBox::about(this, "系统信息", info);
    PhysicalEngine::getInstance()->stopEngine();
    //EngineUtil::app->setFont(QFont("Microsoft Yahei",20));
}

void MsnhMainWindow::on_actionCreateMaterial_triggered()
{
    addMaterial();
}

Model *MsnhMainWindow::loadURDF(const QString &path)
{
    std::shared_ptr<Msnhnet::URDFModel> model =  Msnhnet::URDF::parseFromUrdfFile(path.toUtf8().toStdString());
    std::shared_ptr<Msnhnet::URDFLinkTree> linkTree = Msnhnet::URDF::getLinkTree(model->rootLink);
    return loadModel(linkTree,model->rootPath);
}

Model *MsnhMainWindow::loadModel(const std::shared_ptr<Msnhnet::URDFLinkTree> &link, const std::string &rootPath)
{
    Model* md = new Model();
    Model* cube = new Model();

    cube->setObjectName(QString::fromStdString(link->joint.value()->name));

    Msnhnet::Frame frame = link->joint.value()->parentToJointTransform;

    ModelLoader loader;
    cube->addChildMesh(loader.loadMeshFromFile( ":/shapes/TransX.obj" ));
    cube->addChildMesh(loader.loadMeshFromFile( ":/shapes/TransY.obj" ));
    cube->addChildMesh(loader.loadMeshFromFile( ":/shapes/TransZ.obj" ));

    //cube->getChildMeshes()[0]->scaleApi({0.05f,0.05f,0.05f});
    //cube->getChildMeshes()[1]->scaleApi({0.05f,0.05f,0.05f});
    //cube->getChildMeshes()[2]->scaleApi({0.05f,0.05f,0.05f});

    cube->translateApi({(float)frame.trans[0],(float)frame.trans[1],(float)frame.trans[2]});
    Msnhnet::EulerDS euler = Msnhnet::GeometryS::rotMat2Euler(frame.rotMat,Msnhnet::ROT_ZYX);
    cube->rotateApi({(float)(euler[0]*MSNH_RAD_2_DEG),(float)(euler[1]*MSNH_RAD_2_DEG),(float)(euler[2]*MSNH_RAD_2_DEG)});
    md->addChildModel(cube);

    for (size_t i = 0; i < link->visuals.size(); ++i)
    {
        std::string path = ((Msnhnet::URDFMesh*)link->visuals[i]->geometry->get())->filename;

        Msnhnet::ExString::replace(path,"package:/","");

        path = rootPath + path;

        ModelLoader loader;
        Model* mode = loader.loadModelFromFile(QString::fromStdString(path));
        md->getChildModels()[0]->addChildModel(mode);
    }

    if(link->nextLink.size()>0)
    {
        for (size_t i = 0; i < link->nextLink.size(); ++i)
        {
            md->getChildModels()[0]->addChildModel(loadModel(link->nextLink[i],rootPath));
        }
    }
    return md;
}

void MsnhMainWindow::on_actionUpdateLog_triggered()
{
    _updateLogWindow->show();
}


void MsnhMainWindow::on_actionAddPointClouds_triggered()
{
    addPointClouds();
}


void MsnhMainWindow::on_actionOptions_triggered()
{
    _optionsWindow->show();
}


void MsnhMainWindow::on_actionImportURDF_triggered()
{
    addRobot();
}

void MsnhMainWindow::loadProcesses()
{
    MsnhProcessFactory* processFactory = MsnhSingleton<MsnhProcessFactory>::getInstance();
    processFactory->registerProcess("testBase", new MsnhTestBase());
    processFactory->registerProcess("testImage", new MsnhTestImage());
    processFactory->registerProcess("opencvCamera", new MsnhOpenCVCamera());

    MsnhProcess* basepPocess = processFactory->getProcInstance("testBase");

    MsnhProtocal::Ping ping(MsnhProtoType::IS_DATA);
    ping.setDataError(222);
    basepPocess->processInputData(&ping);

    delete basepPocess;

}

void MsnhMainWindow::loadPlugins()
{
    MsnhProcessFactory* processFactory = MsnhSingleton<MsnhProcessFactory>::getInstance();
    MsnhPluginManager* pluginManager = MsnhSingleton<MsnhPluginManager>::getInstance();
    pluginManager->loadPlugins("./plugins",processFactory);

    //MsnhProcess* rgb2gray = processFactory->getProcInstance("MsnhRGB2Gray");
    //    if(rgb2gray)
    //    {
    //        MsnhProtocal::MsnhImage image;
    //        image.mat = cv::imread("C:/Users/msnh-pc/Desktop/a.jpg");
    //        rgb2gray->processInputData(&image);
    //        image.mat = cv::imread("C:/Users/msnh-pc/Desktop/b.png");
    //        rgb2gray->processInputData(&image);
    //        MsnhProtocal::MsnhImage* IMG = (MsnhProtocal::MsnhImage*)rgb2gray->getResultData();
    //        cv::imshow("fuck",IMG->mat);
    //        cv::waitKey();
    //    }
}


void MsnhMainWindow::on_actionAddCurve_triggered()
{
    addCurve();
}


void MsnhMainWindow::on_actionCamIntrinCalibrate_triggered()
{
    _calibIntrinWindow = new MsnhCalibrateIntrinWindow(this);
    _calibIntrinWindow->show();
}


void MsnhMainWindow::on_actionCamStereoCalibrate_triggered()
{
    _calibStereoWindow = new MsnhCalibrateStereoWindow(this);
    _calibStereoWindow->show();
}

QPlainTextEdit *MsnhMainWindow::createLabel(QString string)
{
    auto label = new QPlainTextEdit(string);
    return(label);
}

