﻿#include <Gui/MsnhOptionsWindow.h>
#include <Gui/OptionKits/MsnhOpenglOption.h>
#include <Gui/OptionKits/MsnhRobotOption.h>
#include "ui_MsnhOptionsWindow.h"
#include <QIcon>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif
MsnhOptionsWindow::MsnhOptionsWindow(QWidget *parent, Scene *scene) :
    QDialog(parent),
    ui(new Ui::MsnhOptionsWindow),
    _scene(scene)
{
    ui->setupUi(this);

    ui->listWidget->setIconSize(QSize(26,26));
    ui->listWidget->setResizeMode(QListWidget::Adjust);
    QIcon icon(QPixmap(":/icon/opengl.png"));
    ui->listWidget->addItem(new QListWidgetItem(icon,"OpenGL设置"));

    QIcon icon1(QPixmap(":/icon/robot.png"));
    ui->listWidget->addItem(new QListWidgetItem(icon1,"机器人设置"));

    ui->listWidget->setCurrentRow(0);

    ui->scrollArea->setWidget(new MsnhOpenglOption(this));
}

MsnhOptionsWindow::~MsnhOptionsWindow()
{
    delete ui;
}

void MsnhOptionsWindow::on_listWidget_currentRowChanged(int currentRow)
{
    switch (currentRow)
    {
    case 0:
        ui->scrollArea->setWidget(new MsnhOpenglOption(this));
        break;
    case 1:
        ui->scrollArea->setWidget(new MsnhRobotOption(this,_scene));
        break;
    }
}
