﻿#include <Gui/MsnhSceneTreeWidget.h>
#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif
SceneTreeWidget::SceneTreeWidget( QWidget * parent ) : QTreeWidget( parent )
{
    setAnimated( true );
    setScene( 0 );
    connect( this, SIGNAL( currentItemChanged( QTreeWidgetItem *, QTreeWidgetItem * ) ),
        this, SLOT( currentItemChanged( QTreeWidgetItem *, QTreeWidgetItem * ) ) );
}


SceneTreeWidget::SceneTreeWidget( Scene* scene, QWidget* parent ) : QTreeWidget( parent )
{
    setAnimated( true );
    setScene( scene );
    connect( this, SIGNAL( currentItemChanged( QTreeWidgetItem *, QTreeWidgetItem * ) ),
        this, SLOT( currentItemChanged( QTreeWidgetItem *, QTreeWidgetItem * ) ) );
}


void SceneTreeWidget::setScene( Scene * scene )
{
    _host = scene;
    if ( _host )
    {
        //connect( _host, SIGNAL( gridlineAdded( Gridline* ) ), this, SLOT( gridlineAdded( Gridline* ) ) );
        //connect( _host, SIGNAL( lightAdded( AbstractLight* ) ), this, SLOT( lightAdded( AbstractLight* ) ) );

        connect( _host, &Scene::curveAdded ,this, &SceneTreeWidget::curveAdded);
        connect( _host, &Scene::tfAdded ,this, &SceneTreeWidget::tfAdded);
        connect( _host, &Scene::tfGroupAdded ,this, &SceneTreeWidget::tfGroupAdded);
        connect( _host, &Scene::modelAdded ,this, &SceneTreeWidget::modelAdded);
        connect( _host, &Scene::pointsCloudAdded , this, &SceneTreeWidget::pointsCloudAdded);
        connect( _host, &Scene::robotAdded, this, &SceneTreeWidget::robotAdded);
        connect( _host, &Scene::destroyed  ,this, &SceneTreeWidget::hostDestroyed);
    }
    reload();
}


void SceneTreeWidget::reload()
{
    this->clear();
    this->setColumnCount( 1 );
    this->setHeaderLabel( "物体对象" );
    this->setCurrentItem( 0 );
    if ( !_host )
        return;

    new CameraItem( _host->getCamera(), invisibleRootItem() );
    new GridlineItemGroup(_host, invisibleRootItem() );
    new LightItemGroup(_host, invisibleRootItem());

    for ( int i = 0; i < _host->getCurves().size(); i++ )
    {
        new Curve3DItem( _host->getCurves()[i], invisibleRootItem() );
    }

    for ( int i = 0; i < _host->getTfs().size(); i++ )
    {
        new TransformItem( _host->getTfs()[i], invisibleRootItem() );
    }

    for ( int i = 0; i < _host->getTfGroups().size(); i++ )
    {
        new TransformGroupItem( _host->getTfGroups()[i], invisibleRootItem() );
    }

    for ( int i = 0; i < _host->getModels().size(); i++ )
    {
        new ModelItem( _host->getModels()[i], invisibleRootItem() );
    }

    for ( int i = 0; i < _host->getPointsClouds().size(); i++ )
    {
        new PointsCloudItem( _host->getPointsClouds()[i], invisibleRootItem() );
    }

    for ( int i = 0; i < _host->getRobots().size(); i++ )
    {
        new RobotItem( _host->getRobots()[i], invisibleRootItem() );
    }

}


void SceneTreeWidget::keyPressEvent( QKeyEvent * e )
{
    if ( !currentItem() )
        return;
    if ( e->key() == Qt::Key_Delete) //注意,此处删除，会影响到mainwindow的，此处已被mainwindow的delete覆盖
    {
        QVariant item = currentItem()->data( 0, Qt::UserRole );
        itemDeselected( item );
        if ( item.canConvert<Camera*>() )
        {
            delete item.value<Camera*>();
        }
        else if ( item.canConvert<Gridline*>() )
        {
            delete item.value<Gridline*>();
        }
        else if ( item.canConvert<Curve3D*>() )
        {
            delete item.value<Curve3D*>();
        }
        else if ( item.canConvert<Transform*>() )
        {
            delete item.value<Transform*>();
        }
        else if ( item.canConvert<TransformGroup*>() )
        {
            delete item.value<TransformGroup*>();
        }
        else if ( item.canConvert<PointsCloud*>() )
        {
            delete item.value<PointsCloud*>();
        }
        else if ( item.canConvert<AbstractLight*>() )
        {
            delete item.value<AbstractLight*>();
        }
        else if ( item.canConvert<Model*>() )
        {
            auto tmp = item.value<Model*>();
            delete tmp;
            tmp = nullptr;
        }
        else if( item.canConvert<Robot*>())
        {
            delete item.value<Robot*>();
        }
        else if ( item.canConvert<Mesh*>() )
        {
            delete item.value<Mesh*>();
        }
        else if ( item.canConvert<Material*>() )
        {
            delete item.value<Material*>(); //最好不要删
        }
    }
}


void SceneTreeWidget::currentItemChanged( QTreeWidgetItem * current, QTreeWidgetItem * previous )
{
    if ( previous )
        itemDeselected( previous->data( 0, Qt::UserRole ) );
    if ( current )
    {
        itemSelected( current->data( 0, Qt::UserRole ) );
        //根据host进行选择,(虚对象, 需要继承AbstractEntity)
        static_cast<BaseItem*>(currentItem() )->selectHost();//界面选择后, 后台界面对应的物体同样需要被选中
        _host->getGizmo()->bindTo( AbstractEntity::getSelectedObject()); //OpenGLWindow::mouseReleaseEvent
    }
}


void SceneTreeWidget::gridlineAdded( Gridline * gridline )
{
    for ( int i = 0; i < topLevelItemCount(); i++ )
    {
        if ( static_cast<BaseItem*>(topLevelItem( i ) )->priority() < GRIDLINEGROUP_PRIORITY )
        {
            insertTopLevelItem( i, new GridlineItem( gridline, 0 ) );
            return;
        }
    }
    addTopLevelItem( new GridlineItem( gridline, 0 ) );
}

void SceneTreeWidget::curveAdded(Curve3D *curve)
{
    for ( int i = 0; i < topLevelItemCount(); i++ )
    {
        if ( static_cast<BaseItem*>(topLevelItem( i ) )->priority() < CURVE3D_PRIORITY )
        {
            insertTopLevelItem( i, new Curve3DItem( curve, 0 ) );
            return;
        }
    }
    addTopLevelItem( new Curve3DItem( curve, 0 ) );
}

void SceneTreeWidget::tfAdded(Transform *tf)
{
    for ( int i = 0; i < topLevelItemCount(); i++ )
    {
        if ( static_cast<BaseItem*>(topLevelItem( i ) )->priority() < TRANSFORM_PRIORITY )
        {
            insertTopLevelItem( i, new TransformItem( tf, 0 ) );
            return;
        }
    }
    addTopLevelItem( new TransformItem( tf, 0 ) );
}

void SceneTreeWidget::tfGroupAdded(TransformGroup *tfGroup)
{
    for ( int i = 0; i < topLevelItemCount(); i++ )
    {
        if ( static_cast<BaseItem*>(topLevelItem( i ) )->priority() < TRANSFORM_GROUP_PRIORITY )
        {
            insertTopLevelItem( i, new TransformGroupItem( tfGroup, 0 ) );
            return;
        }
    }
    addTopLevelItem( new TransformGroupItem( tfGroup, 0 ) );
}

void SceneTreeWidget::pointsCloudAdded(PointsCloud *pointCloud)
{
    for ( int i = 0; i < topLevelItemCount(); i++ )
    {
        if ( static_cast<BaseItem*>(topLevelItem( i ) )->priority() < POINTS_CLOUD_PRIORITY )
        {
            insertTopLevelItem( i, new PointsCloudItem( pointCloud, 0 ) );
            return;
        }
    }
    addTopLevelItem( new PointsCloudItem( pointCloud, 0 ) );
}


void SceneTreeWidget::lightAdded( AbstractLight * l )
{
//    if ( SpotLight * light = qobject_cast<SpotLight*>( l ) )
//    {
//        for ( int i = 0; i < topLevelItemCount(); i++ )
//        {
//            if ( static_cast<BaseItem*>(topLevelItem( i ) )->priority() < SPOTLIGHT_PRIORITY )
//            {
//                insertTopLevelItem( i, new SpotLightItem( light, 0 ) );
//                return;
//            }
//        }
//        addTopLevelItem( new SpotLightItem( light, 0 ) );
//    }
//    else if ( AmbientLight * light = qobject_cast<AmbientLight*>( l ) )
//    {
//        for ( int i = 0; i < topLevelItemCount(); i++ )
//        {
//            if ( static_cast<BaseItem*>(topLevelItem( i ) )->priority() < AMBIENTLIGHT_PRIORITY )
//            {
//                insertTopLevelItem( i, new AmbientLightItem( light, 0 ) );
//                return;
//            }
//        }
//        addTopLevelItem( new AmbientLightItem( light, 0 ) );
//    }
//    else if ( DirectionalLight * light = qobject_cast<DirectionalLight*>( l ) )
//    {
//        for ( int i = 0; i < topLevelItemCount(); i++ )
//        {
//            if ( static_cast<BaseItem*>(topLevelItem( i ) )->priority() < DIRECTIONALLIGHT_PRIORITY )
//            {
//                insertTopLevelItem( i, new DirectionalLightItem( light, 0 ) );
//                return;
//            }
//        }
//        addTopLevelItem( new DirectionalLightItem( light, 0 ) );
//    }
//    else if ( PointLight * light = qobject_cast<PointLight*>( l ) )
//    {
//        for ( int i = 0; i < topLevelItemCount(); i++ )
//        {
//            if ( static_cast<BaseItem*>(topLevelItem( i ) )->priority() < POINTLIGHT_PRIORITY )
//            {
//                insertTopLevelItem( i, new PointLightItem( light, 0 ) );
//                return;
//            }
//        }
//        addTopLevelItem( new PointLightItem( light, 0 ) );
//    }
}


void SceneTreeWidget::modelAdded( Model * model )
{
    addTopLevelItem( new ModelItem( model, 0 ) );
}

void SceneTreeWidget::robotAdded(Robot *robot)
{
    addTopLevelItem(new RobotItem(robot,0));
}


void SceneTreeWidget::hostDestroyed( QObject * host )
{
    if ( host == _host )
    {
        _host = 0;
        reload();
    }
}


