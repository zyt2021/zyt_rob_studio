﻿#include <Gui/OptionKits/MsnhOpenglOption.h>
#include "ui_MsnhOpenglOption.h"
#include <Util/MsnhEngineUtil.h>

MsnhOpenglOption::MsnhOpenglOption(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhOpenglOption)
{
    ui->setupUi(this);

    ui->pointSize->setMin(0.1f);
    ui->pointSize->setMax(100.0f);
    ui->pointSize->setPrecision(0.1f);

    update();

    connect(ui->selectedColor, &ColorMaster::colorChanged, this, &MsnhOpenglOption::changeSelectedColor);
    connect(ui->highlightColor, &ColorMaster::colorChanged, this, &MsnhOpenglOption::changeHighlightColor);
    connect(ui->pointSize, &MsnhFloatBox::valueChanged, this, &MsnhOpenglOption::changePointSize);
}

MsnhOpenglOption::~MsnhOpenglOption()
{
    delete ui;
}

void MsnhOpenglOption::on_AACkbx_clicked()
{
    EngineUtil::enableAnti = ui->AACkbx->isChecked();
}

void MsnhOpenglOption::on_gamaCkbx_clicked()
{
    EngineUtil::enableGama = ui->gamaCkbx->isChecked();
}

void MsnhOpenglOption::changeSelectedColor(const QColor &color)
{
    EngineUtil::selectColor = color;
}

void MsnhOpenglOption::changeHighlightColor(const QColor &color)
{
    EngineUtil::highlightColor = color;
}

void MsnhOpenglOption::changePointSize(float size)
{
    EngineUtil::pointSize = size;
}

void MsnhOpenglOption::on_setDefaultBtn_clicked()
{
    EngineUtil::setOpenGLDefault();
    update();
}

void MsnhOpenglOption::update()
{
    ui->AACkbx->setChecked(EngineUtil::enableAnti);
    ui->gamaCkbx->setChecked(EngineUtil::enableGama);
    ui->selectedColor->setColor(EngineUtil::selectColor);
    ui->selectedColor->update();
    ui->highlightColor->setColor(EngineUtil::highlightColor);
    ui->highlightColor->update();
    ui->pointSize->updateVal(EngineUtil::pointSize);
}

