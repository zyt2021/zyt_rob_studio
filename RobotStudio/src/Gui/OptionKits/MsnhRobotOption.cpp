﻿#include <Gui/OptionKits/MsnhRobotOption.h>
#include "ui_MsnhRobotOption.h"
#include <Util/MsnhEngineUtil.h>
MsnhRobotOption::MsnhRobotOption(QWidget *parent, Scene *scene) :
    QDialog(parent),
    ui(new Ui::MsnhRobotOption),
    _scene(scene)
{
   ui->setupUi(this);
   update();

   connect(ui->animationColor, &ColorMaster::colorChanged, this, &MsnhRobotOption::changeAnimationColor);
   connect(ui->dragColor, &ColorMaster::colorChanged, this, &MsnhRobotOption::changeDragColor);
}

MsnhRobotOption::~MsnhRobotOption()
{
    delete ui;
}

void MsnhRobotOption::update()
{
    ui->maxChainsSpinBx->setValue(RobotUtil::chainNums);
    ui->dragColor->setColor(RobotUtil::dragColor);
    ui->animationColor->setColor(RobotUtil::animationColor);
    ui->dragColor->update();
    ui->animationColor->update();
}

void MsnhRobotOption::on_setDefaultBtn_clicked()
{
    RobotUtil::setRobotDefault();
    update();
}

void MsnhRobotOption::changeDragColor(const QColor &color)
{
    RobotUtil::dragColor = color;

    auto robots = _scene->getRobots();

    for (int i=0; i<robots.size(); i++)
    {
        auto models = robots[i]->getEndModels();
        for(auto model : models)
        {
            model->setColor({color.red()/256.f,color.green()/256.f,color.blue()/256.f});
        }
    }
}

void MsnhRobotOption::changeAnimationColor(const QColor &color)
{
    RobotUtil::animationColor = color;

    auto robots = _scene->getRobots();

    for (int i=0; i<robots.size(); i++)
    {
        auto models = robots[i]->getAnimationlModels();
        for(auto model : models)
        {
            model->setColor({color.red()/256.f,color.green()/256.f,color.blue()/256.f});
        }
    }
}
