﻿#include <Gui/Plot/MsnhGraphicView.h>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhGraphicView::MsnhGraphicView(QWidget *parent):QGraphicsView(parent)
{
    _scene = new QGraphicsScene(this);
    _scene->setBackgroundBrush(QBrush(QColor(25,25,25)));
    this->setScene(_scene);
    this->setDragMode(QGraphicsView::DragMode::ScrollHandDrag);
}

void MsnhGraphicView::setImage(QImage img)
{
    this->_image = img;
    _pix = QPixmap::fromImage(img);
    _scene->clear();
    _scene->addPixmap(_pix);
    QGraphicsPixmapItem pixA(_pix);
    _scene->setSceneRect(0,0,img.width(),img.height());
    if(fitView)
        this->fitInView(pixA.boundingRect(),Qt::KeepAspectRatio);
    manualZoom();
}

void MsnhGraphicView::zoom(bool flag)
{
    if (flag && _scaleFactor >= 0)
    {
        _scaleFactor += 5;
        if(_scaleFactor>800)
        {
            _scaleFactor=800;
        }
        QMatrix old_matrix;
        old_matrix = this->matrix();
        this->resetMatrix();
        this->translate(old_matrix.dx(), old_matrix.dy());
        this->scale(_scaleFactor/100.0, _scaleFactor/100.0);
    }
    else if (!flag && _scaleFactor >= 0)
    {
        _scaleFactor -= 5;

        if(_scaleFactor<=10)
        {
            _scaleFactor=10;
        }

        QMatrix old_matrix;
        old_matrix = this->matrix();
        this->resetMatrix();
        this->translate(old_matrix.dx(), old_matrix.dy());

        this->scale( _scaleFactor/100.0,  _scaleFactor/100.0);
    }
    else if (_scaleFactor < 0)
    {
        _scaleFactor = 0.0;
    }
}

void MsnhGraphicView::manualZoom(int size)
{
    _scaleFactor = size;
    QMatrix old_matrix;
    old_matrix = this->matrix();
    this->resetMatrix();
    this->translate(old_matrix.dx(), old_matrix.dy());
    this->scale( _scaleFactor/100.0,  _scaleFactor/100.0);
}

void MsnhGraphicView::manualZoom()
{
    QMatrix old_matrix;
    old_matrix = this->matrix();
    this->resetMatrix();
    this->translate(old_matrix.dx(), old_matrix.dy());
    this->scale( _scaleFactor/100.0,  _scaleFactor/100.0);
}

QImage MsnhGraphicView::getImage() const
{
    return _image;
}

void MsnhGraphicView::wheelEvent(QWheelEvent *event)
{

    if(event->delta()>0)
    {
        zoom(true);
    }
    else
    {
        zoom(false);
    }

}
