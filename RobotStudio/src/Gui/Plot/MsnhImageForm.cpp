﻿#include <Gui/Plot/MsnhImageForm.h>
#include "ui_MsnhImageForm.h"
#include <QFileDialog>
#include <Util/MsnhCVUtilEx.h>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhImageForm::MsnhImageForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MsnhImageForm)
{
    ui->setupUi(this);
}

MsnhImageForm::~MsnhImageForm()
{
    delete ui;
}

void MsnhImageForm::on_saveBtn_clicked()
{
    QString path=QFileDialog::getSaveFileName(this,"保存图片","a.png","图片(*.png)");
    if(path=="")
    {
        return;
    }
    ui->graphicsView->getImage().save(path);
}

void MsnhImageForm::on_scaleUpBtn_clicked()
{
    ui->graphicsView->zoom(true);
}

void MsnhImageForm::on_scaleDownBtn_clicked()
{
    ui->graphicsView->zoom(false);
}

void MsnhImageForm::on_fixBtn_clicked()
{
    ui->graphicsView->manualZoom(100);
}

cv::Mat MsnhImageForm::getMat() const
{
    return _mat;
}

void MsnhImageForm::setImage(const cv::Mat &mat)
{
    _mat = mat;
    QImage image = MsnhCVUtil::MatToQImage(mat);

    if(image.width()!=_w || image.height()!=_h)
    {
        _w = image.width();
        _h = image.height();

        ui->imageInfo->setText(QString("宽度: %1px 高度: %2px").arg(_w).arg(_h));
    }

    ui->graphicsView->setImage(image);
}

QImage MsnhImageForm::getImage() const
{
    return ui->graphicsView->getImage();
}
