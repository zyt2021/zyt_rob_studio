﻿#include <Gui/Plot/MsnhPlotPosVecAcc.h>
#include "ui_MsnhPlotPosVecAcc.h"
#include <QCheckBox>

MsnhPlotPosVecAcc::MsnhPlotPosVecAcc(PlotType plotType, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhPlotPosVecAcc),
    _plotType(plotType)
{
    ui->setupUi(this);
    configPlots();
    _maxDataSize = 50000;
    _currentDataSize = 0;
    _lastPos     = 0;
    _lastVel     = 0;
    _lastX       = 0;
    _lastXVel    = 0;
    _lastY       = 0;
    _lastYVel    = 0;
    _lastZ       = 0;
    _lastZVel    = 0;
    _lastRX      = 0;
    _lastRXVel   = 0;
    _lastRY      = 0;
    _lastRYVel   = 0;
    _lastRZ      = 0;
    _lastRZVel   = 0;
}

MsnhPlotPosVecAcc::~MsnhPlotPosVecAcc()
{
    delete ui;
}

void MsnhPlotPosVecAcc::configPlots()
{
    QBrush brush(QColor(25,25,25));
    ui->posPlot->legend->setVisible(true);
    ui->posPlot->legend->setBrush(brush);
    ui->posPlot->legend->setTextColor(QColor(240,240,240));

    ui->posPlot->setBackground(brush);
    ui->posPlot->xAxis->setBasePen(QPen(Qt::white, 1));
    ui->posPlot->yAxis->setBasePen(QPen(Qt::white, 1));
    ui->posPlot->xAxis->setTickPen(QPen(Qt::white, 1));
    ui->posPlot->yAxis->setTickPen(QPen(Qt::white, 1));
    ui->posPlot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->posPlot->yAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->posPlot->xAxis->setTickLabelColor(Qt::white);
    ui->posPlot->yAxis->setTickLabelColor(Qt::white);
    ui->posPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->posPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->posPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->posPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->posPlot->xAxis->grid()->setSubGridVisible(true);
    ui->posPlot->yAxis->grid()->setSubGridVisible(true);
    ui->posPlot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->posPlot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->posPlot->xAxis->setLabelColor(QColor(255,255,255));
    ui->posPlot->yAxis->setLabelColor(QColor(255,255,255));

    if(_plotType == PLOT_SINGLE)
    {
        ui->posPlot->addGraph();
        ui->posPlot->graph(0)->setPen(QPen(QColor(232,40,40),2));
        ui->posPlot->graph(0)->setName("x");
    }
    else if(_plotType == PLOT_CARTESIAN)
    {
        ui->posPlot->addGraph();
        ui->posPlot->addGraph();
        ui->posPlot->addGraph();
        ui->posPlot->addGraph();
        ui->posPlot->addGraph();
        ui->posPlot->addGraph();
        ui->posPlot->graph(0)->setPen(QPen(QColor(232,40,40),2));
        ui->posPlot->graph(1)->setPen(QPen(QColor(40,232,40),2));
        ui->posPlot->graph(2)->setPen(QPen(QColor(80,80,232),2));
        ui->posPlot->graph(3)->setPen(QPen(QColor(232,232,40),2));
        ui->posPlot->graph(4)->setPen(QPen(QColor(40,232,232),2));
        ui->posPlot->graph(5)->setPen(QPen(QColor(232,40,232),2));

        ui->posPlot->graph(0)->setName("x");
        ui->posPlot->graph(1)->setName("y");
        ui->posPlot->graph(2)->setName("z");
        ui->posPlot->graph(3)->setName("rx");
        ui->posPlot->graph(4)->setName("ry");
        ui->posPlot->graph(5)->setName("rz");
    }

    ui->speedPlot->legend->setVisible(true);
    ui->speedPlot->legend->setBrush(brush);
    ui->speedPlot->legend->setTextColor(QColor(240,240,240));

    ui->speedPlot->setBackground(brush);
    ui->speedPlot->xAxis->setBasePen(QPen(Qt::white, 1));
    ui->speedPlot->yAxis->setBasePen(QPen(Qt::white, 1));
    ui->speedPlot->xAxis->setTickPen(QPen(Qt::white, 1));
    ui->speedPlot->yAxis->setTickPen(QPen(Qt::white, 1));
    ui->speedPlot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->speedPlot->yAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->speedPlot->xAxis->setTickLabelColor(Qt::white);
    ui->speedPlot->yAxis->setTickLabelColor(Qt::white);
    ui->speedPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->speedPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->speedPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->speedPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->speedPlot->xAxis->grid()->setSubGridVisible(true);
    ui->speedPlot->yAxis->grid()->setSubGridVisible(true);
    ui->speedPlot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->speedPlot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->speedPlot->xAxis->setLabelColor(QColor(255,255,255));
    ui->speedPlot->yAxis->setLabelColor(QColor(255,255,255));

    if(_plotType == PLOT_SINGLE)
    {
        ui->speedPlot->addGraph();
        ui->speedPlot->graph(0)->setPen(QPen(QColor(232,40,40),2));
        ui->speedPlot->graph(0)->setName("x");
    }
    else if(_plotType == PLOT_CARTESIAN)
    {
        ui->speedPlot->addGraph();
        ui->speedPlot->addGraph();
        ui->speedPlot->addGraph();
        ui->speedPlot->addGraph();
        ui->speedPlot->addGraph();
        ui->speedPlot->addGraph();
        ui->speedPlot->graph(0)->setPen(QPen(QColor(232,40,40),2));
        ui->speedPlot->graph(1)->setPen(QPen(QColor(40,232,40),2));
        ui->speedPlot->graph(2)->setPen(QPen(QColor(80,80,232),2));
        ui->speedPlot->graph(3)->setPen(QPen(QColor(232,232,40),2));
        ui->speedPlot->graph(4)->setPen(QPen(QColor(40,232,232),2));
        ui->speedPlot->graph(5)->setPen(QPen(QColor(232,40,232),2));

        ui->speedPlot->graph(0)->setName("x");
        ui->speedPlot->graph(1)->setName("y");
        ui->speedPlot->graph(2)->setName("z");
        ui->speedPlot->graph(3)->setName("rx");
        ui->speedPlot->graph(4)->setName("ry");
        ui->speedPlot->graph(5)->setName("rz");
    }

    ui->accPlot->legend->setVisible(true);
    ui->accPlot->legend->setBrush(brush);
    ui->accPlot->legend->setTextColor(QColor(240,240,240));

    ui->accPlot->setBackground(brush);
    ui->accPlot->xAxis->setBasePen(QPen(Qt::white, 1));
    ui->accPlot->yAxis->setBasePen(QPen(Qt::white, 1));
    ui->accPlot->xAxis->setTickPen(QPen(Qt::white, 1));
    ui->accPlot->yAxis->setTickPen(QPen(Qt::white, 1));
    ui->accPlot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->accPlot->yAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->accPlot->xAxis->setTickLabelColor(Qt::white);
    ui->accPlot->yAxis->setTickLabelColor(Qt::white);
    ui->accPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->accPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->accPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->accPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->accPlot->xAxis->grid()->setSubGridVisible(true);
    ui->accPlot->yAxis->grid()->setSubGridVisible(true);
    ui->accPlot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->accPlot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->accPlot->xAxis->setLabelColor(QColor(255,255,255));
    ui->accPlot->yAxis->setLabelColor(QColor(255,255,255));

    if(_plotType == PLOT_SINGLE)
    {
        ui->accPlot->addGraph();
        ui->accPlot->graph(0)->setPen(QPen(QColor(232,40,40),2));
        ui->accPlot->graph(0)->setName("x");
    }
    else if(_plotType == PLOT_CARTESIAN)
    {
        ui->accPlot->addGraph();
        ui->accPlot->addGraph();
        ui->accPlot->addGraph();
        ui->accPlot->addGraph();
        ui->accPlot->addGraph();
        ui->accPlot->addGraph();
        ui->accPlot->graph(0)->setPen(QPen(QColor(232,40,40),2));
        ui->accPlot->graph(1)->setPen(QPen(QColor(40,232,40),2));
        ui->accPlot->graph(2)->setPen(QPen(QColor(80,80,232),2));
        ui->accPlot->graph(3)->setPen(QPen(QColor(232,232,40),2));
        ui->accPlot->graph(4)->setPen(QPen(QColor(40,232,232),2));
        ui->accPlot->graph(5)->setPen(QPen(QColor(232,40,232),2));
        ui->accPlot->graph(0)->setName("x");
        ui->accPlot->graph(1)->setName("y");
        ui->accPlot->graph(2)->setName("z");
        ui->accPlot->graph(3)->setName("rx");
        ui->accPlot->graph(4)->setName("ry");
        ui->accPlot->graph(5)->setName("rz");
    }

    if(_plotType == PLOT_SINGLE)
    {
        ui->cartOptionCkbx->setVisible(false);
    }
    else if(_plotType == PLOT_CARTESIAN)
    {
        connect(ui->xCkbx,&QCheckBox::toggled,ui->posPlot->graph(0), &QCPGraph::setVisible);
        connect(ui->yCkbx,&QCheckBox::toggled,ui->posPlot->graph(1), &QCPGraph::setVisible);
        connect(ui->zCkbx,&QCheckBox::toggled,ui->posPlot->graph(2), &QCPGraph::setVisible);
        connect(ui->rxCkbx,&QCheckBox::toggled,ui->posPlot->graph(3), &QCPGraph::setVisible);
        connect(ui->ryCkbx,&QCheckBox::toggled,ui->posPlot->graph(4), &QCPGraph::setVisible);
        connect(ui->rzCkbx,&QCheckBox::toggled,ui->posPlot->graph(5), &QCPGraph::setVisible);

        connect(ui->xCkbx,&QCheckBox::toggled,ui->speedPlot->graph(0), &QCPGraph::setVisible);
        connect(ui->yCkbx,&QCheckBox::toggled,ui->speedPlot->graph(1), &QCPGraph::setVisible);
        connect(ui->zCkbx,&QCheckBox::toggled,ui->speedPlot->graph(2), &QCPGraph::setVisible);
        connect(ui->rxCkbx,&QCheckBox::toggled,ui->speedPlot->graph(3), &QCPGraph::setVisible);
        connect(ui->ryCkbx,&QCheckBox::toggled,ui->speedPlot->graph(4), &QCPGraph::setVisible);
        connect(ui->rzCkbx,&QCheckBox::toggled,ui->speedPlot->graph(5), &QCPGraph::setVisible);

        connect(ui->xCkbx,&QCheckBox::toggled,ui->accPlot->graph(0), &QCPGraph::setVisible);
        connect(ui->yCkbx,&QCheckBox::toggled,ui->accPlot->graph(1), &QCPGraph::setVisible);
        connect(ui->zCkbx,&QCheckBox::toggled,ui->accPlot->graph(2), &QCPGraph::setVisible);
        connect(ui->rxCkbx,&QCheckBox::toggled,ui->accPlot->graph(3), &QCPGraph::setVisible);
        connect(ui->ryCkbx,&QCheckBox::toggled,ui->accPlot->graph(4), &QCPGraph::setVisible);
        connect(ui->rzCkbx,&QCheckBox::toggled,ui->accPlot->graph(5), &QCPGraph::setVisible);
    }
}

void MsnhPlotPosVecAcc::setDatas(const QVector<QVector<double> > &pos, const QVector<QVector<double> > &vel, const QVector<QVector<double> > &acc)
{
    if(_plotType == PLOT_SINGLE)
    {
        if(pos.size()!=1)
        {
            return;
        }
        else
        {
            if(vel.size()!=pos.size() || pos.size()!=acc.size())
            {
                return;
            }

            if(vel[0].size()!=pos[0].size() || pos[0].size()!=acc[0].size())
            {
                return;
            }

            for (int i = 0; i < pos[0].size(); ++i)
            {
                ui->posPlot->graph(0)->addData(i, pos[0][i]);
                ui->speedPlot->graph(0)->addData(i, vel[0][i]);
                ui->accPlot->graph(0)->addData(i, acc[0][i]);
            }
            _currentDataSize = pos.size() - 1;

            ui->posPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
            ui->posPlot->yAxis->rescale();
            ui->posPlot->replot();

            ui->speedPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
            ui->speedPlot->yAxis->rescale();
            ui->speedPlot->replot();

            ui->accPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
            ui->accPlot->yAxis->rescale();
            ui->accPlot->replot();
        }
    }
    else if(_plotType == PLOT_CARTESIAN)
    {
        if(pos.size()!=6)
        {
            return;
        }
        else
        {
            if(vel.size()!=pos.size() || pos.size()!=acc.size())
            {
                return;
            }

            if(vel[0].size()!=pos[0].size() || pos[0].size()!=acc[0].size())
            {
                return;
            }

            if(vel[1].size()!=pos[1].size() || pos[1].size()!=acc[1].size())
            {
                return;
            }

            if(vel[2].size()!=pos[2].size() || pos[2].size()!=acc[2].size())
            {
                return;
            }

            if(vel[3].size()!=pos[3].size() || pos[3].size()!=acc[3].size())
            {
                return;
            }

            if(vel[4].size()!=pos[4].size() || pos[4].size()!=acc[4].size())
            {
                return;
            }

            if(vel[5].size()!=pos[5].size() || pos[5].size()!=acc[5].size())
            {
                return;
            }

            if(vel[0].size()!=vel[1].size() || vel[1].size()!=vel[2].size() ||
                    vel[2].size()!=vel[3].size() || vel[3].size()!=vel[4].size() ||
                    vel[4].size()!=vel[5].size() )
            {
                return;
            }

            for (int i = 0; i < pos[0].size(); ++i)
            {
                ui->posPlot->graph(0)->addData(i, pos[0][i]);
                ui->speedPlot->graph(0)->addData(i, vel[0][i]);
                ui->accPlot->graph(0)->addData(i, acc[0][i]);

                ui->posPlot->graph(1)->addData(i, pos[1][i]);
                ui->speedPlot->graph(1)->addData(i, vel[1][i]);
                ui->accPlot->graph(1)->addData(i, acc[1][i]);

                ui->posPlot->graph(2)->addData(i, pos[2][i]);
                ui->speedPlot->graph(2)->addData(i, vel[2][i]);
                ui->accPlot->graph(2)->addData(i, acc[2][i]);

                ui->posPlot->graph(3)->addData(i, pos[3][i]);
                ui->speedPlot->graph(3)->addData(i, vel[3][i]);
                ui->accPlot->graph(3)->addData(i, acc[3][i]);

                ui->posPlot->graph(4)->addData(i, pos[4][i]);
                ui->speedPlot->graph(4)->addData(i, vel[4][i]);
                ui->accPlot->graph(4)->addData(i, acc[4][i]);

                ui->posPlot->graph(5)->addData(i, pos[5][i]);
                ui->speedPlot->graph(5)->addData(i, vel[5][i]);
                ui->accPlot->graph(5)->addData(i, acc[5][i]);
            }

            _currentDataSize = pos.size() - 1;

            ui->posPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
            ui->posPlot->yAxis->rescale();
            ui->posPlot->replot();

            ui->speedPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
            ui->speedPlot->yAxis->rescale();
            ui->speedPlot->replot();

            ui->accPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
            ui->accPlot->yAxis->rescale();
            ui->accPlot->replot();
        }
    }
}

void MsnhPlotPosVecAcc::addData(const QVector<double> &pos, const QVector<double> &vel, const QVector<double> &acc)
{
    if(_plotType == PLOT_SINGLE)
    {
        if(pos.size()!=1)
        {
            return ;
        }
        else
        {
            if(pos.size() != vel.size() || vel.size() != acc.size())
            {
                return;
            }
            else
            {
                _currentDataSize++;

                ui->posPlot->graph(0)->addData(_currentDataSize, pos[0]);
                ui->speedPlot->graph(0)->addData(_currentDataSize, vel[0]);
                ui->accPlot->graph(0)->addData(_currentDataSize, acc[0]);

                ui->posPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
                ui->posPlot->yAxis->rescale();
                ui->posPlot->replot();

                ui->speedPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
                ui->speedPlot->yAxis->rescale();
                ui->speedPlot->replot();

                ui->accPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
                ui->accPlot->yAxis->rescale();
                ui->accPlot->replot();

                if(_currentDataSize >=_maxDataSize)
                {
                    _currentDataSize = 0;
                    ui->posPlot->graph(0)->data()->clear();
                    ui->speedPlot->graph(0)->data()->clear();
                    ui->accPlot->graph(0)->data()->clear();
                }
            }
        }
    }
    else if(_plotType == PLOT_CARTESIAN)
    {
        if(pos.size()!=6)
        {
            return ;
        }
        else
        {
            if(pos.size() != vel.size() || vel.size() != acc.size())
            {
                return;
            }
            else
            {
                _currentDataSize++;
                for (int i = 0; i < 6; ++i)
                {
                    ui->posPlot->graph(i)->addData(_currentDataSize, pos[i]);
                    ui->speedPlot->graph(i)->addData(_currentDataSize, vel[i]);
                    ui->accPlot->graph(i)->addData(_currentDataSize, acc[i]);
                }

                ui->posPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
                ui->posPlot->yAxis->rescale();
                ui->posPlot->replot();

                ui->speedPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
                ui->speedPlot->yAxis->rescale();
                ui->speedPlot->replot();

                ui->accPlot->xAxis->setRange(_currentDataSize, 1000, Qt::AlignRight);
                ui->accPlot->yAxis->rescale();
                ui->accPlot->replot();

                if(_currentDataSize >=_maxDataSize)
                {
                    _currentDataSize = 0;

                    for (int i = 0; i < 6; ++i)
                    {
                        ui->posPlot->graph(i)->data()->clear();
                        ui->speedPlot->graph(i)->data()->clear();
                        ui->accPlot->graph(i)->data()->clear();
                    }
                }
            }
        }
    }
}

//TODO: 角度0~360，速度和加速度问题
void MsnhPlotPosVecAcc::addPos(double pos)
{
    if(_currentDataSize == 0)
    {
        double tmpPos = pos;
        double tmpVel = 0;
        double tmpAcc = 0;
        addData({tmpPos}, {tmpVel}, {tmpAcc});
        _lastPos = pos;
    }
    else if(_currentDataSize == 1)
    {
        double tmpPos = pos;
        double tmpVel = pos - _lastPos;
        double tmpAcc = 0;
        addData({tmpPos}, {tmpVel}, {tmpAcc});
        _lastPos = pos;
        _lastVel = tmpVel;
    }
    else
    {
        double tmpPos = pos;
        double tmpVel = pos - _lastPos;
        double tmpAcc = tmpVel - _lastVel;
        addData({tmpPos}, {tmpVel}, {tmpAcc});
        _lastPos = pos;
        _lastVel = tmpVel;
    }

}

void MsnhPlotPosVecAcc::addFrame(const Msnhnet::Frame &frame)
{
    auto trans = frame.trans*1000;
    auto euler = Msnhnet::GeometryS::rotMat2Euler(frame.rotMat, Msnhnet::ROT_ZYX)*MSNH_RAD_2_DEG;
    if(_currentDataSize == 0)
    {
        double tmpXPos = trans[0];
        double tmpXVel = 0;
        double tmpXAcc = 0;
        double tmpYPos = trans[1];
        double tmpYVel = 0;
        double tmpYAcc = 0;
        double tmpZPos = trans[2];
        double tmpZVel = 0;
        double tmpZAcc = 0;
        double tmpRXPos = euler[0];
        double tmpRXVel = 0;
        double tmpRXAcc = 0;
        double tmpRYPos = euler[1];
        double tmpRYVel = 0;
        double tmpRYAcc = 0;
        double tmpRZPos = euler[2];
        double tmpRZVel = 0;
        double tmpRZAcc = 0;
        addData({tmpXPos,tmpYPos,tmpZPos,tmpRXPos,tmpRYPos,tmpRZPos},
        {tmpXVel,tmpYVel,tmpZVel,tmpRXVel,tmpRYVel,tmpRZVel},
        {tmpXAcc,tmpYAcc,tmpZAcc,tmpRXAcc,tmpRYAcc,tmpRZAcc});

        _lastX = tmpXPos;
        _lastY = tmpYPos;
        _lastZ = tmpZPos;
        _lastRX = tmpRXPos;
        _lastRY = tmpRYPos;
        _lastRZ = tmpRZPos;

    }
    else if(_currentDataSize == 1)
    {
        double tmpXPos = trans[0];
        double tmpXVel = tmpXPos - _lastX;
        double tmpXAcc = 0;
        double tmpYPos = trans[1];
        double tmpYVel = tmpYPos - _lastY;
        double tmpYAcc = 0;
        double tmpZPos = trans[2];
        double tmpZVel = tmpZPos - _lastZ;
        double tmpZAcc = 0;
        double tmpRXPos = euler[0];
        double tmpRXVel = tmpRXPos - _lastRX;
        double tmpRXAcc = 0;
        double tmpRYPos = euler[1];
        double tmpRYVel = tmpRYPos - _lastRY;
        double tmpRYAcc = 0;
        double tmpRZPos = euler[2];
        double tmpRZVel = tmpRZPos - _lastRZ;
        double tmpRZAcc = 0;
        addData({tmpXPos,tmpYPos,tmpZPos,tmpRXPos,tmpRYPos,tmpRZPos},
        {tmpXVel,tmpYVel,tmpZVel,tmpRXVel,tmpRYVel,tmpRZVel},
        {tmpXAcc,tmpYAcc,tmpZAcc,tmpRXAcc,tmpRYAcc,tmpRZAcc}
                );

        _lastX = tmpXPos;
        _lastY = tmpYPos;
        _lastZ = tmpZPos;
        _lastRX = tmpRXPos;
        _lastRY = tmpRYPos;
        _lastRZ = tmpRZPos;

        _lastXVel = tmpXVel;
        _lastYVel = tmpYVel;
        _lastZVel = tmpZVel;
        _lastRXVel = tmpRXVel;
        _lastRYVel = tmpRYVel;
        _lastRZVel = tmpRZVel;
    }
    else
    {
        double tmpXPos = trans[0];
        double tmpXVel = tmpXPos - _lastX;
        double tmpXAcc = tmpXVel - _lastXVel;
        double tmpYPos = trans[1];
        double tmpYVel = tmpYPos - _lastY;
        double tmpYAcc = tmpYVel - _lastYVel;
        double tmpZPos = trans[2];
        double tmpZVel = tmpZPos - _lastZ;
        double tmpZAcc = tmpZVel - _lastZVel;
        double tmpRXPos = euler[0];
        double tmpRXVel = tmpRXPos - _lastRX;
        double tmpRXAcc = tmpRXVel - _lastRXVel;
        double tmpRYPos = euler[1];
        double tmpRYVel = tmpRYPos - _lastRY;
        double tmpRYAcc = tmpRYVel - _lastRYVel;
        double tmpRZPos = euler[2];
        double tmpRZVel = tmpRZPos - _lastRZ;
        double tmpRZAcc = tmpRZVel - _lastRZVel;
        addData({tmpXPos,tmpYPos,tmpZPos,tmpRXPos,tmpRYPos,tmpRZPos},
        {tmpXVel,tmpYVel,tmpZVel,tmpRXVel,tmpRYVel,tmpRZVel},
        {tmpXAcc,tmpYAcc,tmpZAcc,tmpRXAcc,tmpRYAcc,tmpRZAcc});

        _lastX = tmpXPos;
        _lastY = tmpYPos;
        _lastZ = tmpZPos;
        _lastRX = tmpRXPos;
        _lastRY = tmpRYPos;
        _lastRZ = tmpRZPos;

        _lastXVel = tmpXVel;
        _lastYVel = tmpYVel;
        _lastZVel = tmpZVel;
        _lastRXVel = tmpRXVel;
        _lastRYVel = tmpRYVel;
        _lastRZVel = tmpRZVel;
    }
}
