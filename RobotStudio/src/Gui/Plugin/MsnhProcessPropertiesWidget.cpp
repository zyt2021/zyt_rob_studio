﻿#include <Gui/Plugin/MsnhProcessPropertiesWidget.h>

MsnhProcessPropertiesWidget::MsnhProcessPropertiesWidget(QWidget *parent):QWidget(parent)
{
    QFormLayout* layout = new QFormLayout(this);
    layout->setVerticalSpacing(0);

    setLayout(layout);
}

MsnhProcessPropertiesWidget::~MsnhProcessPropertiesWidget()
{

}

void MsnhProcessPropertiesWidget::init(MsnhProcess *process)
{
    _process = process;

    // remove all children
    while (layout()->count() > 0)
    {
        QLayoutItem* item = layout()->takeAt(0);
        if(item  != NULL )
        {
            delete item->widget();
            delete item;
        }
    }
    //  delete layout();

    auto* processSettings = process->getProperties();

    if(processSettings->size() == 0)
    {
        addPropertyWidget("This process has no properties.", "", NULL);
    }

    // sort the properties by user set position
    std::vector<MsnhProcessProperty*> orderedProperties;
    orderedProperties.reserve(processSettings->size());

    for (auto &entry: *processSettings)
        orderedProperties.push_back(entry.second.get());

    std::sort(orderedProperties.begin(), orderedProperties.end(), MsnhProcessPropertiesWidget::sortByPosition);

    // create all property widgets
    for (auto &property: orderedProperties)
    {
        // generate GUI based on the property type

        if (property->widget() == MSNH_WIDGET_HIDDEN) {} //Don't process hidden widgets
        else if (property->widget() == MSNH_WIDGET_LABEL) //Labels are type independent
        {
            QFormLayout* layout = (QFormLayout*) this->layout();

            QLabel* lblDescription = new QLabel(QString::fromLocal8Bit(property->description()));
            lblDescription->setWordWrap(true);
            lblDescription->setStyleSheet("color: #666; font-size: 10px");
            layout->addRow("", lblDescription);

            // add widget to list
            //_propertyWidgets.append(lblDescription);
        }

        else if (auto p = dynamic_cast<MsnhProcessPropertyInt*>(property))
        {
            MsnhPropertyWidget *widget = nullptr;
            QString rawName, name;
            switch(p->widget())
            {
            case MSNH_WIDGET_SLIDER:
                widget = new MsnhPropertySliderInt(p, this);
                addPropertyWidget(QString::fromLocal8Bit(property->title()), QString::fromLocal8Bit(property->description()), widget);
                break;

            case MSNH_WIDGET_RADIOBUTTONS:
                widget = new MsnhPropertyRadioInt(p, this);
                rawName = QString::fromLocal8Bit(property->title()); // name:value1|value2
                name = rawName.split(":").at(0);
                addPropertyWidget(name, QString::fromLocal8Bit(property->description()), widget);
                break;

            case MSNH_WIDGET_COMBOBOX:
                widget = new MsnhPropertyCombobox(p, this);
                rawName = QString::fromLocal8Bit(property->title()); // name:value1|value2
                name = rawName.split(":").at(0);
                addPropertyWidget(name, QString::fromLocal8Bit(property->description()), widget);
                break;

            case MSNH_WIDGET_BUTTON:
                widget = new MsnhPropertyButtonInt(p, this);
                addPropertyWidget(name, QString::fromLocal8Bit(property->description()), widget);
                break;

            default: //MSNH_WIDGET_SPINNER
                IPPropertySpinnerInt* widget = new IPPropertySpinnerInt(p, this);
                addPropertyWidget(QString::fromLocal8Bit(property->title()), QString::fromLocal8Bit(property->description()), widget);
                break;
            }
        }

        else if (auto p = dynamic_cast<MsnhProcessPropertyUnsignedInt*>(property))
        {
            MsnhPropertyWidget *widget = nullptr;
            QString rawName, name;


            switch(p->widget())
            {
                //TODO: Implement Sliders, Checkboxes etc.

            case MSNH_WIDGET_CHECKBOXES:
                widget = new MsnhPropertyCheckboxInt(p, this);
                rawName = QString::fromLocal8Bit(property->title()); // name:value1|value2
                name = rawName.split(":").at(0);
                addPropertyWidget(name, QString::fromLocal8Bit(property->description()), widget);
                break;

            case MSNH_WIDGET_BUTTON:
                widget = new MsnhPropertyButtonUnsignedInt(p, this);
                addPropertyWidget(name, QString::fromLocal8Bit(property->description()), widget);
                break;

            default: //MSNH_WIDGET_SPINNER
                IPPropertySpinnerUnsignedInt* widget = new IPPropertySpinnerUnsignedInt(p, this);
                addPropertyWidget(QString::fromLocal8Bit(property->title()), QString::fromLocal8Bit(property->description()), widget);
                break;
            }
        }

        else if (auto p = dynamic_cast<MsnhProcessPropertyDouble*>(property))
        {
            MsnhPropertyWidget *widget = nullptr;
            switch(p->widget())
            {
            default: //MSNH_WIDGET_SLIDER
                widget = new MsnhPropertySliderDouble(p, this);
                addPropertyWidget(QString::fromLocal8Bit(property->title()), QString::fromLocal8Bit(property->description()), widget);
                break;
            }
        }

        else if (auto p = dynamic_cast<MsnhProcessPropertyString*>(property))
        {
//            MsnhPropertyWidget *widget = nullptr;
//            QString rawName, name;
//            QString defaultDirectory =  _mainWindow->defaultImagePath();
//            switch(p->widget())
//            {
//            case MSNH_WIDGET_FILE_OPEN:
//                widget = new IPPropertyFileOpen(p, defaultDirectory, this);
//                addPropertyWidget(QString::fromLocal8Bit(property->title()), QString::fromLocal8Bit(property->description()), widget);
//                break;

//            case MSNH_WIDGET_FILE_SAVE:
//                widget = new IPPropertyFileSave(p, this);
//                // name:value1|value2
//                rawName = QString::fromLocal8Bit(property->title());
//                name = rawName.split(":").at(0);
//                addPropertyWidget(name, QString::fromLocal8Bit(property->description()), widget);
//                break;

//            case MSNH_WIDGET_FOLDER:
//                widget = new IPPropertyFolder(p, this);
//                addPropertyWidget(QString::fromLocal8Bit(property->title()), QString::fromLocal8Bit(property->description()), widget);
//                break;

//            default: //MSNH_WIDGET_TEXTFIELD
//                widget = new IPPropertyString(p, this);
//                addPropertyWidget(QString::fromLocal8Bit(property->title()), QString::fromLocal8Bit(property->description()), widget);
//                break;
//            }
        }
        else if (auto p = dynamic_cast<MsnhProcessPropertyBool*>(property))
        {
            MsnhPropertyWidget *widget = nullptr;
            switch(p->widget())
            {
            case MSNH_WIDGET_BUTTON:
                widget = new MsnhPropertyButtonBool(p, this);
                addPropertyWidget(QString::fromLocal8Bit(property->title()), QString::fromLocal8Bit(property->description()), widget);
                break;
            default: //MSNH_WIDGET_CHECKBOXES
                widget = new MsnhPropertyCheckbox(p, QString::fromLocal8Bit(property->title()), this);
                addPropertyWidget("", QString::fromLocal8Bit(property->description()), widget);
                break;
            }
        }
    }
}

void MsnhProcessPropertiesWidget::closeSettings()
{
    //TODO:
}

void MsnhProcessPropertiesWidget::resetSettings()
{
    for(MsnhPropertyWidget* widget : qAsConst(_propertyWidgets))
    {
        widget->resetValue();
    }
}

void MsnhProcessPropertiesWidget::addPropertyWidget(QString label, QString description, MsnhPropertyWidget *widget)
{
    QFormLayout* layout = (QFormLayout*) this->layout();

    layout->addRow(label, widget);

    // add widget to list
    _propertyWidgets.append(widget);

    QLabel* lblDescription = new QLabel(description);
    lblDescription->setWordWrap(true);
    lblDescription->setStyleSheet("color: #666; font-size: 10px");
    layout->addRow("", lblDescription);
}

bool MsnhProcessPropertiesWidget::sortByPosition(MsnhProcessProperty *x, MsnhProcessProperty *y)
{
    return x->position() < y->position();
}
