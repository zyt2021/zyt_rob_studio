﻿#include <Gui/Properties//MsnhAmbientLightProperty.h>
#include "ui_MsnhAmbientLightProperty.h"

MsnhAmbientLightProperty::MsnhAmbientLightProperty(AmbientLight *light, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhAmbientLightProperty),
    _host(light)
{
    ui->setupUi(this);
    ui->intensity->setMinMax(0,100);

    configSignals();
    updateVals();


}

MsnhAmbientLightProperty::~MsnhAmbientLightProperty()
{
    delete ui;
}

void MsnhAmbientLightProperty::on_enableCkbx_clicked(bool checked)
{
    ui->paramsGpbx->setEnabled(checked);
}

void MsnhAmbientLightProperty::configSignals()
{
    connect(_host, &AmbientLight::destroyed, this, &MsnhAmbientLightProperty::hostDestroyed);
    connect(ui->enableCkbx, &QCheckBox::toggled, _host, &AmbientLight::setEnabled);
    connect(ui->intensity, &MsnhFloatBox::valueChanged, _host, &AmbientLight::setIntensity);
    connect(ui->color, &ColorMaster::colorChanged,[this](QColor color)->void{
        _host->setColor(QVector3D(color.red()/255.f,color.green()/255.f,color.blue()/255.f));
    });
}

void MsnhAmbientLightProperty::updateVals()
{
    ui->enableCkbx->setChecked(_host->getEnabled());
    ui->paramsGpbx->setEnabled(_host->getEnabled());
    ui->intensity->updateVal(_host->getIntensity());
    ui->color->setColor(QColor(clampColor(_host->getColor().x()*255.f),_host->getColor().y()*255.f,_host->getColor().z()*255.f));
    ui->color->update();
}

void MsnhAmbientLightProperty::hostDestroyed(QObject *)
{
    delete this;
}

