﻿#include <Gui/Properties//MsnhCameraProperty.h>
#include "ui_MsnhCameraProperty.h"

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif
MsnhCameraProperty::MsnhCameraProperty(Camera *cam, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhCameraProperty),
    _host(cam)
{
    ui->setupUi(this);
    ui->movingSpeed->setMinMax(0,100);
    ui->FOV->setMinMax(0,180);
    ui->nearPlane->setMinMax(0.1f,inf);
    ui->farPlane->setMinMax(0.1f,inf);

    ui->xPos->setMinMax(-inf,inf);
    ui->yPos->setMinMax(-inf,inf);
    ui->zPos->setMinMax(-inf,inf);

    ui->xDirection->setMinMax(-inf,inf);
    ui->yDirection->setMinMax(-inf,inf);
    ui->zDirection->setMinMax(-inf,inf);

    ui->pitch->setMinMax(1,179);
    ui->yaw->setMinMax(0,360);
    ui->radius->setMinMax(0.1f,inf);
    ui->maxRadius->setMinMax(0.1f,inf);

    ui->xFocus->setMinMax(-inf,inf);
    ui->yFocus->setMinMax(-inf,inf);
    ui->zFocus->setMinMax(-inf,inf);

    updateVals();
    configSignals();
}

MsnhCameraProperty::~MsnhCameraProperty()
{
    delete ui; //host删除了，属性也删除
}

void MsnhCameraProperty::configSignals()
{
    connect(_host, &Camera::destroyed, this, &MsnhCameraProperty::hostDestroyed);

    connect(ui->movingSpeed, &MsnhFloatBox::valueChanged, _host, &Camera::setMovingSpeed);
    connect(ui->FOV, &MsnhFloatBox::valueChanged, _host, &Camera::setFieldOfView);
    connect(ui->nearPlane, &MsnhFloatBox::valueChanged, _host, &Camera::setNearPlane);
    connect(ui->farPlane, &MsnhFloatBox::valueChanged, _host, &Camera::setFarPlane);

    connect(ui->xPos, &MsnhFloatBox::valueChanged, this, &MsnhCameraProperty::positonChanged);
    connect(ui->yPos, &MsnhFloatBox::valueChanged, this, &MsnhCameraProperty::positonChanged);
    connect(ui->zPos, &MsnhFloatBox::valueChanged, this, &MsnhCameraProperty::positonChanged);

    connect(ui->xDirection, &MsnhFloatBox::valueChanged, this, &MsnhCameraProperty::directionChanged);
    connect(ui->yDirection, &MsnhFloatBox::valueChanged, this, &MsnhCameraProperty::directionChanged);
    connect(ui->zDirection, &MsnhFloatBox::valueChanged, this, &MsnhCameraProperty::directionChanged);

    connect(ui->pitch,&MsnhFloatBox::valueChanged, _host, &Camera::setOrbitPitch);
    connect(ui->yaw, &MsnhFloatBox::valueChanged, _host, &Camera::setOrbitYaw);
    connect(ui->radius, &MsnhFloatBox::valueChanged, _host, &Camera::setOrbitRadius);
    connect(ui->maxRadius, &MsnhFloatBox::valueChanged, _host,&Camera::setOrbitMaxRadius);

    connect(ui->xFocus, &MsnhFloatBox::valueChanged, this, &MsnhCameraProperty::focusChanged);
    connect(ui->yFocus, &MsnhFloatBox::valueChanged, this, &MsnhCameraProperty::focusChanged);
    connect(ui->zFocus, &MsnhFloatBox::valueChanged, this, &MsnhCameraProperty::focusChanged);

    connect(_host, &Camera::movingSpeedChanged, ui->movingSpeed, &MsnhFloatBox::updateVal);
    connect(_host, &Camera::fieldOfViewChanged, ui->FOV, &MsnhFloatBox::updateVal);
    connect(_host, &Camera::nearPlaneChanged, ui->nearPlane, &MsnhFloatBox::updateVal);
    connect(_host, &Camera::farPlaneChanged, ui->farPlane, &MsnhFloatBox::updateVal);

    connect(_host, &Camera::positionChanged,this,&MsnhCameraProperty::updatePosition);
    connect(_host, &Camera::directionChanged,this,&MsnhCameraProperty::updateDirection);

    connect(_host, &Camera::orbitPitchChanged, ui->pitch, &MsnhFloatBox::updateVal);
    connect(_host, &Camera::orbitYawChanged, ui->yaw, &MsnhFloatBox::updateVal);
    connect(_host, &Camera::orbitRadiusChanged, ui->radius, &MsnhFloatBox::updateVal);
    connect(_host, &Camera::orbitMaxRadiusChanged, ui->maxRadius, &MsnhFloatBox::updateVal);

    connect(_host, &Camera::orbitFocusCenterChanged,this,&MsnhCameraProperty::updateFocus);
}

void MsnhCameraProperty::updateVals()
{
    ui->movingSpeed->updateVal(_host->getMovingSpeed());
    ui->FOV->updateVal(_host->getFieldOfView());
    ui->nearPlane->updateVal(_host->getNearPlane());
    ui->farPlane->updateVal(_host->getfFarPlane());

    ui->pitch->updateVal(_host->getOrbitPitch());
    ui->yaw->updateVal(_host->getOrbitYaw());
    ui->radius->updateVal(_host->getOrbitRadius());
    ui->maxRadius->updateVal(_host->getOrbitMaxRadius());

    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());

    ui->xDirection->updateVal(_host->getDirectionApi().x());
    ui->yDirection->updateVal(_host->getDirectionApi().y());
    ui->zDirection->updateVal(_host->getDirectionApi().z());

    ui->xFocus->updateVal(_host->getFocusCenterApi().x());
    ui->yFocus->updateVal(_host->getFocusCenterApi().y());
    ui->zFocus->updateVal(_host->getFocusCenterApi().z());
}

void MsnhCameraProperty::hostDestroyed(QObject *)
{
    delete this;
}

void MsnhCameraProperty::positonChanged(float)
{
    _host->setPositionApi(QVector3D(ui->xPos->getVal(), ui->yPos->getVal(), ui->zPos->getVal()));
}

void MsnhCameraProperty::directionChanged(float)
{
    _host->setDirectionApi(QVector3D(ui->xDirection->getVal(), ui->yDirection->getVal(), ui->zDirection->getVal()));
}

void MsnhCameraProperty::focusChanged(float)
{
    _host->setFocusCenterApi(QVector3D(ui->xFocus->getVal(), ui->yFocus->getVal(), ui->zFocus->getVal()));
}

void MsnhCameraProperty::on_resetBtn_clicked()
{
    _host->reset();
}

void MsnhCameraProperty::updatePosition(QVector3D)
{
    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());
}

void MsnhCameraProperty::updateDirection(QVector3D)
{
    ui->xDirection->updateVal(_host->getDirectionApi().x());
    ui->yDirection->updateVal(_host->getDirectionApi().y());
    ui->zDirection->updateVal(_host->getDirectionApi().z());
}

void MsnhCameraProperty::updateFocus(QVector3D)
{
    ui->xFocus->updateVal(_host->getFocusCenterApi().x());
    ui->yFocus->updateVal(_host->getFocusCenterApi().y());
    ui->zFocus->updateVal(_host->getFocusCenterApi().z());
}

void MsnhCameraProperty::on_setCenterBtn_clicked()
{
    if(AbstractEntity::getSelectedObject()==nullptr)
    {
        QMessageBox::warning(this,"警告","没有物体被选中");
        return;
    }

    _host->setFocusCenterApi(AbstractEntity::getSelectedObject()->getGlobalPositionApi());
}
