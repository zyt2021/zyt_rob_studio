﻿#include <Gui/Properties/MsnhCurveProperty.h>
#include "ui_MsnhCurveProperty.h"

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhCurveProperty::MsnhCurveProperty(Curve3D *curve, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhCurveProperty),
    _host(curve)
{
    ui->setupUi(this);
    //    ui->lineWidth->setEnabled(false);
    //    ui->cubeLineCkbx->setEnabled(false);
    ui->xValue->setMinMax(-FLT_MAX,FLT_MAX);
    ui->yValue->setMinMax(-FLT_MAX,FLT_MAX);
    ui->zValue->setMinMax(-FLT_MAX,FLT_MAX);

    configSignals();
    updateVals();
}

MsnhCurveProperty::~MsnhCurveProperty()
{
    delete ui;
}

void MsnhCurveProperty::configSignals()
{
    connect(_host, &Curve3D::destroyed, this, &MsnhCurveProperty::hostDestroyed);

    connect(ui->visibleCkbx, &QCheckBox::toggled, _host, &Curve3D::setVisible);
    connect(ui->onTopCkbx, &QCheckBox::toggled, _host->getMarker(), &Mesh::setAlwaysOnTop);
    connect(ui->lineWidth, &MsnhFloatBox::valueChanged, _host, &Curve3D::setLineWidth);
    connect(ui->cubeLineCkbx, &QCheckBox::toggled, _host, &Curve3D::setIsCubeLine);

    connect(ui->color, &ColorMaster::colorChanged,[this](QColor color)->void{
        _host->setColor(QVector3D(color.red()/255.f,color.green()/255.f,color.blue()/255.f));
    });
}

void MsnhCurveProperty::updateVals()
{
    ui->color->setColor(QColor(clampColor(_host->getColor().x()*255.f),_host->getColor().y()*255.f,_host->getColor().z()*255.f));
    ui->color->update();

    ui->visibleCkbx->setChecked(_host->getVisible());
    ui->onTopCkbx->setChecked(_host->getMarker()->getAlwaysOnTop());
    ui->cubeLineCkbx->setChecked(_host->isCubeLine());
    ui->lineWidth->updateVal(_host->getLineWidth());
}

void MsnhCurveProperty::hostDestroyed(QObject *)
{
    delete this;
}

void MsnhCurveProperty::on_saveDataBtn_clicked()
{
    QString path = QFileDialog::getSaveFileName(this, "保存数据","","csv(*.csv)");
    if(path == "")
    {
        return;
    }

    QVector<QVector3D> datas = _host->getLineVertices();
    QFile file(path);
    file.open(QIODevice::WriteOnly);

    QTextStream sw(&file);

    for (int i = 0; i < datas.length(); ++i)
    {
        sw << datas[i].x() << "," << datas[i].y() << ","<<datas[i].z() << "\n";
    }

    sw.flush();
    file.close();
}

void MsnhCurveProperty::on_loadDataBtn_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, "加载数据","","csv(*.csv)");
    if(path == "")
    {
        return;
    }

    QVector<QVector3D> datas;

    QFile file(path);
    if(!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::warning(this,"警告","打开文件失败：\n" + path);
        return;
    }

    QTextStream sr(&file);

    QString read;
    while((read = sr.readLine())!="")
    {
        QStringList splits = read.split(",");

        if(splits.length()!=3)
        {
            QMessageBox::warning(this,"警告","数据格式错误：\n" + path);
            file.close();
            return;
        }

        datas.push_back(QVector3D( QString(splits[0]).toFloat(),
            QString(splits[1]).toFloat(),
            QString(splits[2]).toFloat()));
    }

    if(datas.isEmpty())
    {
        QMessageBox::warning(this,"警告","读入数据为空：\n" + path);
        return;
    }

    _host->setLineVertices(datas);
    on_syncDataBtn_clicked();
}

void MsnhCurveProperty::on_syncDataBtn_clicked()
{
    auto datas = _host->getLineVertices();

    ui->dataList->clear();

    for (int i = 0; i < datas.size(); ++i)
    {
        ui->dataList->addItem(QString::number(datas[i].x())+","+QString::number(datas[i].y())+","+QString::number(datas[i].z()));
    }
}


void MsnhCurveProperty::on_addDataBtn_clicked()
{
    _host->addData(ui->xValue->getVal(),ui->yValue->getVal(),ui->zValue->getVal());
        on_syncDataBtn_clicked();
}


void MsnhCurveProperty::on_deleteDataBtn_clicked()
{
    if(ui->dataList->currentItem())
    {
        _host->removeAt(ui->dataList->currentRow());
        on_syncDataBtn_clicked();
    }
    else
    {
        QMessageBox::warning(this,"警告","请选择一项再继续!");
    }

}

