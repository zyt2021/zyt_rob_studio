﻿#include <Gui/Properties/MsnhDirectionalLightProperty.h>
#include "ui_MsnhDirectionalLightProperty.h"

MsnhDirectionalLightProperty::MsnhDirectionalLightProperty(DirectionalLight *light, QWidget *parent) :
    QDialog(parent),
    _host(light),
    ui(new Ui::MsnhDirectionalLightProperty)
{
    ui->setupUi(this);
    ui->intensity->setMinMax(0,100);
    ui->xDirection->setMinMax(-inf, inf);
    ui->yDirection->setMinMax(-inf, inf);
    ui->zDirection->setMinMax(-inf, inf);

    configSignals();
    updateVals();
}

MsnhDirectionalLightProperty::~MsnhDirectionalLightProperty()
{
    delete ui;
}

void MsnhDirectionalLightProperty::configSignals()
{
    connect(_host, &DirectionalLight::destroyed, this, &MsnhDirectionalLightProperty::hostDestroyed);
    connect(ui->enableCkbx, &QCheckBox::toggled, _host, &DirectionalLight::setEnabled);
    connect(ui->intensity, &MsnhFloatBox::valueChanged, _host, &DirectionalLight::setIntensity);
    connect(ui->color, &ColorMaster::colorChanged,[this](QColor color)->void{
        _host->setColor(QVector3D(color.red()/255.f,color.green()/255.f,color.blue()/255.f));
    });

    connect(ui->xDirection, &MsnhFloatBox::valueChanged, this, &MsnhDirectionalLightProperty::directionChanged);
    connect(ui->yDirection, &MsnhFloatBox::valueChanged, this, &MsnhDirectionalLightProperty::directionChanged);
    connect(ui->zDirection, &MsnhFloatBox::valueChanged, this, &MsnhDirectionalLightProperty::directionChanged);

}

void MsnhDirectionalLightProperty::updateVals()
{
    ui->enableCkbx->setChecked(_host->getEnabled());
    ui->paramsGpbx->setEnabled(_host->getEnabled());
    ui->intensity->updateVal(_host->getIntensity());

    ui->xDirection->updateVal(_host->getDirection().x());
    ui->yDirection->updateVal(_host->getDirection().y());
    ui->zDirection->updateVal(_host->getDirection().z());
    ui->color->setColor(QColor(clampColor(_host->getColor().x()*255.f),_host->getColor().y()*255.f,_host->getColor().z()*255.f));
    ui->color->update();
}

void MsnhDirectionalLightProperty::directionChanged(float)
{
    _host->setDirection(QVector3D(ui->xDirection->getVal(),ui->yDirection->getVal(),ui->zDirection->getVal()));
}

void MsnhDirectionalLightProperty::hostDestroyed(QObject *)
{
    delete this;
}

void MsnhDirectionalLightProperty::on_enableCkbx_clicked(bool checked)
{
    ui->paramsGpbx->setEnabled(checked);
}
