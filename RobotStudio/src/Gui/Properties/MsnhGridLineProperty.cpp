﻿#include <Gui/Properties/MsnhGridLineProperty.h>
#include "ui_MsnhGridLineProperty.h"

MsnhGridLineProperty::MsnhGridLineProperty(Gridline *gridline, QWidget *parent) :
    QDialog(parent),
    _host(gridline),
    ui(new Ui::MsnhGridLineProperty)
{
    ui->setupUi(this);

    ui->xStart->setMinMax(-inf,inf);
    ui->xStop->setMinMax(-inf,inf);
    ui->xStride->setMinMax(0.1f,inf);

    ui->yStart->setMinMax(-inf,inf);
    ui->yStop->setMinMax(-inf,inf);
    ui->yStride->setMinMax(0.1f,inf);

    ui->zStart->setMinMax(-inf,inf);
    ui->zStop->setMinMax(-inf,inf);
    ui->zStride->setMinMax(0.1f,inf);

    ui->planeCbx->addItems({"  XY  ","  YZ  ","  ZX  "});
    ui->planeCbx->setCurrentIndex(0);
    updateVals();
    configSignals();
}

MsnhGridLineProperty::~MsnhGridLineProperty()
{
    delete ui;
}

void MsnhGridLineProperty::configSignals()
{
    connect(_host, &Gridline::destroyed, this, &MsnhGridLineProperty::hostDestroyed);

    connect(ui->xStart, &MsnhFloatBox::valueChanged,this,&MsnhGridLineProperty::xChanged);
    connect(ui->xStop,  &MsnhFloatBox::valueChanged,this,&MsnhGridLineProperty::xChanged);
    connect(ui->xStride,&MsnhFloatBox::valueChanged,this,&MsnhGridLineProperty::xChanged);

    connect(ui->yStart, &MsnhFloatBox::valueChanged,this,&MsnhGridLineProperty::yChanged);
    connect(ui->yStop,  &MsnhFloatBox::valueChanged,this,&MsnhGridLineProperty::yChanged);
    connect(ui->yStride,&MsnhFloatBox::valueChanged,this,&MsnhGridLineProperty::yChanged);

    connect(ui->zStart, &MsnhFloatBox::valueChanged,this,&MsnhGridLineProperty::zChanged);
    connect(ui->zStop,  &MsnhFloatBox::valueChanged,this,&MsnhGridLineProperty::zChanged);
    connect(ui->zStride,&MsnhFloatBox::valueChanged,this,&MsnhGridLineProperty::zChanged);

    connect(ui->color, &ColorMaster::colorChanged,[this](QColor color)->void{
        _host->setColor(QVector3D(color.red()/255.f,color.green()/255.f,color.blue()/255.f));
    });

    connect(ui->planeCbx, &QComboBox::currentTextChanged, [this]()->void
    {
            _host->setGridPlane((Gridline::GridPlane)ui->planeCbx->currentIndex());
    });
}

void MsnhGridLineProperty::updateVals()
{
    this->setEnabled(_host->getCanModify());
    ui->xStart->updateVal(_host->getXRangeApi().first);
    ui->xStop->updateVal(_host->getXRangeApi().second);
    ui->xStride->updateVal(_host->getXStrideApi());

    ui->yStart->updateVal(_host->getYRangeApi().first);
    ui->yStop->updateVal(_host->getYRangeApi().second);
    ui->yStride->updateVal(_host->getYStrideApi());

    ui->zStart->updateVal(_host->getZRangeApi().first);
    ui->zStop->updateVal(_host->getZRangeApi().second);
    ui->zStride->updateVal(_host->getZStrideApi());


    ui->color->setColor(QColor(clampColor(_host->getColor().x()*255.f),_host->getColor().y()*255.f,_host->getColor().z()*255.f));
    ui->color->update();

    ui->planeCbx->setCurrentIndex((int)_host->getGridPlane());
}


void MsnhGridLineProperty::xChanged(float )
{
    _host->setXArgumentsApi(QVector3D(ui->xStart->getVal(), ui->xStop->getVal(), ui->xStride->getVal()));
}

void MsnhGridLineProperty::yChanged(float)
{
    _host->setYArgumentsApi(QVector3D(ui->yStart->getVal(), ui->yStop->getVal(), ui->yStride->getVal()));
}

void MsnhGridLineProperty::zChanged(float)
{
    _host->setZArgumentsApi(QVector3D(ui->zStart->getVal(), ui->zStop->getVal(), ui->zStride->getVal()));
}

void MsnhGridLineProperty::hostDestroyed(QObject *)
{
    delete this; //gridline 删除, 其属性也删除
}

void MsnhGridLineProperty::on_resetBtn_clicked()
{
    _host->reset();
    updateVals();
}
