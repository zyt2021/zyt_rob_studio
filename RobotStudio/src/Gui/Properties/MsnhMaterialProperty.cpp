﻿#include <Gui/Properties/MsnhMaterialProperty.h>
#include "ui_MsnhMaterialProperty.h"

MsnhMaterialProperty::MsnhMaterialProperty(Material *material, QWidget *parent) :
    _host(material),
    QDialog(parent),
    ui(new Ui::MsnhMaterialProperty)
{
    ui->setupUi(this);

    ui->ambient->setMinMax(0,1.f);
    ui->diffuse->setMinMax(0,1.f);
    ui->specular->setMinMax(0,1.f);
    ui->shininess->setMinMax(0.01f, 100.0f);

    updateVals();
    configSignals();
}

MsnhMaterialProperty::~MsnhMaterialProperty()
{
    delete ui;
}

void MsnhMaterialProperty::hostDestroyed(QObject *)
{
    delete this;
}

void MsnhMaterialProperty::configSignals()
{
    connect(_host, &Material::destroyed, this, &MsnhMaterialProperty::hostDestroyed);

    connect(ui->ambient, &MsnhFloatBox::valueChanged, _host, &Material::setAmbient);
    connect(ui->diffuse, &MsnhFloatBox::valueChanged, _host, &Material::setDiffuse);
    connect(ui->specular, &MsnhFloatBox::valueChanged, _host, &Material::setSpecular);
    connect(ui->shininess, &MsnhFloatBox::valueChanged, _host, &Material::setShininess);

    connect(ui->color, &ColorMaster::colorChanged,[this](QColor color)->void{
        _host->setColor(QVector3D(color.red()/255.f,color.green()/255.f,color.blue()/255.f));
    });

    if(_host->getDiffuseTexture())
    {
        connect(ui->diffuseMapGpbx, &QGroupBox::toggled, _host->getDiffuseTexture().get(), &Texture::setEnabled);
        connect(ui->diffuseMapGpbx, &QGroupBox::toggled, ui->color, &MsnhFloatBox::setDisabled);
    }

    if(_host->getSpecularTexture())
    {
        connect(ui->specularMapGpbx, &QGroupBox::toggled, _host->getSpecularTexture().get(), &Texture::setEnabled);
        connect(ui->specularMapGpbx, &QGroupBox::toggled, ui->specular, &MsnhFloatBox::setDisabled);
    }

    if(_host->getBumpTexture())
    {
        connect(ui->bumpMapGpbx, &QGroupBox::toggled, _host->getBumpTexture().get(), &Texture::setEnabled);
        connect(ui->bumpMapGpbx, &QGroupBox::toggled, ui->bumpMap, &MsnhFloatBox::setDisabled);
        connect(_host->getBumpTexture().get(), &Texture::enabledChanged, ui->bumpMapGpbx, &QGroupBox::setChecked);
    }
}

void MsnhMaterialProperty::updateVals()
{

    ui->diffuseMapGpbx->hide();
    ui->specularMapGpbx->hide();
    ui->bumpMapGpbx->hide();

    ui->ambient->updateVal(_host->getAmbient());
    ui->diffuse->updateVal(_host->getDiffuse());
    ui->specular->updateVal(_host->getSpecular());
    ui->shininess->updateVal(_host->getShininess());

    ui->color->setColor(QColor(clampColor(_host->getColor().x()*255.f),_host->getColor().y()*255.f,_host->getColor().z()*255.f));
    ui->color->update();

    if(_host->getDiffuseTexture())
    {
        ui->diffuseMapGpbx->show();
        ui->diffuseWidth->setText(QString::number(_host->getDiffuseTexture().get()->getImage().width()));
        ui->diffuseHeight->setText(QString::number(_host->getDiffuseTexture().get()->getImage().height()));
        ui->diffuseMap->setPixmap(QPixmap::fromImage(_host->getDiffuseTexture().get()->getImage()));
        ui->diffuseMapGpbx->setChecked(_host->getDiffuseTexture().get()->getEnabled());

        if(_host->getDiffuseTexture().get()->getEnabled())
        {
            ui->color->setEnabled(false);
        }
        else
        {
            ui->color->setEnabled(true);
        }
    }
    else
    {
        ui->color->setEnabled(true);
    }

    if(_host->getSpecularTexture())
    {
        ui->specularMapGpbx->show();
        ui->specularWidth->setText(QString::number(_host->getSpecularTexture().get()->getImage().width()));
        ui->specularHeight->setText(QString::number(_host->getSpecularTexture().get()->getImage().height()));
        ui->specularMap->setPixmap(QPixmap::fromImage(_host->getSpecularTexture().get()->getImage()));
        ui->specularMapGpbx->setChecked(_host->getSpecularTexture().get()->getEnabled());
        if(_host->getSpecularTexture().get()->getEnabled())
        {
            ui->specular->setEnabled(false);
        }
        else
        {
            ui->specular->setEnabled(true);
        }

    }
    else
    {
        ui->specular->setEnabled(true);
    }

    if(_host->getBumpTexture())
    {
        ui->bumpMapGpbx->show();
        ui->bumpWidth->setText(QString::number(_host->getBumpTexture().get()->getImage().width()));
        ui->bumpHeight->setText(QString::number(_host->getBumpTexture().get()->getImage().height()));
        ui->bumpMap->setPixmap(QPixmap::fromImage(_host->getBumpTexture().get()->getImage()));
        ui->bumpMapGpbx->setChecked(_host->getBumpTexture().get()->getEnabled());
    }
}
