﻿#include <Gui/Properties/MsnhMeshProperty.h>
#include "ui_MsnhMeshProperty.h"

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif
MsnhMeshProperty::MsnhMeshProperty(Mesh *mesh, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhMeshProperty),
    _host(mesh)
{
    ui->setupUi(this);

    ui->alpha->setMinMax(0,1);

    ui->xPos->setMinMax(-inf,inf);
    ui->yPos->setMinMax(-inf,inf);
    ui->zPos->setMinMax(-inf,inf);

    ui->xPos->setEnabled(false);
    ui->yPos->setEnabled(false);
    ui->zPos->setEnabled(false);

    ui->xRot->setMinMax(-180,180);
    ui->yRot->setMinMax(-180,180);
    ui->zRot->setMinMax(-180,180);

    ui->xRot->setEnabled(false);
    ui->yRot->setEnabled(false);
    ui->zRot->setEnabled(false);

    ui->xScale->setMinMax(-inf,inf);
    ui->yScale->setMinMax(-inf,inf);
    ui->zScale->setMinMax(-inf,inf);

    ui->xScale->setEnabled(false);
    ui->yScale->setEnabled(false);
    ui->zScale->setEnabled(false);

    configSignals();
    updateVals();
}

MsnhMeshProperty::~MsnhMeshProperty()
{
    delete ui;
}

void MsnhMeshProperty::hostDestroyed(QObject *)
{
    delete this;
}

void MsnhMeshProperty::configSignals()
{
    connect(_host, &Mesh::destroyed, this, &MsnhMeshProperty::hostDestroyed);

    connect(ui->visibleCkbx,&QCheckBox::toggled,_host,&Mesh::setVisible);
    connect(ui->visibleCkbx, &QCheckBox::toggled, ui->paramsGpbx, &QGroupBox::setEnabled);

    connect(ui->wireFrameCkbx, &QCheckBox::toggled, _host, &Mesh::setWireFrameMode);
    connect(ui->alpha, &MsnhFloatBox::valueChanged, _host, &Mesh::setAlpha);

    connect(ui->xPos, &MsnhFloatBox::valueChanged, this, &MsnhMeshProperty::changePos);
    connect(ui->yPos, &MsnhFloatBox::valueChanged, this, &MsnhMeshProperty::changePos);
    connect(ui->zPos, &MsnhFloatBox::valueChanged, this, &MsnhMeshProperty::changePos);

    connect(ui->xRot, &MsnhFloatBox::valueChanged, this, &MsnhMeshProperty::changeRot);
    connect(ui->yRot, &MsnhFloatBox::valueChanged, this, &MsnhMeshProperty::changeRot);
    connect(ui->zRot, &MsnhFloatBox::valueChanged, this, &MsnhMeshProperty::changeRot);

    connect(ui->xScale, &MsnhFloatBox::valueChanged, this, &MsnhMeshProperty::changeScaling);
    connect(ui->yScale, &MsnhFloatBox::valueChanged, this, &MsnhMeshProperty::changeScaling);
    connect(ui->zScale, &MsnhFloatBox::valueChanged, this, &MsnhMeshProperty::changeScaling);

    connect(_host, &Mesh::positionChanged, this, &MsnhMeshProperty::updatePos);
    connect(_host, &Mesh::rotationChanged, this, &MsnhMeshProperty::updateRot);
    connect(_host, &Mesh::scalingChanged, this, &MsnhMeshProperty::updateScaling);
    connect(_host, &Mesh::alphaChanged, ui->alpha, &MsnhFloatBox::updateVal);
}

void MsnhMeshProperty::updateVals()
{
    if(_host->getMeshType() == Mesh::MeshType::POINT)
    {
        ui->meshType->setText("点");
        ui->facesNum->setText("0");
    }
    else if(_host->getMeshType() == Mesh::MeshType::LINE)
    {
        ui->meshType->setText("线");
        ui->facesNum->setText("0");
    }
    else if(_host->getMeshType() == Mesh::MeshType::TRIANGLE)
    {
        ui->meshType->setText("三角面");
        ui->facesNum->setText(QString::number(_host->getIndices().size()/3));
    }
    ui->vertexNum->setText(QString::number(_host->getIndices().size()));

    ui->alpha->updateVal(_host->getAlpha());

    ui->visibleCkbx->setChecked(_host->getVisible());
    ui->wireFrameCkbx->setChecked(_host->getWireFrameMode());

    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());

    ui->xRot->updateVal(_host->getRotationApi().x());
    ui->yRot->updateVal(_host->getRotationApi().y());
    ui->zRot->updateVal(_host->getRotationApi().z());

    ui->xScale->updateVal(_host->getScalingApi().x());
    ui->yScale->updateVal(_host->getScalingApi().y());
    ui->zScale->updateVal(_host->getScalingApi().z());
}

void MsnhMeshProperty::changePos(float)
{
    _host->setPositionApi(QVector3D(ui->xPos->getVal(), ui->yPos->getVal(), ui->zPos->getVal()));
}

void MsnhMeshProperty::changeRot(float)
{
    _host->setRotationApi(QVector3D(ui->xRot->getVal(), ui->yRot->getVal(), ui->zRot->getVal()));
}

void MsnhMeshProperty::changeScaling(float)
{
    _host->setScalingApi(QVector3D(ui->xScale->getVal(), ui->yScale->getVal(), ui->zScale->getVal()));
}

void MsnhMeshProperty::updatePos(QVector3D )
{

    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());
}

void MsnhMeshProperty::updateRot(QVector3D )
{
    ui->xRot->updateVal(_host->getRotationApi().x());
    ui->yRot->updateVal(_host->getRotationApi().y());
    ui->zRot->updateVal(_host->getRotationApi().z());
}

void MsnhMeshProperty::updateScaling(QVector3D)
{
    ui->xScale->updateVal(_host->getScalingApi().x());
    ui->yScale->updateVal(_host->getScalingApi().y());
    ui->zScale->updateVal(_host->getScalingApi().z());
}
