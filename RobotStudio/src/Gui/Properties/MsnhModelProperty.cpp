﻿#include <Gui/Properties/MsnhModelProperty.h>
#include "ui_MsnhModelProperty.h"

MsnhModelProperty::MsnhModelProperty(Model *model, QWidget *parent) :
    QDialog(parent),
    _host(model),
    ui(new Ui::MsnhModelProperty)
{
    ui->setupUi(this);

    ui->alpha->setMinMax(0,1);

    ui->xPos->setMinMax(-inf,inf);
    ui->yPos->setMinMax(-inf,inf);
    ui->zPos->setMinMax(-inf,inf);

    ui->xRot->setMinMax(-180,180);
    ui->yRot->setMinMax(-180,180);
    ui->zRot->setMinMax(-180,180);

    ui->xScale->setMinMax(-inf,inf);
    ui->yScale->setMinMax(-inf,inf);
    ui->zScale->setMinMax(-inf,inf);

    configSignals();
    updateVals();
}

MsnhModelProperty::~MsnhModelProperty()
{
    delete ui;
}

void MsnhModelProperty::hostDestroyed(QObject *)
{
    delete this;
}

void MsnhModelProperty::configSignals()
{
    connect(_host, &Mesh::destroyed, this, &MsnhModelProperty::hostDestroyed);

    connect(ui->visibleCkbx,&QCheckBox::toggled,_host,&Model::setVisible);
    connect(ui->visibleCkbx, &QCheckBox::toggled, ui->paramsGpbx, &QGroupBox::setEnabled);

    connect(ui->wireFrameCkbx, &QCheckBox::toggled, _host, &Model::setWireFrameMode);
    connect(ui->alpha, &MsnhFloatBox::valueChanged, _host, &Model::setAlpha);

    connect(ui->xPos, &MsnhFloatBox::valueChanged, this, &MsnhModelProperty::changePos);
    connect(ui->yPos, &MsnhFloatBox::valueChanged, this, &MsnhModelProperty::changePos);
    connect(ui->zPos, &MsnhFloatBox::valueChanged, this, &MsnhModelProperty::changePos);

    connect(ui->xRot, &MsnhFloatBox::valueChanged, this, &MsnhModelProperty::changeRot);
    connect(ui->yRot, &MsnhFloatBox::valueChanged, this, &MsnhModelProperty::changeRot);
    connect(ui->zRot, &MsnhFloatBox::valueChanged, this, &MsnhModelProperty::changeRot);

    connect(ui->xScale, &MsnhFloatBox::valueChanged, this, &MsnhModelProperty::changeScaling);
    connect(ui->yScale, &MsnhFloatBox::valueChanged, this, &MsnhModelProperty::changeScaling);
    connect(ui->zScale, &MsnhFloatBox::valueChanged, this, &MsnhModelProperty::changeScaling);

    connect(_host, &Mesh::positionChanged, this, &MsnhModelProperty::updatePos);
    connect(_host, &Mesh::rotationChanged, this, &MsnhModelProperty::updateRot);
    connect(_host, &Mesh::scalingChanged, this, &MsnhModelProperty::updateScaling);
    connect(_host, &Mesh::alphaChanged, ui->alpha, &MsnhFloatBox::updateVal);
}

void MsnhModelProperty::updateVals()
{

    ui->childMeshNum->setText(QString::number(_host->getChildMeshes().size()));
    ui->childModelNum->setText(QString::number(_host->getChildModels().size()));

    ui->alpha->updateVal(_host->getAlpha());

    ui->visibleCkbx->setChecked(_host->getVisible());
    ui->wireFrameCkbx->setChecked(_host->getWireFrameMode());

    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());

    ui->xRot->updateVal(_host->getRotationApi().x());
    ui->yRot->updateVal(_host->getRotationApi().y());
    ui->zRot->updateVal(_host->getRotationApi().z());

    ui->xScale->updateVal(_host->getScalingApi().x());
    ui->yScale->updateVal(_host->getScalingApi().y());
    ui->zScale->updateVal(_host->getScalingApi().z());

    ui->xPosGlobal->setText(QString::number(_host->getGlobalPositionApi().x()));
    ui->yPosGlobal->setText(QString::number(_host->getGlobalPositionApi().y()));
    ui->zPosGlobal->setText(QString::number(_host->getGlobalPositionApi().z()));

    ui->xRotGlobal->setText(QString::number(_host->getGlobalRotationApi().x()));
    ui->yRotGlobal->setText(QString::number(_host->getGlobalRotationApi().y()));
    ui->zRotGlobal->setText(QString::number(_host->getGlobalRotationApi().z()));
}

void MsnhModelProperty::changePos(float)
{
    _host->setPositionApi(QVector3D(ui->xPos->getVal(), ui->yPos->getVal(), ui->zPos->getVal()));
}

void MsnhModelProperty::changeRot(float)
{
    _host->setRotationApi(QVector3D(ui->xRot->getVal(), ui->yRot->getVal(), ui->zRot->getVal()));
}

void MsnhModelProperty::changeScaling(float)
{
    _host->setScalingApi(QVector3D(ui->xScale->getVal(), ui->yScale->getVal(), ui->zScale->getVal()));
}

void MsnhModelProperty::updatePos(QVector3D )
{

    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());

    ui->xPosGlobal->setText(QString::number(_host->getGlobalPositionApi().x()));
    ui->yPosGlobal->setText(QString::number(_host->getGlobalPositionApi().y()));
    ui->zPosGlobal->setText(QString::number(_host->getGlobalPositionApi().z()));
}

void MsnhModelProperty::updateRot(QVector3D )
{
    ui->xRot->updateVal(_host->getRotationApi().x());
    ui->yRot->updateVal(_host->getRotationApi().y());
    ui->zRot->updateVal(_host->getRotationApi().z());

    ui->xRotGlobal->setText(QString::number(_host->getGlobalRotationApi().x()));
    ui->yRotGlobal->setText(QString::number(_host->getGlobalRotationApi().y()));
    ui->zRotGlobal->setText(QString::number(_host->getGlobalRotationApi().z()));
}

void MsnhModelProperty::updateScaling(QVector3D)
{
    ui->xScale->updateVal(_host->getScalingApi().x());
    ui->yScale->updateVal(_host->getScalingApi().y());
    ui->zScale->updateVal(_host->getScalingApi().z());
}

