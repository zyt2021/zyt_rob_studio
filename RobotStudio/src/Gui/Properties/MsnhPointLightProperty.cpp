﻿#include <Gui/Properties/MsnhPointLightProperty.h>
#include "ui_MsnhPointLightProperty.h"

MsnhPointLightProperty::MsnhPointLightProperty(PointLight *light, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhPointLightProperty),
    _host(light)
{
    ui->setupUi(this);

    ui->intensity->setMinMax(0,100);
    ui->xPos->setMinMax(-inf,inf);
    ui->yPos->setMinMax(-inf,inf);
    ui->zPos->setMinMax(-inf,inf);

    ui->quadratic->setMinMax(0, inf);
    ui->linear->setMinMax(0, inf);
    ui->constant->setMinMax(0, inf);

    configSignals();
    updateVals();
}

MsnhPointLightProperty::~MsnhPointLightProperty()
{
    delete ui;
}

void MsnhPointLightProperty::configSignals()
{
    connect(_host, &Mesh::destroyed, this, &MsnhPointLightProperty::hostDestroyed);

    connect(ui->enableCkbx, &QCheckBox::toggled, _host, &PointLight::setEnabled);
    connect(ui->enableCkbx, &QCheckBox::toggled, ui->paramsGpbx, &QGroupBox::setEnabled);

    connect(ui->color, &ColorMaster::colorChanged,[this](QColor color)->void{
        _host->setColor(QVector3D(color.red()/255.f,color.green()/255.f,color.blue()/255.f));
    });

    connect(ui->intensity, &MsnhFloatBox::valueChanged, _host, &PointLight::setIntensity);

    connect(ui->xPos, &MsnhFloatBox::valueChanged, this, &MsnhPointLightProperty::changePos);
    connect(ui->yPos, &MsnhFloatBox::valueChanged, this, &MsnhPointLightProperty::changePos);
    connect(ui->zPos, &MsnhFloatBox::valueChanged, this, &MsnhPointLightProperty::changePos);

    connect(ui->attentuationGpbx, &QGroupBox::toggled, _host, &PointLight::setEnableAttenuation);
    connect(ui->quadratic, &MsnhFloatBox::valueChanged, _host, &PointLight::setAttenuationQuadratic);
    connect(ui->linear, &MsnhFloatBox::valueChanged, _host, &PointLight::setAttenuationLinear);
    connect(ui->constant, &MsnhFloatBox::valueChanged, _host, &PointLight::setAttenuationConstant);
    connect(ui->visiableCkbx, &QCheckBox::toggled, _host, &PointLight::setVisible);


    connect(_host, &PointLight::positionChanged, this, &MsnhPointLightProperty::updatePos);
}

void MsnhPointLightProperty::updateVals()
{
    ui->enableCkbx->setChecked(_host->getEnabled());
    ui->paramsGpbx->setChecked(_host->getEnabled());

    ui->visiableCkbx->setChecked(_host->getVisible());

    ui->color->setColor(QColor(clampColor(_host->getColor().x()*255.f),_host->getColor().y()*255.f,_host->getColor().z()*255.f));
    ui->color->update();

    ui->intensity->updateVal(_host->getIntensity());

    ui->attentuationGpbx->setChecked(_host->getEnableAttenuation());
    ui->quadratic->updateVal(_host->getAttenuationQuadratic());
    ui->linear->updateVal(_host->getAttenuationLinear());
    ui->constant->updateVal(_host->getAttenuationConstant());

    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());
}

void MsnhPointLightProperty::changePos(float)
{
    _host->setPositionApi(QVector3D(ui->xPos->getVal(), ui->yPos->getVal(), ui->zPos->getVal()));
}

void MsnhPointLightProperty::updatePos(QVector3D)
{
    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());
}

void MsnhPointLightProperty::hostDestroyed()
{
    delete this;
}
