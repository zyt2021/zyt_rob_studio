﻿#include <Gui/Properties/MsnhPointsCloudProperty.h>
#include "ui_MsnhPointsCloudProperty.h"

MsnhPointsCloudProperty::MsnhPointsCloudProperty(PointsCloud *pointsCloud, QWidget *parent) :
    QDialog(parent),
    _host(pointsCloud),
    ui(new Ui::MsnhPointsCloudProperty)
{
    ui->setupUi(this);

    configSignals();
    updateVals();
}

MsnhPointsCloudProperty::~MsnhPointsCloudProperty()
{
    delete ui;
}

void MsnhPointsCloudProperty::configSignals()
{
    connect(ui->visiableCkbx, &QCheckBox::toggled, _host, &PointsCloud::setVisible);
    connect(ui->pureColorCkbx, &QCheckBox::toggled, _host, &PointsCloud::setPureColor);
    connect(ui->pureColorCkbx, &QCheckBox::toggled, ui->color, &ColorMaster::setEnabled);
    connect(ui->color, &ColorMaster::colorChanged,[this](QColor color)->void{
        _host->setColor(QVector3D(color.red()/255.f,color.green()/255.f,color.blue()/255.f));
    });
}

void MsnhPointsCloudProperty::updateVals()
{
    ui->subAddrTxt->setText(_host->getSubAddr());
    ui->visiableCkbx->setChecked(_host->getVisible());
    ui->pureColorCkbx->setChecked(_host->isPureColor());
    ui->color->setColor(QColor(clampColor(_host->getColor().x()*255.f),_host->getColor().y()*255.f,_host->getColor().z()*255.f));
    ui->color->update();
}

void MsnhPointsCloudProperty::hostDestroyed()
{
    delete this;
}
