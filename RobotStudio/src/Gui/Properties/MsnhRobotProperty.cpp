﻿#include <Gui/Properties/MsnhRobotProperty.h>
#include "ui_MsnhRobotProperty.h"

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhRobotProperty::MsnhRobotProperty(Robot *robot,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhRobotProperty),
    _host(robot)
{
    ui->setupUi(this);

    _vLayout = new QVBoxLayout(ui->jointTab);

    // 坐标轴
    ui->axesVisiableCkbx->setChecked(_host->getShowAxes());

    ui->endAxesVisiableCkbx->setChecked(_host->getShowEndAxes());

    ui->axesScale->setPrecision(0.001f);

    ui->axesScale->setVal(_host->getAxesScale());

    connect(ui->axesVisiableCkbx,&QCheckBox::toggled,_host,&Robot::setShowAxes);

    connect(ui->endAxesVisiableCkbx,&QCheckBox::toggled,_host,&Robot::setShowEndAxes);

    connect(ui->axesScale, &MsnhFloatBox::valueChanged,_host,&Robot::setAxesScale);

    // ===

    // 末端球
    ui->tfBallScale->setPrecision(0.001f);

    ui->tfBallScale->setVal(_host->getTfToolScale());

    connect(ui->tfBallScale, &MsnhFloatBox::valueChanged,_host,&Robot::setTfToolScale);
    // ===

    // 路径坐标轴缩放
    ui->routePointsScale->setPrecision(0.001f);

    ui->routePointsScale->setVal(_host->getRouteAxesScale());

    connect(ui->routePointsScale, &MsnhFloatBox::valueChanged,_host,&Robot::setRouteAxesScale);
    // ===

    // 模型可视化
    ui->visualRobVisiableCkbx->setChecked(_host->getVisualTree()->getVisible());
    ui->animationRobVisiableCkbx->setChecked(_host->getAnimationTree()->getVisible());
    ui->planRobVisiableCkbx->setChecked(_host->getEndTree()->getVisible());
    ui->collisionRobVisiableCkbx->setChecked(_host->getCollisionTree()->getVisible());

    connect(ui->visualRobVisiableCkbx, &QCheckBox::toggled, _host->getVisualTree(), &Model::setVisible);
    connect(ui->animationRobVisiableCkbx, &QCheckBox::toggled, _host->getAnimationTree(), &Model::setVisible);
    connect(ui->planRobVisiableCkbx, &QCheckBox::toggled, _host->getEndTree(), &Model::setVisible);
    connect(ui->collisionRobVisiableCkbx, &QCheckBox::toggled, _host->getCollisionTree(), &Model::setVisible);
    // ===

    ui->robotTreeWidget->setColumnCount( 1 );

    ui->robotTreeWidget->setAnimated(true);

    ui->robotTreeWidget->header()->setSectionResizeMode(QHeaderView::ResizeToContents); //水平滚动调

    ui->robotTreeWidget->header()->setStretchLastSection(false);

    ui->robotTreeWidget->setHeaderLabel("关节树");

    auto begin = _host->getVisualJoints().begin();

    QStringList jointsName;

    while (begin != _host->getVisualJoints().end())
    {
        auto joint = begin.value();
        if(joint->getJointType()!=Msnhnet::JOINT_FIXED)
        {
            MsnhFloatWidget * floatW = new MsnhFloatWidget(ui->jointTab);
            floatW->setName(begin.key());
            floatW->setMax(joint->getJointMax(), true);
            floatW->setMin(joint->getJointMin(), true);
            floatW->setVal(joint->getSingleMove(), false);
            _vLayout->addWidget(floatW);

            connect(floatW, &MsnhFloatWidget::valueChangedWithName, _host, &Robot::singleJointMove);
            connect(joint,&Model::singleMoved, floatW, &MsnhFloatWidget::updateVal);

            MsnhPlotPosVecAcc *plot = new MsnhPlotPosVecAcc(PLOT_SINGLE,this);
            plot->setWindowTitle(begin.key());

            _JointsWidget[begin.key()] = floatW;

            jointsName.push_front(begin.key());
        }
        begin++;
    }

    ui->jointsCurveNameCbx->clear();
    ui->jointsCurveNameCbx->addItems(jointsName);

    _vLayout->addItem(new QSpacerItem(20, 1000, QSizePolicy::Minimum, QSizePolicy::Expanding));

    std::shared_ptr<StringTree> strTree = _host->getStrTree();

    QTreeWidgetItem * item = new QTreeWidgetItem(QStringList()<<_host->getRobotName());

    ui->robotTreeWidget->addTopLevelItem(item);

    loadStrTree(strTree,item);

    ui->robotTreeWidget->expandAll();

    updateTfTaken();

    _chainGroupVLayout = new QVBoxLayout(ui->chainGroupTab);
    _chainGroupVLayout->setContentsMargins(0, -1, 0, -1);
    _chainGroupVLayout->addItem(new QSpacerItem(20, 1000, QSizePolicy::Minimum, QSizePolicy::Expanding));

    _planVLayout = new QVBoxLayout(ui->planTab);
    _planVLayout->setContentsMargins(0, -1, 0, -1);
    _planVLayout->addItem(new QSpacerItem(20, 1000, QSizePolicy::Minimum, QSizePolicy::Expanding));

    connect(robot, &Robot::chainAdded, this, &MsnhRobotProperty::addCartesian);
    connect(robot, &Robot::chainAdded, this, &MsnhRobotProperty::addPlan);
    connect(robot, &Robot::updateFrame, this, &MsnhRobotProperty::updateFrame);
    connect(robot, &Robot::pathPlanned, this, &MsnhRobotProperty::pathPlanned);
    connect(robot, &Robot::updateStep, this, &MsnhRobotProperty::updateStep);
    connect(robot, &Robot::refreshPoints, this, &MsnhRobotProperty::refreshWayPoints);

    if(robot->getEnableRemoteJoints())
    {
        ui->remoteJointsCkbx->setChecked(true);
    }
    else
    {
        ui->remoteJointsCkbx->setChecked(false);
    }

    //Add cart widget, 通过每个tf的最终Frame, 来重新构建属性面板
    auto chains = robot->getLastTfFrame();
    auto chainBg = chains.begin();
    while(chainBg!=chains.end())
    {
        addCartesian(chainBg.key(), chainBg.value());
        addPlan(chainBg.key(), chainBg.value());
        chainBg++;
    }

    ui->jointSubAddrTxt->setText(robot->getJointsCtrlAddr());

    ui->realTimeCurvesCkbx->setChecked(_host->getUpdateCurvesRealTime());

    ui->linksPairListWidget->clear();
    auto pairs = robot->getLinkNamePairs();
    auto iterPairs = pairs.begin();
    int linkPairCnt = 0;

    //从robot里获取碰撞对
    while(iterPairs != pairs.end())
    {
        ui->linksPairListWidget->addItem(iterPairs.key());
        if(iterPairs.value())
            ui->linksPairListWidget->item(linkPairCnt)->setCheckState(Qt::CheckState::Checked);
        else
            ui->linksPairListWidget->item(linkPairCnt)->setCheckState(Qt::CheckState::Unchecked);
        linkPairCnt++;
        iterPairs++;
    }

    ui->enableCollisionDetCkbx->setChecked(_host->getEnableCollisionDetection());
    ui->enableBodySelectCkbx->setChecked(_host->getBodySelectable());
}

MsnhRobotProperty::~MsnhRobotProperty()
{
    delete ui;
}

void MsnhRobotProperty::loadStrTree(const std::shared_ptr<StringTree> &strTree, QTreeWidgetItem *parent)
{
    if(strTree->name == "WorldFix")
    {
        for (int i = 0; i < strTree->children.size(); ++i)
        {
            loadStrTree(strTree->children[i],parent);
        }
    }
    else
    {
        QTreeWidgetItem * item = new QTreeWidgetItem( QStringList()<<QString::fromStdString(strTree->name));

        parent->addChild(item);

        for (int i = 0; i < strTree->children.size(); ++i)
        {
            loadStrTree(strTree->children[i],item);
        }
    }
}

void MsnhRobotProperty::on_setStartJointBtn_clicked()
{
    if(!ui->robotTreeWidget->currentItem())
    {
        QMessageBox::warning(this,"警告","请选择一项再继续!");
        return;
    }

    if(ui->robotTreeWidget->currentItem()->text(0) == _host->getRobotName())
    {
        QMessageBox::warning(this,"警告","此选项为机器人名称,不可作为起始关节");
        return;
    }

    ui->startJointTxt->setText(ui->robotTreeWidget->currentItem()->text(0));
}


void MsnhRobotProperty::on_setStopJointBtn_clicked()
{
    if(!ui->robotTreeWidget->currentItem())
    {
        QMessageBox::warning(this,"警告","请选择一项再继续!");
        return;
    }


    if(ui->robotTreeWidget->currentItem()->text(0) == _host->getRobotName())
    {
        QMessageBox::warning(this,"警告","此选项为机器人名称,不可作为结束关节");
        return;
    }
    ui->stopJointTxt->setText(ui->robotTreeWidget->currentItem()->text(0));
}


void MsnhRobotProperty::on_addPlanBtn_clicked()
{
    if(ui->startJointTxt->text()=="" || ui->stopJointTxt->text()=="")
    {
        QMessageBox::warning(this,"警告","起始或终止关节不能为空");
        return;
    }

    try
    {
        _host->addChain(ui->startJointTxt->text(),ui->stopJointTxt->text(),ui->tfCbx->currentText());
    }
    catch (Msnhnet::Exception ex)
    {
        QMessageBox::warning(this,"警告",QString::fromStdString(ex.what()));
    }

    //更新剩下的tf, 默认32个
    updateTfTaken();

}

void MsnhRobotProperty::updateTfTaken()
{
    ui->tfCbx->clear();
    ui->cartsCurveNameCbx->clear();

    auto tfTaken = _host->getTfTaken();

    auto bg = tfTaken.begin();

    while(bg!=tfTaken.end())
    {
        if(!bg.value())
        {
            ui->tfCbx->addItem(bg.key());
        }

        if(bg.value())
        {
            ui->cartsCurveNameCbx->addItem(bg.key());
        }
        bg++;
    }
}

void MsnhRobotProperty::doneExecution(const QString &name)
{
    _planTools[name]->executionDone();
}

void MsnhRobotProperty::addCartesian(const QString &name, const Msnhnet::Frame &frame)
{
    MsnhCartesianWidget* cartTool = new MsnhCartesianWidget(name);
    connect(cartTool, &MsnhCartesianWidget::changePosAngle, this, &MsnhRobotProperty::updateTF);
    _chainGroupVLayout->insertWidget(_chainGroupVLayout->count()-1,cartTool);
    cartTool->updateFrame(frame);
    _cartTools[name] = cartTool;
}

void MsnhRobotProperty::addPlan(const QString &name, const Msnhnet::Frame &frame)
{

    MsnhPlanWidget* planTool = new MsnhPlanWidget(name);
    connect(planTool, &MsnhPlanWidget::plan, this, &MsnhRobotProperty::plan);
    connect(planTool, &MsnhPlanWidget::runPlan, this, &MsnhRobotProperty::runPlan);
    connect(planTool, &MsnhPlanWidget::animatePlan, this, &MsnhRobotProperty::animatePlan);
    connect(planTool, &MsnhPlanWidget::addWayPoint, this, &MsnhRobotProperty::addWayPoint);
    connect(planTool, &MsnhPlanWidget::insertWayPoint, this, &MsnhRobotProperty::insertWayPoint);
    connect(planTool, &MsnhPlanWidget::deleteWayPoint, this, &MsnhRobotProperty::deleteWayPoint);
    connect(planTool, &MsnhPlanWidget::clearWayPoints, this, &MsnhRobotProperty::clearWayPoints);
    connect(planTool, &MsnhPlanWidget::endEffortToSelect, this, &MsnhRobotProperty::endEffortToSelect);
    connect(planTool, &MsnhPlanWidget::updateTfPlanParams, this, &MsnhRobotProperty::updateTfPlanParams);
    connect(planTool, &MsnhPlanWidget::gotoZero, this, &MsnhRobotProperty::gotoZero);
    connect(planTool, &MsnhPlanWidget::syncRobot, this, &MsnhRobotProperty::syncRobot);
    connect(planTool, &MsnhPlanWidget::changeEndEffort, this, &MsnhRobotProperty::changeEndEffort);

    if(!_host->getPathPoints()[name].empty())
    {
        planTool->refreshWayPoints(_host->getPathPoints()[name]);
    }

    planTool->refreshPlanParams(_host->getTfPlaneParams()[name]);

    //获取可能已经规划的路径最大step
    planTool->setMaxStep(_host->getPlannedJointsSize(name));
    planTool->setPublishAddr(_host->getPublishAddr(name));
    planTool->updateFrame(frame,true);

    _planVLayout->insertWidget(_planVLayout->count()-1, planTool);
    _planTools[name] = planTool;
}

void MsnhRobotProperty::updateFrame(const QString &name, const Msnhnet::Frame &frame)
{
    _cartTools[name]->updateFrame(frame);
    _planTools[name]->updateFrame(frame);
}

void MsnhRobotProperty::updateTF(const QString &name, bool isRot, const QVector3D &val)
{
    if(isRot)
    {
        _host->getTfModels()[name]->setRotationApi(val);
    }
    else
    {
        _host->getTfModels()[name]->setPositionApi(val);
    }
}

void MsnhRobotProperty::plan(const QString &tfName, float vel, float acc, float dt, float radius, float maxFailedRate, Msnhnet::RotationInterpType rotInterpType, Msnhnet::PlanType planType, bool goHome, bool fullCircle)
{
    QString msg;
    if(!_host->plan(tfName, vel, acc, dt, radius, maxFailedRate, rotInterpType, planType, goHome, fullCircle, msg))
    {
        QMessageBox::warning(this,"警告",msg);
    }
}

void MsnhRobotProperty::runPlan(const QString &tfName)
{
    QString msg;
    if(!_host->runPlan(tfName,msg))
    {
        QMessageBox::warning(this,"警告",msg);
    }
}

void MsnhRobotProperty::animatePlan(const QString &tfName, bool run)
{
    QString msg;
    if(!_host->animatePlan(tfName,run,msg))
    {
        _planTools[tfName]->setAnimated(false);
        QMessageBox::warning(this,"警告",msg);
    }
}

void MsnhRobotProperty::pathPlanned(const QString &tfName, int maxStep)
{
    _planTools[tfName]->setMaxStep(maxStep);
}

void MsnhRobotProperty::updateStep(const QString &tfName, int step)
{
    _planTools[tfName]->setStep(step);
}

void MsnhRobotProperty::addWayPoint(const QString &tfName)
{
    _host->addWayPoint(tfName);
}

void MsnhRobotProperty::insertWayPoint(const QString &tfName, int index)
{
    _host->insertWayPoint(tfName, index);
}

void MsnhRobotProperty::deleteWayPoint(const QString &tfName, int index)
{
    _host->deleteWayPoint(tfName, index);
}

void MsnhRobotProperty::clearWayPoints(const QString &tfName)
{
    _host->clearWayPoints(tfName);
}

void MsnhRobotProperty::gotoZero(const QString &tfName)
{
    _host->gotoZero(tfName);
}

void MsnhRobotProperty::syncRobot(const QString &tfName)
{
    _host->syncRobot(tfName);
}

void MsnhRobotProperty::endEffortToSelect(const QString &tfName, int index)
{
    _host->endEffortToSelected(tfName, index);
}

void MsnhRobotProperty::changeEndEffort(const QString &tfName, float x, float y, float z, float rx, float ry, float rz)
{
    _host->changeEndEffort(tfName, x, y, z, rx, ry,rz);
}

void MsnhRobotProperty::refreshWayPoints(const QString& tfName, const QVector<Msnhnet::Frame> &frames)
{
    _planTools[tfName]->refreshWayPoints(frames);
}

void MsnhRobotProperty::updateTfPlanParams(const QString &tfName, const TfPlanParams &tfPlanParams)
{
    _host->getTfPlaneParams()[tfName] = tfPlanParams;
}


void MsnhRobotProperty::on_remoteJointsCkbx_clicked()
{
    _host->setEnableRemoteJoints(ui->remoteJointsCkbx->isChecked());
}

void MsnhRobotProperty::on_showJointCurveBtn_clicked()
{
    _host->showPlot(ui->jointsCurveNameCbx->currentText());
}


void MsnhRobotProperty::on_showCartCurveBtn_clicked()
{
    _host->showPlot(ui->cartsCurveNameCbx->currentText());
}


void MsnhRobotProperty::on_realTimeCurvesCkbx_clicked()
{
    if(ui->realTimeCurvesCkbx->isChecked())
    {
        _host->setUpdateCurvesRealTime(true);
    }
    else
    {
        _host->setUpdateCurvesRealTime(false);
    }
}

void MsnhRobotProperty::on_updateCollisionPairBtn_clicked()
{
    for (int i = 0; i < ui->linksPairListWidget->count(); ++i)
    {
        if(ui->linksPairListWidget->item(i)->checkState() != Qt::CheckState::Checked)
        {
            _host->getLinkNamePairsChg()[ui->linksPairListWidget->item(i)->text()] = false;
        }
        else
        {
            _host->getLinkNamePairsChg()[ui->linksPairListWidget->item(i)->text()] = true;
        }
    }

    _host->updateCollisionPair();
}

void MsnhRobotProperty::on_linkPairsRegExpTxt_textChanged(const QString &arg1)
{
    for(int i = 0; i < ui->linksPairListWidget->count(); i++)
    {
        bool passCondition = ui->linksPairListWidget->item(i)->text().contains(arg1, Qt::CaseInsensitive);//不区分大小写

        if(passCondition)
            ui->linksPairListWidget->item(i)->setHidden(false);
        else
            ui->linksPairListWidget->item(i)->setHidden(true);
    }
}

void MsnhRobotProperty::on_linksPairListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QString str = item->text();
    QStringList joints = str.split(",");
    _host->selectVisual(QString(joints[1]));
}

void MsnhRobotProperty::on_linksPairListWidget_itemClicked(QListWidgetItem *item)
{
    QString str = item->text();
    QStringList joints = str.split(",");

    _host->selectVisual(QString(joints[0]));
}

void MsnhRobotProperty::on_enableCollisionDetCkbx_clicked()
{
    _host->setEnableCollisionDetection(ui->enableCollisionDetCkbx->isChecked());
}

void MsnhRobotProperty::on_enableBodySelectCkbx_clicked()
{
    _host->enableSelect(ui->enableBodySelectCkbx->isChecked());
}

void MsnhRobotProperty::on_setMoveDelay_clicked()
{
    _host->setExcutionTimer(ui->moveDelaySpBx->value());
}
