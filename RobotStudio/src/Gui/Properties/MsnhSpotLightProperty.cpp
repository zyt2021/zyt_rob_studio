﻿#include <Gui/Properties/MsnhSpotLightProperty.h>
#include "ui_MsnhSpotLightProperty.h"

MsnhSpotLightProperty::MsnhSpotLightProperty(SpotLight *light, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhSpotLightProperty),
    _host(light)
{
    ui->setupUi(this);

    ui->intensity->setMinMax(0,100);
    ui->xPos->setMinMax(-inf,inf);
    ui->yPos->setMinMax(-inf,inf);
    ui->zPos->setMinMax(-inf,inf);

    ui->xDirection->setMinMax(-inf,inf);
    ui->yDirection->setMinMax(-inf,inf);
    ui->zDirection->setMinMax(-inf,inf);

    ui->innerCutOff->setMinMax(0,180);
    ui->outerCutoff->setMinMax(0,180);
    ui->quadratic->setMinMax(0, inf);
    ui->linear->setMinMax(0, inf);
    ui->constant->setMinMax(0, inf);

    configSignals();
    updateVals();

}

MsnhSpotLightProperty::~MsnhSpotLightProperty()
{
    delete ui;
}

void MsnhSpotLightProperty::configSignals()
{
    connect(_host, &SpotLight::destroyed, this, &MsnhSpotLightProperty::hostDestroyed);

    connect(ui->enableCkbx, &QCheckBox::toggled, _host, &SpotLight::setEnabled);
    connect(ui->enableCkbx, &QCheckBox::toggled, ui->paramsGpbx, &QGroupBox::setEnabled);

    connect(ui->color, &ColorMaster::colorChanged,[this](QColor color)->void{
        _host->setColor(QVector3D(color.red()/255.f,color.green()/255.f,color.blue()/255.f));
    });

    connect(ui->intensity, &MsnhFloatBox::valueChanged, _host, &SpotLight::setIntensity);

    connect(ui->xPos, &MsnhFloatBox::valueChanged, this, &MsnhSpotLightProperty::changePos);
    connect(ui->yPos, &MsnhFloatBox::valueChanged, this, &MsnhSpotLightProperty::changePos);
    connect(ui->zPos, &MsnhFloatBox::valueChanged, this, &MsnhSpotLightProperty::changePos);

    connect(ui->attentuationGpbx, &QGroupBox::toggled, _host, &SpotLight::setEnableAttenuation);
    connect(ui->quadratic, &MsnhFloatBox::valueChanged, _host, &SpotLight::setAttenuationQuadratic);
    connect(ui->linear, &MsnhFloatBox::valueChanged, _host, &SpotLight::setAttenuationLinear);
    connect(ui->constant, &MsnhFloatBox::valueChanged, _host, &SpotLight::setAttenuationConstant);

    connect(ui->innerCutOff, &MsnhFloatBox::valueChanged, _host, &SpotLight::setInnerCutOff);
    connect(ui->outerCutoff, &MsnhFloatBox::valueChanged, _host, &SpotLight::setOuterCutOff);
    connect(ui->visiableCkbx, &QCheckBox::toggled, _host, &SpotLight::setVisible);


    connect(_host, &SpotLight::positionChanged, this, &MsnhSpotLightProperty::updatePos);
    connect(_host, &SpotLight::directionChanged, this, &MsnhSpotLightProperty::updateDirection);
}

void MsnhSpotLightProperty::updateVals()
{
    ui->enableCkbx->setChecked(_host->getEnabled());
    ui->paramsGpbx->setEnabled(_host->getEnabled());

    ui->visiableCkbx->setChecked(_host->getVisible());

    ui->color->setColor(QColor(clampColor(_host->getColor().x()*255.f),_host->getColor().y()*255.f,_host->getColor().z()*255.f));
    ui->color->update();

    ui->innerCutOff->updateVal(_host->getInnerCutOff());
    ui->outerCutoff->updateVal(_host->getOuterCutOff());

    ui->intensity->updateVal(_host->getIntensity());

    ui->attentuationGpbx->setChecked(_host->getEnableAttenuation());
    ui->quadratic->updateVal(_host->getAttenuationQuadratic());
    ui->linear->updateVal(_host->getAttenuationLinear());
    ui->constant->updateVal(_host->getAttenuationConstant());

    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());

    ui->xDirection->updateVal(_host->getDirectionApi().x());
    ui->yDirection->updateVal(_host->getDirectionApi().y());
    ui->zDirection->updateVal(_host->getDirectionApi().z());

}

void MsnhSpotLightProperty::changePos(float)
{
    _host->setPositionApi(QVector3D(ui->xPos->getVal(), ui->yPos->getVal(), ui->zPos->getVal()));
}

void MsnhSpotLightProperty::updatePos(QVector3D)
{
    ui->xPos->updateVal(_host->getPositionApi().x());
    ui->yPos->updateVal(_host->getPositionApi().y());
    ui->zPos->updateVal(_host->getPositionApi().z());
}

void MsnhSpotLightProperty::changeDirection(float)
{
    _host->setDirectionApi(QVector3D(ui->xDirection->getVal(), ui->yDirection->getVal(), ui->zDirection->getVal()));
}

void MsnhSpotLightProperty::updateDirection(QVector3D)
{
    ui->xDirection->updateVal(_host->getDirectionApi().x());
    ui->yDirection->updateVal(_host->getDirectionApi().y());
    ui->zDirection->updateVal(_host->getDirectionApi().z());
}

void MsnhSpotLightProperty::hostDestroyed()
{
    delete this;
}
