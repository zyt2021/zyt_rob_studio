#include <Gui/Properties/MsnhTransformProperty.h>
#include "ui_MsnhTransformProperty.h"

MsnhTransformProperty::MsnhTransformProperty(Transform* tf, QWidget *parent) :
    QDialog(parent),
    _host(tf),
    ui(new Ui::MsnhTransformProperty)
{
    ui->setupUi(this);
    ui->xPos->setMinMax(-inf,inf);
    ui->yPos->setMinMax(-inf,inf);
    ui->zPos->setMinMax(-inf,inf);

    ui->xRot->setMinMax(-180,180);
    ui->yRot->setMinMax(-180,180);
    ui->zRot->setMinMax(-180,180);

    ui->xScale->setMinMax(-inf,inf);
    ui->yScale->setMinMax(-inf,inf);
    ui->zScale->setMinMax(-inf,inf);

    updateVals();
    configSignals();
}

MsnhTransformProperty::~MsnhTransformProperty()
{
    delete ui;
}

void MsnhTransformProperty::hostDestroyed(QObject *)
{
    delete this;
}

void MsnhTransformProperty::on_visibleCkbx_clicked()
{
    _host->getTfModel()->setVisible(ui->visibleCkbx->isChecked());
}

void MsnhTransformProperty::updateVals()
{
    ui->visibleCkbx->setChecked(_host->getTfModel()->getVisible());
    auto pos = _host->getTfModel()->getGlobalPositionApi();
    auto rot = _host->getTfModel()->getGlobalRotationApi();
    auto scale = _host->getTfModel()->getScalingApi();

    ui->xPos->setVal(pos.x());
    ui->yPos->setVal(pos.y());
    ui->zPos->setVal(pos.z());

    ui->xRot->setVal(rot.x());
    ui->yRot->setVal(rot.y());
    ui->zRot->setVal(rot.z());

    ui->xScale->setVal(scale.x());
    ui->yScale->setVal(scale.y());
    ui->zScale->setVal(scale.z());

}

void MsnhTransformProperty::configSignals()
{
    connect(_host, &Mesh::destroyed, this, &MsnhTransformProperty::hostDestroyed);

    connect(ui->visibleCkbx,&QCheckBox::toggled,_host->getTfModel(),&Model::setVisible);
    connect(ui->visibleCkbx, &QCheckBox::toggled, ui->paramsGpbx, &QGroupBox::setEnabled);

    connect(ui->xPos, &MsnhFloatBox::valueChanged, this, &MsnhTransformProperty::changePos);
    connect(ui->yPos, &MsnhFloatBox::valueChanged, this, &MsnhTransformProperty::changePos);
    connect(ui->zPos, &MsnhFloatBox::valueChanged, this, &MsnhTransformProperty::changePos);

    connect(ui->xRot, &MsnhFloatBox::valueChanged, this, &MsnhTransformProperty::changeRot);
    connect(ui->yRot, &MsnhFloatBox::valueChanged, this, &MsnhTransformProperty::changeRot);
    connect(ui->zRot, &MsnhFloatBox::valueChanged, this, &MsnhTransformProperty::changeRot);

    connect(ui->xScale, &MsnhFloatBox::valueChanged, this, &MsnhTransformProperty::changeScaling);
    connect(ui->yScale, &MsnhFloatBox::valueChanged, this, &MsnhTransformProperty::changeScaling);
    connect(ui->zScale, &MsnhFloatBox::valueChanged, this, &MsnhTransformProperty::changeScaling);

    connect(_host->getTfModel(), &Model::positionChanged, this, &MsnhTransformProperty::updatePos);
    connect(_host->getTfModel(), &Model::rotationChanged, this, &MsnhTransformProperty::updateRot);
    connect(_host->getTfModel(), &Model::scalingChanged, this, &MsnhTransformProperty::updateScaling);
}

void MsnhTransformProperty::changePos(float)
{
    _host->getTfModel()->setPositionApi(QVector3D(ui->xPos->getVal(), ui->yPos->getVal(), ui->zPos->getVal()));
}

void MsnhTransformProperty::changeRot(float)
{
    _host->getTfModel()->setRotationApi(QVector3D(ui->xRot->getVal(), ui->yRot->getVal(), ui->zRot->getVal()));
}

void MsnhTransformProperty::changeScaling(float)
{
    _host->getTfModel()->setScalingApi(QVector3D(ui->xScale->getVal(), ui->yScale->getVal(), ui->zScale->getVal()));
}

void MsnhTransformProperty::updatePos(QVector3D )
{

    ui->xPos->updateVal(_host->getTfModel()->getPositionApi().x());
    ui->yPos->updateVal(_host->getTfModel()->getPositionApi().y());
    ui->zPos->updateVal(_host->getTfModel()->getPositionApi().z());
}

void MsnhTransformProperty::updateRot(QVector3D )
{
    ui->xRot->updateVal(_host->getTfModel()->getRotationApi().x());
    ui->yRot->updateVal(_host->getTfModel()->getRotationApi().y());
    ui->zRot->updateVal(_host->getTfModel()->getRotationApi().z());
}

void MsnhTransformProperty::updateScaling(QVector3D)
{
    ui->xScale->updateVal(_host->getTfModel()->getScalingApi().x());
    ui->yScale->updateVal(_host->getTfModel()->getScalingApi().y());
    ui->zScale->updateVal(_host->getTfModel()->getScalingApi().z());
}
