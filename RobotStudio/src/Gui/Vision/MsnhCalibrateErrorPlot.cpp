﻿#include "Gui/Vision/MsnhCalibrateErrorPlot.h"
#include "ui_MsnhCalibrateErrorPlot.h"
#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif
MsnhCalibrateErrorPlot::MsnhCalibrateErrorPlot(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhCalibrateErrorPlot)
{
    ui->setupUi(this);

    QBrush brush(QColor(25,25,25));
    ui->plot->legend->setVisible(true);
    ui->plot->legend->setBrush(brush);
    ui->plot->legend->setTextColor(QColor(240,240,240));

    ui->plot->setBackground(brush);
    ui->plot->xAxis->setBasePen(QPen(Qt::white, 1));
    ui->plot->yAxis->setBasePen(QPen(Qt::white, 1));
    ui->plot->xAxis->setTickPen(QPen(Qt::white, 1));
    ui->plot->yAxis->setTickPen(QPen(Qt::white, 1));
    ui->plot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->plot->yAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->plot->xAxis->setTickLabelColor(Qt::white);
    ui->plot->yAxis->setTickLabelColor(Qt::white);
    ui->plot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->plot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->plot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->plot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->plot->xAxis->grid()->setSubGridVisible(true);
    ui->plot->yAxis->grid()->setSubGridVisible(true);
    ui->plot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->plot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->plot->xAxis->setLabelColor(QColor(255,255,255));
    ui->plot->yAxis->setLabelColor(QColor(255,255,255));

    _singleError = new QCPBars(ui->plot->xAxis, ui->plot->yAxis);
    _singleError->setAntialiased(false);
    _singleError->setStackingGap(1);

    _singleError->setName("单张误差");
    _singleError->setPen(QPen(QColor(250, 170, 20).lighter(150)));
    _singleError->setBrush(QColor(250, 170, 20));

    ui->plot->addGraph();
    ui->plot->graph(0)->setPen(QPen(QColor(0, 168, 140),5));
    ui->plot->graph(0)->setName("平均误差");
}

MsnhCalibrateErrorPlot::~MsnhCalibrateErrorPlot()
{
    delete ui;
}

void MsnhCalibrateErrorPlot::clearDatas()
{
    _count = 0;
    _ticks.clear();
    _labels.clear();
    _barValues.clear();
}

void MsnhCalibrateErrorPlot::addValue(double value)
{
    _ticks.push_back(_count);
    _labels.push_back(QString::number(_count));
    _barValues.push_back(value);

    if(value > _max)
    {
        _max = value;
    }

    if(value < _min)
    {
        _min = value;
    }

    _count++;
}

void MsnhCalibrateErrorPlot::init()
{
    ui->plot->yAxis->setRange(_min,_max);
    ui->plot->xAxis->setRange(-1,_ticks.size());

    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    textTicker->addTicks(_ticks, _labels);
    ui->plot->xAxis->setTicker(textTicker);

    _singleError->setData(_ticks, _barValues);

    QVector<double> tmpValues = _barValues;

    double sum = 0;
    for (int i = 0; i < tmpValues.size(); ++i)
    {
        sum += tmpValues[i];
    }

    tmpValues.fill(sum/tmpValues.size());

    ui->plot->graph(0)->addData(_ticks, tmpValues);
}

void MsnhCalibrateErrorPlot::on_savePicBtn_clicked()
{
    QString path = QFileDialog::getSaveFileName(this,"保存图片","Image","png格式(*.png);;bmp格式(*.bmp);;jpg格式(*.jpg);;pdf格式(*.pdf)");

    QString end = path.right(3);

    if(end == "png")
    {
        ui->plot->savePng(path);
    }
    else if(end == "jpg")
    {
        ui->plot->saveJpg(path);
    }
    else if(end == "bmp")
    {
        ui->plot->saveBmp(path);
    }
    else if(end == "pdf")
    {
        ui->plot->savePdf(path);
    }
    else
    {
        return;
    }
}

