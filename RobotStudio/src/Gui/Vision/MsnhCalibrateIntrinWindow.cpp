﻿#include "ui_MsnhCalibrateIntrinWindow.h"
#include <Gui/Vision/MsnhCalibrateIntrinWindow.h>
#include <lib/MsnhOpenCVCamera.h>
#include <Plugin/MsnhLoopThread.h>
#include <Plugin/MsnhProcessFactory.h>
#include <Util/MsnhCVUtilEx.h>
#include <QDebug>
#include <QMessageBox>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhCalibrateIntrinWindow::MsnhCalibrateIntrinWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MsnhCalibrateIntrinWindow)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    // ui==
    ui->imgList->setViewMode(QListWidget::IconMode);
    ui->imgList->setIconSize(QSize(180,180));
    ui->imgList->setResizeMode(QListWidget::Adjust);

    _status = new QLabel(this);
    _statusCaption = new QLabel(this);
    _statusCaption->setText("提示：");
    ui->statusBar->addWidget(_statusCaption);
    ui->statusBar->addWidget(_status);

    connect(ui->imgList->delAction,&QAction::triggered,this,[this]()->void{
        auto index = ui->imgList->currentRow();
        _imgListBuff.removeAt(index);
        this->refreshImgList();
    });

    // ==
    MsnhProcessFactory* factory = MsnhSingleton<MsnhProcessFactory>::getInstance();

    auto camIO2d = factory->get2dCamIO();

    for (int i = 0; i < camIO2d.size(); ++i)
    {
        ui->camTypeCbx->addItem(camIO2d[i]);
    }

    _currentIOType = "opencvCamera";
    _process = factory->getProcInstance("opencvCamera");
    ui->processPropertiesWidget->init(_process);

    _loopThread = new MsnhLoopThread(_process, true, 30, this);
    connect(_loopThread,&MsnhLoopThread::revData,this,&MsnhCalibrateIntrinWindow::revData);

    // ==
    _paramsWindow = new MsnhIntrinParamsWindow(this);


    // msnhnet
//    std::string msnhnetPath = "E:/works/qt/casia/projects/models/yolov3/yolov3.msnhnet";
//    std::string msnhbinPath = "E:/works/qt/casia/projects/models/yolov3/yolov3.msnhbin";
//    std::string labelsPath = "E:/Github/Msnhnet/labels/coco.names";

//    Msnhnet::NetBuilder::setOnlyGpu(true);
//    msnhNet.buildNetFromMsnhNet(msnhnetPath);
//    msnhNet.loadWeightsFromMsnhBin(msnhbinPath);
//    Msnhnet::IO::readVectorStr(labels, labelsPath.data(), "\n");

}

MsnhCalibrateIntrinWindow::~MsnhCalibrateIntrinWindow()
{
    _loopThread->stopIt();
    _process->destroy();
    delete ui;
}

void MsnhCalibrateIntrinWindow::showImage(const cv::Mat &mat)
{
    if(_checkChessBoard)
    {
        cv::Mat temp = mat;
        std::vector<cv::Point2f> pointBuf;
        bool find = cv::findChessboardCorners(temp,_boardSize,pointBuf,cv::CALIB_CB_FAST_CHECK); //快速检测
        if(find)
        {
            cv::drawChessboardCorners(temp,_boardSize,pointBuf,find);
        }
        ui->camImage->setImage(temp);
    }
    else
    {
        /// ===
//        Msnhnet::Point2I inSize = msnhNet.getInputSize();
//        cv::Mat tmp = mat.clone();
//        cv::Mat org = mat.clone();
//        std::vector<float> img = Msnhnet::OpencvUtil::getPaddingZeroF32C3(tmp, cv::Size(inSize.x,inSize.y));
//        std::vector<std::vector<Msnhnet::YoloBox>> result = msnhNet.runYoloGPU(img);
//        Msnhnet::OpencvUtil::drawYoloBox(org,labels,result,inSize);
        // msnhnet

        ui->camImage->setImage(mat);
    }

}

void MsnhCalibrateIntrinWindow::closeEvent(QCloseEvent *event)
{
    event->accept();
}

void MsnhCalibrateIntrinWindow::revData(MsnhProtocal::Base *data)
{
    MsnhProtocal::MsnhImage* image = (MsnhProtocal::MsnhImage*)data;
    showImage(image->mat);
}

void MsnhCalibrateIntrinWindow::on_openCamBtn_clicked()
{
    _loopThread->stopIt();
    if(_process->resetParams())
    {
        _loopThread->startIt();
        qDebug()<<"打开相机成功!";
    }
    else
    {
        qWarning()<<"打开相机失败!";
    }
}


void MsnhCalibrateIntrinWindow::on_closeCamBtn_clicked()
{
    _loopThread->stopIt();
    qDebug()<<"关闭相机成功!";
}

void MsnhCalibrateIntrinWindow::on_refreshBtn_clicked()
{
    _process->refresh(); //刷新参数
    ui->processPropertiesWidget->init(_process);//重新初始化
}

void MsnhCalibrateIntrinWindow::on_switchBtn_clicked()
{
    if(_currentIOType != ui->camTypeCbx->currentText())
    {
        _currentIOType = ui->camTypeCbx->currentText();
        _loopThread->stopIt();
        _process->destroy();

        MsnhProcessFactory* factory = MsnhSingleton<MsnhProcessFactory>::getInstance();
        _process = factory->getProcInstance(_currentIOType);
        ui->processPropertiesWidget->init(_process);

        disconnect(_loopThread,nullptr,this,nullptr);
        delete  _loopThread;

        _loopThread = new MsnhLoopThread(_process, true, 30, this);
        connect(_loopThread,&MsnhLoopThread::revData,this,&MsnhCalibrateIntrinWindow::revData);
    }
}


void MsnhCalibrateIntrinWindow::on_takePicBtn_clicked()
{
    _imgListBuff.push_back(ui->camImage->getMat());
    QIcon icon(QPixmap::fromImage(MsnhCVUtil::MatToQImage(_imgListBuff.last())));
    ui->imgList->addItem(new QListWidgetItem(icon,QString::number(_imgListBuff.length())));
}

void MsnhCalibrateIntrinWindow::refreshImgList()
{
    ui->imgList->clear();
    for(int i=0;i<_imgListBuff.length();i++)
    {
        QIcon icon(QPixmap::fromImage(MsnhCVUtil::MatToQImage(_imgListBuff[i])));
        ui->imgList->addItem(new QListWidgetItem(icon,QString::number(i+1)));
    }
}

void MsnhCalibrateIntrinWindow::on_clearImgListBtn_clicked()
{
    ui->imgList->clear();
    _imgListBuff.clear();
}

void MsnhCalibrateIntrinWindow::on_calibBtn_clicked()
{
    int flag = 0;

    if(ui->rationalModelCkbx->isChecked())
    {
        flag |= cv::CALIB_RATIONAL_MODEL;
        _coeffsNum = 8;
    }

    if(ui->thinPrismModelCkbx->isChecked())
    {
        flag |= cv::CALIB_THIN_PRISM_MODEL;
        _coeffsNum = 12;
    }

    if(ui->tiltedModelCkbx->isChecked())
    {
        flag |= cv::CALIB_TILTED_MODEL;
        _coeffsNum = 14;
    }

    if(ui->fixPrincipalPointCkbx->isChecked())
    {
        flag |= cv::CALIB_FIX_PRINCIPAL_POINT;
    }

    if(ui->fixS1234Ckbx->isChecked())
    {
        flag |= cv::CALIB_FIX_S1_S2_S3_S4;
    }

    if(ui->fixTauxTauYCkbx->isChecked())
    {
        flag |= cv::CALIB_FIX_TAUX_TAUY;
    }

    if(ui->fixWHRatioCkbx->isChecked())
    {
        flag |= cv::CALIB_FIX_ASPECT_RATIO;
    }

    if(ui->fixZeroTanDistCkbx->isChecked())
    {
        flag |= cv::CALIB_ZERO_TANGENT_DIST;
    }

    _intrinCalibPlot = new MsnhCalibrateErrorPlot(this);

    double count = 0;
    double sum = _imgListBuff.length()*3+1;

    if(_imgListBuff.length()==0)
    {
        QMessageBox::warning(this,"警告","没有需要处理的图片");
        return;
    }

    if(!setParams())
    {
        ui->checkChessCbx->setChecked(false);
        return;
    }

    //清除以前标定结果
    _image2DPoints.clear();
    _object3DPoints.clear();
    _tvecsMat.clear();
    _rvecsMat.clear();
    _errList.clear();
    std::vector<cv::Point2f> image2DPoint;  /* 缓存每幅图像上检测到的角点 */

    int tempH = 0;
    int tempW = 0;
    cv::Mat tempMat;

    for(int i = 0;i<_imgListBuff.length();i++)                       //1
    {
        tempMat = _imgListBuff[i];
        if(i==0)
        {
            tempH = tempMat.rows;
            tempW = tempMat.cols;
        }

        if(tempH!=tempMat.rows||tempW!=tempMat.cols)
        {
            QMessageBox::warning(this,"警告","第"+QString::number(i)+"个图像与之前图像长宽不相等");
            return;
        }

        /* 提取角点 */
        if (false == findChessboardCorners(tempMat, _boardSize, image2DPoint))
        {
            QMessageBox::warning(this,"警告","第"+QString::number(i)+"个图未找到角点");
            return;
        }

        cv::Mat view_gray;

        if(tempMat.channels()==1)
        {
            view_gray = tempMat;
        }
        else
        {
            cv::cvtColor(tempMat, view_gray, cv::COLOR_RGB2GRAY);
        }

        /* 亚像素精确化 */
        cv::find4QuadCornerSubpix(view_gray, image2DPoint, cv::Size(5, 5)); //对粗提取的角点进行精确化
        _image2DPoints.push_back(image2DPoint);  //保存亚像素角点

        ///////////////////////////////进度条/////////////////////////////////////
        count=count+1;
        _status->setText(QString::number(count/sum*100,'f',2)+"%");
        qApp->processEvents();
    }

    /* 初始化标定板上角点的三维坐标  以第一个角点为(0,0,0)*/ //2
    for (int t = 0; t<_imgListBuff.length(); t++)                //  <--width--> j
    {                                                           ///  ■ ■ ■ ■ ■     || i
        std::vector<cv::Point3f> tempPointSet;                  ///   ■ ■ ■ ■      ||
        for (int i = 0; i<_boardSize.height; i++)                ///  ■ ■ ■ ■ ■   height
        {                                                       ///   ■ ■ ■ ■      ||
            for (int j = 0; j<_boardSize.width; j++)             ///  ■ ■ ■ ■ ■     ||
            {
                cv::Point3f realPoint;
                /* 假设标定板放在世界坐标系中z=0的平面上 */
                realPoint.x = i*_boxSize.width;
                realPoint.y = j*_boxSize.height;
                realPoint.z = 0;
                tempPointSet.push_back(realPoint);
            }
        }
        _object3DPoints.push_back(tempPointSet);
        ///////////////////////////////进度条/////////////////////////////////////
        count=count+1;
        _status->setText(QString::number(count/sum*100,'f',2)+"%");
        qApp->processEvents();
    }

    std::vector<int> point_counts;  // 每幅图像中角点的数量
    /* 初始化每幅图像中的角点数量，假定每幅图像中都可以看到完整的标定板 */
    for ( int i = 0;  i<_imgListBuff.length(); i++)
    {
        point_counts.push_back(_boardSize.width*_boardSize.height);
    }

    /* 开始标定 */
    cv::calibrateCamera(_object3DPoints, _image2DPoints, cv::Size(tempW,tempH), _cameraMatrix, _distCoeffs, _rvecsMat, _tvecsMat, flag);

    ///////////////////////////////进度条/////////////////////////////////////
    count=count+1;
    _status->setText(QString::number(count/sum*100,'f',2)+"%");
    qApp->processEvents();

    /**@breif 保存重新计算得到的投影点 */
    std::vector<cv::Point2f> newImage2DPoint;
    double total_err = 0.0; /* 所有图像的平均误差的总和 */
    double err = 0.0; /* 每幅图像的平均误差 */

    for (size_t i = 0; i<_imgListBuff.length(); i++)                    //3
    {
        /**@breif 第i个标定板角点三维坐标*/
        std::vector<cv::Point3f> temp3DPoint = _object3DPoints[i];
        /* 通过得到的摄像机内外参数，对空间的三维点进行重新投影计算，得到新的投影点 */
        projectPoints(temp3DPoint, _rvecsMat[i], _tvecsMat[i], _cameraMatrix, _distCoeffs, newImage2DPoint);//Levenberg-Marquard算法
        /* 计算新的投影点和旧的投影点之间的误差*/
        std::vector<cv::Point2f> oldImage2DPoint = _image2DPoints[i];
        cv::Mat oldImage2dPointMat = cv::Mat(1, (int)oldImage2DPoint.size(), CV_32FC2);//转为行向量
        cv::Mat newImage2DPointMat = cv::Mat(1, (int)newImage2DPoint.size(), CV_32FC2);//转为行向量
        for (int j = 0; j < oldImage2DPoint.size(); j++)
        {
            newImage2DPointMat.at<cv::Vec2f>(0, j) = cv::Vec2f(newImage2DPoint[j].x, newImage2DPoint[j].y);
            oldImage2dPointMat.at<cv::Vec2f>(0, j) = cv::Vec2f(oldImage2DPoint[j].x, oldImage2DPoint[j].y);
        }
        err = norm(newImage2DPointMat, oldImage2dPointMat,cv::NORM_L2);// 求二范数
        total_err += err /= point_counts[i];
        _errList.push_back(err);

        _intrinCalibPlot->addValue(err); //plot显示

        ///////////////////////////////进度条/////////////////////////////////////
        count=count+1;
        _status->setText(QString::number(count/sum*100,'f',2)+"%");
        qApp->processEvents();
    }

    _allErr = total_err/(_imgListBuff.length());
    _calibrated = true;
    _intrinCalibPlot->init();
    _intrinCalibPlot->setWindowTitle("内参标定误差");
    _intrinCalibPlot->show();
    _paramsWindow->setParams(_cameraMatrix, _distCoeffs, _coeffsNum);
    _paramsWindow->show();
    QMessageBox::information(this,"提示","恭喜,标定成功");
}

void MsnhCalibrateIntrinWindow::on_importImgBtn_clicked()
{
    QStringList paths=QFileDialog::getOpenFileNames(this,"选择图片","","图片(*.bmp *.jpg *.png *.jpeg)");
    if(paths.length()==0)
        return;
    cv::Mat temp;
    for(int i=0;i<paths.length();i++)
    {
        temp = cv::imread(paths[i].toLocal8Bit().toStdString());
        _imgListBuff.push_back(temp);
    }
    refreshImgList();
}


void MsnhCalibrateIntrinWindow::on_imgList_doubleClicked(const QModelIndex &index)
{
    if(ui->undistortCbx->isChecked())
    {
        cv::Mat mat;
        cv::undistort(_imgListBuff[index.row()],mat,_cameraMatrix,_distCoeffs);
        ui->listImage->setImage(mat);
    }
    else
    {
        ui->listImage->setImage(_imgListBuff[index.row()]);
    }

}

void MsnhCalibrateIntrinWindow::on_checkChessCbx_clicked()
{
    if(ui->checkChessCbx->isChecked())
    {
        if(!setParams())
        {
            ui->checkChessCbx->setChecked(false);
            _checkChessBoard = false;
            return;
        }
        else
        {
            _checkChessBoard = true;
        }
    }
}

void MsnhCalibrateIntrinWindow::on_undistortCbx_clicked()
{
    if(ui->undistortCbx->isChecked())
    {
        if(!_calibrated)
        {
            QMessageBox::warning(this,"警告","相机未标定");
            ui->undistortCbx->setChecked(false);
        }
    }
}

bool MsnhCalibrateIntrinWindow::setParams()
{
    int rows = ui->cornerRowsTxt->text().toInt();
    int cols = ui->cornerColsTxt->text().toInt();
    float width = ui->boxWTxt->text().toFloat();
    float height = ui->boxHTxt->text().toFloat();
    if(cols==0||rows==0||fabs(width)<0.01||fabs(height)<0.01)
    {
        QMessageBox::warning(this,"警告","参数错误");
        return false;
    }

    if((cols%2==0&&rows%2==0)||(cols%2==1&&rows%2==1))
    {
        if(QMessageBox::Yes != QMessageBox::question(this,"警告","标定板棋盘格长和宽应该为奇偶组合，否则求解外参会出现奇异，是否继续?"))
        {
            return false;
        }
    }

    _boardSize.width = cols;
    _boardSize.height = rows;
    _boxSize.width = width;
    _boxSize.height = height;
    _status->setText("设置成功");
    return true;
}


void MsnhCalibrateIntrinWindow::on_importIntrinParamBtn_clicked()
{
    QString path=QFileDialog::getOpenFileName(this,"选择文件","","yaml(*.yaml);;xml(*.xml);;json(*.json)");
    if(path=="")
        return;
    cv::FileStorage fs(path.toLocal8Bit().toStdString().data(),cv::FileStorage::READ);

    fs["cameraMatrix"]>>_cameraMatrix;
    fs["distCoeffs"]>>_distCoeffs;

    _calibrated = false;

    if(_cameraMatrix.rows==0||_cameraMatrix.cols==0||
        _distCoeffs.rows==0||_distCoeffs.cols==0)
    {
        QMessageBox::warning(this,"警告","内参获取失败");
    }
    else if(_cameraMatrix.cols!=3||_cameraMatrix.rows!=3)
    {
        QMessageBox::warning(this,"警告","文件损坏");
    }
    else if(_distCoeffs.cols!=1||_distCoeffs.rows<5)
    {
        QMessageBox::warning(this,"警告","文件损坏");
    }
    else
    {
        _calibrated = true;
        QMessageBox::warning(this,"提示","内参读取成功");
    }
}

