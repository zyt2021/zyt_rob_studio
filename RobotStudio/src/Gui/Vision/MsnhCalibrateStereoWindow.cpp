﻿#include <Gui/Vision/MsnhCalibrateStereoWindow.h>
#include "ui_MsnhCalibrateStereoWindow.h"
#include <lib/MsnhOpenCVCamera.h>
#include <Plugin/MsnhLoopThread.h>
#include <Plugin/MsnhProcessFactory.h>
#include <Util/MsnhCVUtilEx.h>
#include <QDebug>
#include <QMessageBox>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhCalibrateStereoWindow::MsnhCalibrateStereoWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MsnhCalibrateStereoWindow)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    // ui==
    ui->imgList->setViewMode(QListWidget::IconMode);
    ui->imgList->setIconSize(QSize(180,180));
    ui->imgList->setResizeMode(QListWidget::Adjust);

    _status = new QLabel(this);
    _statusCaption = new QLabel(this);
    _statusCaption->setText("提示：");
    ui->statusBar->addWidget(_statusCaption);
    ui->statusBar->addWidget(_status);

    // ==
    MsnhProcessFactory* factory = MsnhSingleton<MsnhProcessFactory>::getInstance();

    auto camIO2d = factory->get2dCamIO();

    // cam1
    for (int i = 0; i < camIO2d.size(); ++i)
    {
        ui->camTypeCbx->addItem(camIO2d[i]);
    }

    _currentIOType = "opencvCamera";
    _process = factory->getProcInstance("opencvCamera");
    ui->processPropertiesWidget->init(_process);

    _loopThread = new MsnhLoopThread(_process, true, 30, this);
    connect(_loopThread,&MsnhLoopThread::revData,this,&MsnhCalibrateStereoWindow::revData1);

    // cam2
    for (int i = 0; i < camIO2d.size(); ++i)
    {
        ui->camTypeCbx_2->addItem(camIO2d[i]);
    }

    _currentIOType2 = "opencvCamera";
    _process2 = factory->getProcInstance("opencvCamera");
    ui->processPropertiesWidget_2->init(_process2);

    _loopThread2 = new MsnhLoopThread(_process2, true, 30, this);
    connect(_loopThread2,&MsnhLoopThread::revData,this,&MsnhCalibrateStereoWindow::revData2);

    connect(ui->imgList->delAction,&QAction::triggered,this,[this]()->void{
        auto index = ui->imgList->currentRow();
        leftImages.removeAt(index);
        rightImages.removeAt(index);
        this->refreshImgList();
    });
}

MsnhCalibrateStereoWindow::~MsnhCalibrateStereoWindow()
{
    _loopThread->stopIt();
    _process->destroy();
    _loopThread2->stopIt();
    _process2->destroy();
    delete ui;
}

void MsnhCalibrateStereoWindow::showImage1(const cv::Mat &mat)
{
    if(_checkChessBoard)
    {
        cv::Mat temp = mat;
        std::vector<cv::Point2f> pointBuf;
        bool find = cv::findChessboardCorners(temp,_boardSize,pointBuf,cv::CALIB_CB_FAST_CHECK); //快速检测
        if(find)
        {
            cv::drawChessboardCorners(temp,_boardSize,pointBuf,find);
        }
        ui->camImage->setImage(temp);
    }
    else
    {
        /// ===
        //        Msnhnet::Point2I inSize = msnhNet.getInputSize();
        //        cv::Mat tmp = mat.clone();
        //        cv::Mat org = mat.clone();
        //        std::vector<float> img = Msnhnet::OpencvUtil::getPaddingZeroF32C3(tmp, cv::Size(inSize.x,inSize.y));
        //        std::vector<std::vector<Msnhnet::YoloBox>> result = msnhNet.runYoloGPU(img);
        //        Msnhnet::OpencvUtil::drawYoloBox(org,labels,result,inSize);
        // msnhnet

        ui->camImage->setImage(mat);
    }
}

void MsnhCalibrateStereoWindow::showImage2(const cv::Mat &mat)
{
    if(_checkChessBoard)
    {
        cv::Mat temp = mat;
        std::vector<cv::Point2f> pointBuf;
        bool find = cv::findChessboardCorners(temp,_boardSize,pointBuf,cv::CALIB_CB_FAST_CHECK); //快速检测
        if(find)
        {
            cv::drawChessboardCorners(temp,_boardSize,pointBuf,find);
        }
        ui->camImage_2->setImage(temp);
    }
    else
    {
        /// ===
        //        Msnhnet::Point2I inSize = msnhNet.getInputSize();
        //        cv::Mat tmp = mat.clone();
        //        cv::Mat org = mat.clone();
        //        std::vector<float> img = Msnhnet::OpencvUtil::getPaddingZeroF32C3(tmp, cv::Size(inSize.x,inSize.y));
        //        std::vector<std::vector<Msnhnet::YoloBox>> result = msnhNet.runYoloGPU(img);
        //        Msnhnet::OpencvUtil::drawYoloBox(org,labels,result,inSize);
        // msnhnet

        ui->camImage_2->setImage(mat);
    }
}

void MsnhCalibrateStereoWindow::revData1(MsnhProtocal::Base *data)
{
    MsnhProtocal::MsnhImage* image = (MsnhProtocal::MsnhImage*)data;

    if(_singleCamDoubleImgMode)
    {
        cv::Mat left = image->mat(cv::Range(0,image->mat.rows),cv::Range(0,image->mat.cols/2));
        cv::Mat right= image->mat(cv::Range(0,image->mat.rows),cv::Range(image->mat.cols/2,image->mat.cols));

        showImage1(left);
        showImage2(right);
    }
    else
    {
        showImage1(image->mat);
    }
}

void MsnhCalibrateStereoWindow::revData2(MsnhProtocal::Base *data)
{
    MsnhProtocal::MsnhImage* image = (MsnhProtocal::MsnhImage*)data;
    showImage2(image->mat);
}

bool MsnhCalibrateStereoWindow::refreshImgList()
{
    ui->imgList->clear();

    if(leftImages.size()==0||rightImages.size()==0)
    {
        QMessageBox::warning(this,"警告","左侧或右侧未包含图片");
        return false;
    }

    if(leftImages.size()!=rightImages.size())
    {
        QMessageBox::warning(this,"警告","左侧和右侧图片不相等");
        return false;
    }
    cv::Mat result(leftImages[0].rows,leftImages[0].cols+rightImages[0].cols+1,leftImages[0].type());
    for(size_t i=0;i<leftImages.size();i++)
    {
        cv::hconcat(leftImages[i],rightImages[i],result);
        QIcon icon(QPixmap::fromImage(MsnhCVUtil::MatToQImage(result)));
        ui->imgList->addItem(new QListWidgetItem(icon,QString::number(i+1)));
    }
    return true;
}

bool MsnhCalibrateStereoWindow::setParams()
{
    int rows = ui->cornerRowsTxt->text().toInt();
    int cols = ui->cornerColsTxt->text().toInt();
    float width = ui->boxWTxt->text().toFloat();
    float height = ui->boxHTxt->text().toFloat();
    if(cols==0||rows==0||fabs(width)<0.01||fabs(height)<0.01)
    {
        QMessageBox::warning(this,"警告","参数错误");
        return false;
    }

    if((cols%2==0&&rows%2==0)||(cols%2==1&&rows%2==1))
    {
        if(QMessageBox::Yes != QMessageBox::question(this,"警告","标定板棋盘格长和宽应该为奇偶组合，否则求解外参会出现奇异，是否继续?"))
        {
            return false;
        }
    }

    _boardSize.width = cols;
    _boardSize.height = rows;
    _boxSize.width = width;
    _boxSize.height = height;
    _status->setText("设置成功");
    return true;
}

void MsnhCalibrateStereoWindow::on_singleCamDoubleImgCkbx_clicked()
{
    if(ui->singleCamDoubleImgCkbx->isChecked())
    {
        _singleCamDoubleImgMode = true;
        ui->cam2Gpbx->setEnabled(false);
    }
    else
    {
        _singleCamDoubleImgMode = false;
        ui->cam2Gpbx->setEnabled(true);
    }
}


void MsnhCalibrateStereoWindow::on_refreshBtn_clicked()
{
    _process->refresh(); //刷新参数
    ui->processPropertiesWidget->init(_process);//重新初始化
}


void MsnhCalibrateStereoWindow::on_switchBtn_clicked()
{
    if(_currentIOType != ui->camTypeCbx->currentText())
    {
        _currentIOType = ui->camTypeCbx->currentText();
        _loopThread->stopIt();
        _process->destroy();

        MsnhProcessFactory* factory = MsnhSingleton<MsnhProcessFactory>::getInstance();
        _process = factory->getProcInstance(_currentIOType);
        ui->processPropertiesWidget->init(_process);

        disconnect(_loopThread,nullptr,this,nullptr);
        delete  _loopThread;

        _loopThread = new MsnhLoopThread(_process, true, 30, this);
        connect(_loopThread,&MsnhLoopThread::revData,this,&MsnhCalibrateStereoWindow::revData1);
    }
}


void MsnhCalibrateStereoWindow::on_openCamBtn_clicked()
{
    _loopThread->stopIt();
    if(_process->resetParams())
    {
        _loopThread->startIt();
        qDebug()<<"打开相机1成功!";
    }
    else
    {
        qWarning()<<"打开相机1失败!";
    }
}


void MsnhCalibrateStereoWindow::on_closeCamBtn_clicked()
{
    _loopThread->stopIt();
    qDebug()<<"关闭相机1成功!";
}


void MsnhCalibrateStereoWindow::on_refreshBtn_2_clicked()
{
    _process2->refresh(); //刷新参数
    ui->processPropertiesWidget_2->init(_process2);//重新初始化
}


void MsnhCalibrateStereoWindow::on_switchBtn_2_clicked()
{
    if(_currentIOType2 != ui->camTypeCbx_2->currentText())
    {
        _currentIOType2 = ui->camTypeCbx_2->currentText();
        _loopThread2->stopIt();
        _process2->destroy();

        MsnhProcessFactory* factory = MsnhSingleton<MsnhProcessFactory>::getInstance();
        _process2 = factory->getProcInstance(_currentIOType2);
        ui->processPropertiesWidget_2->init(_process2);

        disconnect(_loopThread2,nullptr,this,nullptr);
        delete  _loopThread2;

        _loopThread2 = new MsnhLoopThread(_process2, true, 30, this);
        connect(_loopThread2,&MsnhLoopThread::revData,this,&MsnhCalibrateStereoWindow::revData2);
    }
}


void MsnhCalibrateStereoWindow::on_openCamBtn_2_clicked()
{
    _loopThread2->stopIt();
    if(_process2->resetParams())
    {
        _loopThread2->startIt();
        qDebug()<<"打开相机2成功!";
    }
    else
    {
        qWarning()<<"打开相机2失败!";
    }
}


void MsnhCalibrateStereoWindow::on_closeCamBtn_2_clicked()
{
    _loopThread2->stopIt();
    qDebug()<<"关闭相机2成功!";
}


void MsnhCalibrateStereoWindow::on_takePicBtn_clicked()
{
    {
    _imgListBuff1.push_back(ui->camImage->getMat());
    QIcon icon(QPixmap::fromImage(MsnhCVUtil::MatToQImage(_imgListBuff1.last())));
    ui->imgList->addItem(new QListWidgetItem(icon,QString::number(_imgListBuff1.length())));
    }

    {
    _imgListBuff2.push_back(ui->camImage->getMat());
    QIcon icon(QPixmap::fromImage(MsnhCVUtil::MatToQImage(_imgListBuff2.last())));
    ui->imgList->addItem(new QListWidgetItem(icon,QString::number(_imgListBuff2.length())));
    }

    refreshImgList();
}

void MsnhCalibrateStereoWindow::on_importLeftImgBtn_clicked()
{
    leftCamPaths = QFileDialog::getOpenFileNames(this,"选择左侧相机图片","","图片(*.bmp *.jpg *.png *.jpeg)");

    if(leftCamPaths.length()==0)
        return;

    if(leftCamPaths.length()>30)
    {
        leftCamPaths.clear();
        QMessageBox::warning(this,"提示","标定图片不要超过30张");
        return;
    }

    QMessageBox::warning(this,"提示","左侧导入"+QString::number(leftCamPaths.size())+"张图片");
}

void MsnhCalibrateStereoWindow::on_importRightImgBtn_clicked()
{
    rightCamPaths = QFileDialog::getOpenFileNames(this,"选择右侧相机图片","","图片(*.bmp *.jpg *.png *.jpeg)");

    if(rightCamPaths.length()==0)
        return;

    if(rightCamPaths.length()>30)
    {
        rightCamPaths.clear();
        QMessageBox::warning(this,"提示","标定图片不要超过30张");
        return;
    }
    QMessageBox::warning(this,"提示","右侧导入"+QString::number(rightCamPaths.size())+"张图片");
}

void MsnhCalibrateStereoWindow::on_collectImgBtn_clicked()
{
    if(rightCamPaths.length()==0||leftCamPaths.length()==0)
    {
        QMessageBox::warning(this,"警告","左侧或右侧相机图片路径为空");
        return;
    }

    if(rightCamPaths.length()!=leftCamPaths.length())
    {
        QMessageBox::warning(this,"警告","左侧和右侧图片不相等");
        return;
    }
    leftImages.clear();
    rightImages.clear();
    cv::Mat lTmp,rTmp;

    for(int i=0;i<leftCamPaths.length();i++)
    {
        lTmp = cv::imread(leftCamPaths[i].toLocal8Bit().toStdString());
        rTmp = cv::imread(rightCamPaths[i].toLocal8Bit().toStdString());
        if(lTmp.cols!=rTmp.cols||lTmp.rows!=rTmp.rows)
        {
            QMessageBox::warning(this,"警告","第"+QString::number(i+1)+ "组左侧和右侧图片分辨率不一致");
            leftImages.clear();
            rightImages.clear();
            return;
        }

        leftImages.push_back(lTmp);
        rightImages.push_back(rTmp);
    }
    if(refreshImgList())
        QMessageBox::information(this,"警告","整合成功");
}

void MsnhCalibrateStereoWindow::on_clearImgListBtn_clicked()
{
    leftImages.clear();
    rightImages.clear();
    ui->imgList->clear();
}

void MsnhCalibrateStereoWindow::on_calibBtn_clicked()
{

    if(!setParams())
    {
        return;
    }

    cv::Mat grayL;
    cv::Mat grayR;

    _leftCalibPlot = new MsnhCalibrateErrorPlot(this);
    _rightCalibPlot = new MsnhCalibrateErrorPlot(this);

    double count = 0;
    double sum = leftImages.size()*2+4;
    /**@brief 保存标定板上角点的三维坐标 */
    std::vector<std::vector<cv::Point3f>> object3DPoints;
    std::vector<cv::Point2f> cornersL;
    std::vector<cv::Point2f> cornersR;

    /**@brief 保存左侧相机检测到的所有角点 */
    std::vector<std::vector<cv::Point2f>> leftImage2DPoints;
    /**@brief 保存右侧相机检测到的所有角点 */
    std::vector<std::vector<cv::Point2f>> rightImage2DPoints;

    std::vector<std::vector<cv::Point2f>> lImgPoints;

    std::vector<std::vector<cv::Point2f>> rImgPoints;

    tvecsMatL.clear();
    tvecsMatR.clear();
    rvecsMatL.clear();
    rvecsMatR.clear();
    errListL.clear();
    errListR.clear();

    if(leftImages.length()==0||rightImages.length()==0)
    {
        QMessageBox::warning(this,"警告","左侧或右侧相机图片路径为空");
        return;
    }

    if(leftImages.length()!=rightImages.length())
    {
        QMessageBox::warning(this,"警告","左侧和右侧图片不相等");
        return;
    }

    if(leftImages.length()>30)
    {
        QMessageBox::warning(this,"警告","两侧标定图片分别不要超过30张");
        return;
    }

    for (int i = 0;i <leftImages.length();i++)                                   //1
    {

        bool foundL = false;
        bool foundR = false;

        if(leftImages[i].size!=rightImages[i].size)
        {
            QMessageBox::warning(this,"警告","第"+QString::number(i)+"组图左右长宽不等");
            return ;
        }

        if(i == 0)
        {
            tmpHL = leftImages[i].rows;
            tmpHR = rightImages[i].rows;
            tmpWL = leftImages[i].cols;
            tmpWR = rightImages[i].cols;
        }
        else
        {
            if(tmpHL!=leftImages[i].rows   ||
                    tmpHR!= rightImages[i].rows ||
                    tmpWL!= leftImages[i].cols  ||
                    tmpWR!= rightImages[i].cols)
            {
                QMessageBox::warning(this,"警告","第"+QString::number(i)+"组图与之前的图像像长宽不等");
                return ;
            }

        }

        //查找角点
        foundL = cv::findChessboardCorners(leftImages[i], _boardSize, cornersL,
                                           cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FILTER_QUADS);
        //查找角点
        foundR = cv::findChessboardCorners(rightImages[i], _boardSize, cornersR,
                                           cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FILTER_QUADS);


        if(!foundL)
        {
            QMessageBox::warning(this,"警告","左图第"+QString::number(i)+"个图未找到角点");
            return;
        }

        if(!foundR)
        {
            QMessageBox::warning(this,"警告","右图第"+QString::number(i)+"个图未找到角点");
            return;
        }

        ///与单目相机进行对比

        //灰度
        cv::cvtColor(leftImages[i], grayL, cv::COLOR_BGR2GRAY);

        //灰度
        cv::cvtColor(rightImages[i], grayR,cv::COLOR_BGR2GRAY);


        //查找亚像素 cornersL->Point2f 图像坐标系
        cv::cornerSubPix(grayL, cornersL, cv::Size(5, 5), cv::Size(-1, -1),
                         cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::MAX_ITER, 30, 0.1));

        //查找亚像素 cornersR->Point2f 图像坐标系
        cv::cornerSubPix(grayR, cornersR, cv::Size(5, 5), cv::Size(-1, -1),
                         cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::MAX_ITER, 30, 0.1));

        /* 初始化标定板上角点的三维坐标 以第一个角点为(0,0,0)*/      //  <--width--> j
        std::vector<cv::Point3f > obj;                           ///  ■ ■ ■ ■ ■     || i
        for (int i = 0; i < _boardSize.height; i++)               ///   ■ ■ ■ ■      ||
        {                                                        ///  ■ ■ ■ ■ ■   height
            for (int j = 0; j < _boardSize.width; j++)            ///   ■ ■ ■ ■      ||
            {                                                    ///  ■ ■ ■ ■ ■     ||
                /* 假设标定板放在世界坐标系中z=0的平面上 */
                obj.push_back(cv::Point3f((float)(i * _boxSize.width),
                                          (float)(j * _boxSize.height),
                                          0));
            }
        }
        if (foundL && foundR)
        {
            lImgPoints.push_back(cornersL);  //存所有左侧图像等点
            rImgPoints.push_back(cornersR);  //存所有右侧图像等点
            object3DPoints.push_back(obj);     //存入三维坐标
        }

        ///////////////////////////////进度条/////////////////////////////////////
        count=count+1;
        _status->setText(QString::number(count/sum*100,'f',2)+"%");
        qApp->processEvents();
    }

    for (int i = 0; i < lImgPoints.size(); i++)  //所有等角点位置，存入
    {
        std::vector< cv::Point2f > v1;
        std::vector< cv::Point2f > v2;
        for (int j = 0; j < lImgPoints[i].size(); j++)
        {
            v1.push_back(cv::Point2f((double)lImgPoints[i][j].x, (double)lImgPoints[i][j].y)); //提高精度
            v2.push_back(cv::Point2f((double)rImgPoints[i][j].x, (double)rImgPoints[i][j].y)); //提高精度
        }

        leftImage2DPoints.push_back(v1);
        rightImage2DPoints.push_back(v2);
    }

    cv::calibrateCamera(object3DPoints,leftImage2DPoints,cv::Size(tmpWL,tmpHL),
                        cameraMatrixL,distCoeffsL,rvecsMatL,tvecsMatL,0);

    count=count+1;
    _status->setText(QString::number(count/sum*100,'f',2)+"%");

    ///////////////////////////////进度条/////////////////////////////////////
    count=count+1;
    _status->setText(QString::number(count/sum*100,'f',2)+"%");
    qApp->processEvents();


    cv::calibrateCamera(object3DPoints,rightImage2DPoints,cv::Size(tmpWR,tmpHR),
                        cameraMatrixR,distCoeffsR,rvecsMatR,tvecsMatR,0);

    ///////////////////////////////进度条/////////////////////////////////////
    count=count+1;
    _status->setText(QString::number(count/sum*100,'f',2)+"%");
    qApp->processEvents();

    cv::stereoCalibrate(object3DPoints,leftImage2DPoints,rightImage2DPoints,cameraMatrixL,
                        distCoeffsL,cameraMatrixR,distCoeffsR,cv::Size(tmpWL,tmpHL),
                        R,T,E,F); //相机内参和畸变系数需初始化 flag = CALIB_FIX_INTRINSIC

    ///////////////////////////////进度条/////////////////////////////////////
    count=count+1;
    _status->setText(QString::number(count/sum*100,'f',2)+"%");
    qApp->processEvents();
    std::vector<cv::Point2f> newImage2DPointL; /* L保存重新计算得到的投影点 */
    std::vector<cv::Point2f> newImage2DPointR; /* R保存重新计算得到的投影点 */
    double totalErrL = 0.0; /* 所有L图像的平均误差的总和 */
    double totalErrR = 0.0; /* 所有R图像的平均误差的总和 */
    double errL = 0.0; /* L的平均误差 */
    double errR = 0.0; /* R平均误差 */

    std::vector<int> point_counts;  // 每幅图像中角点的数量
    /* 初始化每幅图像中的角点数量，假定每幅图像中都可以看到完整的标定板 */
    for ( int i = 0;  i<leftImages.length(); i++)
    {
        point_counts.push_back(_boardSize.width*_boardSize.height);
    }

    for (size_t i = 0; i<leftImages.length(); i++)                    // 3
    {
        std::vector<cv::Point3f> tmp3DPoint = object3DPoints[i];
        /* 通过得到的摄像机内外参数，对空间的三维点进行重新投影计算，得到新的投影点 */
        projectPoints(tmp3DPoint, rvecsMatL[i], tvecsMatL[i], cameraMatrixL, distCoeffsL, newImage2DPointL);
        projectPoints(tmp3DPoint, rvecsMatR[i], tvecsMatR[i], cameraMatrixR, distCoeffsR, newImage2DPointR);

        /* 计算新的投影点和旧的投影点之间的误差*/
        std::vector<cv::Point2f> oldImage2DPointL = leftImage2DPoints[i];
        std::vector<cv::Point2f> oldImage2DPointR = rightImage2DPoints[i];

        cv::Mat oldImage2DPointMatL = cv::Mat(1, oldImage2DPointL.size(), CV_32FC2);//转为行向量
        cv::Mat oldImage2DPointMatR = cv::Mat(1, oldImage2DPointR.size(), CV_32FC2);//转为行向量
        cv::Mat newImage2DPointmatL = cv::Mat(1, newImage2DPointL.size(), CV_32FC2); //转为行向量
        cv::Mat newImage2DPointmatR = cv::Mat(1, newImage2DPointR.size(), CV_32FC2); //转为行向量

        for (int j = 0; j < oldImage2DPointL.size(); j++)
        {
            newImage2DPointmatL.at<cv::Vec2f>(0, j) = cv::Vec2f(newImage2DPointL[j].x, newImage2DPointL[j].y);
            oldImage2DPointMatL.at<cv::Vec2f>(0, j) = cv::Vec2f(oldImage2DPointL[j].x, oldImage2DPointL[j].y);

            newImage2DPointmatR.at<cv::Vec2f>(0, j) = cv::Vec2f(newImage2DPointR[j].x, newImage2DPointR[j].y);
            oldImage2DPointMatR.at<cv::Vec2f>(0, j) = cv::Vec2f(oldImage2DPointR[j].x, oldImage2DPointR[j].y);
        }
        errL = cv::norm(newImage2DPointmatL, oldImage2DPointMatL,cv::NORM_L2);
        errR = cv::norm(newImage2DPointmatR, oldImage2DPointMatR,cv::NORM_L2);
        totalErrL += errL /= point_counts[i];
        totalErrR += errR /= point_counts[i];

        errListL.push_back(errL);
        errListR.push_back(errR);

        _leftCalibPlot->addValue(errL);
        _rightCalibPlot->addValue(errR);

        ///////////////////////////////进度条/////////////////////////////////////
        count=count+1;
        _status->setText(QString::number(count/sum*100,'f',2)+"%");
        qApp->processEvents();
    }

    allErrL = totalErrL/(leftImages.length());
    allErrR = totalErrR/(rightImages.length());

    cv::stereoRectify(cameraMatrixL,distCoeffsL,cameraMatrixR,distCoeffsR,cv::Size(tmpWL,tmpHL),
                      R,T,RL,RR,PL,PR,Q,0,1,cv::Size(0,0),&roiL,&roiR);

    cv::Vec3d rotation = MsnhCVUtil::rot2euler(R,MsnhCVUtil::Sequence::XYZ);

    _leftCalibPlot->init();
    _rightCalibPlot->init();
    _leftCalibPlot->show();
    _rightCalibPlot->show();

    _calibrated = true;
}
