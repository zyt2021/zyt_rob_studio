﻿#include "Gui/Vision/MsnhIntrinParamsWindow.h"
#include "ui_MsnhIntrinParamsWindow.h"
#include <QDebug>
#include <QFileDialog>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhIntrinParamsWindow::MsnhIntrinParamsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MsnhIntrinParamsWindow)
{
    ui->setupUi(this);
}

MsnhIntrinParamsWindow::~MsnhIntrinParamsWindow()
{
    delete ui;
}

void MsnhIntrinParamsWindow::setParams(const cv::Mat &cameraMat, const cv::Mat &coeffs, int coeffsNum)
{
    _cameraMat = cameraMat;
    _coeffs    = cv::Mat(1, coeffsNum, CV_64F, cv::Scalar::all(0));

    for (int i = 0; i < coeffsNum; ++i)
    {
        _coeffs.at<double>(0,i) =  coeffs.at<double>(0,i);
    }

    ui->paramsTxt->clear();

    std::stringstream buf;
    buf<<std::setiosflags(std::ios::left)<<std::setw(12)<<std::setprecision(12)<<std::setiosflags(std::ios::fixed)
        <<_cameraMat.at<double>(0,0)<<","<<_cameraMat.at<double>(0,1)<<","<<_cameraMat.at<double>(0,2)<<","
        <<_cameraMat.at<double>(1,0)<<","<<_cameraMat.at<double>(1,1)<<","<<_cameraMat.at<double>(1,2)<<","
        <<_cameraMat.at<double>(2,0)<<","<<_cameraMat.at<double>(2,1)<<","<<_cameraMat.at<double>(2,2);

    QString argsTemp     = QString::fromStdString(buf.str());
    QStringList argsList = argsTemp.split(",");
    QString args         = "内参矩阵:\n{\n";

    args += "   " + QString(argsList[0]).leftRef(9)+",";
    args += "   " + QString(argsList[1]).leftRef(9)+",";
    args += "   " + QString(argsList[2]).leftRef(9)+",\n";

    args += "   " + QString(argsList[3]).leftRef(9)+",";
    args += "   " + QString(argsList[4]).leftRef(9)+",";
    args += "   " + QString(argsList[5]).leftRef(9)+",\n";

    args += "   " + QString(argsList[6]).leftRef(9)+",";
    args += "   " + QString(argsList[7]).leftRef(9)+",";
    args += "   " + QString(argsList[8]).leftRef(9)+",\n}\n";
    ui->paramsTxt->appendPlainText(args);

    std::stringstream buf1;
    buf1<<std::setiosflags(std::ios::left)<<std::setw(12)<<std::setprecision(12)<<std::setiosflags(std::ios::fixed);
    for (int i = 0; i < coeffsNum; ++i)
    {
        buf1<<_coeffs.at<double>(0,i)<<",";
    }

    argsTemp     = QString::fromStdString(buf1.str());
    argsList     = argsTemp.split(",");
    args         = "畸变参数:\n{\n";
    for (int i = 0; i < coeffsNum; ++i)
    {
        args += "   "+QString(argsList[i]).leftRef(9)+"\n";
    }
    args += "}";
    ui->paramsTxt->appendPlainText(args);
}

void MsnhIntrinParamsWindow::on_saveBtn_clicked()
{
    QString path = QFileDialog::getSaveFileName(this,"保存参数","calibData","yaml格式(*.yaml);;xml格式(*.xml);;json格式(*.json)");
    cv::FileStorage camcalibrate(path.toLocal8Bit().toStdString(),cv::FileStorage::WRITE);
    camcalibrate<<"cameraMatrix"<<_cameraMat<<"distCoeffs"<<_coeffs.t();
    camcalibrate.release();
}

