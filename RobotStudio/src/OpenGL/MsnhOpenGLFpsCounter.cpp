﻿#include <OpenGL/MsnhOpenGLFpsCounter.h>

FpsCounter::FpsCounter(QObject *parent):QObject(parent)
{
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &FpsCounter::timeout);
    timer->start(1000);
}

void FpsCounter::increase()
{
    fps++;
}

void FpsCounter::timeout()
{
    emit fpsChanged(fps);
    fps = 0;
}


