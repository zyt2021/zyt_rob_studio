﻿#include <OpenGL/MsnhOpenGLMaterial.h>

struct ShaderMaterialInfo {
    QVector4D	color;                  /* 16          // 0 */
    float		ambient;        /* 4           // 16 */
    float		diffuse;        /* 4           // 20 */
    float		specular;       /* 4           // 24 */
    float		shininess;      /* 4           // 28 */

    int         useDiffuseMap;  /* 4           // 32 */
    int         useSpecularMap; /* 4           // 36 */
    int         useBumpMap;     /* 4           // 40 */
    int         padding;        /* 4           // 44 */
};

static ShaderMaterialInfo shaderMaterialInfo;

OpenGLUBO *OpenGLMaterial::_materialInfo = nullptr;

OpenGLMaterial::OpenGLMaterial( Material * material, QObject* parent ) : QObject( parent )
{
    _host = material;

    this->diffuseTextureChanged( _host->getDiffuseTexture() );
    this->specularTextureChanged( _host->getSpecularTexture() );
    this->bumpTextureChanged( _host->getBumpTexture() );

    connect( _host, SIGNAL( diffuseTextureChanged( QSharedPointer<Texture>) ), this, SLOT( diffuseTextureChanged( QSharedPointer<Texture>) ) );
    connect( _host, SIGNAL( specularTextureChanged( QSharedPointer<Texture>) ), this, SLOT( specularTextureChanged( QSharedPointer<Texture>) ) );
    connect( _host, SIGNAL( bumpTextureChanged( QSharedPointer<Texture>) ), this, SLOT( bumpTextureChanged( QSharedPointer<Texture>) ) );
    connect( _host, SIGNAL( destroyed( QObject* ) ), this, SLOT( hostDestroyed( QObject* ) ) );

    setParent( parent );
}

OpenGLMaterial::~OpenGLMaterial()
{
    delete _materialInfo;
    _materialInfo = nullptr;
}

Material * OpenGLMaterial::getHost() const
{
    return(_host);
}

void OpenGLMaterial::bind()
{
    shaderMaterialInfo.useDiffuseMap	= false;
    shaderMaterialInfo.useSpecularMap	= false;
    shaderMaterialInfo.useBumpMap		= false;

    if ( _openGLDiffuseTexture )
        shaderMaterialInfo.useDiffuseMap = _openGLDiffuseTexture->bind();
    if ( _openGLSpecularTexture )
        shaderMaterialInfo.useSpecularMap = _openGLSpecularTexture->bind();
    if ( _openGLBumpTexture )
        shaderMaterialInfo.useBumpMap = _openGLBumpTexture->bind();

    shaderMaterialInfo.color	= _host->getColor();
    shaderMaterialInfo.ambient	= _host->getAmbient();
    shaderMaterialInfo.diffuse	= _host->getDiffuse();
    shaderMaterialInfo.specular	= _host->getSpecular();
    shaderMaterialInfo.shininess	= _host->getShininess();

    if ( _materialInfo == nullptr )
    {
        _materialInfo = new OpenGLUBO();
        _materialInfo->create();
        _materialInfo->bind();
        _materialInfo->allocate( MATERIAL_INFO_BINDING_POINT, NULL, sizeof(ShaderMaterialInfo) );
        _materialInfo->release();
    }
    _materialInfo->bind();
    _materialInfo->write( 0, &shaderMaterialInfo, sizeof(ShaderMaterialInfo) );
    _materialInfo->release();
}


void OpenGLMaterial::release()
{
    if ( _openGLDiffuseTexture )
        _openGLDiffuseTexture->release();
    if ( _openGLSpecularTexture )
        _openGLSpecularTexture->release();
    if ( _openGLBumpTexture )
        _openGLBumpTexture->release();

    shaderMaterialInfo.color            = QVector3D( 0, 0, 0 );
    shaderMaterialInfo.useDiffuseMap	= 0;
    shaderMaterialInfo.useSpecularMap	= 0;
    shaderMaterialInfo.useBumpMap		= 0;

    if ( _materialInfo == nullptr )
    {
        _materialInfo = new OpenGLUBO();
        _materialInfo->create();
        _materialInfo->bind();
        _materialInfo->allocate( 3, NULL, sizeof(ShaderMaterialInfo) );
        _materialInfo->release();
    }
    _materialInfo->bind();
    _materialInfo->write( 0, &shaderMaterialInfo, sizeof(ShaderMaterialInfo) );
    _materialInfo->release();
}


void OpenGLMaterial::diffuseTextureChanged( QSharedPointer<Texture> diffuseTexture )
{
    if ( diffuseTexture.isNull() )
        _openGLDiffuseTexture = nullptr;
    else if ( diffuseTexture->property( "OpenGLTexturePointer" ).isValid() )
        _openGLDiffuseTexture = diffuseTexture->property( "OpenGLTexturePointer" ).value<OpenGLTexture*>();
    else
        _openGLDiffuseTexture = new OpenGLTexture( diffuseTexture.data() );
}


void OpenGLMaterial::specularTextureChanged( QSharedPointer<Texture> specularTexture )
{
    if ( specularTexture.isNull() )
        _openGLSpecularTexture = nullptr;
    else if ( specularTexture->property( "OpenGLTexturePointer" ).isValid() )
        _openGLSpecularTexture = specularTexture->property( "OpenGLTexturePointer" ).value<OpenGLTexture*>();
    else
        _openGLSpecularTexture = new OpenGLTexture( specularTexture.data() );
}


void OpenGLMaterial::bumpTextureChanged( QSharedPointer<Texture> bumpTexture )
{
    if ( bumpTexture.isNull() )
        _openGLBumpTexture = nullptr;
    else if ( bumpTexture->property( "OpenGLTexturePointer" ).isValid() )
        _openGLBumpTexture = bumpTexture->property( "OpenGLTexturePointer" ).value<OpenGLTexture*>();
    else
        _openGLBumpTexture = new OpenGLTexture( bumpTexture.data() );
}


void OpenGLMaterial::hostDestroyed( QObject * )
{
    /* Commit suicide */
    delete this;
}


