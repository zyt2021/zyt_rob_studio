﻿#include <OpenGL/MsnhOpenGLMesh.h>

struct ShaderModelInfo {
    float       modelMat[16];   /* 64          // 0 */
    float       normalMat[16];  /* 64          // 64 */
    int         sizeFixed;      /* 4           // 128 */
    int         selected;       /* 4           // 132 */
    int         highlighted;    /* 4           // 136 */
    uint        pickingID;      /* 4           // 140 */
    float       alpha;          /* 4           // 144 */
};

static ShaderModelInfo shaderModelInfo;

OpenGLUBO *OpenGLMesh::_modelInfo = nullptr;

OpenGLMesh::OpenGLMesh( Mesh * mesh, QObject* parent ) : QObject( parent )
{
    _host               = mesh;
    _sizeFixed          = false;
    _pickingID          = 0;
    _vao                = nullptr;
    _vbo                = nullptr;
    _ebo                = nullptr;
    if ( _host->getMaterial() )
        _openGLMaterial = new OpenGLMaterial( _host->getMaterial(), this);
    else
        _openGLMaterial = nullptr;

    connect( _host, SIGNAL( materialChanged( Material* ) ), this, SLOT( materialChanged( Material* ) ) );
    connect( _host, SIGNAL( geometryChanged( QVector<Vertex>, QVector<uint32_t>) ), this, SLOT( geometryChanged( QVector<Vertex>, QVector<uint32_t>) ) );
    connect( _host, SIGNAL( destroyed( QObject* ) ), this, SLOT( hostDestroyed( QObject* ) ) );
}


OpenGLMesh::~OpenGLMesh()
{
    delete _modelInfo;
    _modelInfo = nullptr;

    this->destroy();
}


Mesh * OpenGLMesh::getHost() const
{
    return(_host);
}


void OpenGLMesh::create()
{
    this->destroy();

    _vao = new QOpenGLVertexArrayObject(this);
    _vao->create();
    _vao->bind();
    _vbo = new QOpenGLBuffer( QOpenGLBuffer::VertexBuffer );
    _vbo->create();
    _vbo->bind();
    if ( _host->getVertices().size() )
        _vbo->allocate( &_host->getVertices()[0], int(sizeof(Vertex) * _host->getVertices().size() ) );
    _ebo = new QOpenGLBuffer( QOpenGLBuffer::IndexBuffer );
    _ebo->create();
    _ebo->bind();
    if ( _host->getIndices().size() )
        _ebo->allocate( &_host->getIndices()[0], int(sizeof(uint32_t) * _host->getIndices().size() ) );

    glFuncs = QOpenGLContext::currentContext()->versionFunctions<MSNH_OPENGL>();
    glFuncs->glEnableVertexAttribArray( 0 );
    glFuncs->glEnableVertexAttribArray( 1 );
    glFuncs->glEnableVertexAttribArray( 2 );
    glFuncs->glEnableVertexAttribArray( 3 );
    glFuncs->glEnableVertexAttribArray( 4 );
    glFuncs->glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof( Vertex, position ) );
    glFuncs->glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof( Vertex, normal ) );
    glFuncs->glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof( Vertex, tangent ) );
    glFuncs->glVertexAttribPointer( 3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof( Vertex, bitangent ) );
    glFuncs->glVertexAttribPointer( 4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof( Vertex, texCoords ) );

    _vao->release();
}

void OpenGLMesh::commit()
{
    QMatrix4x4 modelMat = _host->getGlobalModelMatrix();

    memcpy( shaderModelInfo.modelMat, modelMat.constData(), 64 );
    memcpy( shaderModelInfo.normalMat, QMatrix4x4( modelMat.normalMatrix() ).constData(), 64 );
    shaderModelInfo.sizeFixed	= this->_sizeFixed;
    shaderModelInfo.selected	= _host->getSelected();
    shaderModelInfo.highlighted	= _host->getHighlighted();
    shaderModelInfo.pickingID	= this->_pickingID;    //picking ID写入 UBO区域
    shaderModelInfo.alpha       = _host->getAlpha();


    if ( _modelInfo == 0 )
    {
        _modelInfo = new OpenGLUBO();
        _modelInfo->create();
        _modelInfo->bind();
        _modelInfo->allocate( MODEL_INFO_BINDING_POINT, NULL, sizeof(ShaderModelInfo) );
        _modelInfo->release();
    }
    _modelInfo->bind();
    _modelInfo->write( 0, &shaderModelInfo, sizeof(ShaderModelInfo) );
    _modelInfo->release();
}


void OpenGLMesh::render(bool pickingPass)
{
    if ( !_host->getVisible() )
        return;
    if ( _vao == 0 || _vbo == 0 || _ebo == 0 )
        create();
    //render的时候，设置pick ID, 反应在Opengl上是颜色信息
    commit();

    if ( !pickingPass && _host->getWireFrameMode() )
    {
        glFuncs->glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    }
    else  if( _openGLMaterial )
    {
        _openGLMaterial->bind();
    }

    _vao->bind();
    if ( _host->getMeshType() == Mesh::TRIANGLE )
        glFuncs->glDrawElements( GL_TRIANGLES, (GLsizei) _host->getIndices().size(), GL_UNSIGNED_INT, 0 );
    else if ( _host->getMeshType() == Mesh::LINE )
        glFuncs->glDrawElements( GL_LINES, (GLsizei) _host->getIndices().size(), GL_UNSIGNED_INT, 0 );
    else if ( _host->getMeshType() == Mesh::LINE_LOOP )
        glFuncs->glDrawElements( GL_LINE_STRIP_ADJACENCY_ARB, (GLsizei) _host->getIndices().size(), GL_UNSIGNED_INT, 0 );
    else
        glFuncs->glDrawElements( GL_POINTS, (GLsizei) _host->getIndices().size(), GL_UNSIGNED_INT, 0 );

    glFuncs->glActiveTexture(GL_TEXTURE0);
    _vao->release();

    if ( !pickingPass && _host->getWireFrameMode() )
        glFuncs->glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    else if ( _openGLMaterial )
        _openGLMaterial->release();
}


void OpenGLMesh::destroy()
{
    if ( _vao )
        delete _vao;
    if ( _vbo )
        delete _vbo;
    if ( _ebo )
        delete _ebo;

    _vao	= nullptr;
    _vbo	= nullptr;
    _ebo	= nullptr;

}


void OpenGLMesh::setSizeFixed( bool sizeFixed )
{
    _sizeFixed = sizeFixed;
}


void OpenGLMesh::setPickingID( uint id )
{
    _pickingID = id;
}


void OpenGLMesh::childEvent( QChildEvent * e )
{
    if ( e->removed() )
    {
        if ( e->child() == _openGLMaterial )
            _openGLMaterial = 0;
    }
}


void OpenGLMesh::materialChanged( Material * material )
{
    if ( material == nullptr )
        _openGLMaterial = nullptr;
    else
        _openGLMaterial = new OpenGLMaterial( material );
}


void OpenGLMesh::geometryChanged( const QVector<Vertex> &, const QVector<uint32_t> & )
{
    this->create();
}


void OpenGLMesh::hostDestroyed( QObject * )
{
    /* Commit suicide */
    delete this;
}


