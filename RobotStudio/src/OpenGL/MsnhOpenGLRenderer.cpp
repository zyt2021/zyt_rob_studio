﻿#include <OpenGL/MsnhOpenGLRenderer.h>

OpenGLRenderer::OpenGLRenderer( QObject* parent ) : QObject( parent )
{
    _log		= "";
}


OpenGLRenderer::OpenGLRenderer(const OpenGLRenderer & , QObject *parent) : QObject( parent )
{
    _log		= "";
}

OpenGLRenderer::~OpenGLRenderer()
{
    delete _pickingPassFBO;
    _pickingPassFBO = nullptr;
}


bool OpenGLRenderer::hasErrorLog()
{
    return(_log != "");
}


QString OpenGLRenderer::errorLog()
{
    QString tmp = _log;
    _log = "";
    return(tmp);
}


bool OpenGLRenderer::reloadShaders()
{
    if ( _pickingShader )
    {
        delete _pickingShader;
        _pickingShader = nullptr;
    }

    if ( _basicShader )
    {
        delete _basicShader;
        _basicShader = nullptr;
    }

    if ( _pointCloudShader )
    {
        delete _pointCloudShader;
        _pointCloudShader = nullptr;
    }

    if ( _phongShader )
    {
        delete _phongShader;
        _phongShader = nullptr;
    }

    if( _skyShader )
    {
        delete  _skyShader;
        _skyShader  = nullptr;
    }

    _pickingShader      = loadShaderFromFile( ":/shaders/picking.vert", ":/shaders/picking.frag" );
    _pointCloudShader	= loadShaderFromFile( ":/shaders/pointClouds.vert", ":/shaders/pointClouds.frag" );
    _basicShader        = loadShaderFromFile( ":/shaders/basic.vert", ":/shaders/basic.frag" );
    _phongShader        = loadShaderFromFile( ":/shaders/phong.vert", ":/shaders/phong.frag" );
    _skyShader          = loadShaderFromFile( ":/shaders/sky.vert", ":/shaders/sky.frag" );

    if ( _phongShader )
    {
        _phongShader->bind();
        _phongShader->setUniformValue( "diffuseMap", 0 );
        _phongShader->setUniformValue( "specularMap", 1 );
        _phongShader->setUniformValue( "bumpMap", 2 );
    }

    return(_pickingShader && _basicShader && _phongShader && _skyShader && _pointCloudShader);
}


void OpenGLRenderer::reloadFrameBuffers()
{
    if ( _pickingPassFBO )
        delete _pickingPassFBO;
    int data[4];
    glGetIntegerv( GL_VIEWPORT, data );
    _pickingPassFBO = new QOpenGLFramebufferObject( data[2], data[3], QOpenGLFramebufferObject::CombinedDepthStencil );
}


uint32_t OpenGLRenderer::pickingPass( OpenGLScene * openGLScene, QPoint cursorPos )
{
    if ( _pickingPassFBO == 0 )
        reloadFrameBuffers();

    int data[4];
    glGetIntegerv( GL_VIEWPORT, data );
    if ( data[2] != _pickingPassFBO->width() || data[3] != _pickingPassFBO->height() )
        reloadFrameBuffers();

    if ( cursorPos.x() < 0 || cursorPos.y() < 0 || cursorPos.x() >= _pickingPassFBO->width() || cursorPos.y() >= _pickingPassFBO->height() )
        return(0);

    _pickingPassFBO->bind();

    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    glEnable( GL_DEPTH_TEST );

    if ( _pickingShader )
    {
        _pickingShader->bind(); //使用获取屏幕中对象的shader
        openGLScene->renderModels(true ); //场景模型mesh
        openGLScene->renderLights(); //点光，手电筒等mesh
        openGLScene->renderAxis(true);//Gizmo的mesh
    }

    glDisable( GL_DEPTH_TEST );
    _pickingPassFBO->release();

    QColor rgb = _pickingPassFBO->toImage().pixelColor( cursorPos );
    return(rgb.red() + rgb.green() * 256 + rgb.blue() * 256 * 256);
}


void OpenGLRenderer::render( OpenGLScene* openGLScene )
{
    glClearColor( 0.7f, 0.7f, 0.7f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glDisable( GL_FRAMEBUFFER_SRGB ); /* Gamma矫正 */

    glPointSize(EngineUtil::pointSize);

    if(EngineUtil::enableAnti)
    {
        glEnable(GL_MULTISAMPLE);
    }
    else
    {
        glDisable(GL_MULTISAMPLE);
    }

    glCullFace(GL_FRONT);

    glEnable( GL_DEPTH_TEST );

    if( _skyShader )
    {
        openGLScene->getSky()->setAspectRatio(_screenSize);
        _skyShader->bind();
        _skyShader->setUniformValue("resolution", openGLScene->getSky()->getAspectRatio());
        _skyShader->setUniformValue("lightPos",openGLScene->getSky()->getLightPos());
        openGLScene->renderSky();
    }

    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    if ( _basicShader )
    {
        _basicShader->bind();
        openGLScene->renderGridlines();
        openGLScene->renderLights();
        openGLScene->renderSelectAndHighLight();
    }


    if(_pointCloudShader)
    {
        _pointCloudShader->bind();
        openGLScene->renderCloudPoints(false);
    }

    if(EngineUtil::enableGama)
    {
        glEnable( GL_FRAMEBUFFER_SRGB ); /* Gamma矫正 */
    }

    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    if ( _phongShader )
    {
        _phongShader->bind();
        openGLScene->renderModels();
    }

    if ( _basicShader )
    {
        _basicShader->bind();
        // 置顶渲染
        openGLScene->renderCurves(false);// 一般渲染
        openGLScene->renderAxis(false);
        openGLScene->renderCamera();
        openGLScene->renderCloudPoints();
        openGLScene->renderCurves(true);
    }
}


QOpenGLShaderProgram * OpenGLRenderer::loadShaderFromFile(
    QString vertexShaderFilePath,
    QString fragmentShaderFilePath,
    QString geometryShaderFilePath )
{
    QFile	glslDefineFile( ":/shaders/define.glsl" );
    QFile	glslUBOFile( ":/shaders/ubo.glsl" );
    if ( !glslDefineFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        _log += "Failed to load glsl definitions: " + glslDefineFile.errorString();
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to load glsl definitions:" << glslDefineFile.errorString();
        return (nullptr);
    }
    if ( !glslUBOFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        _log += "Failed to load glsl UBOs: " + glslUBOFile.errorString();
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to load glsl UBOs:" << glslUBOFile.errorString();
        return (nullptr);
    }

    QFile	vertexShaderFile( vertexShaderFilePath );
    QFile	fragmentShaderFile( fragmentShaderFilePath );
    QFile	geometryShaderFile( geometryShaderFilePath );
    if ( !vertexShaderFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        _log += "Failed to open file " + vertexShaderFilePath;
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to open file" + vertexShaderFilePath;
        return (nullptr);
    }
    if ( !fragmentShaderFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        _log += "Failed to open file " + fragmentShaderFilePath;
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to open file" + fragmentShaderFilePath;
        return (nullptr);
    }
    if ( geometryShaderFilePath != "" && !geometryShaderFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        _log += "Failed to open file " + geometryShaderFilePath;
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to open file" + geometryShaderFilePath;
        return (nullptr);
    }

    QByteArray	glslDefineCode	= glslDefineFile.readAll();
    QByteArray	glslUBOCode	= glslUBOFile.readAll();

#ifdef USE_OPENG_3_3
    QByteArray vertexShaderCode = "#version 330 core\n"
        + glslDefineCode + "\n"
        + glslUBOCode + "\n"
        + vertexShaderFile.readAll();
    QByteArray fragmentShaderCode = "#version 330 core\n"
        + glslDefineCode + "\n"
        + glslUBOCode + "\n"
        + fragmentShaderFile.readAll();
    QByteArray geometryShaderCode = "#version 330 core\n"
        + glslDefineCode + "\n"
        + glslUBOCode + "\n"
        + (geometryShaderFilePath != "" ? geometryShaderFile.readAll() : "");
#endif

#ifdef USE_OPENG_4_3
    QByteArray vertexShaderCode = "#version 430 core\n"
        + glslDefineCode + "\n"
        + glslUBOCode + "\n"
        + vertexShaderFile.readAll();
    QByteArray fragmentShaderCode = "#version 430 core\n"
        + glslDefineCode + "\n"
        + glslUBOCode + "\n"
        + fragmentShaderFile.readAll();
    QByteArray geometryShaderCode = "#version 430 core\n"
        + glslDefineCode + "\n"
        + glslUBOCode + "\n"
        + (geometryShaderFilePath != "" ? geometryShaderFile.readAll() : "");
#endif

    QOpenGLShaderProgram* shader = new QOpenGLShaderProgram( this );
    if ( !shader->addShaderFromSourceCode( QOpenGLShader::Vertex, vertexShaderCode ) )
    {
        _log += "Failed to compile vertex shader: " + shader->log();
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to compile vertex shader:" + shader->log();
        return (nullptr);
    }
    if ( !shader->addShaderFromSourceCode( QOpenGLShader::Fragment, fragmentShaderCode ) )
    {
        _log += "Failed to compile fragment shader: " + shader->log();
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to compile fragment shader:" + shader->log();
        return (nullptr);
    }
    if ( geometryShaderFilePath != "" && !shader->addShaderFromSourceCode( QOpenGLShader::Geometry, geometryShaderCode ) )
    {
        _log += "Failed to compile geometry shader: " + shader->log();
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to compile geometry shader:" + shader->log();
        return (nullptr);
    }
    if ( !shader->link() )
    {
        _log += "Failed to link shaders: " + shader->log();
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "Failed to link shaders:" + shader->log();
        return (nullptr);
    }
    OpenGLUBO::bindUniformBlock( shader ); //全局UBO绑定
    return(shader);
}

void OpenGLRenderer::setScreenSize(const QVector2D &screenSize)
{
    _screenSize = screenSize;
}


