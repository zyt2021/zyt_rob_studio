﻿#include <OpenGL/MsnhOpenGLScene.h>
#include <Core/MsnhSceneLoader.h>

struct ShaderAxisInfo {                         /* struct size: 64 */
    // base align  // aligned offset
    float hostModelMat[16];                     /* 64          // 0 */
} shaderAxisInfo;

struct ShaderCameraInfo {                       /* struct size: 144 */
    // base align  // aligned offset
    float		projMat[16];                    /* 64          // 0 */
    float		viewMat[16];                    /* 64          // 64 */
    QVector4D	cameraPos;                      /* 16          // 128 */
} shaderCameraInfo;

struct ShaderAmbientLight {                     /* struct size: 16 */
    // base align  // aligned offset
    QVector4D color;                            /* 16          // 0 */
};

struct ShaderDirectionalLight {                 /* struct size: 32 */
    // base align  // aligned offset
    QVector4D	color;                          /* 16          // 0 */
    QVector4D	direction;                      /* 16          // 16 */
};

struct ShaderPointLight {                        /* struct size: 48 */
    // base align  // aligned offset
    QVector4D	color;                          /* 16          // 0 */
    QVector4D	pos;                            /* 16          // 16 */
    QVector4D	attenuation;                    /* 16          // 32 */
};

struct ShaderSpotLight {                         /* struct size: 80 */
    // base align  // aligned offset */
    QVector4D	color;                          /* 16          // 0 */
    QVector4D	pos;                            /* 16          // 16 */
    QVector4D	direction;                      /* 16          // 32 */
    QVector4D	attenuation;                    /* 16          // 48 */
    QVector4D	cutOff;                         /* 16          // 64 */
};

struct ShaderlightInfo {                            /* struct size: 1424 */
    // base align  // aligned offset
    int                     ambientLightNum;        /* 4           // 0 */
    int                     directionalLightNum;    /* 4           // 4 */
    int                     pointLightNum;          /* 4           // 8 */
    int                     spotLightNum;           /* 4           // 12 */
    ShaderAmbientLight      ambientLight[8];        /* 16          // 16 */
    ShaderDirectionalLight  directionalLight[8];    /* 32          // 144 */
    ShaderPointLight        pointLight[8];          /* 48          // 400 */
    ShaderSpotLight         spotLight[8];           /* 80          // 784 */
};


static ShaderlightInfo shaderlightInfo;

OpenGLUBO	*OpenGLScene::_cameraInfo	= 0;
OpenGLUBO	*OpenGLScene::_lightInfo	= 0;

OpenGLScene::OpenGLScene(QObject *parent):QObject(parent)
{

    _host =  new Scene(this);
    _sky  =  new Sky(this);
    //GIZMO：
    //  |Transfrom:
    //    00 x
    //    01 y
    //    02 z
    //    03 xy
    //    04 yz
    //    05 zx
    //  |Scale:
    //    06 x
    //    07 y
    //    08 z
    //    09 xy
    //    10 yz
    //    11 zx
    //    12 xyz
    //  |Rotate:
    //    13 x
    //    14 y
    //    15 z
    //
    this->gizmoAdded( _host->getGizmo() );

    this->cameraAdded( _host->getCamera() );

    // add new item step 0
    for ( int i = 0; i < _host->getGridlines().size(); i++ )
    {
        this->gridlineAdded( _host->getGridlines()[i] );
    }

    for ( int i = 0; i < _host->getPointLights().size(); i++ )
    {
        this->lightAdded( _host->getPointLights()[i] );
    }

    for ( int i = 0; i < _host->getSpotLights().size(); i++ )
    {
        this->lightAdded( _host->getSpotLights()[i] );
    }

    for ( int i = 0; i < _host->getModels().size(); i++ )
    {
        this->modelAdded( _host->getModels()[i] );
    }

    for (int i = 0; i < _host->getCurves().size(); ++i)
    {
        this->curveAdded( _host->getCurves()[i]);
    }

    for (int i = 0; i < _host->getTfs().size(); ++i)
    {
        this->tfAdded(_host->getTfs()[i]);
    }

    for (int i = 0; i < _host->getPointsClouds().size(); ++i)
    {
        this->pointsCloudAdded(_host->getPointsClouds()[i]);
    }

    for (int i = 0; i < _host->getRobots().size(); ++i)
    {
        this->robotAdded(_host->getRobots()[i]);
    }

    connect( _host, &Scene::gridlineAdded, this, &OpenGLScene::gridlineAdded );
    connect( _host, &Scene::lightAdded, this, &OpenGLScene::lightAdded );
    connect( _host, &Scene::modelAdded, this, &OpenGLScene::modelAdded );
    connect( _host, &Scene::destroyed, this, &OpenGLScene::hostDestroyed);
    connect( _host, &Scene::curveAdded, this, &OpenGLScene::curveAdded);
    connect( _host, &Scene::tfAdded, this, &OpenGLScene::tfAdded);
    connect( _host, &Scene::tfGroupAdded, this, &OpenGLScene::tfGroupAdded);
    connect( _host, &Scene::pointsCloudAdded, this, &OpenGLScene::pointsCloudAdded);
    connect( _host, &Scene::robotAdded, this, &OpenGLScene::robotAdded);
}

OpenGLScene::OpenGLScene(Scene *scene, QObject *parent):QObject(parent)
{
    _host =  scene;
    _host->setParent(this);
    _sky  =  new Sky(this);
    //GIZMO：
    //  |Transfrom:
    //    00 x
    //    01 y
    //    02 z
    //    03 xy
    //    04 yz
    //    05 zx
    //  |Scale:
    //    06 x
    //    07 y
    //    08 z
    //    09 xy
    //    10 yz
    //    11 zx
    //    12 xyz
    //  |Rotate:
    //    13 x
    //    14 y
    //    15 z
    //
    this->gizmoAdded( _host->getGizmo() );

    this->cameraAdded( _host->getCamera() );

    // add new item step 1
    for ( int i = 0; i < _host->getGridlines().size(); i++ )
    {
        this->gridlineAdded( _host->getGridlines()[i] );
    }

    for (int i = 0; i < _host->getCurves().size(); ++i)
    {
        this->curveAdded(_host->getCurves()[i]);
    }

    for (int i = 0; i < _host->getTfs().size(); ++i)
    {
        this->tfAdded(_host->getTfs()[i]);
    }

    for (int i = 0; i < _host->getPointsClouds().size(); ++i)
    {
        this->pointsCloudAdded(_host->getPointsClouds()[i]);
    }

    for ( int i = 0; i < _host->getPointLights().size(); i++ )
    {
        this->lightAdded( _host->getPointLights()[i] );
    }

    for ( int i = 0; i < _host->getSpotLights().size(); i++ )
    {
        this->lightAdded( _host->getSpotLights()[i] );
    }

    for ( int i = 0; i < _host->getModels().size(); i++ )
    {
        this->modelAdded( _host->getModels()[i] );
    }

    for (int i = 0; i < _host->getRobots().size(); ++i)
    {
        this->robotAdded(_host->getRobots()[i]);
    }

    connect( _host, &Scene::gridlineAdded, this, &OpenGLScene::gridlineAdded );
    connect( _host, &Scene::lightAdded, this, &OpenGLScene::lightAdded );
    connect( _host, &Scene::modelAdded, this, &OpenGLScene::modelAdded );
    connect( _host, &Scene::destroyed, this, &OpenGLScene::hostDestroyed);
    connect( _host, &Scene::curveAdded, this, &OpenGLScene::curveAdded);
    connect( _host, &Scene::tfAdded, this, &OpenGLScene::tfAdded);
    connect( _host, &Scene::tfGroupAdded, this, &OpenGLScene::tfGroupAdded);
    connect( _host, &Scene::pointsCloudAdded, this, &OpenGLScene::pointsCloudAdded);
    connect( _host, &Scene::robotAdded, this, &OpenGLScene::robotAdded);
}

OpenGLScene::~OpenGLScene()
{
    delete _cameraInfo;
    _cameraInfo = nullptr;

    delete _lightInfo;
    _lightInfo  = nullptr;

    //    delete _host;
    //    _host = nullptr;
}


Scene * OpenGLScene::getHost() const
{
    return(_host);
}


OpenGLMesh * OpenGLScene::pick( uint32_t pickingID )
{
    if ( pickingID >= MODELS_START_ID && pickingID - MODELS_START_ID < (uint32_t) _normalMeshes.size() )
        return(_normalMeshes[pickingID - MODELS_START_ID]);
    else if ( pickingID >= LIGHT_START_ID && pickingID - LIGHT_START_ID < (uint32_t) _lightMeshes.size() )
        return(_lightMeshes[pickingID - LIGHT_START_ID]);
    else if ( pickingID >= GIZMO_START_ID && pickingID - GIZMO_START_ID < (uint32_t) _gizmoMeshes.size() )
        return(_gizmoMeshes[pickingID - GIZMO_START_ID]);
    return (nullptr);
}


void OpenGLScene::renderAxis(bool pickPass)
{
    if ( _host->getGizmo()->getAlwaysOnTop() )
        glClear( GL_DEPTH_BUFFER_BIT );

    if(pickPass) //放大gizmo的触发大小
    {
        for ( int i = 0; i < _gizmoMeshes.size(); i++ )
        {

            //放大gizmo的触发大小
            if(i==0)// T_x
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(1,2,2));
            if(i==1)// T_y
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(2,1,2));
            if(i==2)// T_z
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(2,2,1));

            if(i==6)// S_x
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(2,1,1));
            if(i==7)// S_y
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(1,2,1));
            if(i==8)// S_z
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(1,1,2));
            if(i==12)// S_xyz
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(2,2,2));

            if(i==13)
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(2,1,1));
            if(i==14)
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(1,2,1));
            if(i==15)
                _gizmoMeshes[i]->getHost()->setScaling(QVector3D(1,1,2));

            _gizmoMeshes[i]->render();
        }
    }
    else //显示gizmo
    {
        for ( int i = 0; i < _gizmoMeshes.size(); i++ )
        {
            _gizmoMeshes[i]->getHost()->setScaling(QVector3D(1,1,1));
            _gizmoMeshes[i]->render();
        }
    }
}

void OpenGLScene::renderGridlines()
{
    for ( int i = 0; i < _gridlineMeshes.size(); i++ )
    {
        _gridlineMeshes[i]->render();
    }
}

void OpenGLScene::renderCurves(bool onTopRender)
{

    ///TODO: 此处_host->getCurves()为所有以scene为parent的curves 2021/07/11
    ///现Robot内的是否指定参数如何获取

    for (int i = 0; i < _curveMeshes.size(); ++i)
    {
        if(onTopRender)
        {
            if(_curveMeshes[i]->getHost()->getAlwaysOnTop())
            {
                _curveMeshes[i]->render();
            }
        }
        else
        {
            if(!_curveMeshes[i]->getHost()->getAlwaysOnTop())
            {
                _curveMeshes[i]->render();
            }
        }
    }
}

void OpenGLScene::gizmoAdded( AbstractGizmo* gizmo )
{
    for ( int i = 0; i < gizmo->getMarkers().size(); i++ )
    {
        _gizmoMeshes.push_back( new OpenGLMesh( gizmo->getMarkers()[i], this ) );
        _gizmoMeshes.back()->setSizeFixed( true );
        _gizmoMeshes.back()->setPickingID( GIZMO_START_ID + i );
    }
}

void OpenGLScene::cameraAdded(Camera *camera)
{
    _cameraCenterMesh = new OpenGLMesh(camera->getCenterMarker(), this);
    _cameraCenterMesh->setSizeFixed( true );
}

void OpenGLScene::renderLights()
{
    for ( int i = 0; i < _lightMeshes.size(); i++ )
    {
        _lightMeshes[i]->setPickingID( LIGHT_START_ID + i );
        _lightMeshes[i]->render();
    }
}


void OpenGLScene::renderModels( bool pickingPass )
{
    for ( int i = 0; i < _normalMeshes.size(); i++ )
    {
        _normalMeshes[i]->setPickingID( MODELS_START_ID + i );
        _normalMeshes[i]->render( pickingPass );
    }
}

void OpenGLScene::renderCloudPoints(bool renderPureColor)
{

    // shader basic 渲染 pure color
    // shader point cloud 渲染非pure color

    //  basic attach
    //  renderCloudPoints(true)
    //  point cloud attach
    //  renderCloudPoints(false)


    for (int i = 0; i < _pointsCloudMeshes.size(); ++i)
    {
        if(renderPureColor)
        {

            if(_host->getPointsClouds()[i]->isPureColor())
            {
                _pointsCloudMeshes[i]->render();
            }
        }
        else
        {
            if(!_host->getPointsClouds()[i]->isPureColor())
            {
                _pointsCloudMeshes[i]->render();
            }
        }
    }
}

void OpenGLScene::renderSky()
{
    _sky->render();
}

void OpenGLScene::renderSelectAndHighLight()
{
    for ( int i = 0; i < _normalMeshes.size(); i++ )
    {
        if(_normalMeshes[i]->getHost() == Mesh::getHighlightedObject())
        {
            QVector3D color ;
            if(_normalMeshes[i]->getHost()->getMaterial()) //material可能被删除
            {
                color = _normalMeshes[i]->getHost()->getMaterial()->getColor();
                _normalMeshes[i]->getHost()->getMaterial()->setColor(QVector3D(EngineUtil::highlightColor.red()/255.0f,
                                                                               EngineUtil::highlightColor.green()/255.0f,
                                                                               EngineUtil::highlightColor.blue()/255.0f));
            }

            _normalMeshes[i]->render(false);

            if(_normalMeshes[i]->getHost()->getMaterial()) //material可能被删除
            {
                _normalMeshes[i]->getHost()->getMaterial()->setColor(color);
            }
        }

        if(_normalMeshes[i]->getHost()->getSelected())
        {
            QVector3D color;
            if(_normalMeshes[i]->getHost()->getMaterial()) //material可能被删除
            {
                color = _normalMeshes[i]->getHost()->getMaterial()->getColor();
                _normalMeshes[i]->getHost()->getMaterial()->setColor(QVector3D(EngineUtil::selectColor.red()/255.0f,
                                                                               EngineUtil::selectColor.green()/255.0f,
                                                                               EngineUtil::selectColor.blue()/255.0f));
            }

            _normalMeshes[i]->render(false);

            if(_normalMeshes[i]->getHost()->getMaterial()) //material可能被删除
            {
                _normalMeshes[i]->getHost()->getMaterial()->setColor(color);
            }
        }

        if(_normalMeshes[i]->getHost()->getCollisionSelected())
        {
            QVector3D color;
            if(_normalMeshes[i]->getHost()->getMaterial()) //material可能被删除
            {
                color = _normalMeshes[i]->getHost()->getMaterial()->getColor();
                _normalMeshes[i]->getHost()->getMaterial()->setColor(QVector3D(255.0f,
                                                                               255.0f,
                                                                               0.0f));
            }

            _normalMeshes[i]->render(false);

            if(_normalMeshes[i]->getHost()->getMaterial()) //material可能被删除
            {
                _normalMeshes[i]->getHost()->getMaterial()->setColor(color);
            }
        }
    }
}

void OpenGLScene::renderCamera()
{
    if(_host->getCamera()->getMeshVisible() && _host->getCamera()->isOrbitCam()) //mesh可见且是orbit相机
    {
        _cameraCenterMesh->getHost()->setPosition(_host->getCamera()->getFocusCenter());
        _cameraCenterMesh->render();
    }
}

// add new item step 2


void OpenGLScene::commitCameraInfo()
{
    if ( !_host->getCamera() )
        return;

    // projection
    memcpy( shaderCameraInfo.projMat, _host->getCamera()->getProjectionMatrix().constData(), 64 );
    // view
    memcpy( shaderCameraInfo.viewMat, _host->getCamera()->getViewMatrix().constData(), 64 );
    //model
    shaderCameraInfo.cameraPos = _host->getCamera()->getPosition();

    if ( _cameraInfo == nullptr )
    {
        _cameraInfo = new OpenGLUBO();
        _cameraInfo->create();
        _cameraInfo->bind();
        _cameraInfo->allocate( CAMERA_INFO_BINDING_POINT, NULL, sizeof(ShaderCameraInfo) );
        _cameraInfo->release();
    }

    _cameraInfo->bind();
    _cameraInfo->write( 0, &shaderCameraInfo, sizeof(ShaderCameraInfo) );
    _cameraInfo->release();
}


void OpenGLScene::commitLightInfo()
{
    int ambientLightNum = 0, directionalLightNum = 0, pointLightNum = 0, spotLightNum = 0;
    for ( int i = 0; i < _host->getAmbientLights().size(); i++ )
        if ( _host->getAmbientLights()[i]->getEnabled() )
        {
            shaderlightInfo.ambientLight[ambientLightNum].color             = _host->getAmbientLights()[i]->getColor() * _host->getAmbientLights()[i]->getIntensity();
            ambientLightNum++;
        }
    for ( int i = 0; i < _host->getDirectionalLights().size(); i++ )
        if ( _host->getDirectionalLights()[i]->getEnabled() )
        {
            shaderlightInfo.directionalLight[directionalLightNum].color     = _host->getDirectionalLights()[i]->getColor() * _host->getDirectionalLights()[i]->getIntensity();
            shaderlightInfo.directionalLight[directionalLightNum].direction = _host->getDirectionalLights()[i]->getDirection();
            directionalLightNum++;
        }
    for ( int i = 0; i < _host->getPointLights().size(); i++ )
        if ( _host->getPointLights()[i]->getEnabled() )
        {
            shaderlightInfo.pointLight[pointLightNum].color             = _host->getPointLights()[i]->getColor() * _host->getPointLights()[i]->getIntensity();
            shaderlightInfo.pointLight[pointLightNum].pos               = _host->getPointLights()[i]->getPosition();
            shaderlightInfo.pointLight[pointLightNum].attenuation[0]	= _host->getPointLights()[i]->getEnableAttenuation();
            shaderlightInfo.pointLight[pointLightNum].attenuation[1]	= _host->getPointLights()[i]->getAttenuationQuadratic();
            shaderlightInfo.pointLight[pointLightNum].attenuation[2]	= _host->getPointLights()[i]->getAttenuationLinear();
            shaderlightInfo.pointLight[pointLightNum].attenuation[3]	= _host->getPointLights()[i]->getAttenuationConstant();
            pointLightNum++;
        }
    for ( int i = 0; i < _host->getSpotLights().size(); i++ )
        if ( _host->getSpotLights()[i]->getEnabled() )
        {
            shaderlightInfo.spotLight[spotLightNum].color               = _host->getSpotLights()[i]->getColor() * _host->getSpotLights()[i]->getIntensity();
            shaderlightInfo.spotLight[spotLightNum].pos                 = _host->getSpotLights()[i]->getPosition();
            shaderlightInfo.spotLight[spotLightNum].direction           = _host->getSpotLights()[i]->getDirection();
            shaderlightInfo.spotLight[spotLightNum].attenuation[0]      = _host->getSpotLights()[i]->getEnableAttenuation();
            shaderlightInfo.spotLight[spotLightNum].attenuation[1]      = _host->getSpotLights()[i]->getAttenuationQuadratic();
            shaderlightInfo.spotLight[spotLightNum].attenuation[2]      = _host->getSpotLights()[i]->getAttenuationLinear();
            shaderlightInfo.spotLight[spotLightNum].attenuation[3]      = _host->getSpotLights()[i]->getAttenuationConstant();
            shaderlightInfo.spotLight[spotLightNum].cutOff[0]           = (float) cos( rad( _host->getSpotLights()[i]->getInnerCutOff() ) );
            shaderlightInfo.spotLight[spotLightNum].cutOff[1]           = (float) cos( rad( _host->getSpotLights()[i]->getOuterCutOff() ) );
            spotLightNum++;
        }

    shaderlightInfo.ambientLightNum		= ambientLightNum;
    shaderlightInfo.directionalLightNum         = directionalLightNum;
    shaderlightInfo.pointLightNum		= pointLightNum;
    shaderlightInfo.spotLightNum		= spotLightNum;

    if ( _lightInfo == nullptr)
    {
        _lightInfo = new OpenGLUBO();
        _lightInfo->create();
        _lightInfo->bind();
        _lightInfo->allocate( LIGHT_INFO_BINDING_POINT, NULL, sizeof(ShaderlightInfo) );
        _lightInfo->release();
    }
    _lightInfo->bind();
    _lightInfo->write( 0, &shaderlightInfo, sizeof(ShaderlightInfo) );
    _lightInfo->release();
}

// add new item step 3
void OpenGLScene::childEvent( QChildEvent * e )
{
    if ( e->removed() )
    {
        for ( int i = 0; i < _gridlineMeshes.size(); i++ )
        {
            if ( _gridlineMeshes[i] == e->child() )
                _gridlineMeshes.removeAt( i );
        }

        for ( int i = 0; i < _curveMeshes.size(); i++ )
        {
            if ( _curveMeshes[i] == e->child() )
                _curveMeshes.removeAt( i );
        }

        for ( int i = 0; i < _lightMeshes.size(); i++ )
        {
            if ( _lightMeshes[i] == e->child() )
                _lightMeshes.removeAt( i );
        }

        for ( int i = 0; i < _normalMeshes.size(); i++ )
        {
            if ( _normalMeshes[i] == e->child() )
                _normalMeshes.removeAt( i );
        }

        for ( int i = 0; i < _pointsCloudMeshes.size(); i++ )
        {
            if ( _pointsCloudMeshes[i] == e->child() )
                _pointsCloudMeshes.removeAt( i );
        }
    }
}

void OpenGLScene::setHost(Scene *host)
{
    _host = host;
}

Sky *OpenGLScene::getSky() const
{
    return _sky;
}


void OpenGLScene::gridlineAdded( Gridline * gridline )
{
    _gridlineMeshes.push_back( new OpenGLMesh( gridline->getMarker(), this ) );
}

void OpenGLScene::curveAdded(Curve3D *curve)
{
    _curveMeshes.push_back(new OpenGLMesh( curve->getMarker(), this));
}

void OpenGLScene::tfAdded(Transform *tf)
{
    modelAdded(tf->getTfModel());
}

void OpenGLScene::tfGroupAdded(TransformGroup *tfGroup)
{
    auto tfVecs = tfGroup->getTfVecs();

    for(auto val : tfVecs)
    {
        tfAdded(val);
    }
    connect(tfGroup, &TransformGroup::tfAdded, this, &OpenGLScene::tfAdded);
}


void OpenGLScene::lightAdded( AbstractLight * light )
{
    if ( light->getMarker() )
        _lightMeshes.push_back( new OpenGLMesh( light->getMarker(), this ) );
}


void OpenGLScene::modelAdded( Model * model )
{
    //对象销毁时，会直接断开connect
    connect( model, &Model::childMeshAdded, this, &OpenGLScene::meshAdded);
    connect( model, &Model::childModelAdded, this, &OpenGLScene::modelAdded);

    for ( int i = 0; i < model->getChildMeshes().size(); i++ )
        meshAdded( model->getChildMeshes()[i] );
    for ( int i = 0; i < model->getChildModels().size(); i++ )
        modelAdded( model->getChildModels()[i] );
}


void OpenGLScene::meshAdded( Mesh* mesh )
{
    _normalMeshes.push_back( new OpenGLMesh( mesh, this ) );
}

void OpenGLScene::pointsCloudAdded(PointsCloud *poinsClouds)
{
    _pointsCloudMeshes.push_back( new OpenGLMesh(poinsClouds->getMarker(), this));
}

void OpenGLScene::robotAdded(Robot *robot)
{
    modelAdded(robot->getShowModel());
    connect(robot, &Robot::curveAdded, this, &OpenGLScene::curveAdded);
    connect(robot, &Robot::axesGroupAdded, this, &OpenGLScene::tfGroupAdded);
}


void OpenGLScene::hostDestroyed( QObject * )
{
    /* Commit suicide */
    delete this;
}


