﻿#include <OpenGL/MsnhOpenGLSky.h>

Sky::Sky(QObject *parent) : QObject(parent)
{

}

Sky::~Sky()
{
    destroy();
}

void Sky::create()
{
    destroy();
    float vertex[4][3];
    vertex[0][0] = -1.0f; vertex[1][0] = 1.0f; vertex[2][0] = -1.0f; vertex[3][0] = 1.0f;
    vertex[0][1] = -1.0f; vertex[1][1] = -1.0f; vertex[2][1] = 1.0f; vertex[3][1] = 1.0f;
    for (int i = 0; i < 4; i++)
    {
        vertex[i][2] = 0.f;
    }

    _vao = new QOpenGLVertexArrayObject(this);
    _vao->create();
    _vao->bind();
    _vbo = new QOpenGLBuffer( QOpenGLBuffer::VertexBuffer );
    _vbo->create();
    _vbo->bind();
    _vbo->allocate( vertex, int(sizeof(float)*12));

    glFuncs = QOpenGLContext::currentContext()->versionFunctions<MSNH_OPENGL>();
    glFuncs->glEnableVertexAttribArray( 0 );
    glFuncs->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    _vao->release();

    _lightPos = QVector3D(50.f,50.f,50.f);
}

void Sky::destroy()
{
    if ( _vao )
        delete _vao;
    if ( _vbo )
        delete _vbo;

    _vao	= nullptr;
    _vbo	= nullptr;
}

QVector2D Sky::getAspectRatio() const
{
    return _aspectRatio;
}

QVector3D Sky::getLightPos() const
{
    return _lightPos;
}

void Sky::setAspectRatio(const QVector2D &aspectRatio)
{
    _aspectRatio = aspectRatio;
}

void Sky::setLightPos(const QVector3D &lightPos)
{
    _lightPos = lightPos;
}

void Sky::render()
{
    if ( _vao == 0 || _vbo == 0)
        create();

    _vao->bind();
    glFuncs->glDisable(GL_DEPTH_TEST);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glFuncs->glEnable(GL_DEPTH_TEST);
    _vao->release();
}

