﻿#include <OpenGL/MsnhOpenGLTexture.h>

OpenGLTexture::OpenGLTexture( Texture * texture )
{
    _host               = texture;
    _openGLTexture	= nullptr;

    if ( _host->property( "OpenGLTexturePointer" ).isValid() )
    {
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "FATAL: Multiple OpenGLTexture instances are bound to one texture";
        exit( -1 );
    }
    _host->setProperty( "OpenGLTexturePointer", QVariant::fromValue( this ) );

    connect( _host, SIGNAL( imageChanged( QImage ) ), this, SLOT( imageChanged( QImage ) ) );
    connect( _host, SIGNAL( destroyed( QObject* ) ), this, SLOT( hostDestroyed( QObject* ) ) );
}


OpenGLTexture::~OpenGLTexture()
{
    delete _openGLTexture;
    _host->setProperty( "OpenGLTexturePointer", QVariant() );
}


void OpenGLTexture::create()
{
    _openGLTexture = new QOpenGLTexture( _host->getImage().mirrored() );
    _openGLTexture->setMinificationFilter( QOpenGLTexture::Nearest );
    _openGLTexture->setMagnificationFilter( QOpenGLTexture::Linear );
    _openGLTexture->setWrapMode( QOpenGLTexture::Repeat );
}


bool OpenGLTexture::bind()
{
    if ( !_openGLTexture )
        create();
    if ( !_host->getEnabled() )
        return(false);
    QOpenGLFunctions * glFuncs = QOpenGLContext::currentContext()->functions();
    if ( _host->getTextureType() == Texture::DIFFUSE )              /* Diffuse map */
    {
        glFuncs->glActiveTexture( GL_TEXTURE0 + 0 );
        glFuncs->glBindTexture( GL_TEXTURE_2D, _openGLTexture->textureId() );
    } else if ( _host->getTextureType() == Texture::SPECULAR )      /* Specular map */
    {
        glFuncs->glActiveTexture( GL_TEXTURE0 + 1 );
        glFuncs->glBindTexture( GL_TEXTURE_2D, _openGLTexture->textureId() );
    } else if ( _host->getTextureType() == Texture::BUMP )          /* Bump map */
    {
        glFuncs->glActiveTexture( GL_TEXTURE0 + 2 );
        glFuncs->glBindTexture( GL_TEXTURE_2D, _openGLTexture->textureId() );
    }
    return(true);
}


void OpenGLTexture::release()
{
    QOpenGLFunctions * glFuncs = QOpenGLContext::currentContext()->functions();
    if ( _host->getTextureType() == Texture::DIFFUSE )              /* Diffuse map */
    {
        glFuncs->glActiveTexture( GL_TEXTURE0 + 0 );
        glFuncs->glBindTexture( GL_TEXTURE_2D, 0 );
    } else if ( _host->getTextureType() == Texture::SPECULAR )      /* Specular map */
    {
        glFuncs->glActiveTexture( GL_TEXTURE0 + 1 );
        glFuncs->glBindTexture( GL_TEXTURE_2D, 0 );
    } else if ( _host->getTextureType() == Texture::BUMP )          /* Bump map */
    {
        glFuncs->glActiveTexture( GL_TEXTURE0 + 2 );
        glFuncs->glBindTexture( GL_TEXTURE_2D, 0 );
    }
}


void OpenGLTexture::imageChanged( const QImage & image )
{
    if ( _openGLTexture )
    {
        delete _openGLTexture;
        _openGLTexture = new QOpenGLTexture( image );
        _openGLTexture->setMinificationFilter( QOpenGLTexture::Nearest );
        _openGLTexture->setMagnificationFilter( QOpenGLTexture::Linear );
        _openGLTexture->setWrapMode( QOpenGLTexture::Repeat );
    }
}


void OpenGLTexture::hostDestroyed( QObject * )
{
    /* Commit suicide */
    delete this;
}


