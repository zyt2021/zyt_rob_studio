﻿#include <OpenGL/MsnhOpenGLUBO.h>

MSNH_OPENGL* OpenGLUBO::glFuncs = nullptr;

OpenGLUBO::OpenGLUBO()
{
    _id     = 0;
    glFuncs = nullptr;
}


OpenGLUBO::~OpenGLUBO()
{
    if ( _id )
        destroy();
}


GLuint OpenGLUBO::bufferId() const
{
    return(_id);
}


bool OpenGLUBO::create()
{
    if ( _id )
        destroy();
    if ( !glFuncs )
        glFuncs = QOpenGLContext::currentContext()->versionFunctions<MSNH_OPENGL>();
    glFuncs->glGenBuffers( 1, &_id );
    return (_id > 0);
}


void OpenGLUBO::allocate( int indx, const void * data, int count )
{
    if ( _id == 0 )
        return;
    if ( !glFuncs )
        return;
    glFuncs->glBufferData( GL_UNIFORM_BUFFER, count, data, GL_STATIC_DRAW );
    glFuncs->glBindBufferRange( GL_UNIFORM_BUFFER, indx, _id, 0, count );
}


void OpenGLUBO::destroy()
{
    if ( _id && glFuncs )
    {
        glFuncs->glDeleteBuffers( 1, &_id );
        _id = 0;
    }
}


void OpenGLUBO::bind()
{
    if ( _id && glFuncs )
        glFuncs->glBindBuffer( GL_UNIFORM_BUFFER, _id );
}


void OpenGLUBO::write( int offset, const void * data, int count )
{
    if ( _id && glFuncs )
        glFuncs->glBufferSubData( GL_UNIFORM_BUFFER, offset, count, data );
}


void OpenGLUBO::release()
{
    if ( glFuncs )
        glFuncs->glBindBuffer( GL_UNIFORM_BUFFER, 0 );
}


void OpenGLUBO::bindUniformBlock( QOpenGLShaderProgram* shader )
{
    if ( !glFuncs )
        glFuncs = QOpenGLContext::currentContext()->versionFunctions<MSNH_OPENGL>();
    GLuint indx = glFuncs->glGetUniformBlockIndex( shader->programId(), "CameraInfo" );
    glFuncs->glUniformBlockBinding( shader->programId(), indx, CAMERA_INFO_BINDING_POINT );
    indx = glFuncs->glGetUniformBlockIndex( shader->programId(), "ModelInfo" );
    glFuncs->glUniformBlockBinding( shader->programId(), indx, MODEL_INFO_BINDING_POINT );
    indx = glFuncs->glGetUniformBlockIndex( shader->programId(), "MaterialInfo" );
    glFuncs->glUniformBlockBinding( shader->programId(), indx, MATERIAL_INFO_BINDING_POINT );
    indx = glFuncs->glGetUniformBlockIndex( shader->programId(), "LightInfo" );
    glFuncs->glUniformBlockBinding( shader->programId(), indx, LIGHT_INFO_BINDING_POINT );
}

