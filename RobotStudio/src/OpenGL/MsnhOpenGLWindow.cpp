﻿#include <OpenGL/MsnhOpenGLWindow.h>
#include <Core/MsnhModelLoader.h>
#include <Core/MsnhPointsCloudIO.h>
#include <QPainterPath>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

OpenGLWindow::OpenGLWindow()
{
    //
    _lastCursorPos          = QCursor::pos();
    _enableMousePicking     = true;
    _renderer               = new OpenGLRenderer(this);
    _openGLScene            = new OpenGLScene(this);
    _fpsCounter             = new FpsCounter( this );
    _customRenderingLoop    = nullptr;
    configSignals();

    // Tool Box
    _activated              = -1;
    _padding                = 6;
    _subToolActivated       = -1;
    _buttonSize             = QSize(40,40);
    _longPressed            = false;
    _toolBoxHandlePressed   = false;
    _toolArea               = false;
    _timer.setInterval(200);
    _timer.setSingleShot(true);
    connect(&_timer, &QTimer::timeout, this, &OpenGLWindow::longPressEvent);
    // ==
}

OpenGLWindow::~OpenGLWindow()
{

}


OpenGLWindow::OpenGLWindow( OpenGLScene * openGLScene, OpenGLRenderer * renderer )
{
    _lastCursorPos          = QCursor::pos();
    _enableMousePicking     = true;
    _renderer               = renderer;
    _openGLScene            = openGLScene;
    _fpsCounter             = new FpsCounter( this );
    _customRenderingLoop    = nullptr;
    configSignals();
}

QString OpenGLWindow::getVendor()
{
    return(isInitialized() ? QString( (char *) glGetString( GL_VENDOR ) ) : "");
}


QString OpenGLWindow::getRendererName()
{
    return(isInitialized() ? QString( (char *) glGetString( GL_RENDERER ) ) : "");
}


QString OpenGLWindow::getOpenGLVersion()
{
    return(isInitialized() ? QString( (char *) glGetString( GL_VERSION ) ) : "");
}


QString OpenGLWindow::getShadingLanguageVersion()
{
    return(isInitialized() ? QString( (char *) glGetString( GL_SHADING_LANGUAGE_VERSION ) ) : "");
}

void OpenGLWindow::setAxisMode(AxisMode axisMode)
{
    if(axisMode == AXIS_Y_UP)
    {
        EngineUtil::setYAxisUp();                                                                              // Y - Z
        _openGLScene->getHost()->getGizmo()->getMarkers()[0]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // X - Y
        _openGLScene->getHost()->getGizmo()->getMarkers()[1]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // Y - Z
        _openGLScene->getHost()->getGizmo()->getMarkers()[2]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // Z - X

        _openGLScene->getHost()->getGizmo()->getMarkers()[3]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // XY - YZ
        _openGLScene->getHost()->getGizmo()->getMarkers()[4]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // YZ - ZX
        _openGLScene->getHost()->getGizmo()->getMarkers()[5]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // ZX - XY

        _openGLScene->getHost()->getGizmo()->getMarkers()[6]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // X - Y
        _openGLScene->getHost()->getGizmo()->getMarkers()[7]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // Y - Z
        _openGLScene->getHost()->getGizmo()->getMarkers()[8]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // Z - X

        _openGLScene->getHost()->getGizmo()->getMarkers()[9 ]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // XY - YZ
        _openGLScene->getHost()->getGizmo()->getMarkers()[10]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // YZ - ZX
        _openGLScene->getHost()->getGizmo()->getMarkers()[11]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // ZX - XY

        _openGLScene->getHost()->getGizmo()->getMarkers()[13]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // X - Y
        _openGLScene->getHost()->getGizmo()->getMarkers()[14]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // Y - Z
        _openGLScene->getHost()->getGizmo()->getMarkers()[15]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // Z - X
    }
    else
    {
        EngineUtil::setZAxisUp();
        _openGLScene->getHost()->getGizmo()->getMarkers()[0]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // X - Y
        _openGLScene->getHost()->getGizmo()->getMarkers()[1]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // Y - Z
        _openGLScene->getHost()->getGizmo()->getMarkers()[2]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // Z - X

        _openGLScene->getHost()->getGizmo()->getMarkers()[3]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // XY - YZ
        _openGLScene->getHost()->getGizmo()->getMarkers()[4]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // YZ - ZX
        _openGLScene->getHost()->getGizmo()->getMarkers()[5]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // ZX - XY

        _openGLScene->getHost()->getGizmo()->getMarkers()[6]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // X - Y
        _openGLScene->getHost()->getGizmo()->getMarkers()[7]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // Y - Z
        _openGLScene->getHost()->getGizmo()->getMarkers()[8]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // Z - X

        _openGLScene->getHost()->getGizmo()->getMarkers()[9 ]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // XY - YZ
        _openGLScene->getHost()->getGizmo()->getMarkers()[10]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // YZ - ZX
        _openGLScene->getHost()->getGizmo()->getMarkers()[11]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // ZX - XY

        _openGLScene->getHost()->getGizmo()->getMarkers()[13]->getMaterial()->setColor(QVector3D(0,0.66f,0));  // X - Y
        _openGLScene->getHost()->getGizmo()->getMarkers()[14]->getMaterial()->setColor(QVector3D(0,0,0.66f));  // Y - Z
        _openGLScene->getHost()->getGizmo()->getMarkers()[15]->getMaterial()->setColor(QVector3D(0.66f,0,0));  // Z - X
    }

}


void OpenGLWindow::setScene( OpenGLScene* openGLScene )
{
    if ( _openGLScene )
        disconnect( _openGLScene, nullptr, this, nullptr );
    _openGLScene = openGLScene;
    if ( _openGLScene )
        connect( _openGLScene, SIGNAL( destroyed( QObject* ) ), this, SLOT( sceneDestroyed( QObject* ) ) );
}


void OpenGLWindow::setRenderer( OpenGLRenderer * renderer )
{
    _renderer = renderer;
    if ( isInitialized() && _renderer )
    {
        _renderer->reloadShaders();
        if ( _renderer->hasErrorLog() )
        {
            QString log = _renderer->errorLog();
            QMessageBox::critical( nullptr,"Failed to load shaders", log );
            if ( logLevel >= LOG_LEVEL_ERROR )
            {
                dout << log;
            }
        }
    }
}


void OpenGLWindow::setEnableMousePicking( bool enabled )
{
    _enableMousePicking = enabled;
}


void OpenGLWindow::setCustomRenderingLoop( void (*customRenderingLoop)( Scene* ) )
{
    _customRenderingLoop = customRenderingLoop;
}

Model *OpenGLWindow::loadURDF(const QString &path)
{
    _axis.clear();
    std::shared_ptr<Msnhnet::URDFModel> model =  Msnhnet::URDF::parseURDF(path.toLocal8Bit().toStdString(),true);
    std::shared_ptr<Msnhnet::URDFLinkTree> linkTree = Msnhnet::URDF::getLinkTree(model->rootLink);

    Model* md = loadModel(linkTree,model->rootPath);
    md->setObjectName(QString::fromStdString(model->getName()));

    //    std::vector<std::string> names;
    //    StringTree::getShortestPath(tree,"world","JointEffector",names);


    //    for (int i = 0; i < names.size(); ++i)
    //    {
    //        std::cout<<names[i]<<std::endl;
    //    }
    //set scaling api


    foreach (auto ax, _axis)
    {
        if(ax->getJointType()!=Msnhnet::JOINT_FIXED)
            ax->singleMove(-90);
        //        ax->getChildMeshes()[0]->setScalingApi({0.06f,0.06f,0.06f});
        //        ax->getChildMeshes()[1]->setScalingApi({0.06f,0.06f,0.06f});
        //        ax->getChildMeshes()[2]->setScalingApi({0.06f,0.06f,0.06f});
    }

    return md;
}

Model *OpenGLWindow::loadModel(const std::shared_ptr<Msnhnet::URDFLinkTree> &link, const std::string &rootPath)
{
    Model* md       = new Model();
    Model* origin   = new Model();
    Model* joint    = new Model();

    origin->setObjectName("origin");
    joint->setObjectName(QString::fromStdString(link->joint.value()->name));

    Msnhnet::Frame frame = link->joint.value()->parentToJointTransform;

    joint->setEnd2TipFrame(frame);

    Msnhnet::Vector3DS axis = link->joint.value()->axis;

    Msnhnet::URDFJointType jointType = link->joint.value()->type;

    if(jointType == Msnhnet::URDFJointType::URDF_FIXED || jointType == Msnhnet::URDFJointType::URDF_UNKNOWN)
    {
        joint->setJointType(Msnhnet::JointType::JOINT_FIXED);
    }
    else if(jointType == Msnhnet::URDFJointType::URDF_CONTINUOUS)
    {
        if(axis == Msnhnet::Vector3DS(0,0,0))
        {
            throw Msnhnet::Exception(1,"Continuous axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        else
        {
            joint->setJointType(Msnhnet::JOINT_ROT_AXIS);
        }
    }
    else if(jointType == Msnhnet::URDFJointType::URDF_REVOLUTE)
    {
        if(axis == Msnhnet::Vector3DS(0,0,0))
        {
            throw Msnhnet::Exception(1,"Revolute axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        else
        {
            joint->setJointType(Msnhnet::JOINT_ROT_AXIS);
            joint->setAxis(axis);
        }

        double min = link->joint.value()->limits.value()->lower;
        double max = link->joint.value()->limits.value()->upper;
        joint->setJointMinMax(min,max);
    }
    else if(jointType == Msnhnet::URDFJointType::URDF_PRISMATIC)
    {
        if(axis == Msnhnet::Vector3DS(0,0,0))
        {
            throw Msnhnet::Exception(1,"Prismatic axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        else
        {
            joint->setJointType(Msnhnet::JOINT_TRANS_AXIS);
            joint->setAxis(axis);
        }
    }
    else
    {
        throw Msnhnet::Exception(1,"Type is not supported!",__FILE__, __LINE__, __FUNCTION__);
    }

    ModelLoader loader;
    joint->addChildMesh(loader.loadMeshFromFile( ":/shapes/TransX.obj" ));
    joint->addChildMesh(loader.loadMeshFromFile( ":/shapes/TransY.obj" ));
    joint->addChildMesh(loader.loadMeshFromFile( ":/shapes/TransZ.obj" ));

    joint->getChildMeshes()[0]->scaleApi({0.02f,0.02f,0.02f});
    joint->getChildMeshes()[1]->scaleApi({0.02f,0.02f,0.02f});
    joint->getChildMeshes()[2]->scaleApi({0.02f,0.02f,0.02f});

    joint->getChildMeshes()[0]->setObjectName(QString::fromStdString(link->joint.value()->name)+"_X");
    joint->getChildMeshes()[1]->setObjectName(QString::fromStdString(link->joint.value()->name)+"_Y");
    joint->getChildMeshes()[2]->setObjectName(QString::fromStdString(link->joint.value()->name)+"_Z");

    joint->getChildMeshes()[2]->getMaterial()->setColor( QVector3D( 0.66f, 0, 0 ) );
    joint->getChildMeshes()[0]->getMaterial()->setColor( QVector3D( 0, 0.66f, 0 ) );
    joint->getChildMeshes()[1]->getMaterial()->setColor( QVector3D( 0, 0, 0.66f ) );

    Msnhnet::EulerDS euler = Msnhnet::GeometryS::rotMat2Euler(frame.rotMat,Msnhnet::ROT_ZYX);
    origin->setPositionApi({(float)frame.trans[0],(float)frame.trans[1],(float)frame.trans[2]});
    origin->setRotationApi({(float)(euler[0]*MSNH_RAD_2_DEG),(float)(euler[1]*MSNH_RAD_2_DEG),(float)(euler[2]*MSNH_RAD_2_DEG)});

    origin->addChildModel(joint);
    md->addChildModel(origin);

    _axis[QString::fromStdString(link->joint.value()->name)] = joint;

    for (size_t i = 0; i < link->visuals.size(); ++i)
    {
        std::string path = ((Msnhnet::URDFMesh*)link->visuals[i]->geometry->get())->filename;

        Msnhnet::ExString::replace(path,"package:/","");

        path = rootPath + path;

        ModelLoader loader;
        Model* mode = loader.loadModelFromFile(QString::fromStdString(path));
        md->getChildModels()[0]->getChildModels()[0]->addChildModel(mode);
    }

    if(link->nextLink.size()>0)
    {
        for (size_t i = 0; i < link->nextLink.size(); ++i)
        {
            md->getChildModels()[0]->getChildModels()[0]->addChildModel(loadModel(link->nextLink[i],rootPath));
        }
    }
    return md;
}

void OpenGLWindow::resetOpenglScene()
{
    delete _openGLScene;
    _openGLScene = nullptr;

    setScene(new OpenGLScene(this));
}

void OpenGLWindow::resetOpenglScene(Scene *scene)
{
    delete _openGLScene;
    _openGLScene = nullptr;

    setScene(new OpenGLScene(scene, this));
}

void OpenGLWindow::initializeGL()
{
    initializeOpenGLFunctions();

    //    logger = new QOpenGLDebugLogger(this);
    //    logger->initialize();
    //    connect(logger,&QOpenGLDebugLogger::messageLogged,[](const QOpenGLDebugMessage& msg)->void
    //    {
    //        qDebug()<<msg;
    //    });
    //    logger->startLogging();

    if ( _renderer )
    {
        _renderer->reloadShaders();
        if ( _renderer->hasErrorLog() )
        {
            QString log = _renderer->errorLog();
            QMessageBox::critical( nullptr, "Failed to load shaders", log );
            if ( logLevel >= LOG_LEVEL_ERROR )
                dout << log;
        }
    }
    else
    {
        QMessageBox::critical( nullptr, "Failed to initialize OpenGL", "No renderer specified." );
        if ( logLevel >= LOG_LEVEL_ERROR )
            dout << "No renderer specified";
    }
}

void OpenGLWindow::paintGL()
{
    glClearColor( 0.7f, 0.7f, 0.7f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_MULTISAMPLE);// MSAA4X抗锯齿

    glEnable( GL_DEPTH_TEST );
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA); //Alpha需要排序

    processUserInput();

    if ( _renderer && _openGLScene && _openGLScene->getHost()->getCamera() )
    {
        //其它场景同时再此处渲染，再一个管线里
        if ( _customRenderingLoop )
            _customRenderingLoop( _openGLScene->getHost() );

        _openGLScene->getHost()->getCamera()->setAspectRatio( float(width() ) / height() );

        _openGLScene->commitCameraInfo();
        _openGLScene->commitLightInfo();

        //先渲染一层用于pick的模型
        if ( !_keyPressed[Qt::LeftButton] && _enableMousePicking )
        {
            //通过颜色ID来获取物体ID
            // uint r = (pickingID & uint(0x000000FF)) >>  0;
            // uint g = (pickingID & uint(0x0000FF00)) >>  8;
            // uint b = (pickingID & uint(0x00FF0000)) >> 16;
            // fragColor = vec4(r / 255.0f, g / 255.0f, b / 255.0f, 1.0f);

            uint32_t	pickingID		= _renderer->pickingPass( _openGLScene, mapFromGlobal( QCursor::pos() ) * devicePixelRatioF() );

            OpenGLMesh	* pickedOpenGLMesh	= _openGLScene->pick( pickingID );
            if ( pickedOpenGLMesh )
            {
                pickedOpenGLMesh->getHost()->setHighlighted( true );
            }
            else if ( Mesh::getHighlightedObject() )
            {
                Mesh::getHighlightedObject()->setHighlighted( false );
            }
        }

        //再渲染主场景
        _renderer->setScreenSize(QVector2D(width(),height()));
        _renderer->render( _openGLScene );
    }


    QPainter painter(this);
    QPen pen(QColor(0,0,0,100));
    painter.setPen(pen);
    QFont font = QFont(allFontFamily, 40);
    font.setBold(true);
    painter.setFont(font);
    painter.drawText(0, height()-80, width(), height(), Qt::AlignLeft, "AG-ROBOT(MSNH2012)");
    painter.end();

    if(_world)
    {
        _world->debugDrawWorld();
    }

    // ToolBox
    {
        QPainter gc(this);
        gc.setRenderHint(QPainter::Antialiasing);
        QPoint tempTopLeft(10,10);
        QColor highlightedBack(255,0,0,200);
        QColor back(0,0,0,200);

        QPoint topLeft = tempTopLeft;
        _toolRects.clear();

        for(int i=0; i<_breaks.count(); i++)
        {
            int count = _breaks[i] + 1;
            if(i>0)
            {
                count = count - _breaks[i-1] - 1;
            }
            int h = _buttonSize.height() * (count);
            QRect backRect(topLeft, QSize(_buttonSize.width(), h));
            QPainterPath p;
            p.addRoundedRect(backRect, 3, 3);
            gc.fillPath(p, back);
            topLeft += QPoint(0, 2+h);
        }

        int breakCount = 0;
        topLeft = tempTopLeft;

        for(int i=0;i<_tools.count();i++)
        {
            QRect toolRect(topLeft, _buttonSize);
            QPainterPath path;
            path.addRoundedRect(toolRect, 3, 3);

            if(i == _activated)
            {
                gc.fillPath(path, highlightedBack);
            }
            else
            {
                gc.fillPath(path, back);
            }

            if(!_tools[i].getSubTools().isEmpty())
            {
                QPainterPath p;
                p.moveTo(toolRect.bottomRight());
                p.lineTo(toolRect.bottomRight() + QPoint(0, -5));
                p.lineTo(toolRect.bottomRight() + QPoint(-5, 0));
                gc.fillPath(p, Qt::black);
            }

            QRect iconRect = toolRect.adjusted(_padding, _padding, -_padding, -_padding);
            _tools[i].getIcon().paint(&gc, iconRect);
            _toolRects.push_back(toolRect);
            topLeft += QPoint(0, _buttonSize.height());

            if(breakCount < _breaks.count() && _breaks[breakCount] == i)
            {
                topLeft += QPoint(0, 2);
                breakCount++;
            }
        }

        QPoint subTopLeft(0,0);
        if(_longPressed)
        {
            QVector<MsnhTool> tools = _tools[_activated].getSubTools();
            _secondaryTools.clear();

            if(!tools.isEmpty())
            {
                subTopLeft = QPoint(_toolRects[_activated].topRight());
                for(int i=0; i<tools.count(); i++)
                {
                    QRect toolRect(subTopLeft, _buttonSize + QSize(130, 0));
                    if(_subToolActivated == i)
                    {
                        gc.fillRect(toolRect, highlightedBack);
                    }
                    else
                    {
                        gc.fillRect(toolRect, back);
                    }

                    QRect iconRect = toolRect.adjusted(_padding, _padding, -_padding-130, -_padding);
                    tools[i].getIcon().paint(&gc, iconRect);
                    gc.setPen(Qt::white);
                    gc.drawText(iconRect.bottomRight() + QPoint(10, -5), tools[i].getName());
                    subTopLeft += QPoint(0, _buttonSize.height());
                    _secondaryTools.push_back(toolRect);
                }
            }
        }

        int w = _buttonSize.width();
        int h = topLeft.y() + _buttonSize.height() + 2;

        if(_longPressed)
        {
            w = w*2 + 130;
            if(subTopLeft.y() > topLeft.y())
            {
                h = subTopLeft.y() + _buttonSize.height() + 2;
            }
        }
    }

}

// drag
bool OpenGLWindow::event( QEvent * event )
{
    if ( QOpenGLWindow::event( event ) )
        return (true);
    if ( !_openGLScene )
        return (false);

    if ( event->type() == QEvent::DragEnter )
    {
        QDragEnterEvent* dragEnterEvent = static_cast<QDragEnterEvent*>(event);
        if ( dragEnterEvent->mimeData()->hasUrls() )
        {
            dragEnterEvent->acceptProposedAction();
        }
        event->accept();
        return (true);
    }
    else if ( event->type() == QEvent::DragMove )
    {
        QDragMoveEvent* dragMoveEvent = static_cast<QDragMoveEvent*>(event);
        if ( dragMoveEvent->mimeData()->hasUrls() )
        {
            dragMoveEvent->acceptProposedAction();
        }
        event->accept();
        return (true);
    }
    else if ( event->type() == QEvent::Drop )
    {
        QDropEvent* dropEvent = static_cast<QDropEvent*>(event);
        foreach( const QUrl &url, dropEvent->mimeData()->urls() )
        {
            QString path = url.toLocalFile().toLocal8Bit();
            QStringList splits = path.split(".");
            QString end = QString(splits[splits.length()-1]);
            if(end == "msnhrob")
            {
                emit openScene(path);
            }
            else if(end == "ply" || end == "xyz" || end == "xyzrgb")
            {
                PointsCloudIO io;

                PointsCloud *pointsCloud = io.readFromFile(path);

                if(io.hasErr())
                {
                    QMessageBox::critical(nullptr,"错误",io.getErr());
                }
                //TODO: Exception 2021/07/11

                if( pointsCloud )
                {
                    _openGLScene->getHost()->addPointsCloud(pointsCloud);
                }
            }
            else if(end == "urdf")
            {
                try
                {
                    Robot* robot = new Robot(path);
                    _openGLScene->getHost()->addRobot( robot );
                }
                catch(Msnhnet::Exception ex)
                {
                    QMessageBox::warning(nullptr,"错误",QString::fromStdString(ex.what() + ex.getErrFile()) + QString::number(ex.getErrLine()));
                }
            }
            else
            {
                ModelLoader	loader;
                Model		* model = loader.loadModelFromFile( path);

                if ( loader.hasErrorLog() )
                {
                    QString log = loader.errorLog();
                    QMessageBox::critical(nullptr,"错误",log);
                    if ( logLevel >= LOG_LEVEL_ERROR )
                        dout << log;
                }

                if ( model )
                {
                    _openGLScene->getHost()->addModel( model );
                }
            }

        }
        event->accept();
        return (true);
    }
    return (false);
}


void OpenGLWindow::keyPressEvent( QKeyEvent * event )
{
    _keyPressed[event->key()] = true;
    event->accept();
}


void OpenGLWindow::keyReleaseEvent( QKeyEvent * event )
{
    _keyPressed[event->key()] = false;
    event->accept();
}


void OpenGLWindow::mousePressEvent( QMouseEvent * event )
{
    if ( !_openGLScene )
        return;

    _lastCursorPos                  = mapFromGlobal( QCursor::pos() );
    _lastMousePressTime             = QTime::currentTime();
    _keyPressed[event->button()]	= true;

    event->accept();

    if ( Mesh::getHighlightedObject() && Mesh::getHighlightedObject()->isGizmo() )
        _openGLScene->getHost()->getGizmo()->setTransformAxis( Mesh::getHighlightedObject() );

    // ToolBox
    {
        QPoint pos = event->pos();
        _lastMousePos = pos;

        if(event->button() != Qt::LeftButton)
            return;

        _toolArea = false;

        for(int i=0; i<_tools.count(); i++)
        {
            if(_toolRects[i].contains(pos))
            {
                if(_activated != i)
                {
                    _activated = i;
                    emit toolChanged(i);
                }
                _toolArea = true;

                _timer.start();
                update();
                return;
            }
        }
    }
}

void OpenGLWindow::mouseMoveEvent(QMouseEvent *event)
{
    // ToolBox
    {
        QPoint pos = event->pos();
        if(_longPressed)
        {
            for(int i=0; i<_secondaryTools.count(); i++)
            {
                if(_secondaryTools[i].contains(pos))
                {
                    _subToolActivated = i;
                    update();
                    return;
                }
            }
            _subToolActivated = -1;
        }
    }
}


void OpenGLWindow::mouseReleaseEvent( QMouseEvent * event )
{
    if ( !_openGLScene )
        return;

    _keyPressed[event->button()] = false;
    _openGLScene->getHost()->getGizmo()->setTransformAxis( TransformGizmo::GIZMO_AXIS_NONE );
    event->accept();

    if(!_toolArea)
    {
        if ( _lastMousePressTime.msecsTo( QTime::currentTime() ) < 200 ) /* click */
        {
            if ( Mesh::getHighlightedObject() )
            {
                if ( !Mesh::getHighlightedObject()->isGizmo() && Mesh::getHighlightedObject()->getSelectable())
                {
                    Mesh::getHighlightedObject()->setSelected( true );
                    _openGLScene->getHost()->getGizmo()->bindTo( Mesh::getSelectedObject() ); //SceneTreeWidget::currentItemChanged
                }
            }
            else
            {
                if ( Mesh::getSelectedObject() )
                {
                    Mesh::getSelectedObject()->setSelected( false );
                    _openGLScene->getHost()->getGizmo()->unbind();
                }

                //如果点击空地，则取消任何选中高亮的碰撞体
                if(Mesh::getCollisionSelectedObject())
                {
                    Mesh::getCollisionSelectedObject()->setWireFrameMode(true);//默认碰撞体为wireFrame模式,恢复默认
                    Mesh::getCollisionSelectedObject()->setCollisionSelected(false);//去除碰撞选择高亮属性。
                }
            }
        }
    }


    // ToolBox
    {
        _longPressed = false;

        if(_subToolActivated >= 0)
        {
            _tools[_activated].swap(_subToolActivated);
        }
        _timer.stop();
        _subToolActivated = -1;
        _toolBoxHandlePressed = false;
        update();
        setCursor(Qt::ArrowCursor);
    }
}


void OpenGLWindow::wheelEvent( QWheelEvent * event )
{
    float scale = 50;
    if(_openGLScene->getHost()->getCamera()->getOrbitRadius() < 5)
    {
        scale = 500;
    }
    if ( !_openGLScene )
        return;

    if ( !event->pixelDelta().isNull() )
    {
        if(_openGLScene->getHost()->getCamera()->isOrbitCam())
        {
            _openGLScene->getHost()->getCamera()->changeRadius(-event->pixelDelta().y()/scale);
        }
        else
        {
            _openGLScene->getHost()->getCamera()->moveForward( event->pixelDelta().y() );
        }
    }
    else if ( !event->angleDelta().isNull() )
    {
        if(_openGLScene->getHost()->getCamera()->isOrbitCam())
        {
            _openGLScene->getHost()->getCamera()->changeRadius(-event->angleDelta().y()/scale);
        }
        else
        {
            _openGLScene->getHost()->getCamera()->moveForward( event->angleDelta().y() );
        }
    }

    event->accept();
}


void OpenGLWindow::focusOutEvent( QFocusEvent * )
{
    for ( int i = 0; i < _keyPressed.keys().size(); i++ )
        _keyPressed[_keyPressed.keys()[i]] = false;
}


OpenGLRenderer *OpenGLWindow::getRenderer() const
{
    return _renderer;
}

void OpenGLWindow::setDynamicWorld(btDynamicsWorld *world)
{
    _world = world;
}

void OpenGLWindow::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color)
{
    glBegin(GL_LINES);
    glColor3f(color.getX(), color.getY(), color.getZ());
    glVertex3f(from.getX(), from.getY(), from.getZ());
    glVertex3f(to.getX(), to.getY(), to.getZ());
    glEnd();
    glFlush();
}

void OpenGLWindow::drawContactPoint(const btVector3 &pointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color)
{
    btVector3 const startPoint = pointOnB;
    btVector3 const endPoint = pointOnB + normalOnB * distance;
    drawLine( startPoint, endPoint, color );
}

void OpenGLWindow::ToggleDebugFlag(int flag)
{

}

void OpenGLWindow::addTool(MsnhTool t)
{
    _tools.push_back(t);
}

void OpenGLWindow::addBreak()
{
    _breaks.push_back(_tools.count()-1);
}

OpenGLScene *OpenGLWindow::getOpenGLScene() const
{
    return _openGLScene;
}

void OpenGLWindow::processUserInput()
{
    if ( !_openGLScene || !_openGLScene->getHost()->getCamera() )
        return;

    float shift = 1.0f;
    if ( _keyPressed[Qt::Key_Shift] )
        shift *= 5.0f;
    if ( _keyPressed[Qt::Key_PageUp] )
        _openGLScene->getHost()->getCamera()->moveForward( shift );
    if ( _keyPressed[Qt::Key_PageDown] )
        _openGLScene->getHost()->getCamera()->moveForward( -shift );
    if ( _keyPressed[Qt::Key_Left] )
        _openGLScene->getHost()->getCamera()->moveRight( -shift );
    if ( _keyPressed[Qt::Key_Right] )
        _openGLScene->getHost()->getCamera()->moveRight( shift );
    if ( _keyPressed[Qt::Key_Up] )
        _openGLScene->getHost()->getCamera()->moveUp( shift );
    if ( _keyPressed[Qt::Key_Down] )
        _openGLScene->getHost()->getCamera()->moveUp( -shift );

    if ( _keyPressed[Qt::Key_Home] )
        _openGLScene->getHost()->getCamera()->reset();

    if(_keyPressed[Qt::LeftButton] || _keyPressed[Qt::MidButton]) //鼠标按下时orbit center显示
    {
        TransformGizmo	* gizmo		= _openGLScene->getHost()->getGizmo();
        if (gizmo->getAxis() == TransformGizmo::GIZMO_AXIS_NONE )
        {
            _openGLScene->getHost()->getCamera()->setMeshVisible(true);
        }
    }
    else    //释放时不显示
    {
        _openGLScene->getHost()->getCamera()->setMeshVisible(false);
    }

    if( _keyPressed[Qt::MidButton])
    {
        QPoint		cntCursorPos	= mapFromGlobal( QCursor::pos() );

        if(_openGLScene->getHost()->getCamera()->isOrbitCam())
        {
            // Z up方式计算, 和opengl一致,实际可能使用Y up方式比较多
            float factor =  0.001f * _openGLScene->getHost()->getCamera()->getOrbitRadius();
            _openGLScene->getHost()->getCamera()->shiftPosY((-_lastCursorPos.y() + cntCursorPos.y() ) * factor);
            float pos   = (-_lastCursorPos.x() + cntCursorPos.x() ) * factor;
            float yaw   = _openGLScene->getHost()->getCamera()->getOrbitYaw()*MSNH_DEG_2_RAD;
            float x   = pos*sin(yaw);
            float z   = pos*cos(yaw);
            _openGLScene->getHost()->getCamera()->shiftPosX(-x);
            _openGLScene->getHost()->getCamera()->shiftPosZ(z);
        }
        else
        {
            _openGLScene->getHost()->getCamera()->moveRight( (_lastCursorPos.x() - cntCursorPos.x() ) / 2.0f );
            _openGLScene->getHost()->getCamera()->moveUp( -(_lastCursorPos.y() - cntCursorPos.y() ) / 2.0f );
        }

        _lastCursorPos = cntCursorPos;
    }


    if ( _keyPressed[Qt::LeftButton] )
    {
        QPoint		cntCursorPos	= mapFromGlobal( QCursor::pos() );

        TransformGizmo	* gizmo		= _openGLScene->getHost()->getGizmo();
        if ( gizmo->getVisible() && gizmo->getAxis() != TransformGizmo::GIZMO_AXIS_NONE )
        {
            gizmo->drag( _lastCursorPos, cntCursorPos, width(), height(),
                         _openGLScene->getHost()->getCamera()->getProjectionMatrix(),
                         _openGLScene->getHost()->getCamera()->getViewMatrix() );
        }
        else
        {

            if(_openGLScene->getHost()->getCamera()->isOrbitCam())
            {
                _openGLScene->getHost()->getCamera()->turnYaw((_lastCursorPos.x() - cntCursorPos.x())/-10.0f);
                _openGLScene->getHost()->getCamera()->turnPitch((_lastCursorPos.y() - cntCursorPos.y())/10.0f);
            }
            else
            {
                _openGLScene->getHost()->getCamera()->turnLeft( (_lastCursorPos.x() - cntCursorPos.x() ) / 10.0f );
                _openGLScene->getHost()->getCamera()->lookUp( (_lastCursorPos.y() - cntCursorPos.y() ) / 10.0f );
            }
        }
        _lastCursorPos = cntCursorPos;
    }
}


void OpenGLWindow::configSignals()
{
    connect( _fpsCounter, SIGNAL( fpsChanged( int ) ), this, SIGNAL( fpsChanged( int ) ) );
    connect( this, SIGNAL( frameSwapped() ), _fpsCounter, SLOT(increase()) );
    connect( this, SIGNAL( frameSwapped() ), this, SLOT( update() ) );
    if ( _openGLScene )
        connect( _openGLScene, SIGNAL( destroyed( QObject* ) ), this, SLOT( sceneDestroyed( QObject* ) ) );
}

void OpenGLWindow::setToolActivated(int activated)
{
    _activated = activated;
}


void OpenGLWindow::sceneDestroyed( QObject * )
{
    _openGLScene = nullptr;
}

void OpenGLWindow::longPressEvent()
{
    _longPressed = true && !_toolBoxHandlePressed;
    update();
}
