﻿#include <Physics/Collision/MsnhCollisionEnv.h>

const double MAX_DISTANCE_MARGIN = 99;

CollisionEnv::CollisionEnv(const QMap<QString, QVector<GeomModel> > &geommeshes)
{
    //TODO: CCD
    if(!geommeshes.isEmpty())
    {
        auto geommesh = geommeshes.begin();
        while(geommesh != geommeshes.end())
        {
            std::vector<std::shared_ptr<Msnhnet::URDFGeometry> > shapes;
            std::vector<Msnhnet::Frame> shapePoses;
            std::vector<CollisionObjectType> collisionObjTypes;

            if(!geommesh.value().empty())
            {
                for(auto geom : geommesh.value())
                {
                    shapePoses.push_back(geom.model->getGlobalFrameApi());

                    if(geom.geom->type == Msnhnet::URDF_MESH)
                    {
                        collisionObjTypes.push_back(CollisionObjectType::COLLISION_CONVEX_HULL);
                    }
                    else
                    {
                        collisionObjTypes.push_back(CollisionObjectType::COLLISION_USE_SHAPE);
                    }

                    shapes.push_back(geom.geom);
                }

                std::string name = geommesh.key().toLocal8Bit().toStdString();
                if(_manager->hasCollisionObject(name))
                {
                    _manager->removeCollisionObject(name);
                    //_managerCCD->removeCollisionObject(name);
                }

                CollisionObjectPtr collisionObj(new CollisionObject(name, BodyType::BODY_ROBOT_LINK, shapes, shapePoses,collisionObjTypes, true));

                _manager->addCollisionObject(collisionObj);
                //_managerCCD->addCollisionObject(collisionObj->clone());
                _activeCollisionNames.push_back(name);
            }
            geommesh++;
        }
    }
}

void CollisionEnv::checkSelfCollision(const CollisionRequest &req, CollisionResult &res, const QMap<QString, Model *> &joints, const AllowedCollisionMatrix &acm)
{
    checkSelfCollisionHelper(req,res,joints, &acm);
}

void CollisionEnv::checkSelfCollision(const CollisionRequest &req, CollisionResult &res, const QMap<QString, Model *> &joints1, const QMap<QString, Model *> &joints2, const AllowedCollisionMatrix &acm)
{
    checkSelfCollisionHelperCCD(req,res,joints1,joints2,&acm);
}

void CollisionEnv::checkSelfCollisionHelper(const CollisionRequest &req, CollisionResult &res, const QMap<QString, Model *> &joints, const AllowedCollisionMatrix *acm)
{
    //TODO:动态加载attached物体

    if(req.computeDis)
    {
        _manager->setContactDistanceThreshold(MAX_DISTANCE_MARGIN);
    }

    for(const auto& name : _activeCollisionNames)
    {
        _manager->setCollisionObjectsTransform(name, joints[QString::fromStdString(name)]->getGlobalFrameApi());
    }

    _manager->contactTest(res,req, acm, true);
}

void CollisionEnv::checkSelfCollisionHelperCCD(const CollisionRequest &req, CollisionResult &res, const QMap<QString, Model *> &joints1, const QMap<QString, Model *> &joints2, const AllowedCollisionMatrix *acm)
{
    for(const auto& name : _activeCollisionNames)
    {
        _managerCCD->setCastCollisionObjectsTransform(name, joints1[QString::fromStdString(name)]->getGlobalFrameApi(),joints2[QString::fromStdString(name)]->getGlobalFrameApi());
    }
    _manager->contactTest(res,req, acm, true);
}
