﻿#include <Physics/Collision/MsnhCollisionManager.h>

BulletBVHManager::BulletBVHManager()
{
    _dispatcher.reset(new btCollisionDispatcher(&_collisionConfig));
    //允许注册自定义/备选碰撞函数
    _dispatcher->registerCollisionCreateFunc(BOX_SHAPE_PROXYTYPE, BOX_SHAPE_PROXYTYPE,
                                             _collisionConfig.getCollisionAlgorithmCreateFunc(CONVEX_SHAPE_PROXYTYPE,
                                                                                              CONVEX_SHAPE_PROXYTYPE));

    _dispatcher->setDispatcherFlags(_dispatcher->getDispatcherFlags() & ~btCollisionDispatcher::CD_USE_RELATIVE_CONTACT_BREAKING_THRESHOLD);

    _broadphase.reset(new btDbvtBroadphase());
    _broadphase->getOverlappingPairCache()->setOverlapFilterCallback(&_filterCallback);

    _contactDistance = BULLET_DEFAULT_CONTACT_DISTANCE;
}

BulletBVHManager::~BulletBVHManager()
{
    for (const std::pair<const std::string, std::shared_ptr<CollisionObject>>& collisionObj : _link2CollisionObj)
        removeCollisionObjectFromBroadphase(collisionObj.second, _broadphase, _dispatcher);
}

bool BulletBVHManager::hasCollisionObject(const std::string &name) const
{
    return (_link2CollisionObj.find(name) != _link2CollisionObj.end());
}

bool BulletBVHManager::removeCollisionObject(const std::string &name)
{
    auto it = _link2CollisionObj.find(name);
    if (it != _link2CollisionObj.end())
    {
        const CollisionObjectPtr& collisionObj = it->second;
        removeCollisionObjectFromBroadphase(collisionObj, _broadphase, _dispatcher);
        _link2CollisionObj.erase(name);
        return true;
    }

    return false;
}

bool BulletBVHManager::enableCollisionObject(const std::string &name)
{
    auto it = _link2CollisionObj.find(name);
    if (it != _link2CollisionObj.end())
    {
        it->second->enabled = true;
        return true;
    }

    return false;
}

bool BulletBVHManager::disableCollisionObject(const std::string &name)
{
    auto it = _link2CollisionObj.find(name);
    if (it != _link2CollisionObj.end())
    {
        it->second->enabled = false;
        return true;
    }

    return false;
}

void BulletBVHManager::setCollisionObjectsTransform(const std::string &name, const Msnhnet::Frame &pose)
{
    auto it = _link2CollisionObj.find(name);
    if (it != _link2CollisionObj.end())
    {
        CollisionObjectPtr& collisionObj = it->second;
        btTransform tf = cvtMsnhFrame2Bt(pose);
        collisionObj->setWorldTransform(tf);

        // Now update Broadphase AABB (See BulletWorld updateSingleAabb function)
        if (collisionObj->getBroadphaseHandle())
            updateBroadphaseAABB(collisionObj, _broadphase, _dispatcher);
    }
}

void BulletBVHManager::setActiveCollisionObjects(const std::vector<std::string> &names)
{
    _active = names;

    // 现在需要使用正确的aabb更新broadphase
    for (std::pair<const std::string, CollisionObjectPtr>& co : _link2CollisionObj)
    {
        CollisionObjectPtr& collisionObj = co.second;
        updateCollisionObjectFilters(_active, *collisionObj);

        // broadphase树结构必须更新，因此需要删除和添加
        removeCollisionObjectFromBroadphase(collisionObj, _broadphase, _dispatcher);
        addCollisionObjectToBroadphase(collisionObj, _broadphase, _dispatcher);
    }
}

const std::vector<std::string> &BulletBVHManager::getActiveCollisionObjects() const
{
    return _active;
}

void BulletBVHManager::setContactDistanceThreshold(double contactDistance)
{
    _contactDistance = contactDistance;

    for (std::pair<const std::string, CollisionObjectPtr>& co : _link2CollisionObj)
    {
        CollisionObjectPtr& collisionObj = co.second;
        collisionObj->setContactProcessingThreshold(static_cast<btScalar>(contactDistance));
        if (collisionObj->getBroadphaseHandle())
            updateBroadphaseAABB(collisionObj, _broadphase, _dispatcher);
    }
}

double BulletBVHManager::getContactDistanceThreshold() const
{
    return _contactDistance;
}

const std::map<std::string, std::shared_ptr<CollisionObject> > &BulletBVHManager::getCollisionObjects() const
{
    return _link2CollisionObj;
}

std::shared_ptr<BulletCastBVHManager> BulletCastBVHManager::clone() const
{
    std::shared_ptr<BulletCastBVHManager> manager(new BulletCastBVHManager());

    for (const std::pair<const std::string, CollisionObjectPtr>& collisionObj : _link2CollisionObj)
    {
        CollisionObjectPtr newCollisionObj = collisionObj.second->clone();

        assert(newCollisionObj->getCollisionShape());
        assert(newCollisionObj->getCollisionShape()->getShapeType() != CUSTOM_CONVEX_SHAPE_TYPE);

        newCollisionObj->setWorldTransform(collisionObj.second->getWorldTransform());
        newCollisionObj->setContactProcessingThreshold(static_cast<btScalar>(_contactDistance));
        manager->addCollisionObject(newCollisionObj);
    }

    manager->setActiveCollisionObjects(_active);
    manager->setContactDistanceThreshold(_contactDistance);

    return manager;
}

void BulletCastBVHManager::setCastCollisionObjectsTransform(const std::string &name, const Msnhnet::Frame &pose1, const Msnhnet::Frame &pose2)
{
    auto it = _link2CollisionObj.find(name);
    if (it != _link2CollisionObj.end())
    {
        CollisionObjectPtr& collisionObj = it->second;
        assert(collisionObj->collisionFilterGroup == btBroadphaseProxy::KinematicFilter);

        btTransform tf1 = cvtMsnhFrame2Bt(pose1);
        btTransform tf2 = cvtMsnhFrame2Bt(pose2);

        collisionObj->setWorldTransform(tf1);
        _link2CollisionObj[name]->setWorldTransform(tf1);

        // If collision object is disabled dont proceed
        if (collisionObj->enabled)
        {
            if (btBroadphaseProxy::isConvex(collisionObj->getCollisionShape()->getShapeType()))
            {
                static_cast<CastHullShape*>(collisionObj->getCollisionShape())->updateCastTransform(tf1.inverseTimes(tf2));
            }
            else if (btBroadphaseProxy::isCompound(collisionObj->getCollisionShape()->getShapeType()))
            {
                btCompoundShape* compound = static_cast<btCompoundShape*>(collisionObj->getCollisionShape());

                for (int i = 0; i < compound->getNumChildShapes(); ++i)
                {
                    if (btBroadphaseProxy::isConvex(compound->getChildShape(i)->getShapeType()))
                    {
                        const btTransform& localTf = compound->getChildTransform(i);

                        btTransform delta_tf = (tf1 * localTf).inverseTimes(tf2 * localTf);
                        static_cast<CastHullShape*>(compound->getChildShape(i))->updateCastTransform(delta_tf);
                        compound->updateChildTransform(i, localTf, false);  // This is required to update the BVH tree
                    }
                    else if (btBroadphaseProxy::isCompound(compound->getChildShape(i)->getShapeType()))
                    {
                        btCompoundShape* secondCompound = static_cast<btCompoundShape*>(compound->getChildShape(i));

                        for (int j = 0; j < secondCompound->getNumChildShapes(); ++j)
                        {
                            assert(!btBroadphaseProxy::isCompound(secondCompound->getChildShape(j)->getShapeType()));
                            const btTransform& localTf = secondCompound->getChildTransform(j);

                            btTransform delta_tf = (tf1 * localTf).inverseTimes(tf2 * localTf);
                            static_cast<CastHullShape*>(secondCompound->getChildShape(j))->updateCastTransform(delta_tf);
                            secondCompound->updateChildTransform(j, localTf, false);  // This is required to update the BVH tree
                        }
                        secondCompound->recalculateLocalAabb();
                    }
                }
                compound->recalculateLocalAabb();
            }
            else
            {
                throw std::runtime_error("I can only continuous collision check convex shapes and compound shapes made of convex shapes");
            }

            //现在更新Broadphase AABB (See BulletWorld updateSingleAabb function)
            updateBroadphaseAABB(collisionObj, _broadphase, _dispatcher);
        }
    }
}

void BulletCastBVHManager::contactTest(CollisionResult &collisions, const CollisionRequest &req, const AllowedCollisionMatrix *acm, bool self)
{
    ContactTestData cdata(_active, _contactDistance, collisions, req);
    _broadphase->calculateOverlappingPairs(_dispatcher.get());
    btOverlappingPairCache* pairCache = _broadphase->getOverlappingPairCache();
    BroadphaseContactResultCallback cc(cdata, _contactDistance, acm, false, true);
    TesseractCollisionPairCallback collision_callback(_dispatcherInfo, _dispatcher.get(), cc);
    pairCache->processAllOverlappingPairs(&collision_callback, _dispatcher.get());
}

void BulletCastBVHManager::addCollisionObject(const CollisionObjectPtr &collisionObj)
{
    std::string name = collisionObj->getName();                  //StaticFilter
    if (collisionObj->collisionFilterGroup == btBroadphaseProxy::KinematicFilter)
    {
        CollisionObjectPtr castCollisionobj = makeCastCollisionObject(collisionObj);
        _link2CollisionObj[name] = castCollisionobj;
    }
    else
    {
        _link2CollisionObj[name] = collisionObj;
    }

    btVector3 aabb_min, aabb_max;
    _link2CollisionObj[name]->getAABB(aabb_min, aabb_max);

    int type = _link2CollisionObj[name]->getCollisionShape()->getShapeType();
    _link2CollisionObj[name]->setBroadphaseHandle(_broadphase->createProxy(aabb_min, aabb_max, type, _link2CollisionObj[name].get(),
                                                                           _link2CollisionObj[name]->collisionFilterGroup,
                                                                           _link2CollisionObj[name]->collisionFilterMask, _dispatcher.get()));
}

std::shared_ptr<BulletDiscreteBVHManager> BulletDiscreteBVHManager::clone() const
{
    std::shared_ptr<BulletDiscreteBVHManager> manager(new BulletDiscreteBVHManager());

    for (const std::pair<const std::string, CollisionObjectPtr>& collisionObj : _link2CollisionObj)
    {
        CollisionObjectPtr new_collisionObj = collisionObj.second->clone();

        assert(new_collisionObj->getCollisionShape());
        assert(new_collisionObj->getCollisionShape()->getShapeType() != CUSTOM_CONVEX_SHAPE_TYPE);

        new_collisionObj->setWorldTransform(collisionObj.second->getWorldTransform());
        new_collisionObj->setContactProcessingThreshold(static_cast<btScalar>(_contactDistance));
        manager->addCollisionObject(new_collisionObj);
    }

    manager->setActiveCollisionObjects(_active);
    manager->setContactDistanceThreshold(_contactDistance);
    return manager;
}

void BulletDiscreteBVHManager::contactTest(CollisionResult &collisions, const CollisionRequest &req, const AllowedCollisionMatrix *acm, bool self)
{
    ContactTestData cdata(_active, _contactDistance, collisions, req);

    _broadphase->calculateOverlappingPairs(_dispatcher.get());
    btOverlappingPairCache* pairCache = _broadphase->getOverlappingPairCache();

    BroadphaseContactResultCallback cc(cdata, _contactDistance, acm, self);
    TesseractCollisionPairCallback collision_callback(_dispatcherInfo, _dispatcher.get(), cc);
    pairCache->processAllOverlappingPairs(&collision_callback, _dispatcher.get());
}

void BulletDiscreteBVHManager::addCollisionObject(const CollisionObjectPtr &collisionObj)
{
    _link2CollisionObj[collisionObj->getName()] = collisionObj;
    addCollisionObjectToBroadphase(collisionObj, _broadphase, _dispatcher);
}
