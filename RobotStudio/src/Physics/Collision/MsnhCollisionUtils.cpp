﻿#include <Physics/Collision/MsnhCollisionUtils.h>
#include <Core/MsnhMesh.h>
#include <Core/MsnhModelLoader.h>
#include <bullet/LinearMath/btConvexHullComputer.h>
#include <bullet/BulletCollision/Gimpact/btGImpactShape.h>

/**
 * @brief 使用Bullet凸面外壳计算机从顶点创建凸面外壳
 * @param (Output) vertices 顶点向量
 * @param (Output) faces 索引
 * @param (input) input 用于创建凸面外壳的点向量
 * @param (input) shrink
 * @param (input) 如果为正，则凸面外壳将收缩该量（每个面沿其法线向中心移动“收缩”长度单位）。
 * @return 面数。如果小于零，则在尝试创建凸面外壳时发生错误
 */
inline int createConvexHull(QVector<QVector3D>& vertices, std::vector<int>& faces,
    const QVector<Vertex>& input, double shrink = -1, double shrinkClamp = -1)
{
    vertices.clear();
    faces.clear();

    btConvexHullComputer conv;
    btAlignedObjectArray<btVector3> points;

    points.reserve(static_cast<int>(input.size()));

    for (const Vertex& v : input)
    {
        QVector3D pos = v.position;
        points.push_back(btVector3(static_cast<btScalar>(pos[0]), static_cast<btScalar>(pos[1]), static_cast<btScalar>(pos[2])));
    }

    btScalar val = conv.compute(&points[0].getX(), sizeof(btVector3), points.size(), static_cast<btScalar>(shrink), static_cast<btScalar>(shrinkClamp));

    // 计算凸包错误
    if (val < 0)
    {
        return -1;
    }

    int numVerts = conv.vertices.size();

    vertices.reserve(static_cast<size_t>(numVerts));

    for (int i = 0; i < numVerts; i++)
    {
        btVector3& v = conv.vertices[i];
        vertices.push_back({v.getX(), v.getY(), v.getZ()});
    }

    int numFaces = conv.faces.size();

    faces.reserve(static_cast<size_t>(3 * numFaces));

    for (int i = 0; i < numFaces; i++)
    {
        std::vector<int> face;
        face.reserve(3);

        const btConvexHullComputer::Edge* sourceEdge = &(conv.edges[conv.faces[i]]);
        int a = sourceEdge->getSourceVertex();
        face.push_back(a);

        int b = sourceEdge->getTargetVertex();
        face.push_back(b);

        const btConvexHullComputer::Edge* edge = sourceEdge->getNextEdgeOfFace();
        int c = edge->getTargetVertex();
        face.push_back(c);

        edge = edge->getNextEdgeOfFace();
        c = edge->getTargetVertex();
        while (c != a)
        {
            face.push_back(c);

            edge    = edge->getNextEdgeOfFace();
            c       = edge->getTargetVertex();
        }

        faces.push_back(static_cast<int>(face.size()));
        faces.insert(faces.end(), face.begin(), face.end());
    }

    return numFaces;
}
/// \brief 将几何图形强制转换为btCollisionShape
btCollisionShape* createShapePrimitive(const std::shared_ptr<Msnhnet::URDFGeometry>& geom, const CollisionObjectType& collisionObjectTypes,
    CollisionObject* collisonObj)
{
    if(geom->type == Msnhnet::URDF_BOX)
    {
        auto tmpGeom = *(Msnhnet::URDFBox*)(geom.get());
        return (new btBoxShape(btVector3(btScalar(tmpGeom.dim[0]/2),btScalar(tmpGeom.dim[1]/2),btScalar(tmpGeom.dim[2]/2))));
    }
    else if(geom->type == Msnhnet::URDF_SPHERE)
    {
        auto tmpGeom = *(Msnhnet::URDFSphere*)(geom.get());
        return (new btSphereShape(btScalar(tmpGeom.radius)));
    }
    else if(geom->type == Msnhnet::URDF_CYLINDER)
    {
        auto tmpGeom = *(Msnhnet::URDFCylinder*)(geom.get());
        return (new btCylinderShapeZ(btVector3(btScalar(tmpGeom.radius/2),btScalar(tmpGeom.radius/2),btScalar(tmpGeom.length))));
    }
    else if(geom->type == Msnhnet::URDF_CONE)
    {
        auto tmpGeom = *(Msnhnet::URDFCone*)(geom.get());
        return (new btConeShapeZ(btScalar(tmpGeom.radius),btScalar(tmpGeom.length)));
    }
    else if(geom->type == Msnhnet::URDF_MESH)
    {
        auto tmpGeom = *(Msnhnet::URDFMesh*)(geom.get());
        assert(collisionObjectTypes == COLLISION_USE_SHAPE ||
            collisionObjectTypes == COLLISION_CONVEX_HULL ||
            collisionObjectTypes == COLLISION_MULTI_SPHERE);

        std::string filename = tmpGeom.filename;

        ModelLoader loader;

        qDebug()<<QString::fromStdString(filename);
        Mesh* mesh = loader.loadMeshFromFile(QString::fromStdString(filename)); //TODO: 加载优化，内存提取

        if(mesh->getVertices().size() >0 && mesh->getIndices().size()>0)
        {
            switch (collisionObjectTypes)
            {
            case COLLISION_USE_SHAPE:
            {
                QVector<QVector3D> vertices;
                std::vector<int> faces;

                if (createConvexHull(vertices, faces, mesh->getVertices()) < 0)
                    return nullptr;

                btConvexHullShape* subshape = new btConvexHullShape();

                for (const QVector3D& v : vertices)
                {
                    subshape->addPoint(btVector3(static_cast<btScalar>(v[0]), static_cast<btScalar>(v[1]), static_cast<btScalar>(v[2])));
                }

                return subshape;
            }
            case COLLISION_CONVEX_HULL:
            {
                btCompoundShape* compound = new btCompoundShape(BULLET_COMPOUND_USE_DYNAMIC_AABB, static_cast<int>(mesh->getVertices().size()));
                compound->setMargin(
                    BULLET_MARGIN);  // margin: compound seems to have no effect when positive but has an effect when negative

                for (size_t i = 0; i < mesh->getIndices().size(); i+=3)
                {
                    btVector3 v[3];

                    for (unsigned x = 0; x < 3; ++x)
                    {
                        unsigned idx = mesh->getIndices()[i + x];

                        QVector3D pos = mesh->getVertices()[idx].position;

                        v[x] = btVector3(btScalar(pos.x()),btScalar(pos.y()),btScalar(pos.z()));
                    }

                    btCollisionShape* subshape = new btTriangleShapeEx(v[0], v[1], v[2]);

                    if (subshape != nullptr)
                    {
                        collisonObj->manage(subshape);
                        subshape->setMargin(BULLET_MARGIN);
                        btTransform geomTrans;
                        geomTrans.setIdentity();
                        compound->addChildShape(geomTrans, subshape);
                    }
                }

                return compound;
            }
            default:
                return nullptr;
            }
        }
    }
}


CollisionObject::CollisionObject(const std::string &name, const BodyType &type, const std::vector<std::shared_ptr<Msnhnet::URDFGeometry> > &shapes,
    const std::vector<Msnhnet::Frame> &shapePoses, const std::vector<CollisionObjectType> &collisionObjectTypes,
    bool active):
    _name(name),
    _bodyType(type),
    _shapes(shapes),
    _shapePoses(shapePoses),
    _collisionObjectTypes(collisionObjectTypes)
{
    if (shapes.empty() || shapePoses.empty() || (shapes.size() != shapePoses.size() || collisionObjectTypes.empty() || shapes.size() != collisionObjectTypes.size()))
    {
        throw Msnhnet::Exception(1,"错误",__FILE__,__LINE__,__FUNCTION__); //TODO: Exception
    }

    setContactProcessingThreshold(BULLET_DEFAULT_CONTACT_DISTANCE);

    if (active)
    {
        collisionFilterGroup = btBroadphaseProxy::KinematicFilter;
        collisionFilterMask = btBroadphaseProxy::KinematicFilter | btBroadphaseProxy::StaticFilter;
    }
    else
    {
        collisionFilterGroup = btBroadphaseProxy::StaticFilter;
        collisionFilterMask = btBroadphaseProxy::KinematicFilter;
    }

    if (shapes.size() == 1)
    {
        btCollisionShape* shape = createShapePrimitive(_shapes[0], _collisionObjectTypes[0], this);

        if(!shape)
            throw std::exception();//TODO: Exception

        shape->setMargin(BULLET_MARGIN);
        manage(shape);
        setCollisionShape(shape);
        setWorldTransform(cvtMsnhFrame2Bt(_shapePoses[0]));
    }
    else
    {
        btCompoundShape* compound = new btCompoundShape(BULLET_COMPOUND_USE_DYNAMIC_AABB, static_cast<int>(_shapes.size()));
        manage(compound);
        // margin on compound seems to have no effect when positive but has an effect when negative
        compound->setMargin(BULLET_MARGIN);
        setCollisionShape(compound);

        setWorldTransform(cvtMsnhFrame2Bt(_shapePoses[0]));

        Msnhnet::Frame invWorld = _shapePoses[0].invert();

        for (std::size_t j = 0; j < _shapes.size(); ++j)
        {
            btCollisionShape* subshape = createShapePrimitive(_shapes[j], _collisionObjectTypes[j], this);
            if(!subshape)
            {
                throw Msnhnet::Exception(1,"错误",__FILE__,__LINE__,__FUNCTION__); //TODO: Exception
            }


            if (subshape != nullptr)
            {
                manage(subshape);
                subshape->setMargin(BULLET_MARGIN);
                btTransform geomTrans = cvtMsnhFrame2Bt(invWorld * _shapePoses[j]);
                compound->addChildShape(geomTrans, subshape);
            }
        }
    }
}

CollisionObject::CollisionObject(const std::string &name, const BodyType &type, const std::vector<std::shared_ptr<Msnhnet::URDFGeometry> > &shapes,
    const std::vector<Msnhnet::Frame> &shapePoses, const std::vector<CollisionObjectType> &collisionObjectTypes,
    const std::set<std::string> &touchLinks)
    :CollisionObject(name, type, shapes, shapePoses, collisionObjectTypes, true)
{
    this->touchLinks = touchLinks;
}

CollisionObject::CollisionObject(const std::string &name, const BodyType &type, const std::vector<std::shared_ptr<Msnhnet::URDFGeometry> > &shapes,
    const std::vector<Msnhnet::Frame> &shapePoses, const std::vector<CollisionObjectType> &collisionObjectTypes,
    const std::vector<std::shared_ptr<void> > &data):
    _name(name),
    _bodyType(type),
    _shapes(shapes),
    _shapePoses(shapePoses),
    _collisionObjectTypes(collisionObjectTypes),
    _data(data)
{

}

CastHullShape::CastHullShape(btConvexShape *shape, const btTransform &transform):shape(shape), transform(transform)
{
    m_shapeType = CUSTOM_CONVEX_SHAPE_TYPE; //凸包类型
}

btVector3 CastHullShape::localGetSupportingVertex(const btVector3 &vec) const
{
    btVector3 supportVec1 = shape->localGetSupportingVertex(vec);
    btVector3 supportVec2 = transform*shape->localGetSupportingVertex(vec*transform.getBasis());

    return (vec.dot(supportVec1) > vec.dot(supportVec2))? supportVec1:supportVec2;
}

void CastHullShape::getAabb(const btTransform &transformWorld, btVector3 &aabbMin, btVector3 &aabbMax) const
{
    shape->getAabb(transformWorld, aabbMin, aabbMax);
    btVector3 min1, max1;
    shape->getAabb(transformWorld * transform, min1, max1);
    aabbMin.setMin(min1);
    aabbMax.setMax(max1);
}

const btVector3 &CastHullShape::getLocalScaling() const
{
    static btVector3 out(1, 1, 1);
    return out;
}

void AllowedCollisionMatrix::setEntry(const std::string &name1, const std::string &name2, bool allowed)
{
    const AllowedCollisionType v = allowed? ALLOW_COLLISION_ALWAYS:ALLOW_COLLISION_NEVER;

    _entries[name1][name2] = _entries[name2][name1] = v;

    //如果之前存在碰撞对, 且存在决定碰撞的回调函数,删除
    auto it = _allowedContacts.find(name1);
    if(it != _allowedContacts.end())
    {
        auto jt = it->second.find(name2);
        if(jt != it->second.end())
        {
            it->second.erase(jt);
        }
    }

    it = _allowedContacts.find(name2);
    if(it != _allowedContacts.end())
    {
        auto jt = it->second.find(name1);
        if(jt != it->second.end())
        {
            it->second.erase(jt);
        }
    }

}

void AllowedCollisionMatrix::setEntry(const std::string &name1, const std::string &name2, const DecideContactFun &fun)
{
    _entries[name1][name2] = _entries[name2][name1] = ALLOW_COLLISION_CONDITIONAL;
    _allowedContacts[name1][name2] = _allowedContacts[name2][name1] = fun;
}

void AllowedCollisionMatrix::setEntry(const std::string &name, bool allowed)
{
    std::string last = name;

    for(auto& entry : _entries)
    {
        if(name != entry.first && last != entry.first)
        {
            last = entry.first;
            setEntry(name, entry.first, allowed);
        }
    }
}

void AllowedCollisionMatrix::setEntry(const std::string &name, const std::vector<std::string> &otheNames, bool allowed)
{
    for (const auto& otherName : otheNames)
    {
        if (otherName != name)
            setEntry(otherName, name, allowed);
    }
}

void AllowedCollisionMatrix::setEntry(const std::vector<std::string> &names1, const std::vector<std::string> &names2, bool allowed)
{
    for (const auto& name1 : names1)
        setEntry(name1, names2, allowed);
}

void AllowedCollisionMatrix::setEntry(bool allowed)
{
    const auto v = allowed ? ALLOW_COLLISION_ALWAYS:ALLOW_COLLISION_NEVER;
    for (auto& entry : _entries)
    {
        for (auto& it2 : entry.second)
        {
            it2.second = v;
        }
    }
}

void AllowedCollisionMatrix::setDefaultEntry(const std::string &name, bool allowed)
{
    const AllowedCollisionType v = allowed? ALLOW_COLLISION_ALWAYS:ALLOW_COLLISION_NEVER;

    _defaultEntries[name] = v;
    _defaultAllowedContacts.erase(name);
}

void AllowedCollisionMatrix::setDefaultEntry(const std::string &name, const DecideContactFun &fun)
{
    _defaultEntries[name] =ALLOW_COLLISION_CONDITIONAL;
    _defaultAllowedContacts[name] = fun;
}

bool AllowedCollisionMatrix::getEntry(const std::string &name1, const std::string &name2, AllowedCollisionType &allowedCollisionType) const
{
    auto it1 = _entries.find(name1);
    if (it1 == _entries.end())
        return false;

    auto it2 = it1->second.find(name2);
    if (it2 == it1->second.end())
        return false;

    allowedCollisionType = it2->second;
    return true;
}

bool AllowedCollisionMatrix::getEntry(const std::string &name1, const std::string &name2, DecideContactFun &fun) const
{
    auto it1 = _allowedContacts.find(name1);
    if (it1 == _allowedContacts.end())
        return false;

    auto it2 = it1->second.find(name2);
    if (it2 == it1->second.end())
        return false;

    fun = it2->second;
    return true;
}

bool AllowedCollisionMatrix::hasEntry(const std::string &name) const
{
    return _entries.find(name) != _entries.end();
}

void AllowedCollisionMatrix::removeEntry(const std::string &name)
{
    _entries.erase(name);
    _allowedContacts.erase(name);
    for (auto& entry : _entries)
        entry.second.erase(name);
    for (auto& allowedContact : _allowedContacts)
        allowedContact.second.erase(name);
}

void AllowedCollisionMatrix::removeEntry(const std::string &name1, const std::string &name2)
{
    auto jt = _entries.find(name1);
    if (jt != _entries.end())
    {
        auto it = jt->second.find(name2);
        if (it != jt->second.end())
            jt->second.erase(it);
    }
    jt = _entries.find(name2);
    if (jt != _entries.end())
    {
        auto it = jt->second.find(name1);
        if (it != jt->second.end())
            jt->second.erase(it);
    }

    auto it = _allowedContacts.find(name1);
    if (it != _allowedContacts.end())
    {
        auto jt = it->second.find(name2);
        if (jt != it->second.end())
            it->second.erase(jt);
    }
    it = _allowedContacts.find(name2);
    if (it != _allowedContacts.end())
    {
        auto jt = it->second.find(name1);
        if (jt != it->second.end())
            it->second.erase(jt);
    }
}

void AllowedCollisionMatrix::getAllEntryNames(std::vector<std::string> &names) const
{
    names.clear();
    for (const auto& entry : _entries)
    {
        if (!names.empty() && names.back() == entry.first)
            continue;
        else
            names.push_back(entry.first);
    }
}

void AllowedCollisionMatrix::clear()
{
    _entries.clear();
    _allowedContacts.clear();
    _defaultEntries.clear();
    _defaultAllowedContacts.clear();
}

std::size_t AllowedCollisionMatrix::getSize() const
{
    return _entries.size();
}

bool AllowedCollisionMatrix::getDefaultEntry(const std::string &name, AllowedCollisionType &allowedCollision) const
{
    auto it = _defaultEntries.find(name);
    if (it == _defaultEntries.end())
        return false;
    allowedCollision = it->second;
    return true;
}

bool AllowedCollisionMatrix::getDefaultEntry(const std::string &name, DecideContactFun &fn) const
{
    auto it = _defaultAllowedContacts.find(name);
    if (it == _defaultAllowedContacts.end())
        return false;
    fn = it->second;
    return true;
}

static bool andDecideContact(const DecideContactFun& f1, const DecideContactFun& f2, Contact& contact)
{
    return f1(contact) && f2(contact);
}

bool AllowedCollisionMatrix::getAllowedCollision(const std::string &name1, const std::string &name2, DecideContactFun &fun) const
{
    DecideContactFun fun1, fun2;
    bool found1 = getDefaultEntry(name1, fun1);
    bool found2 = getDefaultEntry(name2, fun2);

    if (!found1 && !found2)
        return getEntry(name1, name2, fun);
    else
    {
        if (found1 && !found2)
            fun = fun1;
        else if (!found1 && found2)
            fun = fun2;
        else if (found1 && found2)
            fun = std::bind(&andDecideContact, fun1, fun2, std::placeholders::_1);
        else
            return false;
        return true;
    }
}

bool AllowedCollisionMatrix::getAllowedCollision(const std::string &name1, const std::string &name2, AllowedCollisionType &allowedCollision) const
{
    AllowedCollisionType t1, t2;
    bool found1 = getDefaultEntry(name1, t1);
    bool found2 = getDefaultEntry(name2, t2);

    if (!found1 && !found2)
        return getEntry(name1, name2, allowedCollision);
    else
    {
        if (found1 && !found2)
            allowedCollision = t1;
        else if (!found1 && found2)
            allowedCollision = t2;
        else if (found1 && found2)
        {
            if (t1 == ALLOW_COLLISION_NEVER || t2 == ALLOW_COLLISION_NEVER)
                allowedCollision = ALLOW_COLLISION_NEVER;
            else if (t1 == ALLOW_COLLISION_CONDITIONAL || t2 == ALLOW_COLLISION_CONDITIONAL)
                allowedCollision = ALLOW_COLLISION_CONDITIONAL;
            else  // ALWAYS is the only remaining case
                allowedCollision = ALLOW_COLLISION_ALWAYS;
        }
        else
            return false;
        return true;
    }
}

bool AllowedCollisionMatrix::hasEntry(const std::string &name1, const std::string &name2) const
{
    auto it1 = _entries.find(name1);
    if (it1 == _entries.end())
        return false;
    auto it2 = it1->second.find(name2);
    return it2 != it1->second.end();
}
