﻿#include "Physics/MsnhPhysicalEngine.h"

QMutex PhysicalEngine::_mutex;
QSharedPointer<PhysicalEngine> PhysicalEngine::_instance;

QSharedPointer<PhysicalEngine> &PhysicalEngine::getInstance()
{
    if(_instance.isNull())
    {
        QMutexLocker mutexLocker(&_mutex);
        if(_instance.isNull())
        {
            _instance = QSharedPointer<PhysicalEngine>(new PhysicalEngine());
        }
    }
    return _instance;
}

void PhysicalEngine::addRigidBody(Model *model)
{
    _models.push_back(model);

    _rigidBodies.push_back(model->getRigidBody());

    if(model->getRigidBody())
        _world->addRigidBody(model->getRigidBody());

    connect(model, &Model::destroying, this, &PhysicalEngine::removeModel);
}

PhysicalEngine::PhysicalEngine()
{
    _collisionCfg   = new btDefaultCollisionConfiguration();
    _dispatcher     = new btCollisionDispatcher(_collisionCfg);
    _broadPhase     = new btDbvtBroadphase();
    _solver         = new btSequentialImpulseConstraintSolver();
    _world          = new btDiscreteDynamicsWorld(_dispatcher, _broadPhase, _solver, _collisionCfg);
    _world->setGravity(btVector3(0, 0, -9.81f));

    _timer           = new QTimer(this);
    connect(_timer, &QTimer::timeout, this, &PhysicalEngine::runSimulation);
}

PhysicalEngine::~PhysicalEngine()
{
    _timer->stop();
    delete _world         ;
    delete _solver        ;
    delete _broadPhase    ;
    delete _dispatcher    ;
    delete _collisionCfg  ;
}

void PhysicalEngine::removeModel(Model *model)
{
    int index = 0;

    auto begin = _models.begin();

    while(begin != _models.end())
    {
        if(*begin == model)
        {
            _models.removeAt(index);
            _world->removeRigidBody(_rigidBodies[index]);
            _rigidBodies.removeAt(index);
            return;
        }
        index ++;
        begin ++;
    }
}

void PhysicalEngine::startEngine()
{
    EngineUtil::physicalEngineRunning = true;
    _timer->start(10);
}

void PhysicalEngine::stopEngine()
{
    EngineUtil::physicalEngineRunning = false;
    _timer->stop();
}

void PhysicalEngine::setDebugDrawer(btIDebugDraw *debugDrawer)
{
    _world->setDebugDrawer(debugDrawer);
}

void PhysicalEngine::runSimulation()
{
    float dt = btclock.getTimeMilliseconds();
    btclock.reset();

    if(_world)
    {
        _world->stepSimulation(dt*10);
    }

    auto begin = _models.begin();

    while(begin != _models.end())
    {
        (*begin)->updatePhysical();
        begin++;
    }
}

btDynamicsWorld *PhysicalEngine::getWorld() const
{
    return _world;
}
