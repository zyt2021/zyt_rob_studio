﻿#include <Plugin/MsnhLoopThread.h>
#include <QDebug>

MsnhLoopThread::MsnhLoopThread(MsnhProcess *process, bool needSleep, unsigned long sleepMS, QObject *parent)
    :QThread(parent),_process(process),_needSleep(needSleep),_sleepMS(sleepMS)
{

}

MsnhLoopThread::~MsnhLoopThread()
{
    stopIt();
    quit();
    wait();
}

void MsnhLoopThread::startIt()
{
    start();
    _running = true;
}

void MsnhLoopThread::stopIt()
{
     _running = false;
}

void MsnhLoopThread::run()
{
    while(_running)
    {
        if(_process)
        {
            if(_process->processInputData(nullptr))
            {
                _revData = _process->getResultData();
                emit revData(_revData);
            }

            if(_needSleep)
            {
                msleep(_sleepMS);
            }
        }
    }
}

void MsnhLoopThread::setProcess(MsnhProcess *process)
{
    _process = process;
}

MsnhProcess *MsnhLoopThread::getProcess() const
{
    return _process;
}

bool MsnhLoopThread::getNeedSleep() const
{
    return _needSleep;
}

void MsnhLoopThread::setNeedSleep(bool newNeedSleep)
{
    _needSleep = newNeedSleep;
}

unsigned long MsnhLoopThread::getSleepMS() const
{
    return _sleepMS;
}

void MsnhLoopThread::setSleepMS(unsigned long newSleepMS)
{
    _sleepMS = newSleepMS;
}
