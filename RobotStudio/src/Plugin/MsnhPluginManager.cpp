#include <Plugin/MsnhPluginManager.h>
#include <QMessageBox>
#include <QDebug>
#include <QDir>
#include <QDateTime>

MsnhPluginManager::MsnhPluginManager()
{

}

MsnhPluginManager::~MsnhPluginManager()
{
    unloadPlugins();
}

void MsnhPluginManager::loadPlugins(const QString &pluginPath, MsnhProcessFactory *factory)
{
    // plugin loading doesn't work in debug mode
#ifdef QT_DEBUG
    return;
#endif

#if defined(_WIN32)
    static const auto pluginFilter = QStringList() << "*.dll";
#else
    static const auto pluginFilter = QStringList() << "*.so";
#endif

    qDebug() << "MsnhPluginManager::loadPlugins: " << pluginPath;

    _pluginPath = pluginPath;
    _factory = factory;
    _loadedPlugins.clear();

    QDir pluginsDir(_pluginPath);

    // watch the original dll for changes
    // _pluginFileSystemWatcher->addPath(pluginPath());

    QDateTime now = QDateTime::currentDateTime();
    _pluginTmpPath = _pluginPath + "/tmp/";
    QDir tmpPluginsDir(_pluginTmpPath);

    // delete old tmp directory
    if(!removeDir(_pluginTmpPath))
    {
        qWarning() << "Could not remove old plugin dir: " << _pluginTmpPath;
        return;
    }

    // create new directory, copy dlls
    if(!tmpPluginsDir.exists())
        tmpPluginsDir.mkpath(".");

    pluginsDir.setNameFilters(pluginFilter);
    foreach (QString fileName, pluginsDir.entryList(QDir::Files))
    {
        QFile file(pluginsDir.filePath(fileName));
        file.copy(tmpPluginsDir.filePath(fileName));
    }

    if(tmpPluginsDir.exists())
    {
        tmpPluginsDir.setNameFilters(pluginFilter);

        foreach (QString fileName, tmpPluginsDir.entryList(QDir::Files))
        {
            QString pluginFilePath = _pluginTmpPath + "/" + fileName;
            pugg::Kernel* kernel = new pugg::Kernel;
            _kernels.push_back(kernel);
            kernel->add_server(MsnhProcess::serverName(), MsnhProcess::version);
            kernel->load_plugin(pluginFilePath.toStdString());

            // we can load all drivers from a specific server
            _drivers = kernel->get_all_drivers<MsnhProcessDriver>(MsnhProcess::serverName());

            if(_drivers.size() == 0)
            {
                QString msg("Plugin version does not match MsnhLibs API version %2.\nCan't load %3.");
                QMessageBox::warning(NULL, "Plugin Error", msg.arg(MSNH_LIBS_VERSION).arg(fileName));
                continue;
            }

            for (std::vector<MsnhProcessDriver*>::iterator iter = _drivers.begin(); iter != _drivers.end(); ++iter) {
                MsnhProcessDriver& driver = *(*iter);

                QString className = QString::fromStdString(driver.className());
                QString author = QString::fromStdString(driver.author());
                int version = driver.version();

                MsnhProcess* pluginInstance = driver.create();
                _factory->registerProcess(className, pluginInstance);
                _loadedPlugins.push_back(className);
            }
        }
    }
}

void MsnhPluginManager::unloadPlugins()
{

    // unregister process
    for(int i=0; i<_loadedPlugins.count(); i++)
    {
        QString plugin = _loadedPlugins.at(i);
        _factory->unregisterProcess(plugin);
    }
    _loadedPlugins.clear();
    // unload DLL
    for(int i=0; i<_kernels.count(); i++)
    {
        pugg::Kernel* kernel = _kernels.at(i);
        kernel->clear();
    }
    _kernels.clear();
    _drivers.clear();

    // delete old tmp directory
    removeDir(_pluginTmpPath);
}

bool MsnhPluginManager::removeDir(const QString &dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists(dirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }

    return result;
}
