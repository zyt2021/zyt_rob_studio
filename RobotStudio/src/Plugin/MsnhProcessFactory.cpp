﻿#include <Plugin/MsnhProcessFactory.h>
#include <QDebug>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

MsnhProcessFactory::MsnhProcessFactory()
{

}

MsnhProcessFactory::~MsnhProcessFactory()
{
    unregisterAllProcess();
}

void MsnhProcessFactory::unregisterProcess(const QString &name)
{
    //qDebug() << "MsnhProcessFactory::unregisterProcess: " << name ;
    if(_processMap.contains(name))
    {
        // delete instance and remove from map
         delete _processMap.value(name);
         _processMap.remove(name);
    }
}

void MsnhProcessFactory::unregisterAllProcess()
{
    if(_processMap.isEmpty())
    {
        return;
    }

    auto bg = _processMap.begin();

    while(bg != _processMap.end())
    {
        delete bg.value();
        bg.value() = nullptr;
        bg++;
    }
    _processMap.clear();
    _2dCamIO.clear();
}

MsnhProcess *MsnhProcessFactory::getProcInstance(const QString &name)
{
    //qDebug() << "MsnhProcessFactory::getInstance: " << name;
    if(_processMap.contains(name))
    {
        // return a fresh copy of the template process
        MsnhProcess* process = _processMap.value(name)->clone();
        process->init();
        return process;
    }
    else
    {
        return nullptr;
    }
}

QVector<QString> MsnhProcessFactory::get2dCamIO() const
{
    return _2dCamIO;
}

void MsnhProcessFactory::registerProcess(const QString &name, MsnhProcess *process)
{
    qDebug() << "注册插件: " << name;
    _processMap.insert(name, process);
    if(process->getCategory() == MsnhProcess::CATEGORY_2D_CAM_IO)
    {
        _2dCamIO.push_back(name); //保存所有2D相机IO名称
    }
}
