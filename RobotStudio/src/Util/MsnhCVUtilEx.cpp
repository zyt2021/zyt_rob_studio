﻿#include <Util/MsnhCVUtilEx.h>
#include <opencv2/opencv.hpp>
#include <QTimer>
#include <QEventLoop>
MsnhCVUtil::MsnhCVUtil()
{

}

cv::Mat MsnhCVUtil::QImageToMat(const QImage& image)
{
    cv::Mat mat;
    switch (image.format())
    {
    case QImage::Format_ARGB32:
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32_Premultiplied:
        mat = cv::Mat(image.height(), image.width(), CV_8UC4, (void*)image.constBits(), image.bytesPerLine());
        break;
    case QImage::Format_RGB888:
        mat = cv::Mat(image.height(), image.width(), CV_8UC3, (void*)image.constBits(), image.bytesPerLine());
        cv::cvtColor(mat, mat, cv::COLOR_BGR2RGB);
        break;
    case QImage::Format_Indexed8:
        mat = cv::Mat(image.height(), image.width(), CV_8UC1, (void*)image.constBits(), image.bytesPerLine());
        break;
    }
    return mat;
}

QImage MsnhCVUtil::MatToQImage(const cv::Mat &mat)
{
    // 8-bits unsigned, NO. OF CHANNELS = 1
    if(mat.type() == CV_8UC1)
    {
        QImage image(mat.cols, mat.rows, QImage::Format_Indexed8);
        // Set the color table (used to translate colour indexes to qRgb values)
        image.setColorCount(256);
        for(int i = 0; i < 256; i++)
        {
            image.setColor(i, qRgb(i, i, i));
        }
        // Copy input Mat
        uchar *pSrc = mat.data;
        for(int row = 0; row < mat.rows; row ++)
        {
            uchar *pDest = image.scanLine(row);
            memcpy(pDest, pSrc, mat.cols);
            pSrc += mat.step;
        }
        return image;
    }
    // 8-bits unsigned, NO. OF CHANNELS = 3
    else if(mat.type() == CV_8UC3)
    {
        // Copy input Mat
        const uchar *pSrc = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
        return image.rgbSwapped();
    }
    else if(mat.type() == CV_8UC4)
    {
        //qDebug() << "CV_8UC4";
        // Copy input Mat
        const uchar *pSrc = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_ARGB32);
        return image.copy();
    }
    else
    {
        return QImage();
    }
}

bool MsnhCVUtil::isRotationMatrix(cv::Mat &R)
{
    cv::Mat Rt;
    cv::transpose(R,Rt);
    cv::Mat shouldBeIdentity = Rt * R;
    cv::Mat I = cv::Mat::eye(3,3, shouldBeIdentity.type());
    return  norm(I, shouldBeIdentity) < 1e-6;
}

void MsnhCVUtil::cvDelay(int mSec)
{
    QEventLoop loop;
    QTimer::singleShot(mSec, &loop, SLOT(quit()));
    loop.exec();
}

cv::Mat MsnhCVUtil::rot2euler(cv::Mat &R, MsnhCVUtil::Sequence seq)
{
    assert(isRotationMatrix(R));

    cv::Mat tmp = rot2quat(R);

    return quat2euler(tmp,seq);
}

cv::Mat MsnhCVUtil::rot2quat(cv::Mat &R)
{
    assert(isRotationMatrix(R));
    double q[4];
    q[0] = sqrt(1+R.at<double>(0,0)+R.at<double>(1,1)+R.at<double>(2,2))/2;
    q[1] = (R.at<double>(2,1) - R.at<double>(1,2)) / (4*q[0]) ;
    q[2] = (R.at<double>(0,2) - R.at<double>(2,0)) / (4*q[0]) ;
    q[3] = (R.at<double>(1,0) - R.at<double>(0,1)) / (4*q[0]) ;
    return cv::Mat_<double>(1,4)<<q[0],q[1],q[2],q[3];
}

cv::Mat MsnhCVUtil::quat2rot(cv::Mat &quat)
{
    double q0 = quat.at<double>(0);
    double q1 = quat.at<double>(1);
    double q2 = quat.at<double>(2);
    double q3 = quat.at<double>(3);

    return cv::Mat_<double>(3,3)
            << q0*q0+q1*q1-q2*q2-q3*q3,  2.0*(q1*q2-q0*q3),         2.0*(q1*q3+q0*q2),
            2.0*(q1*q2+q0*q3),        q0*q0-q1*q1+q2*q2-q3*q3,   2.0*(q2*q3-q0*q1),
            2.0*(q1*q3-q0*q2),        2.0*(q2*q3+q0*q1),         q0*q0-q1*q1-q2*q2+q3*q3;
}

cv::Mat MsnhCVUtil::euler2rot(cv::Mat &euler, MsnhCVUtil::Sequence seq)
{
    cv::Mat rotationMatrix(3,3,CV_64F);

    //R = R(z)R(y)R(x)
    //R = R(c)R(b)R(a)

    //Rx = | 1   0     0   |
    //     | 0  cosa -sina |
    //     | 0  sina  cosa |

    //Ry = |  cosb  0  sinb |
    //     |   0    1    0  |
    //     | -sinb  0  cosb |

    //Rz = | cosc  -sinc  0 |
    //     | sinc  cosc   0 |
    //     |  0     0     1 |
    double sina = sin(euler.at<double>(0)); //x
    double sinb = sin(euler.at<double>(1)); //y
    double sinc = sin(euler.at<double>(2)); //z

    double cosa = cos(euler.at<double>(0)); //x
    double cosb = cos(euler.at<double>(1)); //y
    double cosc = cos(euler.at<double>(2)); //z

    cv::Mat Rx = (cv::Mat_<double>(3,3)<<
                  1 ,  0   ,   0   ,
                  0 , cosa , -sina ,
                  0 , sina ,  cosa
                  );

    cv::Mat Ry = (cv::Mat_<double>(3,3)<<
                  cosb , 0 , sinb ,
                  0   , 1 ,   0  ,
                  -sinb , 0 , cosb
                  );

    cv::Mat Rz = (cv::Mat_<double>(3,3)<<
                  cosc , -sinc , 0 ,
                  sinc , cosc  , 0 ,
                  0   ,  0    , 1
                  );

    switch (seq)
    {
    case Sequence::XYZ:
        return Rx*Ry*Rz;
    case Sequence::ZYX:
        return Rz*Ry*Rx;
    case Sequence::ZYZ:
        return Rz*Ry*Rz;
    }
}

cv::Mat MsnhCVUtil::euler2quat(cv::Mat &euler, MsnhCVUtil::Sequence seq)
{
    cv::Mat tmp = euler2rot(euler,seq);
    return rot2quat(tmp);
}

cv::Mat MsnhCVUtil::quat2euler(cv::Mat &quat, MsnhCVUtil::Sequence seq)
{
    cv::Mat euler(1,3,CV_64F);
    double yaw;
    double pitch;
    double roll;

    double q[]={quat.at<double>(0),
                quat.at<double>(1),
                quat.at<double>(2),
                quat.at<double>(3)};

    switch (seq)
    {
    case MsnhCVUtil::Sequence::XYZ:
        ///psi   RZ
        pitch= atan2(2*(q[0]*q[1]-q[2]*q[3]),
                q[0]*q[0]-q[1]*q[1]-q[2]*q[2]+q[3]*q[3]);

        ///theta RY
        yaw  = asin(2*(q[0]*q[2] + q[1]*q[3]));

        ///phi   RX
        roll = atan2(2*(q[0]*q[3]-q[1]*q[2]),
                q[0]*q[0]+q[1]*q[1]-q[2]*q[2]-q[3]*q[3]);
        break;

    case MsnhCVUtil::Sequence::ZYX:
        pitch=atan2(2 * (q[1] * q[2] + q[3] * q[0]),
                (q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]));

        yaw = asin(2 * (q[2] * q[0] - q[1] * q[3]));

        roll =atan2(2 * (q[1] * q[0] + q[3] * q[2]),
                (q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]));
        break;

    case MsnhCVUtil::Sequence::ZYZ:
        pitch = atan2((q[2] * q[3] - q[1] * q[0]), (q[1] * q[3] + q[2] * q[0]));

        yaw = acos(q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]);

        roll = atan2((q[1] * q[0] + q[2] * q[3]), (q[2] * q[0] - q[1] * q[3]));
        break;
    }

    euler.at<double>(0) = pitch;
    euler.at<double>(1) = yaw;
    euler.at<double>(2) = roll;

    return euler;
}
