﻿#include <Util/MsnhDiskSN.h>

DiskSN::DiskSN()
{

}
#ifdef __unix__
QString DiskSN::getSN()
{
    QProcess pro;
    pro.start("df -h /var/");
    pro.waitForStarted();
    pro.waitForFinished();
    QString temp=pro.readAll();

    uchar flag =0;  //1 sd, 2 nvme

    if(temp.contains("/dev/sda"))
    {
        pro.start("sudo hdparm -i /dev/sda");
        flag=1;
    }
    else if(temp.contains("/dev/sdb"))
    {
        pro.start("sudo hdparm -i /dev/sdb | grep SerialNo");
        flag=1;
    }
    else if(temp.contains("/dev/sdc"))
    {
        pro.start("sudo hdparm -i /dev/sdc | grep SerialNo");
        flag=1;
    }
    else if(temp.contains("/dev/sdd"))
    {
        pro.start("sudo hdparm -i /dev/sdd | grep SerialNo");
        flag=1;
    }
    else if(temp.contains("/dev/nvme0n1"))
    {
        pro.start("sudo nvme id-ctrl /dev/nvme0n1 | grep sn");
        flag=2;
    }
    else if(temp.contains("/dev/nvme0n2"))
    {
        pro.start("sudo nvme id-ctrl /dev/nvme0n2 | grep sn");
        flag=2;
    }
    else if(temp.contains("/dev/nvme0n3"))
    {
        pro.start("sudo nvme id-ctrl /dev/nvme0n3 | grep sn");
        flag=2;
    }
    else if(temp.contains("/dev/nvme0n4"))
    {
        pro.start("sudo nvme id-ctrl /dev/nvme0n4 | grep sn");
        flag=2;
    }
    else
    {
        throw DiskSNException("OS ERR");
    }

    pro.waitForStarted();
    pro.waitForFinished();

    temp=pro.readAll();

    QString finalSN = "";

    if(flag==1)
    {
        QStringList temps = temp.split(",");
        for(int i =0 ;i<temps.length();i++)
        {
            QString tem = temps[i];
            if(tem.contains("SerialNo"))
            {
                finalSN=(tem.split("="))[1];
            }
        }
        if(finalSN=="")
        {
            throw DiskSNException("SD DISK ERR");
        }
    }
    else if(flag==2)
    {
        QStringList temps = temp.split("\n");
        for(int i =0 ;i<temps.length();i++)
        {
            QString tem = temps[i];
            if(tem.contains("sn"))
            {
                finalSN=(tem.split(":"))[1];
            }
        }

        if(finalSN=="")
        {
            throw DiskSNException("NV DISK ERR");
        }
    }

    finalSN=finalSN.replace("\r","");
    finalSN=finalSN.replace("\n","");
    finalSN=finalSN.replace("\t","");
    finalSN=finalSN.replace(" ","");
    std::string tempF = finalSN.toStdString();

    std::string tempF1 ="";
    for(int i=0;i<tempF.length();i++)
    {
        char charg = (char)tempF[i];
       if((charg>=48&&charg<=57)||(charg>=65&&charg<=90)||(charg>=97&&charg<=122))
       {
          tempF1+= charg;
       }

    }

    return QString::fromStdString(tempF1);
}
#endif
#ifdef WIN32
QString DiskSN::getSN()
{
     QProcess pro;
     pro.start("wmic diskdrive 0 get serialnumber");
     pro.waitForStarted();
     pro.waitForFinished();
     QString temp = pro.readAll();
     QStringList temps = temp.split("\r\n");
     QString final = "";
     final = temps[1];
     final = final.replace(" ","");
     final = final.replace("\r","");

     std::string tempF = final.toStdString();

     std::string tempF1 ="";
     for(int i=0;i<tempF.length();i++)
     {
         char charg = (char)tempF[i];
        if((charg>=48&&charg<=57)||(charg>=65&&charg<=90)||(charg>=97&&charg<=122))
        {
           tempF1+= charg;
        }

     }

     return QString::fromStdString(tempF1);
}
#endif
