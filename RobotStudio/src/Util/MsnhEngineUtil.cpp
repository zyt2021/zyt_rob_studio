﻿#include <Util/MsnhEngineUtil.h>


AxisMode EngineUtil::axisMode        = AXIS_Y_UP;
QColor   EngineUtil::highlightColor  = QColor(50,50,255);
QColor   EngineUtil::selectColor     = QColor(255,128,0);
QApplication* EngineUtil::app        = nullptr;
bool     EngineUtil::enableGama      = true;
bool     EngineUtil::enableAnti      = true;
float    EngineUtil::pointSize       = 2.0f;
bool     EngineUtil::physicalEngineRunning = false;


uint8_t  RobotUtil::chainNums       = 32;
QColor   RobotUtil::dragColor       = QColor(255,0,0);
QColor   RobotUtil::animationColor  = QColor(0,255,0);

void EngineUtil::setYAxisUp()
{
    EngineUtil::axisMode = AXIS_Y_UP;
}

void EngineUtil::setZAxisUp()
{
    EngineUtil::axisMode = AXIS_Z_UP;
}

void EngineUtil::setOpenGLDefault()
{
   EngineUtil::highlightColor  = QColor(50,50,255);
   EngineUtil::selectColor     = QColor(255,128,0);
   EngineUtil::enableGama      = true;
   EngineUtil::enableAnti      = true;
   EngineUtil::pointSize       = 2;
}

void RobotUtil::setRobotDefault()
{
   RobotUtil::chainNums       = 10;
   RobotUtil::dragColor       = QColor(255,0,0);
   RobotUtil::animationColor  = QColor(0,255,0);
}
