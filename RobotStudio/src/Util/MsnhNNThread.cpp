﻿#include<Util/MsnhNNThread.h>

NNThread::NNThread(QObject *parent):QThread(parent)
{
    qRegisterMetaType<std::vector<uint8_t>>("std::vector<uint8_t>");
    _so = new nn::socket(AF_SP, NN_SUB);
    _so->setsockopt(NN_SUB, NN_SUB_SUBSCRIBE,"",0);
    int linger = 2000000000;
    _so->setsockopt(NN_SOL_SOCKET,NN_RCVMAXSIZE,&linger,sizeof(linger));
}

NNThread::~NNThread()
{
    _running = false;
    quit();
    wait();
    delete _so;
}

void NNThread::startIt()
{
    start();
    _running = true;
}

void NNThread::stopIt()
{
    _running = false;
}

void NNThread::send(void *data, size_t len)
{
    _so->send(data, len,0);
}

void NNThread::setAddr(const QString &addr)
{
    _addr = addr;
}

void NNThread::run()
{
    _eid = _so->connect(_addr.toStdString().data());
    uint8_t* buff = nullptr;

    while(_running)
    {
        int revNum = _so->recv(&buff, NN_MSG, NN_DONTWAIT);
        if(revNum >0)
        {
            std::vector<uint8_t> datas;
            for (int i = 0; i < revNum; ++i)
            {
                datas.push_back(buff[i]);
            }
            emit revMsg(datas);
        }
        msleep(1);
    }
    _so->shutdown(_eid);
}

const QString &NNThread::getAddr() const
{
    return _addr;
}
