﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    bb = new BB(this);

    connect(bb, &BB::canShow, [this]()->void{
        auto objs = bb->objs;
        ui->plainTextEdit->clear();
        foreach (QObject* var, objs)
        {
            ui->plainTextEdit->appendPlainText(var->objectName());
        }
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_2_clicked()
{
    delete bb->objs[0];
}


void MainWindow::on_addBtn_clicked()
{
    bb->addObj();
}


BB::BB(QObject *parent):QObject(parent)
{

}

void BB::addObj()
{
    static int a = 0;
    a++;
    QObject *obj = new QObject();
    obj->setObjectName(QString("A%1").arg(a));
    obj->setParent(this);
}

void BB::push(QObject *obj)
{
    objs.push_back(obj);
}

void BB::childEvent(QChildEvent *event)
{
    if(event->added())
    {
        if(QObject* obj = qobject_cast<QObject*>(event->child()))
        {
            push(obj);
            emit canShow();
        }
    }
    else if(event->removed())
    {
        if(QObject* obj = qobject_cast<QObject*>(event->child()))
        {
            foreach (QObject* var, objs)
            {
                if(obj == var)
                {
                    objs.removeOne(var);
                    emit canShow();
                }
            }
        }
    }
}
