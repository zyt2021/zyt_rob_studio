﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QChildEvent>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class BB :public QObject
{
    Q_OBJECT
public:
    explicit BB(QObject *parent = nullptr);
    void addObj();

    QVector<QObject*> objs;

    void push(QObject *obj);

signals:
    void canShow();

protected:
    void childEvent(QChildEvent *event);
};


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_2_clicked();

    void on_addBtn_clicked();

private:
    Ui::MainWindow *ui;
    BB *bb;
};
#endif // MAINWINDOW_H
