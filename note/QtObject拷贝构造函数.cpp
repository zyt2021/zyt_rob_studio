﻿#include "mainwindow.h"
#include <QApplication>
#include <vld.h>
#include <iostream>
class B: public QObject
{
public:  //QObject的拷贝构造函数和使用
    explicit B(QObject *parent = nullptr):QObject(parent)
    {
        a = 10;
    }

    B(const B& b, QObject *parent = nullptr):QObject(parent)
    {
        this->a = b.a;
    }

    int getA() const;

private:
    int a = 0;
};


class A:public QObject  //QObject的拷贝构造函数和使用
{
public:
    A()
    {
        b = new B(this);
        c = new B(*b, this);
        addB(new B(*c,this));
        std::cout<<c->getA()<<std::flush;
    }
    B *b = nullptr;
    B *c = nullptr;

    void addB(B* b)
    {
        v.push_back(b);
    }

    std::vector<B*> v;
};


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    A C;

}

int B::getA() const
{
return a;
}
