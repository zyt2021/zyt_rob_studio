﻿#include <stdio.h>
class A
{
public:
    int getA()
    {
        return a;
    }

    virtual void setA(int a)
    {
        this->a = a;
    }
protected:
    int a = 0;
};

class B:public A
{
public:
    void init()
    {
        setA(11);//调用重写的方法
        A::setA(10); //如果还想调用父类原来的方法，则用这个
    }

    void setA(int a) override
    {
        cc = a;
    }

private:
    int cc = 0;
};


int main()
{
    B c;
    c.init();
    printf("%d",c.getA());

}
