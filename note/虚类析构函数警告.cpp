#include <iostream>
using namespace std;
class based{
public:
	based(){
	}
	//~based(){  //不加virtual flag 2不会释放
    virtual ~based(){ // ok
		 cout<<"flag 1"<<endl;
	}
	virtual void f(){
		cout<<"ok1"<<endl;
	}
};
class derived:public based{
 
public:
	~derived(){
		cout<<"flag 2"<<endl;
	}
	void f(){
		cout<<"ok2"<<endl;
	}
};
int main(){
based* p = new derived();
p->f();
delete p;
	
}